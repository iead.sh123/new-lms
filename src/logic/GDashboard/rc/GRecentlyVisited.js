import react, { useState } from "react"

import RRecentlyVisited from "./RRecentlyVisited";

function GRecentlyVisited({}){
  const recentVisited=[
    {day:"25",month:"Aug",cours:'java script for beginners',coursColor:'rgb(232,150,45)',progress:'70%' ,nextCorse:'what is dom',nextIcon:"fa fa-play"},
    {day:"17",month:"Aug",cours:'java script for beginners',coursColor:'rgb(122,150,45)',progress:'90%' ,nextCorse:'what is dom',nextIcon:"fa fa-play"},

    {day:"2",month:"Aug",cours:'java script for beginners',coursColor:'rgb(200,150,45)',progress:'15%' ,nextCorse:'what is dom',nextIcon:"fa fa-play"}]
  
    return(
        <div style={{width:'100%',marginLeft:'1rem'}}>
       
            {recentVisited.map(i=>
            <RRecentlyVisited 
            day={i.day} month={i.month} course={i.cours} coursColor={i.coursColor}
            progress={i.progress } nextCourse={i.nextCorse} nextIcon={i.nextIcon}/>
            )}
         
        </div>
    )
}
export default GRecentlyVisited;