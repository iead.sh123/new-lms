import RTabsPanel from "components/Global/RComs/RTabsPanel";
import RTabsPanel_Horizontal from "components/Global/RComs/RTabsPanel_Horizontal";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import React, { useCallback, useEffect, useState } from "react";
import { GCollaborationSettingContents } from "./GCollaborationSettingContents";
import { GCollaborationAgreementsClients } from "./GCollaborationAgreementsClients";
import { GCollaborationAgreementsProviders } from "./GCollaborationAgreementsProviders";
import tr from "components/Global/RComs/RTranslator";
import RFormModal from "components/Global/RComs/RFormModal";
import { post } from "jquery";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { useReducer } from "react";
import RSubmit from "components/Global/RComs/RSubmit";
import produce from "immer";
import { AgreementEditor } from "./AgreementEditor";
import { toast } from "react-toastify";

export const GCollaborationAgreements = () => {
	//  const[organizations,setOrganizations]= useState([]);
	//  const[contents,setContents]= useState([])
	//  const[levels,setLevels]= useState([])
	const [clientsCount, setClientsCount] = useState(0);
	const [providersCount, setProvidersCount] = useState(0);

	const [clientsRefresh, setClientsRefresh] = useState(0);

	const Tabs = [
		{
			icon: "fas fa-user-group",
			name: "clients",
			title: "My Clients",
			count: clientsCount,
			url: `${baseURL}/${genericPath}/collaboration_agreements/clients`,
			content: () => {
				return (
					<div>
						<GCollaborationAgreementsClients
							setClientsCount={setClientsCount}
							refresh={clientsRefresh}
							setOpenModal={setOpenAdd}
							setAgreementId={setAgreementId}
							setRefresh={setClientsRefresh}
						/>
					</div>
				);
			},
		},
		{
			icon: "fas fa-layer-group",
			name: "providers",
			title: "My Providers",
			count: providersCount,
			url: `${baseURL}/${genericPath}/collaboration_agreements/providers`,
			content: () => {
				return (
					<GCollaborationAgreementsProviders
						refresh={clientsRefresh}
						setProvidersCount={setProvidersCount}
						setAgreementId={setAgreementId}
					/>
				);
			},
		},
	];

	const addSubmit = async (values) => {
		const name = values[0].value;
		const desc = values[1].value;
		//------------------------------------
		const url = Services.collaboration.backend + `api/collaboration-level`;
		const payload = {}; //{"name" : name,"description" :desc,"order" :records.length+1
		//  };
		let response1 = await post(url, payload);
		if (response1 && response1.data && response1.data.status == 1) {
			setData1([
				...data1,
				{
					name: name,
					description: desc,
					creator: {
						id: response1.data.data[0].id,
						name: response1.data.data[0].creator.name,
						image: response1.data.data[0].creator.image,
					},
				},
			]);
			setOpenAdd(false);
		} else {
			toast.error(tr`Something went wrong`);
		}
		//-------------------------------
	};

	// return <div></div>

	const [openAdd, setOpenAdd] = useState(false);
	const [agreementId, setAgreementId] = useState(null);

	const updateClients = () => {
		setClientsRefresh(clientsRefresh + 1);
	};
	return (
		<div>
			{/* {Helper.js(organizations)} */}
			{/* <button
        onClick={() => {
          
        }}
      >
      </button> */}
			<div
				className="row"
				style={{
					display: "flex",
					justifyContent: "flex-end",
				}}
			>
				<RSubmit
					value={"Create new agreement"}
					icon={"fa fa-plus"}
					onClick={() => {
						setAgreementId(null);
						setOpenAdd(true);
					}}
					type="button"
					style={{ borderRadius: "2px", borderColor: "#668ad7" }}
				/>
			</div>
			<AgreementEditor agreement_id={agreementId} openAdd={openAdd} setOpenAdd={setOpenAdd} updateClients={updateClients} />

			<RTabsPanel_Horizontal
				// title={`Agreements`}
				Tabs={Tabs}
				changeHorizontalTabs={() => {}}
			/>
		</div>
	);
};
