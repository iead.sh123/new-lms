import React from "react";
import { initialState, reducer } from "./state/calender.reducer";
import {
	addComment,
	addNote,
	blockEventType,
	getEventTypesForRelations,
	initializeCalender,
	loadComments,
	loadNotes,
} from "./state/calender.action";
import { Container } from "reactstrap";
import Loader from "utils/Loader";
import RCalender from "view/Calender/RCalender";
import { useSelector } from "react-redux";
import GCalenderEventsRelation from "./GCalenderEventsRelation";
import tr from "components/Global/RComs/RTranslator";
import { BLOCK_EVENT_TYPE, UNBLOCK_EVENT_TYPE } from "./constants/calender-actions.constant";
import { calenderApi } from "api/calender";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { SUCCESS } from "utils/Alert";
import Event from "./Event";
import GNotesAndComments from "./notes-comments/GNotesAndComments";
import GFilterEvents from "./GFilterEvents/GFilterEvents";
import EventDetails from "./EventDetails/EventDetails";
import { createPrintPayload } from "./utils/print.util";
import GPrintCalender from "./Print/GPrintCalender";
import { toast } from "react-toastify";

const GCalender = ({ advanced = true }) => {
	//refs
	const mounted = React.useRef(false);

	//state
	const [calenderData, dispatch] = React.useReducer(reducer, initialState);
	const [loading, setLoading] = React.useState(false);
	const [showRelationEditor, setShowRelationEditor] = React.useState(false);
	const [selectedEvent, setSelectedEvent] = React.useState(null);
	const [commentsAndNotesModalMode, setCommentsAndNotesModalMode] = React.useState(null);
	const [filterModalIsOpen, setFilterModalIsOpen] = React.useState(false);
	const [eventTypesFilter, setEventTypesFilter] = React.useState([]);
	const [showDetails, setShowDetails] = React.useState(false);
	const [printedCal, setPrintedCal] = React.useState(null);

	//effects
	React.useEffect(() => {
		mounted.current = true;
		loadEvents();

		loadEventTypes();

		return () => {
			mounted.current = false;
		};
	}, []);

	const toggleCommentsAndNotesModal = (mode) => {
		if (mode !== "notes" || mode !== "comments") return setCommentsAndNotesModalMode(null);
		setCommentsAndNotesModalMode(mode);
	};

	const user = useSelector((state) => state.auth.user);

	// const organizationId = user?.current_organization ?? schoolAndTypes.find(st=>st.default_organization);

	const selectEvent = (eventId) => setSelectedEvent(eventId);

	//actions
	const loadEvents = async () => {
		setLoading(true);
		await initializeCalender(dispatch, mounted);
		setLoading(false);
	};

	const loadEventTypes = async () => {
		setLoading(true);
		await getEventTypesForRelations(dispatch);
		setLoading(false);
	};

	const handleSave = async () => {
		setLoading(true);
		try {
			const res = await calenderApi.saveRelations(
				{
					payload: calenderData.entities.event_types.ids.reduce(
						(prev, curr) => ({
							...prev,
							[curr]: calenderData.entities.event_types.byId[curr].blocks,
						}),
						{}
					),
				} // convert to {eventid: [blocked1 , blocked2] , event2Id: []}
			);
			if (!res.data.status) throw new Error(res.data.msg);
		} catch (err) {
			return toast.error(err?.message);
		} finally {
			setLoading(false);
		}
	};

	const toggleRelationEditor = React.useCallback(() => setShowRelationEditor(!showRelationEditor), [showRelationEditor]);

	const handleBlock = (blockerId, blockedId) => {
		dispatch({
			type: BLOCK_EVENT_TYPE,
			payload: {
				blockerId,
				blockedId,
			},
		});
	};

	const handleUnBlock = (blockerId, blockedId) => {
		dispatch({
			type: UNBLOCK_EVENT_TYPE,
			payload: {
				blockerId,
				blockedId,
			},
		});
	};

	const handleLoadComments = async (eventId) => {
		// setLoading(true)
		await loadComments(dispatch, eventId, user.id, mounted);
		// ssetLoading(false);
	};

	const handleLoadNotes = async (eventId) => {
		// setLoading(true)
		await loadNotes(dispatch, eventId, mounted);
		// setLoading(false);
	};

	const handleAddComment = async (eventId, text) => {
		await addComment(eventId, user.id, text, dispatch);
	};

	const handleAddNote = async (eventId, text) => {
		await addNote(eventId, user.id, text, dispatch);
	};

	const toggleFilterModal = () => {
		setFilterModalIsOpen(!filterModalIsOpen);
	};

	const handlePrint = async () => {
		try {
			const payload = createPrintPayload(Object.values(calenderData.entities.events.byId));

			const response = await calenderApi.print(payload);
			if (!response.data.status) throw new Error(response.data.msg);
			setPrintedCal(response.data.data);
		} catch (error) {
			toast.error(error?.message);
		}
	};

	const customButtons = React.useMemo(
		() => ({
			relationEditor: {
				text: tr`show relation editor`,
				click: toggleRelationEditor,
			},

			filterModal: {
				text: tr`filter`,
				click: toggleFilterModal,
			},
			print: {
				text: tr`Print`,
				click: handlePrint, // handlePrint
			},
		}),
		[toggleRelationEditor, showRelationEditor, handlePrint]
	);

	const handleShowComments = async (eventId) => {
		setCommentsAndNotesModalMode("comments");
		setSelectedEvent(eventId);
		await handleLoadComments(eventId);
	};

	const handleShowNotes = async (eventId) => {
		setCommentsAndNotesModalMode("notes");
		setSelectedEvent(eventId);
		await handleLoadNotes(eventId);
	};

	const eventDataTransform = (event) => ({
		...event,
		commentsCount: event.commentsCount,
		notesCount: event.notesCount,
		showComments: () => handleShowComments(event.id),
		showNotes: () => handleShowNotes(event.id),
		showDetails: () => toggleShowDetails(event.id),
		_id: event.id,
	});

	const handleSetEventTypeFilter = (types) => {
		setEventTypesFilter(types);
	};

	const toggleShowDetails = (eventId) => {
		if (!eventId) {
			setSelectedEvent(null);
			setShowDetails(false);
			return;
		}
		setSelectedEvent(eventId);
		setShowDetails(true);
	};

	const events = React.useMemo(() => {
		const eventsToShow = Object.values(calenderData?.entities?.events?.byId ?? {});
		if (!eventTypesFilter.length) return eventsToShow;

		return eventsToShow.filter((ev) => eventTypesFilter.map((t) => t.value).includes(ev.eventTypeId));
	}, [eventTypesFilter, calenderData?.entities?.events]);
	if (!advanced)
		return (
			<RCalender
				advanced={advanced}
				eventDataTransform={eventDataTransform}
				renderEvent={Event}
				handlePrint={handlePrint}
				toggleFilterModal={toggleFilterModal}
				customButtons={customButtons}
				events={events}
			/>
		);
	return (
		<Container className="content fc-toolbar-fixed" fluid style={{ position: "relative" }}>
			{/* <div
				style={{
					fontWeight: "bold",
				}}
			>
				<span>comments</span>
				<i className="fa fa-commenting mr-1" style={{ color: "red" }} />
				&nbsp;&nbsp;
				<span>notes</span>
				<i className="fa fa-sticky-note mr-1" style={{ color: "green" }} />
			</div> */}
			{loading ? (
				<Loader />
			) : showRelationEditor ? (
				<GCalenderEventsRelation
					handleSave={handleSave}
					goBack={toggleRelationEditor}
					eventTypes={calenderData.entities.event_types}
					handleBlock={handleBlock}
					handleUnBlock={handleUnBlock}
				/>
			) : (
				<RCalender
					eventDataTransform={eventDataTransform}
					handlePrint={handlePrint}
					toggleFilterModal={toggleFilterModal}
					renderEvent={Event}
					customButtons={customButtons}
					events={events}
				/>
			)}

			<GFilterEvents
				value={eventTypesFilter}
				isOpen={filterModalIsOpen}
				toggleModal={toggleFilterModal}
				eventTypes={calenderData.entities.event_types}
				handleSetFilter={handleSetEventTypeFilter}
			/>

			{selectedEvent && (
				<>
					<GNotesAndComments
						handleAdd={(text) =>
							commentsAndNotesModalMode === "notes" ? handleAddNote(selectedEvent, text) : handleAddComment(selectedEvent, text)
						}
						mode={commentsAndNotesModalMode}
						comments={calenderData.entities.events.byId?.[selectedEvent]?.[commentsAndNotesModalMode]}
						handleToggle={toggleCommentsAndNotesModal}
						isOpen={!!commentsAndNotesModalMode}
					/>
					<EventDetails
						toggleModal={toggleShowDetails}
						isOpen={showDetails}
						event={calenderData.entities.events.byId[selectedEvent]}
						showComments={handleShowComments}
						showNotes={handleShowNotes}
					/>
				</>
			)}

			<GPrintCalender isOpen={printedCal} handleToggle={() => setPrintedCal(null)} base64File={printedCal} />
		</Container>
	);
};

export default GCalender;
