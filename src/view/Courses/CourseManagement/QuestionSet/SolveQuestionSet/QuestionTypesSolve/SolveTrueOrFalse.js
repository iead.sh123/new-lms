import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Services } from "engine/services";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import styles from "../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";
import { successColor, dangerColor } from "config/constants";

const SolveTrueOrFalse = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  const SelectedValue =
    QuestionSetSolveData.questionAnswers?.questionAnswers.find(
      (item) => +item.questionContextId == +question.module_context_question_id
    );
  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      {question?.trueFalseStatement &&
        question?.trueFalseStatement.length > 0 &&
        question?.trueFalseStatement.map((item, index) => (
          <div key={item.id} className="mb-3 mt-3">
            <RFlex>
              <AppRadioButton
                name={"itemAnswer " + question?.id}
                disabled={QuestionSetSolveData.correctOrReviewMode}
                checked={
                  QuestionSetSolveData.submitted == "solving"
                    ? SelectedValue?.answer == +item?.order
                    : +question?.answer == +item?.order
                }
                onClick={(e) => {
                  QuestionSetSolveData.handleFillDataToSolveQuestion({
                    questionGroupId: questionGroup.id,
                    groupContextId: questionGroup.module_context_group_id,
                    questionContextId: question.module_context_question_id,
                    type: question.type.name,
                    key: "answer",
                    value: item.order,
                    isCorrect: true,
                  });
                }}
              />
              {QuestionSetSolveData.correctOrReviewMode ? (
                <span
                  className="d-flex pt-3"
                  style={{
                    color:
                      +question?.correctAnswer == +item.order
                        ? successColor
                        : +question?.answer == +item.order
                        ? dangerColor
                        : "",
                  }}
                >
                  {/* wrong answers */}
                  {+question?.answer == +item.order &&
                    +question?.answer !== +question.correctAnswer && (
                      <i className="fa-solid fa-xmark pt-1 pr-3 pl-0" />
                    )}
                  {/* correct answers */}
                  {+question?.correctAnswer == +item.order && (
                    <i className="fa-solid fa-check pt-1 pr-3 pl-0" />
                  )}
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{item.text}</p>
                </span>
              ) : (
                <span className="d-flex pt-2">
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{item.text}</p>
                </span>
              )}
            </RFlex>
            {item?.files && item?.files.length > 0 && (
              <img
                src={Services.storage.file + item?.files[0]?.hash_id}
                alt={item?.id}
                className={styles.image}
              />
            )}
          </div>
        ))}
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveTrueOrFalse;
