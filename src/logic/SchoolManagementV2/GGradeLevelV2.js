import React from 'react'
import RCollapsedDivV2 from './RCollapsedDivV2'
import { useState } from 'react'
import GCurriculaV2 from './GCurriculaV2'
import styles from './SchoolLister.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import { organizationTypes, types } from './constants'
import Swal, { DANGER } from 'utils/Alert'
import tr from "components/Global/RComs/RTranslator";
import { deleteSweetAlert } from 'components/Global/RComs/RAlert2'
import GCoursesCategories from './GCoursesCategories'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import { useMutateData } from 'hocs/useMutateData'
import { schoolManagementV2API } from 'api/SchoolManagementV2'

const GGradeLevelV2 = (
    {
        id,
        currentGrade,
        index,
        curriculas,
        allCurriculas,
        openedGradeLevelId,
        setFieldValue,
        handleChangingLevelsProperties,
        reOrderGradeLevelMutation,
        onDelete,
        openCurriculaId,
        levelsProperites,
        allGradeLevels,
        reOrderCurriculaMutation,
        courses,
    }
) => {
    const dispatch = useDispatch()
    console.log("GradeLevel", typeof (id))
    const user = useSelector((state) => state?.auth);
    // const organization_id = user && user?.user && user?.user?.organization_id;
    const organization_id = 1

    const createUpdateGradeLevelMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.createUpdateLevel(payload, organization_id),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ data, variables }) => {
            if (variables.payload?.fakeId) {
                const newGrades = JSON.parse(JSON.stringify(allGradeLevels))
                const gradeIndex = allGradeLevels.findIndex((grade) => grade.fakeId == id)
                newGrades[gradeIndex] = { ...data?.data }
                setFieldValue(types.GradeLevels, newGrades)
                const filteredArray = newGrades.filter((grade) => grade.id)

                reOrderGradeLevelMutation.mutate({ payload: { levels: [...filteredArray] } })
            }
            setSaved(true)
        }
    })
    const [saved, setSaved] = useState(id > 0 ? true : false)
    const [openCurriculaCategories, setOpenCurriculaCategories] = useState(false)
    const [saveClicked, setSaveClicked] = useState(false)
    const [enterPressed, setEnterPressed] = useState(false)
    const [alert1, setAlert] = useState(false);

    const [inputValue, setInputValue] = useState(currentGrade.name ?? "")
    const [addClicked, setAddClicked] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const isItActive = openedGradeLevelId == id ? true : false
    const handleCloseCoursesModal = () => {
        setOpenCurriculaCategories(false)
    }
    const editGradeLevel = () => {
        console.log("Edit Triggered")
        setEnterPressed(false)
        setSaved(!saved)
    }

    const hideAlert = () => setAlert(null);
    const showAlerts = (child) => setAlert(child);

    const deleteGradeLevel = (id, order) => {
        const confirm = tr`Yes, delete it`;
        const message = (
            <div>
                <h6>{tr`Are you sure? `}</h6>
                <p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
            </div>
        );
        deleteSweetAlert(showAlerts, hideAlert, onDelete, { gradeLevelId: id, order }, message, confirm);
    }

    const handleInputChange = (event) => {
        setSaveClicked(false)
        setAddClicked(false)
        setEnterPressed(false)
        setInputValue(event.target.value)
    }

    const handleDivClicked = (id) => {
        console.log("Div Clicked");
        const newProperties = {
            [types.OpenGradeLevelId]: openedGradeLevelId == id ? null : id,
            [types.OpenGradeLevelIndex]: openedGradeLevelId == id ? null : index
        }
        handleChangingLevelsProperties(newProperties)

    }


    const handleInputDown = async (event) => {
        if (event.key === 'Enter') {
            if (enterPressed)
                return
            setEnterPressed(true)
            setSaveClicked(true)
            if (inputValue != "") {
                const { type, attributes, ...other } = currentGrade
                const payload = {
                    ...other,
                    name: inputValue
                }
                createUpdateGradeLevelMutation.mutate({ payload })
            }
        }
    }
    const handleSaveButton = async () => {
        if (enterPressed) {
            return
        }
        setSaveClicked(true)
        setEnterPressed(true)
        if (inputValue != '') {
            const { type, attributes, ...other } = currentGrade
            const payload = {
                ...other,
                name: inputValue
            }
            createUpdateGradeLevelMutation.mutate({ payload })
        }
        // event.stopPropagation()
    }
    const generateCurriculaComponent = () => {
        if (id <= 0) {
            setAddClicked(true)
            return
        }
        const heighstOrder = curriculas.reduce((max, curricula) => curricula.order > max ? curricula.order : max, 0)
        const lastId = allCurriculas?.length + 1
        setFieldValue(types.Curriculas, [
            ...allCurriculas,
            { fakeId: -lastId, order: heighstOrder + 1, name: '', content_type_id: organizationTypes[types.Curriculas], parent_level_id: id }
        ])

    };

    const deleteCurriculaMutation = useMutateData({
        queryFn: ({ levelId }) => schoolManagementV2API.deleteLevel(levelId),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ variables }) => {
            const filteredArray = allCurriculas.filter((curricula) => curricula.id != variables.levelId)
            setFieldValue(types.Curriculas, filteredArray)
        }
    })

    const handleDeleteCurricula = ({ curriculaId }) => {
        if (curriculaId > 0) {
            deleteCurriculaMutation.mutate({ levelId: curriculaId })
        } else {
            const filteredArray = allCurriculas.filter((curricula) => curricula.fakeId ? curricula.fakeId != curriculaId : true)
            setFieldValue(types.Curriculas, filteredArray)
        }
    };
    const showAlert = (curriculaId) => {
        Swal.input({
            title: tr`are_you_sure_to_delete_it`,
            message: tr``,
            type: DANGER,
            placeHolder: "",
            onConfirm: () => handleDeleteCurricula(curriculaId),
            onCancel: () => null,
        });
    }
    const curriculasComponents = curriculas.map((curricula, index) => {
        return {
            component: <GCurriculaV2
                id={curricula.fakeId ?? parseInt(curricula.id)}
                currentCurricula={curricula}
                index={index}
                setFieldValue={setFieldValue}
                openCurriculaId={openCurriculaId}
                handleChangingLevelsProperties={handleChangingLevelsProperties}
                allCurriculas={allCurriculas}
                onDelete={handleDeleteCurricula}
                reOrderCurriculaMutation={reOrderCurriculaMutation}
                courses={courses}
            />,
            id: curricula.fakeId ?? parseInt(curricula.id)
        }
    })

    return (
        <>
            <RCollapsedDivV2
                handleInputChange={handleInputChange}
                handleInputDown={handleInputDown}
                handleDivClicked={handleDivClicked}
                handleEditFunctionality={editGradeLevel}
                handleDeleteFunctionality={deleteGradeLevel}
                handleSaveButton={handleSaveButton}
                generateChildComponent={generateCurriculaComponent}
                setSaved={setSaved}
                id={id}
                order={currentGrade.order}
                collapseChilds={curriculasComponents}
                inputValue={inputValue}
                openLevelId={openedGradeLevelId}
                isOpen={openedGradeLevelId == id ? true : false}
                saved={saved}
                addClicked={addClicked}
                saveClicked={saveClicked}
                number={curriculas?.length}
                inputPlaceHolder="Grade Level"
                buttonText="Add Curricula"
                currentType={types.GradeLevels}
                childType="Curricula"
                childPrefix={"childPrefix"}
                isItActive={isItActive}
                index={index}
                setOpenCurriculaCategories={setOpenCurriculaCategories}
                isLoading={createUpdateGradeLevelMutation?.isLoading}
            />
            <AppModal size={"lg"}
                show={openCurriculaCategories}
                header={true}
                parentHandleClose={handleCloseCoursesModal}
                headerSort={<GCoursesCategories handleCloseCoursesModal={handleCloseCoursesModal} />} />
            {alert1}
        </>
    )
}

export default GGradeLevelV2