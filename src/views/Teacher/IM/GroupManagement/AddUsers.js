import React from "react";

import tr from "components/Global/RComs/RTranslator";
import { IMApi } from "api/IMNewCycle";
import { Button } from "reactstrap";

import UserSelector from "../chatList/UserSelector";
import { useState } from "react";
import { toast } from "react-toastify";

function AddUsers({ chatId, loggedUserId, toggleAddUsers }) {
	const handleSubmit = async () => {
		try {
			const response = await IMApi.addGroupParticipants(chatId, {
				user_ids: users.map((rc) => rc.id),
			});
			if (!response.data.status) throw new Error(response.data.msg);
			toggleAddUsers();
		} catch (err) {
			toast.error(err?.message);
		}
	};

	const [users, setUsers] = useState([]);
	return (
		<>
			{" "}
			<UserSelector setUsers={setUsers} single={false}></UserSelector>
			{users?.length ? <Button onClick={handleSubmit}>{tr`submit`}</Button> : <></>}
		</>
	);

	// const debounceTimer = React.useRef(null);
	// const [receiverIds, setReceiversIds] = React.useState([]);
	// const [searchValue, setSearchValue] = React.useState("");

	// const onSelectedUsersChange = (values) =>
	//   setReceiversIds(
	//     values.map((v) => ({ id: v.id, username: v.label, rel: v.rel }))
	//   );

	// const debouncedLoadUsers = React.useCallback(
	//   (delay = 1000) => {
	//     const loadUsers = async () => {
	//       const response = await IMApi.getAllUsers(searchValue);
	//       if (!response.data.status) return [];

	//       return response.data?.data?.models
	//         .filter((usr) => +usr.id !== loggedUserId)
	//         .map((usr) => ({
	//           label: usr.full_name,
	//           value: usr.id,
	//           id: usr.id,
	//           rel: usr.label,
	//         }));
	//     };

	//     return new Promise((resolve) => {
	//       clearTimeout(debounceTimer.current);
	//       debounceTimer.current = setTimeout(() => {
	//         resolve(loadUsers());
	//       }, delay);
	//     });
	//   },
	//   [searchValue]
	// );

	// return (
	//   <div>
	//     <div className={"new_messege_show1"} style={{ height: "70vh" }}>
	//       {/* <div className="new_messege_show_head">
	//         <button className="show_icon" onClick={toggleAddUsers}>
	//           <i className="fa fa-angle-left"></i>
	//         </button>
	//         {tr("Add Users")}
	//       </div> */}
	//       <div className="RAutocomplete">
	//         <AsyncSelect
	//           isMulti
	//           onInputChange={(val) => setSearchValue(val)}
	//           defaultValue={[]}
	//           loadOptions={() => debouncedLoadUsers(1000)}
	//           onChange={onSelectedUsersChange}
	//         />
	//         <ul>
	//           {receiverIds.map((rc) => (
	//             <li key={"rc-" + rc.id}>
	//               <div>
	//                 {" "}
	//                 {rc.username} : {rc.rel}{" "}
	//               </div>
	//             </li>
	//           ))}
	//         </ul>
	//       </div>

	//       {receiverIds?.length ? (
	//         <Button onClick={handleSubmit}>{tr`submit`}</Button>
	//       ) : (
	//         <></>
	//       )}
	//     </div>
	//   </div>
	// );
}

export default AddUsers;
