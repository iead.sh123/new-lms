import React from "react";
import { Row, Col, Table } from "reactstrap";
import styles from "./RPrinting.Module.scss";
import tr from "../RTranslator";

const RubricPrinting = ({ data }) => {
  return (
    <>
      {data?.map((category) => (
        <Table bordered>
          <thead>
            <tr>
              <th className="ListHeading">
                <Row>
                  <Col sm={8} xs={12}>
                    {category?.subject}
                  </Col>
                  <Col
                    sm={4}
                    xs={12}
                    style={{
                      display: "flex",
                    }}
                  >
                    <Col>
                      <h6>{tr`total_points`}</h6>
                    </Col>
                    <Col>
                      <h6>{category?.total_points}</h6>
                    </Col>
                  </Col>
                </Row>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                {category?.standards?.map((standard, index) => (
                  <>
                    <Row className={styles.RPrintMargin}>
                      <Col
                        sm={6}
                        xs={12}
                        style={{
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        <space className={styles.RPrintSpaceRubricTitle}>
                          <h6 className={styles.colorTitle}>
                            {tr`subject`} : &nbsp;
                          </h6>
                          <h6>{standard?.subject}</h6>
                        </space>
                      </Col>
                      <Col
                        sm={6}
                        xs={12}
                        style={{
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        <space className={styles.RPrintSpaceRubricTitle}>
                          <h6 className={styles.colorTitle}>
                            {tr`description`} : &nbsp;
                          </h6>
                          <h6>{standard?.description}</h6>
                        </space>
                      </Col>
                    </Row>

                    <Table>
                      <thead>
                        <tr>
                          {standard?.ratings.map((rating, index) => (
                            <>
                              <th
                                colSpan={2}
                                className={styles.colorTitle}
                                style={{ textAlign: "center" }}
                              >
                                {rating.title}
                              </th>
                            </>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          {standard?.ratings.map((rating, index) => (
                            <>
                              <td
                                style={{
                                  textAlign: "center",
                                  maxWidth: 50,
                                }}
                              >
                                {rating.score}
                              </td>
                              <td
                                style={{
                                  textAlign: "center",
                                  maxWidth: 50,
                                }}
                              >
                                {rating.description}
                              </td>
                            </>
                          ))}
                        </tr>
                      </tbody>
                    </Table>
                  </>
                ))}
              </td>
            </tr>
          </tbody>
        </Table>
      ))}
    </>
  );
};

export default RubricPrinting;
