import RFlex from "components/Global/RComs/RFlex/RFlex";
import RPaginatorLocal from "components/Global/RComs/RPaginatorLocal";
import { usePaginate } from "components/Global/RComs/RPaginatorLocal";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RSelect from "components/Global/RComs/RSelect";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import { enrollment_components } from "logic/TermManagement/constants/enrolment.components.constant";
import React from "react";
import { Row, Col, Table, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import Loader from "utils/Loader";
import EnrollTeachersEditor from "./components/EnrollTeachersEditor";
import EnrollStudentsEditor from "./components/EnrollStudentsEditor";
import { homeRoomOrCurriculaMode } from "logic/TermManagement/constants/homeroms-curricula-editor-modes.constants";
import CreateNewHomeRoomOrCurriculaModal from "./components/CreateNewHomeRoomOrCurriculaModal";
import ChangeHomeRoomOrClassModal from "./components/ChangeHomeRoomOrClassModal";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

const HomeRoomsAndCurriculaEditor = ({
	mode,
	edited,
	homeRooms,
	handleLoadGradeLevelStudents,
	handleSelect,
	teachers,
	students,
	gradeLevels,
	handleLoadOrganizationTeachers,
	curricula,
	handleEnrollUsers,
	handleDeleteUser,
	_selectedCurriculum,
	handleSave,
	handleChangeUserClassOrHomeRoom,
}) => {
	const [loadingHomeRoom, setLoadingHomeRoom] = React.useState(false);

	const [enrollType, setEnrollType] = React.useState(null);

	const [selectedCurriculum, setSelectedCurriculum] = React.useState(_selectedCurriculum);

	const [createModalIsOpen, setCreateModalIsOpen] = React.useState(false);
	const handleToggleCreationModal = () => setCreateModalIsOpen(!createModalIsOpen);

	const [changeModalIsOpen, setchangeModalIsOpen] = React.useState(false);

	const [editedStudent, setEditedStudent] = React.useState(null);

	const handleToggleChangeModal = () => setchangeModalIsOpen(!changeModalIsOpen);

	const changeEdited = async (id) => {
		setLoadingHomeRoom(true);
		await handleSelect(id);
		setLoadingHomeRoom(false);
	};

	const {
		currentPage: t_currentPage,
		totalPages: t_totalPages,
		handleNext: t_handleNext,
		handlePrevious: t_handlePrevious,
		handleLast: t_handleLast,
		handleFirst: t_handleFirst,
		pItems: teacherPaginatedIds,
	} = usePaginate(10, edited?.teachers?.ids ?? []);

	const {
		currentPage: s_currentPage,
		totalPages: s_totalPages,
		handleNext: s_handleNext,
		handlePrevious: s_handlePrevious,
		handleLast: s_handleLast,
		handleFirst: s_handleFirst,
		pItems: studentPaginatedIds,
	} = usePaginate(10, edited?.students?.ids ?? []);

	const EnrollmentComponent = {
		[enrollment_components.TEACHER]: EnrollTeachersEditor,
		[enrollment_components.STUDENT]: EnrollStudentsEditor,
	}[enrollType];

	const enrollmentComponentProps = {
		[enrollment_components.TEACHER]: {
			handleLoadOrganizationTeachers,
			teachers: { ...teachers, ids: teachers?.ids?.filter?.((i) => !edited?.teachers?.ids?.some?.((p) => p === i)) ?? [] },
			handleEnrollUsers: (user) => handleEnrollUsers(edited.id, user),
		},
		[enrollment_components.STUDENT]: {
			handleLoadGradeLevelStudents,
			gradeLevels,
			students: { ...students, ids: students?.ids?.filter?.((i) => !edited?.students?.ids?.some?.((p) => p === i)) ?? [] },
			handleEnrollUsers: (user) => handleEnrollUsers(edited.id, user),
		},
	}[enrollType];

	return (
		<React.Fragment>
			{mode === homeRoomOrCurriculaMode.HOMEROOM ? (
				<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between", color: "" }}>
					<select
						name="homerooms"
						id="homerooms"
						className=" mt-4 mb-4 border-0"
						onChange={(e) => +e.target.value !== edited.id && changeEdited(+e.target.value)}
					>
						{homeRooms.ids.map((hrId) => (
							<option key={"select-home-room-" + hrId} value={hrId} selected={hrId === edited.id}>
								{homeRooms.byId[hrId].name}
							</option>
						))}
					</select>
					<p role="button" onClick={handleToggleCreationModal} style={{ color: "#668AD7" }}>
						<i className="fa fa-plus" /> {tr`Create New Homeroom`}
					</p>
				</RFlex>
			) : (
				<Row>
					<Col xs={10}>
						<RFlex styleProps={{ alignItems: "center" }}>
							{selectedCurriculum}
							<select
								name="curriculum"
								id="curricula"
								className=" mt-4 mb-4 border-0 h4 flex-grow-2"
								onChange={(e) => setSelectedCurriculum(+e.target.value)}
							>
								{curricula.ids.map((cId) => (
									<option key={"select-curricula-" + cId} value={cId} selected={cId === edited.id}>
										{curricula.byId[cId].name}
									</option>
								))}
							</select>
							<RFlex styleProps={{ justifyContent: "space-between", width: "50%", marginBottom: "15px" }}>
								{curricula.byId[selectedCurriculum]?.courses?.map((co) => (
									<div key={"curr-courses" + co.id} onClick={() => co.id !== edited.id && changeEdited(co.id)}>
										<p
											style={{ ...(edited.id === co.id && { backgroundColor: "#668AD7", color: "#FFF" }) }}
											className="h4 p-2"
											role="button"
										>
											{co.name}
										</p>
									</div>
								))}
							</RFlex>
						</RFlex>
					</Col>
					<Col xs={2} className="d-flex justify-content-end">
						<RFlex styleProps={{ justifyContent: "end", alignItems: "center", marginTop: "40px" }}>
							<p role="button" onClick={handleToggleCreationModal} className="h5" style={{ color: "#668AD7" }}>
								{" "}
								<i className="fa fa-plus" /> {tr`Create New Course`}
							</p>
						</RFlex>
					</Col>
				</Row>
			)}
			{loadingHomeRoom ? (
				<Loader />
			) : (
				<React.Fragment>
					<Row>
						<Col md="6">
							<Table style={{ color: "#000" }} bordered responsive>
								<thead style={{ backgroundColor: "#D7E3FD", fontWeight: "bold" }}>
									<tr style={{ textAlign: "center" }}>
										<td>{tr`Teacher Name`}</td>
										<td>{tr`Action`}</td>
									</tr>
								</thead>
								<tbody>
									{teacherPaginatedIds?.map((tId) => (
										<tr key={"homeroom-teacher-" + tId}>
											<td className="text-center">
												<RFlex>
													<img
														width={30}
														height={30}
														style={{ borderRadius: "100%" }}
														src={Services.auth_organization_management.file + edited?.teachers?.byId?.[tId]?.image}
													/>
													<span>{edited?.teachers?.byId?.[tId]?.full_name}</span>
												</RFlex>
											</td>

											<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
												<RButton
													className="btn-icon ml-1 mr-1"
													color={"danger"}
													size="sm"
													type="button"
													onClick={() => {
														handleDeleteUser(edited.id, enrollment_components.TEACHER, tId);
													}}
													faicon={iconsFa6.delete}
													// text={"remove"}
												/>
												{/* <UncontrolledDropdown direction="end">
													<DropdownToggle
														aria-haspopup={true}
														// caret
														color="default"
														data-toggle="dropdown"
														nav
														style={{
															color: "black",
															fontWeight: "bolder",
															fontSize: "large",
															// background: `black`,
															// borderRadius: "100%",
														}}
													>
														<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer" }}></i>
													</DropdownToggle>
													<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
														<DropdownItem
															key={"un-enroll-teacher" + tId}
															onClick={() => {
																// action?.onClick?.();
																handleDeleteUser(edited.id, enrollment_components.TEACHER, tId);
															}}
														>
															<RFlex
																styleProps={{
																	alignItems: "center",
																	justifyContent: "space-between",
																}}
															>
																<i className="fa fa-trash" aria-hidden="true" />
																<span>{tr`remove`}</span>
															</RFlex>
														</DropdownItem>
													</DropdownMenu>
												</UncontrolledDropdown> */}
											</td>
										</tr>
									))}
								</tbody>
							</Table>
							<Row>
								<Col lg="5">
									<p
										onClick={() => setEnrollType(enrollment_components.TEACHER)}
										className="mt-4"
										style={{ color: "#668AD7", cursor: "pointer" }}
									>
										<i className="fa fa-plus" /> {tr`Enroll New Teacher`}
									</p>
								</Col>
								<Col lg="7" className="p-0 mr-0 ml-0 d-flex justify-content-end">
									<RPaginatorLocal
										total={t_totalPages}
										currentPage={t_currentPage}
										handleNext={t_handleNext}
										handlePrevious={t_handlePrevious}
										handleLast={t_handleLast}
										handleFirst={t_handleFirst}
									/>
								</Col>
							</Row>
						</Col>
						<Col md="6">
							<Table style={{ color: "#000" }} bordered responsive>
								<thead style={{ backgroundColor: "#D7E3FD", fontWeight: "bold" }}>
									<tr style={{ textAlign: "center" }}>
										<td>{tr`Student Name`}</td>
										<td>{tr`Action`}</td>
									</tr>
								</thead>
								<tbody>
									{studentPaginatedIds?.map((tId) => (
										<tr key={"homeroom-teacher-" + tId}>
											<td className="text-center">
												<RFlex>
													<img
														width={30}
														height={30}
														style={{ borderRadius: "100%" }}
														src={Services.auth_organization_management.file + edited?.students?.byId?.[tId]?.image}
													/>
													<span>{edited.students.byId[tId].full_name}</span>
												</RFlex>
											</td>

											<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
												<UncontrolledDropdown direction="right">
													<DropdownToggle
														aria-haspopup={true}
														// caret
														color="default"
														data-toggle="dropdown"
														nav
														style={{
															color: "black",
															fontWeight: "bolder",
															fontSize: "large",
															// background: `black`,
															// borderRadius: "100%",
														}}
													>
														<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer" }}></i>
													</DropdownToggle>
													<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
														<DropdownItem
															key={"un-enroll-student" + tId}
															onClick={() => {
																// action?.onClick?.();
																handleDeleteUser(edited.id, enrollment_components.STUDENT, tId);
															}}
														>
															<RFlex
																styleProps={{
																	alignItems: "center",
																	justifyContent: "space-between",
																}}
															>
																<i className={iconsFa6.delete} aria-hidden="true" />
																<span>{tr`remove`}</span>
															</RFlex>
														</DropdownItem>
														<DropdownItem
															key={"change-class-hr-student" + tId}
															onClick={() => {
																setEditedStudent(students.byId[tId]);
																handleToggleChangeModal();
															}}
														>
															<RFlex
																styleProps={{
																	alignItems: "center",
																	justifyContent: "space-between",
																}}
															>
																<i className="fa fa-users" aria-hidden="true" />
																<span>{mode === homeRoomOrCurriculaMode.HOMEROOM ? tr`change Homeroom` : tr`Change Class`}</span>
															</RFlex>
														</DropdownItem>
													</DropdownMenu>
												</UncontrolledDropdown>
											</td>
										</tr>
									))}
								</tbody>
							</Table>
							<Row>
								<Col lg="5">
									<p
										onClick={() => setEnrollType(enrollment_components.STUDENT)}
										className="mt-4"
										style={{ color: "#668AD7", cursor: "pointer" }}
									>
										<i className="fa fa-plus" /> {tr`Enroll New Student`}
									</p>
								</Col>

								<Col lg="7">
									<RPaginatorLocal
										total={s_totalPages}
										currentPage={s_currentPage}
										handleNext={s_handleNext}
										handlePrevious={s_handlePrevious}
										handleLast={s_handleLast}
										handleFirst={s_handleFirst}
									/>
								</Col>
							</Row>
						</Col>
					</Row>

					<Row>{enrollType && <EnrollmentComponent {...enrollmentComponentProps} />}</Row>
				</React.Fragment>
			)}
			<CreateNewHomeRoomOrCurriculaModal
				teachers={teachers}
				students={students}
				handleLoadGradeLevelStudents={handleLoadGradeLevelStudents}
				handleLoadOrganizationTeachers={handleLoadOrganizationTeachers}
				mode={mode}
				isOpen={createModalIsOpen}
				toggle={handleToggleCreationModal}
				handleSave={async (data) => {
					const id = await handleSave(data);
					id && changeEdited(id);
				}}
			/>

			<ChangeHomeRoomOrClassModal
				mode={mode}
				isOpen={changeModalIsOpen}
				toggle={handleToggleChangeModal}
				handleSave={async (newId) => {
					await handleChangeUserClassOrHomeRoom(
						enrollment_components.STUDENT,
						{ ...editedStudent, type: enrollment_components.STUDENT },
						edited.id,
						newId
					);
					handleToggleChangeModal();
				}}
				options={
					mode === homeRoomOrCurriculaMode.HOMEROOM
						? Object.values(homeRooms.byId).map((h) => ({
								label: h.name,
								value: h.id,
								students: h.statistics.students,
						  }))
						: curricula.byId[selectedCurriculum]?.courses?.map((c) => ({
								label: c.name,
								value: c.id,
								students: Object.values(students.byId).filter((std) => std?.courses?.some?.((co) => +co.id === +c.id))?.length ?? 0,
						  }))
				}
				value={edited.id}
			/>
		</React.Fragment>
	);
};

export default HomeRoomsAndCurriculaEditor;
