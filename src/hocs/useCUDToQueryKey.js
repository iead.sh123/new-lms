import { useQueryClient } from "@tanstack/react-query";
import { setDeepProperty, getDeepProperty } from "utils/ObjectHelper";
export const useCUDToQueryKey = () => {
	const operations = { ADD: "add", UPDATE: "update", DELETE: "delete", REPLACE: "replace" };
	const queryClient = useQueryClient();
	const CUDToQueryKey = ({ queryKey, insertionDepth, id, newData = {}, operation = "add" }) => {
		queryClient.setQueryData(queryKey, (oldData) => {
			try {
				if (!oldData) {
					throw new Error("Invalid Query Key");
				}
			} catch (error) {
				console.error("Caught an error:", error.message);
			}
			// make a deep clone of the oldData instance
			const tempData = JSON.parse(JSON.stringify(oldData));
			// get the array you want to add the newData element to
			const oldArray = getDeepProperty(tempData, insertionDepth);
			// check if the return data is exist in thatdepth and if it's is array

			try {
				if (!oldArray || !Array.isArray(oldArray)) throw new Error("the inseration Depth is not exis or it is not an array");
			} catch (error) {
				console.error("Caught an error:", error.message);
			}
			const newDataId = id ?? newData.id;
			let newArray = [];
			switch (operation) {
				case operations.ADD:
					newArray = [...oldArray, newData];
					setDeepProperty(tempData, insertionDepth, newArray);
					return tempData;
				case operations.UPDATE:
					newArray = [...oldArray];
					newArray = newArray.map((item) => {
						const itemId = item.id ? item.id : item.fakeId;
						return itemId == newDataId ? { ...item, ...newData } : item;
					});
					setDeepProperty(tempData, insertionDepth, newArray);
					return tempData;
				case operations.DELETE:
					newArray = [...oldArray];
					newArray = newArray.filter((item) => {
						const itemId = item.id ? item.id : item.fakeId;
						return itemId != newDataId;
					});

					setDeepProperty(tempData, insertionDepth, newArray);
					return tempData;
				case operations.REPLACE:
					newArray = newData;
					setDeepProperty(tempData, insertionDepth, newArray);
					return tempData;
				default:
					return tempData;
			}
		});
	};
	return { CUDToQueryKey, operations };
};
