import Select from "react-select";
import styles from "./AppSelect.Module.scss";

const AppSelect = (props) => {
  return (
    <Select
      className={`react-select primary rounded ${styles["app-select"]}`}
      style={{zIndex: 100}}
      {...props}
    />
  );
};

export default AppSelect;
