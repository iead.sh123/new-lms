import RFlex from 'components/Global/RComs/RFlex/RFlex'
import styles from './grading.module.scss'
import React from 'react'
import iconsFa6 from 'variables/iconsFa6'
import { Collapse } from 'reactstrap'
import RItem from './RItem'
const RGroupItem = ({ groupId, groupCollapseDetails, activeCollapse, handleToggleGroupCollapse, studentId, isStudent }) => {
    console.log("groupCollapseDetails", groupCollapseDetails)
    return (
        <RFlex className={styles.GroupItemsCollapse} styleProps={{ gap: '30px' }} onClick={() => handleToggleGroupCollapse(groupId)}>
            <RFlex className="justify-content-between align-items-center" styleProps={{ width: '100%' }}>
                <RFlex className="justify-content-between" styleProps={{ width: "375px" }}>
                    <span className='p-0 m-0'>{groupCollapseDetails?.name} {groupCollapseDetails?.items?.ids?.length}</span>
                    <span className='p-0 m-0'>Final Mark : {groupCollapseDetails?.groupMark?.toFixed(2)}</span>
                </RFlex>
                {activeCollapse ?
                    <i className={iconsFa6.chevronDown} /> : <i className={iconsFa6.chevronRight} />}
            </RFlex>
            <Collapse isOpen={activeCollapse} >
                <RFlex className="flex-column" styleProps={{ gap: '40px', cursor: 'default' }} onClick={(event) => event.stopPropagation()}>
                    {groupCollapseDetails?.items?.ids?.map((id, index) => {
                        const item = groupCollapseDetails?.items?.[id]
                        return <RItem isStudent={isStudent} item={item} groupId={groupId} itemId={id} studentId={studentId} />
                    })}
                </RFlex>
            </Collapse>
        </RFlex>
    )
}

export default RGroupItem