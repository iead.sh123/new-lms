import React, { useEffect, useState, useMemo } from "react";
import Swal, { DANGER } from "utils/Alert";
import { baseURL, genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom";
import { Services } from "engine/services";
import { Row, Col } from "reactstrap";
import { post } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RGroupButtons from "components/Global/RComs/RGroupButtons/RGroupButtons";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { successColor } from "config/constants";
import { toast } from "react-toastify";

const GLessonPlanLister = () => {
	const history = useHistory();
	const { courseId, curriculumId, cohortId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [loading, setLoading] = useState(false);
	const [filterData, setFilterData] = useState({
		left: "all",
		right: "recently",
	});
	const [filters, setFilters] = useState({ left: [], right: [] });
	const [rightFilter, setRightFilter] = useState([]);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handlePickFromCollaboration = () => {
		history.push(
			`${baseURL}/${genericPath}/collaboration_area_contents/${
				courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
			}/lesson_plans`
		);
	};

	const handlePushToLessonPlanEditor = () => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/${`course/${courseId}`}/cohort/${cohortId}/editor/lesson-plans`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/lesson-plans`);
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const Request = {
			payload: courseId ? { course_id: courseId } : { curriculum_id: curriculumId },
		};
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&withPagination=${true}`;

		const url = `${Services.lesson_plan.backend}api/v1/lesson-plan/get${QueryReq}`;

		let response = await post(specific_url ? specific_url : url, Request);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleSearch = async (emptySearch) => {
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&${filterData.right}=${true}&withPagination=${true}&filter=${emptySearch ?? searchData}`;

		let specific_url;
		specific_url = `${Services.lesson_plan.backend}api/v1/lesson-plan/get${QueryReq}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg);
		}
	};

	const handleSearchOnLeftTabs = async (leftFilter) => {
		setFilterData({ ...filterData, left: leftFilter });
		let QueryReq = "";
		QueryReq = `?${leftFilter}=${true}&${filterData.right}=${true}&withPagination=${true}${searchData ? `&filter=${searchData}` : ""}`;

		let specific_url;
		specific_url = `${Services.lesson_plan.backend}api/v1/lesson-plan/get${QueryReq}`;

		setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setLoading(false);
			} else {
				setLoading(false);
			}
		} else {
			setLoading(false);
			toast.error(response.data.msg);
		}
	};

	const handleSearchOnRightTabs = async (rightFilter) => {
		setFilterData({ ...filterData, right: rightFilter });
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&${rightFilter}=${true}&withPagination=${true}${searchData ? `&filter=${searchData}` : ""}`;

		let specific_url;
		specific_url = `${Services.lesson_plan.backend}api/v1/lesson-plan/get${QueryReq}`;

		setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setLoading(false);
			} else {
				setLoading(false);
			}
		} else {
			setLoading(false);
			toast.error(response.data.msg);
		}
	};

	// const handelChangeStatue = () => {};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.LessonPlans?.data.map((r) => ({
			id: r.id,
			table_name: "GLessonPlanLister",

			details: [
				{ key: tr`name`, value: r?.name },
				{
					key: tr("status"),
					value: r.published ? (
						<h6
							style={{ cursor: "pointer", color: successColor, fontWeight: 400 }}
							// onClick={() => {
							// 	handelChangeStatue(lsn.id, true);
							// }}
						>
							{tr("published")}
						</h6>
					) : (
						<h6
							style={{ cursor: "pointer", color: "#DD0000", fontWeight: 400 }}
							// onClick={() => {
							// 	handelChangeStatue(lsn.id, true);
							// }}
						>
							{tr("draft")}
						</h6>
					),
					html: true,
				},
			],
			actions: [
				{
					name: tr`view`,
					icon: "fa-solid fa-eye",
					color: "primary",
					onClick: () => {
						if (courseId) {
							history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/lesson-plan/${r.id}`);
						} else if (curriculumId) {
							history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/lesson-plan/${r.id}`);
						}
					},
				},
				{
					name: tr`print`,
					icon: "fa-solid fa-print",
					color: "primary",
					onClick: () => {},
					disabled: true,
				},
				{
					name: tr`edit`,
					icon: "fa-solid fa-pencil",
					color: "primary",
					onClick: () => {
						if (courseId) {
							history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/lesson-plans/${r.id}`);
						} else if (curriculumId) {
							history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/lesson-plans/${r.id}`);
						}
					},
				},
			],
		}));
		setFilters(response?.data?.data?.filters);
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.LessonPlans,
			},
		}),
		[processedRecords]
	);

	useEffect(() => {
		if (filters && filters.right.length > 0) {
			const updatedRightFilter = filters.right.map((filter, index) => ({
				id: index,
				title: filter.title,
				color: "primary",
				outline: true,
				active: false,
				onClick: handleSearchOnRightTabs,
			}));

			setRightFilter(updatedRightFilter);
		}
	}, [filters]);
	return (
		<>
			<RFlex>
				<RSearchHeader
					searchLoading={searchLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					handlePushToAnotherRouteWhenAdd={handlePushToLessonPlanEditor}
					addNew={true}
					buttonName={tr`create_new_lesson_plan`}
					outline={false}
				/>
				<RButton
					text={tr`pick_from_collaboration`}
					faicon={iconsFa6.globe}
					color={"primary"}
					outline
					onClick={handlePickFromCollaboration}
				/>
			</RFlex>

			<Row>
				{filters?.left.length > 0 && (
					<Col xs={12} md={8}>
						<RFilterTabs tabs={filters.left} handleSearchOnTabs={handleSearchOnLeftTabs} />
					</Col>
				)}
				{rightFilter.length > 0 && (
					<Col xs={12} md={4} className="d-flex justify-content-end">
						<RGroupButtons buttons={rightFilter} selectedFilter={filterData.right} />
					</Col>
				)}

				<Col xs={12}>
					<RAdvancedLister {...propsLiterals.listerProps} />
				</Col>
			</Row>
		</>
	);
};

export default GLessonPlanLister;
