import RTags from "components/Global/RComs/RTags";
import React from "react";
import UserImg from "../userImg";
import { Row, Col } from "reactstrap";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import userLogo2 from '../../../../assets/img/testUserImage.svg'
import searchIcon from '../../../../assets/img/searchIcon.svg'
import GroupAvatar from "assets/img/new/svg/group_avatar.svg";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";

function ChatHeader(props) {
  //console.log(props.chat)
  const [search, setSearch] = React.useState(false);
  const searchAreaRef = React.useRef(null);
  //console.log("adasdhkjasldasdlkad", props.users)
  const handleSearchMessage = (msgId) => {
    setSearch(false);
    props.handleSearch(msgId);
  };

	const hideSearch = React.useCallback(
		(e) => {
			if (searchAreaRef.current?.contains(e.target) || e?.target?.id === "search-toggler-im-single") return;
			setSearch(false);
		},
		[search, setSearch]
	);

  let shownindex = 0;
  const userNumber =props?.chat?.entities?.users?.ids?.length
 //const onlineUsersNumber=0;
  const onlineUsersNumber=props?.chat?.entities?.users?.byId?Object.values(props?.chat?.entities?.users?.byId)?.filter((user)=>user?.online==true)?.length:0;
  //Helper.cl(onlineUsersNumber1,"onlineUsersNumber")

  React.useEffect(() => {
    window.addEventListener("click", hideSearch);
    return () => window.removeEventListener("click", hideSearch);
  }, []);
  return (
    <>
      <RFlex
        id="Whole Header"
        className="justify-content-between align-items-center"
        styleProps={{ paddingRight: '20px' }}>

        <RFlex id="Left Side" styleProps={{ cursor: 'pointer' }} className="align-items-center" onClick={props.openGroupManagementModal}>
          {props.users.map((i, index) => {
            let image=Services?.storage?.file + i?.image
            if (typeof(image)==='string'){
            if (image?.endsWith('null') || image?.endsWith('undefined')){
              image=props.chat?.name? GroupAvatar:UserAvatar
            }
          }
            if (i.userId == props.loggedUserId);
            else
              return (
                <UserImg
                  index={0}
                  image={image}
                  // image={userLogo2}
                  online={i.online}
                  name={i.name}
                  width={props.floating === "true" ? 30 : 56}
                  id={i.id}
                  length={props.users.length}
                />

              );
          })}

          <RFlex id="Down Whole Header" className="flex-column" style={{ gap: '5px' }}>
            <ul className="chat_List_Item_hul">
              {props.users.map((i, index) => {
                if (index == 0) shownindex = 0;
                if (i.userId == props.loggedUserId);
                else {
                  shownindex++;
                  return (
                    <li key={"users-" + i.id} className="chat_List_Item_hli">
                      {" "}
                      {shownindex > 1 ? "," : ""} {i.name}
                    </li>
                  );
                }
              })}
            </ul>
            {userNumber>2 &&
            <span className="p-0 m-0">{userNumber} Members, {onlineUsersNumber} Online</span>
            }
          </RFlex>

        </RFlex>

        <RFlex id='input search and icon' className="align-items-center">
          {((props.selectedMultipleMessages?.length)
            && (!search)
          ) ? <div id="search Input" style={{ width: '483px' }}>
            {props.messageActions.map(a => {

              return <button
                class="c_icon_button c_first_button"
                // id="settings_im"
                // className="add_new_but"
                onClick={a.action}
              >
                <i
                  className={a.icon}
                  data-toggle="tooltip"
                  data-placement="top"
                  title={tr(a.label)}
                ></i>
              </button>

            })}

          </div> : <> </>}

          {search && (
            <>
              <div style={{ width: "483px" }} id="search input 2" ref={searchAreaRef}>
                <RTags
                  // defaultLabel={formGroup.tags}
                  url={`${Services.im.backend}api/chats/search-chat/${props.chatId}/`} //"
                  getArrayFromResponse={(res) =>
                    res && res.data && res.data.status
                      ? res.data.data.data
                      : null
                  }
                  placeholder="Search"
                  minLetters={1}
                  getValueFromArrayItem={(i) => i.message}
                  getLabelFromArrayItem={(i) => i.message}
                  getIdFromArrayItem={(i) => i.id}
                  // defaultValues={formGroup.tags}
                  disableAddition
                  onChange={(ids) => handleSearchMessage(ids?.[0]?.id)}
                />
              </div>
            </>
          )}
          <img
            id="search-toggler-im-single"
            src={searchIcon}
            onClick={() => setSearch(!search)}
            style={{ fontSize: "20px", cursor: "pointer" }}
          />

        </RFlex>
      </RFlex>

			{props.floating === "true" ? (
				<div className="blocks">
					<button className="header_icon" onClick={props.close}>
						<i className="fa fa-close"></i>
					</button>
				</div>
			) : (
				""
			)}
		</>
	);
}

export default ChatHeader;
