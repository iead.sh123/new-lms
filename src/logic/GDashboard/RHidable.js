import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import placeholder from "assets/img/placeholder.jpg";
import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RImageStack from 'components/Global/RComs/RImageStack';
import RNoDataLine from 'components/Global/RComs/RNoDataLine';
import RHeader from 'components/Global/RComs/RHeader';
const RHidable = ({count,children,nodata,nodataComponent,title}) => {



  return (<>
  {title?<RHeader title={title} count={count}/>:<></>}
    <div style={{width:"100%",display:(count>0)?"block":"none"}}>
          {children}
    </div>
    <div style={{width:"100%",display:(count>0)?"none":"block"}}>
    {nodataComponent?? <>
               <RNoDataLine messageNoData={nodata}/> 
            </>}

    </div>
   </>
  );
};

export default RHidable;
