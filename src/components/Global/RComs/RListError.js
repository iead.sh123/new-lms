import React from "react";
import tr from "./RTranslator";

const RListError = ({ data }) => {
  return (
    <>
      <h5 style={{ textAlign: "center", fontWeight: "bold" }}>{tr`Errors`}</h5>
      {data?.map((el) => (
        <div
          className="alert alert-danger  second-bg-color content bt-2 mt-3 "
          style={{ fontWeight: "bold" }}
          role="alert"
        >
          <ul>
            <li>{el}</li>
          </ul>
        </div>
      ))}
    </>
  );
};

export default RListError;
