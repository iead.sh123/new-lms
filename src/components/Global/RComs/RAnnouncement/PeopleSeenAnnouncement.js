import React, { useState } from "react";
import withDirection from "hocs/withDirection";
import tr from "../RTranslator";
import styles from "./RAnnouncement.Module.scss";
import { thirdColor } from "config/constants";
import AppModal from "components/Global/ModalCustomize/AppModal";
import REmptyData from "components/RComponents/REmptyData";

const PeopleSeenAnnouncement = ({ announcement, dir }) => {
	const [show, setShow] = useState(false);

	const handleClose = () => {
		setShow(!show);
	};

	const handleOpen = () => {
		setShow(!show);
	};

	return (
		<>
			{announcement && announcement?.seen_by_users && (
				<>
					<AppModal
						size="md"
						show={show}
						parentHandleClose={handleClose}
						header={tr`All People`}
						headerSort={
							announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length > 0 ? (
								announcement?.seen_by_users.map((el) => <p>{el.name}</p>)
							) : (
								<>
									<REmptyData line1={tr`No People To Show`} />
								</>
							)
						}
					/>
					<p
						style={{
							display: "flex",
							justifyContent: dir ? "end" : "start",
							cursor: "pointer",
						}}
					>
						<div
							style={{
								display: "flex",
								justifyContent: dir ? "end" : "start",
							}}
							className={announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length > 0 && styles.showPeoples}
						>
							<p className={styles.numberHover} onClick={handleOpen}>
								{announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length}
							</p>
							&nbsp;
							<i className="fa fa-eye" style={{ fontSize: "19px", color: thirdColor }}></i>
							{announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length > 0 && (
								<span>
									{announcement && announcement?.seen_by_users && announcement.seen_by_users.slice(0, 4).map((el) => <p>{el.name}</p>)}
									&nbsp;
									{announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length > 4 && (
										<p>
											{tr`and`} &nbsp;
											{announcement && announcement?.seen_by_users && announcement?.seen_by_users?.length - 4} &nbsp;
											{tr`other_people`}
										</p>
									)}
								</span>
							)}
						</div>
					</p>
				</>
			)}
		</>
	);
};

export default withDirection(PeopleSeenAnnouncement);
