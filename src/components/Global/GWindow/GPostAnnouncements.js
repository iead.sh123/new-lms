import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  FetchPostAnnouncementsAsync,
  RemovePostAnnouncementAsync,
  setFlag,
} from "store/actions/admin/schoolEvents";
import InfiniteScroll from "react-infinite-scroll-component";
import RAnnouncement from "../RComs/RAnnouncement/RAnnouncement";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";
import { trackLastSeenAsync } from "store/actions/global/track.action";

const GPostAnnouncements = () => {
  const dispatch = useDispatch();
  const { schoolEventID, iDHomeRoom, announcibleID } = useParams();

  const {
    postAnnouncements,
    postLoadMorePage,
    postAnnouncementsLoading,
    hasAddPermission,
    flagAnnouncement,
    postAnnouncementsEmpty,
  } = useSelector((state) => state.SchoolEventsRed);

  const { user } = useSelector((state) => state?.auth);

  useEffect(() => {
    dispatch(setFlag(true));

    dispatch(RemovePostAnnouncementAsync());

    if (postAnnouncementsEmpty) {
      dispatch(
        FetchPostAnnouncementsAsync(
          {
            payload: {
              type:
                user?.type == "teacher"
                  ? "home_rooms"
                  : user?.type == "employee" && !schoolEventID
                  ? ""
                  : "school_events",
              type_id:
                user?.type == "teacher"
                  ? iDHomeRoom
                  : user?.type == "employee" && !schoolEventID
                  ? ""
                  : schoolEventID,
            },
          },
          1
        )
      );
    }
  }, [postAnnouncementsEmpty]);

  useEffect(() => {
    dispatch(
      trackLastSeenAsync({ payload: { trackable_type: "announcements" } })
    );
  }, [postAnnouncements]);

  const fetchData = () => {
    postLoadMorePage !== null &&
      dispatch(
        FetchPostAnnouncementsAsync(
          {
            payload: {
              type:
                user?.type == "teacher"
                  ? "home_rooms"
                  : user?.type == "employee" && !schoolEventID
                  ? ""
                  : "school_events",
              type_id:
                user?.type == "teacher"
                  ? iDHomeRoom
                  : user?.type == "employee" && !schoolEventID
                  ? ""
                  : schoolEventID,
            },
          },
          postLoadMorePage
        )
      );
  };

  return (
    <div className="content">
      <div
        id="scrollableDiv"
        className={
          user?.type == "parent" || user?.type == "student"
            ? "container"
            : "content"
        }
        style={{
          paddingTop:
            (user?.type == "parent" || user?.type == "student") && "25vh",
          height: user?.type == "employee" && !announcibleID ? "80vh" : "50vh",
          overflow: "auto",
        }}
      >
        <InfiniteScroll
          dataLength={postAnnouncements?.length}
          next={fetchData}
          hasMore={postLoadMorePage !== null ? true : false}
          loader={<Loader />}
          scrollableTarget="scrollableDiv"
          style={{ overflow: "hidden" }}

          // endMessage={
          //   <h4
          //     style={{ textAlign: "center" }}
          //   >{tr`you_show_all_announcements`}</h4>
          // }
        >
          {/* {postAnnouncementsLoading ? (
            <h6>Loading ...</h6>
          ) : (
            <RAnnouncement
              Announcements={postAnnouncements}
              hasAddPermission={hasAddPermission}
              schoolEventID={schoolEventID}
              iDHomeRoom={iDHomeRoom}
            />
          )} */}

          <RAnnouncement
            Announcements={postAnnouncements}
            hasAddPermission={hasAddPermission}
            schoolEventID={schoolEventID}
            iDHomeRoom={iDHomeRoom}
            flag={flagAnnouncement}
          />
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default GPostAnnouncements;
