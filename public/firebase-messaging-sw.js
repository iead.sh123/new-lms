importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyAyIUFYX8cqO5I8Ls8ZFIpmL2pSTZBIbAY",
  authDomain: "ugaritcloud.firebaseapp.com",
  projectId: "ugaritcloud",
  storageBucket: "ugaritcloud.appspot.com",
  messagingSenderId: "902777650653",
  appId: "1:902777650653:web:8f492225032bda7e6d8a34",
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

// if(navigator?.serviceWorker)
// {
// // navigator?.serviceWorker?.register("/firebase-messaging-sw.js").then(s => {
// //   console.log("Background : Firebase Worker is registered", s);
// // })
// }
// else

// {
//   console.log("Background : Firebase Worker is not registered , Becaue serviceWorker is false");


// }
messaging.onBackgroundMessage(function (payload) {
  const channel4Broadcast = new BroadcastChannel("channel4");
  channel4Broadcast.postMessage(payload);

  let notify = false;
  if (
    payload?.data?.notifyMe &&
    (payload?.data?.notifyMe == "true" ||
      payload?.data?.notifyMe == "1" ||
      payload?.data?.notifyMe == 1 ||
      payload?.data?.notifyMe == true) &&
    payload?.data?.shown &&
    (payload?.data?.shown == "true" ||
      payload?.data?.shown == "1" ||
      payload?.data?.shown == 1 ||
      payload?.data?.shown == true)
  ) {
    notify = true;
  } else {
    notify = false;
  }

  if (notify) {
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
      icon: "/logo192.png",
    };
    return self.registration.showNotification(
      notificationTitle,
      notificationOptions
    );
  } else {
    return 0;
  }
});
