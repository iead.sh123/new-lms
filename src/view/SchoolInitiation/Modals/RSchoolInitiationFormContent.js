import React, { useContext } from "react";
import { primaryColor, successColor } from "config/constants";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { useHistory, useParams } from "react-router-dom";
import RSchoolInitiationButtons from "./RSchoolInitiationButtons";
import GEducationStage from "logic/SchoolInitiation/Modals/GEducationStage";
import GGradeLevelForm from "logic/SchoolInitiation/Modals/GGradeLevelForm";
import GCurriculaForm from "logic/SchoolInitiation/Modals/GCurriculaForm";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { baseURL, genericPath } from "engine/config";

const RSchoolInitiationFormContent = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { sIType, sITypeId, tabTitle } = useParams();
  const history = useHistory();
  return (
    <div>
      {sIType == "education_stage" ? (
        <GGradeLevelForm />
      ) : sIType == "grade_levels" ? (
        <GCurriculaForm />
      ) : (
        <GEducationStage />
      )}

      <RFlex
        className="mt-3"
        styleProps={{
          justifyContent: SchoolInitiationData.message
            ? "space-between"
            : "end",
        }}
      >
        {SchoolInitiationData.message && (
          <p style={{ color: successColor }}>{SchoolInitiationData.message}</p>
        )}
        {!sIType && <RSchoolInitiationButtons />}
      </RFlex>

      {SchoolInitiationData.message && (
        <RFlex
          className="mt-3"
          styleProps={{
            justifyContent: "space-between",
            padding: "0px 10px",
          }}
        >
          <p
            style={{
              color: primaryColor,
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={() => SchoolInitiationData.handleShowAll()}
          >{tr`show_all`}</p>

          {sIType == "education_stage" ? (
            <p
              style={{
                color: primaryColor,
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={() => {
                history.push(
                  `${baseURL}/${genericPath}/school-initiation/${"grade_levels"}/${
                    SchoolInitiationData.idAdded
                  }/${"properties"}`
                );
                SchoolInitiationData.handleClose();
                SchoolInitiationData.setMessage(null);
              }}
            >{tr`create_curricula`}</p>
          ) : sIType == "grade_levels" ? (
            <p
              style={{
                color: primaryColor,
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={() => {
                history.push(
                  `${baseURL}/${genericPath}/school-initiation/${"curricula"}/${
                    SchoolInitiationData.idAdded
                  }/${"properties"}`
                );
                SchoolInitiationData.handleClose();
                SchoolInitiationData.setMessage(null);
              }}
            >{tr`create_a_book`}</p>
          ) : (
            <p
              style={{
                color: primaryColor,
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={() => {
                history.push(
                  `${baseURL}/${genericPath}/school-initiation/${"education_stage"}/${
                    SchoolInitiationData.idAdded
                  }/${"properties"}`
                );
                SchoolInitiationData.handleClose();
                SchoolInitiationData.setMessage(null);
              }}
            >{tr`create_grade_level`}</p>
          )}
        </RFlex>
      )}
    </div>
  );
};

export default RSchoolInitiationFormContent;
