import { useQuery } from "@tanstack/react-query";
import { toast } from "react-toastify";

export const useFetchDataRQ = ({
	queryKey,
	queryFn,
	enableCondition = true,
	dontFetchOnMount,
	onSuccessFn,
	keepPreviousData,
	onErrorFn,
	selectFn,
}) => {
	const data = useQuery({
		queryKey,
		queryFn,
		keepPreviousData: keepPreviousData ? true : false,
		refetchOnWindowFocus: false,
		refetchOnMount: dontFetchOnMount ? false : true,
		retry: 1,
		enabled: enableCondition,
		onError: (error) => {
			toast.error(error?.message);
		},

		onSuccess: (data) => {
			// Because the response from the backend adopts this method, I don't know why
			if (data.data.status === 0 || data.data.code == 500) {
				// toast.error(data.data.msg ?? data.msg);

				// data.data.msg?.map((el) => toast.error(el)) ?? data.msg?.map((el) => toast.error(el));

				onErrorFn && onErrorFn(data.data.msg ?? data.msg);
			} else {
				onSuccessFn && onSuccessFn(data);
			}
		},

		select: (data) => {
			if (selectFn) {
				return selectFn(data);
			}
			const FetchData = Array.isArray(data.data) ? data.data.map((el) => el) : data.data;
			return { data: FetchData };
		},
	});
	// if (data.isError) {
	// 	toast.error(data.error?.message);
	// }
	return data;
};
