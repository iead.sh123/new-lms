import React, { useState }  from "react";
import './RInlineTextbox.css';

 function RInlineTextbox({text,setText,label}) {

  const [mode,setMode]=useState('label')
  const edit=()=>{
    setMode('edit')
     }
  
  const agree=(event)=>{
    setMode('label')
     }   
   return(
    <div style={{width: "100%",marginLeft:"0px",paddingLeft:"0px"}}>{label}
     <div className="text_label_edit">
      <div  className="text_label">
      {mode=='label'?<span>{text}</span>:<input value={text} style={{width:"90px"}} onChange={event => setText(event.target.value)}/>}
      </div>
      <div className="edit_but">
        <button style={{backgroundColor:'#D7E3FD'}} onClick={mode=='label'?()=>edit():()=>agree()}>
           {mode=='label'?<i className="fa-solid fa-pen"> </i> :  <i className="fa-solid fa-check"> </i> }
        </button>
      </div>
     </div>
     </div>
    )
   }
 
export default RInlineTextbox;