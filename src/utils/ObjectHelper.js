export const setDeepProperty = (obj, path, value, separator = ".") => {
	const properties = path.split(separator);
	let current = obj;

	// Iterate over the properties array, except for the last item
	for (let i = 0; i < properties.length - 1; i++) {
		const property = properties[i];

		// If the property doesn't exist or isn't an object, create it or overwrite it
		if (!(property in current) || typeof current[property] !== "object") {
			current[property] = {};
		}

		// Move our reference deeper into the object
		current = current[property];
	}

	// Set the final property to the new value
	const finalProperty = properties[properties.length - 1];
	current[finalProperty] = value;
};

export const getDeepProperty = (obj, path, separator = ".") => {
	const properties = path.split(separator);
	let current = JSON.parse(JSON.stringify(obj));

	for (let i = 0; i < properties.length; i++) {
		const property = properties[i];

		// If the property does not exist, return null
		if (!(property in current)) {
			return null;
		}

		// Move our reference deeper into the object
		current = current[property];
	}

	// Return the found value
	return current;
};

// // Example usage:
// const myObj = {
//     user: {
//         name: "John Doe",
//         address: {
//             street: "123 Main St",
//             city: "Anytown"
//         }
//     }
// };

// // Modify a deep property
// setDeepProperty(myObj, 'user.address.city', 'Newtown');
// console.log(myObj.user.address.city); // Outputs: Newtown

// // Add a new deep property
// setDeepProperty(myObj, 'user.contact.email', 'john.doe@example.com');
// console.log(myObj.user.contact.email); // Outputs: john.doe@example.com
