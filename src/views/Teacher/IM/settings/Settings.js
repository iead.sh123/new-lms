import RForm from 'components/Global/RComs/RForm';
import tr from 'components/Global/RComs/RTranslator';
import React from 'react';


const Settings = ({ settings, updateSettings, handleSubmit,toggleSettings }) => {

    // const setRef = React.useRef(handleSubmit);

    // React.useEffect(()=>{
    //     return ()=>{
    //         //to pass in refernce pointer to prevent effect callback memoization 
    //         setRef.current?.()
    //     };
    // },[])
   
    const formProps = React.useMemo(() => ({
        formItems: Object.entries(settings).map(es => ({
            label: tr(es[0]),
            type: es[0] === 'archive_chats_times' ? 'number' : 'checkbox',
            name: es[0],
            md: "12",
            checked: es[1],
            //   required: true,
            onChange: (e) => updateSettings(es[0], es[0] === 'archive_chats_times' ? e.target.value : e.target.checked),
            value: es[1],
            id: es[0],
        })),
        withSubmit: true,
        onSubmit: (e)=> e.preventDefault() || handleSubmit()
    }), [settings,updateSettings]);



    return <div>

        <div className={"new_messege_show "}>
            <div className="new_messege_show_head">
                <button
                    className="show_icon"
                    onClick={toggleSettings}
                >
                    <i className="fa fa-angle-left"></i>
                </button>
                {tr("settings")}
            </div>
            <RForm {...formProps} />
        </div>
    </div>


}

export default Settings;