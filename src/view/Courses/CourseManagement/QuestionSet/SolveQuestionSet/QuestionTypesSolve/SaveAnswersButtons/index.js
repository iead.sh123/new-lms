import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../../SolveQuestionSet.module.scss";

const SaveAnswersButtons = ({ questionGroup, question }) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);
  const STEP =
    QuestionSetSolveData?.questionsToSolve?.questionGroups[
      QuestionSetSolveData.step
    ];

  return (
    <RFlex styleProps={{ justifyContent: "end" }}>
      <RButton
        // faicon={"fa-solid fa-check"}
        text={tr`save_my_answer`}
        color={"primary"}
        onClick={() =>
          QuestionSetSolveData.handleConfirmTheSolveToTheQuestion(
            questionGroup.id,
            question.id
          )
        }
        disabled={
          questionGroup.isGroup
            ? !QuestionSetSolveData.questionAnswers?.questionAnswers.some(
                (item) =>
                  item.isCorrect && question.type.name == "UPLOAD_FILE"
                    ? item.files.length > 0
                    : item.answer !== "" &&
                      +item.questionContextId ===
                        +question.module_context_question_id
              )
            : !QuestionSetSolveData.questionAnswers?.questionAnswers.some(
                (item) =>
                  item.isCorrect && question.type.name == "UPLOAD_FILE"
                    ? item.files.length > 0
                    : item.answer !== "" && +item.groupId === +questionGroup.id
              )
        }
      />
      <RButton
        text={tr`reset_answer`}
        color={"link"}
        className={styles.reset_button}
        onClick={() =>
          QuestionSetSolveData.handleResetSolveQuestion({
            questionGroupId: questionGroup.id,
            questionId: question.id,
            requiredQuestion: question.isRequired,
            moduleContextQuestionId: question.module_context_question_id,
            submissionType: question.type.name,
          })
        }
        disabled={
          questionGroup.isGroup
            ? !QuestionSetSolveData.questionAnswers?.questionAnswers.some(
                (item) =>
                  item.isCorrect && question.type.name == "UPLOAD_FILE"
                    ? item.files.length > 0
                    : item.answer !== "" &&
                      +item.questionContextId ===
                        +question.module_context_question_id
              )
            : !QuestionSetSolveData.questionAnswers?.questionAnswers.some(
                (item) =>
                  item.isCorrect && question.type.name == "UPLOAD_FILE"
                    ? item.files.length > 0
                    : item.answer !== "" && +item.groupId === +questionGroup.id
              )
        }
      />
    </RFlex>
  );
};

export default SaveAnswersButtons;
