import React from "react";
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    UncontrolledDropdown,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import updateTheme from "store/actions/student/themes.action";

const RThemeSwitcher = () => {

    const dispatch = useDispatch();
    const theme = useSelector(state => state.theme);
    const swapTheme = (theme) => dispatch(updateTheme(theme));

    return (
        <div>
            <UncontrolledDropdown
                className="btn-magnify"
        /* className="btn-rotate" */ nav
            >
                <DropdownToggle
                    aria-haspopup={true}
                    caret
                    color="default"
                    data-toggle="dropdown"
                    id="navbarDropdownMenuLink"
                    nav
                >
                    <i className="nc-icon nc-image" />
                </DropdownToggle>
                <DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
                    {theme.themes.map((elem) => {
                        return (
                            <DropdownItem
                                key={elem}
                                onClick={() => swapTheme(elem)}
                                style={
                                    theme.selectedTheme === elem
                                        ? { backgroundColor: "#DDD", color: "#FFF" }
                                        : {}
                                }
                            >
                                <i>{elem}</i>
                            </DropdownItem>
                        );
                    })}
                </DropdownMenu>
            </UncontrolledDropdown>
        </div>
    );
};

export default RThemeSwitcher;
