import { studentCoursesAPis } from "api/student/courses";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export function mergeKeys(arr, key) {
	return arr.reduce((acc, d) => {
		const found = acc.find((a) => a[key] === d[key]);
		//const value = { name: d.name, val: d.value };
		const value = { value: d.value, count: d.count }; // the element in data property
		if (!found) {
			//acc.push(...value);
			acc.push({ [key]: d[key], data: [value] }); // not found, so need to add data property
		} else {
			//acc.push({ name: d.name, data: [{ value: d.value }, { count: d.count }] });
			found.data.push(value); // if found, that means data property exists, so just push new element to found.data.
		}
		return acc;
	}, []);
}

export const groupBy = (array, key) => {
	// Return the end result
	return array.reduce((result, currentValue) => {
		// If an array already present for key, push it to the array. Else create an array and push the object
		(result[currentValue[key]] = result[currentValue[key]] || []).push(currentValue);
		// Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
		return result;
	}, {}); // empty object is the initial value for result object
};

export const getPlaceHolderContent = async (e, rab_id, user) => {
	const response = studentCoursesAPis.getPlaceHolderContent(e.detail.qid, rab_id);

	if (response.data.status == 1) {
		let type = response.data.data["type"];
		switch (type) {
			case "qs-linked":
				let patternId = response.data.data.models["id"];
				if (user["type"] === "teacher") {
					window.open(`${baseUrl}teacher/courses/pattern/${patternId}`);
				} else {
					if (user["type"] === "student" || user["type"] === "learner") {
						window.open(`${baseUrl}questionset/course/${rab_id}/pattern/${patternId}`);
					} else {
						toast.error(tr`You Are Not Allowed !`);
					}
				}

				break;
			case "qs":
				let qsPatternId = response.data.data.models["id"];
				if (user["type"] === "teacher") {
					window.open(`${process.env.REACT_APP_BASE_URL}/teacher/courses/pattern/${qsPatternId}`);
				} else {
					if (user["type"] === "student" || user["type"] === "learner") {
						window.open(`${process.env.REACT_APP_BASE_URL}/student/courses/pattern/${qsPatternId}`);
					} else {
						toast.error(tr`You Are Not Allowed !`);
					}
				}
				break;
			case "file":
				let url = response.data.data.models;
				window.open(`${resourcesBaseUrl}/${url}`);
				break;
			case "link":
				url = response.data.data.models;
				window.open(`${url}`);
				break;
			default:
				break;
		}
	} else {
		toast.error(response.data.msg);
	}
};

export const switchLang = (lang) => {
	if (lang === "switch") {
		if ("en" === "en") {
			localStorage.setItem("lang", "ar");
			// axios.defaults.headers.common["lang"] = "ar";
		} else {
			localStorage.setItem("lang", "en");
			// axios.defaults.headers.common["lang"] = "en";
		}
	} else {
		localStorage.setItem("lang", lang);
		// axios.defaults.headers.common["lang"] = lang;
	}
	window.location.reload();
};
