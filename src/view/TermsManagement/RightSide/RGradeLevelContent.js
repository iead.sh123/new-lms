import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { primaryColor } from "config/constants";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../TermsManagement.module.scss";

const RGradeLevelContent = ({ users, handleSortBy }) => {
	const TermsManagementData = useContext(TermsManagementContext);
	const displayedUsers = TermsManagementData.showAllStudentsInGradeLevel ? users : users?.slice(0, 12);

	const viewAsButton = (
		<RFlex
			id="view as"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr`sort_by`}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const viewAsFilterActions = [{ label: tr`alphabetically`, action: () => handleSortBy() }];
	return (
		<RFlex styleProps={{ flexDirection: "column", gap: 20 }}>
			<h6>{TermsManagementData.itemObj?.dataObj?.title}</h6>

			<RFlex styleProps={{ width: "100%", justifyContent: "flex-end" }}>
				<RDropdownIcon actions={viewAsFilterActions} component={viewAsButton} />
			</RFlex>

			<RFlex styleProps={{ flexDirection: "column", flexWrap: "wrap" }}>
				<RFlex>
					<span style={{ fontWeight: "bold" }}>{tr`students`}</span>
					<div className={styles.users__count}>{users?.length}</div>
				</RFlex>
				<RFlex styleProps={{ flexWrap: "wrap" }}>
					{displayedUsers?.map((student, index) => {
						return (
							<div key={index} style={{ width: "31%" }}>
								<RProfileName key={student.id} name={student.name} img={student.hash_id} />
							</div>
						);
					})}{" "}
				</RFlex>
			</RFlex>

			{users?.length > 12 && (
				<RFlex>
					<span
						style={{ width: "fit-content", color: primaryColor, cursor: "pointer" }}
						onClick={() => TermsManagementData.handleShowAllStudentsInGradeLevel()}
					>
						{!TermsManagementData.showAllStudentsInGradeLevel ? tr`hide_student` : tr`show_all_students`}
					</span>
				</RFlex>
			)}
		</RFlex>
	);
};

export default RGradeLevelContent;
