import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import styles from "./RWindow.Module.scss";
import withDirection from "hocs/withDirection";

const RWindow = ({ windowData, nodeIndex, dir }) => {
  const [show, setShow] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [indexAction, setIndexAction] = useState(null);

  return (
    <Row>
      <div className={dir ? styles["show-div"] : styles["show-div-rtl"]}></div>
      <div
        className={
          show
            ? dir
              ? styles.hidden
              : styles["hidden-rtl"]
            : dir
            ? styles["hidden-close"]
            : styles["hidden-close-rtl"]
        }
      >
        {show ? (
          <i
            class="fas fa-plus-square fa-2x"
            aria-hidden="true"
            onClick={() => setShow(!show)}
          ></i>
        ) : (
          <i
            class="fa fa-times fa-2x"
            aria-hidden="true"
            onClick={() => {
              setShow(!show);
              setShowForm(false);
            }}
          ></i>
        )}
      </div>

      <Col xs={12}>
        {!show && (
          <div className={styles.div}>
            {windowData.actions.map((action, index) => (
              <>
                <p
                  onClick={() => {
                    setShowForm(!showForm);
                    setIndexAction(index);
                  }}
                  style={{ color: action.color }}
                >
                  {action.icon && (
                    <i
                      className={action.icon}
                      style={{ color: action.color }}
                    ></i>
                  )}
                  {action.name}
                </p>
              </>
            ))}
          </div>
        )}
      </Col>
      {showForm && (
        <Col xs={12}>{windowData?.actions[indexAction].component}</Col>
      )}
    </Row>
  );
};

export default withDirection(RWindow);
