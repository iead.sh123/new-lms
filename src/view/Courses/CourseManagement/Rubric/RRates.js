import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";

import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import { FieldArray } from "formik";
import { Input, FormGroup, FormText } from "reactstrap";
import { dangerColor } from "config/constants";

const RRates = ({ categoryData }) => {
	const RubricData = useContext(RubricContext);

	return (
		<FieldArray name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings`}>
			{(arrayHelpers) => {
				return (
					<RFlex className={styles.rubric__editor__rates}>
						<label>{tr("rates")}</label>
						<RFlex className={styles.rubric__editor__rate}>
							{RubricData.values.categories[categoryData.categoryIndex]?.standards?.[categoryData.standardIndex]?.ratings?.map(
								(rate, ratingIndex) => (
									<RFlex key={ratingIndex} styleProps={{ alignItems: "center" }}>
										<Input
											type="text"
											name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.title`}
											value={rate?.title}
											onChange={RubricData.handleChange}
											placeholder={tr`rate`}
										/>
										{RubricData.values.pointed && (
											<FormGroup className="mb-0">
												<Input
													type="number"
													name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.score`}
													value={rate?.score ?? null}
													onChange={RubricData.handleChange}
													placeholder={tr`rate's_points`}
													min={0}
													className={
														RubricData?.errors?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex]
															?.ratings[ratingIndex]?.score
													}
												/>
												<FormText color="danger">
													{
														RubricData?.errors?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex]
															?.ratings[ratingIndex]?.score
													}
												</FormText>
											</FormGroup>
										)}
										<i
											className={iconsFa6.delete + " cursor-pointer"}
											style={{ color: dangerColor }}
											onClick={() => arrayHelpers.remove(ratingIndex)}
										/>
									</RFlex>
								)
							)}
						</RFlex>
						<RFlex
							styleProps={{ color: primaryColor, alignItems: "center", width: "fit-content", cursor: "pointer" }}
							onClick={() => {
								arrayHelpers.push({
									fakeId: Math.random(),
									title: "",
									score: null,
								});
							}}
						>
							<i className={iconsFa6.plus} />
							<span>{tr`new_rate`}</span>
						</RFlex>
					</RFlex>
				);
			}}
		</FieldArray>
	);
};

export default RRates;
