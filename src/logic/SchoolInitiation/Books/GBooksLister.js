import React, { useState } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { get, destroy, post } from "config/api";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import RHeaderBook from "view/SchoolInitiation/Books/RHeaderBook";
import GAddBook from "./GAddBook";
import styles from "./GBookLister.module.scss";
import BookIframe from "components/Global/BookIframe";
import { iframeFriendlySource } from "utils/iframeSource";
import RModal from "components/Global/RComs/RModal";
import { useDispatch, useSelector } from "react-redux";
import { fillNameSchoolInitiation } from "store/actions/global/schoolInitiation.actions";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const GBooksLister = () => {
	const dispatch = useDispatch();
	const { sIType, sITypeId, tabTitle, curriculumId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchLoading, setSearchLoading] = useState(false);
	const [searchData, setSearchData] = useState("");
	const [bookModal, setBookModal] = useState(false);
	const [alert, setAlert] = useState(false);
	const [openIframe, setOpenIframe] = useState(false);
	const [editBook, setEditBook] = useState(false);

	const [addBook, setAddBook] = useState({
		title: "",
		url: "",
		curricula_id: null,
		hash_id: "",
		start_page: null,
		attachment: [],
	});
	const { user } = useSelector((state) => state.auth);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleOpenIframe = (data) => {
		setOpenIframe(true);
		setAddBook(data);
	};

	const handleOpenEditBook = (data) => {
		setEditBook(true);
		setAddBook(data);
	};

	const handleCloseEditBook = () => {
		setEditBook(false);
		setAddBook({
			title: "",
			url: "",
			curricula_id: null,
			hash_id: "",
			start_page: null,
			attachment: [],
		});
	};

	const handleOpenAddBook = () => setBookModal(true);
	const handleCloseAddBook = () => {
		setBookModal(false);
		setAddBook({
			title: "",
			url: "",
			curricula_id: null,
			hash_id: "",
			start_page: null,
			attachment: [],
		});
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const getDataFromBackend = async (specific_url) => {
		let url;

		url = `${Services.auth_organization_management.backend}api/book/getByCourse/${sITypeId ?? curriculumId}`;

		let response = await get(specific_url ? specific_url : url);

		if (response.data.status == 1) {
			dispatch(
				fillNameSchoolInitiation(
					sIType == "education_stage"
						? response.data.data?.education_stage
						: sIType == "grade_levels"
						? response.data.data?.grade_level
						: response.data.data?.course
				)
			);
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.data.map((r) => ({
			id: r?.id,
			table_name: "educationStagePrincipalsLister",
			details: [
				{
					key: tr`title`,
					value: r?.title,
				},
			],

			actions: [
				{
					name: tr`view`,
					icon: "fa fa-eye",
					onClick: () => {
						handleOpenIframe(r);
					},
				},
				{
					name: tr`edit`,
					icon: "fa fa-edit",
					onClick: () => {
						handleOpenEditBook(r);
					},
					hidden: curriculumId ? true : false,
				},
				{
					name: tr`remove`,
					icon: iconsFa6.delete,
					onClick: () => {
						handleRemove(r.id);
					},
					hidden: curriculumId ? true : false,
				},
			],
		}));

		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response?.data?.data?.data,
			},
		}),
		[processedRecords]
	);

	const handleSearch = async (emptySearch) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/book/getByCourse/${sITypeId ?? curriculumId}?filter=${
			emptySearch ?? searchData
		}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);

		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);

			toast.error(response.data.msg);
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const successDelete = async (prameters) => {
		let response;

		response = await destroy(`${Services.auth_organization_management.backend}api/book/delete/${prameters.id}`);
		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					hideAlert();
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleChangeBook = (name, value) => {
		setAddBook({ ...addBook, [name]: value });
	};

	return (
		<>
			{alert}

			<RModal
				isOpen={openIframe}
				toggle={() => setOpenIframe(!openIframe)}
				size="lg"
				className={styles["iframe-modal"]}
				body={<BookIframe source={iframeFriendlySource(addBook.url) + "#p=" + addBook.start_page} />}
			/>
			<AppModal
				show={editBook}
				parentHandleClose={handleCloseEditBook}
				header={tr`edit_book`}
				size={"lg"}
				headerSort={
					<GAddBook
						handleCloseAddBook={handleCloseEditBook}
						handleChangeBook={handleChangeBook}
						addBook={addBook}
						getDataFromBackend={getDataFromBackend}
						setTableData={setTableData}
					/>
				}
			/>

			<AppModal
				show={bookModal}
				parentHandleClose={handleCloseAddBook}
				header={tr`add_book`}
				size={"lg"}
				headerSort={
					<GAddBook
						handleCloseAddBook={handleCloseAddBook}
						handleChangeBook={handleChangeBook}
						addBook={addBook}
						getDataFromBackend={getDataFromBackend}
						setTableData={setTableData}
					/>
				}
			/>
			<RHeaderBook
				handleOpenAddBook={handleOpenAddBook}
				handleChangeSearch={handleChangeSearch}
				handleSearch={handleSearch}
				searchLoading={searchLoading}
				searchData={searchData}
				setSearchData={setSearchData}
				seeAddingABook={curriculumId || ["learner", "student"].includes(user?.type) ? false : true}
			/>
			<RAdvancedLister {...propsLiterals.listerProps} />
		</>
	);
};

export default GBooksLister;
