import { get, post } from "config/api";

export const reportAPI = {
  reports: ()=>get`api/report-content/`,
  addReport:(payload)=>post(`api/report-content/report`,payload),
  reportCategories: ()=>get(`api/report-content/categories`)
};
