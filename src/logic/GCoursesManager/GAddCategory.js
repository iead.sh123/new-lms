import React from "react";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";

const GAddCategory = ({ handleCloseAddCategory, handleAddCategoryToTree, parentId, addEditCategoryToTreeLoading, handleChange }) => {
	return (
		<React.Fragment>
			<Row>
				<Col xs={12} className="m-0 p-0">
					<Form>
						<FormGroup>
							<Input
								name="name"
								required
								type="text"
								onChange={(event) => handleChange(event.target.name, event.target.value)}
								// placeholder={parentId ? tr`sub_category_name` : tr`category_name`}
								placeholder={tr`category_name`}
								onBlur={() => handleChange("parent_id", parentId)}
							/>
						</FormGroup>
					</Form>
				</Col>
				<Col xs={12} className="m-0 p-0">
					<RFlex>
						<RButton
							text={tr`save`}
							color="primary"
							onClick={handleAddCategoryToTree}
							loading={addEditCategoryToTreeLoading}
							disabled={addEditCategoryToTreeLoading}
						/>
						<RButton text={tr`cancel`} color="link" onClick={handleCloseAddCategory} />
					</RFlex>
				</Col>
			</Row>
		</React.Fragment>
	);
};

export default GAddCategory;
