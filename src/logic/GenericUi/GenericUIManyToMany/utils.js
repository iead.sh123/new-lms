import { checkIfObject } from "utils/validator";

export const getPivotTablePayload = (
  masterTable,
  pivotTable,
  masterId,
  detailsTable
) => ({
  payload: {
    masterTable: masterTable,
    masterId: masterId,
    pivotTable: pivotTable,
    detailTable: detailsTable,
  },
});

export const oneToManyCreateDeleteRelationPayload = (
  tableName,
  masterTable,
  detaildId
) => ({
  payload: {
    tableName: tableName, //"students",
    masterTable: masterTable, //"parents",
    id: detaildId,
  },
});

export const oneToManyCreateAddRelationPayload = (
  tableName,
  masterTable,
  details,
  masterId
) => ({
  payload: {
    tableName: tableName,
    masterTable: masterTable,
    details: details.map((d) => d.detailId), //details: details
    value: masterId, //master
  },
});

export const manyToManyCreateDeleteRelationPayload = (tableName, id) => ({
  payload: {
    tableName: tableName,
    id: id,
  },
});

export const manyToManyCreateAddRelationPayload = (
  tableName,
  masterTable,
  masterId,
  detailsTable,
  details
) => {
  return {
    payload: {
      tableName: tableName,
      masterTable: masterTable,
      masterId: masterId,
      detailTable: detailsTable,
      details: details.map((det) => ({
        ...det,
        item: (() => {
          let payloadItem = {};
          let keys = Object.keys(det.item);
          for (let i of keys) {
            if (checkIfObject(det.item[i])) {
              payloadItem[i] = det.item[i].value;
              continue;
            }
            payloadItem[i] = det.item[i];
          }

          return payloadItem;
        })(),
      })),
    },
  };
};

export const getTableDataPayload = (
  tableName,
  master_table,
  master_id,
  exempted_ids = [],
  id = null,
  filters = [],
  Category
) => ({
  payload: {
    TableName: tableName,
    exempted_ids: exempted_ids,
    id: id,
    Filters: filters,
    Category: {
      table_name: null,
      row_id: null,
    },
    Relation_Filter: {
      filter_table: master_table,
      filter_row: master_id,
    },
  },
});
// "Field": "assessment_plan_id",
//                     "Title": "Assessment Plan",
//                     "Type": "bigint(20) unsigned",
//                     "Nullability": "YES",
//                     "IsDropDownList": true,
//                     "data": [
//                         {
//                             "ID": null,
//                             "name": "select...."
//                         },
//                         {

const sqlType_inputType_mapper = {
  tinyint: "checkbox",
  varchar: "text",
  int: "number",
};

export const getInput = (scheme) => {
  if (scheme.IsDropDownList) {
    return {
      type: "select",
      options: scheme.data.map((d) => ({
        label: d.name,
        value: d.ID,
      })),
    };
  }
  return {
    type: mapSqlType_inputType(scheme.Type),
    options: null,
  };
};

const mapSqlType_inputType = (sqltype) => {
  sqltype = sqltype.substring(0, sqltype.indexOf("("));
  return sqlType_inputType_mapper[sqltype];
};

export const getDefaultValue = (sqltype) => {
  switch (mapSqlType_inputType(sqltype)) {
    case "checkbox":
      return false;
    case "text":
      return "";
    case "number":
      return 0;
    default:
      return null;
  }
};
