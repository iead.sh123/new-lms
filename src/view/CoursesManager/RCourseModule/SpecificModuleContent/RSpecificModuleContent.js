import React, { useContext, useRef, useEffect, useState } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Row, Col } from "reactstrap";
import RListModuleContent from "../ListModuleContent/RListModuleContent";
import RAddCourseModule from "../AddCourseModuleForm/RAddCourseModule";
import RModuleContent from "./RModuleContent";
import RButton from "components/Global/RComs/RButton";
import styles from "../../CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RSpecificModuleContent = () => {
	const CourseModuleData = useContext(CourseModuleContext);
	const listModuleContentRef = useRef(null);

	// const handleScrollToModuleContent = () => {
	// 	const targetElement = listModuleContentRef.current;

	// 	if (targetElement) {
	// 		const yOffset = -20;
	// 		const y = targetElement.offsetTop + yOffset;

	// 		// targetElement.scrollIntoView({ top: y, behavior: "smooth" });
	// 		targetElement.scrollIntoView({ block: "center", behavior: "smooth", inline: "nearest" });
	// 	}
	// };

	return (
		<div>
			{/* start editModule  */}
			{!CourseModuleData.studentView && (
				<>
					{CourseModuleData.editModule ? (
						<Col xs={12}>
							<RAddCourseModule edit={true} />
						</Col>
					) : (
						<Row>
							<Col xs={12}>
								<RFlex>
									<h6 className="mt-1">{CourseModuleData.specificModule.title}</h6>
									<div
										className={styles.icon_div}
										onClick={CourseModuleData?.handleShowEditModuleForm}
										disabled={CourseModuleData.createModule}
									>
										<i className="fa fa-pencil mt-1" />
									</div>
								</RFlex>
							</Col>
						</Row>
					)}
				</>
			)}
			{/* end editModule  */}

			{/* start module content*/}
			{CourseModuleData.specificModule?.contents?.map((content) => (
				<RModuleContent onClick content={content} />
			))}
			{/* end module content*/}

			{/* start footer module content*/}
			{/* {!CourseModuleData.studentView && (
				<Row className="d-flex justify-content-between mt-4">
					<Col xs={4}>
						<RButton
							text={tr`add_content`}
							outline
							color={"primary"}
							onClick={() => {
								CourseModuleData.handleAddContent();
								handleScrollToModuleContent();
							}}
						/>
					</Col>
					<Col xs={4} className="d-flex justify-content-end">
						<RButton text={tr`add_teaching_elements`} outline color={"primary"} />
					</Col>
				</Row>
			)} */}
			{/* end footer module content*/}

			{/* start list module content to added*/}
			{/* {CourseModuleData.addContent && !["student", "learner"].includes(CourseModuleData.user.type) && ( */}
			{!["student", "learner"].includes(CourseModuleData.user.type) &&
				(!CourseModuleData.studentView ? (
					<Row>
						<Col xs={12}>
							<RListModuleContent ref={listModuleContentRef} content={CourseModuleData.specificModule} />
						</Col>
					</Row>
				) : (
					""
				))}
			{/* end list module content to added*/}
		</div>
	);
};

export default RSpecificModuleContent;
