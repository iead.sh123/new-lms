import React, { useEffect, useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { useHistory, useParams } from "react-router-dom";
import { post, get } from "config/api";
import { Services } from "engine/services";
import RAddEdit from "components/Global/RComs/RAddEdit";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const UserManagementAdd = (props) => {
	const { user_type, organizationUserId, tabTitle } = useParams();

	const [initialformItems, setInitialFormItems] = useState([]);
	const [tableProps, setTableProps] = useState({});
	const [alerts, setAlerts] = useState(false);
	const [record, setRecord] = useState(null);
	const [loading, setLoading] = useState(false);
	const [addLoading, setAddLoading] = useState(false);

	const TypeEnum = {
		text: "text",
		number: "number",
		time: "time",
		checkbox: "checkbox",
		select: "select",
		color: "color",
		file: "file",
		password: "password",
		email: "email",
	};

	const showAlerts = (child) => {
		setAlerts(child);
	};

	//handle options to set in select
	const getOptionsFromData = (data) => {
		const output = [];
		data.map((d) => {
			// if (d.ID) output.push({ value: d.ID, label: d.Name });
			if (d.value) output.push({ value: d.value, label: d.label });
		});

		return output;
	};

	//handleChange type:text
	const handleTextChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:number
	const handleValueChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:select
	const handleSelectValueChanged = (e, column) => {
		let temp = record;
		temp[column] = e.value;
		setRecord(temp);
	};

	//handleChange type:checkbox
	const handleChecked = (e, column) => {
		let temp = record;
		temp[column] = e.target.checked;
		setRecord({ ...temp });
	};

	//handleChange type:file
	//use this way because the RDropzone Component return callback (contain data)
	const handleFileChanged = (childData, column) => {
		if (childData) {
			let temp = record;
			temp[column] = childData;
			setRecord({ ...temp });
		}
	};

	//routing to function to set data
	const getFormItemOnChange = (fieldtype, c) => {
		if (fieldtype == TypeEnum.text) {
			return (e) => handleTextChanged(e, c);
		} else if (fieldtype == TypeEnum.select) {
			return (e) => handleSelectValueChanged(e, c);
		} else if (fieldtype == TypeEnum.number) {
			return (e) => handleValueChanged(e, c);
		} else if (fieldtype == TypeEnum.checkbox) {
			return (e) => handleChecked(e, c);
		} else {
			return (e) => handleTextChanged(e, c);
		}
	};

	//to set type in object
	const getFormItemType = (field) => {
		let type = TypeEnum.text;
		const fieldType = field?.Type;
		const fieldsText = fieldType.includes("varchar") || fieldType.includes("text");

		if (field.IsDropDownList) {
			type = TypeEnum.select;
		} else if (field?.Type == "file") {
			type = TypeEnum.file;
		} else {
			if (fieldsText && field.specific_type == "email") {
				type = TypeEnum.email;
			} else if (fieldsText && field.specific_type == "password") {
				type = TypeEnum.password;
			} else if (fieldsText && field.specific_type == "color") {
				type = TypeEnum.color;
			} else {
				type = TypeEnum.text;
			}
			if (fieldType.includes("bigint") || fieldType.includes("int") || fieldType.includes("smallint")) {
				type = TypeEnum.number;
			}
			if (fieldType.includes("time")) {
				type = TypeEnum.time;
			}
			if (fieldType.includes("tinyint")) {
				type = TypeEnum.checkbox;
			}
		}
		return type;
	};

	//call api to set record ...
	useEffect(() => {
		const getData = async () => {
			setLoading(true);
			let response;
			if (props.addExtraPropsToUser) {
				response = await get(`${Services.auth_organization_management.backend}api/user/extraProps/${tabTitle}`);
			} else {
				response = await get(
					`${Services.auth_organization_management.backend}api/organization/users/store/${organizationUserId
						? tabTitle
							? `${tabTitle}/${organizationUserId}`
							: `${user_type}/${organizationUserId}`
						: tabTitle
							? tabTitle
							: user_type
					}`
				);
			}

			if (response && response.data.status == 1 && response.data && response.data.data) {
				setLoading(false);
				let data = response.data.data;
				let init = {};

				setRecord(organizationUserId ? data.record : init);
				setTableProps(data.table_props ? data.table_props : data.extra_props);
			} else {
				toast.error(response.data.msg)
				setLoading(false);
			}
		};
		getData();
	}, [user_type]);

	//initialFormItems
	useEffect(() => {
		let form_data = [];

		if (tableProps && tableProps.length > 0) {
			tableProps &&
				tableProps?.map((tp, key) => {
					let t = getFormItemType(tp);
					let formitem = {};
					let selectOptions = getOptionsFromData(tp.data);
					let ss = selectOptions.filter((i) => i.value == record[tp.Field]);
					let selected = record ? ss[0] : null;

					if (t == TypeEnum.select) {
						formitem = {
							id: key,
							type: t,
							name: tp.Field,
							label: tp.Title,
							required: false,
							isMulti: false,
							option: selectOptions,
							onChange: getFormItemOnChange(t, tp.Field),
							// defaultValue: tp.description
							value: selected,
							defaultValue: selected,
						};
					} else if (t == TypeEnum.checkbox) {
						formitem = {
							id: key,
							type: t,
							name: tp.Field,
							label: tp.Title,
							onChange: getFormItemOnChange(t, tp.Field),
							checked: record ? record[tp.Field] : null,
						};
					} else if (t == TypeEnum.file) {
						formitem = {
							id: key,
							type: t,
							name: tp.Field,
							label: tp.Title,
							handleData: (file) => handleFileChanged(file, tp.Field),
							fileSize: 5000,
							binary: true,
							singleFile: true,

							value: record ? record[tp.Field] : null,
							theme: "round",
							// fileType: ["image/*"],
						};
					} else {
						formitem = {
							id: key,
							type: t,
							name: tp.Field,
							label: tp.Title,
							required: tp.Nullability == "YES" ? false : true,
							onChange: getFormItemOnChange(t, tp.Field),
							// defaultValue: tp.description
							defaultValue: record ? record[tp.Field] : null,
						};
					}
					form_data.push(formitem);
				});
		}

		setInitialFormItems(form_data);
	}, [record, tableProps]);

	//action
	const onAddEditSubmit = async (e) => {
		setAddLoading(true);
		e.preventDefault();

		const payload = {
			payload: props.curriculumId ? { ...record, curricula_id: props.curriculumId } : record,
		};

		const res = await post(
			`${Services.auth_organization_management.backend}api/organization/users/store/${tabTitle ? tabTitle : user_type}`,
			payload
		);

		if (res && res.data && res.data.status == 1) {
			setAddLoading(false);
			//i use this way because i using in school initiation to load data in user managment after add new user you need to pass same data in school initiation
			if (tabTitle && props.fetchDataAfterAdded) {
				const response1 = await props.getDataFromBackend();
				if (response1) {
					props.handleCloseModal && props.handleCloseModal();
					props.setTableData(response1);
				} else {
					toast.error(res.data.msg)
				}
			}
		} else {
			setAddLoading(false);
			toast.error(res.data.msg)
		}

		return false;
	};

	const handleAddExtraPropsToUSer = (e) => {
		e.preventDefault();
		props.handleAddExtraProps(record);
	};

	return (
		<>
			{loading ? (
				<Loader />
			) : (
				<>
					{alerts}

					<RAddEdit
						initialFormItems={initialformItems}
						onSubmit={(e) => {
							if (props.addExtraPropsToUser) {
								handleAddExtraPropsToUSer(e);
							} else {
								onAddEditSubmit(e);
							}
						}}
						//preSetValues => pass data when add new child =>stage1 in genericUiAddEdit

						oldObject={!(props && props.preSetValues) && organizationUserId && initialformItems}
						allowEdit={record?.has_edit_permission}
						preSetValues={props && props.preSetValues}
						addLoading={addLoading}
					/>
				</>
			)}
		</>
	);
};

export default UserManagementAdd;
