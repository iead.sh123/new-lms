import React from "react";
import styles from "./RLessonPlanViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { TransformedTags } from "utils/TransformedTags";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";

const RLessonPlanFullData = ({ lesson_plan_item }) => {
	return (
		<div className={styles.lp__viewer__full__data}>
			<div className={styles.lp__viewer__items}>
				<h6 style={{ color: primaryColor }}>{lesson_plan_item?.subject}</h6>
				<RFlex>
					<i className="fa-solid fa-clock pt-1"></i>
					<span>
						{lesson_plan_item.time} {tr`min`}
					</span>
				</RFlex>
				{lesson_plan_item?.tags && lesson_plan_item?.tags?.length > 0 && (
					<RFlex>
						<i className="fa-solid fa-tag pt-1"></i>
						<RFlex>
							<TransformedTags tags={lesson_plan_item?.tags} />
						</RFlex>
					</RFlex>
				)}
				<RFlex>
					<i className="fa-solid fa-align-left pt-1"></i>
					<span>{lesson_plan_item.subject}</span>
				</RFlex>
				<RFlex style={{ wordWrap: "break-word" }}>
					<i className="fa-solid fa-align-left pt-1"></i>
					<span
						dangerouslySetInnerHTML={{
							__html: lesson_plan_item?.description,
						}}
					/>
				</RFlex>
				{lesson_plan_item.attachments.length > 0 && (
					<>
						<RFlex>
							<i className="fa-solid fa-paperclip"></i>
							<span>{tr`attachment`} :</span>
						</RFlex>
						{lesson_plan_item?.rubric && (
							<RFlex>
								<span>{tr`rubric`} :</span>
								<span>{lesson_plan_item?.rubric}</span>
							</RFlex>
						)}
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{lesson_plan_item.attachments.map((attachment) => (
								<img
									src={Services.storage.file + attachment?.hash_id}
									alt={attachment?.id}
									key={attachment?.id}
									className={styles.lp__viewer__image}
								/>
							))}
						</RFlex>
					</>
				)}
			</div>
		</div>
	);
};

export default RLessonPlanFullData;
