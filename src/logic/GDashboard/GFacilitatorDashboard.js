import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import RTimeLine from "./RTimeLine";
import GGreetingSearch from "./GGreetingSearch";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import { fileImage } from "components/Global/RComs/RResourceViewer/RFile";
import { relativeDate } from "utils/dateUtil";
import RHeader from "components/Global/RComs/RHeader";
import GStudentSwitch from "logic/General/GStudentSwitch";
import GRecentlyAddedCourses from "./GRecentlyAddedCourses";
import GTodayLiveSessions from "./GTodayLiveSessions";

import GCoursesLine from "./GCoursesLine";
import GFacilitatorWeekEvents from "./GFacilitatorWeekEvents";
import GStudentsSubmitedTasks from "./GStudentsSubmitedTasks";
import { Services } from "engine/services";
import GLiveSessions from "./rc/GLiveSessions";
import GUnpublished from "./rc/GUnpublished";
import GWeekEvents from "./rc/GWeekEvents";
import RRLiveSessions from "./rc/RRLiveSessions";
import RBieChart from "components/Global/RComs/RBieChart";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import dashboardReducer from "store/reducers/teacher/dashboard.reducer";
import REmptyData from "components/RComponents/REmptyData";

const GFacilitatorDashboard = () => {
	const history = useHistory();
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	useEffect(() => {
		getRecentAddedRolesAsync(dispatch);
		getRecentUserTypesAsync(dispatch);
		getRecentActivitiesAsync(dispatch);
	}, []);

	const [data, setData] = useState([]);
	const [recentLessons, setRecentLessons] = useState([]);
	const [recentTasks, setRecentTasks] = useState([]);
	const [recentlyAddedContents, setRecentlyAddedContents] = useState([]);
	const [statistics, setStatistics] = useState([]);
	const [todayActivities, setTodayActivities] = useState([]);

	const [courses1, setCourses1] = useState([]);
	const [courses2, setCourses2] = useState([]);
	const [courses3, setCourses3] = useState([]);

	const { myCoursesLoading, myCourses } = useSelector((state) => state.coursesManagerRed);

	const courses = myCourses;
	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "response");
					setData(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-lessons",
				"fail to get api/dashboard/recent-lessons",
				(response) => {
					Helper.cl(response, "response");
					setRecentLessons(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-tasks",
				"fail to get api/dashboard/recent-tasks",
				(response) => {
					Helper.cl(response, "response");
					setRecentTasks(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recently-added-contents",
				"fail to get api/dashboard/recently-added-contents",
				(response) => {
					Helper.cl(response, "response");
					setRecentlyAddedContents(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/statistics",
				"fail to get api/dashboard/statistics",
				(response) => {
					Helper.cl(response, "response");
					setStatistics(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/today",
				"fail to get api/dashboard/today",
				(response) => {
					Helper.cl(response, "response");
					setTodayActivities(response.data?.data);
				},
				() => {}
			);

			//----------------------------Courses
			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isMaster: false },
				"success",
				"fail",
				(response) => {
					setCourses1(response?.data?.courses?.data);
				},
				() => {},
				false
			);

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isInstance: true },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setCourses2(response?.data?.courses?.data);
				},
				() => {},
				false
			);

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isPublished: true },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setCourses3(response?.data?.courses?.data);
				},
				() => {},
				false
			);
		};
		get_it();
	}, []);

	const [unpublishedContent, setUnpublishedContent] = useState();

	useEffect(() => {
		let unpublishedContent1 = [];

		recentLessons?.map((l) =>
			l?.todayOrTommorrowUnpublishedLessons?.map((t) =>
				t.unpublishedContents?.contents?.map((co) => {
					unpublishedContent1.push({
						icon: "fa fa-pen",
						course: l.name,
						subName: co.name,
						publishCallback: () => {
							alert("dd");
						},
					});
				})
			)
		);

		Helper.cl(unpublishedContent1, "unpublishedContent1");
		setUnpublishedContent(unpublishedContent1);
	}, [recentLessons]);

	// useEffect(() => {
	//   const unpublished = [];
	//   courses.map((c) => {
	//     return c.todayOrTommorrowUnpublishedLessons.map((e) => {
	//       unpublished.push({
	//         time: e.lesson_date,
	//         AmPm: "",
	//         cours: c.name,
	//         lessonTitle: e.subject,
	//         leftTime: relativeDate(e.nextLesson?.lesson_time),
	//       });
	//     });
	//   });
	//   setUnpublishedContent(unpublished);
	// }, [courses]);

	const items = recentTasks?.activeTasks?.map((t) => getTimeLineItemFromTask(t));

	const reddivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "red",
	};

	const greendivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "green",
	};

	const orangedivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "orange",
	};

	const smileyStyle = {
		fontSize: "12px",
		marginRight: "3px",
	};

	const textStyle = {
		fontSize: "12px",
		marginRight: "3px",
	};

	const [myCoursesCount, setMyCoursesCount] = useState(0);

	const recentlyUploadedFiles = [
		{
			icon: null,
			title: "First Event",
			description: (
				<div style={greendivStyle}>
					<div style={smileyStyle}>😊 Good answer</div>

					<div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
				</div>
			),

			image: fileImage("pdf"),
		},
		{
			icon: null,
			title: "Second Event",
			description: (
				<div style={reddivStyle}>
					<div style={smileyStyle}>😊Good answer</div>

					<div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
				</div>
			),
			image: fileImage("jpg"),
		},
		{
			icon: null,
			title: "Second Event",
			description: (
				<div style={orangedivStyle}>
					<div style={smileyStyle}>😊Good answer</div>

					<div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
				</div>
			),
			image: fileImage("mp3"),
		},
		{
			icon: null,
			title: "Second Event",
			description: (
				<div style={greendivStyle}>
					<div style={smileyStyle}>😊Good answer</div>

					<div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
				</div>
			),
			image: fileImage("mp4"),
		},
		// { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
	];
	// In your component...

	return (
		<Container>
			{/* {Helper.jstree(recentLessons)} */}
			<GGreetingSearch />
			<Row>
				<Col md={recentLessons ? 8 : 12}>
					<GMyCourses advanced={false} perView={recentLessons ? 2 : 3} />
				</Col>
				<Col md={recentLessons ? 4 : 0}>
					{/* <GTodayLiveSessions /> */}
					<RTitle text="Today's Live Sessions" />
					{recentLessons
						?.filter((e) => e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0])
						.map((e) => {
							return (
								<div>
									{/* {
Helper.js(
  e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)[0])
} */}
									{/*   ?.meeting_id */}

									{/* <RRLiveSessions
            meetingId={e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}
            userType={"teacher"}
            text="manage"
            time={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time}
            AmPm={""}
            cours={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.name}
            lessonTitle={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.subject}
            leftTime={relativeDate(e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time)}
            onClickAction={()=>{history.push(
              `${baseURL}/${genericPath}/course-management/course/${e.id}/cohort/${e.cohort_id.cohort_id}/category/${e.category_id}/lesson/${
                e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.id 
              }/meeting/${e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}`
            );}}
          /> */}
									<RRLiveSessions
										meetingId={
											e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents
												?.filter((c) => c?.type == "live_session")
												.map((c) => c.live_session)?.[0]?.meeting_id
										}
										userType={"teacher"}
										text="manage"
										time={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time}
										AmPm={""}
										cours={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.name}
										lessonTitle={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.subject}
										leftTime={relativeDate(e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time)}
										onClickAction={() => {
											history.push(
												`${baseURL}/${genericPath}/course-management/course/${e.id}/cohort/${e.cohort_id.cohort_id}/category/${
													e.category_id
												}/lesson/${e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.id}/meeting/${
													e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents
														?.filter((c) => c?.type == "live_session")
														.map((c) => c.live_session)?.[0]?.meeting_id
												}`
											);
										}}
									/>
								</div>
							);
						})}
				</Col>
			</Row>

			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					<div style={{ height: "100%" }}>
						Student submited Tasks
						{data?.whatWeHaveForToday?.ungradedQuestionSets ? (
							<GStudentsSubmitedTasks tasks={data?.whatWeHaveForToday?.ungradedQuestionSets} />
						) : (
							<REmptyData />
						)}
					</div>
				</Col>
				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<RTitle text="Days Students are most Active at" />
						<RBieChart
							data={[
								{ text: "su", count: 1 },
								{ text: "mon", count: 2 },
								{ text: "tu", count: 3 },
								{ text: "th", count: 4 },
								{ text: "fr", count: 3 },
							]}
							color={"#007bff"}
							label={"Activity"}
						/>
					</div>
				</Col>
			</Row>

			<Row className="mt-4">
				<GStatisticsRow data={statistics.statistics} />
			</Row>
			<Row className="mt-4">
				{/* {Helper.jstree(courses?.[0]?.todayOrTommorrowUnpublishedLessons,"courses todayOrTommorrowUnpublishedLessons")}

    id:520
:
lesson_date:2023-10-31 00:00:00



module_id:177
lesson_plan_id:
creator_id:1
created_at:2023-10-24T11:51:01.000000Z
updated_at:2023-10-24T11:51:01.000000Z */}
				<Col xs={12} md={6}>
					<RTitle text="Unpublished Content" />
					<GUnpublished data={unpublishedContent} />
					{/* <courses advanced={false}/> */}
				</Col>
			</Row>
			<Row>
				{" "}
				<GStatisticsRow data={data?.statistics} />{" "}
			</Row>

			<Row>
				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					<GWeekEvents />
				</Col>

				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					<GCalender advanced={false} />
				</Col>
			</Row>

			{/* <Col xs="6" md="5">
    <RHeader title="Tasks" count={"Current Tasks"}/>
    <RTimeLine items={items} color="#668AD7" />
  </Col> */}

			<Row>
				<RTitle text="Providers" />
				<GCollaborationAreaProviders />
			</Row>
			<Row>
				<RTitle text="Clients" />
				<GCollaborationAreaClients />
			</Row>
			<Row>
				<RTitle text="consumed contents" />
				<GCollaborationAreaConsumedContents />
			</Row>
			{/* Users and roles */}
			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="6">
					{/* Table placeholder (75% height) */}

					<RFlex>
						<h6>{tr`Recently added users`}</h6>
					</RFlex>
					<RecentAddedUser />

					{/* Swiper of cards placeholder (25% height) */}
				</Col>
				<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
					<h6 className="mb-3">{tr`Recent Added Roles`}</h6>
					<RFlex styleProps={{ flexWrap: "wrap" }}>
						{usersAndPermissionsData.recentAddedRolesLoading ? (
							<Loader />
						) : (
							usersAndPermissionsData.recentAddedRoles.map((role) => <RRole role={role} lister={true} />)
						)}
					</RFlex>
				</Col>
			</Row>

			<Row>
				{/* Second column */}
				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div></div>
				</Col>
			</Row>

			<Row className="mt-4" style={{ width: "100%" }}>
				{/* <div style={{ display: myCoursesCount == 0 ? "hidden" : "block" }}> */}
				{/* <RHeader
            title="My Courses"
            count={myCoursesCount}
            link="/g/my-courses"
            linkTitle="See All"
          ></RHeader> */}

				<RHeader count={courses1?.length} text="Cloned Courses" />
				<GCoursesLine courses={courses1} />
				<RHeader count={courses2?.length} text="Newly Instantiated Courses" />
				<GCoursesLine courses={courses2} />
				<RHeader count={courses3?.length} text="Newly published Courses" />
				<GCoursesLine courses={courses3} />

				{/* </div> */}
			</Row>
		</Container>
	);
};
export default GFacilitatorDashboard;
