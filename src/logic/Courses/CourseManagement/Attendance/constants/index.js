import tr from "components/Global/RComs/RTranslator"
import * as colors from '../../../../../config/constants'
export const tablesTypes = {
    todayDetails: "todayDetails",
    overallDetails: "overallDetails",
    studentDetails: "studentDetails",
    moduleContent: 'moduleContent',
    attendanceSettings: 'attendanceSettings',
    teacherCourses: "teacherCourses",
}
export const attendanceTypes = [
    { symbol: 'P', label: 'Presented', value: 'p' },
    { symbol: 'L', label: 'Late', value: 'l' },
    { symbol: 'LE', label: 'Late With Excuse', value: 'le' },
    { symbol: 'A', label: 'Absent', value: 'a' },
    { symbol: 'AE', label: 'Absent With Excuse', value: 'ae' }
]
export const settingsTable = [
    { label: "Attendance Status", value: 'title', type: 'text' },
    { label: "Acronym", value: 'acronym', type: 'text' },
    { label: "Points", value: 'points', type: 'number' },
]
export const tabs = [
    { title: "My_Template" },
    { title: "School_Template" },
]
export const options = {
    allCourses: "allCourses",
    thisCourse: "thisCourse",
    specificCourse: "specificCourse"
}
export const studentHeaders = [
    { label: tr("Required_To_Attendance"), value: "module_content", type: "text", color: colors.boldGreyColor },
    { label: tr("Time"), value: "date", type: 'text', color: colors.greyColor },
    { label: tr("Attendance"), value: "attendance", type: "select", color: colors.primaryColor },
    { label: tr("Points"), value: "points", type: 'text', color: colors.warningColor },
]