import React from "react";
import RSwiper from "../RSwiper/RSwiper";
import RCard from "./RCard";
import { SwiperSlide } from "swiper/react";

const RCardSlider = () => {
  return (
    <RSwiper>
      <SwiperSlide>
        <RCard />
      </SwiperSlide>
      <SwiperSlide>
        <RCard />
      </SwiperSlide>
    </RSwiper>
  );
};

export default RCardSlider;
