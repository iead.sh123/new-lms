import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { paymentApi } from "api/global/payment";
import { useFormik } from "formik";
import RPaymentPolicy from "view/Organization/PaymentPolicy/RPaymentPolicy";
import GAddDiscount from "./Discount/GAddDiscount";
import GAddCoupon from "./Coupon/GAddCoupon";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";

export const PaymentPolicyContext = React.createContext();
const GPaymentPolicy = () => {
	const history = useHistory();
	const location = useLocation();
	const pathName = location.pathname.split("/");
	const tabType = pathName[pathName.length - 1];

	const [openCoupon, setOpenCoupon] = useState(false);
	const [openDiscount, setOpenDiscount] = useState(false);

	const initialValues = {
		selectItems: [
			{ label: tr`discounts`, value: "discounts" },
			{ label: tr`coupons`, value: "coupons" },
		],
		timeItems: [
			{ label: tr`more_than_24_hours`, value: 1 },
			{ label: tr`less_than_24_hours`, value: 2 },
		],
		assignedToItems: [
			{ label: tr`all`, value: "All" },
			{ label: tr`not_assigned_yet`, value: "Normal" },
			{ label: tr`the_whole_organization`, value: "General" },
		],
		search: "",
		itemNameValue: "coupons",
		timeItemValue: null,
		assignedToItemValue: null,
		groupingOfSearchFields: "",
		showFilter: false,
		switchMode: true,
		couponIdSelected: null,
		discountIdSelected: null,
		couponOrDiscountDetails: null,
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	// --------------- Start UseQueries ---------------
	const couponData = useFetchDataRQ({
		queryKey: tabType == "archive" ? ["couponsArchiveList", values.groupingOfSearchFields] : ["couponsList", values.groupingOfSearchFields],
		queryFn: () =>
			tabType == "archive"
				? paymentApi.couponsArchiveList(values.groupingOfSearchFields)
				: paymentApi.couponsList(values.groupingOfSearchFields),
		enableCondition: values.itemNameValue == "coupons",
	});

	const discountData = useFetchDataRQ({
		queryKey:
			tabType == "archive" ? ["discountsArchiveList", values.groupingOfSearchFields] : ["discountsList", values.groupingOfSearchFields],

		queryFn: () =>
			tabType == "archive"
				? paymentApi.discountsArchiveList(values.groupingOfSearchFields)
				: paymentApi.discountsList(values.groupingOfSearchFields),
		enableCondition: values.itemNameValue == "discounts",
	});

	const checkGeneralDiscountData = useFetchDataRQ({
		queryKey: ["checkGeneralDiscount"],
		queryFn: () => paymentApi.checkGeneralDiscount({}),
	});

	const addDiscount = useMutateData({
		queryFn: ({ type, data }) => paymentApi.addDiscount({ type, data }),
		invalidateKeys: ["discountsList"],
		onSuccessFn: () => {
			setOpenDiscount(false);
		},
	});

	const addCoupon = useMutateData({
		queryFn: ({ data }) => paymentApi.addCoupon({ data }),
		invalidateKeys: ["couponsList"],
		onSuccessFn: () => {
			setOpenCoupon(false);
		},
	});
	// --------------- End UseQueries ---------------

	// --------------- Actions ---------------

	const handleSelectCouponId = (id, item) => {
		setFieldValue("couponIdSelected", id);
		setFieldValue("couponOrDiscountDetails", item);
	};

	const handleSelectDiscountId = (id, item) => {
		setFieldValue("discountIdSelected", id);
		setFieldValue("couponOrDiscountDetails", item);
	};

	const handlePushToAnotherRoute = (url) => {
		history.push(`${baseURL}/${genericPath}/${url}`);
	};

	const handleChangeSearch = (text) => {
		setFieldValue("search", text);
	};

	const handleSelectItemName = (value) => {
		setFieldValue("itemNameValue", value);
		// To Empty State
		setFieldValue("switchMode", true);
		setFieldValue("search", "");
		setFieldValue("assignedToItemValue", null);
		setFieldValue("groupingOfSearchFields", "");
		setFieldValue("timeItemValue", null);
		setFieldValue("showFilter", false);
		setFieldValue("couponIdSelected", null);
		setFieldValue("discountIdSelected", null);
		setFieldValue("couponOrDiscountDetails", null);
	};

	const handleEmptyRightSideAfterDeActivate = (value) => {
		// To Remove RightSide Open
		setFieldValue("couponIdSelected", null);
		setFieldValue("discountIdSelected", null);
		// To Remove HeightLight
		setFieldValue("couponIdSelected", null);
		setFieldValue("discountIdSelected", null);
	};

	const handleSelectTime = (check) => {
		setFieldValue("timeItemValue", check);
	};

	const handleSelectAssignedToItem = (check) => {
		setFieldValue("assignedToItemValue", check);
	};

	const handleSearch = (emptyData, emptyFilter) => {
		const { search, timeItemValue, assignedToItemValue } = values;
		let queryParams = "";
		if (search) {
			queryParams += `?search=${emptyData !== undefined ? "" : search}`;
		}
		if (timeItemValue && timeItemValue.value) {
			queryParams += `${queryParams ? `${emptyFilter !== undefined ? "" : "&"}` : `${emptyFilter !== undefined ? "" : "?"}`}${
				emptyFilter !== undefined ? "" : `leftTime=${timeItemValue.value}`
			}`;
		}
		if (assignedToItemValue && assignedToItemValue.value) {
			queryParams += `${queryParams ? `${emptyFilter !== undefined ? "" : "&"}` : `${emptyFilter !== undefined ? "" : "?"}`} ${
				emptyFilter !== undefined ? "" : `isFull=${assignedToItemValue.value}`
			}`;
		}
		setFieldValue("groupingOfSearchFields", queryParams ? `${queryParams}` : "");
	};

	const handleShowFilter = () => {
		setFieldValue("showFilter", !values.showFilter);
	};

	const handleSwitchMode = () => {
		setFieldValue("switchMode", !values.switchMode);
	};

	const handleResetFilter = () => {
		if (values.groupingOfSearchFields == "coupons") {
			setFieldValue("timeItemValue", null);
			handleSearch(undefined, "");
			setFieldValue("showFilter", false);
		} else {
			setFieldValue("timeItemValue", null);
			setFieldValue("assignedToItemValue", null);
			handleSearch(undefined, "");
			setFieldValue("showFilter", false);
		}
	};

	const handleAddDiscount = (type, data) => {
		addDiscount.mutate({ type: type ? "General" : "Normal", data: data });
	};

	const handleAddCoupon = (data) => {
		addCoupon.mutate({ data: data });
	};

	// --------------- Actions Modals ---------------

	const handleOpenCouponModal = () => {
		setOpenCoupon(true);
	};
	const handleCloseCouponModal = () => {
		setOpenCoupon(false);
	};

	const handleOpenDiscountModal = () => {
		setOpenDiscount(true);
	};
	const handleCloseDiscountModal = () => {
		setOpenDiscount(false);
	};

	return (
		<PaymentPolicyContext.Provider
			value={{
				values,
				tabType,
				couponData,
				discountData,
				generalDiscountData: checkGeneralDiscountData?.data?.data?.data,

				setFieldValue,

				handlePushToAnotherRoute,
				handleChangeSearch,
				handleSearch,
				handleShowFilter,
				handleSelectItemName,
				handleSelectTime,
				handleSelectAssignedToItem,
				handleSwitchMode,
				handleAddDiscount,
				handleAddCoupon,
				handleResetFilter,
				handleSelectCouponId,
				handleSelectDiscountId,
				handleEmptyRightSideAfterDeActivate,

				handleOpenCouponModal,
				handleOpenDiscountModal,
				handleCloseCouponModal,
				handleCloseDiscountModal,

				checkIfIHaveGeneralDiscount: checkGeneralDiscountData?.data?.data?.data !== null ? true : false,
				couponLoading: couponData.isLoading && couponData.fetchStatus != "idle",
				discountLoading: discountData.isLoading && discountData.fetchStatus != "idle",
				addDiscountLoading: addDiscount.isLoading,
				addCouponLoading: addCoupon.isLoading,
			}}
		>
			<AppModal
				size={"md"}
				show={openCoupon}
				parentHandleClose={handleCloseCouponModal}
				headerSort={<GAddCoupon handleCloseCouponModal={handleCloseCouponModal} />}
			/>
			<AppModal
				size={"md"}
				show={openDiscount}
				parentHandleClose={handleCloseDiscountModal}
				headerSort={<GAddDiscount handleCloseDiscountModal={handleCloseDiscountModal} />}
			/>
			<RPaymentPolicy />
		</PaymentPolicyContext.Provider>
	);
};

export default GPaymentPolicy;
