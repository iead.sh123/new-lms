import React from "react";
import styles from "../QuestionViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Label } from "reactstrap";
import { Services } from "engine/services";

const ViewFillInBlank = ({ question, isGroup }) => {
  const QUESTION = (
    <h6>
      {question?.text
        ?.replace(/<[^>]*>/g, "")
        ?.split(/{{(.*?)}}/g)
        .map((part, index) => {
          if (index % 2 === 0) {
            return <span key={index}>{part}</span>;
          } else {
            return (
              <span key={index} className={styles.fill__in}>
                {part}
              </span>
            );
          }
        })}
    </h6>
  );
  return (
    <Row className="mt-4 d-flex align-items-center">
      {!isGroup && (
        <Col xs={12} className="p-0">
          <RFlex>
            <i className="fa-solid fa-pen-to-square pt-1" />
            <Label className="p-0 m-0">{tr`draw`}</Label>
          </RFlex>
        </Col>
      )}
      <Col xs={12} className="p-0 pt-3">
        {isGroup ? (
          <ul>
            <li>{QUESTION}</li>
          </ul>
        ) : (
          QUESTION
        )}
        {question?.files[0]?.hash_id !== undefined && (
          <img
            src={Services.storage.file + question?.files[0]?.hash_id}
            alt={question?.id}
            className={styles.image}
          />
        )}
      </Col>
    </Row>
  );
};

export default ViewFillInBlank;
