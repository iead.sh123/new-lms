import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useContext, useState } from 'react'
import RNavIcon from '../../../../components/Global/RComs/RNavIcon/RNavIcon'
import RProfileName from '../../../../components/Global/RComs/RProfileName/RProfileName'
import RTextIcon from '../../../../components/Global/RComs/RTextIcon/RTextIcon'
import RLister from 'components/Global/RComs/RLister'
import { FormGroup, Input } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import RSelect from 'components/Global/RComs/RSelect'
import { AttendanceContext } from './GAttendanceDetailsLister'
import iconsFa6 from 'variables/iconsFa6'
import { baseURL, genericPath } from 'engine/config'
import Loader from 'utils/Loader'
import RButton from 'components/Global/RComs/RButton'
import { studentHeaders } from './constants'
import RHoverSelect from '../Grading/RHoverSelect'
import moment from 'moment'
import styles from './attendance.module.scss'
//{justIcon:true, faicon: 'fa-regular fa-pen-to-square',actionIconClass:styles.EditIcon, onClick: () => { } }
const GStudentAttendanceDetails = () => {
  const attendanceContext = useContext(AttendanceContext)
  console.log("attendanceContext", attendanceContext)
  if (attendanceContext.isLoadingTodayDetails || attendanceContext.isLoadingStudentDetails) {
    return <Loader />
  }
  const customAttendanceOptions = ({ innerProps, label, data }) => {
    console.log("here",)

    return (
      <RFlex key={data.value} className={`p-1 cursor-pointer `} {...innerProps}>
        <span className='p-0 m-0 text-primary'>{label}</span>
        <span className='p-0 m-0 text-warning'>({data.points})</span>
      </RFlex>
    );
  };
  const currentIndexId = attendanceContext.studentDetails?.users?.indexOf(attendanceContext.studentId)
  let prevId = currentIndexId - 1 < 0 ? null : attendanceContext.studentDetails?.users?.[currentIndexId - 1]
  let nextId = currentIndexId + 1 < attendanceContext.studentDetails?.users?.length ? attendanceContext.studentDetails?.users?.[currentIndexId + 1] : null
  const attendanceTypes = attendanceContext?.studentDetails?.attendance?.map((item) => ({
    label: item.title, points: item.points, value: item.id
  }))
  const studentDetails = attendanceContext?.studentDetails?.module_contents?.map((content, index) => {

    return {
      id: content.content_id,
      table_name: 'GStudentAttendanceDetails',
      details: studentHeaders.map((header) => {
        const AttendanceComponent = <div
          style={{ width: 'fit-content', position: 'relative' }}>
          <RHoverSelect
            inputWidth='160px'
            item={content}
            extraInfo={{ id: content.content_id, inputLabel: header }}
            handleSelectItem={attendanceContext.handleSelectAttendanceType}
            handleEditFunctionality={attendanceContext.handleEditSelectInput}
            options={attendanceTypes}
            value={attendanceContext?.attendanceStates?.attendanceTypesInput?.[content.content_id]?.value ?? { label: content?.attendance, value: null }}
            saved={attendanceContext.attendanceStates.attendanceTypesInput[content.content_id]?.saved ?? true}
            customOption={customAttendanceOptions}
          />
        </div>
        const time = moment(content.date).format("h:mm A")
        return {
          key: header.label,
          value: header.value == "attendance" ? AttendanceComponent :
            header.value == 'date' ? time :
              header.value == 'points' ? <RFlex className="justify-content-end" styleProps={{ width: "40px" }}>{content?.points}</RFlex> :
                <RFlex styleProps={{ minWidth: '150px' }}><span className='p-0 m-0'>{content[header.value]}</span></RFlex>,
          color: header.color,
          type: 'component'
        }
      })
    }
  })

  return (
    <RFlex className="d-flex flex-column" styleProps={{ gap: '15px', marginTop: '0px' }}>
      <RFlex className="justify-content-between" styleProps={{ width: 'fit-content' }}>
        <RButton
          text={tr("Back_To_All_Students")}
          faicon={iconsFa6.arrowLeft}
          color='link'
          className="text-primary p-0 m-0"
          onClick={() => { attendanceContext.history.replace(`${baseURL}/${genericPath}/course-management/course/${attendanceContext.courseId}/cohort/${attendanceContext.cohortId}/attendance`) }}
          style={{ fontSize: '14px', width: 'fit-content' }} />
      </RFlex>
      <RFlex className='justify-content-between align-items-center' >
        <RProfileName
          name={attendanceContext?.studentDetails?.name}
          img={attendanceContext?.studentDetails?.image}
          imgStyle={{ width: '40px', height: '40px' }}
        />
        <RFlex styleProps={{ width: "fit-content" }}>
          <RButton
            text={tr("Previous_Student")}
            faicon={iconsFa6.arrowLeft}
            color='link'
            className="text-primary p-0 m-0"
            disabled={prevId ? false : true}
            onClick={() => { attendanceContext.history.replace(`${baseURL}/${genericPath}/course-management/course/${attendanceContext.courseId}/cohort/${attendanceContext.cohortId}/attendance/student/${prevId}`) }}
            style={{ fontSize: '14px', width: 'fit-content' }} />
          <RButton
            text={tr("Next_Student")}
            faicon={iconsFa6.arrowRight}
            color='link'
            iconRight
            className="text-primary p-0 m-0"
            disabled={nextId ? false : true}
            onClick={() => { attendanceContext.history.replace(`${baseURL}/${genericPath}/course-management/course/${attendanceContext.courseId}/cohort/${attendanceContext.cohortId}/attendance/student/${nextId}`) }}
            style={{ fontSize: '14px', width: 'fit-content' }} />
        </RFlex>
        {/* <RTextIcon
          flexStyle={{ width: 'max-content', height: '30px', gap: '12px' }}
          text='Presented 150/150 Days'
          textStyle={{ color: '#46C37E', fontSize: '16px' }}
          icon='fa-solid fa-check'
          iconStyle={{ width: '8px', height: '8px', color: '#46C37E', marginBottom: '2px' }} /> */}
      </RFlex>
      <Input
        name="dueDate"
        type="date"
        placeholder={tr`due_date`}
        onChange={attendanceContext.handleDateChange}
        value={attendanceContext.frontDate}
        style={{ height: "40px", width: "180px" }}
      />

      <RLister
        Records={studentDetails}
        activeScroll={attendanceContext?.studentDetails?.module_contents?.length > 10 ? true : false}
        center={false}
        disableXScroll={true} />
    </RFlex>
  )
}

export default GStudentAttendanceDetails