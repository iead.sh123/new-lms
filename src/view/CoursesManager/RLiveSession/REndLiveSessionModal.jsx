import React, { useContext } from "react";
import { LiveSessionContext } from "logic/GCoursesManager/GLiveSession/GLiveSession";
import { Row, Col } from "reactstrap";
import Cool_Kids_Working from "assets/img/new/Cool_Kids_Working.png";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory } from "react-router-dom";

const REndLiveSessionModal = () => {
  const LiveSessionData = useContext(LiveSessionContext);
  const history = useHistory();

  return (
    <Row>
      <Col
        md={12}
        lg={7}
        className="d-flex justify-content-between flex-column"
      >
        <p className="text-bold">{tr`the_session_was_saved`}</p>

        <p>
          {tr`you can see all recorded session in`}&nbsp;
          <span style={{ textDecoration: "underline" }}>{tr`recordings`}</span>
        </p>
        <p>{tr`or start a new session`}</p>
        <RFlex>
          <RButton
            onClick={() => LiveSessionData.handleStartScheduleLiveSession()}
            text={tr`start_new_session`}
            color="primary"
            outline
            loading={LiveSessionData.startLiveSessionLoading}
            disabled={LiveSessionData.startLiveSessionLoading}
          />
          <RButton
            onClick={() => {
              LiveSessionData.handleCloseModal();
              history.goBack();
            }}
            text={tr`got_it`}
            color="primary"
            outline
          />
        </RFlex>
      </Col>
      <Col md={12} lg={5}>
        <img src={Cool_Kids_Working} width={"298px"} height={"272px"} />
      </Col>
    </Row>
  );
};

export default REndLiveSessionModal;
