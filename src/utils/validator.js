////------------------validation roles
export const REQUIRED = "REQUIRED";
export const MINLENGTH = "MINLENGTH";
export const MAXLENGTH = "MAXLENGTH";
export const ALPHANUMERIC = "ALPHANUMERIC";
export const REAL = "REAL";
export const UNSIGNED = "UNSIGNED";
export const INTEGER = "INTEGER";
export const NUMBER = "NUMBER";
export const FILE = "FILE";
export const ARRAY = "ARRAY";
export const MIN = "MIN";
export const MAX = "MAX";
export const REQUIRED_WITH = "REQUIRED_WITH";
export const UNIQUEINARRAY = "UNIQUEINARRAY";

//nested validation roles
export const AND = "AND";
export const OR = "OR";
export const NOT = "NOT";
export const NAND = "NAND";
export const NOR = "NOR";
export const XOR = "XOR";

const validationFunctions = {
  [REQUIRED]: (value, validation) => {
    if (!value && validation == true) {
      return false;
    }
    return true;
  },

  [MINLENGTH]: (value, validation) => {
    if (value?.trim().length < +validation) {
      return false;
    }
    return true;
  },

  [MAXLENGTH]: (value, validation) => {
    if (value?.trim().length > +validation) {
      return false;
    }
    return true;
  },

  [ALPHANUMERIC]: (value, validation) => {
    const reg = /([a-z]|[A-Z]|[0-9])+/;
    return reg.test(value);
  },

  [REAL]: (value, validation) => {
    if (Number(value) !== value && value % 1 !== 0) {
      if (value < validation.MIN || value > validation.MAX) return false;
      return false;
    }
    return true;
  },

  [NUMBER]: (value, validation) => {
    if (isNaN(value) || value === "") {
      return false;
    }
    if (value < validation.MIN || value > validation.MAX) {
      return false;
    }
    return true;
  },

  [UNSIGNED]: (value, validation) => {
    return +value >= 0;
  },

  [INTEGER]: (value, validation) => {
    return Number(value) === value && value % 1 === 0;
  },

  [FILE]: (value, validation) => {
    return true;
  },

  [ARRAY]: (value, validation) => {
    if (Array.isArray(value)) {
      if (
        value.length < +validation[MINLENGTH] ||
        value.length > +validation[MAXLENGTH]
      ) {
        return false;
      }

      return true;
    }
    return false;
  },
  [UNIQUEINARRAY]: (value, arr) => {
    return arr.findIndex(value) < 0;
  },
};

/**
 * Returns a validator function that related to a specific role.
 * @author eng-Mostafa jomaa
 * @param {string} validationRole .
 * @return {function validator(validationRole) {}}
 }} role validator function.
 */

export const evalLogicalOperation = (op = AND, bool1, bool2) => {
  switch (op) {
    case AND:
      return bool1 && bool2;

    case OR:
      return bool1 || bool2;

    case NOR:
      return !(bool1 || bool2);

    case XOR:
      return bool1 !== bool2;

    case NAND:
      return !(bool1 && bool2);
    default:
      throw new Error("evaluation faild unknow operation");
  }
};

export const getValidationFunctions = (validationRole) => {
  return validationFunctions[validationRole];
};

export const getValidValue = (object, caption = "") => {
  if (checkIfObject(object)) {
    if (!object.valid)
      throw new Error(`trying to submit invalid values ${caption}`);

    return object.value;
  }

  return object;
};

export const checkIfObject = (object) => {
  if (typeof object === "object" && !Array.isArray(object) && object !== null) {
    return true;
  }
  return false;
};
