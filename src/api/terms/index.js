import { patch } from "config/api";
import { put } from "config/api";
import { get, post, destroy } from "config/api";
import { Services } from "engine/services";
import { gradeLevelsByIdAsync } from "store/actions/global/schoolInitiation.actions";

export const termsApi = {
    academicYears: () => get(`${Services.terms.backend}academic-years`),


    saveAcademicYear: (data) =>
        post(`${Services.terms.backend}academic-years`, data),

    updateAcademicYear: (id, data) => patch(`${Services.terms.backend}academic-years/${id}`, data),

    terms: (academicYearId) => get(`${Services.terms.backend}terms/academic-years/${academicYearId}`),


    saveTerm: (academicYearId, data) => post(`${Services.terms.backend}terms/academic-years/${academicYearId}`, data),
    updateTerm: (termId, data) => patch(`${Services.terms.backend}terms/${termId}`, data),

    activateTerm: termId => post(`${Services.terms.backend}terms/activate/${termId}`),

    deactivateTerm: termId => post(`${Services.terms.backend}terms/deactivate/${termId}`),
    finalizeTerm: termId => post(`${Services.terms.backend}terms/finalize/${termId}`),

    gradeLevels: (termId) => get(`${Services.terms.backend}terms/${termId}/manage/grade-levels`),
    homerooms: (termId, gradeLevelId) => get(`${Services.terms.backend}terms/${termId}/home-rooms${gradeLevelId ? `?gradeLevelId=${gradeLevelId}` : ''}`),
    createHomeroom: (termId, data) => post(`${Services.terms.backend}home-rooms/term/${termId}`, data),

    curricula: (termId, gradeLevelId) => get(`${Services.terms.backend}terms/${termId}/grade-levels/${gradeLevelId}/curricula`),
    weeklyScheduleElements: (termId, gradeLevelId) => get(`${Services.terms.backend}weekly-schedule-elements/term/${termId}/grade-level/${gradeLevelId}`),
    addWeeklyScheduleElement: (gradeLevelId, termId, payload) => post(`${Services.terms.backend}weekly-schedule-elements/grade-level/${gradeLevelId}/term/${termId}`, payload),
    updateWeeklyScheduleElement: (gradeLevelId, termId, payload) => patch(`${Services.terms.backend}weekly-schedule-elements/grade-level/${gradeLevelId}/term/${termId}`, payload),
    deleteWeeklyScheduleElement: (id) => destroy(`${Services.terms.backend}weekly-schedule-elements/${id}`),
    availableWeeklyScheduleItems: (gradeLevelId, termId) => get(`${Services.terms.backend}weekly-schedule-elements/available-editor-elements/grade-level/${gradeLevelId}/term/${termId}`),
    createWeeklyOtherScheduleItemType: (payload) => post(`${Services.terms.backend}weekly-schedules`, payload),
    updateWeeklyScheduleOtherItemType: (id, payload) => patch(`${Services.terms.backend}weekly-schedules/${id}`, payload),

    deleteWeeklyScheduleOtherItemType: (id) => destroy(`${Services.terms.backend}weekly-schedules/${id}`),

    organizationStudents: (termId , gradeLevels) => get(`${Services.courses_manager.backend}api/users/terms/${termId}/students-with-enrolled-classes${gradeLevels?.length ? `?${gradeLevels.map((gd, ind) => `${ind > 0 ? '&' : ''}gradeLevelIds[]=${gd}`)}` : ''}`),
    organizationTeachers: (termId , searchText) => get(`${Services.courses_manager.backend}api/users/terms/${termId}/teachers-with-enrolled-classes${searchText ? `?filter=${searchText}` : ''}`),
    
    getHomeRoomById: id => get(`${Services.terms.backend}home-rooms/${id}`),
    enrollUsersInHomeRoom: (homeRoomId , payload) => put(`${Services.terms.backend}home-rooms/${homeRoomId}/users`,payload),
    deleteHomeRoomUsers: (homeRoomId , payload) => post(`${Services.terms.backend}home-rooms/${homeRoomId}/un-enroll/users` , payload),
    getCourseById : id=>get(`${Services.courses_manager.backend}api/courses/${id}`),
    

    enrollUsersInCourse: (id , payload) => put(`${Services.courses_manager.backend}api/courses/${id}/learners-and-teachers`,payload),
    deleteCourseUsers: (id , payload) => post(`${Services.courses_manager.backend}api/courses/${id}/users/delete-enrollment`,payload),
    createHomeRoom: (termId , payload)=>post(`${Services.terms.backend}home-rooms/term/${termId}`,payload),
    createCourse: (payload)=>post(`${Services.courses_manager.backend}api/courses`,payload)
};
