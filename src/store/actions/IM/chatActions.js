import Helper from "components/Global/RComs/Helper";
import { MARK_CHATS_AS_READ, SET_CHAT } from "../global/globalTypes";
import { SEE_MORE } from "../global/globalTypes";
import { SET_USER_CHATS } from "../global/globalTypes";
import { ADD_USER_CHAT } from "../global/globalTypes";
import { UPDATE_USER_CHAT } from "../global/globalTypes";

import { SET_ACTIVE_CHAT } from "../global/globalTypes";
import { ADD_TO_CHAT } from "../global/globalTypes";
import { get } from "config/api";
import { IMApi } from "api/IM";

//C:\wamp64\www\ylf\src\store\actions\IM\chatActions.js
export const setChat = (tableName, rowID) => {
  return (dispatch, getState) => {
    dispatch({
      type: SET_CHAT,
      TableName: tableName,
      RowID: rowID,
      shown: true,
    });
  };
};

export const openNotes = async (tableName, rowID) => {
  const response = await get(
    "chats/getNoteHistoryAccordingItem?table_name=" +
      tableName +
      "&row_id=" +
      rowID
  );
  if (response) {
    if (response.data && response.data.status) {
      {
        /*  {status: 1, code: 200, msg: "nodata.note_histories", data: {models: []}, other: 0, other1: null} */
        const modelscount = response.data.data.models.length;
        //

        if (
          !response.data.data ||
          !response.data.data.models ||
          modelscount == 0
        ) {
          return 5;
        } else {
          return 4;
        }
      }
    }
    return 5;
    /*  {status: 1, code: 200, msg: "nodata.note_histories", data: {models: []}, other: 0, other1: null} */
  }
  // dispatch({type:OPEN_NOTES,table:"question-sets",row:})}}

  // return (dispatch, getState) => {
  //     dispatch({type: SET_CHAT,
  //         TableName:tableName,
  //         RowID:rowID,
  //         shown:true
  //     })
  // }
};

export const markChatsAsSeen = () => async (dispatch) => {
  try {
    const response = await IMApi.markChatsAsSeen();
    if (!response.data.status) throw new Error(response.data.msg);
    return dispatch({ type: MARK_CHATS_AS_READ });
  } catch (err) {
    return;
  }
};
