import React, { useContext } from 'react'
import { GradingContext } from './GGradingManagement'
import Loader from 'utils/Loader'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { baseURL, genericPath } from 'engine/config'
import RLister from 'components/Global/RComs/RLister'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import iconsFa6 from 'variables/iconsFa6'
import tr from 'components/Global/RComs/RTranslator'
import { tablesTypes } from './Constants'

const RViewAlternatives = ({ item }) => {
  const gradingData = useContext(GradingContext)
  console.log("gradingData.isDeletingAlternative", gradingData.isDeletingAlternative)
  if (gradingData.isLoadingAlternatives)
    return <Loader />
  console.log("FirstTime", gradingData.gradingState)
  const alternatives = gradingData.gradingState.itemAlternatives
  const _alternativesData = alternatives?.ids?.map((id) => {
    const record = alternatives?.[id]
    console.log("currentRecord", record)
    return {
      id: id,
      table_name: "termData",
      details: Object.keys(record)?.filter((header) => !record[header].hidden).map((header) => {
        const gradedStudentComponent = <RTextIcon
          ///${currentRecord['Graded Students'].value.numberOFStudents}
          onClick={() => {
            if (id > 0) {
              gradingData.history.push(`${baseURL}/${genericPath}/course-management/course/${gradingData.courseId}/cohort/${gradingData.cohortId}/gradebook/type/${item.type}/${item.id}/alternatives/${record["Type"].value}/${id}`)
              return
            }
            return
          }}
          flexStyle={{ cursor: 'pointer' }}
          text={`${record['Assigned To'].value}`}
          icon='fa-solid fa-user-group' />
        return {
          key: header, value: header == "Assigned To" ? gradedStudentComponent : record[header].value, type: header == "Assigned To" ? 'component' : ''
        }
      }),
      actions: [
        { name: "", loading: gradingData.isDeletingAlternative, color: 'danger', icon: iconsFa6.delete, actionIconClass: 'text-danger', onClick: () => { gradingData.handleDelete(id, tablesTypes.Alternatives, { itemType: record['Type'].value }) } },
      ]

    }
  })
  return (
    <RFlex className="flex-column">
      <RFlex styleProps={{ gap: '30px' }} className="align-items-center">
        <p className='p-0 m-0' style={{ fontSize: '16px' }}>Alternatives To: {item?.name}</p>
        <RButton
          outline
          color='primary'
          text={tr("Add_Alternative")}
          style={{ padding: '5px 10px' }}
          onClick={() => {
            const filteredItems = JSON.parse(JSON.stringify(gradingData?.gradingState?.gradableItems))
            filteredItems.ids = filteredItems?.ids.filter((id) => id != item.id)
            delete filteredItems[item.id]
            gradingData.setFieldValue(tablesTypes.FilteredAlternatives, filteredItems)
            gradingData.setAddAlternativesModal(true)
          }} />
      </RFlex>
      {/* <RFlex className="flex-column">
          {alternatives?.ids?.map((id) => {
            return <RTextIcon text={alternatives?.[id]?.["Name"]?.value} icon={iconsFa6.add} />
          })}
        </RFlex> */}
      <RLister Records={_alternativesData} />
    </RFlex>
  )
}

export default RViewAlternatives