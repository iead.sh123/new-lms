export var optionsHammer = {
  touchAction: "compute",
  recognizers: {
    swipe: {
      threshold: 10,
      velocity: 2,
    },
  },
};
