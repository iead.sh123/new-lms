import React, { useEffect, useContext } from "react";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { seeAllPeopleWhoLikeAsync } from "store/actions/global/discussions.actions";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Loader from "utils/Loader";
import RShowAllPeople from "view/Discussion/RShowAllPeople";
import InfiniteScroll from "react-infinite-scroll-component";
import AppModal from "components/Global/ModalCustomize/AppModal";
import { Table } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";

const GShowAllPeople = () => {
  const { cohortId, categoryName } = useParams();

  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );
  const dispatch = useDispatch();

  const { seeAllPeople, loadMoreLikePage, seeAllPeopleLoading } = useSelector(
    (state) => state.discussionsRed
  );

  useEffect(() => {
    dispatch(seeAllPeopleWhoLikeAsync(DiscussionData.contentId, 1));
  }, []);

  const fetchData = () => {
    loadMoreLikePage !== null &&
      dispatch(
        seeAllPeopleWhoLikeAsync(DiscussionData.contentId, loadMoreLikePage)
      );
  };

  return (
    <AppModal
      show={DiscussionData.showAllPeople}
      parentHandleClose={DiscussionData.handleCloseSeeAllPeople}
      header={tr`likes`}
      size={"lg"}
      headerSort={
        <React.Fragment>
          {seeAllPeople?.length > 0 ? (
            <div
              id="scrollableDiv"
              style={{ height: "65vh", overflow: "auto" }}
            >
              <InfiniteScroll
                dataLength={seeAllPeople?.length}
                next={fetchData}
                hasMore={loadMoreLikePage !== null ? true : false}
                loader={loadMoreLikePage !== null && <Loader />}
                style={{ overflow: "hidden" }}
                scrollableTarget="scrollableDiv"
              >
                <Table hover responsive style={{ border: "1px solid #D9D9D9" }}>
                  <thead>
                    <tr>
                      <th>Full Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {seeAllPeople.map((person) => (
                      <tr key={person.id}>
                        <td>{person.user.full_name}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>{" "}
              </InfiniteScroll>
            </div>
          ) : (
            <Loader />
          )}
        </React.Fragment>
      }
    />
  );
};

export default GShowAllPeople;
