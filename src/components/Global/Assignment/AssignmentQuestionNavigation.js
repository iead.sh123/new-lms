import React from "react";
import { Button, Row } from "reactstrap";
import { translate } from "../../../utils/translate";
import { TYPES } from "./assignmentReducer";

const AssignmentQuestionNavigation = ({
  previousButtonDisabled,
  nextButtonDisabled,
  mode,
  assignmentReadMode,
  currentActiveGroup,
  currentActiveQuestion,
  currentActiveTopic,
  lessonGroups,
  dispatch,
  saveAssignmentDataHandler,
  saveTeacherMarksHandler,
  isLastQuestion,
}) => {
  const clickNextQuestionHandler = () => {
    const currentQuestionsLength =
      lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics[
        currentActiveTopic
      ].inst_questions.length;
    const currentTopicsLength =
      lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics.length;
    const currentGroupsLength = lessonGroups.inst_pattern_groups.length;

    if (currentQuestionsLength - 1 > currentActiveQuestion) {
      dispatch({ type: TYPES.GO_TO_NEXT_QUESTION });
      return;
    }

    if (currentTopicsLength - 1 > currentActiveTopic) {
      dispatch({ type: TYPES.GO_TO_NEXT_TOPIC });
      return;
    }

    if (currentGroupsLength - 1 > currentActiveGroup) {
      dispatch({ type: TYPES.GO_TO_NEXT_GROUP });
      return;
    }
    dispatch({ type: TYPES.DISABLE_NEXT_BUTTON });
  };

  const clickPreviousQuestionHandler = () => {
    if (
      currentActiveQuestion === 0 &&
      currentActiveGroup === 0 &&
      currentActiveTopic === 0
    ) {
      dispatch({ type: TYPES.TOGGLE_PREVIOUS_BUTTON, payload: true });
    }
    if (currentActiveQuestion > 0) {
      dispatch({ type: TYPES.GO_TO_PREVIOUS_QUESTION });
      return;
    }

    if (currentActiveTopic > 0) {
      dispatch({
        type: TYPES.GO_TO_PREVIOUS_TOPIC,
        payload:
          lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics[
            currentActiveTopic - 1
          ].inst_questions.length - 1,
      });
      return;
    }

    if (currentActiveGroup > 0) {
      dispatch({
        type: TYPES.GO_TO_PREVIOUS_GROUP,
        payload: {
          topic:
            lessonGroups.inst_pattern_groups[currentActiveGroup - 1].inst_topics
              .length - 1,
          question:
            lessonGroups.inst_pattern_groups[currentActiveGroup - 1]
              .inst_topics[
              lessonGroups.inst_pattern_groups[currentActiveGroup - 1]
                .inst_topics.length - 1
            ].inst_questions.length - 1,
        },
      });
    }
  };

  return (
    <Row className="justify-content-center mt-3 pb-3">
      <Button
        variant="info"
        className="m-2 gu-assignment-previous h_btn_inline_9 mr-2 "
        onClick={clickPreviousQuestionHandler}
        disabled={previousButtonDisabled}
      >
        <i className="fa fa-angle-left fa-lg" aria-hidden="true"></i>&nbsp;
        {translate("Previous")}
      </Button>

      <Button
        variant="info"
        className="m-2 gu-assignment-next h_btn_inline_9 ml-2"
        onClick={clickNextQuestionHandler}
        disabled={nextButtonDisabled}
      >
        {translate("Next")}&nbsp;
        <i className="fa fa-angle-right fa-lg" aria-hidden="true"></i>
      </Button>
      {mode === "student" && !assignmentReadMode && isLastQuestion() && (
        <Button
          variant="info"
          className="m-2 gu-assignment-next h_btn_inline_9"
          onClick={saveAssignmentDataHandler}
        >
          {translate("Submit")}
        </Button>
      )}
      {(mode === "teacher" || mode === "multi") && isLastQuestion() && (
        <Button
          variant="info"
          className="m-2 gu-assignment-next h_btn_inline_9"
          onClick={saveTeacherMarksHandler}
        >
          {translate("Submit")}
        </Button>
      )}
      {mode === "viewAsStudent" && isLastQuestion() && (
        <Button
          variant="info"
          className="m-2 gu-assignment-next h_btn_inline_9"
          onClick={() => window.close()}
        >
          {translate("Close")}
        </Button>
      )}
    </Row>
  );
};

export default AssignmentQuestionNavigation;
