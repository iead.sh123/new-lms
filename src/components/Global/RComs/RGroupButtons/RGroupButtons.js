import React from "react";
import { ButtonGroup } from "reactstrap";
import RButton from "../RButton";
import tr from "../RTranslator";

const RGroupButtons = ({ buttons, selectedFilter,styleProps, className }) => {
  return (
    <ButtonGroup className={className} style={styleProps}>
      {buttons.map((butt) => (
        <RButton
          key={butt.id}
          color={butt.color}
          outline={selectedFilter == butt.title ? false : butt.outline}
          onClick={() => butt.onClick(butt.title)}
          active={selectedFilter == butt.title ? true : butt.active}
          text={tr(butt.title)}
          className={butt.className ? butt.className : ''}
          style={butt.style? butt.style :{}}
        />
      ))}
    </ButtonGroup>
  );
};

export default RGroupButtons;
