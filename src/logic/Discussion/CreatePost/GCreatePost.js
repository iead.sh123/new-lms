import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllCohortAsync,
  getRoleCohortByUserIdAsync,
} from "store/actions/global/cohorts.actions";
import { getPostByIdAsync } from "store/actions/global/discussions.actions";
import RCreatePost from "view/Discussion/RCreatePost";
import { DiscussionContext } from "../GDiscussion";
import { useParams } from "react-router-dom";

const GCreatePost = () => {
  const DiscussionData = useContext(DiscussionContext);
  const dispatch = useDispatch();
  const { cohortId } = useParams();
  const { cohorts, cohortsLoading } = useSelector((state) => state.cohortsRed);
  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    if (user) {
      dispatch(getAllCohortAsync(user?.id));
      if (DiscussionData.postId) {
        dispatch(getPostByIdAsync(DiscussionData.postId));
      }
    }
  }, [user]);

  return <RCreatePost cohorts={cohorts} cohortsLoading={cohortsLoading} />;
};

export default GCreatePost;
