import { get, post, patch, destroy, put } from "config/api";
import { Services } from "engine/services";
const im_post = (route, payload) => post(`${Services.auth_organization_management.backend}${route}`, payload);
const im_get = (route) => get(`${Services.auth_organization_management.backend}${route}`);
const im_delete = (route) => destroy(`${Services.auth_organization_management.backend}${route}`);
const im_put = (route, payload) => put(`${Services.auth_organization_management.backend}${route}`, payload);

export const schoolManagementAPI = {
    getSchoolInfo: (payload) => im_post('api/generic/getTableData', payload),
    editSchoolInfo: (payload) => im_post('api/generic/store', payload),
    getSchoolTree: () => im_get('api/school-management/school-tree'),
    addEductaionStage: (payload) => im_post("api/education-stage/store", payload),
    addGradeLevel: (payload) => im_post('api/grade-level/store', payload),
    addCurricula: (payload) => im_post('api/curricula/store', payload),
    getDefaultValues: (organizationId) => im_get(`api/school/${organizationId}/default-values`),
    deleteSpecificCollapse: (payload) => im_post('api/generic/delete', payload),
    getPrincipalForEducation: (educatoinStageId, order = null) => im_get(`api/principal/getByEducationStage/${educatoinStageId}?forDropDownList=${true}&order=${order}`),
    getCandidatePrincipalForEducation: (url, educationStageId, prefix) => url ? get(url) : im_get(`api/principal/allUsersForAddAsPrincipal?education_stage_id=${educationStageId}&prefix=${prefix}`),
    reorderEducationStages: (organizationId, payload) => im_post(`api/organization/reorder/education_stage/${organizationId}`, payload),
    reorderGradeLevels: (educationStageId, payload) => im_post(`api/organization/reorder/grade_levels/${educationStageId}`, payload),
    reorderCurricula: (gradeLevelId, payload) => im_post(`api/organization/reorder/curricula/${gradeLevelId}`, payload),
    allCategories: () => get(`${Services.courses_manager.backend}api/all-categories/true`),
    // getCoursesByCategoryId: (data) => post(`${Services.courses_manager.backend}api/courses/search`, data),

    getCoursesByCategoryId: (url, { curriculuaId, name = '', category_id = null, noPagination = false }) => get(url ? url : `${Services.courses_manager.backend}api/course-catalogue/except-curriculum/${curriculuaId}?name=${name}&category_id=${category_id}&noPagination=${noPagination}`),
    addToMasterCourses: (curriculaId, payload) => im_post(`api/curricula/${curriculaId}/add/master_courses`, payload),
    getStudentsForSpecificGrade: (gradeLevelId) => im_get(`api/grade-level/getStudentsList/${gradeLevelId}`),
    addMultiplePrincipalsToStage: (payload) => im_post(`api/user/store/exists/5`, payload),
    getStudentsForGrade: (gradeLevelId, order = null) => im_get(`api/grade-level/getStudentsList/${gradeLevelId}?forDropDownList=${true}&order=${order}`),
    getCandidateStudentsForGrade: (url, gradeLevelId, prefix) => url ? get(url) : im_get(`api/school-management/students-for-grade-level/${gradeLevelId}?prefix=${prefix}`),
    addStudentsToGrade: (gradeLevelId, payload) => im_post(`api/school-management/add-students-to-grade-level/${gradeLevelId}`, payload),
    removeStudentsFromGrade: (gradeLevelId, payload) => im_post(`api/school-management/remove-students-from-a-grade-level/${gradeLevelId}`, payload),
    removePrincipalsFromStage: (educatoinStageId, payload) => im_post(`api/school-management/remove-principlas-from-education-stage/${educatoinStageId}`, payload),
    getOrganizationPeriods: (payload) => im_post(`api/generic/getTableData`, payload),
    addOrganizationPeriod: (payload) => im_post("api/generic/store", payload),
    deleteSpecificCollapse: (payload) => im_post('api/generic/delete', payload),
    deletePeriod: (payload) => im_post('api/generic/delete', payload),
    getMultipleCourses: (ids) => get(`${Services.courses_manager.backend}api/courses/bulk/` + ids),
    sendNotificationToPrincipals: (payload) => im_post(`api/school-management/send-notification`, payload),
    sendNotificationToStudents: (payload) => im_post(`api/school-management/send-notification`, payload),
    getGradeScale: (payload) => im_post('api/generic-ui-hasMany/getPivotData', payload),
    addOrEditScaleDetail: (payload) => im_post('api/generic/store', payload),
    getAssessments: (gradeLevelId) => im_get(`api/assessments/${gradeLevelId}`),
    addOrEditAssessments: (payload) => im_post(`api/assessments`, payload),
    deleteAssessment: (assessmentId) => im_delete(`api/assessments/${assessmentId}`),
    createAssessmentsEvents: (gradeLevelId) => im_post(`api/assessments/create-events/${gradeLevelId}`)

}
