import {
	GET_ALL_COHORT_REQUEST,
	GET_ALL_COHORT_SUCCESS,
	GET_ALL_COHORT_ERROR,
	GET_ROLE_COHORT_BY_USER_ID_REQUEST,
	GET_ROLE_COHORT_BY_USER_ID_SUCCESS,
	GET_ROLE_COHORT_BY_USER_ID_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { cohortsApi } from "api/global/cohorts";
import { toast } from "react-toastify";

export const getAllCohortAsync = (userId) => async (dispatch) => {
	dispatch({ type: GET_ALL_COHORT_REQUEST });
	try {
		const response = await cohortsApi.getAllCohorts(userId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_ALL_COHORT_SUCCESS,
				payload: {
					cohorts: response.data.data,
				},
			});
		} else {
			dispatch({
				type: GET_ALL_COHORT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_COHORT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getRoleCohortByUserIdAsync = (userId) => async (dispatch) => {
	dispatch({ type: GET_ROLE_COHORT_BY_USER_ID_REQUEST });
	try {
		const response = await cohortsApi.getRoleCohortByUserId(userId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_ROLE_COHORT_BY_USER_ID_SUCCESS,
				payload: {
					cohorts: response.data.data,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_ROLE_COHORT_BY_USER_ID_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_ROLE_COHORT_BY_USER_ID_ERROR,
		});
		toast.error(error?.message);
	}
};
