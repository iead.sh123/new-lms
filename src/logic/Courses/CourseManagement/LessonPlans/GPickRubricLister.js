import React, { useState, useMemo } from "react";
import { pickARubricToLessonPlanItem } from "store/actions/teacher/lessonPlan.actions";
import { pickARubricToUnitPlanSection } from "store/actions/global/unitPlan.actions";
import { versionServiceRubric } from "engine/config";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import { post } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GPickRubricLister = ({ handleAddItemsToSection, handleClosePickARubric, openPickARubric, sectionId, itemId, typeToPick }) => {
	const dispatch = useDispatch();
	const { sectionId: sId, lessonId: lId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);

	const getDataFromBackend = async (specific_url) => {
		const Request = {
			data: sId ? { section_id: sId } : { lesson_id: lId },
		};
		const url = `${Services.rubric.backend}api/${versionServiceRubric}/rubric/get`;

		let response = await post(specific_url ? specific_url : url, Request);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data.map((r) => ({
			id: r.id,
			Titles: [r.title],
			table_name: "pickARubricLister",

			details: [
				{ key: tr`title`, value: r?.title },
				{ key: tr`description`, value: r?.description },
			],
			actions: [
				{
					name: tr("pick"),
					icon: "fa fa-check-circle",
					onClick: () => {
						{
							if (lId) {
								dispatch(pickARubricToLessonPlanItem(r, itemId));
							} else if (typeToPick == "item") {
								handleAddItemsToSection(sectionId, itemId, "rubric", r, "parent", true);
								handleAddItemsToSection(sectionId, itemId, "rubric_id", r.id, "parent", true);
							} else if (typeToPick == "section") {
								dispatch(pickARubricToUnitPlanSection(r, sectionId));
							} else {
								return;
							}
						}
					},
				},
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords]
	);

	return (
		<AppModal
			show={openPickARubric}
			parentHandleClose={handleClosePickARubric}
			header={tr`pick_a_rubric`}
			size={"lg"}
			headerSort={
				<>
					<RAdvancedLister {...propsLiterals.listerProps} />
				</>
			}
		/>
	);
};

export default GPickRubricLister;
