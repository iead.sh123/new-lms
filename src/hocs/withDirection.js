import { direction } from "components/Global/RComs/RTranslator";

const withDirection = (Com) => (props) => <Com {...props} dir={direction} />;

export default withDirection;
