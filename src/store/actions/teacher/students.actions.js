import {
  GET_STUDENTS_ERROR,
  GET_STUDENTS_REQUEST,
  GET_STUDENTS_SUCCESS,
} from "./teacherTypes";
import { teacherApi } from "api/teacher";

export const dispatchFetchStudents = (rabId) => async (dispatch) => {
  dispatch({ type: GET_STUDENTS_REQUEST });

  try {
    const response = await teacherApi.getStudents(rabId);
    if (response.data.status === 1) {
      const studentsData = response.data.data.models;

      dispatch({
        type: GET_STUDENTS_SUCCESS,

        payload: studentsData,
      });
      return;
    }
    dispatch({ type: GET_STUDENTS_ERROR, payload: response.data.message });
  } catch (error) {
    dispatch({
      type: GET_STUDENTS_ERROR,
      payload: error.response?.data?.message || "something went wrong",
    });
  }
};
