import { produce } from "immer";
import {
  GET_SEMESTERS_BY_ORGANIZATION_REQUEST,
  GET_SEMESTERS_BY_ORGANIZATION_SUCCESS,
  GET_SEMESTERS_BY_ORGANIZATION_ERROR,
  GET_GRADE_SCALES_REQUEST,
  GET_GRADE_SCALES_SUCCESS,
  GET_GRADE_SCALES_ERROR,
  GET_APP_GRADE_LEVELS_REQUEST,
  GET_APP_GRADE_LEVELS_SUCCESS,
  GET_APP_GRADE_LEVELS_ERROR,
  GET_MAIN_COURSES_REQUEST,
  GET_MAIN_COURSES_SUCCESS,
  GET_MAIN_COURSES_ERROR,
  GET_EDUCATION_STAGE_REQUEST,
  GET_EDUCATION_STAGE_SUCCESS,
  GET_EDUCATION_STAGE_ERROR,
  SAVE_EDUCATION_STAGE_REQUEST,
  SAVE_EDUCATION_STAGE_SUCCESS,
  SAVE_EDUCATION_STAGE_ERROR,
  SAVE_GRADE_LEVEL_REQUEST,
  SAVE_GRADE_LEVEL_SUCCESS,
  SAVE_GRADE_LEVEL_ERROR,
  SAVE_CURRICULA_REQUEST,
  SAVE_CURRICULA_SUCCESS,
  SAVE_CURRICULA_ERROR,
  EDUCATION_STAGE_BY_ID_REQUEST,
  EDUCATION_STAGE_BY_ID_SUCCESS,
  EDUCATION_STAGE_BY_ID_ERROR,
  GRADE_LEVELS_BY_ID_REQUEST,
  GRADE_LEVELS_BY_ID_SUCCESS,
  GRADE_LEVELS_BY_ID_ERROR,
  CURRICULA_BY_ID_REQUEST,
  CURRICULA_BY_ID_SUCCESS,
  CURRICULA_BY_ID_ERROR,
  SCHOOL_INITIATION_TREE_REQUEST,
  SCHOOL_INITIATION_TREE_SUCCESS,
  SCHOOL_INITIATION_TREE_ERROR,
  FILL_NAME_SCHOOL_INITIATION,
} from "store/actions/global/globalTypes";

const InitState = {
  semesters: [],
  gradeScales: [],
  appGradeLevels: [],
  mainCourses: [],
  educationStage: [],
  tree: [],
  namePartOfSchool: {},
  semestersLoading: false,
  gradeScalesLoading: false,
  appGradeLevelsLoading: false,
  mainCoursesLoading: false,
  educationStageLoading: false,
  saveEducationStageLoading: false,
  saveGradeLevelLoading: false,
  saveCurriculaLoading: false,
  egcLoading: false,
  treeLoading: false,
};

const schoolInitiationReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_SEMESTERS_BY_ORGANIZATION_REQUEST:
        draft.semestersLoading = true;
        return draft;
      case GET_SEMESTERS_BY_ORGANIZATION_SUCCESS:
        draft.semestersLoading = false;
        draft.semesters = action.payload.data;
        return draft;
      case GET_SEMESTERS_BY_ORGANIZATION_ERROR:
        draft.semestersLoading = false;
        return draft;

      case GET_GRADE_SCALES_REQUEST:
        draft.gradeScalesLoading = true;
        return draft;
      case GET_GRADE_SCALES_SUCCESS:
        draft.gradeScalesLoading = false;
        for (let i = 0; i < action.payload.data.length; i++) {
          draft.gradeScales.push({
            label: action.payload.data[i].name,
            value: action.payload.data[i].ID,
          });
        }

        return draft;
      case GET_GRADE_SCALES_ERROR:
        draft.gradeScalesLoading = false;
        return draft;

      case GET_APP_GRADE_LEVELS_REQUEST:
        draft.appGradeLevelsLoading = true;
        return draft;
      case GET_APP_GRADE_LEVELS_SUCCESS:
        draft.appGradeLevelsLoading = false;
        draft.appGradeLevels = action.payload.data;
        return draft;
      case GET_APP_GRADE_LEVELS_ERROR:
        draft.appGradeLevelsLoading = false;
        return draft;

      case GET_MAIN_COURSES_REQUEST:
        draft.mainCoursesLoading = true;
        return draft;
      case GET_MAIN_COURSES_SUCCESS:
        draft.mainCoursesLoading = false;
        draft.mainCourses = action.payload.data;
        return draft;
      case GET_MAIN_COURSES_ERROR:
        draft.mainCoursesLoading = false;
        return draft;

      case GET_EDUCATION_STAGE_REQUEST:
        draft.educationStageLoading = true;
        return draft;
      case GET_EDUCATION_STAGE_SUCCESS:
        draft.educationStageLoading = false;
        draft.educationStage = action.payload.data;
        return draft;
      case GET_EDUCATION_STAGE_ERROR:
        draft.educationStageLoading = false;
        return draft;

      case SAVE_EDUCATION_STAGE_REQUEST:
        draft.saveEducationStageLoading = true;
        return draft;
      case SAVE_EDUCATION_STAGE_SUCCESS:
        draft.saveEducationStageLoading = false;
        return draft;
      case SAVE_EDUCATION_STAGE_ERROR:
        draft.saveEducationStageLoading = false;
        return draft;
      case SAVE_GRADE_LEVEL_REQUEST:
        draft.saveGradeLevelLoading = true;
        return draft;
      case SAVE_GRADE_LEVEL_SUCCESS:
        draft.saveGradeLevelLoading = false;
        return draft;
      case SAVE_GRADE_LEVEL_ERROR:
        draft.saveGradeLevelLoading = false;
        return draft;
      case SAVE_CURRICULA_REQUEST:
        draft.saveCurriculaLoading = true;
        return draft;
      case SAVE_CURRICULA_SUCCESS:
        draft.saveCurriculaLoading = false;
        return draft;
      case SAVE_CURRICULA_ERROR:
        draft.saveCurriculaLoading = false;
        return draft;

      case EDUCATION_STAGE_BY_ID_REQUEST:
        draft.egcLoading = true;
        return draft;
      case EDUCATION_STAGE_BY_ID_SUCCESS:
        draft.egcLoading = false;
        return draft;
      case EDUCATION_STAGE_BY_ID_ERROR:
        draft.egcLoading = false;
        return draft;
      case GRADE_LEVELS_BY_ID_REQUEST:
        draft.egcLoading = true;
        return draft;
      case GRADE_LEVELS_BY_ID_SUCCESS:
        draft.egcLoading = false;
        return draft;
      case GRADE_LEVELS_BY_ID_ERROR:
        draft.egcLoading = false;
        return draft;
      case CURRICULA_BY_ID_REQUEST:
        draft.egcLoading = true;
        return draft;
      case CURRICULA_BY_ID_SUCCESS:
        draft.egcLoading = false;
        return draft;
      case CURRICULA_BY_ID_ERROR:
        draft.egcLoading = false;
        return draft;

      case SCHOOL_INITIATION_TREE_REQUEST:
        draft.treeLoading = true;
        return draft;
      case SCHOOL_INITIATION_TREE_SUCCESS:
        draft.treeLoading = false;
        draft.tree = action.payload.data;
        return draft;
      case SCHOOL_INITIATION_TREE_ERROR:
        draft.treeLoading = false;
        return draft;

      case FILL_NAME_SCHOOL_INITIATION:
        draft.namePartOfSchool = action.payload.data;
        return draft;

      default:
        return draft;
    }
  });

export default schoolInitiationReducer;
