import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RGradeLevelContent from "view/TermsManagement/RightSide/RGradeLevelContent";
import Loader from "utils/Loader";
import REmptyData from "components/RComponents/REmptyData";

const GGradeLevelContent = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	const [orderAlpha, setOrderAlpha] = useState("desc");

	const { data, isLoading, isError } = useFetchDataRQ({
		queryKey: ["students", TermsManagementData.itemObj.termId, TermsManagementData.itemObj.gradeLevelId, orderAlpha],
		queryFn: () =>
			coursesManagerApi.organizationStudents({
				url: null,
				termId: TermsManagementData.itemObj.termId,
				gradeLevelIds: TermsManagementData.itemObj.gradeLevelId,
				noPagination: true,
				orderAlpha: orderAlpha,
			}),
	});

	const handleSortByAlphabetically = () => {
		setOrderAlpha((ele) => (ele == "desc" ? "asc" : "desc"));
	};

	if (isLoading) {
		return <Loader />;
	} else if (isError) {
		return <REmptyData />;
	}

	return <RGradeLevelContent users={data?.data?.data} handleSortBy={handleSortByAlphabetically} />;
};

export default GGradeLevelContent;
