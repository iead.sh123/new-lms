import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RListModuleContent = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	const ListModuleContent = [
		{ title: "lesson", type: "LESSON", icon: "fa fa-play-circle" },
		{ title: "poll", type: "POLLS", icon: "fa-solid fa-square-poll-vertical" },
		{ title: "survey", type: "SURVEYS", icon: "fas fa-clipboard-list" },
		{
			title: "assignment",
			type: "ASSIGNMENTS",
			icon: "fa fa-pencil-square",
		},
		{ title: "quiz", type: "QUIZZES", icon: "fa-solid fa-list-check" },
		{ title: "exam", type: "EXAMS", icon: "fa fa-question-circle" },
		{ title: "scorm", type: "SCORM", icon: "fa fa-file-archive" },
	];

	return (
		<RFlex className="mt-4 mb-4">
			{ListModuleContent.map((list, index) => (
				<RFlex
					styleProps={{ alignItems: "center" }}
					key={list.title}
					className="cursor-pointer"
					onClick={() => {
						if (!CourseModuleData.shouldNotBeAddedModuleContent) {
							if (list.type == "LESSON") {
								CourseModuleData.handleOpenLessonModal();
							} else if (list.type === "SCORM") {
								CourseModuleData.handleOpenScormModal(content);
							} else {
								CourseModuleData.handleAddItemsToModule(content, list.type, list.title);
								CourseModuleData.handleSelectModuleContentType(list.type);
								CourseModuleData.collapsesContentToggle(0);
							}
						}
						CourseModuleData.handleShowWarningMessageInModuleContent();
					}}
				>
					<i className={`${list.icon} fa-lg`} />
					<span>{list.title}</span>
					{index !== ListModuleContent.length - 1 && <span>|</span>}
				</RFlex>
			))}
		</RFlex>
	);
};

export default RListModuleContent;
