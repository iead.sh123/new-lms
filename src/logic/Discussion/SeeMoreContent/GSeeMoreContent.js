import React, { useEffect, useContext } from "react";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Loader from "utils/Loader";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import {
  loadPreviousCommentsAsync,
  emptyLoadPreviousComment,
  getCommentByIdAsync,
  getPostByIdAsync,
} from "store/actions/global/discussions.actions";
import RDiscussionCommentDesign from "view/Discussion/RDiscussionCommentDesign";
import RDiscussionPostDesign from "view/Discussion/RDiscussionPostDesign";

const GSeeMoreContent = () => {
  const dispatch = useDispatch();
  const { cohortId, categoryName } = useParams();

  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );
  useEffect(() => {
    // dispatch(emptyLoadPreviousComment());
    // dispatch(
    //   loadPreviousCommentsAsync({
    //     contentId: DiscussionData.contentId,
    //     nextId: DiscussionData.loadMoreComment ?? DiscussionData.lastCommentId,
    //     type: DiscussionData.typeContent,
    //     specificPost: false,
    //     specificContent: DiscussionData.specificContent,
    //   })
    // );
    // dispatch(getCommentByIdAsync(DiscussionData.commentId));
    dispatch(getPostByIdAsync(DiscussionData.postId));
  }, []);

  return (
    <AppModal
      show={DiscussionData.seeMoreContent}
      parentHandleClose={DiscussionData.handleCloseSeeMoreContent}
      header={tr`comment's on`}
      size={"xl"}
      headerSort={
        <React.Fragment>
          {DiscussionData.getPostByIdLoading ? (
            <Loader />
          ) : DiscussionData.loadPreviousLoading ? (
            <Loader />
          ) : (
            <RDiscussionCommentDesign
              post={DiscussionData?.cPost}
              mB={"mb-4"}
              community={true}
              typeContent={"post"}
              indRec={0}
            />
          )}
        </React.Fragment>
      }
    />
  );
};

export default GSeeMoreContent;
