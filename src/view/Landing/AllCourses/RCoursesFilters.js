import React from "react";
import styles from "./AllCourses.module.scss";
import BooleanFilters from "./FiltersTypes.js/BooleanFilters";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RCoursesFilters = ({
  allFiltersToCourses,
  handleFilter,
  handleApplySearch,
  handleResetSearch,
  selectedBooleanItems,
  closeDropdown,
}) => {
  return (
    <section className={styles.section}>
      <div className={styles.left__filter}>
        <BooleanFilters
          data={allFiltersToCourses.boolean}
          handleFilter={handleFilter}
          selectedBooleanItems={selectedBooleanItems}
        />
      </div>
      <div className={styles.right__filter}>
        <RFlex>
          <RButton
            text={tr`apply`}
            onClick={handleApplySearch}
            color="primary"
          />
          <RButton
            text={tr`reset`}
            onClick={() => {
              handleResetSearch();
            }}
            color="primary"
            outline
          />
        </RFlex>
      </div>
      <div></div>
    </section>
  );
};

export default RCoursesFilters;
