import {
  GET_LESSON_SUPPLEMENT_REQUEST,
  GET_LESSON_SUPPLEMENT,
  GET_LESSON_SUPPLEMENT_ERROR,
  GET_SUPPLEMENT_RECORD,
} from "store/actions/teacher/teacherTypes";

const initState = {
  supplementLoading: true,
  supplements: [],
  supplement_record: "",
};

const lessonSupplementReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case GET_LESSON_SUPPLEMENT_REQUEST:
      return {
        supplementLoading: true,
      };
    case GET_LESSON_SUPPLEMENT:
      return {
        ...state,
        supplements: [...payload],
        supplementLoading: false,
      };
    case GET_LESSON_SUPPLEMENT_ERROR:
      return {
        ...state,
        supplements: [...payload],
        supplementLoading: false,
      };
    case GET_SUPPLEMENT_RECORD:
      return {
        ...state,
        supplement_record: payload,
      };

    default:
      return state;
  }
};
export default lessonSupplementReducer;
