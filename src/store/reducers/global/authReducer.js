// import {
//   LOGIN_REQUEST,
//   LOGIN_ERROR,
//   LOGIN_SUCCESS,
//   GET_USER_PHOTO,
//   SET_USER_PHOTO,
//   REFRESH_TOKEN,
//   UPDATE_PASSWORD_REQUEST,
//   UPDATE_PASSWORD_SUCCESS,
//   UPDATE_PASSWORD_ERROR,
//   SWITCH_TYPE,
//   RESET_PASSWORD_REQUEST,
//   RESET_PASSWORD_SUCCESS,
//   RESET_PASSWORD_ERROR,
//   CHANGE_PASSWORD_REQUEST,
//   CHANGE_PASSWORD_SUCCESS,
//   CHANGE_PASSWORD_ERROR,
//   LOADING_FALSE,
//   REFRESH_REQUEST,
//   REFRESH_ERROR,
//   REFRESH_SUCCESS,
// } from "store/actions/global/globalTypes";
// import { ME_SUCCESS } from "store/actions/global/globalTypes";

// import produce from "immer";

// const InitState = {
//   isAuthenticated: false,
//   isStudent: false,
//   token: null,
//   authError: null,
//   userPhoto: null,
//   setUserPhoto: null,
//   user: null,
//   loading: false,
//   updatePasswordLoading: false,
//   schoolsAndTypes: [],
//   selectedSchoolAndType: 0,
//   resetPasswordLoading: false,
//   changePasswordLoading: false,
//   modeLister: true,
// };

// const authReducer = (state = InitState, action) =>
//   produce(state, (draft) => {
//     switch (action.type) {
//       case LOADING_FALSE:
//         draft.loading = false;
//         return draft;

//       case LOGIN_REQUEST:
//         draft.loading = true;
//         return draft;

//       case LOGIN_SUCCESS:
//         draft.loading = false;
//         draft.isAuthenticated = true;
//         draft.token = payload.access_token;
//         draft.refresh_token = payload.refresh_token;
//         draft.expires_in = payload.expires_in;
//         draft.token_type = payload.token_type;
//         return draft;

//       case LOGIN_ERROR:
//         draft.loading = false;
//         return draft;

//       case ME_SUCCESS:
//         draft.loading = false;
//         draft.isStudent = payload.isStudent;
//         draft.user = payload.user.original;
//         draft.schoolsAndTypes = payload.data;
//         draft.selectedSchoolAndType = payload.selected;
//         draft.authError = null;
//         return draft;

//       case RESET_PASSWORD_REQUEST:
//         draft.resetPasswordLoading = true;
//         return draft;
//       case RESET_PASSWORD_SUCCESS:
//         draft.resetPasswordLoading = false;
//         return draft;
//       case RESET_PASSWORD_ERROR:
//         draft.resetPasswordLoading = false;
//         return draft;

//       case CHANGE_PASSWORD_REQUEST:
//         draft.changePasswordLoading = true;
//         return draft;
//       case CHANGE_PASSWORD_SUCCESS:
//         draft.changePasswordLoading = false;
//         return draft;
//       case CHANGE_PASSWORD_ERROR:
//         draft.changePasswordLoading = false;
//         return draft;

//       case REFRESH_REQUEST:
//         draft.refreshing_token = true;
//         return draft;
//       case REFRESH_SUCCESS:
//         draft.changePasswordLoading = false;
//         draft.loading = false;
//         draft.isAuthenticated = true;
//         draft.isStudent = payload?.isStudent;
//         draft.token = payload.access_token;
//         draft.refresh_token = payload.refresh_token;
//         draft.expires_in = payload.expires_in;
//         draft.token_type = payload.token_type;
//         draft.user = payload.user.original;
//         draft.refreshing_token = false;
//         return draft;
//       case REFRESH_ERROR:
//         draft.changePasswordLoading = false;
//         draft.loading = false;
//         draft.refreshing_token = false;
//         draft.authError = payload;
//         return draft;

//       case REFRESH_TOKEN:
//         draft.loading = false;
//         draft.isAuthenticated = true;
//         draft.isStudent = payload.isStudent;
//         draft.token = payload.token;
//         draft.user = payload.user.original;
//         draft.schoolsAndTypes = payload.data;
//         draft.selectedSchoolAndType = payload.selected;
//         draft.authError = null;
//         return draft;

//       case GET_USER_PHOTO:
//         draft.userPhoto = payload;
//         return draft;

//       case SET_USER_PHOTO:
//         draft.setUserPhoto = payload;
//         return draft;

//       case UPDATE_PASSWORD_REQUEST:
//         draft.updatePasswordLoading = false;

//         return draft;

//       case UPDATE_PASSWORD_SUCCESS:
//         draft.updatePasswordLoading = false;
//         draft.isAuthenticated = true;
//         draft.isStudent = payload.isStudent;
//         draft.token = payload.token;
//         draft.user = payload.user.original;
//         draft.schoolsAndTypes = payload.data;
//         draft.selectedSchoolAndType = payload.selected;
//         draft.authError = null;
//         return draft;

//       case UPDATE_PASSWORD_ERROR:
//         draft.updatePasswordLoading = false;
//         return draft;

//       case SWITCH_TYPE:
//         draft.user.current_type = payload.type;
//         draft.user.current_organization = payload.school_id;
//         return draft;

//       default:
//         return draft;
//     }
//   });

// export default authReducer;

import Helper from "components/Global/RComs/Helper";
import { CHANGE_SELECTED_STUDENT } from "store/actions/global/globalTypes";
import {
	LOGOUT_REQUEST,
	LOGOUT_SUCCESS,
	LOGOUT_ERROR,
	LOGIN_REQUEST,
	LOGIN_SUCCESS,
	LOGIN_ERROR,
	REGISTER_REQUEST,
	REGISTER_SUCCESS,
	REGISTER_ERROR,
	GET_USER_PHOTO,
	SET_USER_PHOTO,
	REFRESH_TOKEN,
	UPDATE_PASSWORD_REQUEST,
	UPDATE_PASSWORD_SUCCESS,
	UPDATE_PASSWORD_ERROR,
	SWITCH_TYPE,
	RESET_PASSWORD_REQUEST,
	RESET_PASSWORD_SUCCESS,
	RESET_PASSWORD_ERROR,
	CHANGE_PASSWORD_REQUEST,
	CHANGE_PASSWORD_SUCCESS,
	CHANGE_PASSWORD_ERROR,
	LOADING_FALSE,
	REFRESH_REQUEST,
	REFRESH_ERROR,
	REFRESH_SUCCESS,
	ALL_ORGANIZATIONS_REQUEST,
	ALL_ORGANIZATIONS_SUCCESS,
	ALL_ORGANIZATIONS_ERROR,
	ME_SUCCESS,
} from "store/actions/global/globalTypes";

const initState = {
	isAuthenticated: false,
	isStudent: false,
	token: null,
	authError: null,
	userPhoto: null,
	setUserPhoto: null,
	user: null,
	loading: false,
	updatePasswordLoading: false,
	schoolsAndTypes: [],
	organizations: [],
	selectedSchoolAndType: 0,
	resetPasswordLoading: false,
	changePasswordLoading: false,
	modeLister: true,
	organizationLoading: false,
	registerLoading: false,
};

const authReducer = (state = initState, { type, payload }) => {
	switch (type) {
		case ALL_ORGANIZATIONS_REQUEST:
			return {
				...state,
				organizationLoading: true,
			};
		case ALL_ORGANIZATIONS_SUCCESS:
			return {
				...state,
				organizationLoading: false,
				organizations: payload.data,
			};
		case ALL_ORGANIZATIONS_ERROR:
			return {
				// ...state,
				organizationLoading: false,
			};

		case REGISTER_REQUEST:
			return { ...state, registerLoading: true };
		case REGISTER_SUCCESS:
			return {
				...state,
				registerLoading: false,
				// isAuthenticated: true,
				// token: payload.data.access_token,
				// refresh_token: payload.data.refresh_token,
				// expires_in: payload.data.expires_in,
				// token_type: payload.data.token_type,
			};
		case REGISTER_ERROR:
			return { ...state, registerLoading: false };

		case LOADING_FALSE:
			return {
				loading: false,
			};

		case LOGIN_REQUEST:
			return { ...state, loading: true };

		case LOGIN_SUCCESS:
			return {
				...state,
				loading: false,
				isAuthenticated: true,
				token: payload.access_token,
				refresh_token: payload.refresh_token,
				expires_in: payload.expires_in,
				token_type: payload.token_type,
			};

		case LOGIN_ERROR:
			return { ...state, loading: false };

		case ME_SUCCESS:
			Helper.cl(payload, "me success payload");
			return {
				...state,
				loading: false,
				isStudent: payload.isStudent,
				selectedStudentId: payload.user?.original?.extra?.children?.[0].id ?? null,
				user: payload.user.original,
				schoolsAndTypes: payload.data,
				selectedSchoolAndType: payload.selected,
				authError: null,
			};
		case CHANGE_SELECTED_STUDENT:
			Helper.cl(payload, "CHANGE_SELECTED_STUDENT payload");
			return {
				...state,
				selectedStudentId: payload.studentId ?? null,
			};

		case LOGIN_ERROR:
			return {
				loading: false,

				authError: payload,
			};

		case RESET_PASSWORD_REQUEST:
			return {
				resetPasswordLoading: true,
			};

		case RESET_PASSWORD_SUCCESS:
			return {
				resetPasswordLoading: false,
			};

		case RESET_PASSWORD_ERROR:
			return {
				resetPasswordLoading: false,
			};

		case CHANGE_PASSWORD_REQUEST:
			return {
				changePasswordLoading: true,
			};

		case CHANGE_PASSWORD_SUCCESS:
			return {
				changePasswordLoading: false,
			};

		case CHANGE_PASSWORD_ERROR:
			return {
				changePasswordLoading: false,
			};

		//---------------------------

		case REFRESH_REQUEST:
			return {
				...state,

				//loading: true,

				refreshing_token: true,
			};

		case REFRESH_SUCCESS:
			Helper.cl(payload, "REFRESH_SUCCESS payload");
			return {
				...state,

				loading: false,
				token: payload.access_token,
				refresh_token: payload.refresh_token,
				expires_in: payload.expires_in,
				token_type: payload.token_type,
				refreshing_token: false,
			};

		case REFRESH_ERROR:
			return {
				loading: false,

				refreshing_token: false,

				authError: payload,
			};

		//---------------------------

		case REFRESH_TOKEN:
			return {
				...state,

				loading: false,

				isAuthenticated: true,

				isStudent: payload.isStudent,

				token: payload.token,

				user: payload.user.original,

				schoolsAndTypes: payload.data,

				selectedSchoolAndType: payload.selected,

				authError: null,
			};

		case LOGOUT_REQUEST:
			return { ...state, loading: true };

		case LOGOUT_SUCCESS:
			return {
				...initState,
			};

		case LOGOUT_ERROR:
			return {};

		// case CHECK_AUTH:

		//   return checkAuth(state);

		case GET_USER_PHOTO:
			return {
				...state,

				userPhoto: payload,
			};

		case SET_USER_PHOTO:
			return {
				...state,

				setUserPhoto: payload,
			};

		case UPDATE_PASSWORD_REQUEST:
			return {
				...state,

				updatePasswordLoading: true,
			};

		case UPDATE_PASSWORD_SUCCESS:
			return {
				...state,

				updatePasswordLoading: false,

				isAuthenticated: true,

				isStudent: payload.isStudent,

				token: payload.token,

				user: payload.user.original,

				schoolsAndTypes: payload.data,

				selectedSchoolAndType: payload.selected,

				authError: null,
			};

		case UPDATE_PASSWORD_ERROR:
			return {
				...state,

				updatePasswordLoading: false,
			};

		case SWITCH_TYPE:
			return {
				...state,

				user: {
					...state.user,

					current_type: payload.type_string,
					//current_type: payload.type_id,
					current_organization: payload.organization_id,
				},
			};

		default:
			return state;
	}
};

export default authReducer;
