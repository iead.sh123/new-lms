import RButton from "components/Global/RComs/RButton";
import React from "react";
import { Input } from "reactstrap";

const CommentsAndNotesInput = ({ handleAdd, scrollToEnd }) => {
  const [input, setInput] = React.useState("");

  const handleSubmit = async () => {
    await handleAdd(input);
    setInput("");
    scrollToEnd?.();
  };

  const handleChange = (e) => setInput(e.target.value);

  return (
    <div className="d-flex justify-content-between w-100">
      <Input
        onChange={handleChange}
        value={input}
        onKeyDown={(e) => {
          e.key === "Enter" && handleSubmit();
        }}
      />
      <RButton
        faicon="fa fa-paper-plane"
        onClick={handleSubmit}
        color="primary"
        outline
      />
    </div>
  );
};

export default CommentsAndNotesInput;
