import {
	GET_POST_SERVICE_REQUEST,
	GET_POST_SERVICE_SUCCESS,
	GET_POST_SERVICE_ERROR,
	GET_SPECIFIC_POST_REQUEST,
	GET_SPECIFIC_POST_SUCCESS,
	GET_SPECIFIC_POST_ERROR,
	GET_POSTS_BY_USER_REQUEST,
	GET_POSTS_BY_USER_SUCCESS,
	GET_POSTS_BY_USER_ERROR,
	SEARCH_POSTS_REQUEST,
	SEARCH_POSTS_SUCCESS,
	SEARCH_POSTS_ERROR,
	GET_ACCESS_LEVELS_BY_USERS_REQUEST,
	GET_ACCESS_LEVELS_BY_USERS_SUCCESS,
	GET_ACCESS_LEVELS_BY_USERS_ERROR,
	ADD_LIKE_TO_POST,
	LOAD_MORE_TO_LIKES,
	DELETE_POST_SERVICE,
	ADD_COMMENT_TO_POST,
	UPDATE_COMMENT_TO_POST,
	DELETE_COMMENT_TO_POST,
	ADD_LIKE_TO_COMMENT,
	LOAD_MORE_TO_COMMENTS,
} from "./socialMediaServiceType";
import { socialMediaServiceApi } from "api/socialMedia/socialMediaService";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const getPost = (pageNumber) => async (dispatch) => {
	dispatch({ type: GET_POST_SERVICE_REQUEST });
	try {
		const response = await socialMediaServiceApi.getAllPost(pageNumber);
		if (response.data.status === 1) {
			dispatch({
				type: GET_POST_SERVICE_SUCCESS,
				authError: null,
				social: response.data.data.models,
				socialPosts: response.data.data.models.home_page,
				loadMorePage: response.data.data.models?.page,
			});
		} else {
			dispatch({ type: GET_POST_SERVICE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_POST_SERVICE_ERROR });
		toast.error(error?.message);
	}
};

export const getAccessLevels = (text, id) => async (dispatch) => {
	dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_REQUEST });
	try {
		const response = await socialMediaServiceApi.AccessLevels(text, id);
		if (response.data.status === 1) {
			dispatch({
				type: GET_ACCESS_LEVELS_BY_USERS_SUCCESS,
				authError: null,
				accesslevels: response.data.data,
			});
		} else {
			dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_ERROR });
		toast.error(error?.message);
	}
};

export const addNewPost = (postData) => {
	return (dispatch) => {
		dispatch(showLoading());
		socialMediaServiceApi
			.NewPost(postData)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch(hideLoading());
					dispatch(getPost(0));
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const deletePost = (PostId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.removePost(PostId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: DELETE_POST_SERVICE,
						authError: null,
						PostId: PostId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const addLike = (likeData, comment, postId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.writeLike(likeData)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: comment ? ADD_LIKE_TO_COMMENT : ADD_LIKE_TO_POST,
						authError: null,
						addLike: response.data,
						postId: postId,
						commentId: likeData.comment_id,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const writeComment = (commentData) => {
	return (dispatch) => {
		socialMediaServiceApi
			.addComment(commentData)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: ADD_COMMENT_TO_POST,
						authError: null,
						addcomment: response.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const updateComment = (FormData, CommentId, PostId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.editComment(FormData, CommentId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: UPDATE_COMMENT_TO_POST,
						updatecomment: response.data.data,
						PostId: PostId,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const deleteComment = (CommentId, PostId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.removeComment(CommentId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: DELETE_COMMENT_TO_POST,
						authError: null,
						deletecomment: response.data,
						CommentId: CommentId,
						PostId: PostId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const loadMoreComments = (data, postId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.moreComments(data)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: LOAD_MORE_TO_COMMENTS,
						authError: null,
						loadResult: response.data,
						postId: postId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const loadMoreLikes = (data, commentId, likeComment, postId) => {
	return (dispatch) => {
		socialMediaServiceApi
			.moreLikes(data)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: LOAD_MORE_TO_LIKES,
						authError: null,
						loadResult: response.data,
						likeComment: likeComment,
						postId: postId,
						commentId: commentId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

// to empty state from post old
export const emptySpecificPost = () => {
	return (dispatch) => {
		dispatch({
			type: GET_SPECIFIC_POST_SUCCESS,
			authError: null,
			specificPosts: [],
		});
	};
};

//empty state posts for all users
export const emptyPostsByUser = () => {
	return (dispatch) => {
		dispatch({
			type: GET_POSTS_BY_USER_SUCCESS,
			authError: null,
			postsbyuser: [],
			postsByUsers: [],
		});
	};
};

export const getSpecificPost = (urlPostId) => async (dispatch) => {
	dispatch({ type: GET_SPECIFIC_POST_REQUEST });
	try {
		const response = await socialMediaServiceApi.specificpost(urlPostId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_SPECIFIC_POST_SUCCESS,
				authError: null,
				specificPosts: response.data.data.models.home_page,
				specificPost: response.data.data.models,
			});
		} else {
			dispatch({ type: GET_SPECIFIC_POST_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_SPECIFIC_POST_ERROR });
		toast.error(error?.message);
	}
};

export const filteringByTag = (tag, PageNumber) => async (dispatch) => {
	dispatch({ type: GET_POST_SERVICE_REQUEST });
	try {
		const response = await socialMediaServiceApi.filtringTag(tag, PageNumber);

		if (response.data.status === 1) {
			dispatch({
				type: GET_POST_SERVICE_SUCCESS,
				authError: null,
				social: response.data.data.models,
				loadMorePage: response.data.data.models.page,
				socialPosts: response.data.data.models.home_page,
			});
		} else {
			dispatch({ type: GET_POST_SERVICE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_POST_SERVICE_ERROR });
		toast.error(error?.message);
	}
};

export const getPostsByUser = (UserId, pageNumber) => async (dispatch) => {
	dispatch({ type: GET_POSTS_BY_USER_REQUEST });
	try {
		const response = await socialMediaServiceApi.postByUser(UserId, pageNumber);

		if (response.data.status === 1) {
			dispatch({
				type: GET_POSTS_BY_USER_SUCCESS,
				authError: null,
				postsbyuser: response.data.data.models,
				loadMorePage: response.data.data.models.page,
				postsByUsers: response.data.data.models.home_page,
			});
		} else {
			dispatch({ type: GET_POSTS_BY_USER_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_POSTS_BY_USER_ERROR });
		toast.error(error?.message);
	}
};

export const searchByPost = (query, pageNumber) => async (dispatch) => {
	dispatch({ type: SEARCH_POSTS_REQUEST });
	try {
		const response = await socialMediaServiceApi.searchPost(query, pageNumber);

		if (response.data.status === 1) {
			dispatch({
				type: SEARCH_POSTS_SUCCESS,
				authError: null,
				searchResult: response.data.data.models,
				loadMorePage: response.data.data.models.page,
				searchResults: response.data.data.models.home_page,
			});
		} else {
			dispatch({ type: SEARCH_POSTS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: SEARCH_POSTS_ERROR });
		toast.error(error?.message);
	}
};
