import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { specificOrganization } from "engine/config";
import { enrollInCourseAsync } from "store/actions/global/coursesManager.action";
import { landingPageAsync } from "store/actions/global/coursesManager.action";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { paymentApi } from "api/global/payment";
import GOneDayCourses from "./CoursesCards/GOneDayCourses";
import GLatestCourses from "./CoursesCards/GLatestCourses";
import GWithDiscount from "./CoursesCards/GWithDiscount";
import GFreeCourses from "./CoursesCards/GFreeCourses";
import GCategories from "./GCategories";
import GComingSoon from "./CoursesCards/GComingSoon";
import GBanners from "./GBanners";
import Loader from "utils/Loader";
import styles from "./Home.module.scss";
import { online } from "engine/config";

export const LandingPageContext = React.createContext();

export const GHome = () => {
	const dispatch = useDispatch();

	const { landingPage, landingPageLoading, enrollInCourseLoading } = useSelector((state) => state.coursesManagerRed);
	const { user } = useSelector((state) => state.auth);

	useEffect(() => {
		dispatch(landingPageAsync(user, specificOrganization ?? user?.organization_id ?? 1));
	}, [user, specificOrganization]);

	const handleEnrollInCourse = (courseId) => {
		dispatch(enrollInCourseAsync(courseId));
	};

	const cartItemsData = useFetchDataRQ({
		queryKey: ["cart-items"],
		queryFn: () => paymentApi.fetchCartItems(),
		enableCondition: online ? false : true,
	});

	const addToCartMutation = useMutateData({
		queryFn: (data) => paymentApi.addItemToCart(data),
		invalidateKeys: [["cart-items"], ["cart-prices", []]],
		multipleKeys: true,
	});

	const handleAddToCart = ({ productID, productType }) => {
		addToCartMutation.mutate({ productID: productID, productType: "courses" });
	};

	return (
		<React.Fragment>
			{landingPageLoading ? (
				<Loader />
			) : (
				<LandingPageContext.Provider
					value={{
						landingPage,
						cartItems: cartItemsData?.data?.data?.data,

						handleEnrollInCourse,
						handleAddToCart,

						enrollInCourseLoading,
						addToCartLoading: addToCartMutation.isLoading,
					}}
				>
					<GBanners />

					<div className={styles.home__container}>
						{/* <GSuggestions /> */}
						{landingPage.latestCourses && landingPage.latestCourses.courses && landingPage.latestCourses.courses.length > 0 && (
							<GLatestCourses />
						)}
						{landingPage.categories && landingPage.categories.categories && landingPage.categories.categories.length > 0 && <GCategories />}
						{landingPage.freeCourses && landingPage.freeCourses.courses && landingPage.freeCourses.courses.length > 0 && <GFreeCourses />}
						{landingPage.comingSoon && landingPage.comingSoon.courses && landingPage.comingSoon.courses.length > 0 && <GComingSoon />}
						{landingPage.oneDayCourses && landingPage.oneDayCourses.courses && landingPage.oneDayCourses.courses.length > 0 && (
							<GOneDayCourses />
						)}
						{landingPage.withDiscount && landingPage.withDiscount.courses && landingPage.withDiscount.courses.length > 0 && (
							<GWithDiscount />
						)}

						{/* {landingPage.offlineCourses && landingPage.offlineCourses.courses && landingPage.offlineCourses.courses.length > 0 && (
							<GOfflineCourses />
						)} */}
					</div>
				</LandingPageContext.Provider>
			)}
		</React.Fragment>
	);
};
