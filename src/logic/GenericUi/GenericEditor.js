import React, { useEffect, useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { baseURL, genericPath } from "engine/config";
import { Services } from "engine/services";

import { useParams } from "react-router-dom";
import { get, post } from "config/api";
import getTabsForEntity from "./GenericUtils";
import GenericUIRelations from "./GenericUIManyToMany/GenericUIRelations";
import RRoutesTabsPanel from "components/Global/RComs/RRoutesTabsPanel";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GenericEditor = () => {
	const { entity_specific_name, entity_specific_id } = useParams();
	const [tabs, setTabs] = useState(getTabsForEntity(entity_specific_name));
	const changeHorizontalTabs = () => {};
	const [extraTabsLoaded, setExtraTabsLoaded] = useState(false);

	const [getTitleField, setGetTitleField] = useState("");
	const [getTableTitle, setGetTableTitle] = useState("");

	useEffect(() => {
		const getData = async () => {
			const response = await get(`${Services.auth_organization_management.backend}api/generic/tabs/${entity_specific_name}`);

			if (response && response.data && response.data.data) {
				const temptabs = [];
				response?.data?.data?.map((tab_data) => {
					if (tab_data.pivotTableName && tab_data.pivotTableName.length > 1) {
						let tab = {
							// masterTableName: "school_groups"
							// detailTableName: "schools"
							// pivotTableName: "schools"
							// manyToMany: false
							// title: "Schools"
							title: tab_data.title,
							extraRouteParams:
								tab_data.masterTableName +
								"/" +
								tab_data.detailTableName +
								"/" +
								tab_data.pivotTableName +
								"/" +
								(tab_data.manyToMany ? "1" : "0"),
							RouteParams: ":masterTableName/:detailTableName/:pivotTableName/:manyToMany",

							content: () => {
								return <GenericUIRelations />;
							},
						};
						temptabs.push(tab);
					}
				});
				setTabs([...tabs, ...temptabs]);

				setExtraTabsLoaded(true);
			}
		};
		getData();
	}, []);

	const fetchGetTableData = async () => {
		await post(`${Services.auth_organization_management.backend}api/generic/getTableData`, {
			payload: { TableName: entity_specific_name, id: entity_specific_id },
		})
			.then((response) => {
				setGetTableTitle(response.data.data?.table_props?.table_title);
				setGetTitleField(response.data.data?.records[response.data.data?.title_field]);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		fetchGetTableData();
	}, []);

	return (
		<>
			{extraTabsLoaded ? (
				<RRoutesTabsPanel
					title={`${getTitleField} (${getTableTitle}) Editor`}
					Tabs={tabs}
					SelectedIndex={entity_specific_name}
					changeHorizontalTabs={changeHorizontalTabs}
					baseUrl={`${baseURL}/${genericPath}/manage/${entity_specific_name}/${entity_specific_id}`}
					baseRoute={`${baseURL}/${genericPath}/manage/:entity_specific_name/:entity_specific_id`}
					Url={
						entity_specific_id
							? `${baseURL}/${genericPath}/manage/${entity_specific_name}/${entity_specific_id}/`
							: `${baseURL}/${genericPath}add/${entity_specific_name}/`
					}
				></RRoutesTabsPanel>
			) : (
				Loader()
			)}
		</>
	);
};

export default GenericEditor;
