import React from "react";
import { Link } from "react-router-dom";
import { faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./RTree.css";

const TreeNode = ({
  name,
  marginLevel,
  color,
  url,
  activate,
  collapse,
  active,
}) => {
  const mL = marginLevel * 35;
  return (
    <>
      <i class="expanded" onClick={collapse}>
        <FontAwesomeIcon
          className="Circle-Right-Icon"
          icon={faChevronCircleRight}
          style={
            active
              ? { color: "rgba(253, 131, 44, 1)" }
              : { color: "rgba(253, 131, 44, 0.5)" }
          }
        />
      </i>
      <Link to={url} onClick={activate} replace>
        <span
          className="Tree-Link"
          style={
            active
              ? {
                  backgroundColor: "rgba(253, 131, 44, 1)",
                  color: "rgba(255, 255, 255, 1)",
                }
              : { color: "rgba(99, 95, 89, 1)" }
          }
        >
          <i class="collapsed"></i>
          {name}
        </span>
      </Link>
    </>
  );
};
export default TreeNode;
