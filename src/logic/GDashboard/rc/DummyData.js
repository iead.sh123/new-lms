import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RWeekEvents.css'
function DummyData(){
  
  const recentlyUploadedFiles = [
    {
      icon: null,
      title: "First Event",
      description: (
        <div style={greendivStyle}>
          <div style={smileyStyle}>😊 Good answer</div>

          <div style={textStyle}>
            FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr
          </div>
        </div>
      ),

      image: fileImage("pdf"),
    },
    {
      icon: null,
      title: "Second Event",
      description: (
        <div style={reddivStyle}>
          <div style={smileyStyle}>😊Good answer</div>

          <div style={textStyle}>
            FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr
          </div>
        </div>
      ),
      image: fileImage("jpg"),
    },
    {
      icon: null,
      title: "Second Event",
      description: (
        <div style={orangedivStyle}>
          <div style={smileyStyle}>😊Good answer</div>

          <div style={textStyle}>
            FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr
          </div>
        </div>
      ),
      image: fileImage("mp3"),
    },
    {
      icon: null,
      title: "Second Event",
      description: (
        <div style={greendivStyle}>
          <div style={smileyStyle}>😊Good answer</div>

          <div style={textStyle}>
            FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr
          </div>
        </div>
      ),
      image: fileImage("mp4"),
    },
    // { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
  ];
   
    

  
  const reddivStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    color: "red",
  };

  const greendivStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    color: "green",
  };


  const orangedivStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    color: "orange",
  };

  const smileyStyle = {
    fontSize: "12px",
    marginRight: "3px",
  };

  const textStyle = {
    fontSize: "12px",
    marginRight: "3px",
  };



        {/* {
        Helper.js(
          e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)[0])
        } */}
        {/*   ?.meeting_id */}

        {/* <RRLiveSessions
            meetingId={e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}
            userType={"teacher"}
            text="manage"
            time={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time}
            AmPm={""}
            cours={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.name}
            lessonTitle={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.subject}
            leftTime={relativeDate(e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time)}
            onClickAction={()=>{history.push(
              `${baseURL}/${genericPath}/course-management/course/${e.id}/cohort/${e.cohort_id.cohort_id}/category/${e.category_id}/lesson/${
                e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.id 
              }/meeting/${e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}`
            );}}
          /> */}


          
{/* <RRLiveSessions
            meetingId={e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}
            userType={"teacher"}
            text="manage"
            time={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time}
            AmPm={""}
            cours={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.name}
            lessonTitle={e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.subject}
            leftTime={relativeDate(e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time)}
            onClickAction={()=>{history.push(
              `${baseURL}/${genericPath}/course-management/course/${e.id}/cohort/${e.cohort_id.cohort_id}/category/${e.category_id}/lesson/${
                e?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.id 
              }/meeting/${e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.unpublishedContents?.contents?.filter(c=>(c?.type=="live_session")).map(c=>c.live_session)?.[0]?.meeting_id}`
            );}}
          /> */}

            // useEffect(() => {
  //   const unpublished = [];
  //   courses.map((c) => {
  //     return c.todayOrTommorrowUnpublishedLessons.map((e) => {
  //       unpublished.push({
  //         time: e.lesson_date,
  //         AmPm: "",
  //         cours: c.name,
  //         lessonTitle: e.subject, 
  //         leftTime: relativeDate(e.nextLesson?.lesson_time),
  //       });
  //     });
  //   });
  //   setUnpublishedContent(unpublished);
  // }, [courses]);


  
      {/* <Col xs="6" md="5">
    <RHeader title="Tasks" count={"Current Tasks"}/>
    <RTimeLine items={items} color="#668AD7" />
  </Col> */}


         {/* {Helper.jstree(courses?.[0]?.todayOrTommorrowUnpublishedLessons,"courses todayOrTommorrowUnpublishedLessons")}

    id:520
:
lesson_date:2023-10-31 00:00:00



module_id:177
lesson_plan_id:
creator_id:1
created_at:2023-10-24T11:51:01.000000Z
updated_at:2023-10-24T11:51:01.000000Z */}



}
export default DummyData;