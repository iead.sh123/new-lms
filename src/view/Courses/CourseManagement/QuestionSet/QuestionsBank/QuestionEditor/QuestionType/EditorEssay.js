import React, { useContext } from "react";
import { Row, Col, FormGroup, Label, Button } from "reactstrap";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { dangerColor } from "config/constants";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RCkEditor from "components/Global/RComs/RCkEditor";
import styles from "../../../QuestionsTypes/QuestionsTypes.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const EditorEssay = ({ questionGroupId, question, questionNumber = 0, group }) => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<Row className="mt-4 mb-4">
			<Col md={8} xs={12}>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<i className="fas fa-align-left pt-1" />
						<RFlex>
							{group.isGroup && (
								<Label className="p-0 m-0">
									{tr`question`} &nbsp;
									{questionNumber}
								</Label>
							)}
							<Label className="p-0 m-0" style={{ fontWeight: "bolder" }}>
								{group.isGroup ? `(${tr("essay")})` : tr("essay")}
							</Label>
						</RFlex>
					</RFlex>
					<FormGroup>
						<RCkEditor
							data={question?.text}
							handleChange={(data) =>
								QuestionsBankData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "text",
									value: data,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
			<Col md={4} xs={12}>
				<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
					<RFlex styleProps={{ alignItems: "center", paddingTop: "32px" }}>
						<RFileSuite
							parentCallback={(values) => {
								QuestionsBankData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "files",
									value: values,
								});
							}}
							singleFile={true}
							binary={true}
							value={question?.files}
							theme="button_with_replace"
							fileType="image/*"
						/>
						{group?.isGroup && (
							<i
								className={iconsFa6.delete}
								style={{ color: dangerColor, cursor: "pointer" }}
								onClick={() =>
									QuestionsBankData.handleRemoveQuestionGroup({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										isGroup: group?.isGroup,
									})
								}
							/>
						)}
					</RFlex>
				</RFlex>
				<Button type="button" className={styles.instructions__button}>
					<i className="far fa-question-circle"></i> {tr`instructions`}
				</Button>
			</Col>
		</Row>
	);
};

export default EditorEssay;
