import React from "react";
import Switch from "react-switch";

const RSwitch = ({
	width = 48,
	height = 20,
	uncheckedIcon = false,
	checkedIcon = false,
	onColor = "#86d3ff",
	onHandleColor = "#2693e6",
	checked,
	onChange,
}) => {
	return (
		<Switch
			onChange={onChange}
			checked={checked}
			onColor={onColor}
			onHandleColor={onHandleColor}
			uncheckedIcon={uncheckedIcon}
			checkedIcon={checkedIcon}
			height={height}
			width={width}
			// handleDiameter={30}
			// borderRadius={6}
			boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
			activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
			className="react-switch"
		/>
	);
};

export default RSwitch;
