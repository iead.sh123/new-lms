import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RGrading.css'
import RStudentItemGrades from "./RStudentItemGrades";
import FinalResult from "./FinalResult";


	import { useEffect } from 'react';
	import Helper from 'components/Global/RComs/Helper';
	import { Services } from 'engine/services';
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
function GOneStudentGrades({}){

  const {itemId,studentId,courseId}=useParams();
	const [itemGrades,setItemGrades]=useState([]);

	useEffect (()=>{
	  const get_it=async()=>{ await Helper.fastGet(Services.grading.backend+`student-gradable-items/courses/${courseId}/students/${studentId}/details/groups`,"fail to get",(response)=>{setData(response.data?.data)},()=>{})}
	  get_it();
	  },[])

  const itemGradesfake=[{ item:'assignement (3items)',finalMark:'70',typesGrades:[{ type:'advanced math exam',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                              { type:'advanced math 1',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                              { type:'complex number',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"d-",teacherMark:""},]},
                   { item:'exam group (3items)',finalMark:'70' ,typesGrades:[{ type:'advanced math exam',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                                { type:'advanced math 1',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                                { type:'complex number',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"d-",teacherMark:""},]},
                    { item:'group name (3items)',finalMark:'70',typesGrades:[{ type:'advanced math exam',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                              { type:'advanced math 1',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                                                                               { type:'complex number',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"d-",teacherMark:""},]},
                     ]

 const finalResult={Mark:'50', GPA:'1.0',  letterMark:'F',  result:'Fail', }

 /* const typesGrades=[{ type:'advanced math exam',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                     { type:'advanced math 1',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"f",teacherMark:""},
                     { type:'complex number',Mark:'33/100',submittingDate:"1/1/2021",letterMark:"d-",teacherMark:""},] */
  
    return(
      <div style={{width:'60%',marginTop:"60px",margin:"auto"}}>
  {Helper.js({itemId,studentId,courseId})}
        {Helper.js(data)}
       <div>
        {itemGrades.map(item=>
              <RStudentItemGrades item={item.item} finalMark={item.finalMark} typesGrades={item.typesGrades}/>
        )
        }
        </div>
        <div>
         <FinalResult  Mark={finalResult.Mark} GPA={finalResult.GPA}  letterMark={finalResult.letterMark}  result={finalResult.result}/>
        </div> 

          <div className="Publishing_date">Publishing Date : 5/5/2021   </div>
          <div  className="send_notification"><input type="checkbox"></input> Send Notification to the student  </div> 
      </div>
    )

}

export default GOneStudentGrades;













    