import React, { useEffect, useState } from 'react'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader'
import RSelect from 'components/Global/RComs/RSelect'
import RLister from 'components/Global/RComs/RLister'
import * as actions from 'store/actions/global/schoolManagement.action'
import { useDispatch, useSelector } from 'react-redux'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import NewPaginator from 'components/Global/NewPaginator/NewPaginator'
import Loader from 'utils/Loader'
const GCoursesSchool = ({ handleCloseCoursesModal, isModal }) => {
    const dispatch = useDispatch()
    const { courses, categories } = useSelector((state) => state.schoolManagementRed)
    const [searchData, setSearchData] = useState('')
    const [currentCategoryId, setCurrentCategoryId] = useState(null)
    const { data,
        first_page_url,
        last_page_url,
        current_page,
        last_page,
        prev_page_url,
        next_page_url,
        total } = courses
    const handleSearch = (clearData) => {
        const data = clearData == "" ? clearData : searchData
        console.log(data)
        dispatch(actions.getAllCourses(null, { category_id: currentCategoryId?.value, name: data }))
    }

    const handleSelectCategory = (categoryId) => {
        dispatch(actions.getAllCourses(null, { category_id: categoryId.value, name: searchData }))
        setCurrentCategoryId(categoryId)
    }

    const HandleCourseClicked = (course) => {
        dispatch(actions.handleCourseClicked(course))
        isModal && handleCloseCoursesModal()
        // console.log(courseId)
    }

    useEffect(() => {
        dispatch(actions.getAllCourses(null, {}))
        dispatch(actions.getCouresesCategories())

        return () => { }
    }, [])
    const _records = data ? courses.courseIds?.map((key) => {
        const couresNameComponent = <RProfileName
            name={data[key].name}
            img={data[key].image_id ? data[key].image_id : null}
            imgStyle={{ width: '18px', height: '18px' }}
        />
        return {
            details: [
                { key: tr`course_name`, value: couresNameComponent, type: 'component' },
                { key: tr`course_code`, value: data[key]?.code ?? "-" },
                { key: tr`credit`, value: data[key].credits },
            ],
            actions: [
                {
                    name: " ",
                    icon: iconsFa6.plus,
                    color: "primary",
                    outline: true,
                    loading: courses.loading,
                    onClick: () => HandleCourseClicked(data[key]),
                },
            ],
        }
    }) : null
    return (
        courses.loading ? < Loader /> :
            <RFlex styleProps={{ flexDirection: "column" }}>
                <RFlex styleProps={{ justifyContent: "space-between" }}>
                    <RSearchHeader
                        searchLoading={courses.loading}
                        searchData={searchData}
                        handleSearch={handleSearch}
                        setSearchData={setSearchData}
                        handleChangeSearch={(value) => setSearchData(value)}
                    />

                    <div style={{ width: "250px" }}>
                        <RSelect
                            option={[{ label: `--${tr`no_category`}--`, value: null }].concat(categories)}
                            closeMenuOnSelect={true}
                            value={currentCategoryId}
                            placeholder={tr`category`}
                            onChange={(e) => handleSelectCategory(e)}
                        />
                    </div>
                </RFlex>
                {_records && <RLister Records={_records} />}
                <NewPaginator
                    firstPageUrl={first_page_url}
                    lastPageUrl={last_page_url}
                    currentPage={current_page}
                    lastPage={last_page}
                    prevPageUrl={prev_page_url}
                    nextPageUrl={next_page_url}
                    total={total}
                    data={{ name: searchData, category_id: currentCategoryId?.value }}
                    getAction={actions.getAllCourses}
                />
            </RFlex>
    )
}

export default GCoursesSchool