import React, { useState } from "react";
import { Row, Col, Input, FormGroup, Label, Button } from "reactstrap";
import { uploadFile } from "store/actions/admin/uploadFile.Action";
import { useDispatch, useSelector } from "react-redux";
import RFileUploader from "components/Global/RComs/RFileUploader";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";

import ContentMapping from "./ContentMapping";

const ExcelPage = ({
  master_table,
  master_id,
  detail_table,
  setShowExcelPage,
  typeFile,
}) => {
  const dispatch = useDispatch();
  const { data, loadingData } = useSelector((state) => state.UploadFileRed);

  const [formGroup, setFormGroup] = useState({
    master_table: master_table,
    master_id: master_id ? master_id : null,
    detail_table: detail_table ? detail_table : null,
    file: "",
    with_header: false,
    // master_table: "school_Class",
    // master_id: 13,
    // detail_table: "students",
    // file: "",
    // with_header: false,
  });

  const [showMapping, setShowMapping] = useState(true);

  const handleCallback = (childData) => {
    setFormGroup({
      ...formGroup,
      file: childData[0]?.url,
    });
  };

  const handleChangeContain = (e) => {
    setFormGroup({
      ...formGroup,
      with_header: e.target.checked,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(uploadFile({ payload: formGroup }));
  };
  return (
    <>
      <Row>
        <Col xs={12} style={{ display: "flex", justifyContent: "center" }}>
          <RFileUploader
            parentCallback={handleCallback}
            placeholder={tr`Replace the old selected file`}
            // typeFile={"text/csv"}
            typeFile={typeFile}
            removeButton={true}
          />
        </Col>
      </Row>
      <Row>
        <Col style={{ display: "flex", justifyContent: "center" }}>
          <Col md={4} xs={6} style={{ position: "relative", top: 15 }}>
            <FormGroup check inline>
              <Label check>
                <Input
                  type="checkbox"
                  onChange={(e) => handleChangeContain(e)}
                />
                <span className="form-check-sign">{tr`Contain Header`}</span>
              </Label>
            </FormGroup>
          </Col>
          <Col md={4} xs={6}>
            <Button
              block
              color="primary"
              type="submit"
              onClick={(e) => {
                handleSubmit(e);
                setShowMapping(false);
              }}
              disabled={formGroup.file !== "" ? false : true}
            >
              {loadingData ? (
                <>
                  {tr`upload`} <i className="fa fa-refresh fa-spin"></i>
                </>
              ) : (
                tr("upload")
              )}
            </Button>
          </Col>
        </Col>
      </Row>

      {showMapping ? (
        ""
      ) : loadingData ? (
        <Loader />
      ) : (
        <ContentMapping
          data={data}
          master_table={master_table}
          master_id={master_id}
          detail_table={detail_table}
          with_header={formGroup.with_header}
          setShowExcelPage={setShowExcelPage}
        />
      )}

      {}
    </>
  );
};

export default ExcelPage;
