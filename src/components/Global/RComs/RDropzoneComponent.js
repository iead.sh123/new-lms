import React, { useMemo, useState, useRef } from "react";
import { convertBase64, dataURLtoFileObject } from "utils/convertToBase64";
import { useDropzone } from "react-dropzone";
import { Services } from "engine/services";
import tr from "./RTranslator";
import dropZoneIcon from "assets/img/new/svg/upload-file.svg";
import RFlex from "./RFlex/RFlex";
import { primaryColor } from "config/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import RSwiper from "./RSwiper/RSwiper";
import { SwiperSlide } from "swiper/react";
import stylesDropZone from "./RDropzoneComponent.module.scss";

import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	powerPointIcon,
	defaultImage,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
} from "config/mimeTypes";
import upload_states from "variables/upload_states";
import { post } from "config/api";
import Helper from "./Helper";
import axios from "axios";
import RButton from "./RButton";
import { toast } from "react-toastify";

const baseStyle = {};

const activeStyle = {
	borderColor: "#2196f3",
};

const acceptStyle = {
	borderColor: "#00e676",
};

const rejectStyle = {
	borderColor: "#ff1744",
};

function RDropzoneComponent({
	parentCallback,
	typeFile,
	sizeFile = 5000,
	singleFile,
	placeholder,
	removeButton,
	value = [],
	theme = "default",
	disabled,
	binary = false,
	uploadName = "",
	setSpecificAttachment,
	callApiAfterUploadImage,
}) {
	const fileInputRef = useRef();
	const [files, setFiles] = useState([]);
	const fileIdsGen = React.useRef(null);
	React.useEffect(() => {
		fileIdsGen.current = (function* () {
			let i = 0;
			while (true) {
				yield `file${++i}`;
			}
		})();
	}, []);

	React.useEffect(() => {
		//coming from 2 way binding mechanism( store )
		// value.map((val)=>val?.ul)
		setFiles([
			...value
				.filter((val) => !val.hash_id)
				.map((val) => {
					let fileObject;

					if (binary) {
						fileObject = {
							// ...val,
							preview: val,
							id: fileIdsGen.current.next().value,
						};
					} else {
						fileObject = dataURLtoFileObject(val.url ?? val.hash_id, val.file_name ?? val.name);
						fileObject.preview = URL.createObjectURL(fileObject, fileObject.name);
						fileObject.id = fileIdsGen.current.next().value;
					}

					return fileObject;
				}),
			...value.filter((val) => val.hash_id),
		]);

		//coming from back end ( edit status)
		// setEditFiles()
	}, [value.length]);

	//------------------------------------------------------Binary action

	const uploadToBackend = async (file) => {
		const res = await post(`${Services.storage.backend}api/upload/initiate`);

		if (res && res.data.status == 1 && res.data.data && res.data.data.upload_id) {
			const url = `${Services.storage.backend}api/upload/file`;
			var bodyFormData = new FormData();
			bodyFormData.append("upload_id", res.data.data.upload_id);
			bodyFormData.append("name", file?.name);
			bodyFormData.append("file", file);
			//const token = store.getState().auth.token;

			const Uploadpromis = await axios({
				method: "post",
				url: url,
				data: bodyFormData,
				headers: {
					"Content-Type": "multipart/form-data",
					//,"Authorization": `Bearer ${token}`
				},
			});

			return Uploadpromis;
		}
	};

	const uploadSuccess = (response, file, Attachments, acceptedFiles) => {
		const attachment = {
			url: { upload_id: response.data.data.upload_id },
			file_name: file.name,
			tempid: response.data.data.upload_id,
			mime_type: file.type,
			upload_state: upload_states.finished_uploading,
		};

		if (typeof setSpecificAttachment === "function") {
			setSpecificAttachment(attachment);
		}
		Attachments.push(attachment);
		parentCallback([...Attachments, ...Array.from(acceptedFiles).filter((af) => af.hash_id)]);
	};

	//--------------------------------------------------------
	const uploadFile = async (acceptedFiles) => {
		let Attachments = [];
		if (acceptedFiles[0] !== undefined) {
			for (const file of Array.from(acceptedFiles).filter((af) => !af.hash_id)) {
				const size = file.size / 1024 / 1024;
				if (size > sizeFile) {
					toast.warning(sizeFile ? sizeFile + "GB is the Maximum size Allowed " : "5GB is the Maximum size Allowed ");
				} else {
					//------------------------------------------binary
					if (binary) {
						const uploadPromise = await uploadToBackend(file);

						if (uploadPromise && uploadPromise.data.status == 1 && uploadPromise.data.data && uploadPromise.data.data.upload_id) {
							uploadSuccess(uploadPromise, file, Attachments, acceptedFiles);
							// if (callApiAfterUploadImage) {
							//   callApiAfterUploadImage();
							// }
						} else {
							uploadPromise
								.then(function (response) {
									if (response && response.data.status == 1 && response.data.data && response.data.data.upload_id) {
										uploadSuccess(response, file, Attachments, acceptedFiles);
									} else {
									}
								})
								.catch(function (response) {
									toast.error(response);
								});
						}
					} else {
						const base64 = await convertBase64(file);
						Attachments.push({
							url: base64,
							file_name: file.name,
							mime_type: file.type,
						});
					}
				}
			}
			parentCallback([...Attachments, ...Array.from(acceptedFiles).filter((af) => af.hash_id)]);
		} else {
			toast.warning(tr`warning`, "this file is not valid");
		}
	};

	const onDrop = (acceptedFiles) => {
		if (singleFile && files?.length > 0) {
			toast.warning(tr`only one file is allowed`);
		} else {
			const newFiles = acceptedFiles.map((file) => {
				const size = file.size / 1024 / 1024;

				if (size < sizeFile) {
					return Object.assign(file, {
						preview: URL.createObjectURL(file),
						id: fileIdsGen.current.next().value,
					});
				} else {
					toast.warning(sizeFile ? sizeFile + "GB is the Maximum size Allowed " : "5GB is the Maximum size Allowed ");
				}
			});

			// uploadFile(acceptedFiles.filter((f) => f));
			// if (newFiles[0] !== undefined) {
			setFiles([...files, ...newFiles]);
			uploadFile([...files, ...newFiles]);
			// } else {
			// }
		}
	};

	const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
		onDrop,
		accept: typeFile
			? typeFile
			: "image/jpeg, image/* , image/png ,image/jpg, image/svg, image/svg+xml, .rar, .zip, application/x-zip-compressed,  application/pdf,application/vnd.openxmlformats-officedocument, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
	});

	const style = useMemo(
		() => ({
			...baseStyle,
			...(isDragActive ? activeStyle : {}),
			...(isDragAccept ? acceptStyle : {}),
			...(isDragReject ? rejectStyle : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);

	const removeImage = (fileID) => {
		const dd = files.filter((item) => (item.hash_id ? item.hash_id !== fileID : item.id !== fileID));
		setFiles((files) => files.filter((item) => (item.hash_id ? item.hash_id !== fileID : item.id !== fileID)));
		uploadFile(dd);
		if (binary) {
			// setUploadState(uploadName,upload_states.deleted);
			// const attachment={
			//   url :{upload_id:response.data.data.upload_id},
			//   file_name: file.name,
			//   tempid:response.data.data.upload_id,
			//   mime_type: file.type,
			//   upload_state:upload_states.deleted
			// }
			// if (typeof setSpecificAttachment === "function")
			// {
			//  setSpecificAttachment(attachment);
			// }
		} //binary
	};

	const thumbs = files?.map((file, index) => {
		//NOTE => Used file.hash_id to distinguish the state of modification from the state of addition
		return (
			<SwiperSlide key={index} className={stylesDropZone.thumbs}>
				<div>
					{/* {Helper.js(file)} */}
					<div className="d-flex justify-content-end">
						<FontAwesomeIcon
							icon={faTimesCircle}
							style={{ color: "#DD0000", cursor: "pointer" }}
							onClick={() => removeImage(file?.hash_id ? file?.hash_id : file.id)}
						/>
					</div>
					<div className="mt-2 mb-2">
						<img
							width="75px"
							height="75px"
							src={
								["image/png", "image/jpeg", "image/jpg", "image/svg", "image/svg+xml"].includes(
									file.hash_id ? file.mime_type : file.mime_type
								)
									? file.hash_id
										? `${file?.hash_id}`
										: file.preview
									: fileTypes.includes(file.hash_id ? file.mime_type : file.type)
									? fileIcon
									: fileWord.includes(file.hash_id ? file.mime_type : file.type)
									? wordIcon
									: fileExcel.includes(file.hash_id ? file.mime_type : file.type)
									? excelIcon
									: filePowerPoint.includes(file.hash_id ? file.mime_type : file.type)
									? powerPointIcon
									: filePdf.includes(file.hash_id ? file.mime_type : file.type)
									? pdfIcon
									: fileVideo.includes(file.hash_id ? file.mime_type : file.type)
									? videoIcon
									: fileAudio.includes(file.hash_id ? file.mime_type : file.type)
									? audioIcon
									: file.hash_id
							}
							alt={file.file_name ?? file.name}
						/>
					</div>
					<div>
						<span>{file?.file_name ? file?.file_name?.substring(0, 10) : file?.name?.substring(0, 10)}</span>
					</div>
				</div>

				<button type="button" className={stylesDropZone.btn}>
					{tr`replace`}
				</button>
			</SwiperSlide>
		);
	});

	return (
		<>
			{theme == "default" ? (
				<>
					{" "}
					{files.length != 0 ? (
						<>
							{!singleFile ? (
								<div {...getRootProps({ style })} className="mb-2">
									<input {...getInputProps()} />
									<p className="text-muted" style={{ textDecoration: "underline" }}>
										{placeholder ? placeholder : tr("add_more_file")}
									</p>
								</div>
							) : (
								<></>
							)}
						</>
					) : (
						<div {...getRootProps({ style })} hidden={files.length != 0 ? true : false}>
							<input disabled={true} {...getInputProps()} />
							<RFlex styleProps={{ justifyContent: "center" }}>
								<img src={dropZoneIcon} width={"100px"} height={"100px"} />
							</RFlex>
							<RFlex styleProps={{ justifyContent: "center" }}>
								<span style={{ color: primaryColor }}>
									<span
										style={{
											color: primaryColor,
											textDecoration: "underline",
											fontWeight: "bold",
											padding: "0px 2px",
										}}
									>
										{tr`Upload`}
									</span>
									{tr`or Drag and Drop files here`}
								</span>
							</RFlex>
						</div>
					)}
					{files.length > 0 && (
						<RSwiper slidesPerView={3} spaceBetween={10}>
							{thumbs}
						</RSwiper>
					)}
				</>
			) : theme == "second_theme" ? (
				<div {...getRootProps({ style })}>
					<input disabled={disabled} {...getInputProps()} />
					<RButton className={"pick_buttons m-0"} faicon="fa fa-download" />
				</div>
			) : theme == "button_replace_theme" ? (
				<div {...getRootProps({ style })}>
					<input disabled={disabled} {...getInputProps()} />
					<i class="fa fa-refresh" aria-hidden="true" style={{ cursor: "pointer" }} />
				</div>
			) : (
				""
			)}
		</>
	);
}

export default RDropzoneComponent;
