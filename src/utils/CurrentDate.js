import moment from "moment";

export function getCurrentDate() {
  return moment().format("YYYY-MM-DD hh:mm:ss"); //DD-MM-YYYY hh:mm:ss
}

export function timeDiffCalc(dateFuture, dateNow) {
  let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;

  // calculate days
  const days = Math.floor(diffInMilliSeconds / 86400);
  diffInMilliSeconds -= days * 86400;

  // calculate hours
  const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
  diffInMilliSeconds -= hours * 3600;

  // calculate minutes
  const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
  diffInMilliSeconds -= minutes * 60;

  let difference = "";
  if (days <= 1) {
    if (days > 0 || days === 1) {
      return (difference += days === 1 ? `${days} d ` : `${days} d `);
    }
    if (hours >= 1) {
      return (difference +=
        hours === 0 || hours === 1 ? `${hours} h ` : `${hours} h `);
    }
    if (minutes == 0) {
      return (difference += "just now");
    } else {
      difference +=
        minutes === 0 || hours === 1 ? `${minutes} m` : `${minutes} m`;
    }

    return difference;
  } else {
    return moment(dateFuture).format("MM-DD-YYYY hh:mm a");
  }
}
