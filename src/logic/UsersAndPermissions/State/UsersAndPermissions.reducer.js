import { produce, current } from "immer";
import {
  GET_USERS_TYPES_BY_ORGANIZATION_REQUEST,
  GET_USERS_TYPES_BY_ORGANIZATION_SUCCESS,
  GET_USERS_TYPES_BY_ORGANIZATION_ERROR,
  GET_ROLES_BY_SPECIFIC_USER_REQUEST,
  GET_ROLES_BY_SPECIFIC_USER_SUCCESS,
  GET_ROLES_BY_SPECIFIC_USER_ERROR,
  GET_PERMISSIONS_BY_SPECIFIC_USER_REQUEST,
  GET_PERMISSIONS_BY_SPECIFIC_USER_SUCCESS,
  GET_PERMISSIONS_BY_SPECIFIC_USER_ERROR,
  FETCH_ORGANIZATION_ROLES_REQUEST,
  FETCH_ORGANIZATION_ROLES_SUCCESS,
  FETCH_ORGANIZATION_ROLES_ERROR,
  CREATE_ROLE_TO_ORGANIZATION_REQUEST,
  CREATE_ROLE_TO_ORGANIZATION_SUCCESS,
  CREATE_ROLE_TO_ORGANIZATION_ERROR,
  GET_ALL_ROLE_CATEGORIES_REQUEST,
  GET_ALL_ROLE_CATEGORIES_SUCCESS,
  GET_ALL_ROLE_CATEGORIES_ERROR,
  ADD_VALUE_TO_ROLE_CATEGORIES_FORM,
  GET_ROLE_BY_ID_REQUEST,
  GET_ROLE_BY_ID_SUCCESS,
  GET_ROLE_BY_ID_ERROR,
  GET_PERMISSIONS_BY_ROLE_REQUEST,
  GET_PERMISSIONS_BY_ROLE_SUCCESS,
  GET_PERMISSIONS_BY_ROLE_ERROR,
  GET_ALL_CATEGORIES_ORGANIZATION_REQUEST,
  GET_ALL_CATEGORIES_ORGANIZATION_SUCCESS,
  GET_ALL_CATEGORIES_ORGANIZATION_ERROR,
  GET_PERMISSIONS_BY_CATEGORY_REQUEST,
  GET_PERMISSIONS_BY_CATEGORY_SUCCESS,
  GET_PERMISSIONS_BY_CATEGORY_ERROR,
  GET_PERMISSION_BY_ID_REQUEST,
  GET_PERMISSION_BY_ID_SUCCESS,
  GET_PERMISSION_BY_ID_ERROR,
  GET_RECENT_USER_TYPES_REQUEST,
  GET_RECENT_USER_TYPES_SUCCESS,
  GET_RECENT_USER_TYPES_ERROR,
  GET_RECENT_ADDED_ROLES_REQUEST,
  GET_RECENT_ADDED_ROLES_SUCCESS,
  GET_RECENT_ADDED_ROLES_ERROR,
  GET_RECENT_ACTIVITIES_REQUEST,
  GET_RECENT_ACTIVITIES_SUCCESS,
  GET_RECENT_ACTIVITIES_ERROR,
  GET_USERS_BY_ORGANIZATION_REQUEST,
  GET_USERS_BY_ORGANIZATION_SUCCESS,
  GET_USERS_BY_ORGANIZATION_ERROR,
  GET_PERMISSIONS_BY_ORGANIZATION_REQUEST,
  GET_PERMISSIONS_BY_ORGANIZATION_SUCCESS,
  GET_PERMISSIONS_BY_ORGANIZATION_ERROR,
  GET_USERS_BY_USER_TYPE_REQUEST,
  GET_USERS_BY_USER_TYPE_SUCCESS,
  GET_USERS_BY_USER_TYPE_ERROR,
} from "./UsersAndPermissionsTypes";
import { baseURL, genericPath } from "engine/config";
import UserManagementLister from "logic/UserManagement/UserManagementLister";
import PermissionsLister from "../Permissions/PermissionsLister";

export const initialState = {
  usersTypes: [],
  rolesBySpecificUser: [],
  permissionsBySpecificUser: [],
  organizationRoles: {},
  roleCategories: [],
  roleCategoryData: {},
  permissionByRole: {},
  permissionsByCategory: [],
  categoriesOrganization: [],
  permissionById: [],
  recentUserTypes: [],
  recentAddedRoles: [],
  recentActivities: [],
  usersByOrganization: [],
  permissionsByOrganization: [],
  usersByOrganizationLoading: false,
  permissionsByOrganizationLoading: false,
  permissionByIdLoading: false,
  permissionsByCategoryLoading: false,
  categoriesOrganizationLoading: false,
  permissionByRoleLoading: false,
  roleCategoryDataLoading: false,
  organizationRolesLoading: false,
  createRoleToOrganizationLoading: false,
  usersTypesLoading: false,
  rolesBySpecificUserLoading: false,
  permissionsBySpecificUserLoading: false,
  roleCategoriesLoading: false,
  recentUserTypesLoading: false,
  recentAddedRolesLoading: false,
  recentActivitiesLoading: false,
};

export const usersAndPermissionsReducer = (state = initialState, action) => {
  const { type, payload } = action;

  return produce(state, (draft) => {
    switch (type) {
      case GET_USERS_TYPES_BY_ORGANIZATION_REQUEST:
        draft.usersTypesLoading = true;
        return draft;
      case GET_USERS_TYPES_BY_ORGANIZATION_SUCCESS:
        draft.usersTypesLoading = false;
        if (action.payload.forDropDownList) {
          draft.usersTypes = action.payload.data;
        } else {
          for (let i = 0; i < action.payload.data.length; i++) {
            draft.usersTypes.push({
              name: action.payload.data[i].id,
              title: action.payload.data[i].title,
              url: `${baseURL}/${genericPath}/users-and-permissions/user-type/${action.payload.data[i].id}`,
              content: () => {
                return <UserManagementLister fetchDataAfterAdded={true} />;
              },
            });
          }
        }

        return draft;
      case GET_USERS_TYPES_BY_ORGANIZATION_ERROR:
        draft.usersTypesLoading = false;
        return draft;

      case GET_ROLES_BY_SPECIFIC_USER_REQUEST:
        draft.rolesBySpecificUserLoading = true;
        return draft;
      case GET_ROLES_BY_SPECIFIC_USER_SUCCESS:
        draft.rolesBySpecificUserLoading = false;
        draft.rolesBySpecificUser = action.payload.data;
        return draft;
      case GET_ROLES_BY_SPECIFIC_USER_ERROR:
        draft.rolesBySpecificUserLoading = false;
        return draft;

      case GET_PERMISSIONS_BY_SPECIFIC_USER_REQUEST:
        draft.permissionsBySpecificUserLoading = true;
        return draft;
      case GET_PERMISSIONS_BY_SPECIFIC_USER_SUCCESS:
        draft.permissionsBySpecificUserLoading = false;
        draft.permissionsBySpecificUser = action.payload.data;
        return draft;
      case GET_PERMISSIONS_BY_SPECIFIC_USER_ERROR:
        draft.permissionsBySpecificUserLoading = false;
        return draft;

      case FETCH_ORGANIZATION_ROLES_REQUEST:
        draft.organizationRolesLoading = true;
        return draft;
      case FETCH_ORGANIZATION_ROLES_SUCCESS:
        draft.organizationRolesLoading = false;
        draft.organizationRoles = action.payload.data;
        return draft;
      case FETCH_ORGANIZATION_ROLES_ERROR:
        draft.organizationRolesLoading = false;
        return draft;

      case CREATE_ROLE_TO_ORGANIZATION_REQUEST:
        draft.createRoleToOrganizationLoading = true;
        return draft;
      case CREATE_ROLE_TO_ORGANIZATION_SUCCESS:
        draft.createRoleToOrganizationLoading = false;
        return draft;
      case CREATE_ROLE_TO_ORGANIZATION_ERROR:
        draft.createRoleToOrganizationLoading = false;
        return draft;

      case GET_ALL_ROLE_CATEGORIES_REQUEST:
        draft.roleCategoriesLoading = true;
        return draft;
      case GET_ALL_ROLE_CATEGORIES_SUCCESS:
        draft.roleCategoriesLoading = false;
        draft.roleCategories = action.payload.data;
        return draft;
      case GET_ALL_ROLE_CATEGORIES_ERROR:
        draft.roleCategoriesLoading = false;
        return draft;

      case ADD_VALUE_TO_ROLE_CATEGORIES_FORM:
        draft.roleCategoryData[action.payload.name] = action.payload.value;
        return draft;

      case GET_ROLE_BY_ID_REQUEST:
        draft.roleCategoryDataLoading = true;
        return draft;
      case GET_ROLE_BY_ID_SUCCESS:
        draft.roleCategoryDataLoading = false;
        draft.roleCategoryData = action.payload.data;
        return draft;
      case GET_ROLE_BY_ID_ERROR:
        draft.roleCategoryDataLoading = false;
        return draft;

      case GET_PERMISSIONS_BY_ROLE_REQUEST:
        draft.permissionByRoleLoading = true;
        return draft;
      case GET_PERMISSIONS_BY_ROLE_SUCCESS:
        draft.permissionByRoleLoading = false;
        draft.permissionByRole = action.payload.data;
        return draft;
      case GET_PERMISSIONS_BY_ROLE_ERROR:
        draft.permissionByRoleLoading = false;
        return draft;

      case GET_ALL_CATEGORIES_ORGANIZATION_REQUEST:
        draft.categoriesOrganizationLoading = true;
        return draft;
      case GET_ALL_CATEGORIES_ORGANIZATION_SUCCESS:
        draft.categoriesOrganizationLoading = false;

        if (action.payload.forDropDownList) {
          draft.categoriesOrganization = action.payload.data;
        } else {
          for (let i = 0; i < action.payload.data.length; i++) {
            draft.categoriesOrganization.push({
              name: action.payload.data[i].id,
              title: action.payload.data[i].title,
              count: action.payload.data[i].scopes_count,
              url: `${baseURL}/${genericPath}/users-and-permissions/permissions/${action.payload.data[i].id}`,
              content: () => {
                return <PermissionsLister />;
              },
            });
          }
        }
        return draft;
      case GET_ALL_CATEGORIES_ORGANIZATION_ERROR:
        draft.categoriesOrganizationLoading = false;
        return draft;

      case GET_PERMISSIONS_BY_CATEGORY_REQUEST:
        draft.permissionsByCategoryLoading = true;
        draft.permissionsByOrganizationLoading = true;
        return draft;
      case GET_PERMISSIONS_BY_CATEGORY_SUCCESS:
        draft.permissionsByCategoryLoading = false;
        draft.permissionsByOrganizationLoading = false;
        draft.permissionsByCategory = action.payload.data;
        draft.permissionsByOrganization = action.payload.data;
        return draft;
      case GET_PERMISSIONS_BY_CATEGORY_ERROR:
        draft.permissionsByCategoryLoading = false;
        draft.permissionsByOrganizationLoading = false;
        return draft;

      case GET_PERMISSION_BY_ID_REQUEST:
        draft.permissionByIdLoading = true;
        return draft;
      case GET_PERMISSION_BY_ID_SUCCESS:
        draft.permissionByIdLoading = false;
        draft.permissionById = action.payload.data;
        return draft;
      case GET_PERMISSION_BY_ID_ERROR:
        draft.permissionByIdLoading = false;
        return draft;

      case GET_RECENT_USER_TYPES_REQUEST:
        draft.recentUserTypesLoading = true;
        return draft;
      case GET_RECENT_USER_TYPES_SUCCESS:
        draft.recentUserTypesLoading = false;
        draft.recentUserTypes = action.payload.data;
        return draft;
      case GET_RECENT_USER_TYPES_ERROR:
        draft.recentUserTypesLoading = false;
        return draft;

      case GET_RECENT_ADDED_ROLES_REQUEST:
        draft.recentAddedRolesLoading = true;
        return draft;
      case GET_RECENT_ADDED_ROLES_SUCCESS:
        draft.recentAddedRolesLoading = false;
        draft.recentAddedRoles = action.payload.data;
        return draft;
      case GET_RECENT_ADDED_ROLES_ERROR:
        draft.recentAddedRolesLoading = false;
        return draft;

      case GET_RECENT_ACTIVITIES_REQUEST:
        draft.recentActivitiesLoading = true;
        return draft;
      case GET_RECENT_ACTIVITIES_SUCCESS:
        draft.recentActivitiesLoading = false;
        draft.recentActivities = action.payload.data;
        return draft;
      case GET_RECENT_ACTIVITIES_ERROR:
        draft.recentActivitiesLoading = false;
        return draft;

      case GET_USERS_BY_ORGANIZATION_REQUEST:
        draft.usersByOrganizationLoading = true;
        return draft;
      case GET_USERS_BY_ORGANIZATION_SUCCESS:
        draft.usersByOrganizationLoading = false;
        draft.usersByOrganization = action.payload.data;
        return draft;
      case GET_USERS_BY_ORGANIZATION_ERROR:
        draft.usersByOrganizationLoading = false;
        return draft;

      case GET_PERMISSIONS_BY_ORGANIZATION_REQUEST:
        draft.permissionsByOrganizationLoading = true;
        return draft;
      case GET_PERMISSIONS_BY_ORGANIZATION_SUCCESS:
        draft.permissionsByOrganizationLoading = false;
        draft.permissionsByOrganization = action.payload.data;
        return draft;
      case GET_PERMISSIONS_BY_ORGANIZATION_ERROR:
        draft.permissionsByOrganizationLoading = false;
        return draft;

      case GET_USERS_BY_USER_TYPE_REQUEST:
        draft.usersByOrganizationLoading = true;
        return draft;
      case GET_USERS_BY_USER_TYPE_SUCCESS:
        draft.usersByOrganizationLoading = false;
        draft.usersByOrganization = action.payload.data;
        return draft;
      case GET_USERS_BY_USER_TYPE_ERROR:
        draft.usersByOrganizationLoading = false;
        return draft;

      default:
        return draft;
    }
  });
};
