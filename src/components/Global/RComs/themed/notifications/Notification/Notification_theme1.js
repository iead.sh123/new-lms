import React from 'react';
import { Row, Col } from 'reactstrap';
import moment from 'moment';
import styles from './styles/theme1.Module.scss';

const Notification_theme1 = ({ isSeen, onClick, brief, padge, time }) =>
    <Row
        className={`list-group-item ${styles["NightMode"]} ${isSeen ? "read" : " un-read"
            }`}
        onClick={onClick}
        style={{ marginBottom: 5, cursor: "pointer" }}
    >
        <Col md={1} sm={0}>
            <div
                style={{ fontSize: "50px" }}
                className={
                    (isSeen
                        ? "fa fa-check-circle "
                        : "fa fa-check-circle-o ") + "new class"
                }
            />
        </Col>
        <Col md={8} sm={8} xs={8}>
            <div>
                <div style={{ display: "inline" }}>{brief} </div>
                {padge ? (
                    <div
                        style={{
                            display: "inline",
                            border: "solid gray 1px",
                            background: "#f6aa77",
                            padding: "3px",
                            margin: "20px",
                            borderRadius: "10%",
                        }}
                    >
                        {padge}
                    </div>
                ) : null}
                <br />
                <div style={{ display: "inline" }}>
                    {" "}
                    {moment(time).format("MM-DD-YYYY hh:mm a")}
                </div>
            </div>
        </Col>
        <Col md={2} sm={0} >
            <span> </span>
        </Col>
    </Row>


export default Notification_theme1;