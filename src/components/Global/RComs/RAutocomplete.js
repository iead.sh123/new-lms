import React, { useState } from "react";
import { get } from "config/api";
import AsyncSelect from "react-select/async";
import Helper from "./Helper";

function RAutocomplete({
  defaultLabel,
  url,
  getArrayFromResponse,
  getValueFromArrayItem,
  getLabelFromArrayItem,
  getIdFromArrayItem,
  setId,
}) {
  const [options, setOptions] = useState([
    { value: "", label: defaultLabel, isDisabled: true },
  ]);
  const onSearch = async (e, filtercallback) => {
    const res = await get(url + e);
    if (res) {
      let options1 = [];
      getArrayFromResponse(res)?.forEach(function (i) {
        options1.push({
          value: getValueFromArrayItem(i),
          label: getLabelFromArrayItem(i),
          id: getIdFromArrayItem(i),
        });
      });
      setOptions(options1);
    }

    //const  setId
    return options;
  };
  return (
    <AsyncSelect
      onChange={(e) => {
        setId(e.id);
      }}
      loadOptions={onSearch}
    />
  );
}
export default RAutocomplete;
