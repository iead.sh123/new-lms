import React, { useState, useEffect } from "react";
import {
	getLearningObjectById,
	getLearningObjectAvailable,
	addValuesToLearningObject,
	editLearningObject,
} from "store/actions/global/learningObjects";
import { getCertificateForSection } from "store/actions/global/certificates";
import { useDispatch, useSelector } from "react-redux";
import { userLogsAsync } from "store/actions/global/track.action";
import { useParams } from "react-router-dom";
import RLearningObject from "view/LearningObject/RLearningObject";
import RSubmit from "components/Global/RComs/RSubmit";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";

const GLearningObject = () => {
	const dispatch = useDispatch();
	const { sectionId, lessonId, lessonContentId } = useParams();
	const [certificateSelected, setCertificateSelected] = useState(true);
	const [showCredit, setShowCredit] = useState(false);
	const [options, setOptions] = useState([]);

	const {
		learningObject,
		learningObjectAvailable,
		badges,
		learningObjectLoading,
		learningObjectAvailableLoading,
		editLearningObjectLoading,
	} = useSelector((state) => state.learningObjectRed);

	const { certificate, certificateLoading } = useSelector((state) => state.certificateRed);

	//Options
	const getOptions = () => {
		const arrayData = [];
		learningObjectAvailable &&
			learningObjectAvailable.map((value) => {
				arrayData.push({
					value: value.object_id,
					label: value.name,
				});
			});
		setOptions(arrayData);
	};

	//Fill Select Learning Object Available
	useEffect(() => {
		if (learningObjectAvailable) getOptions();
	}, [learningObjectAvailable]);

	//To Select Credit Type
	useEffect(() => {
		if (learningObject) {
			setCertificateSelected(learningObject?.credit_type == "certificate" ? true : false);
		}
	}, [learningObject]);

	//Call Apis
	useEffect(() => {
		dispatch(
			getLearningObjectById(
				sectionId ? sectionId : lessonId ? lessonId : lessonContentId,
				sectionId ? "sections" : lessonId ? "lessons" : "lesson_contents"
			)
		);
		dispatch(getLearningObjectAvailable(sectionId ? "sections" : lessonId ? "lessons" : "lesson_contents"));

		dispatch(getCertificateForSection(sectionId ? "sections" : "lessons", sectionId ? sectionId : lessonId));
	}, []);

	//userLogs
	useEffect(() => {
		dispatch(
			userLogsAsync([
				{
					interface_name: "learning object",
					operation: "view",
					context: {
						id: sectionId ? sectionId : lessonId ? lessonId : lessonContentId,
						type: sectionId ? "sections" : lessonId ? "lessons" : "lesson_contents",
					},
				},
			])
		);
	}, []);

	const handleAddValueToLearningObject = (name, value) => {
		dispatch(addValuesToLearningObject(name, value));
	};

	const handleEditLearningObject = () => {
		dispatch(editLearningObject(learningObject.id, learningObject));
	};

	return (
		<>
			{learningObjectLoading ? (
				<Loader />
			) : (
				<>
					<RLearningObject
						learningObject={learningObject}
						certificate={certificate}
						badges={badges}
						handleAddValueToLearningObject={handleAddValueToLearningObject}
						options={options}
						showCredit={showCredit}
						setShowCredit={setShowCredit}
						certificateSelected={certificateSelected}
						setCertificateSelected={setCertificateSelected}
					/>
					<RSubmit
						onClick={handleEditLearningObject}
						disabled={!learningObject.id ? true : editLearningObjectLoading ? true : false}
						value={tr`save`}
						loading={editLearningObjectLoading}
					/>
				</>
			)}
		</>
	);
};

export default GLearningObject;
