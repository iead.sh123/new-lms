import React from "react";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./RNavigationHeader.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const RNavigationHeader = ({ steps, selectedStep, disableNavigation, mini }) => {
    const { width } = useWindowDimensions();
    const mobile = width < 1000;

    return (
        <RFlex
            styleProps={{
                flexDirection: mobile ? "column" : "row",
                justifyContent: mobile ? "center" : "space-between",
                margin: mobile ? "auto" : "",
                ...(mini && { width: '50vw' }),
                borderBottom: mini ? '1px solid #9C9C9C' : 'none',
                cursor: 'pointer'
            }}
            className="mb-4"
        >

            {steps.map((step, ind) => <RFlex key={'RStepperHeader ' + step.id}>
                {mini ?
                    <div style={{ borderBottom: step.id === selectedStep ? '2px solid #668AD7' : 'none', ...(step.id === selectedStep && { color: '#668AD7' }) }}
                        onClick={() => !disableNavigation && step?.onClick && !step.disabled ? step.onClick() : () => null}
                    >
                        <FontAwesomeIcon icon={step.icon} className={styles.stepper_icon} />
                        <span>&nbsp; &nbsp;{step.text}</span>
                    </div>
                    : <><div
                        className={styles.stepper_circle}
                        style={{
                            backgroundColor: step.id === selectedStep ? primaryColor : "#dddddd",
                            color: step.id === selectedStep ? "white" : "black",
                        }}
                        onClick={() => !disableNavigation && step?.onClick && !step.disabled ? step.onClick() : () => null}
                    >
                        <FontAwesomeIcon icon={step.icon} className={styles.stepper_icon} />

                    </div>
                        <p className={styles.stepper_text}>{tr(step.text)}</p>
                        {!mobile && ind < steps.length - 1 && <p className={styles.stepper_border}></p>}</>

                }

            </RFlex>
            )}

        </RFlex>
    );
};

export default RNavigationHeader;
