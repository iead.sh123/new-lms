import {
  GET_COURSES_REQUEST,
  GET_COURSES_SUCCESS,
  GET_COURSES_ERROR,
  ADD_COURSE_REQUEST,
  ADD_COURSE_SUCCESS,
  ADD_COURSE_ERROR,
  FETCH_NEW_DATA,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const initState = {
  courses: [],
  loading: false,
  addCourseLoading: false,
  fetchNewData: false,
};

const coursesReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_COURSES_REQUEST:
        draft.loading = true;
        return draft;
      case GET_COURSES_SUCCESS:
        draft.loading = false;
        draft.courses = action.payload;
        return draft;
      case GET_COURSES_ERROR:
        draft.loading = false;
        return draft;

      case ADD_COURSE_REQUEST:
        draft.addCourseLoading = true;
        return draft;
      case ADD_COURSE_SUCCESS:
        draft.addCourseLoading = false;
        return draft;
      case ADD_COURSE_ERROR:
        draft.addCourseLoading = false;
        return draft;

      case FETCH_NEW_DATA:
        draft.fetchNewData = action.payload;
        return draft;

      default:
        return state;
    }
  });

export default coursesReducer;
