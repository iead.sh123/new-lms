import React, { useState } from "react";
import { Nav, NavItem, NavLink } from "reactstrap";
import { primaryColor } from "config/constants";
import classnames from "classnames";
import styles from "./RFilterTabs.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";

const RFilterTabs = ({ tabs, handleSearchOnTabs, handlePushToAnotherRoute, activeT, setTabActive, firstSelectedTab }) => {
	// title
	// disable
	// count
	const [activeTab, setActiveTab] = useState(activeT == null ? (tabs[0]?.title ? tabs[0]?.title : firstSelectedTab) : tr(activeT));

	return (
		<div className={`nav-tabs-navigation ${styles.nav_top}`}>
			<div className="nav-tabs-wrapper">
				<Nav tab>
					{tabs &&
						tabs?.map((tab, ind) => (
							<NavItem key={ind}>
								<NavLink
									disabled={tab.disable}
									className={
										classnames({
											active: activeTab == tab.title,
										}) + styles.border__bottom
									}
									onClick={() => {
										setTabActive && setTabActive(tab.title);
										setActiveTab(tab.title);

										handleSearchOnTabs && handleSearchOnTabs(tab.title);
										handlePushToAnotherRoute && handlePushToAnotherRoute(tab.url);
									}}
									style={{
										color: activeTab === tab.title ? primaryColor : "#333",
										borderBottom: activeTab === tab.title && `1px solid ${primaryColor}`,
										padding: "0px 20px",
										// textTransform: "lowercase",
									}}
								>
									<RFlex>
										{tab.has__behavior ? <span className={styles.has__behavior}></span> : ""}

										{tr(tab?.title?.charAt(0)?.toUpperCase() + tab?.title?.slice(1)?.toLowerCase())}
										{/* {tr(tab.title)} */}
										{tab.count ? (
											<div className={styles.count}>
												<span style={{ fontSize: "12px" }}>{tab?.count}</span>
											</div>
										) : (
											""
										)}
									</RFlex>
								</NavLink>
							</NavItem>
						))}
				</Nav>
			</div>
		</div>
	);
};

export default RFilterTabs;
