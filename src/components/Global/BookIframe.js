const BookIframe = ({ source, title, key, id }) => {
  if (!source) {
    return <div style={{ height: "380px" }}>No Source To Show</div>;
  }

  // const myCustomData = { foo: "bar" };
  // const event = new CustomEvent("myEvent", { detail: myCustomData });
  // window.parent.document.dispatchEvent(event);

  return (
    // basic bootstrap classes. you can change with yours.
    <div className="col-md-12">
      <iframe
        // onClick={dispatchEvent()}
        id={id}
        style={{ width: "100%", height: "600px" }}
        src={source}
        title={title}
        key={key}
        allowFullScreen={true}
      ></iframe>
    </div>
  );
};

export default BookIframe;
