import { calenderApi } from "api/calender";
import { ADD_COMMENT, ADD_NOTES, INITIALIZE, INITIALIZE_TYPES, LOAD_COMMENTS, LOAD_NOTES } from "../constants/calender-actions.constant";
import tr from "components/Global/RComs/RTranslator";
import { repeat } from "../constants/repeat.constant";
import { toast } from "react-toastify";

export const initializeCalender = async (dispatch, mounted) => {
	try {
		const res = await calenderApi.events();

		if (!res?.data?.status) {
			//throw new Error(res?.data?.msg ?? 'error loading events');
		}
		mounted.current &&
			dispatch({
				type: INITIALIZE,
				payload: {
					events: {
						byId: res?.data?.data?.reduce((prev, curr) => {
							const start = new Date(curr.events_from);
							const end = new Date(curr.events_to);

							const diffInMinutes = Math.floor(Math.abs(end.getTime() - start.getTime() / 60000));
							const hours = Math.floor(diffInMinutes / 60);
							const remainingMinutes = diffInMinutes % 60;

							return {
								...prev,
								[curr.events_id]: {
									id: curr.events_id,
									title: curr.events_title,
									start: curr.events_from,
									end: curr.events_to,
									from: curr.events_from,
									to: curr.events_to,
									duration: `${hours}:${remainingMinutes}:00`,
									...(![repeat.CUSTOM, repeat.ONCE].includes(curr.events_repeat) && {
										daysOfWeek:
											curr.events_repeat === repeat.Daily
												? [0, 1, 2, 3, 4, 5, 6]
												: curr.events_repeat === repeat.Weekly
												? curr.events_repeatDaysOfWeek
													? curr.events_repeatDaysOfWeek.split(",")
													: [start.getDay()]
												: [],

										start: start.getHours() + ":" + start.getMinutes(), // a start time (10am in this example)
										end: end.getHours() + ":" + end.getMinutes(),
										commentsCount: curr.commentsCount,
										notesCount: curr.notesCount,
										notes: { byId: {}, ids: [] },
										comments: { byId: {}, ids: [] },

										endRecur: curr.events_lastOcuurance,
										duration: undefined,
									}),
									color: curr.eventtype_color,
									eventTypeId: curr.eventtype_id,
									repeat: curr.events_repeat,
									eventTypeName: curr.eventtype_name,
								},
							};
						}, {}),

						ids: res?.data?.data?.map((d) => d.events_id),
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const blockEventType = async (dispatch, blockerId, organizationId, blockedIds) => {
	try {
		const response = await calenderApi.blockEventTypes(blockerId, organizationId, blockedIds);
		if (response.data.status) {
			// throw new Error(response?.data?.msg ?? 'unknown error ');
		}
		return true;
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const getEventTypesForRelations = async (dispatch) => {
	try {
		const response = await calenderApi.types();
		if (!response.data.status) throw new Error(response.data.msg);
		dispatch({
			type: INITIALIZE_TYPES,
			payload: {
				byId:
					response.data.data?.event_types?.reduce(
						(prev, curr) => ({
							...prev,
							[curr.id]: {
								...curr,
								blocks: response.data.data?.block_rules?.["" + curr.id]?.map((b) => +b.id) ?? [],
							},
						}),
						{}
					) ?? {},
				ids: response.data.data?.event_types?.map((t) => t.id) ?? [],
			},
		});
	} catch (err) {
		// return toast.error(err?.message);
	}
};

export const loadComments = async (dispatch, eventId, currentUserId, mounted) => {
	try {
		const response = await calenderApi.comments(eventId);
		if (!response.data.status) throw new Error(response.data.msg);
		mounted.current &&
			dispatch({
				type: LOAD_COMMENTS,
				payload: {
					eventId,
					comments: {
						byId: response.data?.data?.reduce(
							(prev, curr) => ({
								...prev,
								[curr.comments_id]: {
									id: curr.comments_id,
									text: curr.comments_text,
									user_id: curr.users_id,
									user_name: curr.users_id === currentUserId ? tr`you` : curr.users_name,
									created_at: curr.comments_created_at,
								},
							}),
							{}
						),
						ids: response.data?.data.map((d) => d.comments_id) ?? [],
					},
				},
			});
	} catch (error) {
		toast.error(error?.message);
	}
};

export const loadNotes = async (dispatch, eventId, mounted) => {
	try {
		const response = await calenderApi.notes(eventId);
		if (!response.data.status) throw new Error(response.data.msg);
		mounted.current &&
			dispatch({
				type: LOAD_NOTES,
				payload: {
					eventId,
					notes: {
						byId: response.data?.data?.reduce(
							(prev, curr) => ({
								...prev,
								[curr.comments_id]: {
									id: curr.comments_id,
									text: curr.comments_text,
									user_id: curr.users_id,
									user_name: tr`you`,
									created_at: curr.comments_created_at,
								},
							}),
							{}
						),
						ids: response.data?.data.map((d) => d.comments_id) ?? [],
					},
				},
			});
	} catch (error) {
		toast.error(error?.message);
	}
};

export const addComment = async (eventId, currentUserId, text, dispatch) => {
	try {
		const response = await calenderApi.addComment(eventId, {
			text,
		});
		if (!response.data.status) throw new Error(response.data.msg);
		dispatch({
			type: ADD_COMMENT,
			payload: {
				eventId,
				id: response.data.data.id,
				text: text,
				user_id: currentUserId,
				user_name: tr`you`,
				created_at: response.data.data.created_at,
			},
		});
	} catch (error) {
		toast.error(error?.message);
	}
};

export const addNote = async (eventId, currentUserId, text, dispatch) => {
	try {
		const response = await calenderApi.addNote(eventId, {
			text,
		});
		if (!response.data.status) throw new Error(response.data.msg);
		dispatch({
			type: ADD_NOTES,
			payload: {
				eventId: eventId,
				id: response.data.data.id,
				text: text,
				user_id: currentUserId,
				user_name: tr`you`,
				created_at: response.data.data.created_at,
			},
		});
	} catch (error) {
		toast.error(error?.message);
	}
};

// headerToolbar: an object with properties such as start, center, end, which control the content and layout of the header toolbar.

// initialView: a string representing the initial view to display when the calendar is first loaded.

// events: an array of event objects to display on the calendar.

// eventSources: an array of event source objects to fetch events dynamically from a remote source.

// plugins: an array of plugins to use with FullCalendar.

// eventClick: a callback function triggered when an event is clicked.

// eventMouseEnter: a callback function triggered when the mouse enters an event.

// eventMouseLeave: a callback function triggered when the mouse leaves an event.

// dateClick: a callback function triggered when a date cell is clicked.

// select: a callback function triggered when a date range is selected.

// selectAllow: a function that determines if a given date range is selectable.

// selectOverlap: a function that determines if a given date range overlaps with existing events.

// dayMaxEvents: a number that limits the number of events displayed in a single day cell.

// viewSkeletonRender: a callback function that renders the skeleton of a view.

// viewDidMount: a callback function triggered when a view is loaded.

// viewWillUnmount: a callback function triggered when a view is unloaded.

// datesSet: a callback function triggered when the visible date range changes.

// height: a string or number representing the height of the calendar.

// locale: a string representing the locale to use for formatting dates and times.

// slotDuration: a duration string representing the time duration of each time slot in the calendar.
