import { GET_LESSON_RECORING } from "store/actions/teacher/teacherTypes";

const initState = {
  loading: true,
  records: [],
};

const lessonRecordingReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case GET_LESSON_RECORING:
      return {
        ...state,
        records: [...payload],
        loading: true,
      };

    default:
      return state;
  }
};
export default lessonRecordingReducer;
