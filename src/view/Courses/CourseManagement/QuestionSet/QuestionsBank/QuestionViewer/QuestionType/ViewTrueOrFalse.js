import React from "react";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import styles from "../QuestionViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, FormGroup, Label } from "reactstrap";
import { Services } from "engine/services";

const ViewTrueOrFalse = ({ question, isGroup }) => {
  return (
    <Row className="mt-4 d-flex align-items-center">
      {!isGroup && (
        <Col xs={12} className="p-0">
          <RFlex styleProps={{ flexDirection: "column" }}>
            <RFlex>
              <i className="fa-solid fa-clipboard-check pt-1" />
              <Label className="p-0 m-0">{tr`true/false`}</Label>
            </RFlex>
          </RFlex>
        </Col>
      )}
      <Col xs={12} className="p-0 pt-3">
        {isGroup ? (
          <ul>
            <li>
              <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
            </li>
          </ul>
        ) : (
          <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
        )}
        {question?.files[0]?.hash_id !== undefined && (
          <img
            src={Services.storage.file + question?.files[0]?.hash_id}
            alt={question?.id}
            className={styles.image}
          />
        )}
      </Col>
      <Col xs={12} className="p-0 pt-3">
        {question.trueFalseStatement?.map((choice, index) => (
          <div key={choice.id}>
            <FormGroup className="d-flex align-items-center">
              <AppRadioButton
                name={question?.id}
                label={String.fromCharCode(65 + index) + " . " + choice.text}
                checked={+question.correctAnswer == choice.order}
              />
              {choice?.files[0]?.hash_id !== undefined && (
                <img
                  src={Services.storage.file + choice?.files[0]?.hash_id}
                  alt={choice?.id}
                  className={styles.image}
                />
              )}
            </FormGroup>
          </div>
        ))}
      </Col>
    </Row>
  );
};

export default ViewTrueOrFalse;
