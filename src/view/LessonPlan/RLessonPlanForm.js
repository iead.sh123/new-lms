import React from "react";
import styles from "./RLessonPlan.module.scss";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";

const RLessonPlanForm = ({ lessonPlan, lessonPlanErrors, handelLessonPlanChange, dir }) => {
	const designByDirection = dir ? styles.text_ltr : styles.text_rtl;

	return (
		<Form className={styles.form_basic_info}>
			<Row>
				<Col>
					<RFlex>
						<h6 style={{ color: primaryColor, textTransform: "capitalize" }}>{tr`Basic information`}</h6>
					</RFlex>
				</Col>
			</Row>
			<Row>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("lesson_plan_title")}</label>
						<Input
							id="name"
							required
							type="text"
							onChange={(event) => handelLessonPlanChange("name", event.target.value)}
							value={lessonPlan?.name}
							placeholder={tr("lesson_plan_title")}
						/>
						{lessonPlanErrors.name && <FormText color="danger">{lessonPlanErrors.name}</FormText>}
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("tags")}</label>
						<RTags
							defaultLabel={lessonPlan?.tags}
							url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
							getValueFromArrayItem={(i) => {
								return i.id;
							}}
							getLabelFromArrayItem={(i) => {
								return i.name;
							}}
							getIdFromArrayItem={(i) => i.id}
							defaultValues={lessonPlan?.tags}
							onChange={(data) => handelLessonPlanChange("tags", data)}
						/>
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("unit_number")}</label>
						<Input
							id="unit_nb"
							value={lessonPlan?.unit_nb}
							required
							onChange={(event) => handelLessonPlanChange("unit_nb", event.target.value)}
							type="number"
							placeholder={tr("unit_number")}
						/>
						{/* {lessonPlanErrors.unit_nb && <FormText color="danger">{lessonPlanErrors.unit_nb}</FormText>} */}
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("lesson_number")}</label>
						<Input
							id="lesson_nb"
							required
							value={lessonPlan?.lesson_nb}
							type="number"
							onChange={(event) => handelLessonPlanChange("lesson_nb", event.target.value)}
							placeholder={tr("lesson_number") + " *"}
							min={0}
						/>
						{/* {lessonPlanErrors.lesson_nb && <FormText color="danger">{lessonPlanErrors.lesson_nb}</FormText>} */}
					</FormGroup>
				</Col>
			</Row>
		</Form>
	);
};

export default RLessonPlanForm;
