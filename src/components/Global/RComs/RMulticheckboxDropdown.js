import { useState, useRef, useEffect } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import RFlex from './RFlex/RFlex';

const RMulticheckboxDropdown = ({ options, onChange ,disabled}) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const dropdownRef = useRef(null);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const handleOptionToggle = (option) => {
    const updatedOptions = options.map((o) => {
      if (o.value === option.value) {
        return { ...o, checked: !o.checked };
      }
      return o;
    });

    onChange(updatedOptions);
  };

  const handleClickOutside = (event) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
      setDropdownOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, []);

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggleDropdown} style={{ backgroundColor: 'white',color:disabled?"gray":"black" ,fontStyle: disabled?'italic':null }}>
      {/* disabled={disabled} */}
      <DropdownToggle caret style={{ backgroundColor: 'white', color:disabled?"gray":"black" ,fontStyle: disabled?'italic':null  }}>
        {options?.filter((o) => o.checked).length} selected
      </DropdownToggle>
      <DropdownMenu innerRef={dropdownRef} style={{ maxHeight: '200px', overflowY: 'auto' }}>
        {options?.map((option) => (
          <DropdownItem
            key={option.value}
            onClick={() =>!disabled&&handleOptionToggle(option)}
            active={option.checked}
            style={{ backgroundColor: 'white' }}
          >

            <RFlex>
            {option.checked ? (
              <i className="fa fa-check" />
            ) : (
              <i className="fa fa-square" style={{ color: 'white', border: 'black 1px solid' }} />
            )}
            {option.label}
            </RFlex>
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  );
};

export default RMulticheckboxDropdown;