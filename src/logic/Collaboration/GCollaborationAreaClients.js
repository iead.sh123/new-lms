import React, { useState } from "react";
import { Services } from "engine/services";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";

export const GCollaborationAreaClients = ({ setNoData = (x) => {}, setCount = () => {} }) => {
	const [records1, setRecords1] = useState([]);

	const getDataFromBackend1 = async (specific_url) => {
		const url = Services.collaboration.backend + `api/collaboration-area/destination`;
		let response1 = await get(specific_url ? specific_url : url);
		if (!(response1 && response1.data && response1.data.data && response1.data.data.length > 0))
			if (typeof setNoData === "function" && setNoData !== null) setNoData(true);
		if (response1) {
			return response1;
		}
	};

	const setData1 = (response) => {
		if (!(response && response.data && response.data.data)) if (typeof setNoData === "function" && setNoData !== null) setNoData(true);
		if (typeof setCount === "function" && setCount !== null) setCount(response?.data?.data?.length);
		setRecords1(
			response?.data?.data?.map((r, index) => {
				return {
					specialProps: {
						title: { text: r.sharable_contents_counts + "Content types" },
						Author: { image: r.image, text: r.name },

						bottomBorderColor: "",

						Description: r.updated_at,
					},
				};
			})
		);
	};

	return (
		<div>
			<RAdvancedLister
				hideTableHeader={false}
				colorEveryOtherRow={false}
				getDataFromBackend={getDataFromBackend1}
				setData={setData1}
				records={records1}
				getDataObject={(response) => response.data?.data?.data}
				characterCount={15}
				marginT={"mt-3"}
				marginB={"mb-2"}
				align={"left"}
				SpecialCard={RCollaborationCard}
				showListerMode={"cardLister"}
				swiper
				perLine={4}
			/>
		</div>
	);
};
