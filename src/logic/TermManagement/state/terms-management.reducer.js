import { current, produce } from "immer";
import {
	LOAD_ACADEMIC_YEARS,
	NAVIGATE_STEPPER,
	ADD_ACADEMIC_YEAR,
	SELECT_ACADEMIC_YEAR,
	LOAD_TERMS,
	ADD_TERM,
	UPDATE_TERM,
	MANAGE_TERM,
	REPLACE_TERM,
	REPLACE_ACADEMIC_YEAR,
	MANAGE_GRADE_LEVEL,
	ADD_HOMEROOM,
	LOAD_CURRICULA,
	LOAD_WEEKLY_SCHDEULE_EVENTS,
	DELETE_WEEKLY_SCHEDULE_EVENT,
	LOAD_AVAILABLE_WEEKLY_SCHEDULE_EDITOR_ELEMENTS,
	ADD_OTHER_WEEKLY_SCHEDULE_ELEMENT,
	UPDATE_OTHER_WEEKLY_SCHEDULE_ELEMENT,
	DELETE_OTHER_WEEKLY_SCHEDULE_ELEMENT,
	ADD_WEEKLY_SCHEDULE_ELEMENT,
	UPDATE_WEEKLY_SCHEDULE_ELEMENT,
	ADD_TO_PAYLOAD,
	SET_EDITED_HOME_ROOM,
	LOAD_ORGANIZATION_TEACHERS,
	LOAD_GRADE_LEVELS_STUDENTS,
	ENROLL_HOME_ROOM_USER,
	REMOVE_HOME_ROOM_USER,
	SET_EDITED_COURSE,
	ENROLL_COURSE_USER,
	REMOVE_COURSE_USER,
	MANAGE_CURRICULUM,
	CREATE_COURSE,
} from "./terms-manager.actions";
import { pages } from "../constants/pages.constant";
import { elementsOpType } from "../constants/elements.optype.constant";
import { enrollment_components } from "../constants/enrolment.components.constant";

export const initialState = {
	// selectedPage: pages.ACADEMIC_YEARS,
	selectedPage: pages.WEEKLY_SCHEDULE,
	selectedAcademicYear: null,
	selectedTerm: null,
	selectedGradeLevel: null,
	selectedCurriculum: null,
	entities: {
		academic_years: {
			byId: {},
			ids: [],
		},
		terms: {
			byId: {},
			ids: [],
		},
		homerooms: {
			byId: {},
			ids: [],
		},
		gradeLevels: {
			byId: {},
			ids: [],
		},
		curricula: {
			byId: {},
			ids: [],
		},
		periods: {
			byId: {},
			ids: [],
		},
		weeklyScheduleElements: {
			byId: {},
			ids: [],
		},
		types: {
			byId: {},
			ids: [],
		},
		teachers: {
			byId: {},
			ids: [],
		},
		students: {
			byId: {},
			ids: [],
		},
		availableWeeklyScheduleElements: {
			curricula: { byId: {}, ids: [] },
			homerooms: { byId: {}, ids: [] },
			others: { byId: {}, ids: [] },
		},
	},

	weeklySchedulePayload: {
		elements: [],
		elementIdsToDelete: [],
	},
	editedHomeroom: {},
	editedCourse: {},
};

export const reducer = (state = initialState, action) => {
	const { type, payload } = action;

	return produce(state, (draft) => {
		let temp1 = null;
		switch (type) {
			case NAVIGATE_STEPPER:
				draft.selectedPage = payload;
				return draft;

			case LOAD_ACADEMIC_YEARS:
				draft.entities.academic_years = payload;
				return draft;

			case ADD_ACADEMIC_YEAR:
				draft.entities.academic_years.ids.unshift(payload.id);
				draft.entities.academic_years.byId[payload.id] = payload;
				return draft;

			case SELECT_ACADEMIC_YEAR:
				draft.selectedAcademicYear = payload.academicYearId;
				if (payload.dontChangePage) return draft;

				draft.selectedPage = pages.TERMS;
				return draft;

			case LOAD_TERMS:
				draft.entities.terms = payload;
				return draft;

			case ADD_TERM:
				draft.entities.terms.ids.unshift(payload.id);
				draft.entities.terms.byId[payload.id] = payload;
				return draft;

			case UPDATE_TERM:
				draft.entities.terms.byId[payload.id][payload.field.name] = payload.field.value;
				return draft;

			case MANAGE_TERM:
				draft.selectedTerm = payload.id;

				if (!payload.dontChangePage) draft.selectedPage = pages.MANAGE_TERMS;

				draft.entities.gradeLevels = payload.data.gradeLevels;
				return draft;
			case REPLACE_TERM:
				draft.entities.terms.byId[payload.id] = payload.data;

				return draft;
			case REPLACE_ACADEMIC_YEAR:
				draft.entities.academic_years.byId[payload.id] = payload.data;

				return draft;

			case ADD_HOMEROOM:
				draft.entities.homerooms.ids.unshift(payload.id);
				draft.entities.homerooms.byId[payload.id] = payload;
				return draft;

			case MANAGE_GRADE_LEVEL:
				draft.selectedGradeLevel = payload.id;

				if (!payload.dontChangePage) draft.selectedPage = pages.HOME_ROOMS;

				draft.entities.homerooms = payload.data.homerooms;
				return draft;

			case LOAD_CURRICULA:
				draft.entities.curricula = payload;
				return draft;

			case LOAD_WEEKLY_SCHDEULE_EVENTS:
				draft.entities.weeklyScheduleElements = payload.elements;
				draft.entities.periods = payload.periods;
				draft.weeklySchedulePayload.elements = [];
				draft.weeklySchedulePayload.elementIdsToDelete = [];
				return draft;

			case DELETE_WEEKLY_SCHEDULE_EVENT:
				delete draft.entities.weeklyScheduleElements.byId[payload];
				draft.entities.weeklyScheduleElements.ids = draft.entities.weeklyScheduleElements.ids.filter((d) => +d !== +payload);
				return draft;

			case LOAD_AVAILABLE_WEEKLY_SCHEDULE_EDITOR_ELEMENTS:
				draft.entities.availableWeeklyScheduleElements = payload.elements;
				draft.entities.types = payload.types;
				return draft;

			case ADD_OTHER_WEEKLY_SCHEDULE_ELEMENT:
				draft.entities.availableWeeklyScheduleElements.others.ids.push(payload.id);
				draft.entities.availableWeeklyScheduleElements.others.byId[payload.id] = payload;

				draft.entities.types.ids.push(payload.id);
				draft.entities.types.byId[payload.id] = payload;

				return draft;

			case UPDATE_OTHER_WEEKLY_SCHEDULE_ELEMENT:
				draft.entities.availableWeeklyScheduleElements.others.byId[payload.id] = payload;
				return draft;

			case DELETE_OTHER_WEEKLY_SCHEDULE_ELEMENT:
				delete draft.entities.availableWeeklyScheduleElements.others.byId[payload];
				draft.entities.availableWeeklyScheduleElements.others.ids = draft.entities.availableWeeklyScheduleElements.others.ids.filter(
					(id) => +id !== +payload
				);
				return draft;

			case ADD_WEEKLY_SCHEDULE_ELEMENT:
				draft.entities.weeklyScheduleElements.ids.push(payload.id);
				draft.entities.weeklyScheduleElements.byId[payload.id] = {
					...payload,
					type: payload.courseId
						? Object.values(current(draft.entities.types.byId)).find((t) => t.name === "COURSE")
						: payload.homeRoomId
						? Object.values(current(draft.entities.types.byId)).find((t) => t.name === "HOMEROOM")
						: payload.typeId
						? draft.entities.types.byId[payload.typeId]
						: { color: "rgb(0,0,0)" },
				};

				return draft;

			case UPDATE_WEEKLY_SCHEDULE_ELEMENT:
				draft.entities.weeklyScheduleElements.byId[payload.id] = { ...draft.entities.weeklyScheduleElements.byId[payload.id], ...payload };
				return draft;

			case ADD_TO_PAYLOAD:
				if (payload.opType === elementsOpType.DELETE) {
					draft.weeklySchedulePayload.elementIdsToDelete.push(payload.element.id);
					return draft;
				}

				if (payload.opType === elementsOpType.UPDATE) {
					temp1 = draft.weeklySchedulePayload.elements.find((el) => el.id === payload.element.id);

					draft.weeklySchedulePayload.elements = draft.weeklySchedulePayload.elements.filter((el) => el.id !== payload.element.id);

					//sometimes we update the period then the title so we dont want to lose old updates
					draft.weeklySchedulePayload.elements.push({ ...temp1, ...payload.element });

					return draft;
				}

				draft.weeklySchedulePayload.elements.push(payload.element);
				return draft;

			case SET_EDITED_HOME_ROOM:
				draft.editedHomeroom = payload;
				draft.selectedPage = pages.HOME_ROOM_EDITOR;
				return draft;

			case LOAD_ORGANIZATION_TEACHERS:
				draft.entities.teachers = payload;
				return draft;

			case LOAD_GRADE_LEVELS_STUDENTS:
				draft.entities.students = payload;
				return draft;

			case ENROLL_HOME_ROOM_USER:
				if (payload.homeroomId != draft.editedHomeroom.id) return draft;
				if (payload?.user?.type?.name === enrollment_components.TEACHER) {
					draft.editedHomeroom.teachers.ids.push(payload?.user?.user_id);
					draft.editedHomeroom.teachers.byId[payload?.user?.user_id] = payload?.user;
					return draft;
				}

				draft.editedHomeroom.students.ids.push(payload?.user?.user_id);
				draft.editedHomeroom.students.byId[payload?.user?.user_id] = payload?.user;

				return draft;

			case REMOVE_HOME_ROOM_USER:
				if (payload.type === enrollment_components.TEACHER) {
					draft.editedHomeroom.teachers.ids = draft.editedHomeroom.teachers.ids.filter((id) => id !== payload.userId);
					delete draft.editedHomeroom.teachers.byId[payload.userId];
					return draft;
				}

				draft.editedHomeroom.students.ids = draft.editedHomeroom.students.ids.filter((id) => id !== payload.userId);
				delete draft.editedHomeroom.students.byId[payload.userId];

				return draft;

			case SET_EDITED_COURSE:
				draft.editedCourse = payload;
				return draft;

			case ENROLL_COURSE_USER:
				if (payload.editedCourse != draft.editedCourse.id) return draft;

				if (payload?.user?.type?.name === enrollment_components.TEACHER) {
					draft.editedCourse.teachers.ids.push(payload?.user?.user_id);
					draft.editedCourse.teachers.byId[payload?.user?.user_id] = payload?.user;
					return draft;
				}

				draft.editedCourse.students.ids.push(payload?.user?.user_id);
				draft.editedCourse.students.byId[payload?.user?.user_id] = payload?.user;

				return draft;

			case REMOVE_COURSE_USER:
				if (payload.type === enrollment_components.TEACHER) {
					draft.editedCourse.teachers.ids = draft.editedCourse.teachers.ids.filter((id) => id !== payload.userId);
					delete draft.editedCourse.teachers.byId[payload.userId];
					return draft;
				}

				draft.editedCourse.students.ids = draft.editedCourse.students.ids.filter((id) => id !== payload.userId);
				delete draft.editedCourse.students.byId[payload.userId];

				return draft;

			case MANAGE_CURRICULUM:
				draft.selectedCurriculum = payload;
				draft.selectedPage = pages.CURRICULA_EDITOR;
				return draft;

			case CREATE_COURSE:
				draft.entities.curricula.byId[draft.selectedCurriculum].courses.push(payload);
				return draft;

			default:
				return draft;
		}
	});
};
