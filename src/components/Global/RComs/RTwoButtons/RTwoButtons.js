import React from 'react'
import RFlex from '../RFlex/RFlex'

const RTwoButtons = (
    {
        flexClass,
        flexStyle,
        innerFlexStyle1,
        innerFlexStyle2,
        innerFlexClass1,
        innerFlexClass2,
        text1,
        text2,
        icon1,
        icon2,
        iconRight = false,
        icon1Style,
        icon2Style,
        button1Class,
        button2Class,
        button1Style,
        button2Style,
        type1='button',
        type2="button",
        onClick1,
        onClick2,
        disabled1,
        disabled2,
        useDefualtStyling=true
    }
) => {

    const defualtButtonStyle = useDefualtStyling?
    {display:'flex',justifyContent:'center',alignItems:'center',
    width:'45px',height:'32px',border:'none',padding:'5px 10px' ,fontSize:'12px',borderRadius:'2px'}:''
    return (
        <RFlex className={flexClass} styleProps={{gap:'0px',...flexStyle}}>
            <RFlex className={innerFlexClass1} styleProps={innerFlexStyle1}>
                {iconRight ? (
                    <>
                        {icon1 && <i className={icon1} style={icon1Style} />}
                        <button type={type1}className={`${button1Class}`} style={{...defualtButtonStyle,...button1Style}} disabled={disabled1} onClick={onClick1}>{text1}</button>
                    </>

                ) : (
                    <>
                        <button type={type1} className={`${button1Class} `} style={{...defualtButtonStyle,...button1Style}} disabled={disabled1} onClick={onClick1}>{text1}</button>
                        {icon1 && <i className={icon1} style={icon1Style} />}
                    </>)}
            </RFlex>
            <RFlex className={innerFlexClass2} styleProps={innerFlexStyle2}>
                {iconRight ? (
                    <>
                        {icon2 && <i className={icon2} style={icon2Style} />}
                        <button type={type2} className={`${button2Class} `} style={{...defualtButtonStyle,...button2Style}} disabled={disabled2} onClick={onClick2}>{text2}</button>
                    </>

                ) : (
                    <>
                        <button type={type2} className={`${button2Class}`} style={{...defualtButtonStyle,...button2Style}} disabled={disabled2} onClick={onClick2}>{text2}</button>
                        {icon2 && <i className={icon2} style={icon2Style} />}
                    </>)}
            </RFlex>
        </RFlex>

    )
}

export default RTwoButtons