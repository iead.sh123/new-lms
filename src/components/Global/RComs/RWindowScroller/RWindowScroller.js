import { Row, Col } from "reactstrap";
import React from "react";
import styles from "./RWindowScroller.Module.scss";
import RWindowScrollerItems from "./RWindowScrollerItems";
import RWindow from "../RWindow";

const RWindowScroller = ({ data, windowData }) => {
  return (
    <>
      {data.map((data) => {
        return (
          <Row className={styles.row}>
            <Col
              md={10}
              xs={12}
              className={styles.col}
              style={{ marginTop: "10px" }}
            >
              <Row>
                <Col md={12} xs={12}>
                  <RWindow windowData={windowData} nodeIndex={data.nodeIndex} />
                </Col>
              </Row>

              <RWindowScrollerItems
                data={data.itemsGroup}
                windowData={windowData}
              />
            </Col>
          </Row>
        );
      })}
    </>
  );
};

export default RWindowScroller;
