import React, { useEffect, useRef, useState } from "react";
import { useReactToPrint } from "react-to-print";
import { useParams } from "react-router-dom";
import { DANGER } from "utils/Alert";
import RPrinting from "../RComs/RPrinting/RPrinting";
import RButton from "components/Global/RComs/RButton";
import Loader from "../Loader";
import tr from "../RComs/RTranslator";
import Swal from "utils/Alert";
import { lessonPlanApi } from "api/teacher/lessonPlan";
import withDirection from "hocs/withDirection";
import { toast } from "react-toastify";

const GLessonPlan = ({ dir }) => {
	const { lessonId } = useParams();

	const [lessonPlanData, setLessonPlanData] = useState({});
	const [lessonPlanDataLoading, setLessonPlanDataLoading] = useState(false);

	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
	});

	useEffect(() => {
		setLessonPlanDataLoading(true);
		lessonPlanApi.importLessonPlan(lessonId).then((response) => {
			if (response.data.status == 1) {
				setLessonPlanData(response.data.data);
				setLessonPlanDataLoading(false);
			} else {
				toast.error(response.data.msg);

				setLessonPlanDataLoading(false);
			}
		});
	}, []);

	return (
		<>
			{lessonPlanDataLoading ? (
				<Loader />
			) : (
				<div className="content">
					<RButton text={tr`print`} onClick={handlePrint} color="primary" outline />
					<RPrinting data={lessonPlanData} ref={componentRef} dir={dir} />
				</div>
			)}
		</>
	);
};

export default withDirection(GLessonPlan);
