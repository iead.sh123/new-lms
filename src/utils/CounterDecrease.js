import React, { useEffect } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const CounterDecrease = ({ isRunning, time, setTime }) => {
  useEffect(() => {
    let interval;

    if (isRunning) {
      interval = setInterval(() => {
        setTime((prevTime) => {
          const newSeconds = prevTime.seconds - 1;
          const newMinutes = prevTime.minutes;
          const newHours = prevTime.hours;

          if (newSeconds < 0) {
            const remainingSeconds = 60 + newSeconds;
            const remainingMinutes = prevTime.minutes - 1;
            const remainingHours = prevTime.hours;

            if (remainingMinutes < 0) {
              if (remainingHours <= 0) {
                // Timer reached 0
                clearInterval(interval);
                alert("Timer reached 0!");
                return prevTime; // Time should not go negative
              } else {
                return {
                  hours: remainingHours - 1,
                  minutes: 59,
                  seconds: remainingSeconds,
                };
              }
            } else {
              return {
                hours: remainingHours,
                minutes: remainingMinutes,
                seconds: remainingSeconds,
              };
            }
          }

          return {
            hours: newHours,
            minutes: newMinutes,
            seconds: newSeconds,
          };
        });
      }, 1000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [isRunning]);

  return (
    <RFlex>
      <div>
        <span>{time.hours < 10 ? `0${time.hours}` : time.hours}</span>:
        <span>{time.minutes < 10 ? `0${time.minutes}` : time.minutes}</span>:
        <span>{time.seconds < 10 ? `0${time.seconds}` : time.seconds}</span>
      </div>
    </RFlex>
  );
};

export default CounterDecrease;
