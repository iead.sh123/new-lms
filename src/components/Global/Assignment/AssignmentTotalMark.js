import React from "react";
import { TYPES } from "./assignmentReducer";
import { translate } from "../../../utils/translate";
import { Input } from "reactstrap";

const AssignmentTotalMark = ({ TotalMark }) => {
  return (
    <div className="form-group row  mb-2 mt-2 pt-3">
      <label htmlFor="topic" className="col-sm-3 pt-2 gu-assignment_lableTotal">
        {translate("Total Mark")}
      </label>
      <div className="col-sm-9">
        <Input
          value={TotalMark}
          type="number"
          className="form-control"
          id="group"
          readOnly
          min={0}
        />
      </div>
    </div>
  );
};

export default AssignmentTotalMark;
