import React, { useEffect, useContext } from "react";
import REditContent from "view/Discussion/REditContent";
import { getContentByContentIdAsync } from "store/actions/global/discussions.actions";
import { DiscussionFilterContext } from "../GDiscussionFilter";
import { useDispatch } from "react-redux";

const GEditContent = () => {
  const dispatch = useDispatch();
  const DiscussionData = useContext(DiscussionFilterContext);

  useEffect(() => {
    dispatch(getContentByContentIdAsync(DiscussionData.contentId));
  }, []);
  return <REditContent />;
};

export default GEditContent;
