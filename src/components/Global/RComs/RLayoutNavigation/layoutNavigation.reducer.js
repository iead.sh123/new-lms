import { produce, current } from "immer";
import {
  DOWN,
  LEFT,
  RIGHT,
  UP,
  INITITALIZE,
  GO,
} from "./layoutNavigation.actions";

export const initialState = {
  currentNode: { x: 0, y: 0 },
  mapping: [],
  nav: false,
  // maxY: 0,
  // maxX: 0,
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  let temp1 = null,
    temp2 = null;

  return produce(state, (draft) => {
    switch (type) {
      case INITITALIZE:
        draft.mapping = payload.mapping;
        draft.currentNode = {
          ...payload.activeNode,
          url: payload.mapping.find(
            (mp) =>
              mp.x == payload.activeNode?.x && mp.y == payload.activeNode?.y
          )?.url,
        };

        // draft.maxY = current(draft).mapping.sort((a, b) => a.y < b.y ? 1 : -1)[0].y;
        // draft.maxX = current(draft).mapping.sort((a, b) => a.x < b.x ? 1 : -1)[0].x;

        return draft;
      case UP:
        temp1 = draft.mapping.find(
          (it) => it.y == draft.currentNode.y - 1 && it.x == draft.currentNode.x
        );
        if (temp1 && !temp1.readOnly) {
          draft.currentNode.y--;
          draft.currentNode.url = temp1.url;
        }
        draft.nav = !draft.nav;
        return draft;
      case DOWN:
        temp1 = draft.mapping.find(
          (it) => it.y == draft.currentNode.y + 1 && it.x == draft.currentNode.x
        );
        if (temp1 && !temp1.readOnly) {
          draft.currentNode.y++;
          draft.currentNode.url = temp1.url;
        }
        draft.nav = !draft.nav;

        return draft;

      case LEFT:
        temp1 = draft.mapping.find(
          (it) => it.x == draft.currentNode.x - 1 && it.y == draft.currentNode.y
        );
        if (temp1 && !temp1.readOnly) {
          draft.currentNode.x--;
          draft.currentNode.url = temp1.url;
        }
        draft.nav = !draft.nav;
        return draft;

      case RIGHT:
        temp1 = draft.mapping.find(
          (it) => it.x == draft.currentNode.x + 1 && it.y == draft.currentNode.y
        );
        if (temp1 && !temp1.readOnly) {
          draft.currentNode.x++;
          draft.currentNode.url = temp1.url;
        }
        draft.nav = !draft.nav;
        return draft;
      case GO:
        temp1 = draft.mapping.find(
          (it) => it.x == payload.x && it.y == payload.y
        );
        if (temp1 && !temp1.readOnly) {
          draft.currentNode = temp1;
        }
        draft.nav = !draft.nav;
        return draft;

      default:
        return draft;
    }
  });
};

export default reducer;
