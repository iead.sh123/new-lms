import React from "react";
import GenericUIAddEdit from "./GenericUIAddEdit";
import UsersAddEdit from "./UsersManagment/UsersAddEdit";

const getTabsForEntity = (s) => {
  let tabs = [];
  if (
    s == "students" ||
    s == "teachers" ||
    s == "employees" ||
    s == "principals" ||
    s == "school_advisors" ||
    s == "seniorteachers" ||
    s == "parents"
  )
    tabs = [
      {
        title: "Properties",
        content: () => {
          return <UsersAddEdit />;
        },
      },
    ];
  else
    tabs = [
      {
        title: "Properties",
        content: () => {
          return <GenericUIAddEdit />;
        },
      },
    ];

  return tabs;
};
export default getTabsForEntity;
