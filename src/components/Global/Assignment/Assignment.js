import React from "react";
import { Button, Card, CardBody, Col, Container, Row } from "reactstrap";
import Groups from "./Groups";
import Topics from "./Topics";
import Questions from "./Questions";
import { TYPES } from "./assignmentReducer";
import { backendUrl } from "config/constants";
import TeacherFeedback from "./TeacherFeedback";
import { useAssignment } from "./useAssignment";
import AssignmentQuestionNavigation from "./AssignmentQuestionNavigation";
import InvalidBrowser from "./InvalidBrowser";
import AssignmentFinalMark from "./AssignmentFinalMark";
import TeacherFinalMark from "./TeacherFinalMark";
import AssignmentTotalMark from "./AssignmentTotalMark";
import QuestionsTree from "./QuestionsTree";
import QuestionsMultiCorrect from "./QuestionsMultiCorrect";
import { post, put } from "config/api";
import { translate } from "../../../utils/translate";
import Helper from "../RComs/Helper";
import Loader from "utils/Loader";

const Assignment = ({ mode, rabId }) => {
  const assignment = useAssignment(mode, rabId);

  const {
    lessonGroups,
    isLoader,
    sebBrowserViolation,
    dispatch,
    assignmentState,
  } = assignment;

  const {
    currentActiveGroup,
    currentActiveTopic,
    currentActiveQuestion,
    nextButtonDisabled,
    previousButtonDisabled,
    skipButtonDisabled,
    unSkipButtonDisabled,
    currentSkip,
    skippedTopics,
    currentGroupSkip,
    StudentQsPatternId,
    Details,
    assignmentReadMode,
    Feedback,
    TotalMark,
    questionsMarks,
    TeacherMark,
    questionsMultiMarks,
    studentsTotalMarks,
    teacherMultiMarks,
    multiFeedbacks,
  } = assignmentState;

  //Used In AssignmentQuestionNavigation
  const saveAssignmentDataHandler = async () => {
    const result = confirm(translate("Are You sure you want to submit"));
    if (result) {
      let url = `${backendUrl}std-api/question-set`;
      try {
        const response = await post(url, {
          payload: { StudentQsPatternId, RabId: +rabId, Details },
        });
        if (response.data.status == 1) {
          alert(translate("Submitted Successfully"));
          window.close();
        } else {
          alert(response.data.msg);
        }
      } catch (error) {}
    }
  };

  //Used In AssignmentQuestionNavigation
  const saveTeacherMarksHandler = async () => {
    const teacherMark = questionsMarks.reduce((accumulator, question) => {
      return accumulator + question.Mark;
    }, 0);

    const result = confirm(translate("Are You sure you want to submit"));

    try {
      if (result) {
        if (mode === "multi") {
          let students = {};
          questionsMultiMarks.map((s) => {
            if (!students[s.studentId]) {
              students[s.studentId] = {
                student_qs_pattern_id: s.studentQuestionPatternId,
                questions: [{ questionId: s.questionId, questionMark: s.Mark }],
              };
            } else {
              students[s.studentId] = {
                student_qs_pattern_id: s.studentQuestionPatternId,
                questions: [
                  ...students[s.studentId].questions,
                  { questionId: s.questionId, questionMark: s.Mark },
                ],
              };
            }
          });

          if (multiFeedbacks.length > 0) {
            multiFeedbacks.map((f) => {
              students[f.studentId] = {
                ...students[f.studentId],
                feedback: f.feedback,
              };
            });
          }

          if (studentsTotalMarks.length > 0) {
            studentsTotalMarks.map((s) => {
              students[s.studentId] = {
                ...students[s.studentId],
                totalMark: s.totalMark,
              };
            });
          }

          if (teacherMultiMarks.length > 0) {
            teacherMultiMarks.map((s) => {
              students[s.studentId] = {
                ...students[s.studentId],
                teacher_mark: s.finalMark,
              };
            });
          }

          let details = Object.entries(students).map((s) => ({
            student_id: +s[0],
            ...s[1],
          }));

          const response = await put(
            `${backendUrl}tch-api/rab/${rabId}/correct_single_qs`,
            {
              payload: { RabId: +rabId, students: details },
            }
          );
          if (response.data.status == 1) {
            alert(translate("Submitted Successfully"));
            window.close();
          } else {
            alert(response.data.msg);
          }
        } else {
          const response = await put(
            `${backendUrl}tch-api/rab/${rabId}/std-qs-pattern/${StudentQsPatternId}/correct`,
            {
              payload: {
                Feedback,
                TotalMark,
                TeacherMark,
                QuestionSet: questionsMarks,
              },
            }
          );
          if (response.data.status == 1) {
            alert(translate("Submitted Successfully"));
            window.close();
          } else {
            alert(response.data.msg);
          }
        }
      }
    } catch (error) {}
  };

  //Used In AssignmentQuestionNavigation
  const isLastQuestion = () => {
    return (
      currentActiveGroup === lessonGroups?.inst_pattern_groups.length - 1 &&
      currentActiveQuestion ===
        lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
          currentActiveTopic
        ].inst_questions.length -
          1 &&
      currentActiveTopic ===
        lessonGroups?.inst_pattern_groups[
          lessonGroups?.inst_pattern_groups.length - 1
        ].inst_topics.length -
          1
    );
  };

  const clickSkipButtonHandler = () => {
    const currentTopic =
      lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
        currentActiveTopic
      ];
    const currentGroup = lessonGroups?.inst_pattern_groups[currentActiveGroup];

    if (currentSkip === 0) return;
    if (
      lessonGroups?.inst_pattern_groups.length - 1 === currentActiveGroup &&
      currentGroup.inst_topics.length - 1 === currentActiveTopic
    ) {
      dispatch({
        type: TYPES.SKIP_LAST_GROUP,
        payload: {
          currentTopicId: currentTopic.id,
          currentSkip,
          currentGroupId: currentGroup.id,
        },
      });
      return;
    }
    if (currentGroup.inst_topics.length - 1 === currentActiveTopic) {
      dispatch({
        type: TYPES.SKIP_LAST_TOPIC,
        payload: {
          currentTopicId: currentTopic.id,
          currentSkip,
          currentGroupId: currentGroup.id,
        },
      });
      return;
    }
    dispatch({
      type: TYPES.SKIP_TOPIC,
      payload: {
        currentTopicId: currentTopic.id,
        currentSkip,
        currentGroupId: currentGroup.id,
      },
    });
  };

  const clickUnSkipButtonHandler = () => {
    if (currentSkip === currentGroupSkip) return;

    const currentTopic =
      lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
        currentActiveTopic
      ];
    const currentGroup = lessonGroups?.inst_pattern_groups[currentActiveGroup];
    if (
      lessonGroups?.inst_pattern_groups.length - 1 === currentActiveGroup &&
      currentGroup.inst_topics.length - 1 === currentActiveTopic
    ) {
      dispatch({
        type: TYPES.UN_SKIP_LAST_GROUP,
        payload: {
          currentTopicId: currentTopic.id,
          currentSkip,
          currentGroupId: currentGroup.id,
        },
      });
      return;
    }
    dispatch({
      type: TYPES.UN_SKIP_TOPIC,
      payload: {
        currentTopicId: currentTopic.id,
        currentGroupId: currentGroup.id,
      },
    });
  };

  if (sebBrowserViolation) {
    return <InvalidBrowser msg={sebBrowserViolation} link={lessonGroups} />;
  }

  const containerStyle = {
    paddingRight: "15px",
    paddingLeft: "0px",
  };

  if (mode == "teacher") containerStyle.paddingTop = "66px";

  return isLoader ? (
    <Loader />
  ) : (
    <Container fluid className="mt-1 mb-2 " style={containerStyle} dir="ltr">
      <Row>
        <div className="assignmentsolvetree col-lg-3 col-md-5 col-sm-5">
          <QuestionsTree
            mode={mode}
            lessonGroups={lessonGroups}
            assignmentReadMode={assignmentReadMode}
            dispatch={dispatch}
            currentActiveTopic={currentActiveTopic}
            currentActiveGroup={currentActiveGroup}
            currentActiveQuestion={currentActiveQuestion}
            currentQuestion={
              lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
                currentActiveTopic
              ].inst_questions[currentActiveQuestion]
            }
            saveAssignmentDataHandler={saveAssignmentDataHandler}
            totalMark={TotalMark}
            rabName={lessonGroups?.course_name}
          />
        </div>

        <div className="AssignmentBody col-lg-9 col-md-7 col-sm-7 p-1 ">
          <Card>
            <CardBody>
              <Row className="row ">
                <div
                  className="col-12"
                  style={{ borderBottom: "solid 1px rgba(99, 95, 89, 0.4)" }}
                >
                  {lessonGroups && (
                    <Col
                      xs={11}
                      className=""
                      style={{
                        //  borderColor: "rgba(99,95,89,0.4)",
                        background: "none",
                      }}
                    >
                      <Groups
                        group={
                          lessonGroups?.inst_pattern_groups[currentActiveGroup]
                        }
                        currentActiveGroup={currentActiveGroup}
                        lessonGroups={lessonGroups}
                      />
                    </Col>
                  )}
                  {lessonGroups && (
                    <Col xs={10} className="assign-topic-text pr-0">
                      <Topics
                        topic={
                          lessonGroups?.inst_pattern_groups[currentActiveGroup]
                            .inst_topics[currentActiveTopic]
                        }
                        currentActiveTopic={currentActiveTopic}
                        groupLength={
                          lessonGroups?.inst_pattern_groups[currentActiveGroup]
                            .inst_topics.length
                        }
                      />
                    </Col>
                  )}
                </div>
              </Row>
              {!assignmentReadMode &&
                mode === "student" &&
                currentGroupSkip !== 0 && (
                  <Row className="d-flex pt-2 justify-content-center">
                    <Button
                      disabled={unSkipButtonDisabled}
                      onClick={clickUnSkipButtonHandler}
                      className="m-2 gu-assignment-next h_btn_inline_9"
                      variant="danger"
                    >
                      {translate("Unskip")}
                    </Button>

                    <Button
                      disabled={skipButtonDisabled}
                      onClick={clickSkipButtonHandler}
                      className="m-2 gu-assignment-next h_btn_inline_9 "
                      variant="danger"
                      a
                    >
                      {translate("Skip")}
                    </Button>
                  </Row>
                )}

              <AssignmentQuestionNavigation
                previousButtonDisabled={previousButtonDisabled}
                nextButtonDisabled={nextButtonDisabled}
                mode={mode}
                assignmentReadMode={assignmentReadMode}
                currentActiveGroup={currentActiveGroup}
                currentActiveQuestion={currentActiveQuestion}
                currentActiveTopic={currentActiveTopic}
                lessonGroups={lessonGroups}
                dispatch={dispatch}
                saveAssignmentDataHandler={saveAssignmentDataHandler}
                saveTeacherMarksHandler={saveTeacherMarksHandler}
                isLastQuestion={isLastQuestion}
              />
              {lessonGroups &&
                (mode !== "multi" ? (
                  <Questions
                    assignmentState={assignmentState}
                    assignmentReadMode={assignmentReadMode}
                    groupId={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup].id
                    }
                    question={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup]
                        .inst_topics[currentActiveTopic].inst_questions[
                        currentActiveQuestion
                      ]
                    }
                    dispatch={dispatch}
                    mode={mode}
                    questionsMarks={questionsMarks}
                    currentActiveQuestion={currentActiveQuestion}
                    topicLength={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup]
                        .inst_topics[currentActiveTopic].inst_questions.length
                    }
                  />
                ) : (
                  <QuestionsMultiCorrect
                    lessonGroups={lessonGroups}
                    assignmentState={assignmentState}
                    assignmentReadMode={assignmentReadMode}
                    groupId={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup].id
                    }
                    question={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup]
                        .inst_topics[currentActiveTopic].inst_questions[
                        currentActiveQuestion
                      ]
                    }
                    dispatch={dispatch}
                    mode={mode}
                    questionsMultiMarks={questionsMultiMarks}
                    currentActiveQuestion={currentActiveQuestion}
                    topicLength={
                      lessonGroups?.inst_pattern_groups[currentActiveGroup]
                        .inst_topics[currentActiveTopic].inst_questions.length
                    }
                    studentsTotalMarks={studentsTotalMarks}
                    isLastQuestion={isLastQuestion}
                    teacherMultiMarks={teacherMultiMarks}
                    multiFeedbacks={multiFeedbacks}
                  />
                ))}

              <AssignmentQuestionNavigation
                previousButtonDisabled={previousButtonDisabled}
                nextButtonDisabled={nextButtonDisabled}
                mode={mode}
                assignmentReadMode={assignmentReadMode}
                currentActiveGroup={currentActiveGroup}
                currentActiveQuestion={currentActiveQuestion}
                currentActiveTopic={currentActiveTopic}
                lessonGroups={lessonGroups}
                dispatch={dispatch}
                saveTeacherMarksHandler={saveTeacherMarksHandler}
                saveAssignmentDataHandler={saveAssignmentDataHandler}
                isLastQuestion={isLastQuestion}
              />

              {((assignmentReadMode && mode !== "multi") ||
                mode === "teacher") && (
                <div
                  className="col-12 "
                  style={{ borderTop: "solid 1px rgba(99, 95, 89, 0.4)" }}
                >
                  <Col xs={10} className="assign-teacher-feedback">
                    <TeacherFeedback
                      dispatch={dispatch}
                      mode={mode}
                      feedback={lessonGroups?.feedback}
                      teacherFeedback={Feedback}
                    />
                  </Col>
                </div>
              )}

              {assignmentReadMode && mode === "student" && (
                <Col xs={10} className="assign-final-mark ">
                  <AssignmentFinalMark
                    literalTeacherMark={lessonGroups?.literal_teacher_mark}
                  />
                </Col>
              )}

              {assignmentReadMode && mode === "teacher" && (
                <Col xs={10} className="mx-auto">
                  <AssignmentTotalMark TotalMark={TotalMark} />
                </Col>
              )}

              {assignmentReadMode && mode === "teacher" && isLastQuestion() && (
                <Col xs={10} className="mx-auto">
                  <TeacherFinalMark
                    TeacherMark={TeacherMark}
                    dispatch={dispatch}
                    mode={mode}
                  />
                </Col>
              )}
            </CardBody>
          </Card>
        </div>
      </Row>
    </Container>
  );
};

export default Assignment;
