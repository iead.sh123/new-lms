import React, { useContext } from "react";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import { primaryColor } from "config/constants";
import RUnitPlanSectionForm from "./RUnitPlanSectionForm";
import RLessonPlanAddButton from "view/LessonPlan/RLessonPlanItem/RLessonPlanAddButton";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUnitPlanSections = () => {
	const UnitPlanContextData = useContext(UnitPlanContext);

	return (
		<div>
			{UnitPlanContextData.unitPlan &&
			UnitPlanContextData.unitPlan.uplsections &&
			UnitPlanContextData.unitPlan.uplsections !== null &&
			UnitPlanContextData.unitPlan.uplsections.length > 0 ? (
				<RUnitPlanSectionForm />
			) : (
				<>
					<RLessonPlanAddButton handleAddNewLessonPlanItems={UnitPlanContextData.handleAddNewSections} />
					<RFlex styleProps={{ justifyContent: "center" }}>
						<p>{tr`no_items_yet`}</p>
					</RFlex>

					{/* <RFlex
						styleProps={{
							justifyContent: "center",

							marginBottom: "40px",
						}}
					>
						<p>{tr`click_on_the`}</p>
						<div className={styles.addition_container_plus} style={{ background: primaryColor }}>
							<i className={`fa fa-plus  ${styles.addition_icon_plus}`} />
						</div>
						<p>{tr`to_start_creating_sections`}</p>
					</RFlex> */}
				</>
			)}
		</div>
	);
};

export default RUnitPlanSections;
