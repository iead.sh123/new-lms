import React from "react";
import CollaborateNow from "./CollaborateNow";
import styles from "../../Collaboration.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const CollaborationSettingCard = ({ item, handlers, data }) => {
	return (
		<RFlex
			className={`${styles.rectangle__card} ${
				Object.keys(data?.values.contentShareableIds).length > 0
					? data?.values.contentShareableIds[item.id]?.need_approval
						? styles.border__success
						: styles.border__primary
					: styles.border__primary
			}`}
		>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<span style={{ fontWeight: "bold" }}>{item.name}</span>
				<span>
					{tr`all`} : {item?.counts?.all}
				</span>
			</RFlex>
			<CollaborateNow item={item} handlers={handlers} data={data} />
		</RFlex>
	);
};

export default CollaborationSettingCard;
