import React from "react";
import RDropzoneComponent from "../RComs/RDropzoneComponent";
import Helper from "./Helper";

function RFileUploader(props) {
	return (
		<RDropzoneComponent
			parentCallback={props.parentCallback}
			singleFile={props.singleFile}
			placeholder={props.placeholder}
			sizeFile={props.fileSize}
			typeFile={props.typeFile}
			removeButton={props.removeButton}
			value={props.value}
			uploadName={props.uploadName}
			setSpecificAttachment={props.setSpecificAttachment}
			theme={props.theme}
			disabled={props.disabled}
			binary={props.binary}
			callApiAfterUploadImage={props.callApiAfterUploadImage}
		/>
	);
}

export default RFileUploader;
