import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RGrading.css'

function RStudentTypeGrades({typesGrades}){
 
    return(
        <div className="type_Grades">
      
        {
         typesGrades.map(t=>
         
          <Row className="type_row"> 
             
          <Col  xs lg="4" className="type_title first_column">
             <span>  {t.type}</span> 
           </Col>

          <Col  xs lg="2" className="">
          <Row className="type_title"><span>Mark</span> </Row> 
         <Row className="type_details"><span> {t.Mark} </span></Row>
        
          </Col>

          <Col  xs lg="2" className="">
          <Row className="type_title"><span>submitting Date</span> </Row> 
         <Row className="type_details"><span>{t.submittingDate}</span>  </Row>
         
          </Col>

          <Col  xs lg="2" className="">
          <Row className="type_title"> <span>letter Mark</span></Row> 
         <Row className="type_details"> <span>{t.letterMark}</span></Row> 
          </Col>

          <Col  xs lg="2" className="">
          <Row className="type_title"> <span>teacher Mark</span></Row> 
         <Row className="type_details"><input type="text"/> </Row> 
          </Col>
        </Row>
       
          )
        } 
        
        </div>
    )

}

export default RStudentTypeGrades;


