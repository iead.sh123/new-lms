import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import { useHistory } from "react-router-dom";
import { get } from "config/api";
import { useSelector, useDispatch } from "react-redux";
import { setTitle } from "store/actions/student/navActions";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";
import styles from "./Abuse.Module.scss";

const Abuse = ({ dir }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [processsedRecords, setProcessedRecords] = useState([]);

  const { user } = useSelector((state) => state?.auth);

  const getDataFromBackend = async (specific_url) => {
    const url = `api/abuse-reports/all/user`;
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };

  const setData = (response) => {
    let processsedRecords1 = [];

    response?.data?.data?.data.map((r) => {
      let record = {};
      record.allTitle = r?.creation_date;
      record.title = r?.description;
      record.table_name = "abuse";

      record.details = [
        user?.type !== "parent" && { key: tr`sender`, value: r?.sender_name },

        { key: tr`report`, value: r?.description },
        { key: tr`date`, value: r?.creation_date },
        user?.type !== "parent" && {
          key: tr`seen`,
          value: r?.seen,
        },
      ];
      record.actions = [
        {
          name: tr`view`,
          icon: "fa fa-eye",
          color: "info",

          onClick: () => {
            user?.type === "teacher"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/teacher/abuse/${r?.id}`
                )
              : user?.type === "employee"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/admin/abuse/${r?.id}`
                )
              : user?.type === "school_advisor" ||
                user?.type === "seniorteacher" ||
                user?.type === "principal"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/senior-teacher/abuse/${r?.id}`
                )
              : user?.type === "parent"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/parent/abuse/${r?.id}`
                )
              : "";
          },
        },
        {
          name: tr`print`,
          icon: "fa fa-print",
          color: "info",

          onClick: () => {
            user?.type === "teacher"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/teacher/abuse/print/${r?.id}`
                )
              : user?.type === "employee"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/admin/abuse/print/${r?.id}`
                )
              : user?.type === "school_advisor" ||
                user?.type === "seniorteacher" ||
                user?.type === "principal"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/senior-teacher/abuse/print/${r?.id}`
                )
              : user?.type === "parent"
              ? history.push(
                  `${process.env.REACT_APP_BASE_URL}/parent/abuse/print/${r?.id}`
                )
              : "";
          },
        },
      ];

      processsedRecords1.push(record);
    });
    setProcessedRecords(processsedRecords1);
  };

  useEffect(() => {
    dispatch(setTitle(tr("abuse_reports")));
  }, []);

  const flag = true;

  return (
    <div
      className={
        !user?.useNewTheme && user?.type == "parent" ? "container" : "content"
      }
      style={{
        paddingTop: !user?.useNewTheme && user?.type == "parent" && "18vh",
      }}
    >
      {user?.type == "parent" && (
        <Row>
          <Col xs={12} className={dir ? "" : styles.button_rtl}>
            <RButton
              text={tr`add_abuse_report`}
              onClick={() =>
                history.push(
                  `${process.env.REACT_APP_BASE_URL}/parent/abuse/add`
                )
              }
              color="info"
              outline
            />
          </Col>
        </Row>
      )}

      {user?.type !== "parent" && (
        <h4
          style={{ fontWeight: "bold", textAlign: "center" }}
        >{tr`abuse_reports`}</h4>
      )}
      <RAdvancedLister
        getDataFromBackend={getDataFromBackend}
        setData={setData}
        records={processsedRecords}
        getDataObject={(response) => response.data.data}
        characterCount={15}
        marginT={"mt-3"}
        marginB={"mb-2"}
        showListerMode="tableLister"
      ></RAdvancedLister>
    </div>
  );
};

export default withDirection(Abuse);
