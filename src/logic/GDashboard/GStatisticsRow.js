import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';


import { useSelector } from 'react-redux';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText,InputGroup ,InputGroupAddon,Input  } from 'reactstrap';
import DashboardStatisticsCard from './DashboardStatisticsCard';
import Helper from 'components/Global/RComs/Helper';


const GStatisticsRow = ({ data, color ,size=30 }) => {

  const statisticsMap={
  
  chats:{color:"#AF90E6",icon:"fa fa-message"},
  community:{color:"#AF90E6",icon:"fa fa-users"},
  courses:{color:"#93E078",icon:"fa fa-folder-plus"},
  calendar:{color:"#F3E0E8",icon:"fa fa-calendar-alt"},
  collaboration:{color:"#9BD13F",icon:"fa fa-globe"},
  terms:{color:"#EEBF64",icon:"fas fa-mortar-board"},
}
return    <Row   style={{marginTop:"30px",marginBottom:"30px"}} >
<Col xs="12" md="12" style={{display:"flex",flexDirection:"row"}}>
{/* "statistics": {
          "community": {
              "type": "",
              "title": "Community",
              "value": "0 Posts"
          },
          "courses": {
              "type": "",
              "title": "Courses",
              "value": "0 New Courses"
          },
          "collaboration": {
              "type": "",
              "": "Collaboration",
              "value": "0 Contents"
          },
          "terms": {
              "type": "",
              "title": "Terms",
              "value": "0 Actions"
          }
      } */}



  {data&&Object.entries(data)?.map(c=>
  {
    const [k,v]=c;
    return <>
  
      {/* {Helper.js(v)} */}
      {/* {statisticsMap[v.type]?.color}
      {statisticsMap[v.type]?.icon} */}
       <DashboardStatisticsCard 
        title={v.title} 
        description={v.value} 
        color={statisticsMap[v.type]?.color} 
        icon={statisticsMap[v.type]?.icon}  />
        </>} )} 

</Col>
</Row>
}

export default GStatisticsRow;