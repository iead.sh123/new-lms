/* eslint-disable react/prop-types */
import tr from "../RComs/RTranslator";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";

const SchoolClassFilter = ({
  schoolClasses,
  setSelectedSchoolClasses,
  selectedSchoolClasses,
}) => {
  const handleSelectChanged = (e) => {
    setSelectedSchoolClasses(e);
  };
  return (
    <ReactMultiSelectCheckboxes
      onChange={(e) => {
        handleSelectChanged(e);
      }}
      placeholderButtonLabel={tr`school_classes`}
      options={schoolClasses ? schoolClasses : []}
      defaultValue={selectedSchoolClasses ? selectedSchoolClasses : []}
    />
  );
};

export default SchoolClassFilter;
