import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAccessLevels } from "store/actions/global/socialMediaService.actions";
import {
  FetchUserTypesAsync,
  AddEditAnnouncementAsync,
  AddEditAndPublishAnnouncementAsync,
  SingleAnnouncementAsync,
} from "store/actions/admin/schoolEvents";
import RSelect from "../RSelect";
import { Form, FormGroup, Input, Row, Col, Label, Button } from "reactstrap";
import tr from "../RTranslator";
import Loader from "components/Global/Loader";
import { useHistory, useParams } from "react-router-dom";
import RFileUploader from "../RFileUploader";
import ShowAnnouncementImage from "./ShowAnnouncementImage";
import withDirection from "hocs/withDirection";

const AddAnnouncement = ({ dir }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { announcibleID, schoolEventID, iDHomeRoom, announcementID } =
    useParams();

  const [optionsValueAccessLeveL, setOptionsValueAccessLevel] = useState([]);
  const [optionsValueUserTypes, setOptionsValueUserTypes] = useState([]);

  const [formGroup, setFormGroup] = useState({
    title: "",
    text: "",
    accessibility_level_id: null,
    user_types: null,
    announcible_type:
      user?.type == "teacher"
        ? "home_rooms"
        : user?.type == "employee" && !announcibleID
        ? ""
        : "school_events",

    announcible_id:
      user?.type == "teacher"
        ? iDHomeRoom
        : user?.type == "employee" && !announcibleID
        ? ""
        : announcibleID,
    Attachments: [],
  });

  const { user } = useSelector((state) => state?.auth);
  const { accesslevels, accessLevelLoader } = useSelector(
    (state) => state.socialMediaServiceReducer
  );

  const {
    userTypes,
    announcement,
    userTypesLoading,
    addEditAnnouncementLoading,
    addEditAndPublishAnnouncementLoading,
    editAnnouncementLoading,
  } = useSelector((state) => state.SchoolEventsRed);

  const handleChange = (e) => {
    setFormGroup({
      ...formGroup,
      [e.target.name]: e.target.value,
    });
  };

  //Handle Select UserTypes
  const handleSelectUserTypes = (e) => {
    const ArraySelected = e;
    let ArraySelectedTwo = [];
    for (let i = 0; i < ArraySelected.length; i++) {
      ArraySelectedTwo.push({
        value: ArraySelected[i].value,
        label: ArraySelected[i].label,
      });
    }
    setFormGroup({
      ...formGroup,
      user_types: ArraySelectedTwo,
    });
  };

  //Handle Select AccessLevel
  const handleSelectAccessLevel = (e) => {
    setFormGroup({
      ...formGroup,
      accessibility_level_id: e.value,
    });

    dispatch(FetchUserTypesAsync(e.value));
  };

  //Option AccessLevel
  const getOptionsAccessLevel = () => {
    const arrayDataAccessLevel = [];
    accesslevels &&
      accesslevels.models &&
      accesslevels.models.map((accessLevel) => {
        arrayDataAccessLevel.push({
          value: accessLevel.id,
          label: accessLevel.level_name,
        });
      });
    setOptionsValueAccessLevel(arrayDataAccessLevel);
  };

  //Option UserTypes
  const getOptionsUserTypes = () => {
    const arrayDataUserTypes = [];
    userTypes &&
      userTypes.map((userType) => {
        arrayDataUserTypes.push({
          value: userType.id,
          label: userType.name,
        });
      });

    setOptionsValueUserTypes(arrayDataUserTypes);
  };

  useEffect(() => {
    getOptionsAccessLevel();
  }, [accesslevels]);

  useEffect(() => {
    getOptionsUserTypes();
  }, [userTypes]);

  useEffect(() => {
    if (iDHomeRoom) {
      dispatch(getAccessLevels("home_rooms", iDHomeRoom));
    } else if (schoolEventID) {
      dispatch(getAccessLevels("school_events"));
    } else {
      dispatch(getAccessLevels("announcements"));
    }
  }, []);

  //upload new image
  const handleCallback = (childData) => {
    let listImgAdded = [];
    childData.map((item) => {
      listImgAdded.push({
        url: item.url,
        file_name: item.file_name,
        type: item.type,
      });
    });
    setFormGroup({ ...formGroup, Attachments: listImgAdded });
  };

  const handleSave = (e) => {
    e.preventDefault();

    const allImages = [
      ...formGroup.Attachments,
      ...(announcement?.content?.resource ?? []),
    ];

    let request = {
      payload: {
        id: announcementID,
        title: formGroup.title ? formGroup.title : announcement?.title,
        text: formGroup.text ? formGroup.text : announcement?.content?.text,
        accessibility_level_id: formGroup.accessibility_level_id
          ? formGroup.accessibility_level_id
          : announcement.accessibility_level_id,
        user_types: formGroup.user_types
          ? formGroup.user_types
          : announcement.user_types,

        announcible_type:
          user?.type == "teacher"
            ? "home_rooms"
            : user?.type == "employee" && !schoolEventID
            ? ""
            : "school_events",
        announcible_id:
          user?.type == "teacher"
            ? iDHomeRoom
            : user?.type == "employee" && !schoolEventID
            ? ""
            : schoolEventID,

        Attachments: allImages,
      },
    };

    dispatch(AddEditAnnouncementAsync(request, history));
  };

  const handleSaveAndPublish = (e) => {
    e.preventDefault();

    const allImages = [
      ...formGroup.Attachments,
      ...(announcement?.content?.resource ?? []),
    ];

    let request = {
      payload: {
        id: announcementID,
        isPublished: true,
        title: formGroup.title ? formGroup.title : announcement?.title,
        text: formGroup.text ? formGroup.text : announcement?.content?.text,
        accessibility_level_id: formGroup.accessibility_level_id
          ? formGroup.accessibility_level_id
          : announcement.accessibility_level_id,
        user_types: formGroup.user_types
          ? formGroup.user_types
          : announcement.user_types,
        announcible_type:
          user?.type == "teacher"
            ? "home_rooms"
            : user?.type == "employee" && !schoolEventID
            ? ""
            : "school_events",
        announcible_id:
          user?.type == "teacher"
            ? iDHomeRoom
            : user?.type == "employee" && !schoolEventID
            ? ""
            : schoolEventID,
        Attachments: allImages,
      },
    };
    e.preventDefault();
    dispatch(AddEditAndPublishAnnouncementAsync(request, history));
  };

  useEffect(() => {
    if (announcementID) {
      dispatch(SingleAnnouncementAsync(announcementID));
    }
  }, []);

  return (
    <div className="content">
      {(!announcementID ? accessLevelLoader : editAnnouncementLoading) ? (
        <Loader />
      ) : (
        <Form>
          <Row>
            <Col md={6}>
              <Label md="12" className={dir ? "" : styles.text_rtl}>
                {tr`title`}
              </Label>
              <FormGroup>
                <Input
                  type="textarea"
                  name="title"
                  defaultValue={announcementID && announcement?.title}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                  required={announcement ? false : true}
                />
                {/* tabIndex="1" */}
              </FormGroup>
            </Col>

            <Col md={6}>
              <Label md="12" className={dir ? "" : styles.text_rtl}>
                {" "}
                {tr`text`}
              </Label>
              <FormGroup>
                <Input
                  type="textarea"
                  name="text"
                  defaultValue={announcementID && announcement?.content?.text}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                  required={announcement ? false : true}
                  rows={0}
                  // tabIndex="2"
                />
              </FormGroup>
            </Col>

            <Col md={6}>
              <Label md="12" className={dir ? "" : styles.text_rtl}>
                {" "}
                {tr`select_announcement_level`}
              </Label>
              <RSelect
                option={optionsValueAccessLeveL}
                closeMenuOnSelect={true}
                placeholder={tr`select_announcement_level`}
                defaultValue={
                  announcementID && [
                    {
                      value: announcement.accessibility_level_id,
                      label: announcement.accessibility_level_name,
                    },
                  ]
                }
                onChange={(e) => handleSelectAccessLevel(e)}
                // tabIndex={null}
              />
            </Col>

            <Col md={6}>
              <Label md="12" className={dir ? "" : styles.text_rtl}>
                {" "}
                {tr`who_can_see_the_announcement`}
              </Label>

              {userTypesLoading ? (
                <h6>Loading ...</h6>
              ) : (
                <RSelect
                  option={optionsValueUserTypes}
                  closeMenuOnSelect={true}
                  placeholder={tr`select_user_types`}
                  onChange={(e) => handleSelectUserTypes(e)}
                  defaultValue={announcementID && announcement.user_types}
                  isMulti
                  // tabIndex={null}
                />
              )}
            </Col>

            <Col xs={12} style={{ margin: "15px 15px" }}>
              {announcement?.content?.resource.length !== 0 && (
                <ShowAnnouncementImage
                  images={
                    announcementID &&
                    announcement.content &&
                    announcement.content.resource
                      ? announcement.content.resource
                      : []
                  }
                />
              )}
            </Col>

            <Col xs={12} style={{ display: "flex", justifyContent: "center" }}>
              <RFileUploader
                parentCallback={handleCallback}
                placeholder={tr`Replace the old selected file`}
              />
            </Col>

            <Col
              xs={12}
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <Button
                block
                className="btn-round mb-3 "
                color="warning"
                disabled={addEditAnnouncementLoading ? true : false}
                style={{ width: 400 }}
                type="submit"
                onClick={(e) => handleSave(e)}
              >
                {addEditAnnouncementLoading ? (
                  <>
                    {announcementID ? tr`edit` : tr`save`}{" "}
                    <i className="fa fa-refresh fa-spin"></i>
                  </>
                ) : announcementID ? (
                  tr("edit")
                ) : (
                  tr("save")
                )}
              </Button>
              <Button
                block
                className="btn-round mb-3 "
                color="warning"
                disabled={addEditAndPublishAnnouncementLoading ? true : false}
                style={{ width: 400 }}
                type="submit"
                onClick={(e) => handleSaveAndPublish(e)}
              >
                {addEditAndPublishAnnouncementLoading ? (
                  <>
                    {tr`save_and_publish`}{" "}
                    <i className="fa fa-refresh fa-spin"></i>
                  </>
                ) : (
                  tr("save_and_publish")
                )}
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </div>
  );
};

export default withDirection(AddAnnouncement);
