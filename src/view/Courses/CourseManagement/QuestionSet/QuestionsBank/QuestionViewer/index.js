import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import RQuestionViewer from "./RQuestionViewer";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import RDetails from "./RDetails";

const RQuestionTabsViewer = () => {
  const QuestionsBankData = useContext(QuestionsBankContext);

  return (
    <section>
      <RFilterTabs
        tabs={QuestionsBankData.Tabs}
        setTabActive={QuestionsBankData.setTabActive}
      />
      {QuestionsBankData.tabActive == "question" ? (
        <RQuestionViewer />
      ) : (
        <RDetails />
      )}
    </section>
  );
};

export default RQuestionTabsViewer;
