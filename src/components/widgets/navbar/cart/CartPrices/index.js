import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../cart.module.scss";

const CartPrices = ({ prices }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<span className={styles.prices}>{tr`total`}</span>
				<RFlex styleProps={{ gap: 2 }}>
					<span>{prices?.currency}</span>
					<span>{prices?.totalAmount}</span>
				</RFlex>
			</RFlex>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<span>{tr`discount_amount`}</span>
				<span>{prices?.couponValue == 0 ? tr`no_discount` : prices?.couponValue}</span>
			</RFlex>
			<div className={styles.divider} />
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<span style={{ fontWeight: 700 }}>{tr`final_price`}</span>
				<RFlex styleProps={{ gap: 2, fontWeight: 700 }}>
					<span>{prices?.currency}</span>
					<span>{prices?.finalPrice}</span>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default CartPrices;
