import React, { useContext } from "react";
import { SchoolInitiationContext } from "./GSchoolInitiation";
import { useParams } from "react-router-dom";
import RSchoolInitiationHeader from "view/SchoolInitiation/RSchoolInitiationHeader";
import RSchoolInitiationModal from "view/SchoolInitiation/Modals/RSchoolInitiationModal";

const GSchoolInitiationHeader = ({
  handleChangeSearch,
  handleSearch,
  searchLoading,
  searchData,
  setSearchData,
}) => {
  const { sIType, sITypeId, tabTitle } = useParams();
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  return (
    <>
      {SchoolInitiationData.show && <RSchoolInitiationModal />}

      {(tabTitle == "grade_levels" || tabTitle == "curricula" || !tabTitle) && (
        <RSchoolInitiationHeader
          handleChangeSearch={handleChangeSearch}
          handleSearch={handleSearch}
          searchLoading={searchLoading}
          searchData={searchData}
          setSearchData={setSearchData}
        />
      )}
    </>
  );
};

export default GSchoolInitiationHeader;
