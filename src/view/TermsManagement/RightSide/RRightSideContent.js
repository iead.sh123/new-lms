import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RTermWizard from "./RTermWizard";
import RTermStatus from "./RTermStatus";
import GGradeLevelContent from "logic/TermsManagement/GGradeLevelContent";
import GWeeklySchedule from "logic/TermsManagement/GWeeklySchedule";
import GHomeRoomUsers from "logic/TermsManagement/GHomeRoomUsers";
import GCourseUsers from "logic/TermsManagement/GCourseUsers";

const RRightSideContent = ({ academicPath }) => {
	const TermsManagementData = useContext(TermsManagementContext);

	return (
		<RFlex styleProps={{ flexDirection: "column", width: "100%" }}>
			{TermsManagementData.activeNode == "term" && <RTermStatus />}
			{TermsManagementData.activeNode == "gradeLevel" && <GGradeLevelContent />}
			{TermsManagementData.activeNode == "homeRoom" && <GHomeRoomUsers />}
			{TermsManagementData.activeNode == "course" && <GCourseUsers />}
			{TermsManagementData.activeNode == "weeklySchedule" && <GWeeklySchedule />}
			{!TermsManagementData.activeGradeLevelInWizard && <RTermWizard academicPath={academicPath} />}
			{TermsManagementData.activeNode == "academicYears" && TermsManagementData.activeGradeLevelInWizard && ""}
		</RFlex>
	);
};

export default RRightSideContent;
