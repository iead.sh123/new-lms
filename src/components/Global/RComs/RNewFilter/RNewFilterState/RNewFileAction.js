import { ADD_NEW_FILTER_ON_LISTER } from "store/actions/admin/adminType";

export const addNewFilterOnLister = (data) => {
  return (dispatch) => {
    dispatch({
      type: ADD_NEW_FILTER_ON_LISTER,
      data,
    });
  };
};
