import { get, post } from "config/api";
import { Services } from "engine/services";

export const trackApi = {
  getAllCards: () => get(`api/dashboard/dynamic-cards`),
  cardTrackLastSeen: (table_name) =>
    post(`api/tracking/set-trackable-lastseen`, table_name),
  trackRecords: (data) => post(`api/tracking/set-trackable-event`, data),

  //-----------------------Services---------------------------
  userInteractions: (learningObjectId, data) =>
    post(
      `${Services.scorm.backend}api/learning-objects/${learningObjectId}/interactions`,
      data
    ),

  userLogs: (data) => post(`${Services.logs.backend}api/log/user/store`, data),
};
