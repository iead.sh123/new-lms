import React from "react";
import styles from "./RCardUserFeedback.module.scss";
import RFlex from "../RFlex/RFlex";
import Chemistry from "assets/img/new/Chemistry.jpg";
import tr from "../RTranslator";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import { Services } from "engine/services";
import { relativeDate } from "utils/dateUtil";
import { Rating } from "react-simple-star-rating";

const RCardUserFeedback = ({ feedback }) => {
  return (
    <div className={styles.user_feed_card}>
      <RFlex
        className={"mb-4"}
        styleProps={{ justifyContent: "space-between" }}
      >
        <RFlex>
          <img
            className={styles.image}
            src={
              feedback?.user?.image_hash_id == undefined
                ? Chemistry
                : `${Services.courses_manager.file}${feedback?.user?.image_hash_id}`
            }
            alt={feedback?.user?.image_hash_id}
          />
          <h6 className="d-flex align-items-center">
            {feedback?.user?.full_name}
          </h6>
        </RFlex>

        <p className="text-muted d-flex align-items-center pt-2">
          {relativeDate(feedback?.created_at)}
        </p>
      </RFlex>
      <RFlex className={"mb-4"}>
        <p>{feedback?.feedback.slice(0, 200)}</p>
      </RFlex>
      <RFlex>
        <Rating
          initialValue={feedback?.rating}
          // iconsCount={1}
          readonly={true}
          size={25}
        />
      </RFlex>
    </div>
  );
};

export default RCardUserFeedback;
