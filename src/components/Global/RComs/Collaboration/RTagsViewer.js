import React from "react";
import "./RCourseCard.css";
import RTagViewer from "./RTagViewer";

const RTagsViewer = ({ tags }) => {
  return (
    <div className="course_tags">
      {tags && tags.length ? (
        tags.map((i) => (
          <>
            {i.name && (
              <RTagViewer
                name={i.name}
                iconColor={i.iconColor}
                backgroundColor={i.backgroundColor}
                textColor={i.textColor}
              />
            )}
          </>
        ))
      ) : (
        <></>
      )}
    </div>
  );
};
export default RTagsViewer;
