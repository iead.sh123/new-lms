import React from "react";
import RCollapsible from "./RCollapsible";

function RAccordion({ items }) {
  return (
    <div style={{zIndex:'0'}}>
      {items.map((item, index) => (
        <RCollapsible
          key={index}
          buttonLabel={item.buttonLabel}
          disabled={item.disabled}
          navbar={item.navbar}
          icon={item.icon}
          color={item.color}
        >
          {item.content}
        </RCollapsible>
      ))}
    </div>
  );
}

export default RAccordion;