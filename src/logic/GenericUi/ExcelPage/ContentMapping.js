import React, { useEffect } from "react";
import { Row, Col, Button, Table } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  initialResponse,
  mappingData,
  saveFile,
} from "store/actions/admin/uploadFile.Action";
import Select from "react-select";
import tr from "components/Global/RComs/RTranslator";

const ContentMapping = ({
  data,
  master_table,
  master_id,
  detail_table,
  with_header,
  setShowExcelPage,
}) => {
  const { initRes, loading, successSave } = useSelector(
    (state) => state.UploadFileRed
  );

  const dispatch = useDispatch();

  useEffect(() => {
    let Array = [];
    {
      data?.file_headers?.map((header, index) =>
        Array.push({ id: index, column_name: header, table_field: "" })
      );
    }
    dispatch(initialResponse(Array));
  }, []);

  if (successSave) {
    setShowExcelPage(false);
  }
  return (
    <Row>
      <Col xs={12}>
        {" "}
        <Table responsive>
          <thead style={{ backgroundColor: "#A1A1A1", fontWeight: "bold" }}>
            <tr style={{ textAlign: "center" }}>
              <td>{tr`Headers`}</td>
              <td>{tr`Fields`}</td>
            </tr>
          </thead>
          <tbody>
            {data?.file_headers?.map((header, index) => (
              <tr>
                <td key={index} className="text-center w-25">
                  {header}
                </td>
                <td className="text-center w-50">
                  <Select
                    className="react-select primary"
                    classNamePrefix="react-select"
                    name="singleSelect"
                    onChange={(e) => dispatch(mappingData(e.value, index))}
                    options={data?.table_fields?.map((field) => ({
                      value: field.key,
                      label: field.value,
                    }))}
                    placeholder={tr`accessibility_level`}
                  ></Select>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Col>

      <Col xs={4}>
        <Button
          block
          color="info"
          type="submit"
          onClick={(e) => {
            dispatch(
              saveFile({
                payload: {
                  // master_table: "school_Class",
                  // master_id: 13,
                  // detail_table: "students",
                  // url: data.Url,
                  // pairs_of_headers: initRes,
                  // with_header: with_header,

                  master_table: master_table,
                  master_id: master_id,
                  detail_table: detail_table,
                  url: data.Url,
                  pairs_of_headers: initRes,
                  with_header: with_header,
                },
              })
            );
          }}
        >
          {loading ? (
            <>
              {tr`save`} <i className="fa fa-refresh fa-spin"></i>
            </>
          ) : (
            tr("save")
          )}
        </Button>
      </Col>
    </Row>
  );
};

export default ContentMapping;
