import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader';
import tr from 'components/Global/RComs/RTranslator';
import { Services } from 'engine/services';
import { enrollment_components } from 'logic/TermManagement/constants/enrolment.components.constant';
import React from 'react';
import { Table, Col } from 'reactstrap';
import Loader from 'utils/Loader';

const EnrollTeachersEditor = ({ teachers, handleLoadOrganizationTeachers, handleSearch, handleEnrollUsers }) => {

    const [searchText, setSearchText] = React.useState('');
    const [loadingTeachers, setLoadingTeachers] = React.useState(false);
    const debounceTimer = React.useRef(null);

    const loadTeachers = async (text) => {
        setLoadingTeachers(true);
        await handleLoadOrganizationTeachers(text);
        setLoadingTeachers(false);
    }

    React.useEffect(() => {
        loadTeachers();
    }, []);


    const debouncedLoadTeachers = (delay = 1000) => {
          const loadUsers = async () => {
            await loadTeachers(searchText);
          };
    
          return new Promise((resolve) => {
            clearTimeout(debounceTimer.current);
            debounceTimer.current = setTimeout(() => {
              resolve(loadUsers());
            }, delay);
          });
        }


        React.useEffect(()=>{
            debouncedLoadTeachers();
        } , [searchText]);



    return <React.Fragment>

        <Col md="4">
            <RSearchHeader
                searchData={searchText}
                handleSearch={() => null}
                setSearchData={setSearchText}
                handleChangeSearch={(val) => setSearchText(val)}
                buttonName={tr`search`}
                inputPlaceholder={tr`search`}
            />
        </Col>

        <Table bordered responsive>
            <thead style={{ backgroundColor: "#D7E3FD", fontWeight: "bold" }}>
                <tr style={{ textAlign: "center" }}>
                    <td>{tr`Name`}</td>
                    <td>{tr`Enrolled Classes`}</td>
                    <td>{tr`Actions`}</td>
                </tr>
            </thead>
            <tbody>
                {loadingTeachers ? <Loader /> : teachers?.ids.map(tId => (<tr key={'homeroom-enrolled-' + tId}>
                    <td className="text-center">
                        <RFlex>
                            <img
                                width={30}
                                height={30}
                                style={{ borderRadius: "100%" }}
                                src={Services.auth_organization_management.file + teachers.byId[tId].hash_id}
                            />
                            <span>{teachers.byId[tId].name}</span>
                        </RFlex>
                    </td>
                    <td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
                        {teachers.byId[tId].courses.map(c=>c.name).join(',')}
                    </td>
                    <td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
                        <p onClick={() => handleEnrollUsers({ ...teachers.byId[tId], type: enrollment_components.TEACHER })} className="h5 mt-4" style={{ color: '#668AD7', cursor: 'pointer' }}><i className="fa fa-plus" /> {tr`Enroll`}</p>
                    </td>
                </tr>))}
            </tbody>
        </Table>
    </React.Fragment>
}


export default EnrollTeachersEditor;