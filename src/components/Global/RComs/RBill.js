import React from "react";
const RBill = ({
  count,
  color="#668ad7",
  fontColor="#668AD7"
}) => {

  return (<span  style={{display: "flex",
  height: "20px",
  fontWeight: 500,
  color: fontColor??"#ffffff",
  background: color,
  borderRadius: "50px",
  fontSize: "12px",
  padding: "1px",  paddingLeft: "5px",
  paddingRight: "5px",

  }}>
{count}</span>
  );
};
export default RBill;
