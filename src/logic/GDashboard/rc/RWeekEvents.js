import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RWeekEvents.css'
function RWeekEvents({event,eventType,time,date,pepole,color}){
  
  
    return(
        <div style={{margin:'1rem'}}>
          <Row className="main_event_row" style={{borderLeft:`5px solid ${color}`}}>
          <Col>
            <Row className="event_row">
              <Col className="event_title">{event} <button className="event_type"  style={{backgroundColor:color}}>{eventType}</button></Col>
             {/*  <Col className="event_type"><button>{eventType}</button></Col> */}
            </Row> 
            <Row className="event_row">
             <Col lg="3" className="event_time"><i className="fa fa-clock-o"></i> {time}</Col>
              <Col  lg="4" className="event_date">  <i className=" fa fa-calendar"></i>  {date}</Col>
              {/* <Col lg="5" className="event_pepole">{pepole}</Col> */}
            </Row> 
            </Col>
         </Row>
        </div>
    )
}
export default RWeekEvents;