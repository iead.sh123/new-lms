import {
	CHANGE_RUBRIC_STATUS_REQUEST,
	CHANGE_RUBRIC_STATUS_SUCCESS,
	CHANGE_RUBRIC_STATUS_ERROR,
	REMOVE_RUBRIC_REQUEST,
	REMOVE_RUBRIC_SUCCESS,
	REMOVE_RUBRIC_ERROR,
	EXPORT_RUBRIC_AS_PDF_REQUEST,
	EXPORT_RUBRIC_AS_PDF_SUCCESS,
	EXPORT_RUBRIC_AS_PDF_ERROR,
	GET_RUBRIC_BY_ID_REQUEST,
	GET_RUBRIC_BY_ID_SUCCESS,
	GET_RUBRIC_BY_ID_ERROR,
} from "../../actions/global/globalTypes";
import produce from "immer";

const initState = {
	rubric: {},
	rubricLoading: {},
	changeRubricStatusLoading: false,
	removeRubricLoading: false,
	exportRubricAsPdfLoading: false,
};

const rubricReducer = (state = initState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case CHANGE_RUBRIC_STATUS_REQUEST:
				draft.changeRubricStatusLoading = true;
				return draft;
			case CHANGE_RUBRIC_STATUS_SUCCESS:
				draft.changeRubricStatusLoading = false;
				return draft;
			case CHANGE_RUBRIC_STATUS_ERROR:
				draft.changeRubricStatusLoading = false;
				return draft;

			case REMOVE_RUBRIC_REQUEST:
				draft.removeRubricLoading = true;
				return draft;
			case REMOVE_RUBRIC_SUCCESS:
				draft.removeRubricLoading = false;
				return draft;
			case REMOVE_RUBRIC_ERROR:
				draft.removeRubricLoading = false;
				return draft;

			case EXPORT_RUBRIC_AS_PDF_REQUEST:
				draft.exportRubricAsPdfLoading = true;
				return draft;
			case EXPORT_RUBRIC_AS_PDF_SUCCESS:
				draft.exportRubricAsPdfLoading = false;
				return draft;
			case EXPORT_RUBRIC_AS_PDF_ERROR:
				draft.exportRubricAsPdfLoading = false;
				return draft;

			case GET_RUBRIC_BY_ID_REQUEST:
				draft.rubricLoading = true;
				return draft;
			case GET_RUBRIC_BY_ID_SUCCESS:
				draft.rubricLoading = false;
				draft.rubric = action.payload.data;
				return draft;
			case GET_RUBRIC_BY_ID_ERROR:
				draft.rubricLoading = false;
				return draft;

			default:
				return draft;
		}
	});
export default rubricReducer;
