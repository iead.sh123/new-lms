import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { allQuestionSetForCourseAsync } from "store/actions/global/questionSet.actions";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GCourseResources = () => {
	const { courseId } = useParams();
	const dispatch = useDispatch();
	const { allQuestionSetForCourse, allQuestionSetForCoursePagination, allQuestionSetForCourseLoading } = useSelector(
		(state) => state.questionSetRed
	);

	useEffect(() => {
		dispatch(allQuestionSetForCourseAsync(courseId, `?filter=${"ALL"}`));
	}, []);

	const _records = allQuestionSetForCourse?.map((rc) => ({
		id: rc.id,
		details: [{ key: tr`title`, value: rc?.title }],
		actions: [{}],
	}));

	return (
		<div>
			{allQuestionSetForCourseLoading ? (
				<Loader />
			) : (
				<>
					{allQuestionSetForCourse?.length > 0 ? (
						<>
							<RLister Records={_records} showListerMode={"tableLister"} />
							{allQuestionSetForCoursePagination.total > 0 ? (
								<NewPaginator
									firstPageUrl={allQuestionSetForCoursePagination.firstPageUrl}
									lastPageUrl={allQuestionSetForCoursePagination.lastPageUrl}
									currentPage={allQuestionSetForCoursePagination.currentPage}
									lastPage={allQuestionSetForCoursePagination.lastPage}
									prevPageUrl={allQuestionSetForCoursePagination.prevPageUrl}
									nextPageUrl={allQuestionSetForCoursePagination.nextPageUrl}
									total={allQuestionSetForCoursePagination.total}
									data={`type=${questionSetType}&filter=${"ALL"}`}
									getAction={allQuestionSetForCourseAsync}
								/>
							) : (
								""
							)}
						</>
					) : (
						<REmptyData />
					)}
				</>
			)}
		</div>
	);
};

export default GCourseResources;
