import React from "react";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, UncontrolledTooltip } from "reactstrap";
import Helper from "./Helper";
import tr from "./RTranslator";

export const RMenu = ({ actions }) => (
	<UncontrolledDropdown id="ctm_uclDropDown" direction="right">
		<DropdownToggle
			nav
			aria-expanded={false}
			//aria-haspopup={true}
			//caret
			color="secondary"
			//  data-toggle="dropdown"
			//  href="#pablo"

			onClick={(e) => e.preventDefault()}
			role="button"
			id="ctm_toggle"
			style={{ width: "10px" }}
		>
			<i id="ctm_menu" class="fa fa-ellipsis-v" style={{ color: "gray" }} aria-hidden="true"></i>
		</DropdownToggle>
		<DropdownMenu aria-labelledby="dropdownMenuLink" style={{ transform: "translate(-119px, 29px, 0) !important;" }}>
			{actions.map((ac) => (
				<DropdownItem
					style={{ color: ac.color ?? "black" }}
					id={`ctm_${ac.title}`}
					key={`ctm_${ac.title}`}
					onClick={ac.action ?? ac.onClick}
				>
					{ac.icon && <i className={ac.icon} />} {ac.title}
				</DropdownItem>
			))}
		</DropdownMenu>
	</UncontrolledDropdown>
);
