import {GO_BACK} from "../global/globalTypes";

const goBack = payload=>({
    type: GO_BACK,
    payload: payload
});
export default goBack;