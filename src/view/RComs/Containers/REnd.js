import React from "react";
// eslint-disable-next-line react/prop-types
function REnd({children}) {
  
  return <div class={"Rend"}>
  
    {(children&&children.length>0)?children?.map(c=><div>{c}</div>):<div>{children}</div>}
  
  </div>
}
export default REnd;