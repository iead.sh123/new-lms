import React, { useState } from "react";
import powerPointIcon from "assets/img/png/powerpoint.png";
import defaultImage from "assets/img/avatar.png";
import excelIcon from "assets/img/png/excel.png";
import videoIcon from "assets/img/png/multimedia.png";
import audioIcon from "assets/img/png/sound.png";
import fileIcon from "assets/img/png/file.png";
import wordIcon from "assets/img/png/word.png";
import pdfIcon from "assets/img/png/pdf.png";
import { imageTypes, fileTypes, fileExcel, fileWord, filePowerPoint, filePdf, fileVideo, fileAudio } from "config/mimeTypes";
import { Services } from "engine/services";
import AppModal from "components/Global/ModalCustomize/AppModal";
import { RVideoPlayer } from "../RVideoPlayer";
import RFlex from "../RFlex/RFlex";

const RImageViewer = ({ mimeType, url, imageName, showFullAttachment = true, width = "22px", height = "22px" }) => {
	const [open, setOpen] = useState(false);

	const handleOpenModal = () => setOpen(true);
	const handleCloseModal = () => setOpen(false);

	const openFileInNewTab = () => {
		window.open(Services.storage.file + url, "_blank");
		// <div>
		//   <iframe
		//     src={Services.storage.file + url}
		//     title="File Viewer"
		//     width="100%"
		//     height="600px"
		//     style={{ border: "none" }}
		//   >
		//     <p>Your browser does not support PDF, Word, or Excel file viewing.</p>
		//   </iframe>
		// </div>;
	};

	return (
		<>
			{open && showFullAttachment && (
				<AppModal
					size="xl"
					show={open}
					parentHandleClose={handleCloseModal}
					header={imageName}
					headerSort={
						imageTypes.includes(mimeType) ? (
							<div style={{ position: "relative" }}>
								<img style={{ width: "-webkit-fill-available" }} src={Services.storage.file + url} alt="image" />
							</div>
						) : fileVideo.includes(mimeType) ? (
							<div>
								<RVideoPlayer url={Services.storage.file + url} width={"100%"} height={"100%"} />
							</div>
						) : (
							<></>
						)
					}
				/>
			)}
			<RFlex styleProps={{ alignItems: "center" }}>
				<img
					src={
						imageTypes.includes(mimeType)
							? Services.storage.file + url
							: fileTypes.includes(mimeType)
							? fileIcon
							: fileWord.includes(mimeType)
							? wordIcon
							: fileExcel.includes(mimeType)
							? excelIcon
							: filePowerPoint.includes(mimeType)
							? powerPointIcon
							: filePdf.includes(mimeType)
							? pdfIcon
							: fileVideo.includes(mimeType)
							? videoIcon
							: fileAudio.includes(mimeType)
							? audioIcon
							: defaultImage
					}
					alt="image"
					width={width}
					height={height}
					onClick={() =>
						imageTypes.includes(mimeType) ? handleOpenModal() : fileVideo.includes(mimeType) ? handleOpenModal() : openFileInNewTab()
					}
				/>
				{imageName && <span>{imageName}</span>}
			</RFlex>
		</>
	);
};

export default RImageViewer;
