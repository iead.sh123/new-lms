import React, { useEffect, useReducer, useRef, useState } from "react";
import ContextMenu from "../ContextMenu/ContextMenu";
import UserImg from "../userImg";
import { CHAT_ACTIONS_ICONS } from "../constants/ChatTags";
import { relativeDate } from "utils/dateUtil";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RColumn from "view/RComs/Containers/RColumn";
import RRow from "view/RComs/Containers/RRow";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import RForm from "components/Global/RComs/RForm";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import RTextBox from "components/Global/RComs/RTextBox";
import RMiniUserCard from "components/Global/RComs/RMiniUserCard";
import { get } from "config/api";
import Swal, { SUCCESS, WARNING, DANGER } from "utils/Alert";
import { toast } from "react-toastify";

const initialState = {
	selectedUsers: [],
	optionUsers: [],
};

const reducer = (state, action) => {
	switch (action.type) {
		case "addOptionUser":
			return { ...state, optionUsers: [...state.optionUsers, action.user] };
		case "addOptionUsers": {
			//const ss={ selectedUsers:state.selectedUsers,optionUsers:[...state.optionUsers,action.users]};
			const ss = { ...state, optionUsers: action.users };

			return ss;
		}
		case "addSelectedUser":
			return { ...state, selectedUsers: [...state.selectedUsers, action.user] };

		case "deleteSelectedUser":
			return {
				...state,
				selectedUsers: state.selectedUsers.filter((s) => s.id !== action.user_id),
			};

		default:
			return state;
	}
};

const UserSelector = ({ setUsers, single = false }) => {
	// const [selectedUsers,setSelectedUsers]=useState([]);
	// const [optionUsers,setOptionUsers]=useState([]);
	// const [filter,setFilter]=useState("");

	// // useEffect(()=>{
	const divRef = useRef(null);
	const red = useReducer(reducer, initialState);

	//Helper.cl(red)

	const [dropDownShown, setDropDownShown] = useState(true);
	const [users, dispatch] = red;

	useEffect(() => {
		const handleClickOutside = (event) => {
			Helper.cl("co1");
			Helper.cl("co1 2", divRef.current);
			Helper.cl("co1 2", event.target);
			if (divRef.current && !divRef.current.contains(event.target)) {
				setDropDownShown(false);
			}
		};

		document.addEventListener("mousedown", handleClickOutside);

		return () => {
			document.removeEventListener("mousedown", handleClickOutside);
		};
	}, []);

	useEffect(() => {
		if (users.optionUsers.length > 0) setDropDownShown(true);
	}, [users]);

	useEffect(() => {
		// Helper.cl("su1 selectedUsers");
		setUsers(users?.selectedUsers);
	}, [users.selectedUsers]);
	const optionsDiv = useRef();

	const scrollToBottom = () => {
		// Helper.cl("scroll to bottom");
		optionsDiv?.current?.scrollIntoView({ behavior: "smooth" });
	};

	const autocomplete = async (f) => {
		if (f.length < 1) return;
		const url = Services.auth_organization_management.backend + "api/user/prefix/?prefix=" + f;
		// const url=Services.im.backend+"api/users/prefix/?prefix="+f;
		// Helper.cl("url");
		let response1 = await get(url);
		if (response1.data.status) {
			//  Helper.cl(response1.data.data.users," response1 url");
			//  Helper.cl("ffff1")
			dispatch({ type: "addOptionUsers", users: response1.data.data.users });
			//  Helper.cl("ffff2")
		}
	};
	//   autocomplete(filter);
	const scrollOptions = () => {};
	// },[])
	return (
		<div
			style={{
				border: "0px solid red",
				padding: "0.2rem",
				margin: "0.2rem",
				zIndex: 1000000,
			}}
		>
			<div
				style={{
					border: "1px solid #668ad7",
					padding: "0.2rem",
					margin: "0.2rem",
					maxWidth: "100%",
					wordWrap: "break-word",
					display: "inline-block",
					maxHeight: "300px",
					overflowY: "scroll",
					zIndex: 1000000,
				}}
			>
				{users?.selectedUsers?.map((u) => (
					<RMiniUserCard
						name={u.name}
						withDelete={true}
						image={Services.storage.backend + "api/files/get-by-id/" + u?.image?.hash_id}
						onDelete={() => {
							dispatch({ type: "deleteSelectedUser", user_id: u.id });
						}}
					/>
				))}
				<RTextBox
					ref={optionsDiv}
					style={{ minWidth: "30%", border: "0px solid red", outline: "0" }}
					msgOnChange={(e) => {
						scrollToBottom();
						autocomplete(e.target.value);
					}}
				></RTextBox>
			</div>

			{dropDownShown && (
				<div
					ref={divRef}
					style={{
						position: "absolute",
						zIndex: "1000000",
						background: "white",
						display: "flex",
						flexDirection: "column",
						border: `${users.optionUsers.length ? "gray 1px solid" : ""}`,
					}}
				>
					{users?.optionUsers?.map((u, i) =>
						i < 10 ? (
							<div style={{}}>
								<RMiniUserCard
									onClick={() => {
										if (!(single && users?.selectedUsers.length > 0)) {
											dispatch({ type: "addSelectedUser", user: u }), scrollToBottom();
										} else {
											toast.error("Only one user in the chat ,Create a group if you want more people in the chat");
										}
									}}
									name={u.name}
									border="0px"
									role="" //"some role"
									// image={"http://localhost:8001/api/files/get-by-id/"+u.image}
									image={Services.storage.backend + "api/files/get-by-id/" + u?.image?.hash_id}
									onDelete={() => {
										onDelete(u.id);
									}}
									disabled={users?.selectedUsers?.some((obj) => {
										//  Helper.cl(obj,"obj");
										//  Helper.cl(u,"u")
										return obj.id == u.id;
									})}
								/>
							</div>
						) : (
							<></>
						)
					)}
				</div>
			)}
		</div>
	);
};

export default UserSelector;
