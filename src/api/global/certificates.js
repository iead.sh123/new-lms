import { get } from "config/api";
import { Services } from "engine/services";

export const certificates = {
  getCertificatesForSection: (parent_type, parent_id) =>
    get(
      `${Services.certificate.backend}api/certificates?=parent_type=${parent_type}&parent_id=${parent_id}`
    ),
};
