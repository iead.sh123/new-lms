import React from 'react';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import { resourcesBaseUrl } from 'config/constants';
import ImageEnlarge from 'utils/ImageEnlarge';
import {translate} from "../../../../../utils/translate";

const RightColumn = ({ rightColumns }) => {
    return (
        <div className="col-md-6">
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}>
                <h2>{translate('Drop')}</h2>
            </div>

            {Object.entries(rightColumns).map(([columnId, rightChoice], index) => {
                return (
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center'
                        }}
                        key={columnId}>
                        <div style={{ margin: 8 }}>
                            <Droppable droppableId={columnId} key={columnId}>
                                {(provided, snapshot) => {
                                    return (
                                        <div
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                            style={{
                                                background: snapshot.isDraggingOver
                                                    ? 'lightblue'
                                                    : 'lightgrey',
                                                padding: 4,
                                                width: 250,
                                                minHeight: 200
                                            }}>
                                            {rightChoice.fixedItem.choice &&
                                            rightChoice.fixedItem.choice.includes('<math') ? (
                                                <MathJax math={rightChoice.choice} />
                                            ) : (
                                                <div
                                                    style={{
                                                        userSelect: 'none',
                                                        padding: 16,
                                                        margin: '0 0 8px 0',
                                                        minHeight: '20px',
                                                        backgroundColor: '#263B4A',
                                                        color: 'white'
                                                    }}
                                                    dangerouslySetInnerHTML={{
                                                        __html: rightChoice.fixedItem.choice
                                                    }}
                                                />
                                            )}
                                            {rightChoice.items.length > 0 &&
                                                rightChoice.items.map((item, index) => {
                                                    return (
                                                        <Draggable
                                                            key={item.id}
                                                            draggableId={item.id + ''}
                                                            index={index}>
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                        style={{
                                                                            userSelect: 'none',
                                                                            padding: 16,
                                                                            margin: '0 0 8px 0',
                                                                            minHeight: '50px',
                                                                            backgroundColor: snapshot.isDragging
                                                                                ? '#263B4A'
                                                                                : '#456C86',
                                                                            color: 'white',
                                                                            ...provided
                                                                                .draggableProps
                                                                                .style
                                                                        }}>
                                                                        {item.choice &&
                                                                        item.choice.includes(
                                                                            '<math'
                                                                        ) ? (
                                                                            <MathJax
                                                                                math={item.choice}
                                                                            />
                                                                        ) : (
                                                                            <div
                                                                                dangerouslySetInnerHTML={{
                                                                                    __html:
                                                                                        item.choice
                                                                                }}
                                                                            />
                                                                        )}
                                                                        {item.attachments && (
                                                                            <div className="col-sm-10">
                                                                                {[
                                                                                    'png',
                                                                                    'jpeg'
                                                                                ].includes(
                                                                                    item.attachments
                                                                                        .mime_type
                                                                                ) ? (
                                                                                    <ImageEnlarge
                                                                                        // imageStyle={{
                                                                                        //     outline: `${chooseBackgroundColor(
                                                                                        //         item.id
                                                                                        //     )} solid 10px`
                                                                                        // }}
                                                                                        imageSrc={
                                                                                            resourcesBaseUrl +
                                                                                            item
                                                                                                .attachments
                                                                                                .url
                                                                                        }
                                                                                    />
                                                                                ) : (
                                                                                    <a
                                                                                        href={
                                                                                            resourcesBaseUrl +
                                                                                            item
                                                                                                .attachments
                                                                                                .url
                                                                                        }
                                                                                        alt=""
                                                                                        class="btn btn-primary">
                                                                                        {translate('Download')}
                                                                                    </a>
                                                                                )}
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                );
                                                            }}
                                                        </Draggable>
                                                    );
                                                })}
                                            {provided.placeholder}
                                        </div>
                                    );
                                }}
                            </Droppable>
                        </div>
                    </div>
                );
            })}
        </div>
    );
};
export default RightColumn;
