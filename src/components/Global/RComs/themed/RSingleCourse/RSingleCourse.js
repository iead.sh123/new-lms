import React from "react";
import withTheme from "hocs/withTheme";
import withDirection from "hocs/withDirection";

const RSingleCourse = (theme) => {
  let themedComponent = React.lazy(() =>
    (async () => {
      try {
        return await import(`./SingleCourse_${theme}`);
      } catch (err) {
        return await import(`./SingleCourse_default`);
      }
    })()
  );
  return themedComponent;
};

export default withDirection(withTheme(RSingleCourse));
