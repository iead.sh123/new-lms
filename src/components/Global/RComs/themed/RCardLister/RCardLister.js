import withTheme from "hocs/withTheme";
import React from 'react';


const RCardLister = theme => {

    let themedComponent = React.lazy(() =>
        (async () => {
            try {
                return await import(`./RCardLister_${theme}`);
            }
            catch (err) {
                return await import(`./RCardLister_default`);;
            }
        })()
    )
    return themedComponent;
}

export default withTheme(RCardLister);