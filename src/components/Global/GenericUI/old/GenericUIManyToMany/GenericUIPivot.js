import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {getPivotTableData, removeOneToMany, removeManyToMany} from '../../../../store/actions/genericUIHasManyActions';


const GenericUIPivot = ({pivotTable, masterTable, detailTable}) => {
    const pivot = useSelector(state => state.genericUIHasManyReducer.pivot);
    const props = useSelector(state => state.genericUIHasManyReducer.props);
    const selectedMaster = useSelector(state => state.genericUIHasManyReducer.selectedMaster);
    const masterTableTitle = useSelector(state => state.genericUIHasManyReducer.masterTableTitle);
    const detailTableTitle = useSelector(state => state.genericUIHasManyReducer.detailTableTitle);

    const dispatch = useDispatch();
    useEffect(() => {
        if (masterTable && pivotTable && selectedMaster) {
            dispatch(getPivotTableData(masterTable, selectedMaster.id, pivotTable, detailTable));
        }
    }, [selectedMaster])

    const handleRemoveClicked = (line) => {
        let result = confirm('Are You Sure ?')
        if (result) {
            if (pivotTable == detailTable) {
                let request = {
                    tableName: pivotTable, id: line.id, masterTable: masterTable,
                    value: selectedMaster.id
                }
                dispatch(removeOneToMany(request));
            }
            else {
                let request = {
                    tableName: pivotTable, id: line.id, masterTable: masterTable, detailTable: detailTable,
                    value: selectedMaster.id
                }
                dispatch(removeManyToMany(request));
            }
        }
    }

    return (
        <div className="card mb-4">
            <div className="card-header header-title text-center">
                <h5>{masterTableTitle?.slice(0, -1) + ' ' + detailTableTitle}</h5>
            </div>
            <table id='main-table'
                   className='table table-bordered main-table'>
                {pivot.length > 0 ?
                    <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        {props?.map((item) => {
                            return (
                                item.Title == 'permission_id' ? null :
                                    item.Title == 'Permission' ? null :
                                        <th key={item.Title}>
                                            {item.Title}
                                        </th>
                            )
                        })}
                    </tr>
                    </thead>
                    : null
                }
                <tbody>
                {selectedMaster ?
                    pivot?.map((line, index) => {
                        return (
                            <tr key={line.id}>
                                <td scope='col'>
                                    <h5>{line.name}</h5>
                                </td>
                                {props?.map((item) => {
                                    return (
                                        item.Title == 'permission_id' ? null :
                                            item.Title == 'Permission' ? null :
                                                <td key={line[item.Field]}>
                                                    {line[item.Field]}
                                                </td>
                                    )
                                })}
                                <td scope='col'>
                                    <button id={line.id} className='btn btn-danger' onClick={(e) => {
                                        handleRemoveClicked(line)
                                    }}>-
                                    </button>
                                </td>
                            </tr>
                        )

                    }) : null
                }
                </tbody>
            </table>
        </div>
    );
}

export default GenericUIPivot;