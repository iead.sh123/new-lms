import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Form, Label, FormGroup, Input } from "reactstrap";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import RTags from "components/Global/RComs/RTags";
import RButton from "components/Global/RComs/RButton";

const BasicInformation = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	return (
		<>
			<Form>
				<RFlex styleProps={{ flexDirection: "column", width: "70%" }}>
					<Label className="p-0 m-0">{tr("name")}</Label>
					<FormGroup>
						<Input
							name="title"
							type="text"
							placeholder={tr`name`}
							defaultValue={content?.title}
							onChange={(event) =>
								CourseModuleData.handleChangeQuestionSetTitle({
									moduleId: CourseModuleData.specificModule.id,
									moduleContentId: content.id,
									key: event.target.name,
									value: event.target.value,
								})
							}
						/>
					</FormGroup>
				</RFlex>
				<RFlex styleProps={{ flexDirection: "column", width: "70%" }}>
					<Label className="p-0 m-0">{tr("tags")}</Label>
					<FormGroup>
						<RTags
							url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
							getValueFromArrayItem={(i) => i.id}
							getLabelFromArrayItem={(i) => i.name}
							getIdFromArrayItem={(i) => i.id}
							defaultValues={content?.tags}
							onChange={(data) => {}}
							readOnly={true}
						/>
					</FormGroup>
				</RFlex>
			</Form>
			<RFlex
				styleProps={{
					margin: "10px",
					justifyContent: "space-between",
				}}
			>
				<RFlex
					styleProps={{
						alignItems: "center",
						width: "100%",
						justifyContent: "start",
						cursor: !content.is_published ? "pointer" : "default",
					}}
				>
					<div
						style={{
							width: "25px",
							height: "25px",
							borderRadius: "3px",
							background: "rgba(215, 227, 253, 0.70)",
							padding: "3px 6px",
						}}
					>
						<i className="fa-solid fa-pen"></i>
					</div>
					<span
						onClick={() => {
							!content.is_published
								? CourseModuleData.handlePushToSpecificQuestionSet({
										type: content.type,
										questionSetId: content.questionsetId,
								  })
								: {};
						}}
					>{tr`edit_content`}</span>
				</RFlex>

				<RFlex styleProps={{ width: "100%", justifyContent: "end" }}>
					<RButton
						text={tr`save`}
						color="success"
						loading={CourseModuleData.updateTitleLoading}
						disabled={CourseModuleData.updateTitleLoading}
						onClick={() =>
							CourseModuleData.handleUpdateQuestionSetNameFromModuleContentAsync({
								type: content.type,
								contentId: content.content_id,
								title: content.title,
							})
						}
					/>
					{!content.is_published && (
						<RButton
							text={tr`publish`}
							color="link"
							onClick={() =>
								CourseModuleData.handlePublishAndUnPublishModuleContent(
									CourseModuleData.specificModule.id,
									content.id,
									content.is_published ? false : true
								)
							}
						/>
					)}
				</RFlex>
			</RFlex>
		</>
	);
};

export default BasicInformation;
