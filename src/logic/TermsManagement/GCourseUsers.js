import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RUsersContent from "view/TermsManagement/RightSide/RUsersContent";
import Loader from "utils/Loader";
import { useLocation } from "react-router-dom";
import REmptyData from "components/RComponents/REmptyData";

const GCourseUsers = () => {
	const TermsManagementData = useContext(TermsManagementContext);
	const [sortBy, setSortBy] = useState("alphabiticallyDesc");
	const location = useLocation();
	const AcademicPath = location.pathname.includes("academics");

	const studentData = useFetchDataRQ({
		queryKey: ["student-users-to-course", TermsManagementData?.itemObj?.dataObj?.id, sortBy],
		queryFn: () =>
			coursesManagerApi.usersEnrolledInCourse({
				courseId: TermsManagementData?.itemObj?.dataObj?.id,
				userType: "student",
				orderBy: sortBy,
			}),
		onSuccessFn: ({ data }) => {
			if (AcademicPath) {
				TermsManagementData.setFieldValueUsers("studentsById", data.data);
			} else {
				TermsManagementData.setFieldValueUsers("studentsById", data.data);
			}
		},
	});

	const teacherData = useFetchDataRQ({
		queryKey: ["teacher-users-to-course", TermsManagementData?.itemObj?.dataObj?.id, sortBy],
		queryFn: () =>
			coursesManagerApi.usersEnrolledInCourse({
				courseId: TermsManagementData?.itemObj?.dataObj?.id,
				userType: "teacher",
				orderBy: sortBy,
			}),
		onSuccessFn: ({ data }) => {
			TermsManagementData.setFieldValueUsers("teachersById", data.data);
		},
	});

	const handleSelectedAllUsersInCourse = () => {
		const allTeachers = teacherData?.data?.data?.data?.users?.map((user) => ({
			user_id: user?.user_id,
		}));
		const allStudents = studentData?.data?.data?.data?.users?.map((user) => ({
			user_id: user?.user_id,
		}));

		TermsManagementData.setFieldValueUsers("teachers", allTeachers);
		TermsManagementData.setFieldValueUsers("students", allStudents);
	};

	const handleSort = (sortType) => {
		setSortBy(sortType);
	};

	if (studentData?.isLoading || teacherData?.isLoading) {
		return <Loader />;
	} else if (studentData?.isError || teacherData?.isError) {
		return <REmptyData />;
	}
	return (
		<RUsersContent
			students={studentData?.data?.data?.data}
			teachers={teacherData?.data?.data?.data}
			handleSort={handleSort}
			handleSelectedAllUsers={handleSelectedAllUsersInCourse}
			homeRoomUsers={false}
		/>
	);
};

export default GCourseUsers;
