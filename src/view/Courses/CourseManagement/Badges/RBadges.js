import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./RBadges.module.scss";
import { Services } from "engine/services";
import DefaultImage from "assets/img/new/course-default-cover.png";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { dangerColor } from "config/constants";
import dateFormat from "utils/dateFormat";

const RBadges = ({ badges, handleRemoveBadge, handleOpenStoreModal, userType }) => {
	return (
		<RFlex
			styleProps={{
				flexWrap: "wrap",
				justifyContent: badges?.length > 2 ? "space-between" : "start",
				marginTop: "15px",
				gap: 50,
			}}
		>
			{badges.map((badge) => (
				<section key={badge.id} className={userType == "student" ? styles.badge__section__student : styles.badge__section}>
					<section className={styles.badge__image__container}>
						<img src={badge.hash_id ? Services.storage.file + badge.hash_id : DefaultImage} className={styles.badge__image} />
						<div className={styles.badge__action}>
							<UncontrolledDropdown direction="right">
								<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" nav>
									<i className={`fa-solid fa-ellipsis-vertical ${styles.badge__icon}`} />
								</DropdownToggle>
								<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
									<DropdownItem key={"1"} onClick={() => handleOpenStoreModal(badge)}>
										<RFlex
											styleProps={{
												alignItems: "center",
												justifyContent: "start",
											}}
										>
											<i className="fa-solid fa-pen-to-square" />
											{tr`edit`}
										</RFlex>
									</DropdownItem>
									<DropdownItem
										key={"2"}
										onClick={() => {
											handleRemoveBadge(badge.id);
										}}
										style={{ color: dangerColor }}
									>
										<RFlex
											styleProps={{
												alignItems: "center",
												justifyContent: "start",
											}}
										>
											<i className="fa-solid fa-trash-can" />
											{tr`remove`}
										</RFlex>
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</div>
					</section>
					<section className={userType == "student" ? styles.badge__detail : ""}>
						<h6>{badge.description}</h6>
						{userType == "student" && <h6>{dateFormat(badge.date)}</h6>}
					</section>
				</section>
			))}
		</RFlex>
	);
};

export default RBadges;
