import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { get } from "config/api";
import { useParams } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const AddPermissionsToUserOrRoleLister = ({ payloadData, setPayloadData }) => {
	const { organizationUserId, roleId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/scope/scopesToAddForOrganizationUserOrRole${
			organizationUserId ? `?organization_user_id=${organizationUserId}` : `?role_id=${roleId}`
		}`;
		let response = await get(specific_url ? specific_url : url);

		if (response && response.data && response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const handleAddPermissionsToUser = (record) => {
		setPayloadData((prevState) => {
			// Check if the id already exists in the scopes array
			const exists = prevState.scopes.includes(record.id);

			if (exists) {
				// Remove the id from the scopes array
				const updatedRolesIds = prevState.scopes.filter((roleId) => roleId !== record.id);

				// Return the updated state
				return {
					...prevState,
					scopes: updatedRolesIds,
				};
			} else {
				// Add the id to the scopes array
				const updatedRolesIds = [...prevState.scopes, record.id];

				// Return the updated state
				return {
					...prevState,
					scopes: updatedRolesIds,
				};
			}
		});
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.map((rc) => ({
			id: rc.id,
			tableName: "AddPermissionsToUserOrRoleLister",
			details: [
				{ key: tr`name`, value: rc.title },
				{ key: tr`Description`, value: rc.description },
			],
			actions: [
				{
					name: "add",
					icon: payloadData.scopes.includes(rc.id) ? iconsFa6.delete : "fa fa-plus",
					outline: true,
					color: payloadData.scopes.includes(rc.id) ? "danger" : "primary",
					onClick: () => {
						handleAddPermissionsToUser(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords, payloadData]
	);
	return <RAdvancedLister {...propsLiterals.listerProps} />;
};

export default AddPermissionsToUserOrRoleLister;
