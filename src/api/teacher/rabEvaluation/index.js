import { get, patch } from "config/api";
import { post } from "jquery";

export const rabEvaluationAPI = {

  getEvaluationItems: (rabId) =>
    get(`api/rab-evaluation/all-evaluation-types/rabs/${rabId}`),
  getTeacherEvaluationItems: (rabId) => get(`api/rab-evaluation/all-evaluation-types/rabs/${rabId}`),//get(`api/rab-evaluation/evaluation-types/rabs/${rabId}/users`),
  getParts: (rabId, type, postFix = "") =>
    get(`/api/rab-evaluation/rabs/${rabId}/types/${type}${postFix}`),
  getStudentsDetails: (rabId, type, studentId) =>
    get(`api/rab-evaluation/rabs/${rabId}/types/${type}/students/${studentId}`),

  updateEvaluations: (rabId, payload,setProgress) =>
    patch(`/api/rab-evaluation/rabs/${rabId}`, payload,{
      onUploadProgress: progressEvent => {
        let progress = 0;
        progress = (progressEvent.loaded / progressEvent.total) * 100;
        setProgress(progress)
      }
    }),
  updateEvaluationParts: (rabId, type, payload, postFix = "",setProgress) =>
    patch(`/api/rab-evaluation/rabs/${rabId}/types/${type}${postFix}`, payload,{
      onUploadProgress: progressEvent => {
        let progress = 0;
        progress = (progressEvent.loaded / progressEvent.total) * 100;
        setProgress(progress)
      }
    }),
    importFromTemplate: rabId=>get(`/api/rab-evaluation/import-from-template/${rabId}`)
};
