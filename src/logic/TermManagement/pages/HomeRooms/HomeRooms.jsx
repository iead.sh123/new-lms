 import { faChevronRight, faUserFriends, faEllipsisV, faChalkboardTeacher, faPlus } from "@fortawesome/free-solid-svg-icons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import RRegularCard from "view/TermsManager/cards/RegularCard/RRegularCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
 import RCardSkeleton from "view/TermsManager/cards/RCardSkeleton/RCardSkeleton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CreateNewHomeRoomOrCurriculaModal from "../HomeRoomAndCurriculaEditor/components/CreateNewHomeRoomOrCurriculaModal";
import { homeRoomOrCurriculaMode } from "logic/TermManagement/constants/homeroms-curricula-editor-modes.constants";
import Loader from "utils/Loader";
import REmptyData from "components/RComponents/REmptyData";

const HomeRooms = ({
	homeRooms,
	handleManageHomeRoom,
	handleSave,
	handleUpdate,
	handleLoadOrganizationTeachers,
	handleLoadGradeLevelStudents,
	teachers,
	students,
	loading,
}) => {
	const [createModalIsOpen, setCreateModalIsOpen] = React.useState(false);

	const handleToggleCreationModal = () => setCreateModalIsOpen(!createModalIsOpen);

	return (
		<div>
			{loading ? (
				<Loader />
			) : (
				<React.Fragment>
					{/* <CreateTermModal isOpen={createModalIsOpen} toggle={handleToggle} handleSave={edited ? (data)=>handleUpdate(edited,data) && setEdited(null) : handleSave} edited={terms?.byId?.[edited]} /> */}
					{homeRooms?.ids?.length ? (
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{homeRooms?.ids?.map((tId) => (
								<RRegularCard
									key={"term-" + tId}
									icon={faChevronRight}
									handleIconClicked={() => handleManageHomeRoom(tId)}
									id={tId}
									header={homeRooms.byId[tId]?.name}
									items={[
										{
											text: `students: ${homeRooms.byId[tId]?.statistics?.students}`,
											icon: faUserFriends,
										},
										{
											text: `teachers:  ${homeRooms.byId[tId]?.statistics?.teachers}`,
											icon: faChalkboardTeacher,
										},
									]}
									contextMenuActions={[]}
								/>
							))}
							<RCardSkeleton
								content={
									<RFlex
										styleProps={{
											fontWeight: "400",
											fontSize: "16px",
											color: "#668AD7",
											padding: "50px",
											justifyContent: "center",
											flexDirection: "column",
											alignItems: "center",
										}}
									>
										<FontAwesomeIcon icon={faPlus} />
										<p>add home room</p>
									</RFlex>
								}
								onClick={handleToggleCreationModal}
							/>
						</RFlex>
					) : (
						<REmptyData
							line2={
								<p role="button" onClick={handleToggleCreationModal}>
									click on <span style={{ color: "#668AD7" }}>+ Create new homeroom </span>to start creating homerooms
								</p>
							}
						/>
					)}
				</React.Fragment>
			)}
			<CreateNewHomeRoomOrCurriculaModal
				teachers={teachers}
				students={students}
				handleLoadGradeLevelStudents={handleLoadGradeLevelStudents}
				handleLoadOrganizationTeachers={handleLoadOrganizationTeachers}
				mode={homeRoomOrCurriculaMode.HOMEROOM}
				isOpen={createModalIsOpen}
				toggle={handleToggleCreationModal}
				handleSave={handleSave}
			/>
		</div>
	);
};

export default HomeRooms;
