import React, { useContext } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useParams } from "react-router-dom";

import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { Input, FormGroup } from "reactstrap";
import styles from "./RDiscussion.module.scss";
import RButton from "components/Global/RComs/RButton";

const RSeeMoreText = ({
  text,
  postId,
  contentId,
  replyId,
  specificMandatoryPost,
  typeContent,
  editInput,
}) => {
  const { cohortId, categoryName, sectionId } = useParams();
  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  const textShow = text?.slice(0, 75);

  return (
    <>
      {typeContent !== "post" && editInput ? (
        <>
          <FormGroup>
            <div style={{ position: "relative" }}>
              <Input
                name="text"
                required
                type="textarea"
                defaultValue={text}
                onChange={(event) => {
                  DiscussionData.handleChangeCreateComment(
                    event.target.name,
                    event.target.value
                  );
                  DiscussionData.handleChangeCreateComment("reply_id", replyId);
                  DiscussionData.handleChangeCreateComment(
                    "comment_id",
                    typeContent == "comment" ? postId : null
                  );
                  DiscussionData.handleChangeCreateComment(
                    "for_community",
                    cohortId ? false : true
                  );
                  DiscussionData.handleChangeCreateComment(
                    "typeContent",
                    typeContent
                  );
                }}
                style={{ width: "100%" }}
              />

              <RButton
                text={tr`edit`}
                color="primary"
                className={styles.submit_button}
                onClick={() => DiscussionData.handleEditComment()}
                loading={DiscussionData.updateCommentLoading}
                disabled={DiscussionData.updateCommentLoading ? true : false}
              />
            </div>
          </FormGroup>
        </>
      ) : (
        // <RFlex styleProps={{ flexWrap: "wrap", width: "100%" }}>
        <>
          <p className=" p-0 m-0" style={{ fontWeight: 400 }}>
            {specificMandatoryPost ? text : textShow}
          </p>
          <>
            {!specificMandatoryPost && text != textShow ? (
              <p
                style={{ color: primaryColor, cursor: "pointer" }}
                onClick={() => {
                  DiscussionData.handleOpenSeeTheContentOfThePost();
                  DiscussionData.setPostId(postId);
                  DiscussionData.setSpecificPost(true);
                }}
              >
                {tr` ...See More`}
              </p>
            ) : (
              ""
            )}
          </>
        </>
        // </RFlex>
      )}
    </>
  );
};

export default RSeeMoreText;
