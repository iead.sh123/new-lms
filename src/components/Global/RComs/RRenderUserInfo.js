import React from "react";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";

const RRenderUserInfo = ({ name, image, showAction, clickOnAction }) => {
	return (
		<RFlex styleProps={{ alignItems: "center", justifyContent: "center" }}>
			<img
				width={30}
				height={30}
				style={{ borderRadius: "100%" }}
				src={image ? Services.auth_organization_management.file + image : UserAvatar}
			/>
			<span>{name}</span>
			{showAction && <i style={{ cursor: "pointer" }} className={iconsFa6.close} onClick={() => clickOnAction()} />}
		</RFlex>
	);
};

export default RRenderUserInfo;
