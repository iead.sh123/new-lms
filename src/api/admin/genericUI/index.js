import { post, get } from "config/api";
import { Services } from "engine/services";

export const manyToManyAPI = {
	getUsers: (filter = null) => get(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/getData/${filter}/users`), //to delete deprecated
	getRightSide: (payload) => post(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/getPivotData`, payload),
	deleteRelation: (payload) => post(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/removeManyToMany`, payload),
	storeRelation: (payload) => post(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/storeManyToMany`, payload),
	getTableData: (payload) => post(`${Services.auth_organization_management.backend}api/generic/getTableData`, payload),
};

export const oneToManyAPI = {
	storeRelation: (payload) => post(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/storeOneToMany`, payload),
	deleteRelation: (payload) => post(`${Services.auth_organization_management.backend}api/generic-ui-hasMany/removeOneToMany`, payload),
};
