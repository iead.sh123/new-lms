import {
	GET_UNIT_PLAN_REQUEST,
	GET_UNIT_PLAN_SUCCESS,
	GET_UNIT_PLAN_ERROR,
	SHOW_UNIT_PLAN_EDITOR,
	SET_UNIT_PLAN_VALUE,
	ADD_SECTION_TO_UNIT_PLAN,
	ADD_DATA_TO_SECTION,
	ADD_ITEMS_TO_SECTION,
	REMOVE_SECTION,
	PICK_A_RUBRIC_TO_UNIT_PLAN_SECTION,
	PICK_A_LESSON_PLAN_TO_UNIT_PLAN_SECTION,
	REMOVE_RUBRIC_FROM_SECTION,
	REMOVE_LESSON_PLAN_FROM_SECTION,
	REMOVE_ATTACHMENT_FROM_SECTION,
	REMOVE_ITEM_FROM_SECTION,
	REMOVE_ATTACHMENT_FROM_ITEMS,
	SAVE_UNIT_PLAN_REQUEST,
	SAVE_UNIT_PLAN_SUCCESS,
	SAVE_UNIT_PLAN_ERROR,
	GET_VALIDATION_UNIT_PLAN,
	ADD_VALIDATION_TO_UNIT_PLAN,
	CREATE_UNIT_PLAN_TEMPLATE,
	EMPTY_STORE,
} from "store/actions/global/globalTypes";
import produce from "immer";

const InitState = {
	unitPlan: null,
	courseUnitPlan: null,
	unitPlanErrors: null,
	test: null,

	unitPlanLoading: false,
	showUnitPlanEditor: false,
	saveUnitPlanLoading: false,
	saveAsADraftUnitPlanLoading: false,
	saveAsATemplateUnitPlanLoading: false,
};

let ind;
let indItem;
let indSection;

const Search = (items, ItemId) => {
	let result = false;
	for (let ii = 0; ii < items.length; ii++) {
		const i = items[ii];

		if ((i.id ? i.id : i.fakeId) == ItemId) {
			result = i;
			return i;
		} else if (i.items && i.items.length > 0) {
			const s = Search(i.items, ItemId);
			if (s) {
				result = s;
				return s;
			}
		}
	}
	result = false;
	return result;
};

const SearchItemsArr = (items, ItemId) => {
	let result = false;
	for (let ii = 0; ii < items.length; ii++) {
		const i = items[ii];
		if ((i.id ? i.id : i.fakeId) == ItemId) {
			result = i;
			return items;
		} else if (i.items && i.items.length > 0) {
			const s = SearchItemsArr(i.items, ItemId);
			if (s) {
				result = s;
				return s;
			}
		}
	}
	result = false;
	return result;
};

const generateFakeId = () => {
	return Math.random().toString(36).substring(2, 10);
};

const removeIdsAndReplaceWithFakeIds = (obj) => {
	if (obj instanceof Array) {
		// If the object is an array, recursively process each item
		return obj.map((item) => removeIdsAndReplaceWithFakeIds(item));
	} else if (obj instanceof Object) {
		// If the object is an object, recursively process each property
		return Object.entries(obj).reduce((acc, [key, value]) => {
			if (key === "id") {
				// Replace the "id" property with a fake id
				acc["fakeId"] = generateFakeId();
			} else {
				// Process other properties recursively
				acc[key] = removeIdsAndReplaceWithFakeIds(value);
			}
			return acc;
		}, {});
	}
	// Return other values as is
	return obj;
};

const unitPlanReducer = (state = InitState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case SHOW_UNIT_PLAN_EDITOR:
				draft.showUnitPlanEditor = action.payload.value;
				return draft;

			case SET_UNIT_PLAN_VALUE:
				draft.unitPlan[action.name] = action.value;
				return draft;

			case GET_UNIT_PLAN_REQUEST:
				draft.unitPlanLoading = true;
				return draft;

			case GET_UNIT_PLAN_SUCCESS:
				draft.unitPlanLoading = false;
				draft.unitPlan = action.payload.data;
				return draft;

			case GET_UNIT_PLAN_ERROR:
				draft.unitPlanLoading = false;
				return draft;

			case ADD_SECTION_TO_UNIT_PLAN:
				if (!action.payload.indexAdd) {
					draft?.unitPlan?.uplsections.unshift(action.payload.unitPlanSection);
					// draft?.unitPlanErrors?.uplsections.unshift(
					//   action.payload.unitPlanSection
					// );
				} else {
					draft?.unitPlan?.uplsections.splice(action.payload.indexAdd, 0, action.payload.unitPlanSection);
					// draft?.unitPlanErrors?.uplsections.splice(
					//   action.payload.indexAdd,
					//   0,
					//   action.payload.unitPlanSection
					// );
				}

				return draft;

			case ADD_DATA_TO_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);
				if (ind !== -1) {
					draft.unitPlan.uplsections[ind][action.payload.name] = action.payload.value;
				}
				return draft;

			case ADD_ITEMS_TO_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);

				if (ind !== -1) {
					if (action.payload.type == "parent") {
						if (action.payload.itemId) {
							const itemParentObj = Search(draft.unitPlan.uplsections[ind].items, action.payload.itemId);

							itemParentObj[action.payload.name] = action.payload.value;
						} else {
							draft.unitPlan.uplsections[ind].items.push(action.payload.unitPlanItem);
						}
					} else {
						const itemParentObj = Search(draft.unitPlan.uplsections[ind].items, action.payload.itemId);
						itemParentObj.items.push(action.payload.unitPlanItem);
					}
				}
				return draft;

			case REMOVE_ITEM_FROM_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);
				if (ind !== -1) {
					const resultItem = SearchItemsArr(draft.unitPlan.uplsections[ind].items, action.payload.itemId);

					indItem = resultItem.findIndex((item) => (item.id ? item.id == action.payload.itemId : item.fakeId == action.payload.itemId));

					if (indItem !== -1) {
						resultItem.splice(indItem, 1);
					}
				}
				return draft;

			case REMOVE_ATTACHMENT_FROM_ITEMS:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);
				if (ind !== -1) {
					const resultItem = SearchItemsArr(draft.unitPlan.uplsections[ind].items, action.payload.itemId);

					indItem = resultItem.findIndex((item) => (item.id ? item.id == action.payload.itemId : item.fakeId == action.payload.itemId));

					if (indItem !== -1) {
						if (action.payload.type == "rubric") {
							resultItem[indItem].rubric = {};
							resultItem[indItem].rubric_id = null;
						} else {
							resultItem[indItem].lesson_plan = {};
							resultItem[indItem].lesson_plan_id = null;
						}
					}
				}
				return draft;

			case REMOVE_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);

				indSection = draft.unitPlanErrors?.uplsections.findIndex((sectionUnitPlanError) =>
					sectionUnitPlanError?.id
						? sectionUnitPlanError?.id == action.payload.sectionId
						: sectionUnitPlanError?.fakeId == action.payload.sectionId
				);

				if (ind !== -1) {
					draft.unitPlan.uplsections.splice(ind, 1);
				}

				if (indSection !== -1) {
					draft.unitPlanErrors.uplsections.splice(indSection, 1);
				}
				return draft;

			case PICK_A_RUBRIC_TO_UNIT_PLAN_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);

				if (ind !== -1) {
					draft.unitPlan.uplsections[ind].rubric = action.payload.rubricObj;
					draft.unitPlan.uplsections[ind].rubric_id = action.payload.rubricObj.id;
				}
				return draft;

			case PICK_A_LESSON_PLAN_TO_UNIT_PLAN_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);

				if (ind !== -1) {
					draft.unitPlan.uplsections[ind].lesson_plan = action.payload.lessonPlanObj;
					draft.unitPlan.uplsections[ind].lesson_plan_id = action.payload.lessonPlanObj.id;
				}
				return draft;

			case REMOVE_RUBRIC_FROM_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);
				if (ind !== -1) {
					draft.unitPlan.uplsections[ind].rubric = {};
					draft.unitPlan.uplsections[ind].rubric_id = null;
				}
				return draft;

			case REMOVE_LESSON_PLAN_FROM_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);
				if (ind !== -1) {
					draft.unitPlan.uplsections[ind].lesson_plan = {};
					draft.unitPlan.uplsections[ind].lesson_plan_id = null;
				}
				return draft;

			case REMOVE_ATTACHMENT_FROM_SECTION:
				ind = draft.unitPlan?.uplsections.findIndex((section) =>
					section?.id ? section?.id == action.payload.sectionId : section?.fakeId == action.payload.sectionId
				);

				if (ind !== -1) {
					draft.unitPlan.uplsections[ind].attachments = [];
				}
				return draft;

			case SAVE_UNIT_PLAN_REQUEST:
				if (action.payload.data.data.type == "save_and_publish") {
					draft.saveUnitPlanLoading = true;
				} else if (action.payload.data.data.type == "save_as_a_draft") {
					draft.saveAsADraftUnitPlanLoading = true;
				} else {
					draft.saveAsATemplateUnitPlanLoading = true;
				}

				return draft;
			case SAVE_UNIT_PLAN_SUCCESS:
				if (action.payload.data.data.type == "save_and_publish") {
					draft.saveUnitPlanLoading = false;
				} else if (action.payload.data.data.type == "save_as_a_draft") {
					draft.saveAsADraftUnitPlanLoading = false;
				} else {
					draft.saveAsATemplateUnitPlanLoading = false;
				}
				return draft;
			case SAVE_UNIT_PLAN_ERROR:
				if (action.payload.data.data.type == "save_and_publish") {
					draft.saveUnitPlanLoading = false;
				} else if (action.payload.data.data.type == "save_as_a_draft") {
					draft.saveAsADraftUnitPlanLoading = false;
				} else {
					draft.saveAsATemplateUnitPlanLoading = false;
				}
				return draft;

			case GET_VALIDATION_UNIT_PLAN:
				draft.unitPlanErrors = action.payload.initialValidationData;
				return draft;

			case ADD_VALIDATION_TO_UNIT_PLAN:
				if (action.payload.name == undefined) {
					draft.unitPlanErrors = action.payload.message;
				} else if (action.payload.sectionId && !action.payload.itemId) {
					ind = draft.unitPlanErrors?.uplsections.findIndex((sectionUnitPlanError) =>
						sectionUnitPlanError?.id
							? sectionUnitPlanError?.id == action.payload.sectionId
							: sectionUnitPlanError?.fakeId == action.payload.sectionId
					);

					if (ind !== -1) {
						draft.unitPlanErrors.uplsections[ind][action.payload.name] = action.payload.message;
					}
				} else {
					draft.unitPlanErrors[action.payload.name] = action.payload.message;
				}
				return draft;

			case CREATE_UNIT_PLAN_TEMPLATE:
				const resultItem = removeIdsAndReplaceWithFakeIds(action.payload.data);
				draft.unitPlan = resultItem;
				return draft;

			case EMPTY_STORE:
				draft.unitPlan = action.payload.data;
				draft.unitPlanErrors = action.payload.data;
				return draft;

			default:
				return draft;
		}
	});

export default unitPlanReducer;
