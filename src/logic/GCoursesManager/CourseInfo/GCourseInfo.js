import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import RCourseInfo from "view/CoursesManager/RCourseInfo/RCourseInfo";
import {
	courseByIdAsync,
	categoryAncestorsAsync,
	setCourseValue,
	getCourseCategoriesTreeAsync,
	getLevelsAsync,
	getUserTypeAsync,
	createCourseAsync,
	patchCourseAsync,
	emptyDataWhenAddNewCourse,
} from "store/actions/global/coursesManager.action";
import Loader from "utils/Loader";
import { useHistory } from "react-router-dom";

const GCourseInfo = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { categoryId, courseId } = useParams();

	const { courseById, courseByIdLoading } = useSelector((state) => state.coursesManagerRed);
	useEffect(() => {
		if (courseId) {
			dispatch(courseByIdAsync(courseId));
		}
	}, [courseId]);

	useEffect(() => {
		if (categoryId) {
			dispatch(categoryAncestorsAsync(categoryId));
		}
	}, [categoryId]);

	useEffect(() => {
		dispatch(getCourseCategoriesTreeAsync({ forDropDown: true }));
		dispatch(getLevelsAsync(true));
		dispatch(getUserTypeAsync(true, "teachers"));
		dispatch(getUserTypeAsync(true, "directors"));
		dispatch(getUserTypeAsync(true, "course-administrators"));
	}, []);

	useEffect(() => {
		dispatch(emptyDataWhenAddNewCourse());
	}, []);

	const handelCourseChange = (name, value) => {
		dispatch(setCourseValue(name, value));
	};

	const handleCreateCourse = ({ doNotGoToCourseOverView }) => {
		const data = {
			isOnline: courseById.isOnline ?? false,
			start_date: courseById.start_date ?? "",
			end_date: courseById.end_date ?? "",
			isFree: courseById.isFree ?? false,
			prices: courseById.prices ?? [],
			discounts: courseById.discounts ?? [],
			category_id: categoryId,
			...courseById,
		};

		data.isOnline == false && delete data.start_date;
		data.isOnline == false && delete data.end_date;
		data.isFree == true && delete data?.prices;
		data.isFree == true && delete data?.discounts;
		if (courseById?.course_id || courseId) {
			dispatch(patchCourseAsync(data, "frontend", courseById?.course_id ?? courseId, categoryId, history, doNotGoToCourseOverView));
		} else {
			dispatch(createCourseAsync(data, "frontend", courseById?.course_id ?? courseId, categoryId, history, doNotGoToCourseOverView));
		}
	};

	useEffect(() => {
		// here after upload image and icon , [image,icon] are remove it from backend
		if (courseById?.image?.length > 0 || courseById?.icon?.length > 0) {
			handleCreateCourse({ doNotGoToCourseOverView: true });
		}
	}, [courseById.image, courseById.icon]);

	return (
		<div>
			{courseByIdLoading ? <Loader /> : <RCourseInfo handelCourseChange={handelCourseChange} handleCreateCourse={handleCreateCourse} />}
		</div>
	);
};

export default GCourseInfo;
