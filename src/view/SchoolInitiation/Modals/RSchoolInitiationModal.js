import React, { useContext } from "react";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { useParams } from "react-router-dom";
import RSchoolInitiationFormContent from "./RSchoolInitiationFormContent";
import AppModal from "components/Global/ModalCustomize/AppModal";
import styles from "../RSchoolInitiation.module.scss";
import tr from "components/Global/RComs/RTranslator";

const RSchoolInitiationModal = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { sIType, sITypeId, tabTitle } = useParams();

  return (
    <AppModal
      modalBodyClass={styles.modal_body}
      show={SchoolInitiationData.show}
      parentHandleClose={SchoolInitiationData.handleClose}
      header={
        sIType == "education_stage"
          ? tr`create_grade_level`
          : sIType == "grade_levels"
          ? tr`create_curricula`
          : tr`create_education_stage`
      }
      size={sIType == "education_stage" ? "lg" : "md"}
      headerSort={<RSchoolInitiationFormContent />}
    />
  );
};

export default RSchoolInitiationModal;
