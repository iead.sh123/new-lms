import React from "react";
import styles from "../QuestionViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Label } from "reactstrap";
import { Services } from "engine/services";

const ViewEssay = ({ question, isGroup }) => {
  return (
    <Row className="mt-4 d-flex align-items-center">
      {!isGroup && (
        <Col xs={12} className="p-0">
          <RFlex>
            <i className="fas fa-align-left pt-1" />
            <Label className="p-0 m-0">{tr`essay`}</Label>
          </RFlex>
        </Col>
      )}
      <Col xs={12} className="p-0 pt-3">
        {isGroup ? (
          <ul>
            <li>
              <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
            </li>
          </ul>
        ) : (
          <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
        )}
        {question?.files[0]?.hash_id !== undefined && (
          <img
            src={Services.storage.file + question?.files[0]?.hash_id}
            alt={question?.id}
            className={styles.image}
          />
        )}
      </Col>
    </Row>
  );
};

export default ViewEssay;
