import React, { useEffect, useState } from "react";
import { getFileExtension } from "utils/handelText";
import { getFileNameWithoutExtension } from "utils/handelText";
import { getRelativeDate } from "utils/dateUtil";
import RTimeLine from "./RTimeLine";
import { fileImage } from "components/Global/RComs/RResourceViewer/RFile";
import Helper from "components/Global/RComs/Helper";
import { baseUrl } from "config/constants";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
const fileItem=(title,filename,size,time,courseName,type,link,icon)=>{
  //alert(type);
  return ({
    link:link,
    icon:  icon,
    roundIcon:true,
     title: title, 
  description:
  <>  <div style={{ fontSize: '14px', color: 'black' }}>
 {filename}
<span style={{ fontSize: '11px', color: 'gray', marginLeft: '10px' }}>{size}</span>
<span style={{ fontSize: '20px', color: 'black', marginLeft: '10px' }}>•</span>
<i classname={"fa-clock"} style={{ fontSize: '11px', color: '#668AD7', marginLeft: '10px' }} />
<span style={{ fontSize: '11px', color: '#668AD7', marginLeft: '5px' }}>{time}</span>
</div>
<div style={{ fontSize: '11px', fontWeight: 'bold', color: 'gray' }}>{courseName}</div></>

, image: fileImage(type) })}


const GRecentlyAddedFiles = ({recentFiles}) => {
  
  return  <RTimeLine 
  items= //{data?.recentlyAddedContents?.map(e=>fileItem(e.name,e.id,e.createdAt,"history",e.mime_type))} 
  {recentFiles?.map(e=>
{
Helper.cl(e,"GRecentlyAddedFiles");
return  e.attachment?
        fileItem(
        "attachemnt",
        getFileNameWithoutExtension(e.name),
        "",
        getRelativeDate(e.created_at),
        "Course Name",
        getFileExtension(e.name), 
        `${baseURL}/${genericPath}/course-management/course/${e.course_id}/cohort/null/modules/view/student?moduleId=${e.module_id}&moduleContentId=${e.id}`,
        "fas fa-external-link-alt"
        //`/course-management/course/${courseid}/cohort/null/modules/view/student?moduleId=${moduleid}&moduleContentId=${moduleContentid}`
        )
        //fileItem(e.name,e.id,e.createdAt,"history",e.mime_type)
        :fileItem(
        "link",
     
        e?.link?.link_title,
        "",
        getRelativeDate(e.created_at),
        "Course Name",
        "",
        `${baseURL}/${genericPath}/course-management/course/${e.course_id}/cohort/null/modules/view/student?moduleId=${e.module_id}&moduleContentId=${e.id}`,
        "fas fa-external-link-alt"
        )
  }
        )
}color="#668AD7" />
};

export default GRecentlyAddedFiles;
