import React, { useState } from "react";
import { post } from "config/api";
import { useHistory } from "react-router-dom";
import RAdvancedLister from "../RComs/RAdvancedLister";
import AppModal from "../ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import ExcelPage from "./ExcelPage";
import UserLogs from "./UsersManagment/UserLogs";
import { resetFilters } from "store/actions/admin/genericFilter.Action";
import { useEffect } from "react";
import filterReducer, { filterState } from "../RComs/filterState/filterReducer";
import { toast } from "react-toastify";

const GenericUILister = (props) => {
	const [table_props, setTableProps] = useState([]);
	const [filterField, setFilterField] = useState(null);
	const [filterValue, setFilterValue] = useState(null);
	const [hasAddPermission, setHasAddPermission] = useState(false);
	const [hasImportFile, setHasImportFile] = useState(false);
	const [openedCollapses, setOpenedCollapses] = useState(false);
	const [processsedRecords, setProcessedRecords] = useState([]);
	const [showExcelPage, setShowExcelPage] = useState(false);
	const [importExcel, setImportExcel] = useState(null);
	const [recordId, setRecordId] = useState(null);
	const [showGenericLog, setShowGenericLog] = useState(false);
	const [loaded, setLoaded] = useState(false);

	const history = useHistory();
	//const { newFilter } = useSelector((state) => state.genericFilterRed);

	const [localnewfilter, filterdispatch] = React.useReducer(filterReducer, filterState);

	const dispatch = filterdispatch;
	useEffect(() => {
		dispatch(resetFilters);
		setLoaded(true);

		return () => {
			setLoaded(false);
			dispatch(resetFilters);
		};
	}, []);

	//------------------------Advanced Lister Stuff
	const getDataFromBackend = async (specific_url) => {
		let Request = {
			payload: {
				TableName: props.table_name,
				Filters: loaded ? localnewfilter?.newFilter : [],
			},
		};

		const url = "api/generic/get-table-data";
		let response1 = await post(specific_url ? specific_url : url, Request);

		if (response1 && response1.data.status && response1.data && response1.data.data) {
			const data = response1.data.data;

			if (data.has_add_permission) setHasAddPermission(true);
			if (data.has_import_file) setHasImportFile(true);
			if (data.table_props && data.table_props.data) {
				setTableProps(data.table_props.data);
				return response1;
			}
		} else {
			toast.error(response1.data.msg);
		}
	};

	const setData = (response) => {
		let processsedRecords1 = [];
		const data = response.data.data;
		response?.data?.data?.records?.data?.map((r, i) => {
			let record = {};
			record.allTitle = r.name;
			record.title = r.name;
			record.initialCheckValue = true;
			record.id = r.id;
			record.Titles = [r.name ? r.name : r.title ? r.title : ""];
			record.table_name = "generalUiLister";

			record.details = [];
			response?.data?.data?.table_props;

			response.data.data.table_props.data.map((key) => {
				if (Object.keys(data.fields).includes(key.Field)) {
					let val = r[key.Field];

					if (key.IsDropDownList) {
						const val1 = key.data.filter((s) => s.ID == r[key.Field]);
						val = val1[0].name;
					}
					record.details.push({
						key: key.Title,
						value: val,
						type: key.Title.toLowerCase(),
					});
				}
			});

			record.images = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${r.image}`,
					placeholder: "11",
				},
			];

			record.icons = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${r.icon}`,
					placeholder: "11",
				},
			];

			record.actions = [
				{
					name: "Manage",
					icon: "fa fa-edit",
					color: "info",
					showAction: true,
					onClick: () => {
						history.push(`${process.env.REACT_APP_BASE_URL}/admin/manage/${props.table_name}/${r.id}`);
					},
				},
				{
					name: tr`log`,
					icon: "fa fa-history",
					color: "info",

					onClick: () => {
						setRecordId(r.id);
						showModalGenericLog();
					},
				},
			];

			if (r.has_delete_permission) {
				record.actions.push({
					name: "Delete",
					icon: "fa fa-remove",
					color: "info",

					onClick: () => {
						handleDeleteClicked(r.id);
					},
				});
			}

			processsedRecords1.push(record);
		});
		setImportExcel(response?.data?.data?.ok_to_upload_from_file);

		setProcessedRecords(processsedRecords1);
	};

	const getFilteredData = async () => {
		const response = await getDataFromBackend();
		if (response) {
			setData(response);
		}
	};

	const handleDeleteClicked = async (id) => {
		let agree = prompt("Type 'delete' to accept", "") == "delete";

		if (agree) {
			let request = {
				payload: {
					TableName: props.table_name,
					id: id,
				},
			};

			const response = await post("api/generic/delete", request);
			if (response) {
				if (response.data.status == 1) {
					// alert("done");
					const response1 = await getDataFromBackend();
					if (response1) {
						setData(response1);
					}
				} else {
				}
			} else {
				toast.error(response.data.msg);
			}
		}
	};

	const actionButtons = [];
	if (hasAddPermission) {
		actionButtons.push({
			text: tr("add_new"),
			icon: "nc-icon nc-simple-add",
			className: "h_btn_inline_12 btn-round float-Right  ",
			onClick: () => {
				history.push(`${process.env.REACT_APP_BASE_URL}/admin/add/${props.table_name}`);
			},
		});
	}

	if (!hasImportFile && importExcel !== null && importExcel?.upload_from_file !== false) {
		actionButtons.push({
			text: importExcel?.button_text,
			className: "h_btn_inline_12 btn-round float-Right  ",
			onClick: () => {
				setShowExcelPage(!showExcelPage);
			},
		});
	}

	const showModalGenericLog = () => {
		setShowGenericLog(true);
	};
	const handleCloseGenericLog = () => {
		setShowGenericLog(false);
	};

	return (
		<div className="content">
			<AppModal
				size="xl"
				show={showGenericLog}
				parentHandleClose={handleCloseGenericLog}
				header={tr`logs`}
				headerSort={<UserLogs userId={null} recordId={recordId} tableName={props.table_name} />}
			/>
			{showExcelPage && (
				<ExcelPage
					master_table={props.table_name}
					master_id=""
					detail_table=""
					setShowExcelPage={setShowExcelPage}
					typeFile={importExcel?.allowed_file_types.toString()}
				/>
			)}
			{!showExcelPage && (
				<>
					<RAdvancedLister
						getDataFromBackend={getDataFromBackend}
						setData={setData}
						records={processsedRecords}
						getDataObject={(response) => response.data.data}
						actionButtons={actionButtons}
						characterCount={15}
						marginT={"mt-3"}
						marginB={"mb-2"}
						getCurrentPage={(r) => r.records.current_page}
						getLastPage={(r) => r.records.last_page}
						getFirstPageUrl={(r) => r.records.first_page_url}
						getLastPageUrl={(r) => r.records.last_page_url}
						getNextPageUrl={(r) => r.records.next_page_url}
						getPrevPageUrl={(r) => r.records.prev_page_url}
						getTotal={(r) => r.records.total}
						table_props={table_props}
						getData={getFilteredData}
						filterField={filterField}
						setFilterField={setFilterField}
						filterValue={filterValue}
						setFilterValue={setFilterValue}
						// showListerMode={"both"}
						localnewfilter={localnewfilter}
						filterdispatch={filterdispatch}
					></RAdvancedLister>
					<AppModal
						show={openedCollapses}
						parentHandleClose={() => {
							setOpenedCollapses(false);
						}}
						header={"Adder"}
						headerSort={"s"}
					></AppModal>
				</>
			)}
		</div>
	);
};

export default GenericUILister;
