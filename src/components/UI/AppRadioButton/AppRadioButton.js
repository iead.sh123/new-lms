import Helper from "components/Global/RComs/Helper";
import { FormGroup, Label, Input } from "reactstrap";

const AppRadioButton = (props) => {
	const { style, onChange, ...otherprops } = props;
	return (
		<FormGroup check inline className="ml-0 " disabled={props.disabled}>
			<Label check className=" pl-0 d-flex" {...props}>
				<Input type="radio" style={props.checked && props.selectedStyle ? props.selectedStyle : props.style} {...otherprops} />
				<span style={props.checked && props.selectedStyle ? props.selectedStyle : props.style} className={props.label__class__name}>
					{props.label}
				</span>
			</Label>
		</FormGroup>
	);
};

export default AppRadioButton;
