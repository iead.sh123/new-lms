import React from "react";
import { Button, Col, FormGroup, Label } from "reactstrap";
import { setNewStatusToStartAndEndNode } from "store/actions/global/learningObjects";
import { useDispatch } from "react-redux";
import Interactions from "./Interactions";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RSelect from "components/Global/RComs/RSelect";
import styles from "./RStateMachine.Module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const AllNodes = ({
	stateMachine,
	statuses,
	interactions,
	responses,
	handleAddValueToStateMachine,
	handleRemoveNode,
	handleAddStatusToNode,
}) => {
	const dispatch = useDispatch();
	const handleSetNewStatusToStartAndEnd = (selectedStatus) => {
		dispatch(setNewStatusToStartAndEndNode(selectedStatus));
	};
	return (
		<>
			{stateMachine.nodes.map((node) => (
				<div key={node?.id ? node?.id : node?.fakeId} className={styles.node}>
					<RFlex>
						<Col xs={8}>
							<FormGroup>
								<Label>{tr`statuses`}</Label>
								<RSelect
									option={statuses}
									closeMenuOnSelect={true}
									placeholder={tr`statuses`}
									onChange={(e) => {
										handleAddStatusToNode("object_status_name", e.value, node?.id ? node?.id : node?.fakeId);
										// handleSetNewStatusToStartAndEnd(e.value);
									}}
									defaultValue={[
										{
											label: node.object_status_name,
											value: node.object_status_name,
										},
									]}
								/>
							</FormGroup>
						</Col>
						<Col xs={4} className={styles.delete_button}>
							<Button
								className={"btn-icon " + styles.action_button}
								color="danger"
								size="sm"
								onClick={() => handleRemoveNode(node?.id ? node?.id : node?.fakeId)}
							>
								<i className={iconsFa6.delete + " text-white"} />
							</Button>
						</Col>
					</RFlex>

					<RFlex>
						<Col xs={6}>
							<AppCheckbox
								checked={node.object_status_name == stateMachine.start_object_status_name}
								onChange={(event) => {
									handleAddValueToStateMachine("start_object_status_name", event.target.checked ? node.object_status_name : null);
								}}
								label={tr`start_status`}
								disabled={node.object_status_name == "" ? true : false}
							/>
						</Col>
						<Col xs={6}>
							<AppCheckbox
								checked={
									stateMachine &&
									stateMachine.end_object_statuses_names &&
									stateMachine.end_object_statuses_names.includes(node.object_status_name)
								}
								onChange={(event) => handleAddValueToStateMachine("end_object_statuses_names", node.object_status_name)}
								label={tr`end_status`}
								disabled={node.object_status_name == "" ? true : false}
							/>
						</Col>
					</RFlex>

					<Interactions interactions={interactions} node={node} statuses={statuses} responses={responses} />
				</div>
			))}
		</>
	);
};

export default AllNodes;
