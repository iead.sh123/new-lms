import {
	GET_LEARNING_OBJECT_BY_ID_REQUEST,
	GET_LEARNING_OBJECT_BY_ID_SUCCESS,
	GET_LEARNING_OBJECT_BY_ID_ERROR,
	GET_LEARNING_OBJECT_AVAILABLE_REQUEST,
	GET_LEARNING_OBJECT_AVAILABLE_SUCCESS,
	GET_LEARNING_OBJECT_AVAILABLE_ERROR,
	EDIT_LEARNING_OBJECT_REQUEST,
	EDIT_LEARNING_OBJECT_SUCCESS,
	EDIT_LEARNING_OBJECT_ERROR,
	ADD_VALUES_TO_LEARNING_OBJECT,
	GET_STATE_MACHINE_COMPONENTS_REQUEST,
	GET_STATE_MACHINE_COMPONENTS_SUCCESS,
	GET_STATE_MACHINE_COMPONENTS_ERROR,
	GET_INTERACTION_CONSTRAINT_TYPE,
	GET_STATE_MACHINE_BY_ID_REQUEST,
	GET_STATE_MACHINE_BY_ID_SUCCESS,
	GET_STATE_MACHINE_BY_ID_ERROR,
	ADD_VALUES_TO_STATE_MACHINE,
	ADD_NEW_NODE,
	REMOVE_NODE,
	ADD_STATUS_TO_NODE,
	SET_NEW_STATUS_TO_START_AND_END_NODE,
	ADD_CONSTRAINTS_TO_NODE,
	REMOVE_CONSTRAINTS_FROM_NODE,
	ADD_DATA_TO_CONSTRAINT,
	ADD_RESPONSE_CONSTRAINT_TYPE_TO_CONSTRAINT,
	ADD_RESPONSE_CONSTRAINT,
	REMOVE_AUTO_CORRECT_ANSWER,
	ADD_STATE_MACHINE_REQUEST,
	ADD_STATE_MACHINE_SUCCESS,
	ADD_STATE_MACHINE_ERROR,
	EDIT_STATE_MACHINE_REQUEST,
	EDIT_STATE_MACHINE_SUCCESS,
	EDIT_STATE_MACHINE_ERROR,
	EMPTY_STATE_MACHINE,
	GET_LEARNING_OBJECT_MY_PROGRESS_REQUEST,
	GET_LEARNING_OBJECT_MY_PROGRESS_SUCCESS,
	GET_LEARNING_OBJECT_MY_PROGRESS_ERROR,
} from "../../actions/global/globalTypes";
import produce from "immer";

const initState = {
	learningObject: {},
	stateMachine: {},
	learningObjectAvailable: [],
	badges: [],
	interactionConstraintType: [],
	allInteractions: {},
	interactions: [],
	statuses: [],
	responses: [],
	answers: [],
	learningObjectMyProgress: {},
	learningObjectMyProgressLoading: false,
	learningObjectAvailableLoading: false,
	learningObjectLoading: false,
	editLearningObjectLoading: false,
	badgeLoading: false,
	stateMachineComponentsLoading: false,
	stateMachineLoading: false,
	test: null,
	addStateMachineLoading: false,
	editStateMachineLoading: false,
};

let ind;
let constraintInd;

const learningObjectReducer = (state = initState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case GET_LEARNING_OBJECT_BY_ID_REQUEST:
				draft.learningObjectLoading = true;
				return draft;
			case GET_LEARNING_OBJECT_BY_ID_SUCCESS:
				draft.learningObjectLoading = false;
				draft.learningObject = action.payload.data;
				return draft;
			case GET_LEARNING_OBJECT_BY_ID_ERROR:
				draft.learningObjectLoading = false;
				return draft;

			case GET_LEARNING_OBJECT_AVAILABLE_REQUEST:
				draft.learningObjectAvailableLoading = true;
				return draft;
			case GET_LEARNING_OBJECT_AVAILABLE_SUCCESS:
				draft.learningObjectAvailableLoading = false;
				draft.learningObjectAvailable = action.payload.data;
				return draft;
			case GET_LEARNING_OBJECT_AVAILABLE_ERROR:
				draft.learningObjectAvailableLoading = false;
				return draft;

			case EDIT_LEARNING_OBJECT_REQUEST:
				draft.editLearningObjectLoading = true;
				return draft;
			case EDIT_LEARNING_OBJECT_SUCCESS:
				draft.editLearningObjectLoading = false;
				return draft;
			case EDIT_LEARNING_OBJECT_ERROR:
				draft.editLearningObjectLoading = false;
				return draft;

			case ADD_VALUES_TO_LEARNING_OBJECT:
				draft.learningObject[action.payload.name] = action.payload.value;
				return draft;

			case ADD_VALUES_TO_STATE_MACHINE:
				if (action.payload.name == "start_object_status_name") {
					draft.stateMachine[action.payload.name] = action.payload.value;
				} else {
					draft.stateMachine[action.payload.name] = draft.stateMachine[action.payload.name] ?? [];
					// ind = draft.stateMachine[action.payload.name].findIndex(
					//   (el) => el == action.payload.value
					// );

					ind = draft.stateMachine[action.payload.name].indexOf(action.payload.value);

					if (ind !== -1) {
						draft.stateMachine[action.payload.name].splice(ind, 1);
					} else {
						draft.stateMachine[action.payload.name].push(action.payload.value);
					}
				}
				return draft;

			case GET_STATE_MACHINE_BY_ID_REQUEST:
				draft.stateMachineLoading = true;
				return draft;
			case GET_STATE_MACHINE_BY_ID_SUCCESS:
				draft.stateMachineLoading = false;
				draft.stateMachine = action.payload.data;
				return draft;
			case GET_STATE_MACHINE_BY_ID_ERROR:
				draft.stateMachineLoading = false;
				return draft;

			case GET_STATE_MACHINE_COMPONENTS_REQUEST:
				draft.stateMachineComponentsLoading = true;
				return draft;
			case GET_STATE_MACHINE_COMPONENTS_SUCCESS:
				draft.stateMachineComponentsLoading = false;
				const interactionsKeys = Object.keys(action.payload.data.accept.interactions);
				draft.allInteractions = action.payload.data.accept.interactions;
				draft.interactions = interactionsKeys.map((interaction) => {
					return { label: interaction, value: interaction };
				});
				draft.statuses = action.payload.data.accept.statuses.map((status) => {
					return { label: status, value: status };
				});
				draft.responses = action.payload.data.accept.responses.map((response) => {
					return { label: response, value: response };
				});
				return draft;
			case GET_STATE_MACHINE_COMPONENTS_ERROR:
				draft.stateMachineComponentsLoading = false;
				return draft;

			case ADD_NEW_NODE:
				if (!draft.stateMachine.nodes) {
					draft.stateMachine.nodes = [];
				}
				draft.stateMachine.nodes.push({
					fakeId: Math.random(1000),
					object_status_name: "",
					constraints: [],
				});
				return draft;

			case REMOVE_NODE:
				ind = draft.stateMachine.nodes.findIndex((el) => (el?.id ? el?.id == action.payload.nodeId : el?.fakeId == action.payload.nodeId));
				if (ind !== -1) {
					draft.stateMachine.nodes.splice(ind, 1);
				}
				return draft;

			case ADD_STATUS_TO_NODE:
				ind = draft.stateMachine.nodes.findIndex((el) => (el?.id ? el?.id == action.payload.nodeId : el?.fakeId == action.payload.nodeId));
				if (ind !== -1) {
					draft.stateMachine.nodes[ind][action.payload.name] = action.payload.value;
				}
				return draft;

			case SET_NEW_STATUS_TO_START_AND_END_NODE:
				if (draft.stateMachine.start_object_status_name && draft.stateMachine.start_object_status_name.length > 0) {
					draft.stateMachine.start_object_status_name = action.payload.selectedStatus;
				}
				if (draft.stateMachine.end_object_statuses_names && draft.stateMachine.end_object_statuses_names.length > 0) {
					draft.stateMachine.end_object_statuses_names = action.payload.selectedStatus;
				}
				return draft;

			case ADD_CONSTRAINTS_TO_NODE:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);

				if (ind !== -1) {
					draft.stateMachine.nodes[ind].constraints.push({
						fakeConstraintID: Math.random(100000),
						response_constraint: { correct_responses: { answers: [] } },
					});
				}
				return draft;

			case REMOVE_CONSTRAINTS_FROM_NODE:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);

				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						draft.stateMachine.nodes[ind].constraints.splice(constraintInd, 1);
					}
				}
				return draft;

			case ADD_DATA_TO_CONSTRAINT:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);
				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						draft.stateMachine.nodes[ind].constraints[constraintInd][action.payload.name] = action.payload.value;
					}
				}
				return draft;

			case GET_INTERACTION_CONSTRAINT_TYPE:
				draft.interactionConstraintType = draft.allInteractions[action.payload.interactionValue].map((constrainType) => {
					return { label: constrainType, value: constrainType };
				});

				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);
				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						constraintInd = draft.stateMachine.nodes[ind].constraints[constraintInd][action.payload.name] = draft.interactionConstraintType;
					}
				}
				return draft;

			case ADD_RESPONSE_CONSTRAINT_TYPE_TO_CONSTRAINT:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);

				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						draft.stateMachine.nodes[ind].constraints[constraintInd].response_constraint[action.payload.name] = action.payload.value;
					}
				}
				return draft;

			case ADD_RESPONSE_CONSTRAINT:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);

				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						if (action.payload.responseScoreType) {
							draft.stateMachine.nodes[ind].constraints[constraintInd].response_constraint.correct_responses[action.payload.name] =
								action.payload.value;
						} else {
							// draft.answers.push(action.payload.value);
							// draft.stateMachine.nodes[ind].constraints[
							//   constraintInd
							// ].response_constraint.correct_responses[action.payload.name] =
							//   draft.answers;

							draft.stateMachine.nodes[ind].constraints[constraintInd].response_constraint.correct_responses.answers.push(
								action.payload.value
							);
						}
					}
				}
				return draft;

			case REMOVE_AUTO_CORRECT_ANSWER:
				ind = draft.stateMachine.nodes.findIndex((node) =>
					node?.id ? node?.id == action.payload.nodeId : node?.fakeId == action.payload.nodeId
				);

				if (ind !== -1) {
					constraintInd = draft.stateMachine.nodes[ind].constraints.findIndex((constraint) =>
						constraint?.id ? constraint?.id : constraint?.fakeConstraintID == action.payload.constraintId
					);

					if (constraintInd !== -1) {
						draft.stateMachine.nodes[ind].constraints[constraintInd].response_constraint.correct_responses.answers.splice(
							action.payload.ind,
							1
						);
					}
				}
				return draft;

			case ADD_STATE_MACHINE_REQUEST:
				draft.addStateMachineLoading = true;
				return draft;
			case ADD_STATE_MACHINE_SUCCESS:
				draft.addStateMachineLoading = false;
				return draft;
			case ADD_STATE_MACHINE_ERROR:
				draft.addStateMachineLoading = false;
				return draft;

			case EDIT_STATE_MACHINE_REQUEST:
				draft.addStateMachineLoading = true;
				return draft;
			case EDIT_STATE_MACHINE_SUCCESS:
				draft.addStateMachineLoading = false;
				return draft;
			case EDIT_STATE_MACHINE_ERROR:
				draft.addStateMachineLoading = false;
				return draft;

			case EMPTY_STATE_MACHINE:
				draft.stateMachine = {};
				return draft;

			case GET_LEARNING_OBJECT_MY_PROGRESS_REQUEST:
				draft.learningObjectMyProgressLoading = true;
				return draft;
			case GET_LEARNING_OBJECT_MY_PROGRESS_SUCCESS:
				draft.learningObjectMyProgressLoading = false;
				draft.learningObjectMyProgress = action.payload.data;
				return draft;
			case GET_LEARNING_OBJECT_MY_PROGRESS_ERROR:
				draft.learningObjectMyProgressLoading = false;
				return draft;

			default:
				return draft;
		}
	});
export default learningObjectReducer;
