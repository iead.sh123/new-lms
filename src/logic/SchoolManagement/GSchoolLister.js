import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import styles from './SchoolLister.module.scss'
import SchoolInfo from './SchoolInfo'
import GEducationStage from './GEducationStage'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import Loader from 'utils/Loader'
import { types } from './constants'
import Swal, { DANGER } from 'utils/Alert'
import tr from "components/Global/RComs/RTranslator";

import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import GDetailsLister from './GDetailsLister'
import GPeriods from './GPeriods'
import GGradeScale from './GGradeScale'

const GSchoolLister = () => {
    const user = useSelector((state) => state?.auth);
    const organization_id = user && user?.user && user?.user?.organization_id;
    const { school, stages, loading, error, info } = useSelector((state) => state?.schoolManagementRed)
    console.log('school', school, loading)
    const dispatch = useDispatch()


    const showAlert = (stageId, order) => {
        Swal.input({
            title: tr`are_you_sure_to_delete_it`,
            message: tr``,
            type: DANGER,
            placeHolder: "",
            onConfirm: () => handleDeleteEducationStage(stageId, order),
            onCancel: () => null,
        });
    }
    const handleDeleteEducationStage = ({ stageId, order }) => {
        dispatch(actions?.deleteSpecificCollapse(types.Stage, stageId, organization_id, order))
    };


    const generateEducatoinComponent = () => {
        const lastItemId = school.stageIds[school.stageIds.length - 1]
        const lastOrder = lastItemId ? stages[lastItemId].order : 0
        console.log("order", lastOrder)
        dispatch(actions.addSpecificCollapse(types?.Stage, Object.keys(stages)?.length, organization_id, lastOrder + 1))
    };
    const getSchoolTree = async () => {
        dispatch(actions.getAllPeriods())
        await dispatch(actions?.getSchoolTree(organization_id))
        dispatch(actions.getGradeScale())
    }
    React.useEffect(() => {
        getSchoolTree()
        return () => { }
    }, [])
    const onDragEnd = (result) => {
        console.log(result)
        const sourceIndex = result?.source?.index
        const destinationIndex = result?.destination?.index
        if (sourceIndex == destinationIndex) {
            return
        }
        const matchForType = result.type.match(/level:(\w+)/)
        const matchForId = result.type.match(/type:(\d+)/);
        const type = matchForType ? matchForType[1] : null;
        const id = matchForId ? parseInt(matchForId[1], 10) : null;
        console.log("id isaa", id)
        switch (type) {
            case types.School:
                dispatch(actions.reorderStages(organization_id, sourceIndex, destinationIndex))
                return
            case types.Stage:
                dispatch(actions.reorderGradeLevels(id, sourceIndex, destinationIndex))
                return
            case types.GradeLevel:
                dispatch(actions.reorderCurricula(id, sourceIndex, destinationIndex))
                return
            default:
                console.log("No Matched Type")
        }
    }
    return !error ? school.loading ? <Loader /> : (
        <>
            <RFlex id="Big Container" styleProps={{ gap: '12px' }}>
                <RFlex id="Left Container" styleProps={{ gap: "10px" }} className={styles.LeftContainer} >
                    <RFlex id="Collapsses Container" styleProps={{ gap: "15px", flexDirection: "column" }} >
                        <DragDropContext onDragEnd={onDragEnd} >
                            <Droppable droppableId={`level:${types.School},id:${organization_id}`} type={`level:${types.School},type:${organization_id}`}>
                                {(provided, snapshot) => (

                                    <div className='d-flex flex-column' style={{ gap: '15px' }} ref={provided.innerRef} >
                                        {school?.stageIds.map((key, index) => {
                                            return (
                                                <Draggable key={`item:${types.Stage},key:${String(key)}`} draggableId={`item:${types.Stage},id:${String(key)}`} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div>
                                                            <div ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}>
                                                                <GEducationStage
                                                                    id={parseInt(key)}
                                                                    educationCurrentIndex={index}
                                                                    onDelete={handleDeleteEducationStage}
                                                                />
                                                            </div>
                                                            {provided.placeholder}

                                                        </div>
                                                    )}

                                                </Draggable>
                                            );
                                        })}
                                        {provided.placeholder}

                                    </div>
                                )}

                            </Droppable>
                        </DragDropContext>
                    </RFlex>
                    <RTextIcon
                        flexStyle={{ paddingLeft: "10px", cursor: 'pointer', gap: '5px', width: 'fit-content' }}
                        text={tr("Add_Education_Stage")}
                        onlyText
                        textStyle={{ color: '#668ad7' }}
                        onClick={generateEducatoinComponent}
                    />
                </RFlex>
                <RFlex id="RightContainer" className={styles.RightContainer} styleProps={{ gap: '8px' }}>
                    <SchoolInfo info={info} />
                    <RFlex id="Details Conteiner" className={styles.DetailsConteiner} styleProps={{ gap: '25px' }}>
                        <GDetailsLister />
                        <GPeriods />
                        <GGradeScale />
                    </RFlex>
                </RFlex>
            </RFlex >
        </>
    ) : ""

}

export default GSchoolLister