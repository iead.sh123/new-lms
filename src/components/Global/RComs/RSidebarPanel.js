import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";

import { Card, CardHeader, CardBody, NavItem, NavLink, Nav, TabContent, TabPane, Row, Col } from "reactstrap";
import Switch from "react-bootstrap-switch";
import "./RTabs.css";
import RreactstrapSwitch from "./RreactstrapSwitch";
import tr from "./RTranslator";
import Helper from "./Helper";
import RSecondarySidebar from "view/SecondarySidebar/RsecondarySidebar";
import { useDispatch } from "react-redux";
import { MINIMIZE_SIDEBAR } from "store/actions/global/globalTypes";

const RSidebarPanel = ({
	title,
	Tabs,
	SelectedIndex,
	changeHorizontalTabs,
	Url,
	hideToggle,
	subTitle = false,
	titleStyle,
	omitDefaultClasses,
	ref,
}) => {
	const { tabTitle } = useParams();
	const [activeCom, setActiveCom] = useState(0);
	const [activeTitle, setActiveTitle] = useState("");

	const changeTab = (title, index) => {
		setActiveTitle(title);
		setActiveCom(index);
	};
	const history = useHistory();
	const dispatch = useDispatch();

	dispatch({ type: MINIMIZE_SIDEBAR, payload: { new_state: true } });

	const minimize_sidebar = () => {
		if (!document.body.classList.contains("sidebar-mini")) {
			document.body.classList.add("sidebar-mini");
		}
	};

	const maximize_sidebar = () => {
		if (document.body.classList.contains("sidebar-mini")) {
			document.body.classList.remove("sidebar-mini");
		}
	};
	const cleanUP = useEffect(() => {
		minimize_sidebar();
		return () => {
			maximize_sidebar();
		};
	}, []);

	//document.body.classList.contains("sidebar-mini")

	const sideBarItems1 = {
		title: { title },
		sections: [
			{
				title: "section1",
				items: [
					{ name: "Lessons", link: "", onClick: {} },
					{ name: "Unit plane", link: "", onClick: {} },
					{ name: "Students", link: "", onClick: {} },
					{ name: "Rubrics", link: "", onClick: {} },
				],
			},
			{
				title: "",
				items: [
					{ name: "Topics", link: "", onClick: {} },
					{ name: "Question set", link: "", onClick: {} },
				],
			},
			{
				title: "",
				items: [
					{ name: "Grades", link: "", onClick: {} },
					{ name: "Grading Mechanism", link: "", onClick: {} },
				],
			},
			{
				title: "",
				items: [
					{ name: "Import", link: "", onClick: {} },
					{ name: "Synchronize Section", link: "", onClick: {} },
				],
			},
		],
	};
	const sideBarItems = {
		title: title,
		sections: [
			{
				title: "section1",
				items: Tabs?.map((tab, index) => {
					return {
						disabled: tab.disabled,
						name: tab.title,
						link: "",
						onClick: () => changeTab(tab.title, index),
						content: tab.content,
					};
				}),
			},
		],
	};

	/* {title: sections:[{title:"",items:items:{name,link,onClick}]}}] */
	/* RSecondarySideBar(sideBarItems)

map(item=>
<RSideBarItem item={item}/>
 */

	//------------------------------------------------0
	return (
		<div style={{ display: "flex" }}>
			<RSecondarySidebar sideBarItems={sideBarItems} />
			{activeCom}
			<div>
				{" "}
				{/* {Tabs.map((tab, index) => {  tab.content();})} */}
				{sideBarItems?.sections?.[0].items?.[activeCom]?.content()}
			</div>
		</div>
	);
	return (
		<>
			{Tabs?.map((tab, index) => {
				return (
					<NavLink
						onMouseUp={() => {
							history.replace(Url ? `${Url}${tab.title}` : "#");
						}}
						onClick={() => changeTab(tab.title, index)}
						disabled={tab.disabled}
					>
						{tr(tab.title)}
					</NavLink>
				);
			})}
		</>
	);
};

export default RSidebarPanel;
