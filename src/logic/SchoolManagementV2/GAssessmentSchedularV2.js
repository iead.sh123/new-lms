import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import * as actions from 'store/actions/global/schoolManagement.action'
import { useDispatch, useSelector } from 'react-redux'
import RHoverComponent from 'components/Global/RComs/RHoverComponent/RhoverComponent'
import { Input } from 'reactstrap'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import { assessmentsTypes, types } from './constants'
import { SET_ASSESSMENT_SAVED, SET_ASSESSMENT_TOUCHED, CHANGE_ASSESSMENT_INPUT, SET_ASSESSMENT_TIMESTAMP } from 'store/actions/global/globalTypes'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RButton from 'components/Global/RComs/RButton'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import GCalender from 'logic/calender/GCalender'
import Loader from 'utils/Loader'
import { SAVE_ASSESSMENT_INPUT } from 'store/actions/global/globalTypes'
import { UNSAVE_ASSESSMENT_INPUT } from 'store/actions/global/globalTypes'
import { useMutateData } from 'hocs/useMutateData'
const GAssessmentSchedularV2 = ({
    currentAssessments,
    allAssessments,
    setFieldValue,
    setFieldTouched,
    errors,
    touched,
    handleChange,
    handleBlur,
    levelId
}) => {

    const [openCalendarModal, setOpenCalendarModal] = useState(false)

    const createUpdateAssessmentMutation = useMutateData({
        queryFn: ({ payload }) => { schoolManagementV2API.addUpdateContentToLevel(payload) },
        onSuccessFn: ({ variables }) => {
            // handleEditFunctionality(null, variables.assessment, null, true)
        }
    })



    const handleEditFunctionality = (event, assessment, extraInfo, savedFlage) => {
        setFieldValue(`${types.Assessments}.${assessment?.index}.saved`, savedFlage)
    }
    const handleCloseModal = () => {
        setOpenCalendarModal(false)
    }
    let disableCreateEvents = false

    const handleNewAssessment = () => {
        if (errors?.[types.Assessments]) {
            setFieldTouched(`${types.Assessments}.${allAssessments?.length - 1}.name`, true)
            // setFieldTouched(`${types.Assessments}.${assessments?.length - 1}.from`, true)
            return
        }
        const fakeAssessment = {
            fakeId: -allAssessments.length + 1,
            name: "",
            to: "",
            from: "",
            saved: false,
            level_id: levelId,
            index: allAssessments?.length == 0 ? 0 : allAssessments.length
        }
        setFieldValue(`${types.Assessments}`, [...allAssessments, fakeAssessment])
    }

    const handleOnBlur = async (event, assessment, { type }) => {
        if (errors?.[types.Assessments]?.[period?.index]) {
            if (propertyName == 'name' && errors?.[types.Assessments]?.[assessment?.index]?.name)
                return
            else if (propertyName == 'name')
                handleEditFunctionality(null, assessment, null, true)
            return
        }
        const payload = {
            content_type_id: organizationTypes[types.Assessments],
            level_id: levelId,
            entity: {
                ...assessment,
            }
        }
        createUpdateAssessmentMutation.mutate({ payload })
    }

    const deleteAssessment = (assessment) => {

    }

    return (
        currentAssessments.length <= 0 ?
            <RFlex className='flex-column' styleProps={{ gap: "5px" }}>
                <RFlex>
                    <RFlex className="align-items-center">

                        <span className='p-0 m-0 font-weight-bold'>Assessment Schedular</span>
                        <i
                            className='fas fa-calendar-alt p-0 m-0 text-primary'
                            style={{ cursor: 'pointer' }}
                            onClick={() => setOpenCalendarModal(true)} />
                    </RFlex>
                    <AppModal
                        show={openCalendarModal}
                        size={"lg"}
                        header={true}
                        headerSort={<GCalender />}
                        parentHandleClose={handleCloseModal} />
                </RFlex>
                <RButton text="New Schedular"
                    faicon='fa-solid fa-plus'
                    color="link"
                    className="text-primary m-0 p-0"
                    style={{ width: 'fit-content' }}
                    onClick={handleNewAssessment} />
            </RFlex> :

            <RFlex className="flex-column" styleProps={{ width: '100%', gap: '5px' }}>
                <RFlex className="justify-content-between align-items-center">
                    <RFlex className="align-items-center">

                        <span className='p-0 m-0 font-weight-bold'>{tr("Assessment_Schedular")}</span>
                        <i
                            className='fas fa-calendar-alt p-0 m-0 text-primary'
                            style={{ cursor: 'pointer' }}
                            onClick={() => setOpenCalendarModal(true)} />
                        < RButton
                            text={tr("AddEvents")}
                            color="link"
                            disabled={disableCreateEvents}
                            className="text-primary m-0 p-0"
                            onClick={() => { }} />
                    </RFlex>
                    <AppModal
                        show={openCalendarModal}
                        size={"lg"}
                        header={true}
                        headerSort={<GCalender />}
                        parentHandleClose={handleCloseModal} />

                    <RFlex className="justify-content-end" styleProps={{ width: 'fit-content' }}>
                        <RButton text={tr("New Schedular")}
                            faicon='fa-solid fa-plus'
                            color="link"
                            className="text-primary m-0 p-0"
                            style={{ width: 'fit-content' }}
                            onClick={handleNewAssessment} />

                    </RFlex>
                </RFlex>
                <RFlex className="flex-column justify-content-center" styleProps={{ width: '100%', gap: '5px' }}>
                    {currentAssessments?.map((assessment, index) => {
                        const titleError = errors?.[types.Assessments]?.[assessment?.index]?.title
                        const titleTouched = touched?.[types.Assessments]?.[assessment?.index]?.title
                        const dateError = errors?.[types.Assessments]?.[assessment?.index]?.from
                        const dateTouched = touched?.[types.Assessments]?.[assessment?.index]?.from
                        return (
                            <RFlex className={`justify-content-between ${(titleError || titleTouched) && (dateError && dateTouched) ? "align-items-baseline" : "align-items-center"}`}
                                styleProps={{ width: '95%', gap: '7px' }}>
                                <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                    <RHoverInput
                                        name={`${types.Assessments}.${assessment.index}.name`}
                                        inputValue={assessment.name}
                                        inputWidth='100px'
                                        inputPlaceHolder=""
                                        handleInputChange={handleChange}
                                        inputIsNotValidate={titleError && titleTouched || true}
                                        item={assessment}
                                        handleOnBlur={(event, assessment, extraInfo) => { handleBlur(event); handleOnBlur(event, assessment, extraInfo) }}
                                        handleEditFunctionality={handleEditFunctionality}
                                        saved={assessment.saved}
                                    />
                                    {/* {assessment.touched['name'] && assessment.name == "" && <p className='p-0 m-0 text-danger'>Name is Required</p>} */}

                                </RFlex>
                                <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                    <Input
                                        name="dueDate"
                                        type="date"
                                        placeholder={tr`due_time`}
                                        onBlur={(event, assessment, extraInfo) => { handleBlur(event); handleOnBlur(event, assessment, extraInfo) }}
                                        value={assessment.timestamp || null}
                                        onChange={handleChange}
                                        style={{ height: "40px", width: "150px" }}
                                        className={dateError && dateTouched ? "input__error" : ""}
                                    />
                                    {/* {assessment.touched['timestamp'] && !assessment.timestamp && <p className='p-0 m-0 text-danger'>Date is Required</p>} */}

                                </RFlex>
                                <i
                                    className={`${iconsFa6.delete} text-danger`}
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => deleteAssessment(assessment)} />
                            </RFlex>
                        )
                    })}
                </RFlex>
            </RFlex>
    )
}

export default GAssessmentSchedularV2