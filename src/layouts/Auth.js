import React, { useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom";
import PerfectScrollbar from "perfect-scrollbar";
import AuthImage from "assets/img/new/auth.jpg";
import routes from "routes/auth";

var ps;

function Pages() {
  const fullPages = React.useRef();
  const [viewportWidth, setViewportWidth] = useState(window.innerWidth);
  useEffect(() => {
    function handleResize() {
      // Use window.visualViewport.width to get the viewport width accounting for zoom
      setViewportWidth(window.visualViewport.width);
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(fullPages.current);
    }
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
    };
  });

  const getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return getRoutes(prop.views);
      }

      if (prop.layout === process.env.REACT_APP_BASE_URL + "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  return (
    <div className="auth__container" ref={fullPages}>
      {viewportWidth > 767 && <img className="auth__image" src={AuthImage} />}
      <Switch>{getRoutes(routes)}</Switch>
    </div>
  );
}

export default Pages;
