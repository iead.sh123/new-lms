import * as yup from "yup";

// const itemSchema = yup.object().shape({
//   text: yup.string().required("Text is required"),
//   items: yup.array().nullable().of(itemSchema),
// });

export const validationUnitPlanSchema = yup.object().shape({
  title: yup.string().required("Title is required"),
  unit_number: yup
    .number()
    .typeError("Unit number is required")
    .required("Unit number is required"),
  time: yup.number().typeError("Time is required").required("Time is required"),
  description: yup.string().required("Description is required"),

  uplsection: yup
    .array()
    .nullable()
    .of(
      yup.object().shape({
        name: yup.string().required("Name is required"),
        description: yup.string().required("Description is required"),
        // items: yup.array().nullable().of(itemSchema),
      })
    ),
});
