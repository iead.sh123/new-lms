import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton"
import RProfileName from "../../../../components/Global/RComs/RProfileName/RProfileName"
import { FormGroup, Input } from "reactstrap";

const radioButton=<AppRadioButton style={{height:'25px' ,width:'25px' }}/>
const Profile_name=<RProfileName/>

export function randomIntFromInterval(min=0, max=3) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

export const presented_html_value=<div><span>Presented Days</span > <span style={{color:'#585858'}}>out of 150</span></div>
export const DummyTodayRecords =
    [
        {
            id: 1,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component' },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 2,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 3,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 4,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 5,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 6,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 7,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 8,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },
        {
            id: 9,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students', value: Profile_name ,type:'component'  },
                { key: "L", value: radioButton ,type:'component' },
                { key: "LE", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "A", value: radioButton ,type:'component' },
                { key: "AE", value: radioButton ,type:'component' }
            ]

        },


    ]
export const DummyOverallRecords =
    [
        {
            id: 1,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'85%',fontSize:'18px',color:'#46C37E'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 2,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'71%',fontSize:'18px',color:'#46C37E'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 3,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'45%',fontSize:'18px',color:'#D00'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 4,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'32%',fontSize:'18px',color:'#D00'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 5,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'65%',fontSize:'18px',color:'#46C37E'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 6,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'25%',fontSize:'18px',color:'#D00'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 7,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'95%',fontSize:'18px',color:'#46C37E'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 8,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'80%',fontSize:'18px',color:'#46C37E'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },
        {
            id: 9,
            table_name: 'GAttendanceDetailsLister',
            details: [
                { key: 'Students',value: Profile_name ,type:'component' },
                { key: "Overall",keyStyle:{color:'#668AD7',fontSize:'18px'},value:'0%',fontSize:'18px',color:'#D00'},
                { key: presented_html_value, value: "150 Days" ,fontSize:"16px",color:"#585858" },
            ],
            actions:[
                {name:'Attendance Details',icon:'fa-solid fa-message',onClick:()=>{}},
                {name:'send student a message',icon:'fa-solid fa-user',onClick:()=>{}}
            ]

        },


    ]
export const SettingsWithPermissions=[
    {
        id:1,
        details:[
            {key:"Attendance Status",value:"Presented"},
            {key:"Acronym",value:"P"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],
        actions:[
            {name:"Edit",icon:"fa-solid fa-edit",onClick:()=>{}},
            {name:"Delete",icon:"fa-solid fa-trash",color:'#D00',iconColor:'#D00',onClick:()=>{}},

        ]
    },
    {
        id:2,
        details:[
            {key:"Attendance Status",value:"Late"},
            {key:"Acronym",value:"L"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],
        actions:[
            {name:"Edit",icon:"fa-solid fa-edit",onClick:()=>{}},
            {name:"Delete",icon:"fa-solid fa-trash",color:'#D00',iconColor:'#D00',onClick:()=>{}},

        ]
    },
    {
        id:3,
        details:[
            {key:"Attendance Status",value:"Late With Excuse"},
            {key:"Acronym",value:"LE"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],
        actions:[
            {name:"Edit",icon:"fa-solid fa-edit",onClick:()=>{}},
            {name:"Delete",icon:"fa-solid fa-trash",color:'#D00',iconColor:'#D00',onClick:()=>{}},

        ]
    },
    {
        id:4,
        details:[
            {key:"Attendance Status",value:"Absent"},
            {key:"Acronym",value:"A"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],
        actions:[
            {name:"Edit",icon:"fa-solid fa-edit",onClick:()=>{}},
            {name:"Delete",icon:"fa-solid fa-trash",color:'#D00',iconColor:'#D00',onClick:()=>{}},

        ]
    },
    {
        id:5,
        details:[
            {key:"Attendance Status",value:"Absent With Excuse"},
            {key:"Acronym",value:"AE"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],
        actions:[
            {name:"Edit",icon:"fa-solid fa-edit",onClick:()=>{}},
            {name:"Delete",icon:"fa-solid fa-trash",color:'#D00',iconColor:'#D00',onClick:()=>{}},

        ]
    },
]
export const SettingsWithoutPermissions=[
    {
        id:1,
        details:[
            {key:"Attendance Status",value:"Presented"},
            {key:"Acronym",value:"P"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],

    },
    {
        id:2,
        details:[
            {key:"Attendance Status",value:"Late"},
            {key:"Acronym",value:"L"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],

    },
    {
        id:1,
        details:[
            {key:"Attendance Status",value:"Late With Excuse"},
            {key:"Acronym",value:"LE"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],

    },
    {
        id:1,
        details:[
            {key:"Attendance Status",value:"Absent"},
            {key:"Acronym",value:"A"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],

    },
    {
        id:1,
        details:[
            {key:"Attendance Status",value:"Absent With Excuse"},
            {key:"Acronym",value:"AE"},
            {key:"Points",value:"10"},
            {key:"by default set when not market",value:radioButton,type:'component'},
        ],

    },
]


export const attendanceSettingsTypes=['Presented','Late','Late With Excuse','Absent','Absent With Excuse']
export const overallInfo=['Overall','PresentedDays']
export const students2=[
    {id:1,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:2,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:3,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:4,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:5,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:6,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:7,name:'moaed',icon:'',overall:75,predented_days:100},
    {id:8,name:'moaed',icon:'',overall:75,predented_days:100}
]
export const titleTypes=['Leason Title','Exam Title','Live Session Title','Assignment Title']


export const DummyContentForSelector = [{ label: 'This is item2', value: 1 }, { label: "this is item2", value: 2 }]

 
