import react, { useState } from "react"
import RRLiveSessions from "./RRLiveSessions";
import RUnpublished from "./RUnpublished";

function GUnpublished({data,published}){
 
    return(
        <div style={{width:'100%',marginLeft:'1rem'}}>
           
            {
            
            data?.map(i=>
            
            
           
             (<>
          
            <RUnpublished
            icon={i.icon}
            course={i.course}
            subName={i.subName}
            publishCallback={i.publishCallback}
            />
            </>
            )
               
        )
            }
         
        </div>
    )
}
export default GUnpublished;