import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import { Terms } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import tr from "components/Global/RComs/RTranslator";
import RHeader from "../Shared/RHeader";
import RUsersData from "../Shared/RUsersData";
import RCSVDrawer from "../Shared/RCSVDrawer";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLoader from "components/Global/RComs/RLoader";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useMutateData } from "hocs/useMutateData";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";
import { fileExcel } from "config/mimeTypes";
const GUsers = () => {
	const [backUsersSearchTerm, setBackUserSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [removeIdsLoading, setRemoveIdsLoading] = useState([]);
	const [activeIdsLoading, setActiveIdsLoading] = useState([]);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const [alert1, setAlert] = useState(false);
	const organizationId = 1;
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const history = useHistory();
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};
	const dropdownActions = [
		{
			label: tr("Bulk_delete"),
			action: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "delete" });
			},
		},
		{
			label: tr("Import_CSV"),
			action: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			label: tr("Export_CSV"),
			action: () => {
				exportUserMutaiton.mutate();
			},
		},
	];
	//--------------------------------Members------------------------------------------------
	//-------------------------------import/export users------------------------------------
	const importUsersTempalteMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadUsersTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users Template.xlsx",
	});
	const exportUserMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportUsers(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users.xlsx",
	});
	//-----------------------------------get Users with filters applyied-----------------------
	const {
		data: members,
		isLoading: isLoadingUsers,
		isFetching: isFetchingUsers,
		refetch: refetchUsers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);

			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	//-------------------------------------get users types for filter------------------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};
	//----------------------------removing users--------------------------------
	const handleRemoveMembers = ({ members }) => {
		const userIds = members.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		removeUsersMutation.mutate({ payload });
		setRemoveIdsLoading([...removeIdsLoading, ...userIds]);
	};
	const deleteSingleMember = (member) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`} <span className="p-0 m-0">{member.full_name}</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members: [member] }, message, confirm);
	};
	const deleteMultipleMembers = (members) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`} <span className="p-0 m-0">{tr("the_selected_users")}</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members }, message, confirm);
	};
	const removeUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeUserFromOrganization(organizationId, payload),
		onSuccessFn: ({ variables }) => {
			setSelectedMembers(selectedMembers.filter((member) => !variables?.payload?.user_ids?.includes(member.id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.DELETE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					insertionDepth: "data.data.records",
				});
			});
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		onErrorFn: (error, variables) => {
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		invalidateKeys: [Terms.Members, allMembersFilters],
	});
	//------------------------------------activation status----------------------------------
	const deactivateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.deactivateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: false },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const activateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.activateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: true },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const changeMultipleUserStatus = () => {
		if (selectedMembers.length <= 0) return;
		const userIds = selectedMembers.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		deactivateUsersMutation.mutate({ payload });
		setActiveIdsLoading([...activeIdsLoading, ...userIds]);
	};
	const changeSingleUserStatus = (user) => {
		const payload = {
			user_ids: [user.id],
		};
		if (user.is_active) {
			deactivateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		} else {
			activateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		}
	};
	//----------------------------------------global------------------------------------------
	if (isLoadingUsers || isLoadingUserTypes) {
		return <RLoader />;
	}
	return (
		<RFlex className="flex-column">
			<RHeader
				actions={dropdownActions}
				createText={"New User"}
				onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/users/add`)}
				showCreate={true}
				showSearchHeader={true}
				showBulk={true}
				showColumnSettings={selectedMembers.length > 0 ? false : true}
				showRefresh={selectedMembers.length > 0 ? false : true}
				refetchFunction={refetchUsers}
				isFetching={isFetchingUsers}
				deactivateFn={changeMultipleUserStatus}
				setBackSearchTerm={setBackUserSearchTerm}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				showDeactivate={selectedMembers.length > 0}
				showCreateGroup={selectedMembers.length > 0}
				showAddToGroup={selectedMembers.length > 0}
				showSendEmail={selectedMembers.length > 0}
				showDelete={selectedMembers.length > 0}
				deleteFn={() => deleteMultipleMembers(selectedMembers)}
				addSearchToFilters
			/>

			<RUsersData
				users={members?.data?.data}
				userTypes={userTypes?.data?.data}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				selectedUsers={selectedMembers}
				selectOneFn={handleSelectMember}
				selectAllFn={handleSelectAllMembers}
				deleteSingleMember={deleteSingleMember}
				isRemoving={removeUsersMutation.isLoading}
				changeSingleUserStatus={changeSingleUserStatus}
				activeIdsLoading={activeIdsLoading}
				removeIdsLoading={removeIdsLoading}
				view
			/>

			<RCSVDrawer
				downloadTemplateFn={() => importUsersTempalteMutation.mutate()}
				isOpen={isDrawerOpen.isOpen}
				operationType={isDrawerOpen.operationType}
				toggleDrawer={toggleDrawer}
				currentType={Terms.Users}
			/>
			{alert1}
		</RFlex>
	);
};

export default GUsers;
