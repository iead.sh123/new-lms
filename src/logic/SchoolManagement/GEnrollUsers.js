import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import React, { useEffect, useState } from "react";
import * as colors from "config/constants";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";
import { dataTypes } from "logic/SchoolManagement/constants";
import * as actions from "store/actions/global/schoolManagement.action";
import { useDispatch, useSelector } from "react-redux";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RLister from "components/Global/RComs/RLister";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GCreateUser from "../TermsManagement/GCreateUser";
import { termsManagementApi } from "api/global/termsManagement";
import { useMutateData } from "hocs/useMutateData";
const GEnrollUsers = ({ levelId, type, closeModal }) => {
	const dispatch = useDispatch();
	const { candidatePrincipals, candidateStudents } = useSelector((state) => state.schoolManagementRed);
	const [searchData, setSearchData] = useState("");
	const [openCreateUserModal, setOpenCreateUserModal] = useState(false);
	const data = type == dataTypes.Principals ? candidatePrincipals : type == dataTypes.Students ? candidateStudents : null;
	const selectedUsers = data.ids.length > 0 ? data.ids.filter((key) => data[key].selected).length : 0;

	const { first_page_url, last_page_url, current_page, last_page, prev_page_url, next_page_url, total } = data;
	const handleCloseModal = () => {
		setOpenCreateUserModal(false);
	};
	const handleUserClicked = (user) => {
		if (!user.selected) {
			dispatch(actions.selectCandidate(type, user.id));
		} else {
			dispatch(actions.unSelectCandidate(type, user.id));
		}
	};
	const handleSaveUsers = () => {
		if (type == dataTypes.Principals) {
			dispatch(actions.addPrincipalsToStage());
		} else if (type == dataTypes.Students) {
			dispatch(actions.addStudentsToGrade());
		}
		closeModal();
	};
	const handleSearch = (clearData) => {
		const data = clearData == "" ? clearData : searchData;
		if (type == dataTypes.Principals) {
			dispatch(actions.getCandidatePrincipalForEducation(null, { educationStageId: levelId, prefix: data }));
		} else if (type == dataTypes.Students) {
			dispatch(actions.getCandidateStudentsForGrade(null, { gradeLevelId: levelId, prefix: data }));
		}
	};
	const userMutation = useMutateData({
		queryFn: ({ userId, data }) => termsManagementApi.addUserToOrganization({ userId, data }),
		// invalidateKeys: ['teachers', 'search'],
		invalidateKeys: [""],
		dispatch: true,
		action: dataTypes.Principals == type ? actions.createPrincipale : actions.createStudent,
		closeDialog: () => setOpenCreateUserModal(false),
	});
	const _records =
		data.ids.length > 0
			? data.ids.map((key) => {
					const name = type == dataTypes.Principals ? data[key].name : data[key].first_name + " " + data[key].last_name;
					const couresNameComponent = (
						<RProfileName name={name} img={data[key].image ? data[key].image : null} imgStyle={{ width: "18px", height: "18px" }} />
					);
					return {
						details: [
							{ key: type == dataTypes.Principals ? tr`Principale Name` : tr`Student Name`, value: couresNameComponent, type: "component" },
						],
						actions: [
							{
								name: " ",
								icon: data[key].selected ? iconsFa6.minusSolid : iconsFa6.plus,
								color: data[key].selected ? "danger" : "primary",
								outline: true,
								// loading: courses.loading,
								onClick: () => handleUserClicked(data[key]),
							},
						],
					};
			  })
			: null;
	useEffect(() => {
		if (type == dataTypes.Principals) {
			dispatch(actions.getCandidatePrincipalForEducation(null, { educationStageId: levelId }));
		} else if (type == dataTypes.Students) {
			dispatch(actions.getCandidateStudentsForGrade(null, { gradeLevelId: levelId }));
		}
	}, []);
	return data.loading ? (
		<Loader />
	) : (
		<RFlex className="flex-column" styleProps={{ gap: "5px" }}>
			<RSearchHeader
				// searchLoading={data.ids}
				searchData={searchData}
				handleSearch={handleSearch}
				setSearchData={setSearchData}
				handleChangeSearch={(value) => setSearchData(value)}
			/>
			<RFlex className="justify-content-between align-items-center" styleProps={{ width: "100%" }}>
				<RFlex className="align-items-center">
					<span className="p-0 m-0">{selectedUsers} Selected</span>
					{/* <span
                            style={{ color: colors.primaryColor, textDecoration: 'underline', cursor: 'pointer' }}
                            onClick={() => { handleSaveUsers() }}>Save</span> */}
					<RButton
						text="Save"
						disabled={selectedUsers <= 0}
						color="link"
						className="text-primary m-0 p-0"
						style={{ width: "fit-content", color: colors.primaryColor, textDecoration: "underline", cursor: "pointer" }}
						onClick={handleSaveUsers}
					/>
					<AppModal
						show={openCreateUserModal}
						size={"md"}
						header={true}
						headerSort={<GCreateUser userType={type} addUserMutate={userMutation} gradeLevelId={null} educationStageId={null} />}
						parentHandleClose={handleCloseModal}
					/>
				</RFlex>
				<RButton
					text="Create Account"
					onClick={() => setOpenCreateUserModal(true)}
					faicon={iconsFa6.plus}
					color="primary"
					outline
					style={{ margin: "0px", padding: "5px 10px", height: "32px" }}
				/>
			</RFlex>
			{<RLister Records={_records ?? null} stickyHeader={true} />}
			{/* {<NewPaginator
                    firstPageUrl={first_page_url}
                    lastPageUrl={last_page_url}
                    currentPage={current_page}
                    lastPage={last_page}
                    prevPageUrl={prev_page_url}
                    nextPageUrl={next_page_url}
                    total={total}
                    data={type == dataTypes.Principals ? { educationStageId: levelId, prefix: searchData } : { gradeLevelId: levelId, prefix: searchData }}
                    getAction={type == dataTypes.Principals ? actions.getCandidatePrincipalForEducation
                        : type == dataTypes.Students ? actions.getCandidateStudentsForGrade : null}
                />} */}
		</RFlex>
	);
};

export default GEnrollUsers;
