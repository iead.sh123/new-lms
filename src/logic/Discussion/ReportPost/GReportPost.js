import React, { useEffect } from "react";
import { getAllReportCategoriesAsync } from "store/actions/global/discussions.actions";
import { useDispatch } from "react-redux";
import RCreateReport from "view/Discussion/RCreateReport";

const GReportPost = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllReportCategoriesAsync());
  }, []);

  return <RCreateReport />;
};

export default GReportPost;
