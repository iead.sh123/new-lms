import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../RUnitPlan.module.scss";
import { primaryColor } from "config/constants";
import RUnitPlanItems from "./RUnitPlanItems";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";

const RUnitPlanSections = ({ unitPlanSections }) => {
	return unitPlanSections?.map((unitPlanSection, sectionIndex) => {
		return (
			<RFlex key={unitPlanSection.id} className={styles.up__viewer__sections}>
				<RFlex className={styles.up__viewer__section}>
					<RFlex className={styles.up__viewer__items__section__container}>
						<h6 style={{ color: primaryColor }}>
							{sectionIndex + 1}.{unitPlanSection?.name}
						</h6>
						<span>{unitPlanSection?.description}</span>
						{unitPlanSection?.attachments?.length > 0 && (
							<RImageViewer
								mimeType={unitPlanSection?.attachments[0]?.mime_type}
								url={unitPlanSection?.attachments[0]?.hash_id}
								imageName={unitPlanSection?.attachments[0]?.name}
								showFullAttachment={true}
								width={"25px"}
								height={"25px"}
							/>
						)}
						{/* <div>rubric title Here</div> */}
					</RFlex>

					<RUnitPlanItems items={unitPlanSection.items} sectionIndex={sectionIndex + 1} />
				</RFlex>
			</RFlex>
		);
	});
};

export default RUnitPlanSections;
