import React, { useEffect, useState } from "react";
import DropdownList from "react-widgets/lib/DropdownList";

const GenericUISearch = ({
  table_props,
  getData,
  filterField,
  setFilterField,
  filterValue,
  setFilterValue,
}) => {
  const [fields, setFields] = useState([]);

  useEffect(() => {
    let temp = [];
    for (let i = 0; i < table_props.length; i++) {
      if (table_props[i].Type.indexOf("varchar") !== -1) {
        temp.push(table_props[i].Field);
      }
    }
    setFields(temp);
  }, [table_props]);

  const handleSearch = () => {
    getData();
  };

  return (
    <>
      <div className="form-search">
        <div className="container">
          <div className="row">
            <div className="col-md-5 col-sm-6 col-xs-12">
              <div className="form-group">
                <select
                  className="form-control"
                  value={filterField}
                  onChange={(e) => {
                    setFilterField(e.target.value);
                  }}
                >
                  <option key={0} value={0}>
                    Select a Field ...
                  </option>
                  {fields?.map((field) => {
                    return (
                      <option id={field} key={field} value={field}>
                        {field}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <div className="col-md-5 col-sm-6 col-xs-12">
              <div className="form-group">
                <input
                  type="text"
                  name="value"
                  className="form-control"
                  id="value"
                  placeholder="Value"
                  onChange={(e) => {
                    setFilterValue(e.target.value);
                  }}
                />
              </div>
            </div>
            <div className="col-md-2 col-sm-6 col-xs-12">
              <div className="form-group">
                <button
                  disabled={
                    filterField && filterValue
                      ? false
                      : !filterField && !filterValue
                      ? true
                      : false
                  }
                  onClick={() => {
                    handleSearch();
                  }}
                  className="btn btn-primary loginBtn"
                >
                  Search
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default GenericUISearch;
