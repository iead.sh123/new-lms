export const ChatTags = {
    Archived: 'archived',
    Starred: 'starred',
    Important: 'important',
    Muted: 'muted',
    Reported: 'reported',
    Urgent: 'urgent',
    Select: 'select',
   
    Delete: 'Delete',
    All: 'All'
}


export const CHAT_ACTIONS_ICONS = {
    [ChatTags.Starred]: 'fa fa-star',
    [ChatTags.Important]: 'fa fa-hot',
    [ChatTags.Reported]: 'fa-solid fa-flag',
    [ChatTags.Urgent]: 'fa fa-exclamation-triangle',
    [ChatTags.Delete]: 'fa-regular fa-trash-can',
    [ChatTags.Select]: 'fa fa-check-square',


    [ChatTags.Muted]: 'fa fa-bell-slash',
    [ChatTags.Archived]: 'fa fa-archive',
}
export const CHAT_ACTIONS_COLORS = {
    [ChatTags.Starred]: 'yellow',
    [ChatTags.Important]: '#668ad7',
    [ChatTags.Reported]: 'black',
    [ChatTags.Urgent]: 'orange',
    [ChatTags.Delete]: 'red',
    [ChatTags.Select]: 'green',


    [ChatTags.Muted]: 'black',
    [ChatTags.Archived]: 'black',
}
