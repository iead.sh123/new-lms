import React, { useContext, useState } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Row, Col, Input } from "reactstrap";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const Essay = ({ viewMode, lessonContent, content }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	const TEXT = lessonContent?.text?.text ?? lessonContent?.text;

	const [open, setOpen] = useState(false);
	const handleOpenModal = () => setOpen(true);
	const handleCloseModal = () => setOpen(false);

	return (
		<>
			<AppModal
				size="md"
				show={open}
				parentHandleClose={handleCloseModal}
				header={tr`essay_text`}
				headerSort={
					<RFlex styleProps={{ flexWrap: "wrap", wordBreak: "break-all" }}>
						<span
							className="p-3"
							dangerouslySetInnerHTML={{
								__html: TEXT,
							}}
						/>
					</RFlex>
				}
			/>

			{viewMode ? (
				TEXT.length > 150 ? (
					<RFlex styleProps={{ flexWrap: "wrap", wordBreak: "break-all" }}>
						<span
							className="p-3"
							dangerouslySetInnerHTML={{
								__html: TEXT.substring(0, 150),
							}}
						/>
						&nbsp;
						<span style={{ textDecoration: "underline", color: primaryColor }} onClick={handleOpenModal}>{tr`read_more`}</span>
					</RFlex>
				) : (
					<RFlex styleProps={{ flexWrap: "wrap", wordBreak: "break-all" }}>
						<span
							className="p-3"
							dangerouslySetInnerHTML={{
								__html: TEXT,
							}}
						/>
					</RFlex>
				)
			) : (
				<Row className="mt-4">
					<Col xs={12} className="mb-4">
						<Input
							type="text"
							name="name"
							onChange={(event) => {
								lessonContent.newData
									? CourseModuleData.handleAddContentToLessonContent(content, lessonContent, event.target.name, event.target.value, "text")
									: CourseModuleData.setEditLessonTextState({
											...CourseModuleData.editLessonTextState,
											name: event.target.value,
									  });
							}}
							placeholder={tr`essay_title`}
							defaultValue={lessonContent?.title}
						/>
					</Col>

					<Col xs={12} className="mb-4">
						<CKEditor
							editor={ClassicEditor}
							data={lessonContent?.text?.text ?? lessonContent?.text}
							onChange={(event, editor) => {
								const data = editor.getData();
								lessonContent.newData
									? CourseModuleData.handleAddContentToLessonContent(content, lessonContent, "text", data, "text")
									: CourseModuleData.setEditLessonTextState({
											...CourseModuleData.editLessonTextState,
											text: data,
									  });
							}}
						/>
					</Col>
					<Col xs={12} className="d-flex justify-content-end">
						{/* <RButton
							onClick={() =>
								lessonContent.newData
									? CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, true, "text")
									: CourseModuleData.handleEditLessonContent(
											CourseModuleData.specificModule.id,
											content.id,
											lessonContent.id,
											lessonContent.type
									  )
							}
							color="primary"
							text={tr`save_and_publish`}
							outline
							loading={CourseModuleData.lessonTextLoading}
							disabled={CourseModuleData.lessonTextLoading}
						/> */}
						<RButton
							onClick={() =>
								lessonContent.newData
									? CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, false, "text")
									: CourseModuleData.handleEditLessonContent(
											CourseModuleData.specificModule.id,
											content?.id,
											lessonContent.id,
											lessonContent.type
									  )
							}
							color="success"
							text={tr`save`}
							loading={CourseModuleData.lessonTextLoading}
							disabled={CourseModuleData.lessonTextLoading}
						/>
					</Col>
				</Row>
			)}
		</>
	);
};

export default Essay;
