import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { primaryColor } from "config/constants";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RUnitPlanItem from "./RUnitPlanItem";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import styles from "../../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RUnitPlanItems = ({ sectionId, items }) => {
	const { width } = useWindowDimensions();
	const mobile = width < 800;
	const UnitPlanContextData = useContext(UnitPlanContext);

	return (
		<React.Fragment>
			{items.map(
				(item) =>
					item?.visible && (
						<div
							key={item?.id ? item?.id : item?.fakeId}
							ref={(ref) => {
								UnitPlanContextData.targetRefs.current[item.id ? item.id : item.fakeId] = ref;
							}}
						>
							<div className={styles.form_items}>
								<Row>
									<Col sm={6} xs={12}>
										<Col className="pl-0 pb-2">
											<Form>
												<FormGroup>
													<Input
														id="text"
														required
														type="text"
														onChange={(event) =>
															UnitPlanContextData.handleAddItemsToSection(
																sectionId,
																item?.id ? item?.id : item?.fakeId,
																"text",
																event.target.value,
																"parent",
																true
															)
														}
														value={item?.text}
													/>
												</FormGroup>
											</Form>
										</Col>

										<Col className="pl-0">
											<RFlex styleProps={{ flexWrap: "wrap", gap: "18px 58px " }}>
												{item.items && item.items.length > 0 && (
													<RUnitPlanItem sectionId={sectionId} items={item.items} itemId={item?.id ? item?.id : item?.fakeId} />
												)}
												<div
													className={styles.card_add_item}
													onClick={() => {
														UnitPlanContextData.handleOpenAddItem();
														UnitPlanContextData.setUnitPlanSectionId(sectionId);
														UnitPlanContextData.setUnitPlanItemId(item?.id ? item?.id : item?.fakeId);
														UnitPlanContextData.setTypeToPick("item");
													}}
												>
													<RFlex
														styleProps={{
															flexDirection: "column",
															alignItems: "center",
															position: "absolute",
															top: "50%",
															left: "50%",
															transform: " translate(-50%, -50%)",
															fontWeight: "normal",
															width: "100%",
														}}
													>
														<i className={iconsFa6.plus + " fa-2x"} />
														<h6 className="text-capitalize">{tr`new_item`}</h6>
													</RFlex>
												</div>
											</RFlex>
										</Col>
									</Col>
									<Col sm={6} xs={12} className={`mb-4 ${styles.attachment_border}`}>
										<Col className="mb-2">
											<RFileSuite
												parentCallback={(event) =>
													UnitPlanContextData.handleAddItemsToSection(
														sectionId,
														item?.id ? item?.id : item?.fakeId,
														"attachments",
														event,
														"parent",
														true
													)
												}
												value={item?.attachments}
												singleFile={true}
												binary={true}
											/>
										</Col>
										<RFlex
											styleProps={{
												justifyContent: "space-between",
												padding: "0px 15px",
												marginBottom: "15px",
											}}
										>
											<UncontrolledDropdown direction="down">
												<DropdownToggle aria-haspopup={true} color="primary" data-toggle="dropdown" outline>
													<RFlex styleProps={{ alignItems: "center" }}>
														{tr`pick`}
														<i class="fa fa-chevron-down" aria-hidden="true"></i>
													</RFlex>
												</DropdownToggle>
												<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
													<DropdownItem
														key={1}
														disabled={item.lesson_plan_id ? true : false}
														onClick={() => {
															UnitPlanContextData.setUnitPlanSectionId(sectionId);
															UnitPlanContextData.setUnitPlanItemId(item?.id ? item?.id : item?.fakeId);
															UnitPlanContextData.handleOpenPickALessonPlan();
															UnitPlanContextData.setTypeToPick("item");
														}}
													>
														{tr`a_lesson_plan `}
													</DropdownItem>
													<DropdownItem divider />
													<DropdownItem
														key={1}
														disabled={item.rubric_id ? true : false}
														onClick={() => {
															UnitPlanContextData.setUnitPlanSectionId(sectionId);
															UnitPlanContextData.setUnitPlanItemId(item?.id ? item?.id : item?.fakeId);
															UnitPlanContextData.handleOpenPickARubric();
															UnitPlanContextData.setTypeToPick("item");
														}}
													>
														{tr`a_rubric`}
													</DropdownItem>
												</DropdownMenu>
											</UncontrolledDropdown>
											{/* <RFlex>
												<div className={styles.choose_resource}>
													<FontAwesomeIcon icon={faDownload} style={{ color: primaryColor }} />
													<h6 className={styles.name_choose_resource} style={{ color: primaryColor }}>
														{tr`choose_from_resources`}
													</h6>
												</div>
											</RFlex> */}
										</RFlex>
										{/* rubric */}
										{item.rubric_id && (
											<RFlex
												styleProps={{
													padding: "0px 15px",
												}}
											>
												<i
													className="fa fa-times-circle-o fa-lg"
													aria-hidden="true"
													style={{ color: "red", cursor: "pointer" }}
													onClick={() =>
														UnitPlanContextData.handleRemoveAttachmentFromItem(sectionId, item?.id ? item?.id : item?.fakeId, "rubric")
													}
												/>
												<i
													class="fa fa-refresh"
													aria-hidden="true"
													onClick={() => {
														UnitPlanContextData.setUnitPlanSectionId(sectionId);
														UnitPlanContextData.setUnitPlanItemId(item?.id ? item?.id : item?.fakeId);
														UnitPlanContextData.handleOpenPickARubric();
														UnitPlanContextData.setTypeToPick("item");
													}}
													style={{ cursor: "pointer" }}
												/>
												<h6>{item.rubric.title}</h6>
											</RFlex>
										)}
										{/* lesson_plan */}
										{item.lesson_plan_id && (
											<RFlex
												styleProps={{
													padding: "0px 15px",
												}}
											>
												<i
													className="fa fa-times-circle-o fa-lg"
													aria-hidden="true"
													style={{ color: "red", cursor: "pointer" }}
													onClick={() =>
														UnitPlanContextData.handleRemoveAttachmentFromItem(sectionId, item?.id ? item?.id : item?.fakeId, "lesson_plan")
													}
												/>
												<i
													class="fa fa-refresh"
													aria-hidden="true"
													style={{ cursor: "pointer" }}
													onClick={() => {
														UnitPlanContextData.setUnitPlanSectionId(sectionId);
														UnitPlanContextData.setUnitPlanItemId(item?.id ? item?.id : item?.fakeId);
														UnitPlanContextData.handleOpenPickALessonPlan();
														UnitPlanContextData.setTypeToPick("item");
													}}
												/>
												<h6>{item?.lesson_plan?.name}</h6>
											</RFlex>
										)}
									</Col>
								</Row>

								<RFlex styleProps={{ justifyContent: "end", padding: "0px 15px" }}>
									<i
										className={iconsFa6.delete}
										style={{ color: "#DD0000", cursor: "pointer" }}
										onClick={() => UnitPlanContextData.handleRemoveItemFromSection(sectionId, item?.id ? item?.id : item?.fakeId)}
									/>
								</RFlex>
							</div>

							{item.items && item.items.length > 0 && <RUnitPlanItems sectionId={sectionId} items={item.items} />}
						</div>
					)
			)}
		</React.Fragment>
	);
};

export default RUnitPlanItems;
