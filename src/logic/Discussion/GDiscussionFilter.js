import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	searchInPostAsync,
	getAllCategoriesAsync,
	chooseSpecificCategoryAsync,
	emptyPostsArray,
	ignoreReportedContentAsync,
	deleteReportedContentAsync,
	deleteRejectedContentAsync,
	initialDataToCreatePost,
	setEditContentValues,
	editContentAsync,
	addLikeOnPostOrCommentAsync,
	emptyPeopleLikes,
	setCreateCommentValues,
	addCommentAsync,
} from "store/actions/global/discussions.actions";
import { useParams } from "react-router-dom";
import RDiscussionHeader from "view/Discussion/RDiscussionHeader";
import InfiniteScroll from "react-infinite-scroll-component";
import RDiscussion from "view/Discussion/RDiscussion";
import Loader from "utils/Loader";
import { acceptContentAsync, rejectContentContentAsync } from "store/actions/global/discussions.actions";
import GEditContent from "./EditContent/GEditContent";
import GShowAllPeople from "./ShowAllPeople/GShowAllPeople";

export const DiscussionFilterContext = React.createContext();

const GDiscussionFilter = () => {
	const dispatch = useDispatch();
	const { cohortId, categoryName } = useParams();
	const [textToSearch, setTextToSearch] = useState(null);
	const [contentId, setContentId] = useState(null);
	const [rejectedReason, setRejectedReason] = useState(null);
	const [openRejectReason, setOpenRejectReason] = useState(null);
	const [editPost, setEditPost] = useState(false);
	const [showAllPeople, setShowAllPeople] = useState(false);
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [commentId, setCommentId] = useState(false);

	const handleOpenEditContent = (contentId) => {
		setEditPost(true);
		setContentId(contentId);
	};

	const handleCloseEditContent = () => {
		setEditPost(false);
		setContentId(null);
	};

	const handleOpenSeeAllPeople = () => setShowAllPeople(true);

	const handleCloseSeeAllPeople = () => {
		setShowAllPeople(false);
		setContentId(null);
		dispatch(emptyPeopleLikes());
	};

	const {
		posts,
		editPost: ePost,
		categories,
		createComment,

		loadMorePage,
		postsLoading,
		categoriesLoading,
		acceptLoading,
		rejectLoading,
		ignoreReportedLoading,
		deleteReportedLoading,
		deleteRejectedLoading,
		editPostLoading,
		addLikeLoading,
		saveEditToContent,
		getCommentByIdLoading,
	} = useSelector((state) => state.discussionsRed);

	useEffect(() => {
		dispatch(emptyPostsArray());
		dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		dispatch(getAllCategoriesAsync(cohortId));
	}, [categoryName, cohortId]);

	const fetchData = () => {
		loadMorePage !== null && dispatch(chooseSpecificCategoryAsync(categoryName, loadMorePage, cohortId));
	};

	const handleSearch = (text) => {
		// dispatch(searchInPostAsync(text, loadMorePage));
	};

	const handleAcceptContent = (contentId) => {
		dispatch(acceptContentAsync(contentId, categoryName, cohortId));
	};
	const handleRejectContent = (contentId) => {
		dispatch(rejectContentContentAsync(contentId, rejectedReason, categoryName, cohortId));
	};

	const handleIgnoreReportedContent = (contentId) => {
		dispatch(ignoreReportedContentAsync(contentId, categoryName, cohortId));
	};
	const handleDeleteReportedContent = (contentId) => {
		dispatch(deleteReportedContentAsync(contentId, categoryName, cohortId));
	};

	const handleDeleteRejectedContent = (contentId) => {
		dispatch(deleteRejectedContentAsync(contentId, categoryName, cohortId));
	};

	const handleChangeEditContent = (name, value) => {
		dispatch(setEditContentValues(name, value));
	};

	const handleEditContent = () => {
		dispatch(editContentAsync(ePost, contentId, categoryName, handleCloseEditContent));
	};

	const handleAddLike = ({ content_id, type, post_id, comment_id, typeContent, postType }) => {
		const data = {
			content_id,
			type,
			post_id,
			comment_id,
			typeContent,
			postType,
		};
		dispatch(addLikeOnPostOrCommentAsync(data));
	};

	const handleCommentCollapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const handleChangeCreateComment = (name, value) => {
		dispatch(setCreateCommentValues(name, value));
	};

	const handleSaveComment = () => {
		dispatch(addCommentAsync(addComment, cohortId));
	};

	return (
		<React.Fragment>
			<DiscussionFilterContext.Provider
				value={{
					posts,
					ePost,
					categories,
					createComment,

					handleAcceptContent,
					handleRejectContent,
					handleIgnoreReportedContent,
					handleDeleteReportedContent,
					handleOpenEditContent,
					handleCloseEditContent,
					handleChangeEditContent,
					handleEditContent,
					handleDeleteRejectedContent,
					handleAddLike,
					handleOpenSeeAllPeople,
					handleCloseSeeAllPeople,
					handleCommentCollapsesToggle,
					handleChangeCreateComment,
					handleSaveComment,

					setCommentId,
					setTextToSearch,
					handleSearch,
					textToSearch,
					setContentId,
					setRejectedReason,
					openRejectReason,
					setOpenRejectReason,
					contentId,
					commentId,
					editPost,
					showAllPeople,

					postsLoading,
					categoriesLoading,
					acceptLoading,
					rejectLoading,
					ignoreReportedLoading,
					deleteReportedLoading,
					deleteRejectedLoading,
					addLikeLoading,
					getCommentByIdLoading,

					editPostLoading,
					saveEditToContent,
				}}
			>
				{editPost && <GEditContent />}
				{showAllPeople && <GShowAllPeople />}
				<RDiscussionHeader />
				{posts?.length > 0 ? (
					<div id="scrollableDiv2" style={{ height: "65vh", overflow: "auto" }}>
						<InfiniteScroll
							dataLength={posts?.length}
							next={fetchData}
							hasMore={loadMorePage !== null ? true : false}
							loader={loadMorePage == true && <Loader />}
							style={{ overflow: "hidden" }}
							scrollableTarget="scrollableDiv2"
						>
							<RDiscussion />
						</InfiniteScroll>
					</div>
				) : (
					<Loader />
				)}
			</DiscussionFilterContext.Provider>
		</React.Fragment>
	);
};

export default GDiscussionFilter;
