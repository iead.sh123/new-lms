import { produce } from "immer";
import {
  INITIALIZE,
  INITIALIZE_TYPES,
  BLOCK_EVENT_TYPE,
  UNBLOCK_EVENT_TYPE,
  LOAD_COMMENTS,
  LOAD_NOTES,
  ADD_COMMENT,
  ADD_NOTES,
} from "../constants/calender-actions.constant";

export const initialState = {
  eventTypesLoaded: false,
  entities: {
    event_types: {
      byId: {},
      ids: [],
    },
    events: {
      byId: {},
      ids: [],
    },
    // comments: {
    //     byId: {

    //     },
    //     ids: []
    // },
    // notes: {
    //     byId: {

    //     },
    //     ids: []
    // }
  },
};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  return produce(state, (draft) => {
    switch (type) {
      case INITIALIZE:
        draft.entities.events = payload.events;
        return draft;

      case INITIALIZE_TYPES:
        draft.eventTypesLoaded = true;
        draft.entities.event_types = payload;
        return draft;

      case BLOCK_EVENT_TYPE:
        draft.entities.event_types.byId[payload.blockerId].blocks = [
          ...draft.entities.event_types.byId[payload.blockerId].blocks,
          payload.blockedId,
        ];
        return draft;

      case UNBLOCK_EVENT_TYPE:
        draft.entities.event_types.byId[payload.blockerId].blocks =
          draft.entities.event_types.byId[payload.blockerId].blocks.filter(
            (bl) => bl !== payload.blockedId
          );
        return draft;

      case LOAD_COMMENTS:
        const { eventId: i, ...comments } = payload;
        draft.entities.events.byId[payload.eventId].comments =
          comments.comments;
        return draft;

      case LOAD_NOTES:
        const { eventId, ...notes } = payload;
        draft.entities.events.byId[payload.eventId].notes = notes.notes;
        return draft;

      case ADD_COMMENT:
        draft.entities.events.byId[payload.eventId].comments.byId[payload.id] =
          payload;
        draft.entities.events.byId[payload.eventId].comments.ids.push(
          payload.id
        );
        draft.entities.events.byId[payload.eventId].commentsCount++;
        return draft;

      case ADD_NOTES:
        draft.entities.events.byId[payload.eventId].notes.byId[payload.id] =
          payload;
        draft.entities.events.byId[payload.eventId].notes.ids.push(payload.id);
        draft.entities.events.byId[payload.eventId].notesCount++;
        return draft;
      default:
        return draft;
    }
  });
};
