import React from 'react';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText,InputGroup ,InputGroupAddon,Input  } from 'reactstrap';
import { baseURL, genericPath } from "engine/config";
import discover from "assets/img/discover.png";
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
    function RDiscover({lines,link}){

      const history=useHistory();
        return (
            <div style={{height:"300px" ,width:"100%"}} fluid >
              <Row style={{borderRadius:"10px",margin:"30px", height:"240px" , background: 'linear-gradient(to bottom right,  #668AD7, #0E437E)' }}>
                <Col xs={12} md={6} className="d-flex align-items-center">
            <div style={{display:"flex",flexDirection:"column",cursor:"pointer" }} onClick={()=>{history.push(`${baseURL}/${genericPath}`+link)}}>
                  {lines.map(e=><div>{e}</div>)}
                  </div>             
                </Col>
                <Col xs={12} md={6} className="d-flex align-items-center justify-content-end">
                  <img src={discover} alt="Your Image" style={{ width: 'auto', height: '240px' }} />
                </Col>
              </Row>
            </div>);
    }
    

export default RDiscover;