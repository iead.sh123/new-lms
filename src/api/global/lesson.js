import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const lessonApi = {
  getSectionLessons: (sectionId) =>
    get(`${Services.lesson.backend}api/courses/${sectionId}/lessons`),

  addLesson: (lessonId, data) =>
    post(`${Services.lesson.backend}api/courses/${lessonId}/lessons`, data),

  removeLesson: (lessonId) =>
    destroy(`${Services.lesson.backend}api/lessons/${lessonId}`),

  changeStatusLesson: (lessonId) =>
    post(`${Services.lesson.backend}api/lessons/${lessonId}/publish`),

  lessonById: (lessonId) =>
    get(`${Services.lesson.backend}api/lessons/${lessonId}`),

  createLessonToModuleContent: (moduleId, data, publish) =>
    post(
      `${Services.lesson.backend}api/modules/${moduleId}/lessons?publish=${publish}`,
      data
    ),

  editLessonToModuleContent: (lessonId, data, publish) =>
    patch(
      `${Services.lesson.backend}api/lessons/${lessonId}?publish=${publish}`,
      data
    ),
};
