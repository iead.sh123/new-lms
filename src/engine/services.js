import { online } from "./config";
import { Services_local1 } from "./services_local1";
import { Services_local } from "./services_local";
import { Services_online } from "./services_online";

export const Services = online ? Services_online : Services_local;


