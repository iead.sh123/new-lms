import React from "react";
import RLessonPlanMainData from "./RLessonPlanMainData";
import RLessonPlanFullData from "./RLessonPlanFullData";
import RButton from "components/Global/RComs/RButton";
import styles from "./RLessonPlanViewer.module.scss";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RLessonPlanViewer = ({
	lessonPlan,
	handleBackToLessonPlanEditor,
	previewMode,
	setPreviewMode,
	exportLessonPlanAsPdfLoading,
	handleExportLessonPlanAsPdf,
	importCollaboration,
	removeAllActions,
}) => {
	return (
		<div className={styles.lp__viewer__container}>
			<div className={styles.lp__viewer__main}>
				<RLessonPlanMainData lessonPlan={lessonPlan} />
				<div className={styles.lp__viewer__main__buttons}>
					{removeAllActions ? (
						""
					) : importCollaboration ? (
						<RButton text={tr`import`} onClick={() => importCollaboration()} color="primary" outline faicon={"fas fa-file-import"} />
					) : !previewMode ? (
						<>
							<RButton
								text={tr`export_as_pdf`}
								onClick={() => handleExportLessonPlanAsPdf(lessonPlan.id)}
								color="primary"
								outline
								faicon={iconsFa6.pdf}
								loading={exportLessonPlanAsPdfLoading}
								disabled={exportLessonPlanAsPdfLoading}
							/>
							<RButton
								text={tr`edit`}
								onClick={() => handleBackToLessonPlanEditor && handleBackToLessonPlanEditor()}
								color="primary"
								outline
								faicon={"fa fa-pencil"}
							/>
						</>
					) : (
						<RButton
							text={tr`back_to_editor`}
							color="primary"
							faicon={"fa fa-pencil"}
							outline
							onClick={() => setPreviewMode(!previewMode)}
						/>
					)}
				</div>
			</div>
			{lessonPlan?.lesson_plan_items?.map((lesson_plan_item) => (
				<RLessonPlanFullData lesson_plan_item={lesson_plan_item} />
			))}
		</div>
	);
};

export default RLessonPlanViewer;
