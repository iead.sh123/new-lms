import { get, post } from "config/api";

export const bellCurveApi = {
  getBellCurve: (data) => post(`/api/bell_curve/values`, data),
  getUserHistory: () => get(`/api/rabs/user-history-rabs`),
};
