import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { useParams } from "react-router-dom";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RRubricHeader = () => {
	const { rubricId } = useParams();

	const RubricData = useContext(RubricContext);

	return (
		<RFlex styleProps={{ justifyContent: "space-between", alignItems: "center" }}>
			{!RubricData.previewMode && <RButton text={tr`pick_a_rubric`} color="primary" />}
			<RFlex styleProps={{ justifyContent: "flex-end", width: "100%", alignItems: "center", gap: "20px" }}>
				{!RubricData.previewMode && (
					<h6 style={{ color: RubricData?.values?.is_published ? "#19AF00" : "#dd0000" }}>
						{RubricData?.values?.is_published ? tr`publish` : tr`draft`}
					</h6>
				)}

				{rubricId && !RubricData.isViewer && (
					<RButton
						text={!RubricData.previewMode ? tr`preview` : tr`back_to_editor`}
						color="primary"
						faicon={!RubricData.previewMode ? iconsFa6.eye : iconsFa6.pencil}
						outline
						onClick={() => RubricData.setPreviewMode(!RubricData.previewMode)}
					/>
				)}

				{RubricData.previewMode && RubricData.isViewer && (
					<RFlex>
						<RButton
							text={tr`export_as_pdf`}
							onClick={() => RubricData.handleExportRubricAsPdf()}
							color="primary"
							outline
							faicon={iconsFa6.pdf}
							loading={RubricData.exportRubricAsPdfLoading}
							disabled={RubricData.exportRubricAsPdfLoading}
						/>
						<RButton
							text={tr`edit`}
							onClick={() => RubricData.handleBackToRubricEditor && RubricData.handleBackToRubricEditor()}
							color="primary"
							outline
							faicon={iconsFa6.pencil}
						/>
					</RFlex>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RRubricHeader;
