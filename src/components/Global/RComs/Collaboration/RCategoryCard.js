import React, { useState }  from "react";
import './RCategoryCard.css';

 function RCategoryCard({title,text,focused,icon,backGroundColor,cornerColor,iconColor,onClick,height}) {

   return(
     <div className="card_container"  onClick={onClick} style={{background:backGroundColor??'white',height:height?height:'75px',}}>
       <div className="title_row"> 
         <div className="corner"  style={{backgroundColor:cornerColor??'rgb(215,227,253)'}}> 
           <div className="corner_icon" style={{color:iconColor?iconColor:'rgb(116,148,219)'}}> 
              <i className={icon?icon:null}></i> 
            </div> 
         </div>
         <div className="title" style={{color:focused?iconColor:"black",fontWeight:focused?"600":"200"}}> 
              {title}
         </div>
       </div>
       <div className="text_row">
              {text}
       </div>

     </div>
  
    )
   }
 
export default RCategoryCard;