import React, { useContext, useEffect, useState } from "react";
import { LandingPageContext } from "../GHome";
import { useHistory } from "react-router-dom";
import { Services } from "engine/services";
import RButton from "components/Global/RComs/RButton";
import styles from "../Home.module.scss";
import RCard from "components/Global/RComs/RCards/RCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import REmptyData from "components/RComponents/REmptyData";

const GFreeCourses = () => {
	const history = useHistory();

	const LandingPageData = useContext(LandingPageContext);
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		if (
			LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.freeCourses &&
			LandingPageData.landingPage.freeCourses.courses &&
			LandingPageData.landingPage.freeCourses.courses.length > 0
		) {
			buildCourses();
		}
	}, [
		LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.freeCourses &&
			LandingPageData.landingPage.freeCourses.courses &&
			LandingPageData.landingPage.freeCourses.courses.length > 0,
		LandingPageData.cartItems,
		LandingPageData.addToCartLoading,
	]);

	const buildCourses = () => {
		let handleCourses = [];

		LandingPageData.landingPage?.freeCourses?.courses?.map((course, index) => {
			let data = {};
			data.image = course.image ? `${Services.courses_manager.file + course.image}` : null;
			data.icon = course.icon ? `${Services.courses_manager.file + course.icon}` : null;
			data.id = course.id;
			data.name = course?.name;
			data.color = course?.color;
			data.rate = course?.rating;
			data.categoryName = course?.category?.category_name;
			data.users = course?.teachers;
			data.isFree = course?.is_free;
			data.isOnline = course?.isOnline;
			data.extra = course?.extra;
			data.overview = course?.overview;
			data.isEnroll = course?.isEnrolled;

			data.link = `/landing/course/${course.id}/course-overview`;
			data.enrollCourse = {
				onClick: () => LandingPageData.handleEnrollInCourse(course.id),
				loading: LandingPageData.enrollInCourseLoading,
			};
			data.addToCart = {
				onClick: () => LandingPageData.handleAddToCart({ productID: course?.id, productType: course?.paymentInfo?.type }),
				loading: LandingPageData.addToCartLoading,
				disabled: LandingPageData.cartItems && LandingPageData.cartItems?.find((item) => +item.ProductID == course.id) ? true : false,
			};
			handleCourses.push(data);
		});

		setCourses(handleCourses);
	};
	const handlePushToAllCourses = () => {
		history.push(`/landing/all-courses?booleanFilters=${Object.keys(Object.values(LandingPageData.landingPage.freeCourses.filters)[0])}`);
	};
	return (
		<section className={styles.padding}>
			{courses && courses?.length > 0 ? (
				<React.Fragment>
					<h6>{LandingPageData?.landingPage?.freeCourses?.label}</h6>
					<section className={styles.all_courses}>
						{courses.map((course) => (
							<RCard course={course} hiddenFlag={true} />
						))}
					</section>
					<RFlex styleProps={{ justifyContent: "center" }}>
						<RButton
							iconRight={true}
							faicon={iconsFa6.arrowRight}
							color="primary"
							text={tr`see_all_free_courses`}
							onClick={handlePushToAllCourses}
						/>
					</RFlex>
				</React.Fragment>
			) : (
				<div style={{ justifyContent: "center" }}>
					<REmptyData />
				</div>
			)}
		</section>
	);
};

export default GFreeCourses;
