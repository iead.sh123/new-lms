import React, { useState } from "react";
import "./RTagViewer.css";

function RTitle({ text }) {
	return <div style={{ fontSize: "16px", fontWeight: "1000" }}>{text}</div>;
}

export default RTitle;
