import React, { useContext } from "react";
import { dangerColor, primaryColor } from "config/constants";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { relativeDate } from "utils/dateUtil";
import { successColor } from "config/constants";
import RecodingIcon from "assets/img/new/Recording_Icon.jpg";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import DateUtcWithTz from "utils/dateUtcWithTz";
import { useLocation } from "react-router-dom";
import iconsFa6 from "variables/iconsFa6";

const LiveSession = ({ viewMode, lessonContent, content, studentView }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	const location = useLocation();

	return (
		<>
			{viewMode && <></>}
			{lessonContent.live_session ? (
				<>
					<RFlex className="mt-3" styleProps={{ gap: 20 }}>
						<p
							style={{ color: successColor }}
							onClick={() =>
								CourseModuleData?.studentView || studentView
									? {}
									: CourseModuleData.handleJoinStudentAndTeacherToMeeting(lessonContent.live_session.meeting_id)
							}
						>
							{CourseModuleData?.studentView || studentView ? tr`next live session` : tr`live session was created`} :&nbsp;
							{/* {relativeDate(lessonContent.live_session.created_at)} */}
							{CourseModuleData?.studentView || studentView
								? DateUtcWithTz({
										dateTime: content?.created_at ?? lessonContent?.created_at,
										dateFormate: "llll",
								  })
								: relativeDate(content?.created_at ?? lessonContent?.created_at)}
						</p>
						{CourseModuleData?.studentView || studentView ? (
							""
						) : (
							<RFlex styleProps={{ color: primaryColor }}>
								<i className="mt-1 fa-solid fa-gear"></i>
								<p
									onClick={() =>
										CourseModuleData.handleViewLessonContent({
											LessonId: content.content_id,
											MeetingId: lessonContent.live_session.meeting_id,
										})
									}
								>{tr`manage_the_live_session`}</p>
							</RFlex>
						)}
					</RFlex>
					{lessonContent.live_session.recordings && lessonContent.live_session.recordings.length > 0 && (
						<>
							<span style={{ color: "#818181", fontWeight: "bold" }}>{tr`recorded_live_sessions`}</span>

							<i
								className={`ml-2 ${
									CourseModuleData.reloadLiveSessionLoading ? "fa-solid fa-rotate-right fa-spin" : "fa-solid fa-rotate-right"
								}`}
								style={{ color: primaryColor }}
								onClick={() =>
									CourseModuleData.reloadLiveSessionLoading ? {} : CourseModuleData.handleReloadLiveSession(content, lessonContent)
								}
							/>

							{lessonContent.live_session.recordings.map((record) => (
								<RFlex className="mt-3">
									<img src={RecodingIcon} width="137px" height="78px" />
									<RFlex
										styleProps={{
											flexDirection: "column",
											// justifyContent: "center",
										}}
									>
										<RFlex>
											<p className="mr-1">{record.title}</p>
											<p className="text-muted">{relativeDate(record.created_at)}</p>
										</RFlex>
										<RFlex
											styleProps={{ color: primaryColor }}
											onClick={() => {
												window.open(record.url, "_blank");
											}}
										>
											<i className="mt-1 fa fa-eye" />
											<p>{tr`view`}</p>
										</RFlex>
									</RFlex>
								</RFlex>
							))}
						</>
					)}
				</>
			) : (
				<>
					{!lessonContent.live_session ? (
						<>
							<RFlex className="mt-3" styleProps={{ gap: 20, alignItems: "baseline" }}>
								<h6 style={{ color: dangerColor }}>{tr`no_create_live_session`}</h6>
								{CourseModuleData.liveSessionLoading ? (
									<RFlex styleProps={{ alignItems: "center", justifyContent: "center" }}>
										<i className={iconsFa6.spinner} />
									</RFlex>
								) : (
									<RFlex style={{ color: primaryColor }}>
										<i className="fa fa-plus mr-1" />
										<span
											style={{ fontWeight: "bold" }}
											onClick={() =>
												CourseModuleData.handleAddLessonContent(
													content,
													content.content_id ?? content.id,
													lessonContent,
													true,
													"live_session"
												)
											}
										>{tr`schedule_live_session`}</span>
									</RFlex>
								)}
							</RFlex>
							<h6
								className="mt-2"
								style={{
									color: "#818181",
									fontWeight: "bold",
									fontSize: "12px",
								}}
							>{tr`no_recorded_sessions`}</h6>
						</>
					) : (
						<></>
					)}
				</>
			)}
		</>
	);
};

export default LiveSession;
