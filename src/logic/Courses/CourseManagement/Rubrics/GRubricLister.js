import React, { useState, useMemo } from "react";
import Swal, { DANGER } from "utils/Alert";
import { changeRubricStatusAsync, removeRubricAsync, exportRubricAsPdfAsync } from "store/actions/global/rubric.actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { dangerColor } from "config/constants";
import { Services } from "engine/services";
import { Row, Col } from "reactstrap";
import { get } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import DateUtcWithTz from "utils/dateUtcWithTz";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GRubricLister = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { courseId, curriculumId, cohortId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [filters, setFilters] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [filterData, setFilterData] = useState("all");

	const { changeRubricStatusLoading, removeRubricLoading, exportRubricAsPdfLoading } = useSelector((state) => state.rubricRed);

	const convertFiltersToArray = (filtersObject) => {
		return filtersObject
			? Object?.keys(filtersObject)?.map((title) => ({
					title,
					count: filtersObject[title],
			  }))
			: [];
	};

	const handlePickFromCollaboration = () => {
		history.push(
			`${baseURL}/${genericPath}/collaboration_area_contents/${
				courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
			}/rubrics`
		);
	};

	const handlePushToRubricEditor = (rId) => {
		if (courseId) {
			if (rId) {
				history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/rubric/${rId}`);
			} else {
				history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/rubrics`);
			}
		} else if (curriculumId) {
			if (rId) {
				history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/rubric/${rId}`);
			} else {
				history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/rubrics`);
			}
		}
	};

	const handlePushToRubricViewer = (rId) => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/rubric/${rId}/viewer`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/rubric/${rId}/viewer`);
		}
	};

	const handlePushToStudentsDetails = (rId) => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/rubric/${rId}/students`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/rubric/${rId}/students`);
		}
	};

	const handlePushToGradeStudentByStudent = (rId) => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/rubric/${rId}/grade-student`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/rubric/${rId}/grade-student`);
		}
	};

	const handlePushToGradeStandardByStandard = (rId) => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/rubric/${rId}/grade-standard`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/rubric/${rId}/grade-standard`);
		}
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = async (emptySearch) => {
		let QueryReq = "";

		QueryReq = `?${courseId ? `course_id=${courseId}` : `curriculum=${curriculumId}`}${`&search=${
			emptySearch !== undefined ? emptySearch : searchData
		}`}${filterData ? (filterData == "all" ? "" : `&published=${filterData == "published" ? 1 : 0}`) : ""}`;

		let specific_url;
		specific_url = `${Services.rubric.backend}api/v1/rubric${QueryReq}`;

		setSearchLoading(true);
		// setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
				// setLoading(false);
			} else {
				setSearchLoading(false);
				// setLoading(false);
			}
		} else {
			setSearchLoading(false);
			// setLoading(false);
		}
	};

	const handleFilterOnTabs = async (tabName) => {
		setFilterData(tabName);
		let QueryReq = "";
		QueryReq = `?${courseId ? `course_id=${courseId}` : `curriculum=${curriculumId}`}${
			tabName == "all" ? "" : `&published=${tabName == "published" ? 1 : 0}`
		}${searchData ? `&search=${searchData}` : ""}`;

		let specific_url;
		specific_url = `${Services.rubric.backend}api/v1/rubric${QueryReq}`;

		// setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				// setLoading(false);
			} else {
				// setLoading(false);
			}
		} else {
			// setLoading(false);
		}
	};

	const handleChangeStatus = (rubricId) => {
		let QueryReq = `?${courseId ? `course_id=${courseId}` : `curriculum=${curriculumId}`}${searchData ? `&search=${searchData}` : ""}${
			filterData ? (filterData == "all" ? "" : `&published=${filterData == "published" ? 1 : 0}`) : ""
		}`;
		let specific_url;
		specific_url = `${Services.rubric.backend}api/v1/rubric${QueryReq}`;
		dispatch(changeRubricStatusAsync(rubricId, getDataFromBackend, specific_url, setTableData));
	};

	const handelDeleteRubric = (rubricId) => {
		Swal.input({
			title: tr`are_you_sure_to_delete_it`,
			message: tr``,
			type: DANGER,
			placeHolder: "",
			onConfirm: () => successDelete(rubricId),
			onCancel: () => null,
		});
	};

	const successDelete = (rubricId) => {
		let QueryReq = `?${courseId ? `course_id=${courseId}` : `curriculum=${curriculumId}`}${searchData ? `&search=${searchData}` : ""}${
			filterData ? (filterData == "all" ? "" : `&published=${filterData == "published" ? 1 : 0}`) : ""
		}`;
		let specific_url;
		specific_url = `${Services.rubric.backend}api/v1/rubric${QueryReq}`;
		dispatch(removeRubricAsync(rubricId, getDataFromBackend, specific_url, setTableData));
	};

	const handleExportRubricAsPdf = (rubricId) => {
		dispatch(exportRubricAsPdfAsync(rubricId));
	};
	// ----------------------------------------------getDataFromBackend----------------------------------------------------------------

	const getDataFromBackend = async (specific_url) => {
		let QueryReq = "";
		QueryReq = `?${courseId ? `course_id=${courseId}` : `curriculum=${curriculumId}`}`;

		const url = `${Services.rubric.backend}api/v1/rubric${QueryReq}`;

		let response = await get(specific_url ? specific_url : url);
		// setLoading(true);

		if (response.data.status == 1) {
			// setLoading(false);
			return response;
		} else {
			// setLoading(false);

			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.rubrics?.data.map((r) => ({
			id: r.id,
			table_name: "GRubricLister",

			details: [
				{ key: tr`name`, value: r?.title },
				{ key: tr`categories`, value: `${r?.categories.length} categories` },
				{
					key: tr`standards`,
					value: `${r?.categories
						.map((category) => category.standards.length)
						.reduce((accumulator, currentValue) => accumulator + currentValue, 0)} standards`,
				},
				{
					key: tr`creation_date`,
					value: DateUtcWithTz({
						dateTime: r?.created_at,
						dateFormate: "L",
					}),
					html: true,
				},
				{
					key: tr("status"),
					value: r.is_published ? (
						changeRubricStatusLoading ? (
							<RFlex>
								<i className={iconsFa6.spinner + " pt-1"}></i>
							</RFlex>
						) : (
							<h6 style={{ cursor: "pointer", color: "#148D00" }} onClick={() => handleChangeStatus(r.id)}>
								{tr("publish")}
							</h6>
						)
					) : changeRubricStatusLoading ? (
						<RFlex>
							<i className={iconsFa6.spinner + " pt-1"}></i>
						</RFlex>
					) : (
						<h6 style={{ cursor: "pointer", color: "#DD0000" }} onClick={() => handleChangeStatus(r.id)}>
							{tr("draft")}
						</h6>
					),
					html: true,
				},
			],
			actions: [
				{
					name: tr`grade_student_by_student`,
					icon: "fa-solid fa-user",
					onClick: () => handlePushToGradeStudentByStudent(r.id),
				},
				{
					name: tr`grade_standard_by_standard`,
					icon: "fa-solid fa-clipboard-list",
					onClick: () => handlePushToGradeStandardByStandard(r.id),
				},
				{
					name: tr`students_details`,
					icon: "fa-solid fa-clipboard-list",
					onClick: () => handlePushToStudentsDetails(r.id),
				},
				{
					name: tr`edit`,
					icon: "fa-solid fa-pen-to-square",
					onClick: () => handlePushToRubricEditor(r.id),
				},
				{
					name: tr`view`,
					icon: "fa fa-eye",
					onClick: () => handlePushToRubricViewer(r.id),
				},
				{
					name: tr`export_as_pdf`,
					icon: "fas fa-file-pdf",
					onClick: () => handleExportRubricAsPdf(r.id),
					loading: exportRubricAsPdfLoading,
				},
				{
					name: tr`delete`,
					icon: "fa-solid fa-trash-can",
					onClick: () => handelDeleteRubric(r.id),
					color: dangerColor,
				},
			],
		}));
		setFilters(convertFiltersToArray(response?.data?.data?.filters));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.rubrics,
				// table_name: changeRubricStatusLoading,
			},
		}),
		[processedRecords, changeRubricStatusLoading, exportRubricAsPdfLoading]
	);

	return (
		<Row>
			<Col xs={12} md={8} className="mb-2">
				<RSearchHeader
					searchLoading={searchLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					handlePushToAnotherRouteWhenAdd={handlePushToRubricEditor}
					addNew={true}
					buttonName={tr`create_new_rubric`}
				/>
			</Col>
			<Col xs={12} md={4} className="d-flex justify-content-end mb-2">
				<RButton
					text={tr`pick_from_collaboration`}
					faicon={iconsFa6.globe}
					color={"primary"}
					outline
					onClick={handlePickFromCollaboration}
				/>
			</Col>
			{filters.length > 0 && (
				<Col xs={12} md={8}>
					<RFilterTabs tabs={filters} handleSearchOnTabs={handleFilterOnTabs} />
				</Col>
			)}

			<RAdvancedLister {...propsLiterals.listerProps} />
		</Row>
	);
};

export default GRubricLister;
