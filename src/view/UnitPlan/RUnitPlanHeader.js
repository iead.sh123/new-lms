import React, { useContext } from "react";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import RButton from "components/Global/RComs/RButton";
import styles from "./RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUnitPlanHeader = () => {
	const UnitPlanContextData = useContext(UnitPlanContext);

	return (
		<RFlex styleProps={{ justifyContent: UnitPlanContextData.previewMode ? "flex-end" : "space-between", alignItems: "center" }}>
			{!UnitPlanContextData.previewMode && <RButton text={tr`pick_a_lesson_plan`} color="primary" />}
			<RFlex styleProps={{ alignItems: "center", gap: "20px" }}>
				{!UnitPlanContextData.previewMode && (
					<h6 className={styles.icon_text} style={{ color: UnitPlanContextData.unitPlan?.published ? "#19AF00" : "#dd0000" }}>
						{UnitPlanContextData.unitPlan?.published ? tr`publish` : tr`draft`}
					</h6>
				)}
				{UnitPlanContextData.unitPlan?.id && (
					<RButton
						text={!UnitPlanContextData.previewMode ? tr`preview` : tr`back_to_editor`}
						color="primary"
						faicon={!UnitPlanContextData.previewMode ? "fa fa-eye" : "fa fa-pencil"}
						outline
						onClick={() => UnitPlanContextData.setPreviewMode(!UnitPlanContextData.previewMode)}
					/>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RUnitPlanHeader;
