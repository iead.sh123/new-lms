import React from "react";
import CheckoutForm from "./CheckoutForm";
import styles from "../../cart.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const stripePromise = loadStripe(
	"pk_test_51OisFHAYlqGScOt6Ooni9a92Z5qgmx3BdIAuaFHnk6M8ilVPt1FOgj3lESqPZAvLyf2Ttc65PCOSoY3cX4UUbkpV00eRLVL6zA"
);
const PaymentMethods = ({ clientSecret, orderId }) => {
	const appearance = {
		theme: "stripe",
	};
	const options = {
		clientSecret,
		appearance,
	};

	return (
		<RFlex styleProps={{ gap: 20, flexDirection: "column", width: "75%" }}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<span className={styles.payment_title}>{tr`payment`}</span>
				<div className={styles.divider} />
			</RFlex>
			{clientSecret && (
				<Elements options={options} stripe={stripePromise}>
					<CheckoutForm clientSecret={clientSecret} orderId={orderId} />
				</Elements>
			)}
		</RFlex>
	);
};

export default PaymentMethods;
