import RFileUploaderInFileManagement from "components/RComponents/RFileUploaderInFileManagement";
import React from "react";

const RUploadMedia = ({ data, handlers }) => {
	return (
		<div>
			<RFileUploaderInFileManagement
				parentCallbackToFillData={handlers.handleAddDataToList}
				parentCallback={handlers.handleCallback}
				singleFile={false}
				typeFile={[
					"image/*",
					".docx",
					".doc",
					".pptx",
					"video/mp4",
					"video/x-matroska",
					"video/webm",
					"video/x-m4v",
					"video/quicktime",
					"text/*",
					"application/*",
					"*",
					"*/*",
				]}
				folderId={data.folderId}
			/>
		</div>
	);
};

export default RUploadMedia;
