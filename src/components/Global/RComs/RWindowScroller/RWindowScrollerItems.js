import React, { useState } from "react";
import { Row, Col, Collapse } from "reactstrap";
import RWindow from "../RWindow";
import styles from "./RWindowScroller.Module.scss";

const RWindowScrollerItems = ({ data, windowData }) => {
  const [openedCollapse, setOpenedCollapse] = useState(null);
  const [node, setNode] = useState(null);

  const toggleCollapse = (id) =>
    openedCollapse === id ? setOpenedCollapse(null) : setOpenedCollapse(id);
  return (
    <Row>
      <Col xs={12}>
        <>
          {data?.map((DATA) => {
            return (
              <div className={styles.container}>
                <Row className={styles.content}>
                  <Col xs={2}>
                    <h5
                      className={
                        openedCollapse === DATA?.id
                          ? styles["title-color"]
                          : styles["title-no-color"]
                      }
                      onClick={() => {
                        toggleCollapse(DATA?.id);
                        setNode(DATA.nodeIndex);
                      }}
                    >
                      {DATA.name}
                    </h5>
                  </Col>
                  <Col xs={10} className={styles["content-col"]}>
                    <div className={styles["content-col-div"]}>
                      {DATA?.actions?.map((action) => {
                        return (
                          <i
                            className={action.icon}
                            style={{ color: action.color }}
                            onClick={() => action.onClick(DATA.id)}
                          ></i>
                        );
                      })}

                      {DATA.items?.length > 0 &&
                        (openedCollapse === DATA?.id ? (
                          <i
                            className={
                              openedCollapse === DATA?.id
                                ? `fa fa-chevron-up + ${styles.color}`
                                : "fa fa-chevron-up"
                            }
                            aria-hidden="true"
                            onClick={() => toggleCollapse(DATA?.id)}
                          ></i>
                        ) : (
                          <i
                            className="fa fa-chevron-down"
                            aria-hidden="true"
                            onClick={() => toggleCollapse(DATA?.id)}
                          ></i>
                        ))}
                    </div>
                  </Col>
                </Row>
                {DATA.items?.length > 0 && (
                  <Row>
                    <Col>
                      <RWindow windowData={windowData} nodeIndex={node} />
                    </Col>
                  </Row>
                )}

                <Collapse key={DATA.id} isOpen={openedCollapse === DATA.id}>
                  <RWindowScrollerItems
                    data={DATA.items}
                    windowData={windowData}
                    test={windowData?.actions}
                  />
                </Collapse>
              </div>
            );
          })}
        </>
      </Col>
    </Row>
  );
};

export default RWindowScrollerItems;
