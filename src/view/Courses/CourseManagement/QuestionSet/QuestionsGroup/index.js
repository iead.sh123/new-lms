import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import MultipleChoiceSingleAnswer from "../QuestionsTypes/MultipleChoiceSingleAnswer";
import QuestionsGroupInfo from "./QuestionsGroupInfo";
import MultipleChoice from "../QuestionsTypes/MultipleChoice";
import RQuestionTypes from "../RQuestionTypes";
import RHideCollapse from "../RHideCollapse";
import FillInBlank from "../QuestionsTypes/FillInBlank";
import TrueOrFalse from "../QuestionsTypes/TrueOrFalse";
import Ordering from "../QuestionsTypes/Ordering";
import UploadFile from "../QuestionsTypes/UploadFile";
import styles from "../QuestionSet.module.scss";
import Match from "../QuestionsTypes/Match";
import Draw from "../QuestionsTypes/Draw";
import Essay from "../QuestionsTypes/Essay";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import Collapse from "reactstrap/es/Collapse";
import { primaryColor } from "config/constants";

const QuestionsGroup = ({ group, questionGroupId, questionNumber }) => {
	const QuestionSetData = useContext(QuestionSetContext);

	const questionTypeComponents = {
		ESSAY: Essay,
		FILL_IN: FillInBlank,
		UPLOAD_FILE: UploadFile,
		MULTIPLE_CHOICES_MULTIPLE_ANSWERS: MultipleChoice,
		MULTIPLE_CHOICES_SINGLE_ANSWER: MultipleChoiceSingleAnswer,
		TRUEFALSE: TrueOrFalse,
		SEQUENCING: Ordering,
		DRAW: Draw,
		MATCH: Match,
	};

	return (
		<React.Fragment>
			<QuestionsGroupInfo
				group={group}
				questionGroupId={questionGroupId}
				formChecked={group?.requiredQuestionsToAnswer && group?.points ? true : false}
			/>
			<RQuestionTypes questionSetIndex={null} type={"group"} groupId={questionGroupId} />
			{group.isGroup &&
				group?.questions?.map((question, index) => {
					const isOpen = QuestionSetData.openedCollapses.includes(question?.id || question?.fakeId);
					const QuestionComponent = questionTypeComponents[question.type];
					return (
						<section key={question?.id ? question?.id : question?.fakeId}>
							<section className={styles.section__group + " m-3"}>
								<Collapse isOpen={isOpen}>
									<RHideCollapse
										group={group}
										groupId={question?.id ? question?.id : question?.fakeId}
										questionTitle={question?.text}
										questionPoint={group?.requiredQuestionsToAnswer && group?.points ? group?.points : question?.points}
									/>
								</Collapse>

								{!isOpen && (
									<>
										{QuestionComponent ? (
											<section>
												<QuestionComponent
													questionGroupId={group?.id ? group?.id : group?.fakeId}
													question={question}
													questionNumber={index + 1}
													group={group}
												/>

												<RFlex
													className="justify-content-end pl-4 cursor-pointer"
													// className={styles.hide__collapse}
													onClick={() => QuestionSetData.collapsesQuestionSetGroupToggle(question?.id ? question?.id : question?.fakeId)}
												>
													<i className="fa-solid fa-chevron-up pt-1" style={{ color: primaryColor }} />
													<p>{tr`hide`}</p>
												</RFlex>
											</section>
										) : null}
									</>
								)}
							</section>
							<RQuestionTypes questionSetIndex={index + 1} type={"group"} groupId={questionGroupId} />
						</section>
					);
				})}
		</React.Fragment>
	);
};

export default QuestionsGroup;
