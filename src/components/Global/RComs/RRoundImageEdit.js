import React from "react";
import RFileSuite from "./RFile/RFileSuite";
import { useState } from "react";

function RRoundImageEdit({ images, setImages }) {
	return (
		<div>
			<RFileSuite
				parentCallback={setImages}
				singleFile={true}
				fileType={["image/*"]}
				removeButton={false}
				value={images}
				uploadName="upload"
				showReplace={false}
				showDelete={true}
				showFileList={false}
				showFileAdd={true}
				setSpecificAttachment={() => {}}
				theme="round"
				binary={true}
			></RFileSuite>
		</div>
	);
}

export default RRoundImageEdit;
