import RButton from 'components/Global/RComs/RButton'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader'
import tr from 'components/Global/RComs/RTranslator'
import React, { useState } from 'react'
import iconsFa6 from 'variables/iconsFa6'
import styles from './sharedStyle.module.scss'
import RSelect from 'components/Global/RComs/RSelect'
import { filterOptions } from '../constants'
import * as colors from 'config/constants'
import { Input } from 'reactstrap'
const GFirstMembersHeader = ({
    selectedUsers = [],
    isFetching,
    frontSearchTerm,
    setFrontSearchTerm,
    filters,
    setFilters,
    allMembersFilters,
    setAllMembersFilters,
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    allUsersLength,
    showAddUsers = true

}) => {
    console.log("filtersfilters", filters)
    const handleSearch = (clearData) => {
        let data = frontSearchTerm;
        if (clearData == "") {
            data = clearData;
            setAllMembersFilters({ ...allMembersFilters, searchText: data });
            return
        }
        setAllMembersFilters({ ...allMembersFilters, searchText: data });
    };
    const handleClickOnFilter = (filter) => {
        const newFilters = [...filters]
        const currentFilterIndex = newFilters.findIndex((f1) => f1.id == filter.id)
        if (!filter.active) {
            newFilters[currentFilterIndex].active = true
            setFilters(newFilters)
        }
        else {
            if (!filter.pickedOption) {
                newFilters[currentFilterIndex].active = false
                setFilters(newFilters)
            }
            else {
                if (filters.length == 3) {
                    return
                }
                else {
                    setFilters([...newFilters, { id: getNewId(), active: true, pickedOption: null, isApplied: false }])
                }
            }
        }
    }
    const getNewId = () => {
        const maxId = filters.reduce((max, filter) => {
            return filter.id > max ? filter.id : max
        }, filters[0].id)
        return maxId + 1
    }
    const handleRemoveFilter = (filter) => {
        if (filter.isApplied) {
            setAllMembersFilters({ ...allMembersFilters, [filter.pickedOption.value]: "" })
        }
        if (filters.length == 1) {
            const newFilters = [...filters]
            newFilters[0].active = false
            newFilters[0].pickedOption = null
            newFilters[0].isApplied = false
            setFilters(newFilters)
        }
        else {
            const newFilters = filters.filter((f1, index) => f1.id != filter.id)
            setFilters(newFilters)
        }
    }
    const handleSelectorChange = (option, filter) => {
        if (filter.pickedOption) {
            if (values[filter.pickedOption.value])
                setAllMembersFilters({ ...allMembersFilters, [filter.pickedOption.value]: "" })
        }
        const newFilters = [...filters]
        const currentFilterIndex = newFilters.findIndex((f1) => f1.id == filter.id)
        newFilters[currentFilterIndex].pickedOption = option
        setFilters(newFilters)
    }
    const getAlreadyPickedOptions = () => {
        const usedOptionsIds = filters.map((filter) => filter.pickedOption && filter.pickedOption.id)
        return usedOptionsIds
    }
    const handleApplyFilter = (filter) => {
        if (!filter.pickedOption)
            return
        if (!filter.pickedOption.value)
            return
        const filterName = filter.pickedOption.value
        const filterValue = values[filter.pickedOption.value]
        const newFilters = [...filters]
        const currentFilterIndex = newFilters.findIndex((f1) => f1.id == filter.id)
        newFilters[currentFilterIndex].isApplied = true
        setFilters(newFilters)
        setAllMembersFilters({ ...allMembersFilters, [filterName]: filterValue })
    }
    return (
        <RFlex className="flex-column">
            <RFlex className='align-items-baseline' styleProps={{ gap: '15px' }}>
                {showAddUsers &&
                    <span className='p-0 m-0'>{tr("Add_users")}</span>}
                <RSearchHeader
                    margin={'0px'}
                    inputWidth={"500px"}
                    widthInput={'500px'}
                    searchLoading={isFetching}
                    inputPlaceholder={tr`search_for_a_user`}
                    searchData={frontSearchTerm}
                    handleChangeSearch={setFrontSearchTerm}
                    setSearchData={setFrontSearchTerm}
                    handleSearch={handleSearch}
                />
                {selectedUsers.length > 0 &&
                    <span className='p-0 m-0'>
                        {allUsersLength == selectedUsers.length ?
                            tr("All") :
                            `${selectedUsers.length} ${tr("Selected_users")}`
                        }
                    </span>
                }
            </RFlex>
            <RFlex className='flex-column'>
                {filters.map((filter) => {
                    return (
                        <RFlex className='align-items-center'>
                            <RButton
                                text={tr("Filter")}
                                faicon={iconsFa6.filter}
                                color='link'
                                loading={isFetching}
                                className={` ${styles.filterButton} ${filter.active ? 'text-primary' : ""}`}
                                onClick={() => { handleClickOnFilter(filter) }}
                            />
                            {filter.active &&
                                <RFlex className='algin-items-center' styleProps={{ gap: "15px" }}>
                                    <div style={{ width: "150px" }}>
                                        <RSelect
                                            option={filterOptions.filter((option) => !getAlreadyPickedOptions().includes(option.id))}
                                            closeMenuOnSelect={true}
                                            value={filter.pickedOption}
                                            placeholder={tr("Choose_filter")}
                                            onChange={(option) => handleSelectorChange(option, filter)}
                                        />
                                    </div>
                                    {filter.pickedOption &&
                                        <Input
                                            type={filter.pickedOption.type}
                                            name={filter.pickedOption.value}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values[filter.pickedOption.value]}
                                            className={`${(errors[filter.pickedOption.value] && touched[filter.pickedOption.value]) ? 'input__error' : ""}`}
                                            placeholder={tr(filter.pickedOption.placeholder)}
                                            style={{ width: '200px' }}
                                        />
                                    }
                                    <RButton
                                        text={tr("Apply")}
                                        color='primary'
                                        className='m-0'
                                        loading={isFetching}
                                        onClick={() => { handleApplyFilter(filter) }}
                                    />
                                    <RButton
                                        text={tr("Remove")}
                                        color='link'
                                        className={`m-0`}
                                        style={{ color: colors.warningColor }}
                                        onClick={() => { handleRemoveFilter(filter) }}
                                    />
                                </RFlex>
                            }
                        </RFlex>
                    )
                })}
            </RFlex>

        </RFlex>
    )
}

export default GFirstMembersHeader