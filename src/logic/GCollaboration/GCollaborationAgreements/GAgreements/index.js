import React, { useState } from "react";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import GAddAgreement from "./GAddAgreement";
import RAgreements from "view/Collaboration/CollaborationAgreement/RAgreements";
import AppModal from "components/Global/ModalCustomize/AppModal";
import Loader from "utils/Loader";
import GViewAgreement from "./GViewAgreement";
import { useMutateData } from "hocs/useMutateData";

const GAgreements = () => {
	const [openEditorAgreement, setOpenEditorAgreement] = useState(false);
	const [openViewAgreement, setOpenViewAgreement] = useState(false);
	const [agreementId, setAgreementId] = useState(null);

	// ------------------ Queries ------------------
	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["clients"],
		queryFn: () => collaborationApi.clients(),
		onSuccessFn: ({ data }) => {},
	});

	const deleteAgreementMutate = useMutateData({
		queryFn: (agreementId) => collaborationApi.deleteAgreement(agreementId),
		invalidateKeys: ["clients"],
		onSuccessFn: ({ data, variables }) => {},
	});

	// ------------------ Open Modals ------------------
	const handleOpenEditorAgreement = () => setOpenEditorAgreement(true);
	const handleCloseEditorAgreement = () => {
		setOpenEditorAgreement(false);
		setAgreementId(null);
	};

	const handleOpenViewAgreement = () => setOpenViewAgreement(true);
	const handleCloseViewAgreement = () => {
		setOpenViewAgreement(false);
		setAgreementId(null);
	};

	// ------------------ Handlers ------------------
	const handleSetAgreementId = (id) => {
		setAgreementId(id);
	};

	if (isLoading) return <Loader />;

	return (
		<div>
			<AppModal
				headerSort={<GAddAgreement agreementId={agreementId} handleCloseEditorAgreement={handleCloseEditorAgreement} />}
				show={openEditorAgreement}
				parentHandleClose={handleCloseEditorAgreement}
				size="md"
			/>
			<AppModal
				headerSort={<GViewAgreement agreementId={agreementId} handleCloseViewAgreement={handleCloseViewAgreement} />}
				show={openViewAgreement}
				parentHandleClose={handleCloseViewAgreement}
				size="md"
			/>
			<RAgreements
				data={{ content: data?.data?.data }}
				handlers={{
					handleOpenEditorAgreement: handleOpenEditorAgreement,
					handleOpenViewAgreement: handleOpenViewAgreement,
					handleSetAgreementId: handleSetAgreementId,
				}}
			/>
		</div>
	);
};

export default GAgreements;
