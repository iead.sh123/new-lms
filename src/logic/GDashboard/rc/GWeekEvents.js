import react, { useEffect, useState } from "react"
import { Row,Col } from "reactstrap";
import RWeekEvents from "./RWeekEvents";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import moment from "moment";
import RNoDataLine from "components/Global/RComs/RNoDataLine";
import RHeader from "components/Global/RComs/RHeader";
function GWeekEvents({}){
  
  const [events,setEvents]=useState([]);
  // "userEvents_id": 101,
  // "userEvents_notify_before": null,
  // "userEvents_userId": 815,
  // "userEvents_eventId": 35,
  // "events_id": 35,
  // "events_title": "event hazem 10",
  // "events_repeat": "DAILY",
  // "events_customRepeatPeriod": null,
  // "events_lastOcuurance": null,
  // "events_from": "2023-06-22T05:00:39.000Z",
  // "events_to": "2023-06-22T07:00:39.000Z",
  // "events_repeatDaysOfWeek": "1",
  // "events_created_at": "2023-06-22T07:48:48.489Z",
  // "events_updated_at": "2023-11-22T21:04:38.858Z",
  // "events_typeId": 10,
  // "events_creatorId": 1,
  // "eventtype_id": 10,
  // "eventtype_parent_eventType_id": 10,
  // "eventtype_name": "WEEKLY_SCHEDULE",
  // "eventtype_color": "green",
  // "eventtype_organization_type_id": 1,
  // "commentsCount": "0",
  // "notesCount": "0"

                    useEffect(() => {
                      //setData(dd.data);
                      const get_it = async () => {
                        await Helper.fastGet(
                          `${Services.calender.backend}user-events`,
                          "failed to get user-events",
                          (response) => {
                            Helper.cl(response, "calendar data  ");
                            setEvents(response?.data?.data?.map(e=>({
                              event:e.events_title,//"Parents Meetings (Meeting Title)",
                              eventType:e.eventtype_name,//'Weekly schedule',
                              time:moment(e.events_from).format("HH:mm") +"-"+ moment(e.events_to).format("HH:mm"),
                              date:moment(e.events_from).format("MM/DD") +" " ,
                              pepole:"",//"cristina and 3 others",
                              color:e.eventtype_color,//'rgb(70,195,126)'
                            })));
                          },
                          () => {}
                        );
           
                        };
                      get_it();
                    }, []);
                  
                    return(
        <div style={{margin:'1rem',width:'100%'}}>
<RHeader count={events?.length} title={"week Events"}/>

      {
      events?.length?(<>
            {events?.map(i=>
              <RWeekEvents 
              event={i.event}
              eventType={i.eventType}
              time={i.time}
              date={i.date}
              pepole={i.pepole}
              color={i.color}
                />
                )
               }
               </>
                )
                :(<RNoDataLine messageNoData={"No Events For This Week Yet"}/>)
                }
       
         
        </div>
    )
}
export default GWeekEvents;