import React, { useState } from "react";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { boldGreyColor } from "config/constants";
import { useMutateData } from "hocs/useMutateData";
import { dangerColor } from "config/constants";
import { paymentApi } from "api/global/payment";
import RCouponDetails from "view/Organization/PaymentPolicy/Coupon/RCouponDetails";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GCouponDetails = ({ couponId, couponDetails, handleEmptyRightSideAfterDeActivate }) => {
	const [sort, setSort] = useState("");
	const [alert, setAlert] = useState(false);

	// --------------- Start Queries ---------------
	const couponDetailsData = useFetchDataRQ({
		queryKey: ["couponDetails", couponId, sort],
		queryFn: () => paymentApi.couponDetails({ couponId, params: sort ? `?filter=${sort}` : "" }),
	});

	const couponDeactivateMutate = useMutateData({
		queryFn: () => paymentApi.couponDeactivate(couponId),
		invalidateKeys: [["couponsList"], ["couponDetails", couponId, sort]],
		multipleKeys: true,
		closeDialog: () => setAlert(false),
		onSuccessFn: () => {
			handleEmptyRightSideAfterDeActivate();
		},
	});
	// --------------- End Queries ---------------

	const handleSortBy = (data) => {
		setSort(data);
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const successDelete = (prameters) => {
		couponDeactivateMutate.mutate({});
	};

	const handleDeactivateCoupon = () => {
		const prameters = {
			couponId,
		};
		const message = (
			<RFlex styleProps={{ flexDirection: "column", gap: 15 }}>
				<h6>
					{couponDetails?.couponType == "Percent" ? `${couponDetails.value} %` : `${couponDetails.value} $`} {tr`will_be_no_longer_useable`}
				</h6>
				<span style={{ color: boldGreyColor }}>{tr`this_can't_be_undone,_are_you_sure`}?</span>
			</RFlex>
		);
		const confirm = tr`deactivate`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	if (couponDetailsData.isLoading) return <Loader />;

	return (
		<>
			{alert}
			<RFlex styleProps={{ flexDirection: "column", width: "100%" }}>
				<RFlex styleProps={{ justifyContent: "space-between" }}>
					<span>
						{couponDetails?.couponType == "Percent" ? `${couponDetails.value}%` : `${couponDetails.value}$`}&nbsp;
						{tr`coupon_was_used_by`} :
					</span>
					<span style={{ color: dangerColor, cursor: "pointer" }} onClick={() => handleDeactivateCoupon()}>{tr`deactivate_coupon`}</span>
				</RFlex>
				<RCouponDetails handleSortBy={handleSortBy} users={couponDetailsData?.data?.data?.data} />
			</RFlex>
		</>
	);
};

export default GCouponDetails;
