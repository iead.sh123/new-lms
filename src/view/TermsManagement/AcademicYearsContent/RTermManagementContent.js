import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { Collapse } from "reactstrap";
import RAcademicYearsContentCollapses from "./RAcademicYearsContentCollapses";
import styles from "../TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import { primaryColor, greyColor, boldGreyColor } from "config/constants";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import { CharactersToRandomize } from "config/constants";
import { generateRandomString } from "utils/GenerateRandomString";

const RTermManagementContent = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	return (
		<RFlex className={styles.academic__years} styleProps={{ width: "100%" }}>
			{TermsManagementData?.academicYears?.map((academicYear) => (
				<RFlex key={academicYear.id} styleProps={{ flexDirection: "column" }}>
					<RAcademicYearsContentCollapses
						data={academicYear}
						type="academicYear"
						iD={academicYear.id}
						aYCollapse={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}
						tCollapse={false}
						gLCollapse={false}
						academicYearId={academicYear.id}
					/>
					<Collapse isOpen={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}>
						<RFlex styleProps={{ flexDirection: "column" }}>
							{academicYear?.terms &&
								academicYear?.terms?.length > 0 &&
								academicYear?.terms?.map((term) => (
									<RFlex key={`${academicYear.id}_${term.id}`} styleProps={{ flexDirection: "column" }}>
										<RAcademicYearsContentCollapses
											data={term}
											type="term"
											iD={`${academicYear.id}_${term.id}`}
											aYCollapse={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}
											tCollapse={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}
											gLCollapse={false}
											academicYearId={academicYear.id}
											termId={term.id}
											mLeft={"20px"}
										/>
										<Collapse isOpen={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}>
											<RFlex styleProps={{ flexDirection: "column" }}>
												{term?.gradelevels &&
													term?.gradelevels?.length > 0 &&
													term?.gradelevels?.map((gradeLevel) => (
														<RFlex key={`${academicYear.id}_${term.id}_${gradeLevel.id}`} styleProps={{ flexDirection: "column" }}>
															<RAcademicYearsContentCollapses
																data={gradeLevel}
																type="gradeLevel"
																iD={`${academicYear.id}_${term.id}_${gradeLevel.id}`}
																aYCollapse={
																	TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`
																}
																tCollapse={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}
																gLCollapse={
																	TermsManagementData.openedGradeLevelsCollapses ==
																	"collapse" + `gradeLevel_${academicYear.id}_${term.id}_${gradeLevel.id}`
																}
																academicYearId={academicYear.id}
																termId={term.id}
																gradeLevelId={gradeLevel.id}
																firstCurriculumID={gradeLevel?.children?.curricula[0]?.id}
																allHomeRoomsToGradeLevel={gradeLevel?.children?.homerooms}
																mLeft={"40px"}
															/>

															<Collapse
																isOpen={
																	TermsManagementData.openedGradeLevelsCollapses ==
																	"collapse" + `gradeLevel_${academicYear.id}_${term.id}_${gradeLevel.id}`
																}
															>
																<RFlex styleProps={{ flexDirection: "column" }}>
																	{/* homeRoom */}
																	{gradeLevel?.children?.homerooms &&
																		gradeLevel?.children?.homerooms?.length > 0 &&
																		gradeLevel?.children?.homerooms?.map((homeRoom, index) => (
																			<RFlex
																				key={`${academicYear.id}_${term.id}_${gradeLevel.id}_${homeRoom.id}`}
																				styleProps={{ flexDirection: "column" }}
																			>
																				<RAcademicYearsContentCollapses
																					data={homeRoom}
																					type="homeRoom"
																					iD={`${academicYear.id}_${term.id}_${gradeLevel.id}_${homeRoom.id}`}
																					homeRoomId={homeRoom.id}
																					termId={term.id}
																					gradeLevelId={gradeLevel.id}
																					mLeft={"60px"}
																				/>
																			</RFlex>
																		))}
																	{/* Actions Inside Grade Level */}
																	<RFlex styleProps={{ justifyContent: "space-between" }}>
																		{gradeLevel?.children?.homerooms?.length > 0 ? (
																			<RFlex styleProps={{ marginLeft: "60px" }}>
																				<span
																					className={styles.add__academic__year}
																					onClick={() =>
																						TermsManagementData.handleOpenHomeRoomModal({
																							data: false,
																							termId: term.id,
																							gradeLevelId: gradeLevel.id,
																						})
																					}
																				>{tr`add_home_room`}</span>
																			</RFlex>
																		) : (
																			<RFlex styleProps={{ marginLeft: "60px" }}>
																				<span style={{ color: greyColor }}>{tr`no_home_room_yet`}</span>
																				<span
																					className={styles.add__academic__year}
																					onClick={() =>
																						TermsManagementData.handleOpenHomeRoomModal({
																							data: false,
																							termId: term.id,
																							gradeLevelId: gradeLevel.id,
																						})
																					}
																				>{tr`add_home_room`}</span>
																			</RFlex>
																		)}

																		<UncontrolledDropdown className="btn-magnify">
																			<DropdownToggle
																				style={{ padding: "0px" }}
																				aria-haspopup={true}
																				data-toggle="dropdown"
																				id="navbarDropdownMenuLink"
																				nav
																			>
																				<RTextIcon
																					icon={iconsFa6.chevronDown}
																					text={tr`add_curriculum`}
																					iconOnRight={true}
																					flexStyle={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
																				/>
																			</DropdownToggle>

																			<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink">
																				<DropdownItem
																					key={"from_school_catalogue"}
																					onClick={() =>
																						TermsManagementData.handleOpenCoursesFromSchoolCatalogModal({
																							termId: term.id,
																							gradeLevelId: gradeLevel.id,
																						})
																					}
																				>
																					<span>{tr`from_school_catalogue`}</span>
																				</DropdownItem>
																				<DropdownItem
																					key={"from_course_catalogue"}
																					onClick={() => TermsManagementData.handleOpenCoursesModal()}
																				>
																					<span>{tr`from_course_catalogue`}</span>
																				</DropdownItem>
																				<DropdownItem
																					key={"from_curricula"}
																					onClick={() => TermsManagementData.handleOpenCoursesByCurriculaModal()}
																				>
																					<span>{tr`from_curricula`}</span>
																				</DropdownItem>
																			</DropdownMenu>
																		</UncontrolledDropdown>
																	</RFlex>

																	{/* add curricula loading */}
																	{TermsManagementData.makeCourseClonedLoading && (
																		<RFlex styleProps={{ alignItems: "center", marginLeft: "60px" }}>
																			<span>{tr`We are preparing your course to be cloned`}</span>
																			{TermsManagementData.makeCourseClonedLoading && <i className={iconsFa6.spinner} />}
																		</RFlex>
																	)}
																	{/* curricula */}
																	{gradeLevel?.children?.curricula &&
																		gradeLevel?.children?.curricula?.length > 0 &&
																		gradeLevel?.children?.curricula?.map((curriculum, index) =>
																			curriculum.courses?.map((course) => (
																				<RFlex
																					key={`${academicYear.id}_${term.id}_${gradeLevel.id}_${curriculum.id}_${
																						course.id
																					}${generateRandomString(5, CharactersToRandomize)}`}
																					styleProps={{ flexDirection: "column" }}
																				>
																					<RAcademicYearsContentCollapses
																						data={course}
																						type="course"
																						iD={`${academicYear.id}_${term.id}_${gradeLevel.id}_${curriculum.id}_${
																							course.id
																						}${generateRandomString(5, CharactersToRandomize)}`}
																						mLeft={"60px"}
																						termId={term.id}
																						gradeLevelId={gradeLevel.id}
																					/>
																				</RFlex>
																			))
																		)}
																	{/* weeklySchedule */}
																	{gradeLevel?.children?.weeklySchedule &&
																		gradeLevel?.children?.weeklySchedule?.length > 0 &&
																		gradeLevel?.children?.weeklySchedule?.map((wSchedule, index) => (
																			<RFlex
																				key={`${academicYear.id}_${term.id}_${gradeLevel.id}_weeklySchedule`}
																				styleProps={{ flexDirection: "column" }}
																			>
																				<RAcademicYearsContentCollapses
																					data={wSchedule}
																					type="weeklySchedule"
																					iD={`${academicYear.id}_${term.id}_${gradeLevel.id}_weeklySchedule`}
																					mLeft={"60px"}
																					termId={term.id}
																					gradeLevelId={gradeLevel.id}
																				/>
																			</RFlex>
																		))}
																</RFlex>
															</Collapse>
														</RFlex>
													))}
											</RFlex>

											<RFlex>
												{term?.gradelevels?.length > 0 ? (
													""
												) : (
													<RFlex styleProps={{ marginLeft: "40px" }}>
														<span style={{ color: greyColor }}>{tr`no grade levels yet, you can add new in`}</span>
														<span className={styles.add__academic__year} onClick={TermsManagementData.handlePushToSchoolProfile}>
															{tr`school_profile`}
														</span>
													</RFlex>
												)}
											</RFlex>
										</Collapse>
									</RFlex>
								))}
							<RFlex styleProps={{ marginLeft: "20px" }}>
								{academicYear?.terms?.length > 0 ? (
									<span
										className={styles.add__academic__year}
										onClick={() => TermsManagementData.handleOpenTermModal(null, academicYear.id)}
									>{tr`add_term`}</span>
								) : (
									<RFlex>
										<span style={{ color: greyColor }}>{tr`no_terms_yet`}</span>
										<span
											className={styles.add__academic__year}
											onClick={() => TermsManagementData.handleOpenTermModal(null, academicYear.id)}
										>
											{" "}
											{tr`add_term`}
										</span>
									</RFlex>
								)}
							</RFlex>
						</RFlex>
					</Collapse>
				</RFlex>
			))}

			<RFlex className={styles.add__academic__year} onClick={() => TermsManagementData.handleOpenAcademicYearModal()}>
				<span>{tr`add_academic_year`}</span>
			</RFlex>
		</RFlex>
	);
};

export default RTermManagementContent;
