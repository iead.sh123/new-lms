import React, { useState } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { get, destroy, post } from "config/api";
import RAddBook from "view/SchoolInitiation/Books/RAddBook";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import tr from "components/Global/RComs/RTranslator";
import Helper from "components/Global/RComs/Helper";
import { toast } from "react-toastify";

const GAddBook = ({ handleCloseAddBook, handleChangeBook, addBook, getDataFromBackend, setTableData }) => {
	const params = useParams();
	const { tabTitle, semesterId, sITypeId, curriculumId } = params;
	const [addBookLoading, setAddBookLoading] = useState(false);

	const handleSaveBook = async () => {
		setAddBookLoading(true);
		addBook?.attachment.length > 0 && delete addBook.url;
		const data = { ...addBook, curricula_id: sITypeId ?? curriculumId };
		let response;

		response = await post(`${Services.auth_organization_management.backend}api/book/store`, data);
		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					setAddBookLoading(false);

					handleCloseAddBook();
				} else {
					toast.error(response.data.msg);
					setAddBookLoading(false);
				}
			}
		} else {
			toast.error(response.data.msg);
			setAddBookLoading(false);
		}
	};

	return (
		<>
			{/* {Helper.js(params,"params")} */}
			<RAddBook
				handleChangeBook={handleChangeBook}
				handleCloseAddBook={handleCloseAddBook}
				handleSaveBook={handleSaveBook}
				addBook={addBook}
				addBookLoading={addBookLoading}
			/>
		</>
	);
};

export default GAddBook;
