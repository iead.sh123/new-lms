import React from 'react';
import { translate } from '../../../utils/translate';

const AssignmentFinalMark = ({ literalTeacherMark }) => {
    return (
        <div className="form-group row topic-area" style={{ marginTop: '0.5rem' }}>
            <label className="col-sm-3 gu-assignment_label2 gu-assignment_final_mark">{translate('Final Mark')}</label>
            <div className="col-sm-9">
                <input
                    value={literalTeacherMark || ''}
                    type="text"
                    className="form-control"
                    id="mark"
                    disabled
                />
            </div>
        </div>
    );
};

export default AssignmentFinalMark;
