import React, { useContext, useState, useMemo } from "react";
import { primaryColor, boldGreyColor, warningColor, successColor, dangerColor } from "config/constants";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import { relativeDate } from "utils/dateUtil";
import Hands_Give from "assets/img/new/Hands_Give.png";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import moment from "moment";
import styles from "../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RDiscountLister = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	const [page, setPage] = useState(1);

	const handleChangePage = (pageNumber) => {
		setPage(pageNumber);
	};

	const discountAmountComponent = ({ item }) => {
		return (
			<span
				className={styles.discount__amount}
				onClick={(event) => {
					event.stopPropagation();
					PaymentPolicyData.tabType == "archive" ? {} : PaymentPolicyData.handleSelectDiscountId(item.id + item.discountType, item);
				}}
			>
				{item.value}%
			</span>
		);
	};

	const dateComponent = ({ startAt, expiredAt }) => {
		const newDate = moment();
		const expiredDate = moment(expiredAt);
		const duration = moment.duration(expiredDate.diff(newDate));
		const hours = duration.asHours();
		const equalDates = newDate.isAfter(expiredDate, "day");
		return (
			<RFlex className={styles.date}>
				<span>
					{tr`start_at`}&nbsp;
					{moment(startAt).format("L")}
				</span>
				<span>
					{tr`expired_at`}&nbsp;
					{moment(expiredAt).format("L")}
				</span>
				<span style={{ fontSize: "12px" }}>
					{equalDates ? (
						<span style={{ color: dangerColor }}>{tr`expired`}</span>
					) : hours < 24 ? (
						<span style={{ color: warningColor }}>24 {tr`hours_left`}</span>
					) : (
						<span style={{ color: successColor }}> {relativeDate(expiredAt)}</span>
					)}
				</span>
			</RFlex>
		);
	};

	const accessComponent = ({ item }) => {
		return (
			<div>
				{item?.discountType == "General" ? (
					<RTextIcon text={tr`the_whole_organization`} icon={iconsFa6.paperclip} flexStyle={{ gap: 5, color: primaryColor }} />
				) : (
					<RTextIcon
						text={item?.numberOfItems == 0 ? tr`not_assigned_yet` : `${item?.numberOfItems} ${tr`courses`}`}
						icon={iconsFa6.paperclip}
						flexStyle={{ gap: 5, color: boldGreyColor }}
					/>
				)}
			</div>
		);
	};

	const _records = useMemo(
		() =>
			PaymentPolicyData.discountData?.data?.data?.data?.data?.map((item) => ({
				details: [
					{
						key: tr`discount`,
						value: discountAmountComponent({ item: item }),
						type: "component",
					},
					{
						key: tr`date`,
						value: dateComponent({ startAt: item.startDate, expiredAt: item?.expireDate }),
						type: "component",
					},
					{
						key: tr`access`,
						value: accessComponent({ item: item }),
						type: "component",
					},
				],
			})),
		[PaymentPolicyData.discountData?.data?.data?.data?.data]
	);
	if (PaymentPolicyData.discountLoading) return <Loader />;
	return (
		<RFlex className="mt-3" styleProps={{ flexDirection: "column" }}>
			<RFlex className={styles.div__scroll + " scroll_hidden"} styleProps={{ flexDirection: "column" }}>
				<RLister
					Records={_records}
					info={PaymentPolicyData.discountData}
					withPagination={true}
					handleChangePage={handleChangePage}
					page={page}
					Image={Hands_Give}
					line1={tr`no_discounts_yet`}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RDiscountLister;
