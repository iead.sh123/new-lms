import React from "react";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import tr from "../../../../components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./chatList.module.scss";

const ChatListFilter = ({ items, itemCallback, selectedItemIndex, customStyling = {} }) => {
	return (
		<UncontrolledDropdown direction="end">
			<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" nav style={{ ...customStyling }}>
				<RFlex styleProps={{ paddingLeft: "3px", gap: "5px", width: "100%", display: "flex", alignItems: "center", color: "black" }}>
					{items?.[selectedItemIndex]?.name}
					<i className="fa-solid fa-angle-down fa-xs" style={{ color: "black" }} />
				</RFlex>
			</DropdownToggle>
			<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right className="mb-4">
				{items?.map((item, i) => {
					return (
						<DropdownItem
							key={i}
							onClick={() => itemCallback(item)}
							style={selectedItemIndex === i ? { backgroundColor: "#DDD", color: "#FFF" } : {}}
						>
							<i>{tr(item.name)}</i>
						</DropdownItem>
					);
				})}
			</DropdownMenu>
		</UncontrolledDropdown>
	);
};

export default ChatListFilter;
