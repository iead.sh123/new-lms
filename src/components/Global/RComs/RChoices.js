import React from "react";
import {
    Button,
    ButtonGroup,
    UncontrolledTooltip,
} from "reactstrap";
import PropTypes from "prop-types";
import styles from "./RChoices.Module.scss";

const RChoices = ({ choices, setSelectedChoice, value, className, rowId,containerStyle,showAllText/*unique id for tooltip if we have table of rchoices*/ }) => {
    const [animate, setAnimate] = React.useState(false);

    return (
        <div className={className}>
            {choices.length > 5 ? (
                <select
                    //onAnimationEnd={() => setAnimate(false)}
                    // className={
                    //     animate
                    //         ? [styles["Select"], styles["Pulse"]].join(" ")
                    //         : value
                    //             ? [styles["Select"], styles["Selected-Select"]].join(" ")
                    //             : styles["Select"]
                    // }
                    onChange={(event) => {
                       // setAnimate(true);
                        setSelectedChoice(
                            choices.find((ch) => ch.value.id == event.target.value)
                                .value
                        );
                    }}
                >
                    {choices.map((ch) => (
                        <option
                            key={ch.value.id}
                            value={ch.value.id}
                            selected={ch.value.id === value}
                            className={styles["Option"]}
                        >
                            {ch.label} {!isNaN(ch.tooltip) ? `(${ch.tooltip})` : ""}
                        </option>
                    ))}
                </select>
            ) : (
                <ButtonGroup aria-label="Basic example" className={styles["Container"]} style={ containerStyle??{width: '100%' }}>
                    {choices.map((ch) => {
                        return (
                            <Button
                                style={{ height: "50px",textTransform:"none",margin:"5px" , borderRadius:"5px" }}
                               // onAnimationEnd={() => setAnimate(false)}
                                className={
                                    animate && ch.value.id === value
                                        ? [styles["Choice-Button"], styles["Pulse"]].join(" ")
                                        : [styles["tooltipped"], styles["Choice-Button"]].join(" ")
                                }
                                key={ch.value.id}
                                onClick={() => {
                                   // setAnimate(true);
                                    setSelectedChoice(ch.value);
                                }}
                                variant="secondary"
                                active={ch.value.id === value}
                                id={
                                    "tool-choice" +
                                    // ch.label.substring(0, 1) +
                                    ch.value.id + rowId
                                }
                               
                            >


                                {/* {ch.label.toString().length > 15 ? (
                                    <>
                                        <span

                                            id={
                                                "tools" +
                                                ch.label.substring(0, 2) +
                                                ch.value.id +
                                                ch.label.substring(0, 2)
                                            }
                                        >
                                            {ch.label.substring(0, 13) + "..."}


                                        </span>
                                        <div className={styles["tooltip"]} > {ch.label} </div></>
                                ) : (
                                    <span >{ch.label}</span>
                                )} */}
                                <span>{ch.label.toString().length > 15 && (!showAllText) ? ch.label.substring(0, 13) + "..." : ch.label}</span>

                                <UncontrolledTooltip
                                    delay={0}
                                    target={
                                        "tool-choice" +
                                        // ch.label.substring(0, 1) +
                                        ch.value.id + rowId
                                        // ch.label.substring(0, 2)
                                    }
                                >
                                    {ch.tooltip}
                                </UncontrolledTooltip>
                            </Button>
                        );
                    })}
                </ButtonGroup>
            )}
        </div>
    );
};

RChoices.propTypes = {
    choices: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.object.isRequired,
        })
    ).isRequired,
    value: PropTypes.string,
    setSelectedChoice: PropTypes.func.isRequired,
};

export default React.memo(RChoices);
