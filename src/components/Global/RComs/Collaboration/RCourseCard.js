import React from "react";
import "./RCourseCard.css";
import RTagViewer from "./RTagViewer";
import Helper from "../Helper";
import RLiveAction from "./RLiveAction";
const RCourseCard = (props) => {
	// Helper.cl(props,"RCollaborationCard props");
	// return <div> dfgs dfg sdfg sdf g</div>
	const { onClick1, bgImage, title, tags, actions } = props;
	//Helper .cl(props,"RCourseCard props")

	return (
		<div className="course_card" onClick={onClick1} style={{ border:"blue 1px solid",borderRadius:"",backgroundImage: `url(${bgImage}` }}>
			{/* {Helper.js(props,"props")} */}
			<div class="overlay">
				<div className="course_title">{title.text}
				
			
				</div>

				<div className="course_tags">
					{tags && tags.length ? tags.map((i) => <RTagViewer name={i.name} iconColor={i.iconColor} backgroundColor={i.backgroundColor} />):<></>}
				</div>
				<div className="course_actions">
					{actions &&
						actions.length &&
						actions.map((action) => (
							// <button
							// 	className="course_action_btn"
							// 	onClick={action.onClick}
							// 	style={{ color: action.color, border: `1px solid ${action.color}` }}
							// >
							// 	{action.icon && <i className={action.icon} />}
							// 	{action.title && <span> {action.title} </span>}
							// </button>
							<RLiveAction 

							style={{
								borderRadius: "2px",
								border: "1px solid #668AD7)",
								fontWeight: "500",
								lineHeight: "normal",
								textTransform: "capitalize",
								paddingLeft: "10px",
								paddingRight: "10px",
								paddingTop: "5px",
								paddingBottom: "5px",
								marginLeft: "5px",
								marginRight: "5px",
							}}
								labelStyle={{
									paddingLeft: "10px",
									paddingRight: "10px",
									paddingTop: "5px",
									paddingBottom: "5px",
									marginLeft: "5px",
									marginRight: "5px",
								}}
								color="#668AD7" border={true} 
							onClick={action.onClick}
							icon={action.icon}
							text={action.title}
							></RLiveAction>
						))}
				</div>
			</div>
		</div>
	);
};
export default RCourseCard;
