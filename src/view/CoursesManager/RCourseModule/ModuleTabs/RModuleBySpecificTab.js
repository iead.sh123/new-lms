import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import RSpecificModuleContent from "../SpecificModuleContent/RSpecificModuleContent";
import RAllModuleContent from "../AllModuleContent/RAllModuleContent";

const RModuleBySpecificTab = () => {
	const CourseModuleData = useContext(CourseModuleContext);

	return <section className="mt-4">{!CourseModuleData.moduleIdParams ? <RAllModuleContent /> : <RSpecificModuleContent />}</section>;
};

export default RModuleBySpecificTab;
