import { termsApi } from "api/terms";
import tr from "components/Global/RComs/RTranslator";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import { elementsOpType } from "../constants/elements.optype.constant";
import { enrollment_components } from "../constants/enrolment.components.constant";
import { pages } from "../constants/pages.constant";
import { toast } from "react-toastify";

//steppers
export const NAVIGATE_STEPPER = "stepper/navigate";

//academic years
export const LOAD_ACADEMIC_YEARS = "academic-years/load";
export const ADD_ACADEMIC_YEAR = "academic-years/add";
export const UPDATE_ACADEMIC_YEAR = "academic-years/update";
export const REPLACE_ACADEMIC_YEAR = "academic-years/replace";
export const DELETE_ACADEMIC_YEAR = "academic-years/delete";
export const SELECT_ACADEMIC_YEAR = "academic-years/select";

//terms
export const LOAD_TERMS = "terms/load";
export const MANAGE_TERM = "terms/manage";
export const ADD_TERM = "terms/add";
export const UPDATE_TERM = "terms/update";
export const REPLACE_TERM = "terms/replace";
export const DELETE_TERM = "terms/delete";

//homerooms
export const MANAGE_HOMEROOM = "homerooms/manage";
export const ADD_HOMEROOM = "homerooms/add";
export const UPDATE_HOMEROOM = "homerooms/update";
export const REPLACE_HOMEROOM = "homerooms/replace";
export const DELETE_HOMEROOM = "homerooms/delete";
export const SET_EDITED_HOME_ROOM = "homerooms/setEdited";
export const ENROLL_HOME_ROOM_USER = "homeroom/enrollUser";
export const REMOVE_HOME_ROOM_USER = "homeroom/removeUser";

//grade levels
export const LOAD_GRADE_LEVELS = "gradeLevels/load";
export const MANAGE_GRADE_LEVEL = "gradeLevels/manage";

//curricula
export const LOAD_CURRICULA = "curricula/load";
export const MANAGE_CURRICULUM = "curricula/manage";

//weekly schedule events
export const LOAD_WEEKLY_SCHDEULE_EVENTS = "weeklySchedule/load";
export const DELETE_WEEKLY_SCHEDULE_EVENT = "weeklySchedule/delete";
export const LOAD_AVAILABLE_WEEKLY_SCHEDULE_EDITOR_ELEMENTS = "weeklySchedule/availableElements";
export const ADD_WEEKLY_SCHEDULE_ELEMENT = "weeklySchedule/addElement";
export const UPDATE_WEEKLY_SCHEDULE_ELEMENT = "weeklySchedule/updateElement";

export const ADD_OTHER_WEEKLY_SCHEDULE_ELEMENT = "weeklySchedule/addOtherElement";
export const UPDATE_OTHER_WEEKLY_SCHEDULE_ELEMENT = "weeklySchedule/updateOtherElement";
export const DELETE_OTHER_WEEKLY_SCHEDULE_ELEMENT = "weeklySchedule/deleteOtherElement";

//users
export const LOAD_ORGANIZATION_TEACHERS = "users/loadTeachers";
export const LOAD_GRADE_LEVELS_STUDENTS = "users/loadGradeLevelStudents";

//building payload
export const ADD_TO_PAYLOAD = "payload/add";

export const SET_EDITED_COURSE = "courses/setEdited";
export const ENROLL_COURSE_USER = "courses/enrollUser";
export const REMOVE_COURSE_USER = "courses/removeUser";

//courses
export const CREATE_COURSE = "courses/create";

const fakeIdsGenerator = (function* () {
	let i = 0;
	while (true) {
		yield "f_" + ++i;
	}
})();

export const loadAcademicYears = async (dispatch, mounted) => {
	try {
		const response = await termsApi.academicYears();

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_ACADEMIC_YEARS,
				payload: {
					byId:
						response.data?.data?.reduce?.(
							(prev, curr) => ({
								...prev,
								[curr.id]: curr,
							}),
							{}
						) ?? {},
					ids: response.data?.data?.map?.((d) => d.id) ?? [],
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const addAcademicYear = async (dispatch, mounted, academicYearData) => {
	try {
		const response = await termsApi.saveAcademicYear(academicYearData);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_ACADEMIC_YEAR,
				payload: response.data.data,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const updateAcademicYear = async (dispatch, mounted, id, data) => {
	try {
		const response = await termsApi.updateAcademicYear(id, data);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: REPLACE_ACADEMIC_YEAR,
				payload: { id, data },
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const updateTerm = async (dispatch, mounted, id, data) => {
	try {
		const response = await termsApi.updateTerm(id, data);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: REPLACE_TERM,
				payload: { id, data },
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const loadTerms = async (dispatch, mounted, academicYearId) => {
	try {
		const response = await termsApi.terms(academicYearId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_TERMS,
				payload: {
					byId:
						response.data?.data?.reduce?.(
							(prev, curr) => ({
								...prev,
								[curr.id]: curr,
							}),
							{}
						) ?? {},
					ids: response.data?.data?.map?.((d) => d.id) ?? [],
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const addTerm = async (dispatch, mounted, academicYearId, termData) => {
	try {
		const response = await termsApi.saveTerm(academicYearId, termData);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_TERM,
				payload: response.data.data,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const selectAcademicYear = (academicYearId, dispatch, dontChangePage) => {
	return dispatch({ type: SELECT_ACADEMIC_YEAR, payload: { dontChangePage, academicYearId } });
};

export const activateTerm = async (termId, mounted, dispatch) => {
	try {
		const response = await termsApi.activateTerm(termId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_TERM,
				payload: { id: termId, field: { name: "isActive", value: true } },
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const deactivateTerm = async (termId, mounted, dispatch) => {
	try {
		const response = await termsApi.deactivateTerm(termId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_TERM,
				payload: { id: termId, field: { name: "isActive", value: false } },
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const finalizeTerm = async (termId, mounted, dispatch) => {
	try {
		const response = await termsApi.finalizeTerm(termId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_TERM,
				payload: { id: termId, field: { name: "isFinalized", value: true } },
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const replaceAcademicYear = (dispatch, id, data) => {
	return dispatch({ type: REPLACE_ACADEMIC_YEAR, payload: { id, data } });
};

export const replaceTerm = (dispatch, id, data) => {
	return dispatch({ type: REPLACE_TERM, payload: { id, data } });
};

export const navigateStepper = (page, dispatch) => {
	return dispatch({ type: NAVIGATE_STEPPER, payload: page });
};

export const manageTerm = async (mounted, dispatch, termId, dontChangePage = false) => {
	try {
		const response = await termsApi.gradeLevels(termId);
		if (!response.data.status) throw new Error(response.data.msg);
		mounted?.current &&
			dispatch({
				type: MANAGE_TERM,
				payload: {
					dontChangePage,
					id: termId,
					data: {
						gradeLevels: {
							byId: response.data?.data?.reduce?.((prev, curr) => ({ ...prev, [curr.id]: curr }), {}),
							ids: response.data?.data?.map?.((d) => d.id),
						},
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const manageGradeLevel = async (mounted, dispatch, termId, gradeLvelId, dontChangePage = false) => {
	try {
		const response = await termsApi.homerooms(termId, gradeLvelId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: MANAGE_GRADE_LEVEL,

				payload: {
					id: gradeLvelId,
					dontChangePage,
					data: {
						homerooms: {
							byId:
								response.data?.data?.reduce?.(
									(prev, curr) => ({
										...prev,
										[curr.id]: curr,
									}),
									{}
								) ?? {},
							ids: response.data?.data?.map?.((d) => d.id) ?? [],
						},
					},
				},
			});

		// mounted?.current && loadOrganizationTeachers();
	} catch (err) {
		return toast.error(err?.message);
	}
};

// export const addHomeRoom = async (dispatch, mounted, termId, data) => {

//     try {

//         const response = await termsApi.createHomeroom(termId, data);

//         if (!response.data.status)
//             throw new Error(response.data.msg);

//         mounted?.current && dispatch({
//             type: ADD_HOMEROOM,
//             payload: response.data.data
//         });

//         return response

//     } catch (err) {
//         return toast.error(err?.message);

//     }
// }

export const loadCurricula = async (dispatch, mounted, termId, gradeLevelId) => {
	try {
		const response = await termsApi.curricula(termId, gradeLevelId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_CURRICULA,
				payload: {
					byId:
						response.data?.data?.reduce?.(
							(prev, curr) => ({
								...prev,
								[curr.id]: curr,
							}),
							{}
						) ?? {},
					ids: response.data?.data?.map?.((d) => d.id) ?? [],
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const loadWeeklyScheduleEvents = async (dispatch, mounted, termId, gradeLevelId) => {
	try {
		const response = await termsApi.weeklyScheduleElements(termId, gradeLevelId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_WEEKLY_SCHDEULE_EVENTS,
				payload: {
					periods: {
						byId:
							response.data?.data?.periods?.reduce?.(
								(prev, curr) => ({
									...prev,
									[curr.id]: curr,
								}),
								{}
							) ?? {},
						ids: response.data?.data?.periods?.map?.((d) => d.id) ?? [],
					},
					elements: {
						byId:
							response.data?.data?.elements?.reduce?.(
								(prev, curr) => ({
									...prev,
									[curr.id]: curr,
								}),
								{}
							) ?? {},
						ids: response.data?.data?.elements?.map?.((d) => d.id) ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const deleteWeeklyScheduleEvent = async (dispatch, mounted, id) => {
	try {
		const response = await termsApi.deleteWeeklyScheduleElement(id);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: DELETE_WEEKLY_SCHEDULE_EVENT,
				payload: id,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const getAvailableWeeklyScheduleElements = async (dispatch, mounted, gradeLevelId, termId) => {
	try {
		const response = await termsApi.availableWeeklyScheduleItems(gradeLevelId, termId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_AVAILABLE_WEEKLY_SCHEDULE_EDITOR_ELEMENTS,
				payload: {
					elements: {
						curricula: {
							byId:
								response.data?.data?.curricula?.reduce?.(
									(prev, curr) => ({
										...prev,
										[curr.id]: curr,
									}),
									{}
								) ?? {},
							ids: response.data?.data?.curricula?.map?.((d) => d.id) ?? [],
						},
						homerooms: {
							byId:
								response.data?.data?.homeRooms?.reduce?.(
									(prev, curr) => ({
										...prev,
										[curr.id]: curr,
									}),
									{}
								) ?? {},
							ids: response.data?.data?.homeRooms?.map?.((d) => d.id) ?? [],
						},
						others: {
							byId:
								response.data?.data?.others?.reduce?.(
									(prev, curr) => ({
										...prev,
										[curr.id]: curr,
									}),
									{}
								) ?? {},
							ids: response.data?.data?.others?.map?.((d) => d.id) ?? [],
						},
					},
					types: {
						byId:
							response.data?.data?.types?.reduce?.(
								(prev, curr) => ({
									...prev,
									[curr.id]: curr,
								}),
								{}
							) ?? {},
						ids: response.data?.data?.types?.map?.((d) => d.id) ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const addOtherElementType = async (dispatch, mounted, payload) => {
	try {
		const response = await termsApi.createWeeklyOtherScheduleItemType(payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_OTHER_WEEKLY_SCHEDULE_ELEMENT,
				payload: response.data.data,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const updateOtherElementType = async (dispatch, mounted, id, payload) => {
	try {
		const response = await termsApi.updateWeeklyScheduleOtherItemType(id, payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_OTHER_WEEKLY_SCHEDULE_ELEMENT,
				payload: response.data.data,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const deleteOtherElementType = async (dispatch, mounted, id) => {
	try {
		const response = await termsApi.deleteWeeklyScheduleOtherItemType(id);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: DELETE_OTHER_WEEKLY_SCHEDULE_ELEMENT,
				payload: id,
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const addWeeklyScheduleElement = async (dispatch, mounted, gradeLevelId, termId, payload) => {
	try {
		const response = await termsApi.addWeeklyScheduleElement(gradeLevelId, termId, { elements: [{ ...payload }] });

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_WEEKLY_SCHEDULE_ELEMENT,
				payload: response?.data?.data?.[0],
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//direct update deprecated
export const updateWeeklyScheduleElement = async (dispatch, mounted, gradeLevelId, termId, payload) => {
	try {
		const response = await termsApi.updateWeeklyScheduleElement(gradeLevelId, termId, {
			elements: [{ ...payload }],
			elementIdsToDelete: [],
		});

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_WEEKLY_SCHEDULE_ELEMENT,
				payload: response?.data?.data?.[0],
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

//HERE
export const addToPayload = async (dispatch, opType, element) => {
	if (opType === elementsOpType.ADD) {
		const fId = fakeIdsGenerator.next().value;
		dispatch({ type: ADD_TO_PAYLOAD, payload: { opType, element: { ...element, id: fId } } });
		return dispatch({ type: ADD_WEEKLY_SCHEDULE_ELEMENT, payload: { ...element, id: fId, period: { id: element.periodId } } });
	}
	if (opType === elementsOpType.UPDATE) {
		dispatch({ type: ADD_TO_PAYLOAD, payload: { opType, element } });
		return dispatch({
			type: UPDATE_WEEKLY_SCHEDULE_ELEMENT,
			payload: { ...element, ...(element.periodId ? { period: { id: element.periodId } } : {}) },
		});
	}
	if (opType === elementsOpType.DELETE) {
		dispatch({ type: ADD_TO_PAYLOAD, payload: { opType, element } });
		return dispatch({ type: DELETE_WEEKLY_SCHEDULE_EVENT, payload: element.id });
	}
};

//HERE
export const saveWeeklySchedule = async (dispatch, mounted, gradeLevelId, termId, payload) => {
	try {
		const response = await termsApi.updateWeeklyScheduleElement(gradeLevelId, termId, payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current && (await loadWeeklyScheduleEvents(dispatch, mounted, termId, gradeLevelId));
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const setEditedHomeRoom = async (dispatch, mounted, homeRoomId) => {
	try {
		const response = await termsApi.getHomeRoomById(homeRoomId);

		if (!response.data.status) throw new Error(response.data.msg);

		const teachers = response.data.data.users.filter((us) => us.type.name.toLowerCase() === "teacher");
		const students = response.data.data.users.filter((us) => us.type.name.toLowerCase() === "student");

		mounted?.current &&
			dispatch({
				type: SET_EDITED_HOME_ROOM,
				payload: {
					...response.data.data,
					teachers: {
						byId:
							teachers.reduce(
								(prev, curr) => ({
									...prev,
									[curr.user_id]: curr,
								}),
								{}
							) ?? {},
						ids: teachers.map((t) => t.user_id) ?? [],
					},
					students: {
						byId:
							students.reduce(
								(prev, curr) => ({
									...prev,
									[curr.user_id]: curr,
								}),
								{}
							) ?? {},
						ids: students.map((t) => t.user_id) ?? [],
					},
					users: undefined,
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const loadOrganizationTeachers = async (dispatch, mounted, termId, searchText) => {
	try {
		const response = await termsApi.organizationTeachers(termId, searchText);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_ORGANIZATION_TEACHERS,
				payload: {
					byId:
						response.data.data.reduce(
							(prev, curr) => ({
								...prev,
								[curr.id]: curr,
							}),
							{}
						) ?? {},
					ids: response.data.data.map((t) => t.id) ?? [],
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const loadstudentsByGradeLevels = async (dispatch, mounted, termId, gradeLevelIds) => {
	try {
		const response = await termsApi.organizationStudents(termId, gradeLevelIds);

		if (!response.data.status) throw new Error(response.data.msg);

		const students = response.data.data; //.reduce((prev, curr) => [...prev, ...curr.students], []);

		mounted?.current &&
			dispatch({
				type: LOAD_GRADE_LEVELS_STUDENTS,
				payload: {
					byId:
						students.reduce(
							(prev, curr) => ({
								...prev,
								[curr.id]: curr,
							}),
							{}
						) ?? {},
					ids: students.map((t) => t.id) ?? [],
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const enrollHomeRoomUser = async (dispatch, mounted, homeRoomId, user) => {
	try {
		const payload = {
			learners:
				user.type === enrollment_components.STUDENT
					? [
							{
								user_id: user.id,
							},
					  ]
					: [],
			teachers:
				user.type === enrollment_components.TEACHER
					? [
							{
								user_id: user.id,
							},
					  ]
					: [],
		};

		const response = await termsApi.enrollUsersInHomeRoom(homeRoomId, payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ENROLL_HOME_ROOM_USER,
				payload: {
					homeroomId: homeRoomId,
					user: {
						image: user.hash_id,
						user_id: user.id,
						full_name: user.name,
						type: { name: user.type },
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const deleteHomeRoomUsers = async (dispatch, mounted, homeRoomId, type, userId) => {
	try {
		const response = await termsApi.deleteHomeRoomUsers(homeRoomId, { users: [+userId] });

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: REMOVE_HOME_ROOM_USER,
				payload: {
					homeroomId: homeRoomId,
					type,
					userId: +userId,
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const manageCurriculum = async (dispatch, mounted, curriculum) => {
	dispatch({ type: MANAGE_CURRICULUM, payload: curriculum.id });

	curriculum?.courses?.[0]?.id && loadCourse(dispatch, mounted, curriculum.courses[0].id);
};

export const loadCourse = async (dispatch, mounted, courseId) => {
	try {
		const response = await termsApi.getCourseById(courseId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: SET_EDITED_COURSE,
				payload: {
					...response.data.data,
					teachers: {
						ids: response.data.data.teachers.map((t) => t.user_id),
						byId: response.data.data.teachers.reduce(
							(prev, curr) => ({ ...prev, [curr.user_id]: { ...curr, full_name: curr.user_name } }),
							{}
						),
					},
					students: {
						ids: response.data.data.students.map((t) => t.user_id),
						byId: response.data.data.students.reduce(
							(prev, curr) => ({ ...prev, [curr.user_id]: { ...curr, full_name: curr.user_name } }),
							{}
						),
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const enrollUsersInCourse = async (dispatch, mounted, courseId, user) => {
	try {
		const payload = {
			learners: user.type === enrollment_components.STUDENT ? [user.id] : [],
			teachers: user.type === enrollment_components.TEACHER ? [user.id] : [],
		};
		alert(courseId);
		const response = await termsApi.enrollUsersInCourse(courseId, payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ENROLL_COURSE_USER,
				payload: {
					courseId,
					user: {
						image: user.hash_id,
						user_id: user.id,
						full_name: user.name,
						type: { name: user.type },
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const deleteCourseUsers = async (dispatch, mounted, courseId, type, userId) => {
	try {
		const response = await termsApi.deleteCourseUsers(courseId, { users: [+userId] });

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: REMOVE_COURSE_USER,
				payload: {
					courseId,
					type,
					userId: userId,
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const createHomeRoom = async (dispatch, mounted, termId, gradeLevelId, data) => {
	try {
		const response = await termsApi.createHomeRoom(termId, { homerooms: [{ ...data, gradeLevelId }] });

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_HOMEROOM,
				payload: response?.data?.data?.[0],
			});

		return response?.data?.data?.[0]?.id;
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const createCourse = async (dispatch, mounted, payload) => {
	try {
		const response = await termsApi.createCourse(payload);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: CREATE_COURSE,
				payload: { id: +response?.data?.data?.id, name: response?.data?.data?.name },
			});

		return response?.data?.data?.id;
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const changeUserClass = async (dispatch, mounted, type, user, oldCourseId, newCourseId) => {
	try {
		await deleteCourseUsers(dispatch, mounted, oldCourseId, type, user.id);
		await enrollUsersInCourse(dispatch, mounted, newCourseId, user);
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const changeUserHomeroom = async (dispatch, mounted, type, user, oldHomeRoomId, newHomeRoomId) => {
	try {
		await deleteHomeRoomUsers(dispatch, mounted, oldHomeRoomId, type, user.id);
		await enrollHomeRoomUser(dispatch, mounted, newHomeRoomId, user);
	} catch (err) {
		return toast.error(err?.message);
	}
};
