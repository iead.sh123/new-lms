import { get, post } from "config/api";

export const lessonAssignmentsExamsApi = {
  getLessonAssignments: (rabId) =>
    get(`tch-api/rab/get-rab-qs/${rabId}/assignment`),

  getLessonExams: (rabId) => get(`tch-api/rab/get-rab-qs/${rabId}/quiz`),

  getStudentsAndPatterns: (rabId, questionSetId) =>
    get(`tch-api/question-set/studentsAndpatterns/${rabId}/${questionSetId}`),

  getSuggestedPeriods: (lessonId, rabId) =>
    get(`tch-api/question-set/get-suggested-periods/${lessonId}/${rabId}`),

  insertLink: (linkContent) => post(`tch-api/question-set/assign`, linkContent),

  getQuestionSetClosed: (lessonId) =>
    get(`tch-api/question-set/lesson/${lessonId}`),

  sendNotificationAssignmentExamsClosed: (questionSetId, lessonId) =>
    post(
      `tch-api/question-set/${questionSetId}/lessons/${lessonId}/send-notifications`
    ),

  updateQuestionSetFromUnlinkToLink: (lessonId, questionSetId) =>
    post(`tch-api/question-set/unLinkAssignment/${lessonId}/${questionSetId}`),

  getReportById: (reportId) => get(`emp-api/reports/${reportId}`),

  showReportGridView: (reportId, questionSetId, rabId) =>
    post(`emp-api/reports/call-report/${reportId}`, questionSetId, rabId),
};
