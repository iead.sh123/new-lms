import React, { useEffect, useState } from "react";
import "./GReportedContent.css";

import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import RDiscussionPostDesign from "view/Discussion/RDiscussionPostDesign";
import RFlex from "components/Global/RComs/RFlex/RFlex";

import styles from "view/Discussion/RDiscussionPostDesign.js";
import RSeeMoreText from "view/Discussion/RSeeMoreText";
import RHeader from "components/Global/RComs/RHeader";
export const DiscussionContext = React.createContext();

const GRecentlyAddedContents = ({}) => {
	return <>GRecently Added Contents</>;
};
export default GRecentlyAddedContents;
