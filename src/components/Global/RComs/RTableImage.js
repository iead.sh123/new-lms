import React, { useState } from "react";
import { userInteractionsAsync } from "store/actions/global/track.action";
import { useDispatch } from "react-redux";
import { imageTypes, fileTypes, fileExcel, fileWord, filePowerPoint, filePdf, fileVideo, fileAudio } from "config/mimeTypes";
import powerPointIcon from "assets/img/png/powerpoint.png";
import defaultImage from "assets/img/avatar.png";
import excelIcon from "assets/img/png/excel.png";
import videoIcon from "assets/img/png/multimedia.png";
import audioIcon from "assets/img/png/sound.png";
import fileIcon from "assets/img/png/file.png";
import wordIcon from "assets/img/png/word.png";
import AppModal from "../ModalCustomize/AppModal";
import pdfIcon from "assets/img/png/pdf.png";
import tr from "./RTranslator";
import { Services } from "engine/services";

const RTableImage = ({ detail }) => {
	const dispatch = useDispatch();
	const [imageView, setImageView] = useState(false);
	const [startTime, setStartTime] = useState(null);

	const handleShowImageView = () => {
		setImageView(!imageView);
		setStartTime(Date.now());
	};

	const handleCloseImageView = () => {
		setImageView(!imageView);
		const elapsed = Math.floor((Date.now() - startTime) / 1000);
		dispatch(
			userInteractionsAsync(detail?.learning_object_id, {
				interaction_time_in_seconds: elapsed,
				interaction_type_id: 5,
			})
		);
	};

	return (
		<>
			<AppModal
				size="lg"
				show={imageView}
				parentHandleClose={handleCloseImageView}
				header={tr`Image`}
				headerSort={
					<div style={{ textAlign: "center" }}>
						<a target="_blank" href={detail?.src}>
							<img
								src={
									detail.value
										? Services.storage.file + detail.value
										: imageTypes.includes(detail.mime_type)
										? detail?.src
										: fileTypes.includes(detail.mime_type)
										? fileIcon
										: fileWord.includes(detail.mime_type)
										? wordIcon
										: fileExcel.includes(detail.mime_type)
										? excelIcon
										: filePowerPoint.includes(detail.mime_type)
										? powerPointIcon
										: filePdf.includes(detail.mime_type)
										? pdfIcon
										: fileVideo.includes(detail.mime_type)
										? videoIcon
										: fileAudio.includes(detail.mime_type)
										? audioIcon
										: defaultImage
								}
								alt="image"
								width="300px"
								height="300px"
							/>
						</a>
					</div>
				}
			/>

			<img
				src={
					detail.value
						? Services.storage.file + detail.value
						: imageTypes.includes(detail.mime_type)
						? detail?.src
						: fileTypes.includes(detail.mime_type)
						? fileIcon
						: fileWord.includes(detail.mime_type)
						? wordIcon
						: fileExcel.includes(detail.mime_type)
						? excelIcon
						: filePowerPoint.includes(detail.mime_type)
						? powerPointIcon
						: filePdf.includes(detail.mime_type)
						? pdfIcon
						: fileVideo.includes(detail.mime_type)
						? videoIcon
						: fileAudio.includes(detail.mime_type)
						? audioIcon
						: defaultImage
				}
				alt="image"
				width="40px"
				height="40px"
				style={{ cursor: "pointer" }}
				onClick={handleShowImageView}
			/>
		</>
	);
};

export default RTableImage;
