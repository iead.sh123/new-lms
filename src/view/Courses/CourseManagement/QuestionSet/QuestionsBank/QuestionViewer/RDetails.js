import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { TransformedTags } from "utils/TransformedTags";
import { relativeDate } from "utils/HandleDate";
import { Services } from "engine/services";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";

const RDetails = () => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex>
				<i className="fa-regular fa-calendar-check" />
				<h6>{tr`last_edit`} : </h6>
				<span className="text-muted">{relativeDate(QuestionsBankData?.questionsBankSelected?.updated_at)}</span>
			</RFlex>
			{/* <RFlex>
					<i className="fas fa-user-friends" />
					<h6>{tr`usage_times`} : </h6>
				</RFlex> */}
			<RFlex styleProps={{ alignItems: "center" }}>
				<i className="fas fa-user" style={{ paddingBottom: "12px", position: "relative" }} />
				<h6>{tr`author`} : </h6>
				<RFlex styleProps={{ alignItems: "center" }}>
					<img
						src={
							QuestionsBankData?.questionsBankSelected?.creator?.image == undefined
								? UserAvatar
								: Services.storage.file + QuestionsBankData?.questionsBankSelected?.creator?.image
						}
						alt={QuestionsBankData?.questionsBankSelected?.creator?.image}
						width="40px"
						height="40px"
						style={{ borderRadius: "100%" }}
					/>
					<h6>{QuestionsBankData?.questionsBankSelected?.creator?.full_name}</h6>
				</RFlex>
			</RFlex>
			{QuestionsBankData?.questionsBankSelected?.tags?.length > 0 && (
				<>
					<RFlex>
						<i className="fas fa-tag" />
						<h6>{tr`tags`} : </h6>
					</RFlex>
					<TransformedTags tags={QuestionsBankData?.questionsBankSelected?.tags} />
				</>
			)}
		</RFlex>
	);
};

export default RDetails;
