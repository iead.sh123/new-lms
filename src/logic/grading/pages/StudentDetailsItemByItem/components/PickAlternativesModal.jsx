import RFlex from "components/Global/RComs/RFlex/RFlex";
import RModal from "components/Global/RComs/RModal";
import tr from "components/Global/RComs/RTranslator";
import { entities } from "logic/grading/constants/entities.constants";
import React from "react";
import RGradesTable from "view/grading/RGradesTable/RGradesTable";

import { Row, Col } from "reactstrap";
import RButton from "components/Global/RComs/RButton";
import { gradingApi } from "api/grading";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import Loader from "utils/Loader";
import { SUCCESS } from "utils/Alert";
import { isConstructorDeclaration } from "typescript";
import { toast } from "react-toastify";

const PickAlternativesModal = ({ gradableItemsCollection, itemCompositeId, isOpen, toggle, handleSubmitAlternatives }) => {
	const mounted = React.useRef(false);

	const [addedAlternatives, setAddedAlternatives] = React.useState([]);
	const [removedAlternatives, setRemovedAlternatives] = React.useState([]);
	const [loadedItemAlternatives, setLoadedItemAlternatives] = React.useState([]);
	const [loading, setLoading] = React.useState(false);

	const handleLoadAlternatives = async () => {
		try {
			setLoading(true);
			setAddedAlternatives([]);
			setRemovedAlternatives([]);
			const [type, id] = itemCompositeId?.split?.("_") ?? [null, null];
			const response = await gradingApi.loadItemAlternatives(type, id);
			if (!response.data?.status) throw new Error(response.data?.msg);
			const alternatives = response.data.data.map((item) => ({ id: item.type + "_" + item.id, type: item.type }));
			setLoadedItemAlternatives(alternatives);
			setAddedAlternatives(alternatives);
		} catch (err) {
			return toast.error(err?.message);
		} finally {
			setLoading(false);
		}
	};

	const handleSubmit = async () => {
		try {
			setLoading(true);

			await handleSubmitAlternatives(itemCompositeId, {
				added_alternatives: addedAlternatives.map((alt) => ({ type: alt.type, id: +alt?.id?.split?.("_")?.[1] })),
				removed_alternatives: removedAlternatives.map((alt) => ({ type: alt.type, id: +alt?.id?.split?.("_")?.[1] })),
			});

			toggle();
		} catch (err) {
			return toast.error(err?.message);
		} finally {
			setLoading(false);
		}
	};

	React.useEffect(() => {
		mounted.current = true;

		itemCompositeId && handleLoadAlternatives();

		return () => {
			mounted.current = false;
		};
	}, [itemCompositeId]);

	const handleAddAlternative = (item) => {
		if (removedAlternatives.some((al) => al.type === item.type && al.id === item.id)) {
			setRemovedAlternatives(removedAlternatives.filter((al) => !(al.type === item.type && al.id === item.id)));
		}

		if (!loadedItemAlternatives.some((al) => al.type === item.type && al.id === item.id)) {
			setAddedAlternatives([...addedAlternatives, { type: item.type, id: item.id }]);
		}
	};

	const handleRemoveAlternative = (item) => {
		if (addedAlternatives.some((al) => al.type === item.type && al.id === item.id)) {
			setAddedAlternatives(addedAlternatives.filter((al) => !(al.type === item.type && al.id === item.id)));
		}

		if (!loadedItemAlternatives.some((al) => al.type === item.type && al.id === item.id)) {
			setRemovedAlternatives([...removedAlternatives, { type: item.type, id: item.id }]);
		}
	};

	return (
		<RModal
			isOpen={isOpen}
			withCloseIcon={false}
			toggle={toggle}
			header={
				<RFlex>
					<p>{tr`Create Academic Year`}</p>
				</RFlex>
			}
			footer={
				<RFlex styleProps={{ width: "100%", alignItems: "start" }}>
					{/* <RButton
                    text={edited?.id ? tr`update` : tr`create`}
                    color="primary"
                    onClick={handleCreate}
                // style={{alignSelf: 'end'}}
                />

                <RButton
                    text={tr`cancel`}
                    style={{ background: 'white', color: '#668AD7' }}
                    // faicon="fa fa-plus"
                    // color="link"
                    onClick={toggle}
                // style={{alignSelf: 'end'}}
                /> */}
				</RFlex>
			}
			body={
				loading ? (
					<Loader />
				) : (
					<>
						<Row>
							<Col xs={4}>
								{/* <RSelect
                        option={users}
                        closeMenuOnSelect={true}
                        placeholder={tr`select_user_type`}
                        isMulti
                        onChange={(e) => {
                            handleSelectUsers(e);
                        }}
                    /> */}
								search input goes here
							</Col>

							<Col xs={4}>
								<RButton
									text={tr`Save`}
									onClick={handleSubmit}
									color="link"
									disabled={!addedAlternatives?.length && !removedAlternatives?.length}
								/>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<RGradesTable
									keyPrefix={"alt_" + entities.gradableItems}
									headers={gradableItemsCollection.headers.filter((h) => h.value === "points" || h.value === "type" || h.value === "name")}
									dataCollection={{ ...gradableItemsCollection, ids: gradableItemsCollection.ids.filter((i) => i !== itemCompositeId) }}
									title={tr("Gradable Items")}
									deriveActions={(rec) => {
										return [
											{
												name: addedAlternatives.some((t) => rec?.type === t.type && rec?.id === t.id)
													? tr("remove alternative")
													: tr("add alternative"),
												icon: addedAlternatives.some((t) => rec?.type === t.type && rec?.id === t.id) ? "fa fa-minus" : "fa fa-plus",
												color: addedAlternatives.some((t) => rec?.type === t.type && rec?.id === t.id) ? "danger" : "primary",
												//   outline
												onClick: (r) =>
													addedAlternatives.some((t) => rec?.type === t.type && rec?.id === t.id)
														? handleRemoveAlternative(r)
														: handleAddAlternative(r),
											},
										];
									}}
									loading={false}
									disableAdditon
									hideTitle
								/>
							</Col>

							<Col xs={12}>
								{/* <NewPaginator
                            firstPageUrl={FirstPageUrl}
                            lastPageUrl={LastPageUrl}
                            currentPage={CurrentPage}
                            lastPage={LastPage}
                            prevPageUrl={PrevPageUrl}
                            nextPageUrl={NextPageUrl}
                            total={Total}
                            userSelected={userSelected}
                            getAction={getAvailableUsersAsync}
                        /> */}
							</Col>

							<Col xs={12} style={{ display: "flex", justifyContent: "flex-end" }}>
								{/* <Button
                            block
                            className="btn-round ml-4 mb-2 mt-1 "
                            color="primary"
                            type="submit"
                            disabled={userChecked.length == 0 ? true : false}
                            onClick={(e) => handleSave(e)}
                            style={{ width: 100 }}
                        >
                            {saveAvailableUsersLoading ? (
                                <>
                                    {tr`save`} <i className="fa fa-refresh fa-spin"></i>
                                </>
                            ) : (
                                tr("save")
                            )}
                        </Button>
    
                        <RButton
                            text={tr`cancel`}
                            onClick={addExistingHandleClose}
                            color="primary"
                            outline
                        /> */}
							</Col>
						</Row>
					</>
				)
			}
		/>
	);
};

export default PickAlternativesModal;

/*

*/
