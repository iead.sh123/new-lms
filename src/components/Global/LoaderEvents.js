import React from "react";

function LoaderEvents() {
  return (
    <div className="content text-center">
      <br /> <br />
      <div className="content d-flex justify-content-center align-items-center ">
        <div className="lds-facebook">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
}

export default LoaderEvents;
