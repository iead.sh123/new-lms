import React, { useState } from "react";
import styles from "./Compass.Module.scss";

import { Link } from "react-router-dom";

import { Table } from "reactstrap";
import Helper from "../Helper";

const Compass = ({ points, selected }) => {
  const [showCompass, setShowCompass] = useState(false);

  let sortedPoints = points.sort((a, b) => a.y > b.y);

  const rowsTorender = [];

  while (sortedPoints.length > 0) {
    let row = sortedPoints
      .filter((sp) => sp.y === sortedPoints[0].y)
      .sort((a, b) => a.x < b.x);
    rowsTorender.push(
      <>
        {row.map((r) => (
          <div
            dir="ltr"
            style={selected === r.id ? { backgroundColor: "#ffd9bf" } : {}}
          >
            <Link
              onClick={r.onClick}
              key={r.id}
              className={`${styles["Compass-item"]}`}
            >
              {r.icon && r.icon()}
            </Link>
          </div>
        ))}
      </>
    );
    sortedPoints = sortedPoints.filter((sp) => sp.y !== sortedPoints[0].y);
  }

  return (
    <div id="compoaseContainer">
      <div
        dir="ltr"
        className={
          showCompass ? "navigator_container_animation" : "navigator_container"
        }
      >
        {rowsTorender}
      </div>

      <button
        id="sdf"
        className="btn-compass"
        onClick={() => setShowCompass(!showCompass)}
      >
        {showCompass ? (
          <i
            className="fa fa-arrow-right arrow-down-compass"
            aria-hidden="true"
          ></i>
        ) : (
          <i
            className="fa fa-arrow-right arrow-up-compass"
            aria-hidden="true"
          ></i>
        )}
      </button>
    </div>
  );
};

export default Compass;
