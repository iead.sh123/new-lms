import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
import RSchoolInitiationButtons from "./RSchoolInitiationButtons";
import styles from "../RSchoolInitiation.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const REducationStageForm = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { tabTitle } = useParams();

  return (
    <Form className={tabTitle == "properties" ? styles.add_form : ""}>
      <Row>
        <Col xs={12}>
          <RFlex>
            <label>{tr("name")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="name"
              type="text"
              placeholder={tr`name`}
              value={SchoolInitiationData?.addEducationStage?.name}
              onChange={(event) => {
                SchoolInitiationData.handleChangeEducationStage(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <RFlex>
            <label>{tr("order")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="order"
              type="number"
              placeholder={tr`order`}
              value={SchoolInitiationData?.addEducationStage?.order}
              onChange={(event) => {
                SchoolInitiationData.handleChangeEducationStage(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>
      </Row>
      {tabTitle && <RSchoolInitiationButtons />}
    </Form>
  );
};

export default REducationStageForm;
