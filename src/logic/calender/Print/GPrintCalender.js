import RModal from 'components/Global/RComs/RModal';
import React from 'react';

const GPrintCalender = ({ handleToggle, isOpen, base64File }) => {
    return <RModal isOpen={isOpen}
        toggle={handleToggle}
        header={<h6 className="text-capitalize">Print</h6>}
        body={
            <div style={{ height: "80vh", background: "red" }}>
                <embed
                    width="100%"
                    height="100%"
                    src={`data:application/pdf;base64,${base64File}`}
                />
            </div>
        }

        size="lg" />
}


export default GPrintCalender;