import { Services } from "engine/services";
import { get, post, patch, destroy, put } from "config/api";

export const termsManagementApi = {
	getAcademicYears: (withoutGradeLevels) =>
		get(`${Services.terms.backend}aggregate/academic-years-hierarchy${withoutGradeLevels ? `?withoutGradeLevels=true` : ""}`),
	addAcademicYear: (data) => post(`${Services.terms.backend}academic-years`, data),
	editAcademicYear: ({ data, id }) => patch(`${Services.terms.backend}academic-years/${id}`, data),
	deleteAcademicYear: (id) => destroy(`${Services.terms.backend}academic-years/${id}`),

	addTerm: ({ data, academicYearId }) => post(`${Services.terms.backend}terms/academic-years/${academicYearId}`, data),
	editTerm: ({ data, id }) => patch(`${Services.terms.backend}terms/${id}`, data),
	deleteTerm: (id) => destroy(`${Services.terms.backend}terms/${id}`),
	changeTermStatus: ({ status, termId }) => post(`${Services.terms.backend}terms/${status}/${termId}`),

	addUserToOrganization: ({ userId: userId, data: data }) =>
		post(`${Services.auth_organization_management.backend}api/organization/users/store/${userId}`, data),

	// getStudentsForSpecificGrade: ({ gradeLevelId: gradeLevelId, searchText: searchText }) =>
	// 	get(
	// 		`${Services.auth_organization_management.backend}api/grade-level/getStudentsList/${gradeLevelId}${
	// 			searchText ? `?filter=${searchText}` : ""
	// 		}`
	// 	),

	getStudentsForSpecificGrade: ({ gradeLevelId: gradeLevelId, searchText: searchText, AcademicPath, page }) =>
		get(
			`${Services.auth_organization_management.backend}${
				AcademicPath
					? `api/school-management/students-for-grade-level/${gradeLevelId}${
							searchText ? `?prefix=${searchText}&page=${page}` : `?page=${page}`
					  }`
					: `api/grade-level/getStudentsList/${gradeLevelId}${searchText ? `?filter=${searchText}&page=${page}` : `?page=${page}`}`
			}`
		),

	getTeachersForOrganization: ({ userId: userId, searchText: searchText, page: page }) =>
		get(
			`${Services.auth_organization_management.backend}api/organization/usersForSpecificUserType/${userId}${
				searchText ? `?filter=${searchText}&page=${page}` : `?page=${page}`
			}`
		),

	sendNotificationToUsers: ({ data }) =>
		post(`${Services.auth_organization_management.backend}api/school-management/send-notification`, data),

	homeRoomById: ({ homeRoomId }) => get(`${Services.terms.backend}/home-rooms/${homeRoomId}`),
	addHomeRoom: ({ data, termId }) => post(`${Services.terms.backend}home-rooms/term/${termId}`, data),
	editHomeRoom: ({ data, id }) => patch(`${Services.terms.backend}home-rooms/${id}`, data),
	deleteHomeRoom: (id) => destroy(`${Services.terms.backend}home-rooms/${id}`),
	enrollUsersToHomeRoom: ({ homeRoomId, data }) => put(`${Services.terms.backend}home-rooms/${homeRoomId}/users`, data),
	unEnrollUsersInHomeRoom: ({ data, homeRoomId }) => post(`${Services.terms.backend}home-rooms/${homeRoomId}/un-enroll/users`, data),
	changeUsersFromHomeRoomToAnotherOne: ({ data, sourceHomeRoomId, targetHomeRoomId }) =>
		post(`${Services.terms.backend}home-rooms/${sourceHomeRoomId}/users/move/target/${targetHomeRoomId}`, data),
	allUsersEnrolledToHomeRoom: ({ homeRoomId, userType, orderBy }) =>
		get(`${Services.terms.backend}home-rooms/${homeRoomId}/users${userType ? `/${userType}` : ""}${orderBy ? `?orderBy=${orderBy}` : ""}`),

	getCoursesFromSchoolCatalog: ({ termId, gradeLevelId, searchText, noPagination }) =>
		get(`${Services.terms.backend}aggregate/grade-levels/${gradeLevelId}/courses/terms${searchText ? `?searchText=${searchText}` : ""}`),
};
