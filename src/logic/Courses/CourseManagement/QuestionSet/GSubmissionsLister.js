import React, { useState, useEffect, useMemo } from "react";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { Row, Col } from "reactstrap";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import { useSelector, useDispatch } from "react-redux";
import { submissionsQuestionSetAsync, removeSubmissionAsync } from "store/actions/global/questionSet.actions";
import Loader from "utils/Loader";
import RLister from "components/Global/RComs/RLister";

import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { primaryColor } from "config/constants";
import moment from "moment";
import { DATE_FORMATE } from "config/constants";
import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import REmptyData from "components/RComponents/REmptyData";

const GSubmissionsLister = () => {
	const { courseId, cohortId, categoryId, questionSetType, moduleId, studentStatus } = useParams();
	const dispatch = useDispatch();
	const history = useHistory();

	const [searchData, setSearchData] = useState("");
	const [alert, setAlert] = useState(false);

	const { allSubmissions, allSubmissionsLoading } = useSelector((state) => state.questionSetRed);
	const { user } = useSelector((state) => state.auth);

	useEffect(() => {
		if (studentStatus == "submittedStudents") {
			dispatch(submissionsQuestionSetAsync(questionSetType, moduleId, searchData));
		} else if (studentStatus == "gradedStudents") {
			dispatch(submissionsQuestionSetAsync(questionSetType, moduleId, searchData, true));
		}
	}, [moduleId]);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		dispatch(submissionsQuestionSetAsync(questionSetType, questionSetId, emptySearch !== undefined ? emptySearch : searchData));
	};

	const handleRemoveSubmission = (submissionId) => {
		const prameters = {
			submissionId,
		};
		const message = tr`Are you sure?`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const successDelete = async (prameters) => {
		dispatch(removeSubmissionAsync(questionSetType, prameters.submissionId));
	};
	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const handlePushToStartSolveQuestionSet = ({ questionSetContextId, learnerId }) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleId}/learner/${learnerId}/${questionSetType}/${questionSetContextId}/${
					studentStatus == "submittedStudents" ? "correction" : "review"
				}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleId}/learner/${learnerId}/${questionSetType}/${questionSetContextId}/${
					studentStatus == "submittedStudents" ? "correction" : "review"
				}`
			);
		}
	};

	const handlePushToAllQS = () => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/${questionSetType} `
			);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/${questionSetType}`);
		}
	};

	const records = useMemo(() => {
		return (
			allSubmissions &&
			allSubmissions?.records &&
			allSubmissions?.records.length > 0 &&
			allSubmissions?.records?.map((record) => ({
				table_name: "questionSetLister",
				id: record.id,
				details: allSubmissions?.headers?.map((header) => ({
					key: tr(header.label),
					value:
						header.label == "Submission Date"
							? moment(record[header.value]).format(DATE_FORMATE)
							: header.label == "Students"
							? renderInfo({
									name: record[header.value].full_name,
									image: Services.auth_organization_management.file + record[header.value].hash_id,
							  })
							: record[header.value],
					type: "component",
					dropdown: ["Students", "Mark"].includes(header.label) ? true : false,
					dropdownData:
						header.label == "Students"
							? [
									{
										id: 1,
										title: "alphabetically",
									},
									{ id: 2, title: "recently_graded" },
							  ]
							: header.label == "Mark"
							? [
									{
										id: 1,
										title: "order_descending",
									},
									{ id: 2, title: "order_ascending" },
							  ]
							: [],
				})),
				actions: [
					{
						name: tr("edit"),
						icon: "fa fa-edit",
						onClick: () => {},
					},
					{
						name: tr("mark_details"),
						icon: "fa fa-user",
						onClick: () => {
							handlePushToStartSolveQuestionSet({
								questionSetContextId: record.id,
								learnerId: record.student.id,
							});
						},
					},

					{
						name: tr("remove"),
						icon: iconsFa6.delete,
						color: "danger",
						onClick: () => {
							handleRemoveSubmission(record.id);
						},
					},
				],
			}))
		);
	}, [allSubmissions, allSubmissions.records]);

	return (
		<Row>
			{alert}
			<Col xs={12} md={3} className="d-flex align-items-center">
				<RFlex style={{ color: primaryColor, cursor: "pointer" }} onClick={() => handlePushToAllQS()}>
					<i class="fa-solid fa-arrow-left pt-1 pr-1"></i>
					<span>{`${tr(`back_to_all_${questionSetType}`)}`}</span>
				</RFlex>
			</Col>
			<Col xs={12} md={6}>
				<RSearchHeader
					searchLoading={allSubmissionsLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					addNew={false}
				/>
			</Col>

			{allSubmissionsLoading ? (
				<Loader />
			) : (
				<>
					{records?.length > 0 ? (
						<Col xs={12} className="mt-3">
							<RLister Records={records} />
						</Col>
					) : (
						<Col xs={12}>
							<REmptyData />
						</Col>
					)}
				</>
			)}
		</Row>
	);
};

export default GSubmissionsLister;
