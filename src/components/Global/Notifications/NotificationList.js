import React from "react";

import { viewNotificationsAsync } from "store/actions/global/notification.action";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import RNotificationGroup from "view/Notifications/RnotificationGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilRuler, faComment } from "@fortawesome/free-solid-svg-icons";
import { setNotifyMe } from "store/actions/global/notification.action";
import moment from "moment";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";

const NotificationList = ({ notificationList, notificationsLoading, total }) => {
	const history = useHistory();
	const dispatch = useDispatch();

	const { user } = useSelector((state) => state?.auth);

	const handleRemoveNotification = (notificationId) => {
		dispatch(viewNotificationsAsync(notificationId));
	};
	const moveToDetails = (notificationId) => {
		history.push(`${baseURL}/${genericPath}/notification/` + notificationId);
	};
	const setNotify = (notifyMe) => {
		dispatch(setNotifyMe(notifyMe));
	};
	const oldnots = [
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#007bff",
			title: "title1",
			description: "your freinds posted new image",
			time: "10 min ago",
			Seen: false,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faComment} />,
			iconbg: "#fd7e14",
			title: "you got two likes",
			description: "description description2",
			time: "10 min ago",
			Seen: true,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#ffc107",
			title: "title1",
			description: "your freinds posted new image",
			time: "10 min ago",
			Seen: false,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faComment} />,
			iconbg: "#20c997",
			title: "title1",
			description: "your freinds posted new image",
			time: "10 min ago",
			Seen: false,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#007bff",
			title: "you got two likes",
			description: "description description2",
			time: "10 min ago",
			Seen: true,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faComment} />,
			iconbg: "#007bff",
			title: "title1",
			description: "your freinds posted new image",
			time: "10 min ago",
			Seen: false,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#fd7e14",
			title: "you got two likes",
			description: "description description2",
			time: "10 min ago",
			Seen: true,
		},
		{
			image: "https://unsplash.com/photos/c_GmwfHBDzk/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjgzNzg2ODc3fA&force=true&w=640",
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#007bff",
			title: "you got two likes",
			description: "description description2",
			time: "10 min ago",
			Seen: true,
		},
	];
	//----------------------- Here you go
	const notifications = notificationList?.map((notification) => {
		return {
			id: notification.id,
			image: notification.notifications?.[0]?.user?.image,
			imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
			iconbg: "#007bff",
			title: notification?.description,
			description: notification?.description,
			time: moment(notification.created_at).fromNow(),
			seen: notification.seen,
		};
	});

	const notifyMe = useSelector((state) => state?.NotificationsRed?.notifyMe);
	return (
		<>
			<RNotificationGroup
				onMarkAsSeen={() => {}}
				onStopNotificationChange={(e) => {
					setNotify(e ? 0 : 1);
				}}
				allNotifications={total}
				notifyMe={notifyMe}
				group={{
					faicon: "star",
					title: "Notifications",
					description: "Notifications description",
					notifications: notifications,
				}}
				handleOnClick={moveToDetails}
				handleOnSeenClick={handleRemoveNotification}
			/>
		</>
	);
};

export default NotificationList;
