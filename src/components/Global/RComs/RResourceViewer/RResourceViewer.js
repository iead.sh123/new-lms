import React from 'react';
import { Container, Modal, ModalBody, Button, ModalFooter } from 'reactstrap';
import RFile from './RFile';
import styles from './RResourceViewer.module.scss';
import RImageViewer from '../RImageViewer/RImageViewer';

const RResourceViewer = ({ sourceUrl = '', resources = [], modalMode = false, selectedIndex = 0, onHideCallBack }) => {

    if (!resources.length)
        return <></>

    const [showModal, setShowModal] = React.useState(modalMode);
    const [currentImageIndex, setCurrentImageIndex] = React.useState(selectedIndex);

    //image errors ids
    const [imageErrors, setImageErrors] = React.useState([]);

    //reset image errors if thumnail updated
    React.useEffect(() => {
        setImageErrors([]);
    }, resources.map(rs => rs.thumbUrl))


    const pushErronusImageWithoutDuplicate = (imageId) => {
        if (imageErrors.some(id => id === imageId))
            return;
        setImageErrors([...imageErrors, imageId]);
    }

    const handleNextImage = () => {
        if (currentImageIndex === resources.length - 1)
            return;
        setCurrentImageIndex(currentImageIndex + 1)
    }

    const handlePrevImage = () => {
        if (currentImageIndex === 0)
            return;
        setCurrentImageIndex(currentImageIndex - 1)
    }

    const toggle = () => { setShowModal(!showModal); onHideCallBack?.() };


    return <>
        <div onClick={() => setShowModal(true)} className={styles['ImageContainer']}>

            {
                imageErrors.some(id => id === resources[0]?.id) ?
                 <RFile src={sourceUrl + resources[0]?.hash_id} name={resources[0]?.name} mimetype={resources[0]?.mime_type}/> : 
                <img 
                id="state1" 
                className={styles['Image']} 
                src={sourceUrl + (resources[0]?.thumbUrl ?? resources[0]?.url)} 
                onError={({ currentTarget }) => {
                    currentTarget.onerror = null; // prevents looping
                    pushErronusImageWithoutDuplicate(resources[0]?.id)
                }} />
            }

            {resources.length > 2 ?
                imageErrors.some(id => id === resources[1]?.id) ? 
                <RFile src={sourceUrl + resources[1]?.hash_id} name={resources[1]?.name} /> :
                   
                    <div id="state2" style={{width:"100%", backgroundImage: `url(${sourceUrl + (/*resources[1]?.thumbUrl ??*/ resources[1]?.hash_id)})` }} className={styles['ImageCount']}><span >+{resources.length - 2}</span> 
                    </div> 
                    : resources.length === 2 ?

                    <img id="state3"  className={styles['Image']} style={{width:"150px"}} src={sourceUrl + (/*resources[1]?.thumbUrl ??*/ resources[1]?.hash_id)} onError={({ currentTarget }) => {
                        currentTarget.onerror = null; // prevents looping
                        pushErronusImageWithoutDuplicate(resources[1]?.id)
                    }} />

                    : <></>}
        </div>
        <Modal isOpen={showModal} toggle={toggle} size="lg">
            <ModalBody style={{ height: '70vh', display: 'flex', flexDirection: 'row' }}>
                {/* <div className={styles['Arrow']}> */}
                {/* <i className="nc-icon nc-stre-left" onClick={handlePrevImage} /> */}
                {/* </div> */}
                {
                    imageErrors.some(id => id === resources[currentImageIndex]?.id) ? 
                    // <RFile 
                    //         src={sourceUrl + resources[currentImageIndex]?.hash_id} 
                    //         mimetype={resources[currentImageIndex]?.mime_type}
                    //         name={resources[currentImageIndex]?.name} /> :
                    //     <div>
                    //         <img src={sourceUrl + resources[currentImageIndex]?.url} className={styles['ModalImage']}
                    //             onError={({ currentTarget }) => {
                    //                 currentTarget.onerror = null; // prevents looping
                    //                 pushErronusImageWithoutDuplicate(resources[currentImageIndex]?.id)
                    //             }} />
                    <RImageViewer
                    mimeType={resources[currentImageIndex]?.mime_type}
                    url={resources[currentImageIndex]?.hash_id}
                    imageName={resources[currentImageIndex]?.name}
                    width={"fit-content"}
                    height={"400px"}
                  />:
                      <div>
                          <img src={sourceUrl + resources[currentImageIndex]?.url} className={styles['ModalImage']}
                              onError={({ currentTarget }) => {
                                  currentTarget.onerror = null; // prevents looping
                                  pushErronusImageWithoutDuplicate(resources[currentImageIndex]?.id)
                              }} />

                        </div>
                }
                {/* <div className={styles['Arrow']}> */}
                {/* <i className="nc-icon nc-stre-right " onClick={handleNextImage} /> */}
                {/* </div> */}
            </ModalBody>
            <ModalFooter style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button disabled={currentImageIndex === 0} onClick={handlePrevImage}>
                    &lt;
                </Button>
                <Button disabled={currentImageIndex === resources.length - 1} onClick={handleNextImage}>
                    &gt;
                </Button>
            </ModalFooter>
        </Modal>
    </>
}


export default RResourceViewer;