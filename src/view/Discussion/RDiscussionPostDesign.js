import React, { useContext } from "react";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { relativeDate } from "utils/dateUtil";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";
import {
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	UncontrolledPopover,
	PopoverBody,
	Row,
	Col,
	FormGroup,
	Input,
	Collapse,
} from "reactstrap";
import RActionsOnPosts from "./RActionsOnPosts";
import RSeeMoreText from "./RSeeMoreText";
import RAttachments from "./RAttachments";
import styles from "./RDiscussion.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useSelector } from "react-redux";
import RButton from "components/Global/RComs/RButton";
import RFileUploader from "components/Global/RComs/RFileUploader";
import iconsFa6 from "variables/iconsFa6";

const RDiscussionPostDesign = ({
	post,
	mB,
	ml,
	community,
	typeContent,
	specificMandatoryPost,
	indRec,
	seeMoreComment,
	postId,
	postType,
}) => {
	const history = useHistory();
	const { cohortId, categoryName, sectionId } = useParams();

	const DiscussionData = useContext(categoryName ? DiscussionFilterContext : DiscussionContext);
	const { user } = useSelector((state) => state.auth);
	return (
		<React.Fragment>
			<section key={post.id} className={`${ml} ${mB} mr-4`}>
				{/* post design */}
				<section className={`mb-2 ${typeContent == "post" ? styles.post_section : ""}`}>
					{/* header post */}
					<RFlex
						styleProps={{
							width: "100%",
							alignItems: "center",
							justifyContent: "space-between",
						}}
						className="mb-2"
					>
						{/* title header post */}
						<RFlex>
							<img src={`${Services.discussion.file}${post?.user?.profile_image}`} className={styles.post_img + " cursor-pointer"} />
							<div className={styles.post_name}>
								<p
									className={`font-weight-bold cursor-pointer  ${styles.paragraph}`}
									onClick={() => {
										if (cohortId) {
											history.push(`${baseURL}/${genericPath}/section/${sectionId}/specific-posts/${post.user_id}/${cohortId}`);
										} else {
											history.push(`${baseURL}/${genericPath}/specific-posts/${post.user_id}`);
										}
									}}
								>
									{`${post?.user?.full_name}`}

									{post?.cohort_name && `${` | ${post?.cohort_name}`}`}
								</p>
								{post?.user?.type && <p className={`font-weight-normal ${styles.paragraph}`}>{post?.user?.type}</p>}
							</div>
						</RFlex>

						{/* actions on header */}
						<div className={styles.post_name}>
							{!categoryName && user && (
								<>
									{post?.user_id == user?.id ? (
										<UncontrolledDropdown direction="down" style={{ display: "flex", justifyContent: "end" }}>
											<DropdownToggle className={" btn btn-link"} id="dropdown-basic">
												<i className="fa fa-ellipsis-v" />
											</DropdownToggle>
											<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
												<DropdownItem
													key={1}
													onClick={() => {
														DiscussionData.setPostId(post.id);
														if (typeContent == "post") {
															DiscussionData.handleOpenAddPost();
														} else {
															DiscussionData.handleEditableCommentText(post.id);
														}
													}}
												>
													<RFlex styleProps={{ alignItems: "center" }}>
														<i className="fa fa-edit" aria-hidden="true"></i>
														edit
													</RFlex>
												</DropdownItem>
												<DropdownItem divider />
												<DropdownItem
													key={2}
													onClick={() =>
														typeContent == "post" ? DiscussionData.handleDeleteItem(post.id) : DiscussionData.handleRemoveComment(post.id)
													}
												>
													<RFlex styleProps={{ alignItems: "center" }}>
														<i className={iconsFa6.delete} aria-hidden="true"></i>
														delete
													</RFlex>
												</DropdownItem>
											</DropdownMenu>
										</UncontrolledDropdown>
									) : (
										<i
											className="fa fa-flag d-flex justify-content-end cursor-pointer"
											aria-hidden="true"
											onClick={() => {
												DiscussionData.handleOpenAddReport();
												DiscussionData.setPostId(post.id);
												DiscussionData.setContentId(post.content_id);
											}}
										/>
									)}
								</>
							)}
							{post?.published_at && <p className="text-muted"> {relativeDate(post?.published_at)}</p>}
						</div>
					</RFlex>

					{/* title on post */}
					{post?.title && (
						<RFlex>
							<p className="m-0" style={{ fontWeight: 600 }}>
								{post?.title}
							</p>
						</RFlex>
					)}

					{/* see more on post */}
					<RSeeMoreText
						text={post?.text}
						postId={post?.id}
						contentId={post?.content_id}
						replyId={post?.reply_id}
						specificMandatoryPost={specificMandatoryPost}
						typeContent={typeContent}
						editInput={DiscussionData.editableCommentText === "text" + post.id}
					/>

					{/* attachments on post */}

					{((post.attachments && post.attachments?.length > 0 && !specificMandatoryPost) ||
						(DiscussionData.specificPost && post.attachments?.length > 0)) && (
						<RAttachments attachments={post.attachments} specificMandatoryPost={specificMandatoryPost} />
					)}

					{post.attachments && post.attachments?.length > 0 && specificMandatoryPost && !DiscussionData.specificPost && (
						<RFlex styleProps={{ cursor: "pointer", gap: 20 }}>
							<i className={`fa fa-paperclip ${styles.attach_icon}`} aria-hidden="true" style={{ color: primaryColor }}></i>

							<p
								style={{ color: primaryColor }}
								onClick={() => {
									DiscussionData.handleOpenSeeTheContentOfThePost();
									DiscussionData.setPostId(post.id);
									DiscussionData.setSpecificPost(true);
								}}
							>
								{post.attachments?.length} {tr`media included`}
							</p>
						</RFlex>
					)}
				</section>

				{/* action button when filter on posts */}
				{categoryName && ["Pending", "Reported", "Rejected"].includes(categoryName) && (
					<RActionsOnPosts contentId={post.parent_content_id} />
				)}

				{!categoryName && (
					<RFlex
						styleProps={{
							width: "100%",
							justifyContent: "space-between",
							padding: "0px 20px",
						}}
					>
						{/* like and comment */}
						<RFlex styleProps={{ gap: 20 }}>
							<RFlex styleProps={{ alignItems: "center" }} id={`userPopover-${post.id}`}>
								<div
									className={styles.like_icon}
									onClick={() =>
										DiscussionData.handleAddLike({
											content_id: post.content_id,
											type: "like",
											post_id: typeContent == "post" ? post.id : null,
											comment_id: typeContent == "comment" ? post.id : null,
											typeContent: typeContent,
											postType: postType,
										})
									}
								>
									<i
										className={
											DiscussionData?.addLikeLoading ? `fa fa-refresh fa-spin cursor-pointer` : `fa fa-thumbs-o-up fa-lg cursor-pointer`
										}
										aria-hidden="true"
										style={{
											color: post?.is_liked ? primaryColor : "",
										}}
									/>

									<p className="m-0" style={{ color: post?.is_liked ? primaryColor : "" }}>
										{post?.nb_likes ?? 0}
									</p>
								</div>
								{post?.likes?.length > 0 && (
									<UncontrolledPopover placement="bottom" trigger="hover" target={`userPopover-${post.id}`}>
										<PopoverBody>
											{post.likes.map((like) => (
												<h6 key={like.id}>{like?.user?.full_name}</h6>
											))}
											{post.nb_likes > 4 && (
												<h6
													className="cursor-pointer"
													onClick={() => {
														DiscussionData.handleOpenSeeAllPeople();
														DiscussionData.setContentId(post.content_id);
													}}
												>
													+{post.nb_likes} {tr`others`}
												</h6>
											)}
										</PopoverBody>
									</UncontrolledPopover>
								)}
							</RFlex>
							{indRec !== 2 ? (
								<RFlex
									styleProps={{ alignItems: "center", cursor: "pointer" }}
									onClick={() => {
										typeContent == "post"
											? DiscussionData.handleCommentCollapsesToggle(post.id)
											: DiscussionData.handleReplayCollapsesToggle(post.id);
									}}
								>
									<i className={typeContent == "post" ? "fa fa-comments-o  fa-lg" : "fa fa-reply fa-lg"} aria-hidden="true" />
									<p className="m-0">{post?.nb_comments ?? 0}</p>
								</RFlex>
							) : (
								""
							)}
						</RFlex>

						{/* count replies and comments in mandatory posts */}
						{cohortId && !community && (
							<React.Fragment>
								{post?.is_admin ? (
									<React.Fragment>
										<RFlex styleProps={{ alignItems: "center", cursor: "pointer" }} id={`userPopover-users-${post.id}`}>
											<i class={`fa fa-user fa-lg `} aria-hidden="true" />
											<p className="m-0">
												{post?.users_who_does_not_apply_conditions?.length}/{post.nb_users_who_does_not_apply_conditions}
											</p>
										</RFlex>
										{post?.users_who_does_not_apply_conditions?.length > 0 && (
											<UncontrolledPopover placement="bottom" trigger="hover" target={`userPopover-users-${post.id}`}>
												<PopoverBody>
													{post.users_who_does_not_apply_conditions.map((user) => (
														<h6 key={user.user_id}>{user?.full_name}</h6>
													))}
													{post.nb_users_who_does_not_apply_conditions > 5 && (
														<h6
															className="cursor-pointer"
															onClick={() => {
																DiscussionData.handleOpenSeeAllUsersInMandatory();
																DiscussionData.setPostId(post.id);
															}}
														>
															+{post.nb_users_who_does_not_apply_conditions} {tr`others did not complete yet`}
														</h6>
													)}
												</PopoverBody>
											</UncontrolledPopover>
										)}
									</React.Fragment>
								) : (
									<RFlex styleProps={{ gap: 20 }}>
										<RFlex styleProps={{ alignItems: "center", color: "#19af00" }}>
											<i class={`fa fa-comments-o fa-lg `} aria-hidden="true" />
											<p className="m-0">{post?.my_comments_count}</p>
										</RFlex>
										<RFlex styleProps={{ alignItems: "center", color: "#DD0000" }}>
											<i className={`fa fa-reply fa-lg `} aria-hidden="true" />

											<p className="m-0">
												{post?.my_replies_count}/{post?.mandatory_replies}
											</p>
										</RFlex>
									</RFlex>
								)}
							</React.Fragment>
						)}
					</RFlex>
				)}
			</section>

			<Collapse
				isOpen={
					typeContent == "post"
						? DiscussionData.openedCollapses === "collapse" + post.id
						: DiscussionData.openedReplayCollapses === "collapse" + post.id
				}
			>
				<FormGroup className={ml}>
					<div style={{ position: "relative" }}>
						<Input
							name="text"
							required
							type="textarea"
							placeholder={indRec == 1 ? tr`write a replay` : tr`write a comment`}
							onChange={(event) => {
								DiscussionData.handleChangeCreateComment(event.target.name, event.target.value);
								DiscussionData.handleChangeCreateComment("reply_id", post?.content_id);
								DiscussionData.handleChangeCreateComment("post_id", typeContent == "post" ? post?.id : null);

								DiscussionData.handleChangeCreateComment("comment_id", typeContent == "comment" ? post?.id : null);
								DiscussionData.handleChangeCreateComment("for_community", cohortId ? false : true);
								DiscussionData.handleChangeCreateComment("typeContent", typeContent);
							}}
							style={{ width: "100%" }}
						/>

						<RButton
							text={indRec ? tr`replay` : tr`comment`}
							color="primary"
							className={styles.submit_button}
							onClick={() => DiscussionData.handleSaveComment()}
							loading={DiscussionData.addCommentLoading}
							disabled={DiscussionData.addCommentLoading ? true : false}
						/>
					</div>
				</FormGroup>

				{/* {post?.nb_comments > post?.comments.length && ( */}
				{post?.last_comment_id !== null && (
					<h6
						onClick={() => {
							if (DiscussionData.specificPost) {
								DiscussionData.handleLoadPreviousComments(
									post.content_id,
									post.last_comment_id,
									typeContent,
									indRec ? "replay" : "comment"
								);
							} else {
								if (DiscussionData.specificContentLoad) {
									DiscussionData.handleLoadPreviousComments(
										post.content_id,
										post.last_comment_id,
										typeContent,
										indRec ? "replay" : "comment"
									);
								} else {
									DiscussionData.handleOpenSeeMoreContent();
									DiscussionData.setSpecificContent(true);
									DiscussionData.setCommentId(post?.id);
									DiscussionData.setPostId(post?.id);
									DiscussionData.setLastCommentId(post.last_comment_id);
									DiscussionData.setContentId(post.content_id);
									DiscussionData.setTypeContent(typeContent);
								}
							}
						}}
						className={`mb-4 mt-4 ${ml}`}
						style={{
							textDecoration: "underline",
							color: primaryColor,
							cursor: "pointer",
						}}
					>
						{tr`see more`}
					</h6>
				)}

				{post.comments &&
					post.comments?.length > 0 &&
					post.comments.map((comment) => (
						<div key={comment.id}>
							<RDiscussionPostDesign
								post={comment}
								mB={"mb-4"}
								ml={indRec == 1 ? "ml-4" : "ml-0"}
								community={community}
								typeContent={"comment"}
								specificMandatoryPost={specificMandatoryPost}
								indRec={indRec + 1}
								seeMoreComment={false}
								postType={postType}
							/>
						</div>
					))}
			</Collapse>
		</React.Fragment>
	);
};

export default RDiscussionPostDesign;
