
import React, { useState }  from "react";
import './RCollaborationCard.css';
import RCollaborationCard from "./RCollaborationCard";
import RLiveAction from "./RLiveAction";

function RCollaborationCardTest({}){
  
    const CardItems=[
                      { title : { text: 'Arabic Letters'},subtitle : { text: 'Unit Plan'},Author:{image:'tea.png', text: 'Canvas'},
                      onClick:()=>{}, UpLeftIcon:"fa fa-angle-right", 
                      bottomBorderColor:""},
                    
                      {  Description:"8 Content Types Shared",Author:{image:'tea.png', text: 'Canvas'},
                      from:"", onClick:()=>{}, UpLeftIcon:"fa fa-angle-right", UpLeftText:"", 
                      bottomBorderColor:""},
                     
                      { title : { text: 'Course Resources'},
                      from:"", onClick:()=>{}, UpLeftText:"All:86", 
                      Actions: [{title: "Collaborate", icon: "fa fa-share", color:"blue" , onClick:"",withBorder:false}], bottomBorderColor:""},
                     
                      { title : { text: 'Arabic Letters Alfa'}, Description:"",Author:{image:'tea.png', text: 'Creator:Nuha Farhat'},
                      from:"", onClick:()=>{},
                      Actions: [{title: "View", icon: "	fa fa-eye", color:"gray" , onClick:"",withBorder:true},
                                {title: "Approve", icon: "fa fa-check", color:"green" , onClick:"",withBorder:true} ,
                                {title: "Reject", icon: "fa fa-close", color:"red" , onClick:"",withBorder:true}], 
                     bottomBorderColor:"",additionalComponent:"additionalComponent"},
    ]


 

    return(
        <>
      {  CardItems.map(i=>
        <RCollaborationCard
         title ={i.title?i.title:null} 
         subtitle ={i.subtitle?i.subtitle:null} 
         Description={i.Description ?i.Description:null} 
         Author={i.Author ?i.Author:null} 
         onClick={i.onClick ?i.onClick:null} 
         UpLeftIcon={i.UpLeftIcon ?i.UpLeftIcon:null} 
         UpLeftText={i.UpLeftText ?i.UpLeftText:null} 
         Actions={i.Actions ?i.Actions:null} 
         bottomBorderColor ={i.bottomBorderColor ?i.bottomBorderColor:null} 
         additionalComponent={i.additionalComponent ?i.additionalComponent:null} 
         />
        )}
        
        </>
    )
}
export default RCollaborationCardTest;
/* 
title {image (png) , text} 

subtitle {image (png) , text} 
Description = 8 Content Types Shared
Author={text,image} 

From 
onClick */


