import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";
import GClients from "./GClients";
import GProviders from "./GProviders";

const GCollaborationArea = () => {
	return (
		<RFlex styleProps={{ gap: 15, flexDirection: "column" }}>
			<GProviders />
			<GClients />
		</RFlex>
	);
};

export default GCollaborationArea;
