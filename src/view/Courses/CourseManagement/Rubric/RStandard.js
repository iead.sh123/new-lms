import React, { useContext } from "react";
import { Input, FormGroup, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import RButton from "components/Global/RComs/RButton";
import RRates from "./RRates";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RStandard = ({ categoryData, handleCloseStandardModal }) => {
	const RubricData = useContext(RubricContext);
	const pathVal = RubricData.values.categories[categoryData.categoryIndex]?.standards[categoryData.standardIndex];

	const pathErr = RubricData?.errors?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];
	const pathTouch = RubricData?.touched?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];

	return (
		<RFlex className={styles.rubric__editor__standards}>
			<span className={styles.rubric__editor__standard__title}>{tr`create_new_standard`}</span>
			<FormGroup className="mb-0">
				<label>{tr("standard_title")}</label>
				<Input
					type="text"
					name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.subject`}
					value={pathVal?.["subject"]}
					onChange={RubricData.handleChange}
					placeholder={tr`standard_title`}
					className={pathErr?.["subject"] ? "input__error" : ""}
				/>
				<FormText color="danger">{pathErr?.["subject"]}</FormText>
			</FormGroup>
			{RubricData.values.pointed && (
				<FormGroup className="mb-0">
					<label>{tr("max_point")}</label>
					<Input
						type="number"
						name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.points`}
						value={pathVal?.["points"]}
						onChange={RubricData.handleChange}
						placeholder={tr`max_point`}
						className={pathErr?.["points"] ? "input__error" : ""}
					/>
					<FormText color="danger">{pathErr?.["points"]}</FormText>
				</FormGroup>
			)}
			<FormGroup className="mb-0">
				<label>{tr("description")}</label>
				<Input
					type="textarea"
					name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.description`}
					value={pathVal?.["description"]}
					onChange={RubricData.handleChange}
					placeholder={tr`description`}
				/>
			</FormGroup>
			<RRates categoryData={categoryData} />
			<RFlex styleProps={{ justifyContent: "flex-end" }}>
				<RButton text={tr`save`} color="primary" onClick={handleCloseStandardModal} />
				<RButton text={tr`cancel`} color="link" onClick={handleCloseStandardModal} />
			</RFlex>
		</RFlex>
	);
};

export default RStandard;
