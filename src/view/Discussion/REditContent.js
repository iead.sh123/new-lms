import React, { useContext } from "react";
import { Row, Col, Form, Label, FormGroup, Input, FormText } from "reactstrap";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperclip } from "@fortawesome/free-solid-svg-icons";
import { useParams } from "react-router-dom";
import RFileUploader from "components/Global/RComs/RFileUploader";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import Loader from "utils/Loader";
import styles from "./RDiscussion.module.scss";
import style from "./RDiscussion.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { Services } from "engine/services";

const REditContent = () => {
  const DiscussionData = useContext(DiscussionFilterContext);
  const { cohortId } = useParams();
  return (
    <AppModal
      show={DiscussionData.editPost}
      parentHandleClose={DiscussionData.handleCloseEditContent}
      header={tr`edit_content`}
      size={"xl"}
      headerSort={
        <Form>
          {DiscussionData.editPostLoading ? (
            <Loader />
          ) : (
            <Row className="mb-3">
              <Col xs={12} className="mb-2">
                <RFlex>
                  <Label className="text-muted">{tr`Item name`}</Label>
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    className="text-muted"
                  />
                </RFlex>
                <FormGroup>
                  <Input
                    name="title"
                    required
                    type="text"
                    placeholder={tr`what is this post about ?`}
                    value={DiscussionData.ePost.title}
                    onChange={(event) =>
                      DiscussionData.handleChangeEditContent(
                        event.target.name,
                        event.target.value
                      )
                    }
                  />
                </FormGroup>
              </Col>

              <Col xs={12} className="mb-2">
                <RFlex>
                  <Label className="text-muted">{tr`content`}</Label>
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    className="text-muted"
                  />
                </RFlex>
                <FormGroup>
                  <Input
                    name="text"
                    required
                    type="textarea"
                    placeholder={tr`describe or ask something`}
                    value={DiscussionData.ePost.text}
                    onChange={(event) =>
                      DiscussionData.handleChangeEditContent(
                        event.target.name,
                        event.target.value
                      )
                    }
                  />
                </FormGroup>
              </Col>

              <Col xs={12} className="mb-2">
                <RFlex>
                  <FontAwesomeIcon icon={faPaperclip} />
                  <label>{tr("attachments")}</label>
                </RFlex>
                <div className={style.attachment}>
                  <RFileUploader
                    parentCallback={(event) =>
                      DiscussionData.handleChangeEditContent(
                        "attachments",
                        event
                      )
                    }
                    value={
                      DiscussionData.ePost.attachments &&
                      DiscussionData.ePost?.attachments
                        .filter((val) => !val.url)
                        ?.map((attach) => {
                          return {
                            ...attach,
                            hash_id: Services.discussion.file + attach.hash_id,
                          };
                        })
                    }
                    singleFile={false}
                  />
                </div>
              </Col>

              <RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
                <RButton
                  text={tr`save`}
                  color="primary"
                  onClick={DiscussionData.handleEditContent}
                  loading={DiscussionData.saveEditToContent}
                  disabled={DiscussionData.saveEditToContent ? true : false}
                />
              </RFlex>
            </Row>
          )}
        </Form>
      }
    />
  );
};

export default REditContent;
