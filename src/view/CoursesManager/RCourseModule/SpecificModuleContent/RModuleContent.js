import React, { useEffect, useContext, useRef } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { primaryColor, dangerColor } from "config/constants";
import { lightBlueColor } from "config/constants";
import { warningColor } from "config/constants";
import { successColor } from "config/constants";
import { relativeDate } from "utils/dateUtil";
import { Row, Col } from "reactstrap";
import RListLessonContent from "../ListLessonContent/RListLessonContent";
import QuestionSetContent from "../ListModuleContent/ModuleContentComponents/QuestionSetComponents/QuestionSetContent";
import RLessonContent from "../RLessonContent";
import QuestionSet from "../ListModuleContent/ModuleContentComponents/QuestionSetComponents/QuestionSet";
import styles from "../../CoursesManager.module.scss";
import moment from "moment";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RModuleContent = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	const targetRef = useRef(null);

	useEffect(() => {
		// if (+CourseModuleData.moduleContentIdParams == content.id) {
		// 	if (targetRef?.current !== null) {
		// 		targetRef?.current?.scrollIntoViewIfNeeded(true, {
		// 			block: "center",
		// 			behavior: "smooth",
		// 			inline: "nearest",
		// 		});
		// 	}
		// }
		if (+CourseModuleData.moduleContentIdParams == content.id) {
			if (targetRef?.current !== null) {
				targetRef?.current?.scrollIntoView(true, {
					block: "center",
					behavior: "smooth",
					inline: "nearest",
				});
			}
		}
	}, [CourseModuleData.moduleContentIdParams]);

	const dateToCheck = new Date(content?.student_status?.date);

	// Get the current date and time
	const currentDate = new Date();

	// Calculate the difference in milliseconds
	const timeDifference = currentDate - dateToCheck;

	// Convert milliseconds to hours
	const hoursDifference = timeDifference / (1000 * 60 * 60);

	// Check if the date is within the last 24 hours
	const isWithinLast24Hours = hoursDifference < 24;

	const scormContent = content && content?.type && content?.type?.toLowerCase() == "scorm";
	return (
		<React.Fragment>
			<section className={styles.lesson_editor_section}>
				<Row
					key={content.id}
					onMouseEnter={() => CourseModuleData.showActionsOnHover(content.id)}
					onMouseLeave={() => CourseModuleData.showActionsOnHover(content.id)}
					onClick={() => {
						scormContent
							? CourseModuleData.handleOpenScormToViewable(content.content_id)
							: CourseModuleData.collapsesContentToggle(content.id);
						CourseModuleData.handlePushToAnotherRoute({
							ModuleId: CourseModuleData.specificModule.id,
							ModuleContentID: content.id,
						});
					}}
				>
					<Col
						xs={12}
						sm={9}
						className={styles.col_module_content}
						style={{
							padding: "12px 24px",
							background: CourseModuleData.openedContentCollapses == "collapse" + content.id ? lightBlueColor : "",
							color: CourseModuleData.openedContentCollapses == "collapse" + content.id ? primaryColor : "",
						}}
					>
						{!content.is_published && !content.newData && (
							<RFlex style={{ color: warningColor }}>
								<i className="fa fa-exclamation-triangle" />
							</RFlex>
						)}
						{!content.hasBehaviour && !CourseModuleData.studentView && !content.newData ? <div className={styles.has__behavior}></div> : ""}
						{content?.type?.toLowerCase() == "lesson" ? (
							<i className="fa fa-play-circle fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "polls" ? (
							<i className="fa-solid fa-square-poll-vertical fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "surveys" ? (
							<i className="fas fa-clipboard-list fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "assignments" ? (
							<i className="fa fa-pencil-square fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "quizzes" ? (
							<i className="fa-solid fa-list-check fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "exams" ? (
							<i className="fa fa-question-circle fa-lg pt-1" aria-hidden="true" />
						) : content?.type?.toLowerCase() == "projects" ? (
							<i className="fa fa-eye fa-lg pt-1" aria-hidden="true" />
						) : (
							""
						)}
						<span>{content.title ?? content.subject}</span>
						{CourseModuleData.studentView ? (
							<>
								{content?.student_status?.isSubmitted && (
									<span style={{ color: successColor }}>
										<i className="fa fa-check" />
										{tr`submitted`}
									</span>
								)}
								{!content?.student_status?.isSubmitted && isWithinLast24Hours && (
									<span style={{ color: dangerColor }}>
										{relativeDate(content?.student_status?.date)}&nbsp;
										{tr`to_overdue`}
									</span>
								)}

								{/* {!content?.student_status?.isSubmitted &&
								new Date(content?.student_status?.date) < new Date() && (
									<span style={{ color: dangerColor }}>
										{tr`overdue`}&nbsp;
										{relativeDate(content?.student_status?.date)}
									</span>
								)} */}

								{content?.student_status?.isReviewed ? (
									<span style={{ color: dangerColor }}>{content?.student_status?.percentageMark}</span>
								) : (
									""
								)}
							</>
						) : (
							<>
								{!content?.student_status?.isSubmitted ||
								!isWithinLast24Hours ||
								CourseModuleData.user.type !== "student" ||
								CourseModuleData.user.type !== "learner" ||
								content.type.toLowerCase() != "lesson" ||
								!content.newData ? (
									""
								) : (
									<span style={{ color: dangerColor }}>{tr`not_submitted_yet`}</span>
								)}
							</>
						)}
						<span className="text-muted">
							{content.lesson_date && moment(content.lesson_date).format("MM-DD-YYYY")}
							{content.lesson_time && ` - ${content.lesson_time}`}
						</span>
						{!CourseModuleData.studentView && !content.newData && (
							<span
								style={{
									color: content.is_published ? warningColor : successColor,
									textDecoration: "underline",
								}}
								onClick={(event) => {
									event.stopPropagation();
									CourseModuleData.handlePublishAndUnPublishModuleContent(
										CourseModuleData.specificModule.id,
										content.id,
										content.is_published ? false : true
									);
								}}
							>
								{content.is_published ? "unpublish" : "publish"}
							</span>
						)}
						{CourseModuleData.isHovered === "hover" + content.id &&
							content.type.toLowerCase() == "lesson" &&
							!CourseModuleData.studentView && (
								<>
									<i
										className={iconsFa6.pen + " pt-1"}
										style={{ color: primaryColor }}
										onClick={(event) => {
											event.stopPropagation();

											CourseModuleData.setLessonId(content.content_id ?? content.id);
											CourseModuleData.handleOpenLessonModal();
										}}
									/>
								</>
							)}
					</Col>

					<Col
						xs={12}
						sm={3}
						className="d-flex justify-content-end align-items-center"
						style={{
							background: CourseModuleData.openedContentCollapses == "collapse" + content.id ? lightBlueColor : "",
						}}
					>
						<RFlex>
							{CourseModuleData.isHovered === "hover" + content.id && !CourseModuleData.studentView && (
								<>
									<i
										className={iconsFa6.delete}
										style={{ color: dangerColor }}
										onClick={(event) => {
											event.stopPropagation();
											CourseModuleData.handleDelete(CourseModuleData.specificModule.id, content.id, null, null, content.newData);
										}}
									/>
								</>
							)}
							<i
								className={
									CourseModuleData.openedContentCollapses === "collapse" + content.id
										? "fa fa-chevron-down cursor-pointer"
										: "fa fa-chevron-right cursor-pointer"
								}
								style={{ color: primaryColor }}
							/>
						</RFlex>
					</Col>
				</Row>
				{!scormContent && (
					<Row>
						{CourseModuleData.openedContentCollapses == "collapse" + content.id && (
							<>
								{/* show lesson content inside module content from type [LESSON] */}
								{content?.type?.toLowerCase() == "lesson" && !CourseModuleData.studentView && <RListLessonContent content={content} />}
								{/* to show add options question set inside any module content from type QS and contain newData */}
								{/* {CourseModuleData.moduleContentType !== "LESSON" && */}
								{content?.type?.toLowerCase() !== "lesson" && content.newData && <QuestionSet type={content?.type} />}
								{content?.type?.toLowerCase() !== "lesson" && !content.newData && <QuestionSetContent content={content} />}
							</>
						)}
					</Row>
				)}
			</section>

			{/* start lesson content*/}
			<div ref={targetRef}>
				{CourseModuleData.openedContentCollapses == "collapse" + content.id &&
					content?.contents &&
					content?.contents.length > 0 &&
					content?.contents?.map((lesson_content) => <RLessonContent lessonContent={lesson_content} content={content} viewMode={false} />)}
			</div>
			{/* end lesson content*/}
		</React.Fragment>
	);
};

export default RModuleContent;
