import { React, useState } from "react";
import RCourseCardTest from "../Collaboration/RCourseCardTest";
import RCollaborationCardTest from "../Collaboration/RCollaborationCardTest";
import RSharedContentCardTest from "../Collaboration/RSharedContentCardTest";
import RTagViewerTest from "../Collaboration/RTagViewerTest";
import RAdvancedLister from "../RAdvancedLister";
import { Services } from "engine/services";
import Helper from "../Helper";
import { get } from "config/api";
import RCollaborationCard from "../Collaboration/RCollaborationCard";
import RLiveAction from "../Collaboration/RLiveAction";

export const CollaborationTest = () => {
  const [records, setRecords] = useState([]);
  const getDataFromBackend = async (specific_url) => {
    const url =
      Services.collaboration.backend + `api/organization/sharable-contents`;
    Helper.cl(url, "co url");
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };

  const setData = (response) => {
    //return ;
    setRecords(
      response?.data?.data?.data?.map((r, index) => {
        Helper.cl(r, "col r");

        return {
          title: r.name,
          allTitle: r.name,
          id: r.id,
          Titles: [r.name],
          table_name: `GroupManagement`,
          details: [
            {
              icon: "nc-icon nc-check-2",
              key: `name`,
              value: "hello",
            },

            {
              icon: "nc-icon nc-check-2",
              key: `sdfg`,
              value: "hello",
            },
            {
              icon: "nc-icon nc-check-2",
              key: `gdsfg`,
              value: "hello",
            },
          ],
          actions: [],
        };
      })
    );
  };

  const [loading, setLoading] = useState(false);
  return (
    <RLiveAction
      title="View"
      icon="fa fa-eye"
      color="gray"
      text="text"
      // color="red"
      loading={loading}
      type="checkbox"
      onClick={() => {
        setLoading(!loading);
      }}
    />
  );
  return (
    <div>
      {/* <RAdvancedLister
       hideTableHeader={false}
       colorEveryOtherRow={false}
       //firstCellImageProperty={"image"}
       getDataFromBackend={getDataFromBackend}
       setData={setData}
       records={records}
       getDataObject={(response) => response.data?.data?.data}
       characterCount={15}
       marginT={"mt-3"}
       marginB={"mb-2"}
       align={"left"}
       SpecialCard={RCollaborationCard}
       showListerMode={"cardLister"}
/> */}

      <hr />
      <RCourseCardTest />
      <RCollaborationCardTest />
      <RSharedContentCardTest />
      <RTagViewerTest />
    </div>
  );
};
