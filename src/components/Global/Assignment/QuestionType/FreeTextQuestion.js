import React, { useEffect, useRef, useState } from "react";
import { TYPES } from "../assignmentReducer";
// import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from 'ckeditor5-build-classic-mathtype';

import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
const FreeTextQuestion = ({
  answers,
  question,
  groupId,
  dispatch,
  assignmentReadMode,
  mode,
  student,
}) => {
  const [textInput, setTextInput] = useState("");

  useEffect(() => {
    if (assignmentReadMode && mode !== "multi") {
      if (question.notSolved === 1) {
        setTextInput("");
        return;
      }
      setTextInput(question.student_answer);
      return;
    }
    if (assignmentReadMode && mode === "multi") {
      if (student.notSolved === 1 || student.notSolved === null) {
        setTextInput("");
        return;
      }
      setTextInput(student.student_answer);
      return;
    }

    setTextInput("");
    if (answers && answers.length > 0) {
      let answer = answers.filter(
        (answer) => answer.QuestionId === question.id
      )[0];
      if (answer !== undefined) setTextInput(answer.FreeAnswer);
    }
  }, [question.id, assignmentReadMode]);

  const changeAnswerHandler = () => {
    dispatch({
      type: TYPES.SAVE_ANSWER,
      payload: {
        QuestionId: question.id,
        GroupId: groupId,
        Choices: [],
        FreeAnswer: textInput,
      },
    });
  };
  // return <>ok</>;
  return (
    <CKEditor
      editor={ClassicEditor}
      config={{
        toolbar: {
          items: [
            "heading",
            "MathType",
            "ChemType",
            "|",
            "bold",
            "italic",
            "link",
            "bulletedList",
            "numberedList",
            "imageUpload",
            "mediaEmbed",
            "insertTable",
            "blockQuote",
            "undo",
            "redo",
          ],
        },
      }}
      data={textInput}
      onChange={(event, editor) => {
        const editorData = editor.getData();
        setTextInput(editorData);
      }}
      onBlur={(event, editor) => {
        changeAnswerHandler();
      }}
      disabled={assignmentReadMode}
    />
  );
};

export default FreeTextQuestion;
