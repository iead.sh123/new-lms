import GenericUIAddEdit from "./GenericUIAddEdit";
import React, { useState } from "react";
import UsersAddEdit from "./UsersManagment/UsersAddEdit";
import GenericUIRelations from "./GenericUIManyToMany/GenericUIRelations";
import { useParams } from "react-router-dom";
import Helper from "../RComs/Helper";

const GenericAddContainer = ({
  prop_entity_specific_name,
  preSetValues,
  onSubmit,
}) => {
  const { entity_specific_name, tabTitle } = useParams();
  const name = prop_entity_specific_name
    ? prop_entity_specific_name
    : entity_specific_name;

  const user_table_names = [
    "students",
    "parents",
    "teachers",
    "seniorteachers",
    "school_advisors",
    "principals",
    "employees",
  ];

  return (
    <div className="content">
      {user_table_names.includes(name) ? (
        <>
          <UsersAddEdit
            prop_entity_specific_name={prop_entity_specific_name}
            preSetValues={preSetValues}
          />
        </>
      ) : (
        <>
          <GenericUIAddEdit
            prop_entity_specific_name={prop_entity_specific_name}
            preSetValues={preSetValues}
            onSubmit={onSubmit}
          />
        </>
      )}
    </div>
  );
};
export default GenericAddContainer;
