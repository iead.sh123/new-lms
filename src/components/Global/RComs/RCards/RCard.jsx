import React from "react";
import RCardFront from "./RCardFront";
import RCardBack from "./RCardBack";
import styles from "./RCard.module.scss";
import { useLocation, useHistory } from "react-router-dom";
import { primaryColor } from "config/constants";
import { useSelector } from "react-redux";
import { hexToRGBA } from "utils/hexToRGBA";

const RCard = ({ course, forceStyle, catalogMode = false }) => {
	const location = useLocation();
	const history = useHistory();
	const { user } = useSelector((state) => state.auth);

	const colorRGBA = course?.color ? hexToRGBA(course?.color, 0.7) : hexToRGBA(primaryColor, 0.7);
	const color = course?.color ? course?.color : primaryColor;
	const studentTypes = ["student", "learner"].includes(user?.type);

	return (
		<div className={styles.r__card} style={forceStyle}>
			<div className={studentTypes && location.pathname.includes("/landing") ? styles.flip__card__inner : null}>
				<RCardFront
					catalogMode={catalogMode}
					course={course}
					color={color}
					colorRGBA={colorRGBA}
					studentTypes={studentTypes}
					history={history}
					user={user}
				/>
				<RCardBack course={course} color={color} user={user} history={history} />
			</div>
		</div>
	);
};

export default RCard;
