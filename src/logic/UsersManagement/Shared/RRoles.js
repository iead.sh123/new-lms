import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import React, { useState } from "react";
import RSingleRole from "./RSingleRole";
import { FontBoldIcon, FontItalicIcon, UnderlineIcon } from "@radix-ui/react-icons";
import { ToggleGroup } from "ShadCnComponents/ui/toggle-group";
import { ToggleGroupItem } from "ShadCnComponents/ui/toggle-group";
import RButton from "components/Global/RComs/RButton";

const RRoles = ({
	roles,
	selectedRoles,
	isFetching,
	frontSearchTerm,
	setFrontSearchTerm,
	setBackSearchTerm,
	handleSelectRole,
	review = false,
	canEdit = true,
	view,
	inUser = false,
	currentUserTypes,
	pickedUserType,
	handleChangePickedUserType,
}) => {
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
	};
	const [openCollapseId, setOpenCollapseId] = useState(null);
	const handleCollapseClicked = (collapseId) => {
		openCollapseId == collapseId ? setOpenCollapseId(null) : setOpenCollapseId(collapseId);
	};
	return (
		<RFlex className="flex-column" styleProps={{ gap: review ? "10px" : "5px", marginBottom: review ? "0px" : view ? "0px" : "75px" }}>
			{!review && (
				<RSearchHeader
					searchLoading={isFetching}
					inputPlaceholder={tr`search_for_a_role`}
					searchData={frontSearchTerm}
					handleChangeSearch={setFrontSearchTerm}
					setSearchData={setFrontSearchTerm}
					handleSearch={handleSearch}
				/>
			)}
			{inUser && (
				<RFlex className="gap-4 ">
					{currentUserTypes?.map((userType, index) => (
						<RButton
							text={userType.title}
							color="primary"
							outline={userType.id == pickedUserType.id ? false : true}
							className="m-0 rounded-[50px]"
							onClick={() => handleChangePickedUserType(userType)}
						/>
					))}
				</RFlex>
			)}
			{review && <span className="p-0 m-0 font-weight-bold">{tr("Roles")}</span>}
			{roles?.length > 0 ? (
				<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
					{roles.map((role) => (
						<RSingleRole
							role={role}
							isSelected={selectedRoles.some((r) => role.id == r.id)}
							isOpen={role.id == openCollapseId}
							handleCollapseClicked={handleCollapseClicked}
							handleSelectRole={handleSelectRole}
							canEdit={canEdit}
							review={review}
						/>
					))}
				</RFlex>
			) : (
				<span className="p-0 m-0 ">{tr("No_Roles_Yet")}</span>
			)}
		</RFlex>
	);
};

export default RRoles;
