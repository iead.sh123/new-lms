import React, { useContext, useEffect } from "react";
import Swal, { DANGER, WARNING } from "utils/Alert";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { greyColor } from "config/constants";
import { useFormik } from "formik";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import RButton from "components/Global/RComs/RButton";
import styles from "../TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RTermStatus = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	const handleFormSubmit = (values, { resetForm }) => {
		TermsManagementData.handleChangeTermStatus({
			status:
				values.isActive && !values.isFinalized
					? "activate"
					: !values.isActive && !values.isFinalized
					? "deactivate"
					: !values.isActive && values.isFinalized
					? "finalize"
					: "",
			termId: TermsManagementData.itemObj?.dataObj?.id,
		});
		// resetForm();
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm, dirty } = useFormik({
		initialValues: {
			isActive: TermsManagementData.itemObj?.dataObj?.isActive || false,
			isFinalized: TermsManagementData.itemObj?.dataObj?.isFinalized || false,
		},
		onSubmit: handleFormSubmit,
	});

	useEffect(() => {
		if (TermsManagementData.itemObj?.dataObj?.id) {
			setFieldValue("isActive", TermsManagementData.itemObj?.dataObj?.isActive);
			setFieldValue("isFinalized", TermsManagementData.itemObj?.dataObj?.isFinalized);
		}
	}, [TermsManagementData.itemObj?.dataObj?.id]);

	// useEffect(() => {
	// 	TermsManagementData.setDirtyFlag(dirty);
	// }, [dirty]);

	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ gap: 15, flexDirection: "column" }}>
				<h6>{TermsManagementData.itemObj?.dataObj?.name}</h6>
				<RFlex styleProps={{ flexDirection: "column", gap: 0, width: "100%" }}>
					<AppRadioButton
						onClick={() => {
							if (!TermsManagementData.itemObj?.dataObj?.isActive && TermsManagementData.itemObj?.dataObj?.isFinalized) {
								//if term status is finalize don't use handler
							} else {
								setFieldValue("isActive", true);
								setFieldValue("isFinalized", false);
							}
						}}
						name="radio"
						checked={values.isActive && !values.isFinalized}
						label={tr`active`}
						label__class__name={styles.active__color}
						disabled={!TermsManagementData.itemObj?.dataObj?.isActive && TermsManagementData.itemObj?.dataObj?.isFinalized}
					/>
					<RFlex styleProps={{ gap: 5 }}>
						<span style={{ color: greyColor, fontSize: "12px" }}>
							{tr`only one term can be active. current active term is`}
							{TermsManagementData.itemObj?.dataObj?.name}
						</span>
						{TermsManagementData.itemObj.termActive &&
							!TermsManagementData.itemObj?.dataObj?.isActive &&
							!TermsManagementData.itemObj?.dataObj?.isFinalized && (
								<span
									className={styles.track__active__term}
									onClick={() => {
										TermsManagementData.handleChangeActiveNode({
											nodeName: "term",
											academicYearId: TermsManagementData.itemObj.academicYearId,
											termId: null,
											gradeLevelId: null,
											homeRoomId: null,
											firstCurriculumID: null,
											allHomeRoomsToGradeLevel: [],
											data: TermsManagementData.itemObj.termActive,
										});

										TermsManagementData.handleOpenCollapse({
											id: `${"term"}_${TermsManagementData.itemObj.academicYearId}_${TermsManagementData.itemObj.termActive.id}`,
											type: "term",
										});
									}}
								>
									{tr`change_this`}
								</span>
							)}
					</RFlex>
				</RFlex>

				<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
					<AppRadioButton
						onClick={() => {
							setFieldValue("isActive", false);
							setFieldValue("isFinalized", false);
						}}
						name="radio"
						checked={!values.isActive && !values.isFinalized}
						label={tr`de_active`}
						label__class__name={styles.deActive__color}
					/>
					<span style={{ color: greyColor, fontSize: "12px" }}>{tr`inactive terms aren’t not visible for staff`}</span>
				</RFlex>

				<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
					<AppRadioButton
						onClick={() => {
							setFieldValue("isFinalized", true);
							setFieldValue("isActive", false);
						}}
						name="radio"
						checked={values.isFinalized && !values.isActive}
						label={tr`finalize`}
						label__class__name={styles.finalize__color}
					/>
				</RFlex>

				<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
					<RButton
						type="submit"
						text={tr`apply_changes`}
						color="primary"
						style={{ width: "30%" }}
						loading={TermsManagementData.termStatusLoading}
						disabled={!dirty || TermsManagementData.termStatusLoading}
						// disabled={!TermsManagementData.dirtyFlag || TermsManagementData.termStatusLoading}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default RTermStatus;
