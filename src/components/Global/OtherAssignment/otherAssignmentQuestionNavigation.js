import React from "react";
import tr from "../RComs/RTranslator";
import { Button, Row } from "reactstrap";
import { TYPES } from "./otherAssignmentReducer";
import { useDispatch, useSelector } from "react-redux";
import { updateQuestionSetStudentDetails } from "store/actions/teacher/questionsSets.actions";

const OtherAssignmentQuestionNavigation = ({
  previousButtonDisabled,
  nextButtonDisabled,
  currentActiveGroup,
  currentActiveQuestion,
  currentActiveTopic,
  lessonGroups,
  dispatch,
  studentQsQuestionId,
  mode,
  assignmentReadMode,
  saveAssignmentDataHandler,
  saveTeacherMarksHandler,
  isLastQuestion,
}) => {
  const dispatch1 = useDispatch();

  const { studentDetails } = useSelector((state) => state.questionSets);

  const clickNextQuestionHandler = () => {
    const currentQuestionsLength =
      lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics[
        currentActiveTopic
      ].inst_questions.length;
    const currentTopicsLength =
      lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics.length;
    const currentGroupsLength = lessonGroups.inst_pattern_groups.length;

    if (currentQuestionsLength - 1 > currentActiveQuestion) {
      dispatch({ type: TYPES.GO_TO_NEXT_QUESTION, payload: lessonGroups });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
      return;
    }

    if (currentTopicsLength - 1 > currentActiveTopic) {
      dispatch({ type: TYPES.GO_TO_NEXT_TOPIC, payload: lessonGroups });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
      return;
    }

    if (currentGroupsLength - 1 > currentActiveGroup) {
      dispatch({ type: TYPES.GO_TO_NEXT_GROUP, payload: lessonGroups });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
      return;
    }
    dispatch({ type: TYPES.DISABLE_NEXT_BUTTON, payload: true });
  };

  const clickPreviousQuestionHandler = () => {
    if (
      currentActiveQuestion === 0 &&
      currentActiveGroup === 0 &&
      currentActiveTopic === 0
    ) {
      dispatch({ type: TYPES.TOGGLE_PREVIOUS_BUTTON, payload: true });
    }
    if (currentActiveQuestion > 0) {
      dispatch({ type: TYPES.GO_TO_PREVIOUS_QUESTION, payload: lessonGroups });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
      return;
    }

    if (currentActiveTopic > 0) {
      dispatch({
        type: TYPES.GO_TO_PREVIOUS_TOPIC,
        payload: {
          lessonGroups0:
            lessonGroups.inst_pattern_groups[currentActiveGroup].inst_topics[
              currentActiveTopic - 1
            ].inst_questions.length - 1,
          lessonGroups1: lessonGroups,
        },
      });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
      return;
    }

    if (currentActiveGroup > 0) {
      dispatch({
        type: TYPES.GO_TO_PREVIOUS_GROUP,
        payload: {
          topic:
            lessonGroups.inst_pattern_groups[currentActiveGroup - 1].inst_topics
              .length - 1,
          question:
            lessonGroups.inst_pattern_groups[currentActiveGroup - 1]
              .inst_topics[
              lessonGroups.inst_pattern_groups[currentActiveGroup - 1]
                .inst_topics.length - 1
            ].inst_questions.length - 1,
          lessonGroups1: lessonGroups,
        },
      });
      dispatch1(
        updateQuestionSetStudentDetails(studentQsQuestionId, {
          payload: { data: studentDetails },
        })
      );
    }
  };

  return (
    <Row className="justify-content-center mt-3 pb-3">
      <Button
        variant="info"
        className="m-2 gu-assignment-previous h_btn_inline_9 mr-2 "
        onClick={clickPreviousQuestionHandler}
        disabled={previousButtonDisabled}
      >
        <i className="fa fa-angle-left fa-lg" aria-hidden="true"></i>&nbsp;
        {tr("Previous")}
      </Button>

      <Button
        variant="info"
        className="m-2 gu-assignment-next h_btn_inline_9 ml-2"
        onClick={clickNextQuestionHandler}
        disabled={nextButtonDisabled}
      >
        {tr("Next")}&nbsp;
        <i className="fa fa-angle-right fa-lg" aria-hidden="true"></i>
      </Button>
    </Row>
  );
};

export default OtherAssignmentQuestionNavigation;
