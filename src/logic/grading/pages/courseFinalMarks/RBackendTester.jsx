import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import { useHistory } from "react-router-dom";
import { get } from "config/api";
import { useSelector, useDispatch } from "react-redux";

import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";

import { Input,Button } from 'reactstrap';
const RBackendTester = ({  }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [processsedRecords, setProcessedRecords] = useState([]);


  const [type,setType]=useState("survey");
  const [itemId,setItemId]=useState(50);
  const [groupId,setGroupId]=useState(10);
  const [studentId,setStudentId]=useState(10);


  const  doIt=()=>{
    alert("doit");
    get_it();
  } 
  
  const get_it = async () => {
    Helper.cl("getit1");
  await Helper.fastGet(Services.grading.backend + `student-gradable-items/my-details/type/${type}/${itemId}`,"fail to get api/dashboard",p,p,false);
  Helper.cl("getit2");
  await Helper.fastGet(Services.grading.backend + `student-gradable-items/students-details-list/type/${type}/${itemId}`,"fail to get api/dashboard",p,p,false);
  Helper.cl("getit3");
  await Helper.fastGet(Services.grading.backend + `student-gradable-items/group-details/groups/${groupId}`,"fail to get api/dashboard",p,p,false);
  Helper.cl("getit4");
  await Helper.fastGet(Services.grading.backend + `student-gradable-items/student-my-group-details/groups/${groupId}`,"fail to get api/dashboard",p,p,false);
  Helper.cl("getit5");  
  await Helper.fastGet(Services.grading.backend + `student-gradable-items/student-group-details/groups/${groupId}/students/${studentId}`,"fail to get api/dashboard",p,p,false);
};
//-----------------------------------------------------------------------
const p=(s)=>{Helper.cl(s,"response")}
useEffect(() => {
  get_it();
}, []);




// GET
// /student-gradable-items/student-details/type/${type}/${itemId}/student/${studentId}


// GET
// /student-gradable-items/students-details-list/type/${type}/${itemId}


// GET
// /student-gradable-items/group-details/groups/${groupId}


// GET
// /student-gradable-items/student-my-group-details/groups/${groupId}


// GET
// /student-gradable-items/student-group-details/groups/${groupId}/students/${studentId}
//-------------------------------------------------------------------------
 

  return (
    <div>
      stuff
      {/* const [type,setType]=useState("survey");
  const [itemId,setItemId]=useState(50);
  const [groupId,setGroupId]=useState(10);
  const [studentId,setStudentId]=useState(10); */}

      <Input	id="title" 	type="text" 
              placeholder={tr("type")}
							onChange={(event) => {setType(event.target.value)}}
							value={type}		/>

<Input	id="itemId" 	type="text" 
              placeholder={tr("itemId")}
							onChange={(event) => {setItemId(event.target.value)}}
							value={itemId}		/>

<Input	id="groupId" 	type="text" 
              placeholder={tr("groupId")}
							onChange={(event) => {setGroupId(event.target.value)}}
							value={groupId}		/>

<Input	id="studentId" 	type="text" 
              placeholder={tr("studentId")}
							onChange={(event) => {setStudentId(event.target.value)}}
							value={studentId}		/>

              <Button onClick={doIt}> Do IT </Button>
    </div>
  );
};

export default RBackendTester;
