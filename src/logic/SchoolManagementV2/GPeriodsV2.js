import React, { useState } from 'react'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import { Input } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import Loader from 'utils/Loader'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import * as colors from 'config/constants'
import moment from 'moment'
import tr from 'components/Global/RComs/RTranslator'
import * as actions from 'store/actions/global/schoolManagement.action'
import iconsFa6 from 'variables/iconsFa6'
import PlusIcon from 'assets/img/plus_icon.svg'
import RButton from 'components/Global/RComs/RButton'
import { useMutateData } from 'hocs/useMutateData'
import { schoolManagementV2API } from 'api/SchoolManagementV2'
import { organizationTypes, types } from './constants'
import { convertTimeToDate } from 'utils/dateUtil'


const GPeriodsV2 = ({
    currentPeriods,
    allPeriods,
    setFieldValue,
    handleChange,
    handleBlur,
    errors,
    touched,
    levelId,
    setFieldTouched,
    setFieldError,
}) => {
    console.log("errorserrors", errors)
    // const inputTime = "14:30";
    // const currentDate = moment().format('YYYY-MM-DD');
    // const dateTimeString = moment(currentDate.concat(`T${inputTime}`)).format(); // Adding 'Z' to indicate UTC
    // console.log("dateTimeString", dateTimeString)
    // const resultMoment = moment(dateTimeString).utc().format();

    const createUpdatePeriodMutation = useMutateData({
        queryFn: ({ payload }) => { schoolManagementV2API.addUpdateContentToLevel(payload) },
        onSuccessFn: ({ data, variables }) => {
            // handleEditFunctionality(null, variables.period, null, true)
            const { fakeId, ...other } = { ...variables.payload.entity }
            setFieldValue(
                `${types.Periods
                }.${variables.payload.entity.index}`,
                { ...other, id: data?.data?.data?.id }
            )
        }
    })
    const deletePeriodMutation = useMutateData({
        // queryFn:({payload})=>{schoolManagementV2API.}
    })

    const handleNewPeriod = () => {
        if (errors?.[types.Periods]) {
            setFieldTouched(`${types.Periods}.${allPeriods?.length - 1}.title`, true)
            setFieldTouched(`${types.Periods}.${allPeriods?.length - 1}.from`, true)
            setFieldTouched(`${types.Periods}.${allPeriods?.length - 1}.to`, true)
            return
        }
        const fakePeriod = {
            fakeId: -allPeriods.length + 1,
            title: "",
            to: "",
            from: "",
            saved: false,
            level_id: levelId,
            index: allPeriods?.length == 0 ? 0 : allPeriods.length
        }
        setFieldValue(`${types.Periods} `, [...allPeriods, fakePeriod])
    }


    const handleOnBlur = (event, period, { propertyName }) => {
        if (errors?.[types.Periods]?.[period?.index]) {
            if (propertyName == 'title' && errors?.[types.Periods]?.[period?.index]?.title)
                return
            else if (propertyName == 'title')
                handleEditFunctionality(null, period, null, true)
            return
        }
        // const toTime=
        const payload = {
            content_type_id: organizationTypes[types.Periods],
            level_id: levelId,
            id: period.id ? period.id:undefined,
            entity: {
                ...period,
                day: 1,
                from: convertTimeToDate(period.from),
                to: convertTimeToDate(period.to),
            }
        }
        createUpdatePeriodMutation.mutate({ payload })
    }

    const handleEditFunctionality = (event, period, extraInfo, savedFlage = false) => {
        setFieldValue(`periods.${period.index}.saved`, savedFlage)
    }
    const deletePeriod = (period) => {
    }
    return (
        currentPeriods.length <= 0 ? <RFlex className="flex-column" styleProps={{ gap: '5px', width: '100%' }}>
            <span className='font-weight-bold p-0 m-0'>{tr("Periods")}</span>
            <RFlex className="justify-content-start" styleProps={{ width: '100%' }}>
                <RButton text={tr("New_Period")}
                    faicon='fa-solid fa-plus'
                    color="link"
                    className="text-primary m-0 p-0"
                    style={{ width: 'fit-content' }}
                    onClick={handleNewPeriod} />

            </RFlex>
        </RFlex> :
            <RFlex className="flex-column" styleProps={{ gap: '5px', width: '100%' }}>
                <RFlex className="justify-content-between" styleProps={{ width: '100%' }}>
                    <span className='font-weight-bold p-0 m-0'>{tr("Periods")}</span>
                    <RButton text={tr("New_Period")}
                        faicon='fa-solid fa-plus'
                        color="link"
                        className="text-primary m-0 p-0"
                        style={{ width: 'fit-content' }}
                        onClick={handleNewPeriod} />

                </RFlex>

                <RFlex styleProps={{ gap: '7px', width: '100%' }}>
                    <RFlex className="flex-column" styleProps={{ width: '100%', gap: '5px' }}>
                        <RFlex id="text" className="justify-content-between" styleProps={{ width: '64%' }}>
                            <span style={{ color: colors.boldGreyColor, margin: '0px', padding: '0px' }}>{tr("Period Title")}</span>
                            <span style={{ color: colors.boldGreyColor, margin: '0px', marginRight: '25px', padding: '0px' }}>{tr("From")}</span>
                            <span style={{ color: colors.boldGreyColor, margin: '0px', padding: '0px' }}>{tr("To")}</span>
                        </RFlex>
                        {currentPeriods?.map((period) => {
                            const titleError = errors?.[types.Periods]?.[period?.index]?.title
                            const titleTouched = touched?.[types.Periods]?.[period?.index]?.title
                            const fromError = errors?.[types.Periods]?.[period?.index]?.from
                            const fromTouched = touched?.[types.Periods]?.[period?.index]?.from
                            const toError = errors?.[types.Periods]?.[period?.index]?.to
                            const toTouched = touched?.[types.Periods]?.[period?.index]?.to
                            return (
                                <RFlex
                                    className={`justify-content-between ${titleError || fromError || toError ? "align-items-start" : "align-items-start"}`}
                                    styleProps={{ width: '95%', gap: '7px' }}>
                                    <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                        <RHoverInput
                                            isLoading={createUpdatePeriodMutation.isLoading}
                                            name={`periods.${period.index}.title`}
                                            inputIsNotValidate={titleError && titleTouched}
                                            inputWidth="120px"
                                            item={period}
                                            extraInfo={{ propertyName: "title" }}
                                            inputValue={period.title}
                                            inputPlaceHolder="Period title"
                                            saved={period.saved}
                                            handleOnBlur={(event, period, extraInfo) => { handleBlur(event); handleOnBlur(event, period, extraInfo) }}
                                            handleEditFunctionality={handleEditFunctionality}
                                            handleInputChange={handleChange}
                                        />
                                        {/* {period.touched['title'] && period.title == "" && <p className='p-0 m-0 text-danger'>Title is Required</p>} */}
                                    </RFlex>
                                    <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                        <Input
                                            name={`periods.${period.index}.from`}
                                            type="time"
                                            // onFocus={() => dispatch({ type: SET_FROM_TIME_TOUCHED, payload: { id: period.id } })}
                                            placeholder={tr`due_time`}
                                            onBlur={() => { handleBlur; handleOnBlur(null, period, { propertyName: "to" }) }}
                                            value={period.from || null}
                                            onChange={handleChange}
                                            style={{ height: "40px", width: "120px" }}
                                            className={fromError && fromTouched ? "input__error" : ""}
                                        />
                                        {/* {period.touched['start_time'] && !period.start_time && !period.invalidateTime && <p className='p-0 m-0 text-danger'>Time is Required</p>} */}
                                        {/* {period.invalidateTime && <p className='p-0 m-0 text-danger' style={{ width: '120px' }}>Start Time can't be in the future</p>} */}

                                    </RFlex>
                                    <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                        <Input
                                            name={`periods.${period.index}.to`}
                                            type="time"
                                            // onFocus={() => dispatch({ type: SET_TO_TIME_TOUCHED, payload: { id: period.id } })}
                                            placeholder={tr`due_time`}
                                            onBlur={() => { handleBlur; handleOnBlur(null, period, { propertyName: "from" }) }}
                                            value={period.to || null}
                                            onChange={handleChange}
                                            style={{ height: "40px", width: "120px" }}
                                            className={toError && toTouched ? "input__error" : ""}

                                        />
                                        {/* {period.touched['end_time'] && !period.end_time && !period.invalidateTime && <p className='p-0 m-0 text-danger'>Time is Required</p>} */}
                                        {/* {period.invalidateTime && <p className='p-0 m-0 text-danger' style={{ width: '120px' }}>End Time can't be in the past</p>} */}

                                    </RFlex>
                                    <RFlex styleProps={{ height: "100%", marginTop: "12px" }} className="align-items-start">
                                        <i
                                            className={`${iconsFa6.delete} text-danger`}
                                            style={{ cursor: 'pointer' }}
                                            onClick={() => deletePeriod(period)} />

                                    </RFlex>
                                </RFlex>
                            )
                        }
                        )}
                    </RFlex>

                </RFlex>
            </RFlex >

    )
}

export default GPeriodsV2

