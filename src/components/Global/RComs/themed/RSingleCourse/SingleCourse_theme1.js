import { faCircle } from "@fortawesome/free-solid-svg-icons";
import tr from "../../RTranslator";
import { Col } from "reactstrap";
import styles from "./styles/theme1.Module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SingleCourse_theme1 = ({
  onClick,
  style = {},
  color,
  hasNotification,
  image,
  name,
  numberOfLessons,
  dir,
}) => (
  <Col lg={4} sm={6} xs={6}>
    <div
      onClick={onClick}
      style={{ backgroundColor: color, ...style }}
      className={`card primary_card ${styles["NightMode"]}`}
    >
      {hasNotification && (
        <span className="notipointCo">
          <FontAwesomeIcon icon={faCircle} style={{ color: "red" }} />
        </span>
      )}
      <div className="header-card text-center">
        <img src={image} className="img-session" alt="image not found" />
        <span
          className="name-session"
          style={{
            color: color,
          }}
        >
          {name}
        </span>
      </div>

      <div className="footer-card">
        <div className="row">
          <div className="col-xs-6 coursesClassStatus text-center">
            {terms &&
              terms.map((term) => (
                <div
                  className="row"
                  onClick={() =>
                    term.nb_sections !== 0 &&
                    history.push(
                      `${process.env.REACT_APP_BASE_URL}/student/courses/${term.term_id}`
                    )
                  }
                  style={{ margin: "0px 5px 5px 5px" }}
                >
                  <div className="col-xs-6">
                    <span
                      className="text-center"
                      style={{ cursor: term.nb_sections !== 0 && "pointer" }}
                    >
                      {term.term_name} &nbsp;
                    </span>
                  </div>
                  <div className="col-xs-6">
                    <span
                      className="text-center"
                      style={{ cursor: term.nb_sections !== 0 && "pointer" }}
                    >
                      - &nbsp;{tr`Sections `} : {term.nb_sections}
                    </span>
                  </div>
                </div>
              ))}

            {!terms && (
              <span className="text-center">
                {numberOfLessons
                  ? dir
                    ? numberOfLessons + " " + tr("lessons")
                    : tr("lessons") + " : " + numberOfLessons
                  : tr`No lessons`}
              </span>
            )}
          </div>
        </div>
      </div>
    </div>
  </Col>
);
export default SingleCourse_theme1;
