import Helper from "components/Global/RComs/Helper";
import { get, post, patch,destroy,put } from "config/api";
import { Services } from "engine/services";

const im_post=(route,payload)=>post(`${Services.im.backend}${route}`,payload);
const im_get=(route)=>get(`${Services.im.backend}${route}`);
const im_delete=(route)=>destroy(`${Services.im.backend}${route}`);
const im_put=(route,payload)=>put(`${Services.im.backend}${route}`,payload);
export const IMApi = {
  getAllConversations: () =>im_get(`api/chats/all`),
  getSettings: () => im_get(`api/im/setting`),
  fetchMoreMessages: (chatId, pageId = 1, direction) =>    im_get(`api/message/fetch-more/${chatId}/${pageId}/${direction}`),

  changePointer: (chatId, payload) =>    im_post(`api/chats/${chatId}/changePointer`, payload),
  sendToChat: (chatId, payload) => im_post(`api/chats/sendMessage/${chatId}`, payload),
  getAllUsers: (prefix) => im_get(`api/chats/getUsers?prefix=${prefix}`),
  createConversation: (payload) => im_post(`api/chats/initialNewChat`, payload),
  updateSettings: (payload) => im_post(`api/im/setting`, payload),
  loadMessagesAround: (msgId) =>im_get(`api/message/load-around/${msgId}`),


  tagMessage: (chatId, messageId, payload) =>    im_post(`api/message/${messageId}/tag`, payload),
  deleteMessage: (messageId) => im_delete(`api/message/${messageId}/delete`),
  unsendMessage: (msgId) => im_delete(`api/message/${msgId}/unsend`),
  searchForwardTo: (prefix) =>    im_get(`api/chats/search-parties-to-forward/${prefix}`),
  searchMultipleChats: (prefix) => im_get(`api/chats/search/${prefix}/all`),
  searchChat: (prefix,chatId) => im_get(`api/chats/search-chat/${chatId}/${prefix}`),
  replyToMessage: (msgId, payload) =>    im_post(`api/message/reply/${msgId}`, payload),
  forwardMessagetoContact: (contactId, msgId) =>    im_post(`api/message/forward/${msgId}/user/${contactId}`),
  forwardMessagetoExistingChat: (chatId, msgId) =>    im_post(`api/message/forward/${msgId}/chat/${chatId}`),

  tagChat: (chatId, payload) => im_post(`api/chats/${chatId}/tag`, payload),
  deleteChat: (chatId) => im_delete(`api/chats/${chatId}`),

  editGroup: (chatId, payload) => im_put(`api/group/${chatId}/edit-properties`, payload),

  getGroupParticipants: (chatId) => im_get(`api/group/participants/${chatId}`),
  addGroupParticipants: (chatId, payload) =>    im_post(`api/group/${chatId}/participants`, payload),
  markAsGroupAdmin: (chatId, userId) =>    im_post(`api/group/make-as-admin/${chatId}/${userId}`),

  removeGroupParticipants: (chatId, userId) =>    im_delete(`api/group/${chatId}/participant/${userId}`),

  //chatGallery: (chatId, nextPage) =>  im_get(`api/chats/gallery/${chatId}${nextPage ? `?page=${nextPage}` : ""}`),
  chatGallery: (chatId, nextPage) =>    im_get(`api/chats/${chatId}/gallery`),
  getChat: (chatId) => im_get(`api/chats/${chatId}`), 
  saveReport: (payload) => im_post(`api/report/`, payload),
  getAllReportCategories: (chatId) => im_get(`api/report/categories`),

  setOnline:(s) => im_post(`api/users/online/${s?1:0}`),
  chatSharedGroup:(chatId)=>im_get(`api/chats/${chatId}/shared-groups`),
  chatLinks:(chatId)=>im_get(`api/chats/${chatId}/links`),
  starredMessages:(chatId)=>im_get(`api/chats/${chatId}/starred-messages`),
  // im_getGroupUsers: (groupId, pageId = 1, pageSize = 20) => im_get(`api/chats/participants/${groupId}?pageSize=${pageSize}&pageId=${pageId}`)
  leaveGroup:(chatId)=>im_post(`api/group/leave/${chatId}`)
  
};
