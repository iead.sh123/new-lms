import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RSelect from 'components/Global/RComs/RSelect';
import tr from 'components/Global/RComs/RTranslator';
import { Services } from 'engine/services';
import { enrollment_components } from 'logic/TermManagement/constants/enrolment.components.constant';
import React from 'react';
import { Row, Col, Table } from 'reactstrap';
import Loader from 'utils/Loader';

const EnrollStudentsEditor = ({ handleLoadGradeLevelStudents, gradeLevels, students, handleEnrollUsers }) => {

    const [loadingStudents, setLoadingStudents] = React.useState(false);


    React.useEffect(() => {
        loadGradeLevelStudents([]);
    }, [])

    const loadGradeLevelStudents = async (gds) => {
        setLoadingStudents(true);
        await handleLoadGradeLevelStudents(gds.map(gd => +gd.value));
        setLoadingStudents(false);
    }

    return <React.Fragment>


        <Col md="3" className='mb-4'>
            <RSelect name={tr`Grade Levels`}
                onChange={loadGradeLevelStudents}
                option={Object.values(gradeLevels.byId).map(gl => ({ label: gl.title, value: gl.id }))}
                isMulti
                closeMenuOnSelect={false}
                placeholder={tr`Grade Levels`} />
        </Col>
        <Table bordered responsive>
            <thead style={{ backgroundColor: "#D7E3FD", fontWeight: "bold" }}>
                <tr style={{ textAlign: "center" }}>
                    <td>{tr`Name`}</td>
                    <td>{tr`Enrolled Classes`}</td>
                    <td>{tr`Grade Level`}</td>
                    <td>{tr`Actions`}</td>
                </tr>
            </thead>
            {loadingStudents ? <Loader /> : <tbody>
                {students?.ids.map(tId => (<tr key={'homeroom-enrolled-' + tId}>
                    <td className="text-center">
                        <RFlex>
                            <img
                                width={30}
                                height={30}
                                style={{ borderRadius: "100%" }}
                                src={Services.auth_organization_management.file + students.byId[tId].hash_id}
                            />
                            <span>{students.byId[tId].name}</span>
                        </RFlex>
                    </td>
                    <td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
                        {students.byId[tId].courses.map(c=>c.name).join(',')}
                    </td>
                    <td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
                        {students.byId[tId].gradeLevel}
                    </td>
                    <td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
                        <p onClick={() => handleEnrollUsers({ ...students.byId[tId], type: enrollment_components.STUDENT })} className="h5 mt-4" style={{ color: '#668AD7', cursor: 'pointer' }}><i className="fa fa-plus" /> {tr`Enroll`}</p>
                    </td>
                </tr>))}
            </tbody>}
        </Table>
    </React.Fragment>
}


export default EnrollStudentsEditor;