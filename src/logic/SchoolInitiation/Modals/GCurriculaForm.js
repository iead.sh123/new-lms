import React, { useEffect, useContext } from "react";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { getMainCoursesAsync } from "store/actions/global/schoolInitiation.actions";
import { curriculaByIdAsync } from "store/actions/global/schoolInitiation.actions";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import RCurriculaForm from "view/SchoolInitiation/Modals/RCurriculaForm";
import Loader from "utils/Loader";

const GCurriculaForm = () => {
  const { sIType, sITypeId } = useParams();
  const dispatch = useDispatch();
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  useEffect(() => {
    if (sIType == "curricula" && sITypeId) {
      dispatch(
        curriculaByIdAsync(sITypeId, SchoolInitiationData.setAddCurricula)
      );
    }
    dispatch(getMainCoursesAsync(true));
  }, [sITypeId]);

  return (
    <>
      {SchoolInitiationData.egcLoading ? (
        <Loader />
      ) : (
        <>
          <RCurriculaForm />
        </>
      )}
    </>
  );
};

export default GCurriculaForm;
