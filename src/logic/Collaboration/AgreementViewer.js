import RTabsPanel from "components/Global/RComs/RTabsPanel";
import RTabsPanel_Horizontal from "components/Global/RComs/RTabsPanel_Horizontal";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import React, { useCallback, useEffect, useState } from "react";
import { GCollaborationSettingContents } from "./GCollaborationSettingContents";
import { GCollaborationAgreementsClients } from "./GCollaborationAgreementsClients";
import { GCollaborationAgreementsProviders } from "./GCollaborationAgreementsProviders";
import tr from "components/Global/RComs/RTranslator";
import RFormModal from "components/Global/RComs/RFormModal";

import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { useReducer } from "react";
import RSubmit from "components/Global/RComs/RSubmit";
import produce from "immer";
import store from "store";
import { put } from "config/api";
import { post } from "config/api";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { SUCCESS } from "utils/Alert";
import RCollaborationAccordion from "components/Global/RComs/Collaboration/RCollaborationAccordion";
import RFlex from "components/Global/RComs/RFlex/RFlex";

export const AgreementViewer = ({agreement_id}) => {

const [aggregatedDetails,setAggregatedDetails]=useState([]);
const aggregate=(details)=>{
  const aggregateContents=[];
    const det1=details.map(d=>
     {

      
       const lid=d.collaboration_level.id;
       const cid=d.content_type.id;
       const prev=aggregateContents.filter(a=>a.id==cid);
       if(prev&&prev.length)
       {
          if(!prev[0].levels)  prev[0].levels=[];
          const prevlev=prev[0].levels.filter(ll=>ll.id==lid);
          if(!(prevlev&&prevlev.length)) 
              { prev[0].levels.push(d.collaboration_level);
                  //Helper.cl("added ------------------------");
              }
              else{
                //Helper.cl("not added 000000000000000");
                //Helper.cl(d.collaboration_level,"atest collaboration_level");
                //Helper.cl(prevlev,"atest prevlev");
              }
        }
       else
       {
         const o=d.content_type;
         o.levels=[];
         o.levels.push(d.collaboration_level);
         //Helper.cl("first added -------------------");
         aggregateContents.push(o);
       }
  
     });
       //Helper.cl(aggregateContents,"aggregateContents");
     //dispatch({type:"SET_AGREEMENT_CONTENTS",payload:aggregateContents});
     setAggregatedDetails(aggregateContents);
  }

useEffect(()=>{
  //Helper.cl("agid 11");
  const getFromBackend = async () => {
    //Helper.cl("agid 2");
    if(!agreement_id) return;
    //{{collaboration_url}}/api/organization/all
    //{{collaboration_url}}/api/organization/2/contents
    //{{collaboration_url}}/api/organization/2/levels
    const url = Services.collaboration.backend + `api/collaboration-agreement/`+agreement_id;
    //Helper.cl("agid 13");
    let response1 = await get(url);
    //Helper.cl("agid 14");
    if (response1 && response1.data && response1.data.status == 1) {
        const agreement1=response1.data.data.agreement;
         //Helper.cl(agreement1?.details.length,"agid 16 agreement1?.details.length");
      
         aggregate(agreement1?.details);
    
      }
  };
  getFromBackend();
 
},[agreement_id])

//return <div>  {Helper.js(aggregatedDetails,"aggregatedDetails")}</div>
  return (<RFlex style={{flexWrap: "wrap",justifyContent:"flex-start",alignItems:"flex-start"}}>  

  
  {   aggregatedDetails.map(d=>{
      return <div style={{display:"inline-block",width:"45%",margin:"15px"}}><RCollaborationAccordion title={{text:<div>{d.name}</div>}} content={<div>
        <ul>{d.levels.map(l=><li>{l.name}</li>)}</ul>
       </div>}></RCollaborationAccordion>
       </div>
       })
     }
    </RFlex>
  );
};