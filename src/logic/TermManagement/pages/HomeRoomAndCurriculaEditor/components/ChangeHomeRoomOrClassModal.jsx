
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RModal from "components/Global/RComs/RModal";

import tr from "components/Global/RComs/RTranslator";
import { homeRoomOrCurriculaMode } from "logic/TermManagement/constants/homeroms-curricula-editor-modes.constants";

import React from "react";
import Loader from "utils/Loader";




const ChangeHomeRoomOrClassModal = ({ mode, isOpen, toggle, handleSave, options, value }) => {

    const [selected, setSelected] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    const save = async () => {
        setLoading(true);
        await handleSave(+selected);
        setLoading(false);
    }

    React.useEffect(() => {
        setSelected(+value);
    }, [value]);

    return <RModal isOpen={isOpen}
        withCloseIcon={false}
        toggle={toggle}
        header={<RFlex> <p>{mode === homeRoomOrCurriculaMode.HOMEROOM ? tr`Change Student Homeroom` : tr`Change Student Class`}</p></RFlex>}
        footer={
            loading ? <Loader /> : <RFlex styleProps={{ width: '100%', alignItems: 'start' }}>
                <RButton
                    text={tr`create`}
                    color="primary"
                    onClick={save}
                // style={{alignSelf: 'end'}}
                />

                <RButton
                    text={tr`cancel`}
                    style={{ background: 'white', color: '#668AD7' }}
                    // faicon="fa fa-plus"
                    // color="link"
                    onClick={toggle}
                // style={{alignSelf: 'end'}}
                />
            </RFlex>
        }
        body={<div>
            {options.map(op => (
                <RFlex key={'ChangeHomeRoomOrClassModal-radio-' + op.value} styleProps={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <RFlex styleProps={{ alignItems: 'center' }}>
                        <input type="radio" onChange={(e) => setSelected(+e.target.value)} value={op.value} name="class-home-room" checked={selected === op.value} />
                        <p>{op.label}</p>
                    </RFlex>
                    <RFlex>
                        <i className="fa fa-users" />
                        <p>{op.students} Students</p>
                    </RFlex>
                </RFlex>
            ))}
        </div>}
    ></RModal>

}

export default ChangeHomeRoomOrClassModal;