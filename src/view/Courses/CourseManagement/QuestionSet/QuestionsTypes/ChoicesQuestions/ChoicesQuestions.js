import React, { useContext } from "react";
import { FormGroup, Input, Col } from "reactstrap";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { dangerColor } from "config/constants";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const ChoicesQuestions = ({ questionGroupId, question, choice, questionType, qType, index, questionBank }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const QuestionsBankData = useContext(QuestionsBankContext);

	const QS = questionBank ? QuestionsBankData : QuestionSetData;

	return (
		<>
			<Col xs={12} sm={10} className="mb-2">
				<FormGroup className="d-flex align-items-center justify-content-between">
					{/* <i className="fa-solid fa-grip-vertical pl-0 pr-2"></i> */}
					{questionType == "SEQUENCING" ? <span>{index + 1}.</span> : qType == "choice" ? String.fromCharCode(65 + index) : ""}

					<Input
						style={{ margin: "0px 20px" }}
						name="text"
						type="text"
						placeholder={tr`text`}
						value={choice?.text}
						onChange={(event) => {
							QS.handleFillDataInChoicesAnswers({
								questionGroupId: questionGroupId,
								questionTypeId: question?.id ? question?.id : question?.fakeId,
								choiceId: choice?.id ? choice?.id : choice?.fakeId,
								key: event.target.name,
								value: event.target.value,
								questionType: qType,
							});
						}}
						disabled={questionType == "TRUEFALSE" ? true : false}
					/>

					{questionType !== "SEQUENCING" && (
						<>
							{questionType == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS" ? (
								<AppCheckbox
									// name={questionGroupId}
									onChange={(e) => {
										QS.handleFillDataInCorrectAnswerToSomeQuestions({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											value: choice.order,
											key: "correctAnswer",
											questionType: question.type,
										});
									}}
									label={tr`correct`}
									checked={question.correctAnswer?.split(",")?.map(Number).includes(+choice.order) ? true : false}
								/>
							) : (
								<AppRadioButton
									name={questionGroupId + question?.id ? question?.id : question?.fakeId}
									label={tr`correct`}
									checked={+question.correctAnswer == choice.order ? true : false}
									onClick={(e) =>
										QS.handleFillDataToQuestionType({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											key: "correctAnswer",
											value: choice.order,
										})
									}
								/>
							)}
						</>
					)}
				</FormGroup>
			</Col>

			<Col xs={12} sm={2} className="mb-2">
				<RFlex
					styleProps={{
						alignItems: "center",
						justifyContent: "end",
					}}
				>
					<RFileSuite
						parentCallback={(values) => {
							QS.handleFillDataInChoicesAnswers({
								questionGroupId: questionGroupId,
								questionTypeId: question?.id ? question?.id : question?.fakeId,
								choiceId: choice?.id ? choice?.id : choice?.fakeId,
								key: "files",
								value: values,
								questionType: qType,
							});
						}}
						singleFile={true}
						binary={true}
						value={choice?.files}
						theme="button_with_replace"
						fileType="image/*"
					/>
					<i
						className={iconsFa6.delete}
						style={{ color: dangerColor, cursor: "pointer" }}
						onClick={() =>
							QS.handleRemoveChoiceFromQuestion({
								questionGroupId: questionGroupId,
								questionTypeId: question?.id ? question?.id : question?.fakeId,
								choiceId: choice?.id ? choice?.id : choice?.fakeId,
								questionType: qType,
								choiceOrder: choice.order,
							})
						}
					/>
				</RFlex>
			</Col>
		</>
	);
};

export default ChoicesQuestions;
