
import RFileUploader from "components/Global/RComs/RFileUploader";

const baseURL = process.env.REACT_APP_RESOURCE_URL;

const RFileUploaderAdapter = ({ parentCallback, singleFile, editedImages,sizeFile,value }) => {
  //when we have an image id that mesns we are editing
  return (
    <div style={{ margin: "5px" }}>
      {editedImages && editedImages.length > 0 ? (
        editedImages?.filter(ei=>ei.id).map(ei=><div>
          <label
            style={{ color: "red", fontSize: 24, margin: "2px" }}
            onClick={() => parentCallback([])}
          >
            {" "}
            x{" "}
          </label>
          <img src={`${baseURL}${ei.url}`} />
        </div>)
      ) : (
        <RFileUploader parentCallback={parentCallback} singleFile={singleFile} sizeFile={sizeFile} value={value} />
      )}
    </div>
  );
};

export default RFileUploaderAdapter;
