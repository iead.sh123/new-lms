import React, { useEffect, useRef, useState } from "react";
import { TYPES } from "../assignmentReducer";
import { toast } from "react-toastify";
import CanvasDraw from "react-canvas-draw";
import lzString from "lz-string";
import DrawTools from "./DrawTools";

const DrawQuestion = ({ question, groupId, dispatch, answers, assignmentReadMode, mode, student }) => {
	const [savedCanvas, setSavedCanvas] = useState("");
	const [brushColor, setBrushColor] = useState("");
	const [brushSize, setBrushSize] = useState(6);
	const [imageUrl, setImageUrl] = useState("");

	useEffect(() => {
		if (assignmentReadMode && mode !== "multi") {
			if (question.notSolved === 1) {
				setSavedCanvas("");
				return;
			}

			setSavedCanvas(lzString.decompressFromUTF16(question.student_draw));
			return;
		}
		if (mode === "multi") {
			if (student.notSolved === 1 || student.notSolved === null) {
				setSavedCanvas("");
				return;
			}

			setSavedCanvas(lzString.decompressFromUTF16(student.student_draw));
			return;
		}
		if (answers && answers.length > 0) {
			const canvas = answers.filter((answer) => answer.QuestionId === question.id)[0];
			if (canvas !== undefined) {
				const draw = lzString.decompressFromUTF16(canvas.draw);
				setSavedCanvas(draw);
			}
		}

		if (question.attachments) {
			setImageUrl(question.attachments?.url);
		}
	}, [question.attachments]);

	const saveCanvas = () => {
		try {
			dispatch({
				type: TYPES.SAVE_ANSWER,
				payload: {
					QuestionId: question.id,
					GroupId: groupId,
					Attachments: [],
					draw: lzString.compressToUTF16(canvasRef.current.getSaveData()),
					Choices: [],
					FreeAnswer: null,
				},
			});
		} catch (error) {
			toast.error(error?.message);
		}
	};

	const canvasRef = useRef();

	return (
		<div>
			{!assignmentReadMode && (
				<DrawTools
					canvasRef={canvasRef}
					brushColor={brushColor}
					setBrushSize={setBrushSize}
					brushSize={brushSize}
					setBrushColor={setBrushColor}
					saveCanvas={saveCanvas}
				/>
			)}
			<CanvasDraw
				lang={"ar"}
				ref={(canvasDraw) => (canvasRef.current = canvasDraw)}
				canvasWidth={600}
				canvasHeight={600}
				brushColor={brushColor}
				saveData={savedCanvas}
				brushRadius={brushSize}
				disabled={assignmentReadMode}
				imgSrc={`${process.env.REACT_APP_RESOURCE_URL}${imageUrl}`}
				// imgSrc="https://upload.wikimedia.org/wikipedia/commons/a/a1/Nepalese_Mhapuja_Mandala.jpg"
				// imgSrc="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Cappuchin_Bridge_%2830706076020%29.jpg/500px-Cappuchin_Bridge_%2830706076020%29.jpg"
				// imgSrc="https://ics.alrowad-tech.com/b37/storage/app/public/Skelton.jpg"
				// imgSrc="https://upload.wikimedia.org/wikivoyage/en/thumb/8/8c/Wikivoyage_stars_map.svg/900px-Wikivoyage_stars_map.svg.png"
			/>
			{!assignmentReadMode && (
				<DrawTools
					canvasRef={canvasRef}
					brushColor={brushColor}
					setBrushSize={setBrushSize}
					brushSize={brushSize}
					setBrushColor={setBrushColor}
					saveCanvas={saveCanvas}
				/>
			)}
		</div>
	);
};

export default DrawQuestion;
