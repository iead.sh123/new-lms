export default {
  TEACHER: "teacher",
  SENIORTEACHER: "seniorteacher",
  PRINCIPLE: "principal",
  SCHOOL_ADVISOR: "school_advisor",
  STUDENT: "student",
  PARENT: "parent",
  ADMIN: "employee",
};
