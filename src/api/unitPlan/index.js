import { get, post } from "config/api";
import { Services } from "engine/services";
import { versionServiceUnitPlan } from "engine/config";

export const unitPlanApi = {
  getUnitPlanById: (unitPlanId) =>
    get(
      `${Services.unit_plan.backend}api/${versionServiceUnitPlan}/unit-plan/view/${unitPlanId}`
    ),

  saveUnitplan: (request) =>
    post(
      `${Services.unit_plan.backend}api/${versionServiceUnitPlan}/unit-plan/store`,
      request
    ),
};
