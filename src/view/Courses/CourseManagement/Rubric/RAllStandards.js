import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RAllStandards = ({ arrayHelpers }) => {
	const RubricData = useContext(RubricContext);

	return (
		<ul style={{ padding: "5px 0px 0px 22px" }}>
			{RubricData.values.categories &&
				RubricData.values.categories.length > 0 &&
				RubricData.values.categories.map((category, index) =>
					category.standards.map((standard, standardIndex) => {
						return (
							<RFlex key={standard.id} styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
								<li
									style={{ listStyleType: "square", color: "#818181" }}
									onClick={() =>
										arrayHelpers.push({
											fakeId: Math.random(),
											subject: standard.subject,
											points: standard.points,
											description: standard.description,
											ratings: standard.ratings.map((rate) => {
												return { fakeId: Math.random(), title: rate.title, score: rate.score };
											}),
										})
									}
								>
									{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}
								</li>
							</RFlex>
						);
					})
				)}
		</ul>
	);
};

export default RAllStandards;
