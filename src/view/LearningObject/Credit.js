import React from "react";
import styles from "./Credit.Module.scss";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col } from "reactstrap";
import REmptyData from "components/RComponents/REmptyData";

const Credit = ({
	learningObject,
	showCredit,
	certificate,
	badges,
	handleAddValueToLearningObject,
	certificateSelected,
	setCertificateSelected,
}) => {
	return (
		<Col xs={12}>
			{showCredit && (
				<Row>
					<Col sm={6} xs={12} className={styles.credit_center}>
						<h6
							className={learningObject?.credit_type == "certificate" && styles.credit_selected}
							style={{ textAlign: "center" }}
							onClick={() => {
								handleAddValueToLearningObject("credit_type", "certificate");
								setCertificateSelected(true);
							}}
						>{tr`certificate`}</h6>
					</Col>
					<Col sm={6} xs={12} className={styles.credit_center}>
						<h6
							className={learningObject?.credit_type == "badge" && styles.credit_selected}
							onClick={() => {
								handleAddValueToLearningObject("credit_type", "badge");
								setCertificateSelected(false);
							}}
						>{tr`badge`}</h6>
					</Col>

					<Col xs={12}>
						{certificateSelected ? (
							<Row>
								{certificate.length > 0 ? (
									certificate.map((cer) => (
										<Col sm={6} xs={12} key={cer.id}>
											<div
												className={styles.credit}
												style={{
													border: learningObject.credit_id == cer.id ? "2px solid #333" : "2px solid #b5b7ba",
												}}
												onClick={() => handleAddValueToLearningObject("credit_id", cer.id)}
											>
												{cer.name}
											</div>
										</Col>
									))
								) : (
									<Col xs={12}>
										<REmptyData />
									</Col>
								)}
							</Row>
						) : (
							<Row>
								{badges.length > 0 ? (
									badges.map((bad) => (
										<Col sm={6} xs={12} key={bad.id}>
											<div
												className={styles.credit}
												style={{
													border: learningObject.credit_id == bad.id ? "2px solid #333" : "2px solid #b5b7ba",
												}}
												onClick={() => handleAddValueToLearningObject("credit_id", bad.id)}
											>
												{bad.name}
											</div>
										</Col>
									))
								) : (
									<Col xs={12}>
										<REmptyData />
									</Col>
								)}
							</Row>
						)}
					</Col>
				</Row>
			)}
		</Col>
	);
};

export default Credit;
