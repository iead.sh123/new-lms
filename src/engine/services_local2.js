export const Services_local = {
  section: {
    backend: "http://192.168.1.71:8008/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  courses_manager: {
    backend: "http://192.168.1.71:8008/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  lesson: {
    backend: "http://192.168.1.71:8003/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  question_set: {
    backend: "http://192.168.1.71:3002/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  grading: {
    backend: "http://192.168.1.71:3005/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  scorm: {
    backend: "http://192.168.1.71:8020/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  certificate: {
    backend: "http://192.168.1.71:8010/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  unit_plan: {
    backend: "http://192.168.1.72:15003/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },
  rubric: {
    backend: "http://192.168.1.72:15005/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  badge: {
    backend: "http://192.168.1.72:15002/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  lesson_plan: {
    backend: "http://192.168.1.72:15007/",
    print: "http://192.168.1.9:9003/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  tag_search: {
    backend: "http://192.168.1.72:15004/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  logs: {
    backend: "http://192.168.1.72:8009/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  school_initiation: {
    backend: "http://192.168.1.72:15000/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  lesson_content: {
    backend: "http://192.168.1.71:8034/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  authentication: {
      backend: "http://localhost:7001/",
      file: "http://192.168.1.73:8001/api/files/get-by-id/",
    },
  
    auth_organization_management: {
    backend: "http://localhost:7000/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
    },

  discussion: {
    backend: "http://192.168.1.71:18000/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },
  cohorts: {
    backend: "http://192.168.1.71:6324/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  cohort: {
    backend: "http://192.168.1.70:8095/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  notficiation: {
    backend: "http://192.168.1.73:8002/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  storage: {
    backend: "http://192.168.1.73:8001/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
    flipBookFile: "http://192.168.1.73:8001/api/files/get/",
  },

  im: {
    backend: "http://192.168.1.70:8077/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  terms: {
    backend: "http://192.168.1.71:3003/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  calender:{
    backend: "http://192.168.1.71:3001",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },
  collaboration: {
    backend: "http://192.168.1.73:7003/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },

  meeting: {
    backend: "http://192.168.1.73:8004/",
    file: "http://192.168.1.73:8001/api/files/get-by-id/",
  },
};
