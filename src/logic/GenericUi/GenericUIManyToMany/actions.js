
//initialization
export const INITIALIZE_STATE = "INITIALIZE_STATE";

//relations
export const ADD_RELATION = "ADD_RELATION";
export const DELETE_RELATION = "DELETE_RELATION";
export const CANCEL_ADD_RELATION = "CANCEL_ADD_RELATION";

//relation attribute
// export const ADD_ATTRIBUTE = "ADD_ATTRIBUTE";
export const UPDATE_ATTRIBUTE = "UPDATE_ATTRIBUTE";
// export const DELETE_ATTRIBUTE = "DELETE_ATTRIBUTE";

//stage
export const SET_STAGE = "SET_STAGE";