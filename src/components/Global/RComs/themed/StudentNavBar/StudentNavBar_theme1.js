import styles from "./styles/theme1.Module.scss";
import React from 'react';
import {
    Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem, UncontrolledDropdown,
    DropdownToggle,
    DropdownItem,
    Nav,
    Collapse,
    DropdownMenu
} from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import RThemeSwitcher from "../../RthemeSwitcher";

const StudentNavBar_theme1 = ({
    brandHref,
    brandImage,
    handleBack,
    title,
    dropLists,
    customStyle = {}
}) => <section className={`navbar-section ${styles["NightMode"]}`} style={customStyle}>
        <div>
            <Navbar color="primary" container expand fixed="" dark className="pb-4">
                <NavbarBrand
                    href={
                        brandHref
                    }
                    className="navBrand"
                >
                    <img src={brandImage} className="d-block" />
                    <div></div>

                    <NavbarText>
                        {handleBack ? (
                            // <Link to={nav.linkBack}>
                            <FontAwesomeIcon
                                onClick={handleBack}
                                icon={faChevronLeft}
                                style={{ color: "#32353B" }}
                            />
                            // </Link>
                        ) : (
                            <div className="backPlaceholder"></div>
                        )}
                    </NavbarText>
                </NavbarBrand>

                <NavbarToggler onClick={() => null} />
                {title && (
                    <NavbarBrand className="pageTitle greyNav">
                        <div>
                            <h4 className={styles["title"]}>{title}</h4>
                        </div>
                    </NavbarBrand>
                )}

                <Collapse navbar>
                    <Nav className="me-auto">
                        {dropLists.map((di, i) => (
                            <UncontrolledDropdown key={"di" + i} nav>
                                <DropdownToggle nav caret>
                                    {di.toggleElement}
                                </DropdownToggle>
                                <DropdownMenu right>
                                    {di.menu.map(it => <DropdownItem
                                        key={it.id}
                                        onClick={it.onClick}
                                        style={
                                            it.id === di.selectedItemId
                                                ? { backgroundColor: "#DDD", color: "#FFF" }
                                                : {}
                                        }
                                    >
                                        <i>{it.caption}</i>
                                    </DropdownItem>
                                    )
                                    }
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        ))}

                    </Nav>

                </Collapse>
                <RThemeSwitcher />
            </Navbar>
        </div>
    </section>

export default StudentNavBar_theme1;


// dropDownItems: [{
//   toggleElement: element,
//   selectedItemId: "",
//   menu: [
//     {
//       id: "",
//       onClick: "",
//       caption: ""
//     }

//   ]
// }]
