import { gradingApi } from "api/grading";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import Loader from "utils/Loader";
import styles from "./grading.module.scss";
import { useFormik } from "formik";
import RGroupItem from "./RGroupItem";
import RLister from "components/Global/RComs/RLister";
import { termTableData } from "./Constants";
import clickCourse from "assets/img/clickCourse.png";
import REmptyData from "components/RComponents/REmptyData";
const GStudentTerm = ({ studentId }) => {
	const [activeTermId, setActiveTermId] = useState(null);
	const [activeCourseId, setActiveCourseId] = useState(null);
	const [openGroupCollapseId, setOpenGroupCollapseId] = useState(null);
	const handleToggleGroupCollapse = (groupId) => {
		openGroupCollapseId == null ? setOpenGroupCollapseId(groupId) : setOpenGroupCollapseId(null);
	};
	const initialValues = {
		courseDetails: {},
		oneTermValue: {},
	};
	const { values: studentTermState, setFieldValue, resetForm } = useFormik({ initialValues });
	const {
		data: terms,
		isLoading: isLoadingTerms,
		fetchStatus,
	} = useFetchDataRQ({
		queryKey: ["allTerms"],
		queryFn: () => gradingApi.getAllOrganizationTerms(),
		onSuccessFn: (data) => {
			const firstId = data?.data?.data?.[0]?.id ?? null;
			setActiveTermId(firstId);
		},
	});
	const { data: termData, isLoading: isLoadingTermData } = useFetchDataRQ({
		queryKey: ["term", activeTermId],
		queryFn: () => gradingApi.getTermData(activeTermId),
		enableCondition: activeTermId ? true : false,
		keepPreviousData: true,
		onSuccessFn: (data) => {
			setActiveCourseId(null);
			setFieldValue("courseDetails", {});
			const finalData = {};
			const courses = data.data.data.courses.reduce((result, record) => {
				result[record.name] = { id: record.id, value: record?.grades?.calculatedMark, type: "course" };
				return result;
			}, {});
			const RestData = termTableData.reduce((result, item) => {
				result[item.label] = { value: data.data.data[item.value], type: "text" };
				return result;
			}, {});
			finalData[1] = { ...courses, ...RestData };
			finalData["ids"] = [1];
			setFieldValue("oneTermValue", finalData);
		},
	});

	const {
		isLoading: isLoadingStudentDetailsSlider,
		fetchStatus: studentDetailsSliderFetchStatus,
		isError,
		refetch: refetchStudentDetailsSlider,
	} = useFetchDataRQ({
		queryFn: () => gradingApi.getStudentDetailsForGroup(activeCourseId, studentId),
		queryKey: ["StudentTerm", activeCourseId],
		keepPreviousData: true,
		enableCondition: activeCourseId ? true : false,
		onSuccessFn: (data) => {
			const groupsIds = [];
			const studentDetailsSlider = {};
			studentDetailsSlider["courseInfo"] = data?.data?.data?.courseInfo;
			studentDetailsSlider["studentInfo"] = data?.data?.data?.student;
			const groupscollapses = data?.data?.data?.groups?.reduce((result, groupCollapse) => {
				const itemsIds = [];
				groupsIds.push(groupCollapse?.group?.id);
				result[groupCollapse?.group?.id] = {};
				result[groupCollapse?.group?.id]["name"] = groupCollapse?.group?.name;
				result[groupCollapse?.group?.id]["groupMark"] = groupCollapse?.groupMark;
				const items = groupCollapse?.items?.reduce((result, item) => {
					itemsIds.push(item?.gradableItem?.id);
					result[item?.gradableItem?.id] = {
						name: item?.gradableItem?.name,
						points: item?.gradableItem?.points,
						mark: item?.studentDetails?.calculatedFinalTeacherGrade,
						teacherGrade: { touched: false, saved: true, value: item?.studentDetails?.teacherGrade },
						type: item?.studentDetails?.type,
					};
					return result;
				}, {});
				items["ids"] = itemsIds;
				result[groupCollapse?.group?.id]["items"] = { ...items };
				return result;
			}, {});
			groupscollapses["ids"] = groupsIds;
			studentDetailsSlider["groups"] = { ...groupscollapses };
			console.log("studentDetailsSlider", studentDetailsSlider);
			setFieldValue("courseDetails", studentDetailsSlider);
		},
	});
	if (isLoadingTerms) {
		return <Loader />;
	}
	console.log("OneTermValue", studentTermState);
	const _termData = !isLoadingTermData
		? studentTermState.oneTermValue?.ids?.map((id) => {
				const record = studentTermState?.oneTermValue?.[id];
				console.log("currentRecord", record);
				return {
					id: id,
					table_name: "termData",
					details: Object.keys(record)?.map((header) => {
						const courseValueComponent = (
							<RFlex styleProps={{ cursor: "pointer" }} onClick={() => setActiveCourseId(record?.[header].id)}>
								{record?.[header].value}
							</RFlex>
						);
						const courseKeyComponent = (
							<RFlex styleProps={{ cursor: "pointer" }} onClick={() => setActiveCourseId(record?.[header].id)}>
								{header}
							</RFlex>
						);
						const key = record?.[header].type == "course" ? courseKeyComponent : header;
						const headerValue = record?.[header].type == "course" ? courseValueComponent : record?.[header].value;
						return {
							key: key,
							value: headerValue,
							type: record?.[header].type == "course" ? "component" : "",
							keyType: record?.[header].type == "course" ? "component" : "",
						};
					}),
				};
		  })
		: null;
	return (
		<RFlex className="flex-column" styleProps={{ gap: "25px" }}>
			<RFlex>
				{terms?.data?.data?.map((term) => (
					<div
						className={`${styles.TermButton} ${term?.id == activeTermId ? styles.ActiveTermButton : ""}`}
						style={{ gap: "15px", cursor: "pointer" }}
						onClick={() => setActiveTermId(term.id)}
					>
						{term?.name}
					</div>
				))}
			</RFlex>
			<RFlex styleProps={{ width: "100%" }}>{isLoadingTermData ? <Loader /> : <RLister Records={_termData} />}</RFlex>
			{isLoadingStudentDetailsSlider && studentDetailsSliderFetchStatus != "idle" ? (
				<Loader />
			) : activeCourseId ? (
				studentTermState?.courseDetails?.groups?.ids.length > 0 ? (
					<RFlex className="flex-column " styleProps={{ gap: "30px" }}>
						{studentTermState?.courseDetails?.groups?.ids?.map((id, index) => {
							const currentGroup = studentTermState?.courseDetails?.groups?.[id];
							return (
								<RGroupItem
									isStudent={true}
									groupId={id}
									studentId={studentId}
									activeCollapse={openGroupCollapseId == id}
									groupCollapseDetails={currentGroup}
									handleToggleGroupCollapse={handleToggleGroupCollapse}
								/>
							);
						})}
					</RFlex>
				) : (
					<REmptyData />
				)
			) : (
				""
			)}
			{!activeCourseId && (
				<div className="d-flex flex-column align-items-center" style={{ position: "absolute", right: "10%", top: "37%" }}>
					<img src={clickCourse} style={{ width: "250px", height: "250px", position: "relative" }} />
					<p className="p-0 m-0" style={{ fontSize: "21px", width: "200px", textAlign: "center", fontWeight: "600" }}>
						Click On Course Name To See Details
					</p>
				</div>
			)}
		</RFlex>
	);
};

export default GStudentTerm;
