import React, { useContext } from "react";
import { Form, FormGroup, Input, Row, Col } from "reactstrap";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import RButton from "components/Global/RComs/RButton";
import styles from "../../CoursesManager.module.scss";
import tr from "components/Global/RComs/RTranslator";

const RAddCourseModule = ({ edit }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<Row>
			<Col md={8}>
				<Form
					onSubmit={(event) => {
						event.preventDefault();
					}}
				>
					<FormGroup>
						<Input
							type="text"
							placeholder={tr`module_title`}
							onChange={(event) => {
								event.preventDefault();
								CourseModuleData.handleChangeCourseModule(
									edit
										? { value: event.target.value }
										: {
												value: event.target.value,
												order: CourseModuleData?.courseModule?.modules?.length + 1 ?? 1,
										  }
								);
							}}
							onKeyDown={(event) => {
								if (event.key === "Enter" && event.target.value.length > 2) {
									edit ? CourseModuleData.handleEditModule() : CourseModuleData.handleCreateModule();
								}
							}}
							value={CourseModuleData.moduleData?.title}
						/>
					</FormGroup>
				</Form>
			</Col>
			<Col md={4} className={styles.add_module_buttons}>
				<RButton
					text={tr`save`}
					onClick={edit ? CourseModuleData.handleEditModule : CourseModuleData.handleCreateModule}
					color="primary"
					loading={CourseModuleData.createModuleLoading}
					disabled={CourseModuleData.moduleData?.title.length < 2 || CourseModuleData.createModuleLoading}
				/>
				<RButton
					text={tr`cancel`}
					onClick={edit ? CourseModuleData.handleCloseEditModuleForm : CourseModuleData.handleCloseCreateModuleForm}
					color="link"
					disabled={CourseModuleData.createModuleLoading}
				/>
			</Col>
		</Row>
	);
};

export default RAddCourseModule;
