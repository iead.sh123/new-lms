import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Row, Col, Collapse } from "reactstrap";
import { primaryColor } from "config/constants";
import { dangerColor } from "config/constants";
import RLessonContent from "../RLessonContent";
import styles from "../../CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RAllModuleContent = () => {
	const CourseModuleData = useContext(CourseModuleContext);
	return (
		<div>
			{CourseModuleData?.courseModule &&
				CourseModuleData?.courseModule?.modules &&
				CourseModuleData?.courseModule?.modules?.map((module) => (
					<div
						key={module.id}
						onMouseEnter={() => CourseModuleData.showActionsOnHover(module.id)}
						onMouseLeave={() => CourseModuleData.showActionsOnHover(module.id)}
					>
						<section onClick={() => CourseModuleData.collapsesModuleToggle(module.id)} className={styles.module_section}>
							<Row>
								<Col md={3} xs={12} className={styles.col_module_content}>
									<i className="fa fa-folder" />
									<span> {module.title}</span>
								</Col>

								<Col md={6} xs={12} className="text-muted ">
									{Object.entries(CourseModuleData.studentView ? module.published_statistics : module.statistics)
										.map(([key, value]) => `${value} ${key[0] + key.substring(1).toLowerCase()}`)
										.join(" | ")}
								</Col>
								<Col md={3} xs={12} className="d-flex justify-content-end">
									<RFlex styleProps={{ gap: 20 }}>
										{!CourseModuleData.studentView && CourseModuleData.isHovered === "hover" + module.id && (
											<>
												<i
													className="fa fa-pencil"
													style={{ color: primaryColor }}
													onClick={() => {
														CourseModuleData.setSelectedButton(module.id);
														CourseModuleData.handleFillDataToSpecificModule(module);
														CourseModuleData.setModuleData({
															title: module.title,
															order: module.order_id,
														});
														CourseModuleData.handleCloseAllCollapses();
														CourseModuleData.handlePushToAnotherRoute({
															ModuleId: module.id,
														});
													}}
												/>
												<i
													className={iconsFa6.delete}
													style={{ color: dangerColor }}
													onClick={() => CourseModuleData.handleDelete(module.id)}
												/>
											</>
										)}
										<i
											className={
												CourseModuleData.openedModuleCollapses === "collapse" + module.id
													? "fa fa-chevron-down cursor-pointer"
													: "fa fa-chevron-right cursor-pointer"
											}
											style={{ color: primaryColor }}
										/>
									</RFlex>
								</Col>
							</Row>
						</section>

						<Collapse isOpen={CourseModuleData.openedModuleCollapses === "collapse" + module.id}>
							<section className={styles.lesson_module_section}>
								{module.contents && module.contents.length == 0 ? (
									<RFlex styleProps={{ gap: 25 }}>
										{CourseModuleData.studentView ? (
											<span className="text-muted">{tr`no content yet`}</span>
										) : (
											<span className="text-muted">{tr`no content yet, you can add content here`}</span>
										)}
										{CourseModuleData.studentView ? (
											""
										) : (
											<RFlex styleProps={{ color: primaryColor, cursor: "pointer" }}>
												<i className="fa fa-pencil mt-1 " />
												<span
													onClick={() => {
														CourseModuleData.handlePushToSpecificModule(module.id);
														// CourseModuleData.setSelectedButton(module.id);
														// CourseModuleData.handleFillDataToSpecificModule(module);
														// CourseModuleData.setModuleData({
														// 	title: module.title,
														// 	order: module.order_id,
														// });
													}}
												>{tr`edit_module`}</span>
											</RFlex>
										)}
									</RFlex>
								) : (
									module.contents.map((content) => (
										<Row key={content.id} className={styles.row_module_content}>
											<Col
												xs={3}
												className={styles.col_module_content}
												style={{
													padding: "12px 24px",
													cursor: content?.contents && content?.contents.length !== 0 ? "pointer" : "auto",
												}}
												onClick={() => CourseModuleData.collapsesContentToggle(content.id)}
											>
												{content?.type.toLowerCase() == "lesson" ? (
													<i className="fa fa-play-circle fa-lg " aria-hidden="true" />
												) : content?.type.toLowerCase() == "polls" ? (
													<i className="fa fa-bar-chart fa-lg" aria-hidden="true" />
												) : content?.type.toLowerCase() == "surveys" ? (
													<i className="fa fa-pencil-square fa-lg" aria-hidden="true" />
												) : content?.type.toLowerCase() == "assignments" ? (
													<i className="fa fa-eye fa-lg" aria-hidden="true" />
												) : content?.type.toLowerCase() == "quizzes" ? (
													<i className="fa fa-eye fa-lg" aria-hidden="true" />
												) : content?.type.toLowerCase() == "exams" ? (
													<i className="fa fa-question-circle fa-lg" aria-hidden="true" />
												) : content?.type.toLowerCase() == "projects" ? (
													<i className="fa fa-eye fa-lg" aria-hidden="true" />
												) : (
													""
												)}
												<span>{content.title}</span>
											</Col>
											{content?.contents && content?.contents.length !== 0 && (
												<>
													<Col
														xs={9}
														className="d-flex justify-content-end align-items-center cursor-pointer"
														onClick={() => CourseModuleData.collapsesContentToggle(content.id)}
													>
														<i
															className={
																CourseModuleData.openedContentCollapses === "collapse" + content.id
																	? "fa fa-chevron-down cursor-pointer"
																	: "fa fa-chevron-right cursor-pointer"
															}
															style={{ color: primaryColor }}
														/>
													</Col>
													<Col xs={12}>
														{CourseModuleData.openedContentCollapses == "collapse" + content.id &&
															content?.contents.map((lesson_content) => (
																<RLessonContent lessonContent={lesson_content} content={content} viewMode={true} />
															))}
													</Col>
												</>
											)}
										</Row>
									))
								)}
							</section>
						</Collapse>
					</div>
				))}
		</div>
	);
};

export default RAllModuleContent;
