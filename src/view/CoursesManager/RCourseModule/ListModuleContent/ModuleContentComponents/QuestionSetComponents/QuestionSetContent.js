import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Row, Col } from "reactstrap";
import { useState } from "react";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import tr from "components/Global/RComs/RTranslator";
import BasicInformation from "./Content/BasicInformation";
import Behavior from "./Content/Behavior";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { primaryColor } from "config/constants";
import moment from "moment";
import { DATE_FORMATE } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import { boldGreyColor } from "config/constants";

const QuestionSetContent = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	const Tabs = [{ title: `basic_information` }, { title: `behavior`, has__behavior: !content?.hasBehaviour }];
	const [tabActive, setTabActive] = useState(Tabs[0]?.title);

	return (
		<>
			{CourseModuleData.studentView ? (
				<RFlex styleProps={{ flexDirection: "column", width: "100%", justifyContent: "space-between", gap: 0 }}>
					<RFlex styleProps={{ flexDirection: "column", gap: 10, height: "100%", cursor: "default" }} className="p-4">
						<div>
							<span style={{ color: primaryColor, fontWeight: 600 }}>{tr`questions:`}&nbsp;</span>
							<span style={{ color: boldGreyColor }}>{content?.behavior?.questionset?.questionsCount}</span>
						</div>
						{content?.behavior?.dueDate && (
							<div>
								<span style={{ color: primaryColor, fontWeight: 600 }}>{tr`dueDate:`}&nbsp;</span>
								<span style={{ color: boldGreyColor }}>{moment(content?.behavior?.dueDate).format(DATE_FORMATE)}</span>
							</div>
						)}
						{content?.behavior?.cutoffDate && (
							<div>
								<span style={{ color: primaryColor, fontWeight: 600 }}>{tr`cutoffDate:`}&nbsp;</span>
								<span style={{ color: boldGreyColor }}>{moment(content?.behavior?.cutoffDate).format(DATE_FORMATE)}</span>
							</div>
						)}
						{/* <div>
              <span style={{ color: primaryColor,fontWeight:600 }}>{tr`attempts:`}&nbsp;</span>
              0 {tr`left_from`}
              {content?.behavior?.maxAttemptsAllowed}
            </div> */}
						<div>
							<span style={{ color: primaryColor, fontWeight: 600 }}>{tr`points:`}&nbsp;</span>
							<span style={{ color: boldGreyColor }}>{content?.behavior?.questionset?.points}</span>
						</div>
					</RFlex>

					<RFlex
						styleProps={{
							justifyContent: "end",
							padding: "0px 30px",
						}}
					>
						<RButton
							text={tr`go`}
							className="btn-round"
							color="primary"
							faicon={iconsFa6.rocket}
							onClick={() =>
								CourseModuleData.handlePushToStartSolveQuestionSet(
									content.content_id,
									content.id,
									content.type,
									content?.student_status?.isSubmitted
								)
							}
						/>
					</RFlex>
				</RFlex>
			) : (
				<>
					<Row style={{ width: "100%", display: "flex", alignItems: "center" }}>
						<Col xs={12} sm={8}>
							<RFilterTabs tabs={Tabs} setTabActive={setTabActive} />
						</Col>
						{/* <Col xs={12} sm={4} className="d-flex justify-content-end">
							<h6>
								{tr`go_to_advanced_settings`}
								<i className="fa-solid fa-arrow-right pt-1 pr-1"></i>
							</h6>
						</Col> */}
					</Row>
					<Row style={{ width: "100%" }}>
						{tabActive == "basic_information" ? (
							<Col xs={12}>
								<BasicInformation content={content} />
							</Col>
						) : (
							<Col xs={12}>
								<Behavior content={content} />
							</Col>
						)}
					</Row>
				</>
			)}
		</>
	);
};

export default QuestionSetContent;
