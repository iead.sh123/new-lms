import {
	INITIAL_DATA_TO_CREATE_POST,
	GET_VALIDATION_CREATE_POST,
	GET_HOME_PAGE_POSTS_REQUEST,
	GET_HOME_PAGE_POSTS_SUCCESS,
	GET_HOME_PAGE_POSTS_ERROR,
	SET_CREATE_POST_VALUE,
	SET_VALIDATION_TO_POST,
	SET_EDIT_CONTENT_VALUE,
	SET_CREATE_COMMENT,
	ADD_POST_REQUEST,
	ADD_POST_SUCCESS,
	ADD_POST_ERROR,
	EDIT_POST_REQUEST,
	EDIT_POST_SUCCESS,
	EDIT_POST_ERROR,
	REMOVE_POST_REQUEST,
	REMOVE_POST_SUCCESS,
	REMOVE_POST_ERROR,
	GET_POST_BY_ID_REQUEST,
	GET_POST_BY_ID_SUCCESS,
	GET_POST_BY_ID_ERROR,
	EMPTY_POSTS_ARRAY,
	EMPTY_PEOPLE_LIKES,
	SEARCH_IN_REQUEST,
	SEARCH_IN_SUCCESS,
	SEARCH_IN_ERROR,
	GET_ALL_CATEGORIES_REQUEST,
	GET_ALL_CATEGORIES_SUCCESS,
	GET_ALL_CATEGORIES_ERROR,
	CHOOSE_SPECIFIC_CATEGORY_REQUEST,
	CHOOSE_SPECIFIC_CATEGORY_SUCCESS,
	CHOOSE_SPECIFIC_CATEGORY_ERROR,
	GET_ALL_REPORT_CATEGORIES_REQUEST,
	GET_ALL_REPORT_CATEGORIES_SUCCESS,
	GET_ALL_REPORT_CATEGORIES_ERROR,
	SAVE_REPORT_ON_CONTENT_REQUEST,
	SAVE_REPORT_ON_CONTENT_SUCCESS,
	SAVE_REPORT_ON_CONTENT_ERROR,
	ACCEPT_CONTENT_REQUEST,
	ACCEPT_CONTENT_SUCCESS,
	ACCEPT_CONTENT_ERROR,
	REJECT_CONTENT_REQUEST,
	REJECT_CONTENT_SUCCESS,
	REJECT_CONTENT_ERROR,
	IGNORE_REPORTED_CONTENT_REQUEST,
	IGNORE_REPORTED_CONTENT_SUCCESS,
	IGNORE_REPORTED_CONTENT_ERROR,
	DELETE_REPORTED_CONTENT_REQUEST,
	DELETE_REPORTED_CONTENT_SUCCESS,
	DELETE_REPORTED_CONTENT_ERROR,
	DELETE_REJECTED_CONTENT_REQUEST,
	DELETE_REJECTED_CONTENT_SUCCESS,
	DELETE_REJECTED_CONTENT_ERROR,
	GET_CONTENT_BY_CONTENT_ID_REQUEST,
	GET_CONTENT_BY_CONTENT_ID_SUCCESS,
	GET_CONTENT_BY_CONTENT_ID_ERROR,
	EDIT_CONTENT_REQUEST,
	EDIT_CONTENT_SUCCESS,
	EDIT_CONTENT_ERROR,
	SET_REPORTING_ON_PORT,
	SAVE_REPORTING_ON_POST_REQUEST,
	SAVE_REPORTING_ON_POST_SUCCESS,
	SAVE_REPORTING_ON_POST_ERROR,
	EMPTY_REPORTING_ON_POST,
	GET_POSTS_CREATED_BY_SPECIFIC_USER_REQUEST,
	GET_POSTS_CREATED_BY_SPECIFIC_USER_SUCCESS,
	GET_POSTS_CREATED_BY_SPECIFIC_USER_ERROR,
	ADD_LIKE_ON_POET_OR_COMMENT_REQUEST,
	ADD_LIKE_ON_POET_OR_COMMENT_SUCCESS,
	ADD_LIKE_ON_POET_OR_COMMENT_ERROR,
	SEE_ALL_PEOPLE_WHO_LIKE_REQUEST,
	SEE_ALL_PEOPLE_WHO_LIKE_SUCCESS,
	SEE_ALL_PEOPLE_WHO_LIKE_ERROR,
	GET_ALL_MANDATORY_POSTS_POSTS_REQUEST,
	GET_ALL_MANDATORY_POSTS_POSTS_SUCCESS,
	GET_ALL_MANDATORY_POSTS_POSTS_ERROR,
	ADD_COMMENT_REQUEST,
	ADD_COMMENT_SUCCESS,
	ADD_COMMENT_ERROR,
	UPDATE_COMMENT_REQUEST,
	UPDATE_COMMENT_SUCCESS,
	UPDATE_COMMENT_ERROR,
	REMOVE_COMMENT_REQUEST,
	REMOVE_COMMENT_SUCCESS,
	REMOVE_COMMENT_ERROR,
	EMPTY_COMMENT,
	LOAD_PREVIOUS_COMMENTS_REQUEST,
	LOAD_PREVIOUS_COMMENTS_SUCCESS,
	LOAD_PREVIOUS_COMMENTS_ERROR,
	EMPTY_LOAD_PREVIOUS_COMMENT,
	EMPTY_POST_BY_ID,
	CHANGE_FLAG_SPECIFIC_CONTENT_LOAD,
	GET_COMMENT_BY_ID_REQUEST,
	GET_COMMENT_BY_ID_SUCCESS,
	GET_COMMENT_BY_ID_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { discussionsApi } from "api/global/discussions";
import { baseURL, genericPath } from "engine/config";
import { toast } from "react-toastify";

export const initialDataToCreatePost = (cohortId) => {
	const post = {
		title: "",
		text: "",
		private_comments: false,
		enable_comments: false,
		mandatory_comments: false,
		mandatory_replies: 0,
		cohort_id: null,
		attachments: [],
		for_community: cohortId ? false : true,
		mandatory_replies_check: false,
		cohort_id_obj: {},
	};
	return (dispatch) => {
		dispatch({
			type: INITIAL_DATA_TO_CREATE_POST,
			payload: { post },
		});
		if (cohortId) {
			dispatch(setCreatePostValues("cohort_id", cohortId));
		}
	};
};

export const getValidationCreatePost = () => {
	return (dispatch) => {
		const initialValidationData = {
			text: "",
			cohort_id: null,
		};
		dispatch({
			type: GET_VALIDATION_CREATE_POST,
			payload: { initialValidationData },
		});
	};
};

export const setCreatePostValues = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_CREATE_POST_VALUE,
			payload: { name, value },
		});
	};
};

export const setEditContentValues = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_EDIT_CONTENT_VALUE,
			payload: { name, value },
		});
	};
};

export const setReportingOnPost = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_REPORTING_ON_PORT,
			payload: { name, value },
		});
	};
};

export const setValidationToPost = ({ name, message }) => {
	return (dispatch) => {
		dispatch({
			type: SET_VALIDATION_TO_POST,
			payload: { name, message },
		});
	};
};

export const getHomePagePostsAsync = (pageNumber, contentId) => async (dispatch) => {
	dispatch({ type: GET_HOME_PAGE_POSTS_REQUEST });
	try {
		const response = await discussionsApi.getHomePagePosts(pageNumber, contentId);

		if (response.data.status === 1) {
			const loadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);

			dispatch({
				type: GET_HOME_PAGE_POSTS_SUCCESS,
				payload: {
					posts: response.data.data.posts.data,
					mostReactedPost: response.data.data.post_with_highest_interactions,
					mandatoryPosts: response.data.data.mandatory_posts,
					loadMorePage: loadMore,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_HOME_PAGE_POSTS_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_HOME_PAGE_POSTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const addPostAsync = (data, handleCloseAddPost, cohortId, searchQuery, history, sectionId) => async (dispatch) => {
	dispatch({ type: ADD_POST_REQUEST });
	try {
		const response = await discussionsApi.createPost(data);

		if (response.data.status === 1) {
			dispatch({
				type: ADD_POST_SUCCESS,
				payload: {},
			});
			if (searchQuery) {
				if (cohortId) {
					history.push(`${baseURL}/${genericPath}/section/${sectionId}/discussions/${cohortId}`);
				} else {
					history.push(`${baseURL}/${genericPath}/community`);
				}
			}
			handleCloseAddPost();
			dispatch(emptyPostsArray());
			dispatch(getHomePagePostsAsync(1, cohortId));
			dispatch(getAllCategoriesAsync(cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: ADD_POST_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: ADD_POST_ERROR,
		});
		toast.error(error?.message);
	}
};

export const editPostAsync =
	(data, postId, handleCloseAddPost, cohortId, searchQuery, history, sectionId, handleCloseSeeTheContentOfThePost) => async (dispatch) => {
		dispatch({ type: EDIT_POST_REQUEST });
		try {
			const response = await discussionsApi.updatePost(data, postId);

			if (response.data.status === 1) {
				dispatch({
					type: EDIT_POST_SUCCESS,
					payload: {},
				});
				if (searchQuery) {
					if (cohortId) {
						history.push(`${baseURL}/${genericPath}/section/${sectionId}/discussions/${cohortId}`);
					} else {
						history.push(`${baseURL}/${genericPath}/community`);
					}
				}
				handleCloseAddPost();
				handleCloseSeeTheContentOfThePost();
				dispatch(emptyPostsArray());
				dispatch(getHomePagePostsAsync(1, cohortId));
				dispatch(getAllCategoriesAsync(cohortId));
			} else {
				toast.error(response.data.msg);
				dispatch({
					type: EDIT_POST_ERROR,
				});
			}
		} catch (error) {
			dispatch({
				type: EDIT_POST_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const removePostAsync =
	(postId, hideAlert, cohortId, searchQuery, history, handleCloseSeeTheContentOfThePost) => async (dispatch) => {
		dispatch({ type: REMOVE_POST_REQUEST });
		try {
			const response = await discussionsApi.removePost(postId);

			if (response.data.status === 1) {
				dispatch({
					type: REMOVE_POST_SUCCESS,
				});
				if (searchQuery) {
					if (cohortId) {
						history.push(`${baseURL}/${genericPath}/discussions`);
					} else {
						history.push(`${baseURL}/${genericPath}/community`);
					}
				}
				hideAlert();
				handleCloseSeeTheContentOfThePost();
				dispatch(emptyPostsArray());
				dispatch(getHomePagePostsAsync(1, cohortId));
				dispatch(getAllCategoriesAsync(cohortId));
			} else {
				toast.error(response.data.msg);
				dispatch({
					type: REMOVE_POST_ERROR,
				});
			}
		} catch (error) {
			dispatch({
				type: REMOVE_POST_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const getPostByIdAsync = (postId) => async (dispatch) => {
	dispatch({ type: GET_POST_BY_ID_REQUEST });
	try {
		const response = await discussionsApi.getPostById(postId);

		if (response.data.status === 1) {
			const data = response.data.data.post_document;
			dispatch({
				type: GET_POST_BY_ID_SUCCESS,
				payload: { data },
			});
			dispatch(changeFlagSpecificContentLoad(true));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_POST_BY_ID_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_POST_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getCommentByIdAsync = (commentId) => async (dispatch) => {
	dispatch({ type: GET_COMMENT_BY_ID_REQUEST });
	try {
		const response = await discussionsApi.getCommentById(commentId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_COMMENT_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_COMMENT_BY_ID_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_COMMENT_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const searchInPostAsync = (text, pageNumber, cohortId) => async (dispatch) => {
	dispatch({ type: SEARCH_IN_REQUEST });
	try {
		const response = await discussionsApi.searchPost(text, pageNumber, cohortId);

		if (response.data.status === 1) {
			const loadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);

			dispatch({
				type: SEARCH_IN_SUCCESS,
				payload: {
					posts: response.data.data.posts.data,
					loadMorePage: loadMore,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: SEARCH_IN_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: SEARCH_IN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getAllCategoriesAsync = (cohortId) => async (dispatch) => {
	dispatch({ type: GET_ALL_CATEGORIES_REQUEST });
	try {
		const response = await discussionsApi.getAllCategories(cohortId);

		if (response.data.status === 1) {
			const data = response.data.data.post_categories;
			dispatch({
				type: GET_ALL_CATEGORIES_SUCCESS,
				payload: { data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_ALL_CATEGORIES_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_CATEGORIES_ERROR,
		});
		toast.error(error?.message);
	}
};

export const chooseSpecificCategoryAsync = (categoryId, pageNumber, cohortId) => async (dispatch) => {
	dispatch({ type: CHOOSE_SPECIFIC_CATEGORY_REQUEST });
	try {
		const response = await discussionsApi.chooseSpecificCategory(categoryId, pageNumber, cohortId);

		if (response.data.status === 1) {
			const loadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);

			dispatch({
				type: CHOOSE_SPECIFIC_CATEGORY_SUCCESS,
				payload: {
					posts: response.data.data.posts.data,

					loadMorePage: loadMore,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: CHOOSE_SPECIFIC_CATEGORY_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: CHOOSE_SPECIFIC_CATEGORY_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getAllReportCategoriesAsync = () => async (dispatch) => {
	dispatch({ type: GET_ALL_REPORT_CATEGORIES_REQUEST });
	try {
		const response = await discussionsApi.getAllReportCategories();

		if (response.data.status === 1) {
			const data = response.data.data.report_categories_reasons;
			dispatch({
				type: GET_ALL_REPORT_CATEGORIES_SUCCESS,
				payload: { data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_ALL_REPORT_CATEGORIES_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_REPORT_CATEGORIES_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveReportOnContentAsync = (contentId, data, handleCloseAddReport) => async (dispatch) => {
	dispatch({ type: SAVE_REPORT_ON_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.reportOnContent(contentId, data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_REPORT_ON_CONTENT_SUCCESS,
			});

			handleCloseAddReport();
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: SAVE_REPORT_ON_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: SAVE_REPORT_ON_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const acceptContentAsync = (contentId, categoryName, cohortId) => async (dispatch) => {
	dispatch({ type: ACCEPT_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.acceptContent(contentId, cohortId);

		if (response.data.status === 1) {
			dispatch({
				type: ACCEPT_CONTENT_SUCCESS,
			});

			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync(cohortId));
			dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: ACCEPT_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: ACCEPT_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const rejectContentContentAsync = (contentId, reason, categoryName, cohortId) => async (dispatch) => {
	dispatch({ type: REJECT_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.rejectContent(contentId, reason, cohortId);

		if (response.data.status === 1) {
			dispatch({
				type: REJECT_CONTENT_SUCCESS,
			});

			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync(cohortId));
			dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: REJECT_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: REJECT_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const ignoreReportedContentAsync = (contentId, categoryName, cohortId) => async (dispatch) => {
	dispatch({ type: IGNORE_REPORTED_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.ignoreReportedContent(contentId, cohortId);

		if (response.data.status === 1) {
			dispatch({
				type: IGNORE_REPORTED_CONTENT_SUCCESS,
			});

			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync(cohortId));
			dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: IGNORE_REPORTED_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: IGNORE_REPORTED_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const deleteReportedContentAsync = (contentId, categoryName, cohortId) => async (dispatch) => {
	dispatch({ type: DELETE_REPORTED_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.deleteReportedContent(contentId, cohortId);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_REPORTED_CONTENT_SUCCESS,
			});

			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync(cohortId));
			dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: DELETE_REPORTED_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: DELETE_REPORTED_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const deleteRejectedContentAsync = (contentId, categoryName, cohortId) => async (dispatch) => {
	dispatch({ type: DELETE_REJECTED_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.deleteRejectedContent(contentId, cohortId);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_REJECTED_CONTENT_SUCCESS,
			});

			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync(cohortId));
			dispatch(chooseSpecificCategoryAsync(categoryName, 1, cohortId));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: DELETE_REJECTED_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: DELETE_REJECTED_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getContentByContentIdAsync = (contentId) => async (dispatch) => {
	dispatch({ type: GET_CONTENT_BY_CONTENT_ID_REQUEST });
	try {
		const response = await discussionsApi.getContentByContentId(contentId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_CONTENT_BY_CONTENT_ID_SUCCESS,
				payload: { data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_CONTENT_BY_CONTENT_ID_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_CONTENT_BY_CONTENT_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const editContentAsync = (data, contentId, categoryName, handleCloseEditContent) => async (dispatch) => {
	dispatch({ type: EDIT_CONTENT_REQUEST });
	try {
		const response = await discussionsApi.editContent(contentId, data);

		if (response.data.status === 1) {
			dispatch({
				type: EDIT_CONTENT_SUCCESS,
			});

			handleCloseEditContent();
			dispatch(emptyPostsArray());
			dispatch(getAllCategoriesAsync());
			dispatch(chooseSpecificCategoryAsync(categoryName, 1));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: EDIT_CONTENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: EDIT_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveReportingOnPostAsync = (data, contentId, handleCloseAddReport) => async (dispatch) => {
	dispatch({ type: SAVE_REPORTING_ON_POST_REQUEST });
	try {
		const response = await discussionsApi.reportOnPost(contentId, data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_REPORTING_ON_POST_SUCCESS,
			});

			handleCloseAddReport();
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: SAVE_REPORTING_ON_POST_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: SAVE_REPORTING_ON_POST_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getPostsCreatedBySpecificUserAsync = (userId, pageNumber, cohortId) => async (dispatch) => {
	dispatch({ type: GET_POSTS_CREATED_BY_SPECIFIC_USER_REQUEST });
	try {
		const response = await discussionsApi.getPostsCreatedBySpecificUser(userId, pageNumber, cohortId);
		if (response.data.status === 1) {
			const loadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);

			dispatch({
				type: GET_POSTS_CREATED_BY_SPECIFIC_USER_SUCCESS,
				payload: {
					posts: response.data.data.posts.data,
					loadMorePage: loadMore,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_POSTS_CREATED_BY_SPECIFIC_USER_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_POSTS_CREATED_BY_SPECIFIC_USER_ERROR,
		});
		toast.error(error?.message);
	}
};

export const emptyPostsArray = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_POSTS_ARRAY,
			payload: { data: [] },
		});
	};
};

export const emptyReportingOnPost = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_REPORTING_ON_POST,
			payload: { data: {} },
		});
	};
};

export const emptyPeopleLikes = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_PEOPLE_LIKES,
			payload: { data: [] },
		});
	};
};

export const addLikeOnPostOrCommentAsync = (data) => async (dispatch) => {
	dispatch({ type: ADD_LIKE_ON_POET_OR_COMMENT_REQUEST });
	try {
		const response = await discussionsApi.addLikeOnPostOrComment(data);

		if (response.data.status === 1) {
			const likeCount = response.data.data.nb_likes;
			const isLiked = response.data.data.is_liked;

			dispatch({
				type: ADD_LIKE_ON_POET_OR_COMMENT_SUCCESS,
				payload: { likeCount, isLiked, data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: ADD_LIKE_ON_POET_OR_COMMENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: ADD_LIKE_ON_POET_OR_COMMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const seeAllPeopleWhoLikeAsync = (contentId, pageNumber) => async (dispatch) => {
	dispatch({ type: SEE_ALL_PEOPLE_WHO_LIKE_REQUEST });
	try {
		const response = await discussionsApi.loadAllPeople(contentId, pageNumber);

		if (response.data.status === 1) {
			const loadMoreLike =
				response.data.data.likes.next_page_url &&
				response.data.data.likes.next_page_url.substr(+response.data.data.likes.next_page_url.indexOf("=") + 1);

			dispatch({
				type: SEE_ALL_PEOPLE_WHO_LIKE_SUCCESS,
				payload: {
					seeAllPeople: response.data.data.likes.data,
					loadMoreLikePage: loadMoreLike,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: SEE_ALL_PEOPLE_WHO_LIKE_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: SEE_ALL_PEOPLE_WHO_LIKE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getAllMandatoryPostsAsync = (contentId, pageNumber) => async (dispatch) => {
	dispatch({ type: GET_ALL_MANDATORY_POSTS_POSTS_REQUEST });
	try {
		const response = await discussionsApi.getAllMandatoryPosts(contentId, pageNumber);

		if (response.data.status === 1) {
			const loadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);

			dispatch({
				type: GET_ALL_MANDATORY_POSTS_POSTS_SUCCESS,
				payload: {
					posts: response.data.data.posts.data,
					mostReactedPost: response.data.data.post_with_highest_interactions,
					mandatoryPosts: response.data.data.mandatory_posts,
					loadMorePage: loadMore,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_ALL_MANDATORY_POSTS_POSTS_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_MANDATORY_POSTS_POSTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const addCommentAsync = (data, cohortId, specificPost, postId, specificContent, typeContent, contentId) => async (dispatch) => {
	dispatch({ type: ADD_COMMENT_REQUEST });
	try {
		const response = await discussionsApi.addComment(data);

		if (response.data.status === 1) {
			dispatch({
				type: ADD_COMMENT_SUCCESS,
			});
			dispatch(emptyComment());
			if (specificPost) {
				dispatch(emptyPostById());
				dispatch(getPostByIdAsync(postId));
			} else if (specificContent) {
				dispatch(emptyLoadPreviousComment());
				dispatch(
					loadPreviousCommentsAsync({
						contentId: contentId,
						nextId: -1,
						type: typeContent,
						specificPost: false,
						specificContent: true,
					})
				);
				dispatch(changeFlagSpecificContentLoad(false));
			} else {
				dispatch(emptyPostsArray());
				dispatch(getHomePagePostsAsync(1, cohortId));
				dispatch(getAllCategoriesAsync(cohortId));
			}
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: ADD_COMMENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: ADD_COMMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const updateCommentAsync =
	(commentId, data, cohortId, specificPost, postId, specificContent, typeContent, contentId) => async (dispatch) => {
		dispatch({ type: UPDATE_COMMENT_REQUEST });
		try {
			const response = await discussionsApi.updateComment(commentId, data);

			if (response.data.status === 1) {
				dispatch({
					type: UPDATE_COMMENT_SUCCESS,
				});
				dispatch(emptyPostsArray());
				if (specificPost) {
					dispatch(emptyPostById());
					dispatch(getPostByIdAsync(postId));
				} else if (specificContent) {
					dispatch(emptyLoadPreviousComment());
					dispatch(
						loadPreviousCommentsAsync({
							contentId: contentId,
							nextId: -1,
							type: typeContent,
							specificPost: false,
							specificContent: true,
						})
					);
					dispatch(changeFlagSpecificContentLoad(false));
				} else {
					dispatch(getHomePagePostsAsync(1, cohortId));
					dispatch(getAllCategoriesAsync(cohortId));
				}
			} else {
				toast.error(response.data.msg);
				dispatch({
					type: UPDATE_COMMENT_ERROR,
				});
			}
		} catch (error) {
			dispatch({
				type: UPDATE_COMMENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const removeCommentAsync =
	(commentId, hideAlert, cohortId, searchQuery, history, specificPost, postId, specificContent, typeContent, contentId) =>
	async (dispatch) => {
		dispatch({ type: REMOVE_COMMENT_REQUEST });
		try {
			const response = await discussionsApi.removeComment(commentId);

			if (response.data.status === 1) {
				dispatch({
					type: REMOVE_COMMENT_SUCCESS,
				});
				if (searchQuery) {
					if (cohortId) {
						history.push(`${baseURL}/${genericPath}/discussions`);
					} else {
						history.push(`${baseURL}/${genericPath}/community`);
					}
				}
				if (specificPost) {
					dispatch(emptyPostById());
					dispatch(getPostByIdAsync(postId));
				} else if (specificContent) {
					dispatch(emptyLoadPreviousComment());
					dispatch(
						loadPreviousCommentsAsync({
							contentId: contentId,
							nextId: -1,
							type: typeContent,
							specificPost: false,
							specificContent: true,
						})
					);
					dispatch(changeFlagSpecificContentLoad(false));
				} else {
					hideAlert();
					dispatch(emptyPostsArray());
					dispatch(getHomePagePostsAsync(1, cohortId));
					dispatch(getAllCategoriesAsync(cohortId));
				}
			} else {
				toast.error(response.data.msg);
				dispatch({
					type: REMOVE_COMMENT_ERROR,
				});
			}
		} catch (error) {
			dispatch({
				type: REMOVE_COMMENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const loadPreviousCommentsAsync =
	({ contentId, nextId, type, specificPost, specificContent, typeSeeMore }) =>
	async (dispatch) => {
		dispatch({ type: LOAD_PREVIOUS_COMMENTS_REQUEST });
		try {
			const response = await discussionsApi.loadPreviousComments(contentId, nextId);
			const allData = response.data.data;
			const data = response.data.data.comments;
			const lastCommentId = response.data.data.last_comment_id;

			if (response.data.status === 1) {
				dispatch({
					type: LOAD_PREVIOUS_COMMENTS_SUCCESS,
					payload: {
						allData,
						data,
						lastCommentId,
						contentId,
						type,
						specificPost,
						specificContent,
						typeSeeMore,
					},
				});
			} else {
				toast.error(response.data.msg);
				dispatch({
					type: LOAD_PREVIOUS_COMMENTS_ERROR,
				});
			}
		} catch (error) {
			dispatch({
				type: REMOVE_COMMENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const setCreateCommentValues = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_CREATE_COMMENT,
			payload: { name, value },
		});
	};
};

export const emptyComment = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_COMMENT,
			payload: { data: {} },
		});
	};
};

export const emptyLoadPreviousComment = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_LOAD_PREVIOUS_COMMENT,
		});
	};
};

export const emptyPostById = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_POST_BY_ID,
		});
	};
};

export const changeFlagSpecificContentLoad = (flag) => {
	return (dispatch) => {
		dispatch({
			type: CHANGE_FLAG_SPECIFIC_CONTENT_LOAD,
			payload: { flag },
		});
	};
};
