import React, { useEffect } from "react";
import RLessonPlanViewer from "view/Courses/CourseManagement/LessonPlan/RLessonPlanViewer";
import Loader from "utils/Loader";
import { getLessonPlanByLessonPlanId } from "store/actions/teacher/lessonPlan.actions";
import { useDispatch, useSelector } from "react-redux";
import { getUnitPlanByIdAsync } from "store/actions/global/unitPlan.actions";
import { useParams } from "react-router-dom";
import RUnitPlanViewer from "view/UnitPlan/RUnitPlanViewer";

const GCollaborationViewer = ({ tableName, id, handleImport }) => {
	const { content_type } = useParams();
	const dispatch = useDispatch();

	useEffect(() => {
		if (tableName == "lesson_plans") {
			dispatch(getLessonPlanByLessonPlanId(id));
		} else if (tableName == "unit_plans") {
			dispatch(getUnitPlanByIdAsync(id));
		}
	}, []);

	const { lessonPlanLoading, lessonPlan } = useSelector((state) => state.lessonPlan);
	const { unitPlanLoading, unitPlan } = useSelector((state) => state.unitPlan);

	return (
		<div>
			{(tableName == "lesson_plans" ? lessonPlanLoading : tableName == "unit_plans" ? unitPlanLoading : false) ? (
				<Loader />
			) : (
				<>
					{tableName === "lesson_plans" ? (
						<RLessonPlanViewer
							lessonPlan={lessonPlan}
							removeAllActions={content_type ? false : true}
							importCollaboration={content_type ? () => handleImport(id) : null}
						/>
					) : (
						<RUnitPlanViewer
							unitPlan={unitPlan}
							removeAllActions={content_type ? false : true}
							importCollaboration={content_type ? () => handleImport(id) : null}
						/>
					)}
				</>
			)}
		</div>
	);
};

export default GCollaborationViewer;
