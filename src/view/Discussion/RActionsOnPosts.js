import React, { useContext } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useParams } from "react-router-dom";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import styles from "./RDiscussion.module.scss";

const RActionsOnPosts = ({ contentId }) => {
  const { cohortId, categoryName } = useParams();
  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );
  return (
    <>
      <RFlex
        styleProps={{
          justifyContent: "center",
          width: "100%",
        }}
        className="mb-3"
      >
        <RButton
          text={
            categoryName == "Pending"
              ? tr`accept`
              : categoryName == "Reported"
              ? tr`delete_content`
              : categoryName == "Rejected"
              ? tr`delete_content`
              : ""
          }
          color="success"
          style={{ width: "50%" }}
          outline
          onClick={() => {
            categoryName == "Pending"
              ? DiscussionData.handleAcceptContent(contentId)
              : categoryName == "Reported"
              ? DiscussionData.handleDeleteReportedContent(contentId)
              : categoryName == "Rejected"
              ? DiscussionData.handleDeleteRejectedContent(contentId)
              : "";
          }}
          loading={
            categoryName == "Pending"
              ? DiscussionData.acceptLoading
              : categoryName == "Reported"
              ? DiscussionData.deleteReportedLoading
              : categoryName == "Rejected"
              ? DiscussionData.deleteRejectedLoading
              : ""
          }
          disabled={
            categoryName == "Pending"
              ? DiscussionData.acceptLoading
                ? true
                : false
              : categoryName == "Pending"
              ? DiscussionData.deleteReportedLoading
                ? true
                : false
              : categoryName == "Rejected"
              ? DiscussionData.deleteReportedLoading
                ? true
                : false
              : ""
          }
        />
        <RButton
          text={
            categoryName == "Pending"
              ? tr`reject`
              : categoryName == "Reported"
              ? tr`ignore`
              : categoryName == "Rejected"
              ? tr`edit`
              : ""
          }
          color={categoryName == "Rejected" ? "primary" : "danger"}
          style={{ width: "50%" }}
          outline
          loading={
            categoryName == "Reported"
              ? DiscussionData.ignoreReportedLoading
              : ""
          }
          disabled={
            categoryName == "Reported"
              ? DiscussionData.ignoreReportedLoading
              : false
          }
          onClick={() => {
            categoryName == "Pending"
              ? DiscussionData.setOpenRejectReason(contentId)
              : categoryName == "Reported"
              ? DiscussionData.handleIgnoreReportedContent(contentId)
              : categoryName == "Rejected"
              ? DiscussionData.handleOpenEditContent(contentId)
              : "";
          }}
        />
      </RFlex>

      {DiscussionData.openRejectReason == contentId &&
        categoryName == "Pending" && (
          <Row>
            <Col xs={12} className="p-0">
              <FormGroup>
                <RFlex>
                  <Label>{tr`why did you reject this content ?`}</Label>
                  <i
                    className={`fa fa-question-circle-o ${styles.circle_icon}`}
                    aria-hidden="true"
                  ></i>
                </RFlex>
                <div style={{ position: "relative" }}>
                  <Input
                    name="mandatory_replies"
                    className=""
                    required
                    type="textarea"
                    placeholder={tr`write feedback to the author  `}
                    onChange={(event) =>
                      DiscussionData.setRejectedReason(event.target.value)
                    }
                    style={{ width: "100%" }}
                  />
                  <RButton
                    text={tr`submit`}
                    color="primary"
                    className={styles.submit_button}
                    onClick={() =>
                      DiscussionData.handleRejectContent(contentId)
                    }
                    loading={DiscussionData.rejectLoading}
                    disabled={DiscussionData.rejectLoading ? true : false}
                  />
                </div>
              </FormGroup>
            </Col>
          </Row>
        )}
    </>
  );
};

export default RActionsOnPosts;
