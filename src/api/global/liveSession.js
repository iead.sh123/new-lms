import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const liveSessionApi = {
  startMeeting: (meetingId) =>
    post(`${Services.meeting.backend}api/meeting/${meetingId}/start`),

  endMeeting: (meetingId) =>
    post(`${Services.meeting.backend}api/meeting/${meetingId}/end`),

  joinMeetingToGetAttendLink: (meetingId, userType) =>
    get(
      `${Services.meeting.backend}api/meeting/${meetingId}/link/${
        (userType == "student"||userType == "learner") ? "attendee" : "moderator"
      }`
    ),
};
