import { faPencilRuler } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { Services } from "engine/services";
import React,{ useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeStudentId } from "store/actions/global/auth.actions";
import { NavLink } from "react-router-dom";

const GStudentSwitch = () => {
const dispatch=useDispatch();

  const children=useSelector(s=>s.auth.user.extra.children)
  const selectedStudentId=useSelector(s=>s.auth.selectedStudentId)
  const changeSelectedStudentId=(sid)=>
  {
  dispatch(changeStudentId(sid));
  //window.location.reload();
  }
const float="left";

return <div  style={{marginLeft:"30px"}}>


children 
{children.map(c=>
    <div>
    <NavLink
      to="#"
      onClick={()=>{changeSelectedStudentId(c.id)}}
      style={selectedStudentId==c.id?
        {
       fontWeight: '800',
      textAlign: float,
      paddingBottom: "5px",
      margin: "0px"}
      :
     {
        textAlign: float,
        paddingBottom: "5px",
        margin: "0px",
      }}
    >
      <span className="sidebar-normal">{c.name}</span>
    </NavLink>
  </div>
     
    )}
   
</div>


};
export default GStudentSwitch;
