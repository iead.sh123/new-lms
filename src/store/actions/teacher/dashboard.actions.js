import { dashboardeApi } from "api/teacher/dashboard";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import {
	GET_MEETINGINFO,
	GET_LESSONINFO,
	EMPTY_STORE,
	GET_TEACHER_TODOLIST,
	GET_TEACHER_COURSES,
	GET_TEACHER_LIVESESSIONS,
	GET_TEACHER_EVENTS,
	GET_TEACHER_COLLABORATION,
	MARK_AS_DONE_REQUEST,
	MARK_AS_DONE_SUCCESS,
	MARK_AS_DONE_ERROR,
	GET_TEACHER_HOME_ROOM_REQUEST,
	GET_TEACHER_HOME_ROOM_SUCCESS,
	GET_TEACHER_HOME_ROOM_ERROR,
	GET_STUDENT_HOME_ROOM_REQUEST,
	GET_STUDENT_HOME_ROOM_SUCCESS,
	GET_STUDENT_HOME_ROOM_ERROR,
	GET_ATTENDANCE_TYPES_REQUEST,
	GET_ATTENDANCE_TYPES_SUCCESS,
	GET_ATTENDANCE_TYPES_ERROR,
	SET_ATTENDANCE_VALUES_TO_STUDENTS,
	GET_ATTENDANCE_VALUES_TO_STUDENTS,
	POST_SET_ATTENDANCE_REQUEST,
	POST_SET_ATTENDANCE_SUCCESS,
	POST_SET_ATTENDANCE_ERROR,
	SEARCH_STUDENTS_ATTENDANCE_REQUEST,
	SEARCH_STUDENTS_ATTENDANCE_SUCCESS,
	SEARCH_STUDENTS_ATTENDANCE_ERROR,
	DELETE_STUDENTS_ATTENDANCE_REQUEST,
	DELETE_STUDENTS_ATTENDANCE_SUCCESS,
	DELETE_STUDENTS_ATTENDANCE_ERROR,
} from "./teacherTypes";
import { toast } from "react-toastify";

export const getTeacherHomeRoom = () => async (dispatch) => {
	dispatch({ type: GET_TEACHER_HOME_ROOM_REQUEST });

	try {
		const response = await dashboardeApi.getTeacherHomeRoom();

		if (response.data.status === 1) {
			dispatch({
				type: GET_TEACHER_HOME_ROOM_SUCCESS,
				payload: { data: response.data.data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_TEACHER_HOME_ROOM_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_TEACHER_HOME_ROOM_ERROR,
		});
	}
};

export const getStudentHomeRoom = (homeRoomId) => async (dispatch) => {
	dispatch({ type: GET_STUDENT_HOME_ROOM_REQUEST });

	try {
		const response = await dashboardeApi.getStudentHomeRoom(homeRoomId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_STUDENT_HOME_ROOM_SUCCESS,
				payload: { StudentHomeRoom: response.data.data },
			});
			dispatch(getAttendanceTypes());
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_STUDENT_HOME_ROOM_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_STUDENT_HOME_ROOM_ERROR,
		});
	}
};

export const getAttendanceValuesToStudents = (ObjectIsDefault) => {
	return (dispatch) => {
		dispatch({
			type: GET_ATTENDANCE_VALUES_TO_STUDENTS,
			payload: { ObjectIsDefault },
		});
	};
};
export const setAttendanceValuesToStudents = (radioValue, studentId) => {
	return (dispatch) => {
		dispatch({
			type: SET_ATTENDANCE_VALUES_TO_STUDENTS,
			payload: { radioValue, studentId },
		});
	};
};

export const postSetAttendanceAsync = (date, attendance, homeRoomId) => async (dispatch) => {
	dispatch({ type: POST_SET_ATTENDANCE_REQUEST });

	try {
		const response = await dashboardeApi.postSetAttendance(date, attendance, homeRoomId);

		if (response.data.status === 1) {
			dispatch({
				type: POST_SET_ATTENDANCE_SUCCESS,
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: POST_SET_ATTENDANCE_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: POST_SET_ATTENDANCE_ERROR,
		});
	}
};

export const getAttendanceTypes = () => async (dispatch) => {
	dispatch({ type: GET_ATTENDANCE_TYPES_REQUEST });

	try {
		const response = await dashboardeApi.getAttendanceTypes();

		if (response.data.status === 1) {
			dispatch({
				type: GET_ATTENDANCE_TYPES_SUCCESS,
				payload: { AttendanceTypes: response.data.data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_ATTENDANCE_TYPES_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_ATTENDANCE_TYPES_ERROR,
		});
	}
};

export const searchStudentsAttendance = (data) => async (dispatch) => {
	dispatch({ type: SEARCH_STUDENTS_ATTENDANCE_REQUEST });

	try {
		const response = await dashboardeApi.searchStudentsAttendance(data);

		if (response.data.status === 1) {
			dispatch({
				type: SEARCH_STUDENTS_ATTENDANCE_SUCCESS,
				payload: { StudentAttendance: response.data.data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: SEARCH_STUDENTS_ATTENDANCE_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: SEARCH_STUDENTS_ATTENDANCE_ERROR,
		});
	}
};

export const deleteStudentsAttendance = (studentGID, studentQsID, studentName, studentDate, homeRoomId, formData) => async (dispatch) => {
	dispatch({ type: DELETE_STUDENTS_ATTENDANCE_REQUEST });

	try {
		const response = await dashboardeApi.deleteStudentAttendance(studentGID, studentQsID, studentName, studentDate, homeRoomId);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_STUDENTS_ATTENDANCE_SUCCESS,
			});
			dispatch(searchStudentsAttendance(formData));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: DELETE_STUDENTS_ATTENDANCE_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: DELETE_STUDENTS_ATTENDANCE_ERROR,
		});
	}
};

export const MeetingInfo = (LessonId) => {
	return async (dispatch) => {
		await dashboardeApi
			.meetingInfo(LessonId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_MEETINGINFO,
						payload: response.data,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const StartNewMeetingLive = (LessonId) => {
	return async (dispatch) => {
		await dashboardeApi
			.startNewMeeting(LessonId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch(MeetingInfo(LessonId));
					window.open(response.data.data, "_blank");
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const LessonInfo = (LessonId) => {
	return (dispatch) => {
		dashboardeApi
			.getLessonInfo(LessonId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_LESSONINFO,
						payload: response.data,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const EndMeetingLive = (LessonId) => {
	return () => {
		dashboardeApi
			.EndMeeting(LessonId)
			.then((response) => {
				dispatch(MeetingInfo(LessonId));
				if (response.data.status === 1) {
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const EndLessonLive = (LessonId) => {
	return (dispatch) => {
		dashboardeApi
			.EndLesson(LessonId)
			.then((response) => {
				dispatch(MeetingInfo(LessonId));
				dispatch(LessonInfo(LessonId));
				if (response.data.status === 1) {
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const emptyStore = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_STORE,
		});
	};
};

export const getTeacherTodolist = () => {
	return async (dispatch) => {
		await dashboardeApi
			.getToDoList()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_TEACHER_TODOLIST,
						payload: response.data.data.toDoList,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getTeacherCourses = () => {
	return async (dispatch) => {
		await dashboardeApi
			.getTeacherCourses()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_TEACHER_COURSES,
						payload: response.data.data.courses,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getTeacherLiveSessions = () => {
	return async (dispatch) => {
		await dashboardeApi
			.getTeacherLiveSessions()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_TEACHER_LIVESESSIONS,
						payload: response.data.data.liveSessions,
					});
				} else {
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getTeacherEvents = () => {
	return async (dispatch) => {
		await dashboardeApi
			.getTeacherEvents()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_TEACHER_EVENTS,
						payload: response.data.data.events,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getTeacherCollaborations = () => {
	return async (dispatch) => {
		await dashboardeApi
			.getTeacherCollaborations()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_TEACHER_COLLABORATION,
						payload: response.data.data.collaboration,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const markAsDone = (taskChecked, check) => {
	const request = {
		payload: {
			ids: taskChecked,
			check: check,
		},
	};
	if (taskChecked.length != 0) {
		return async (dispatch) => {
			dispatch({ type: MARK_AS_DONE_REQUEST });
			await dashboardeApi
				.markAsDone(request)
				.then((response) => {
					if (response.data.status === 1) {
						dispatch({ type: MARK_AS_DONE_SUCCESS });
						dispatch(getTeacherTodolist());
					} else {
						dispatch({ type: MARK_AS_DONE_ERROR });
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(response.data.msg);
				});
		};
	} else {
		return async (dispatch) => {
			toast.warning(`there is no task is checked !`);
		};
	}
};
