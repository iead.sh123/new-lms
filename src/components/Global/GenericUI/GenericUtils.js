import GenericUIAddEdit from "./GenericUIAddEdit";
import React, { useState } from "react";
import UsersAddEdit from "./UsersManagment/UsersAddEdit";
import GenericUIRelations from "./GenericUIManyToMany/GenericUIRelations";

const getTabsForEntity = (s) => {
  // const [state, setState] = useState({
  //   entyte: {
  //     types: [
  //       {
  //         name: "student",
  //         tabs: [
  //           { title: "student1", componentName: "hello" },
  //           { title: "student2", componentName: "hello" },
  //           { title: "student3", componentName: "hello" },
  //         ],
  //       },
  //       {
  //         name: "teacher",
  //         tabs: [
  //           { title: "teacher1", componentName: "hello" },
  //           { title: "teacher2", componentName: "hello" },
  //         ],
  //       },
  //       {
  //         name: "senior teacher",
  //         tabs: [
  //           { title: "senior teacher1", componentName: "hello" },
  //           { title: "senior teacher2", componentName: "hello" },
  //         ],
  //       },
  //     ],
  //   },
  // });
  // let tabs = [];
  // const elements = state.entyte.types.filter((el) => el.name == s);
  // if (elements) {
  //   elements.map((el) =>
  //     el.tabs.map(
  //       (ell) =>
  //         (tabs = [
  //           {
  //             title: ell.title,
  //             content: () => {
  //               return <UsersAddEdit />;
  //             },
  //           },
  //         ])
  //     )
  //   );
  // }
  // return tabs;

  let tabs = [];
  if (
    s == "students" ||
    s == "teachers" ||
    s == "employees" ||
    s == "principals" ||
    s == "school_advisors" ||
    s == "seniorteachers" ||
    s == "parents"
  )
    tabs = [
      {
        title: "Properties",
        content: () => {
          return <UsersAddEdit />;
        },
      },
    ];
  //if (s=="school_groups")
  else
    tabs = [
      {
        title: "Properties",
        content: () => {
          return <GenericUIAddEdit />;
        },
      },
    ];

  return tabs;
};
export default getTabsForEntity;
