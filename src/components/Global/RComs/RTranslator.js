import FlagEnglish from "assets/img/new/svg/flag-english.svg";
import FlagChina from "assets/img/new/svg/flag-china.svg";
import FlagArabic from "assets/img/new/svg/flag-arabic.svg";
export const languages = {
	selected: localStorage.getItem("language") || "english",
	list: {
		arabic: { name: "arabic", title: "ar", icon: `${FlagArabic}` },
		chinese: { name: "chinese", title: "cn", icon: `${FlagChina}` },
		english: { name: "english", title: "en", icon: `${FlagEnglish}` },
	},
};

const direction_mapping = {
	arabic: false,
	english: true,
	chinese: true,
};

const translator = {
	isInitialized: false,
	lang: languages.selected,
	direction: direction_mapping[languages.selected],
	dectionary: null,
	scopes: null,
	init: function (mapping) {
		this.dectionary = mapping;
		this.isInitialized = true;
		this.scopes = Object.keys(mapping);
		document.getElementsByTagName("html")[0].setAttribute("dir", direction_mapping[languages.selected] ? "ltr" : "rtl");
		document.getElementsByTagName("html")[0].setAttribute("lang", direction_mapping[languages.selected] ? "en" : "ar");
	},
};

const tr = (word, scope = "Global") => {
	try {
		const word2 = word.toString().toLowerCase().trim();

		if (!translator.isInitialized) {
			const mapping = require(`assets/translate/${translator.lang}.json`);

			//if there is a word with multiple latter case remove duplicate as we only compare with lower case
			for (let key in mapping) {
				for (let key2 in mapping[key]) {
					for (let key3 in mapping[key][key2]) {
						if (key3.toLowerCase() === key3) continue;
						mapping[key][key2][key3.toLowerCase()] = mapping[key][key2][key3];
						delete mapping[key][key2][key3];
					}
				}
			}

			translator.init(mapping);
		}

		if (translator.dectionary[scope][word2[0]][word2]) {
			return translator.dectionary[scope][word2[0]][word2];
		}
		for (let i = 0; i < translator.scopes.length; i++) {
			// scopes must be ordered by priority
			if (translator.dectionary[translator.scopes[i]][word2[0]][word2]) {
				return translator.dectionary[translator.scopes[i]][word2[0]][word2];
			}
		}
		// return translator.lang === "arabic" ? "تجريب" : word //just for rtl testing
		return word;
	} catch (err) {
		return word;
	}
};

export default tr;

export const swapLanguage = (lang) => {
	if (lang === translator.lang) {
		return;
	}

	localStorage.setItem("language", lang);
	localStorage.setItem("lang", lang == "english" ? "en" : "ar");
	window.location.reload();
};

export const direction = translator.direction;

export const lang = languages.selected;
