import { cn } from "lib/utils";
const RFlex = ({ children, className, styleProps, ref, ...props }) => {
	return (
		<section
			className={cn(`gap-[10px]`, className)}
			style={{
				display: "flex",
				...styleProps,
			}}
			ref={ref}
			{...props}
		>
			{children}
		</section>
	);
};

export default RFlex;
