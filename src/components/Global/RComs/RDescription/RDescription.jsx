import React from "react";
import RCkEditor from "../RCkEditor";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { FormGroup, FormText } from "reactstrap";
import { primaryColor } from "config/constants";

const RDescription = ({ value, handleChange, error, openCollapseValue, collapseToggle,defaultLabel=true }) => {
	return (
		<div>
			{defaultLabel&&
			<RFlex styleProps={{ color: primaryColor, cursor: "pointer", width: "fit-content" }} onClick={collapseToggle}>
				<i className="fa fa-plus pt-1" />
				<p>{tr`add_description`}</p>
			</RFlex>
			}
			{openCollapseValue && (
				<FormGroup>
					{defaultLabel && <label>{tr("description")}</label>}
					<RCkEditor data={value} handleChange={handleChange} />
					{error && <FormText color="danger">{value}</FormText>}
				</FormGroup>
			)}
		</div>
	);
};

export default RDescription;
