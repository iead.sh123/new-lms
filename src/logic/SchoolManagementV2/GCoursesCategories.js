import React, { useEffect, useState } from 'react'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader'
import RSelect from 'components/Global/RComs/RSelect'
import RLister from 'components/Global/RComs/RLister'
import * as actions from 'store/actions/global/schoolManagement.action'
import { useDispatch, useSelector } from 'react-redux'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import NewPaginator from 'components/Global/NewPaginator/NewPaginator'
import Loader from 'utils/Loader'
import RButton from 'components/Global/RComs/RButton'
const GCoursesCategories = ({ handleCloseCoursesModal, isModal }) => {
    const dispatch = useDispatch()
    const { courses, categories } = useSelector((state) => state.schoolManagementRed)
    const [currentCategoryId, setCurrentCategoryId] = useState(null)
    const { data } = courses

    const handleSelectCategory = (categoryId) => {
        dispatch(actions.getAllCourses(null, { category_id: categoryId.value, noPagination: true }, true))
        setCurrentCategoryId(categoryId)
    }

    const HandleSaveClicked = async () => {
        dispatch(actions.handleAddingCategory(currentCategoryId, data, courses.courseIds))
        handleCloseCoursesModal()
    }

    const getAllCategories = async () => {
        await dispatch(actions.getCouresesCategories())

    }
    useEffect(() => {
        dispatch(actions.getAllCourses(null, { category_id: currentCategoryId?.value, noPagination: true }, true))
        getAllCategories()
        return () => { }
    }, [])
    const _records = data ? courses.courseIds?.map((key) => {
        const couresNameComponent = <RProfileName
            name={data[key].name}
            img={data[key].image_id ? data[key].image_id : null}
            imgStyle={{ width: '18px', height: '18px', borderRaduis: '100%' }}
        />
        return {
            details: [
                { key: tr`course_name`, value: couresNameComponent, type: 'component' },
                { key: tr`course_code`, value: data[key]?.code ?? "-" },
                { key: tr`credit`, value: data[key].credits ?? "-" },
            ],
        }
    }) : null
    return (
        courses.loading ? < Loader /> :
            <RFlex styleProps={{ flexDirection: "column" }}>
                <RFlex styleProps={{ justifyContent: "space-between" }}>

                    <RFlex styleProps={{ width: '30%' }} >
                        <div style={{ width: "100%" }}>
                            <RSelect
                                option={categories}
                                closeMenuOnSelect={true}
                                value={currentCategoryId}
                                placeholder={tr`category`}
                                onChange={(e) => handleSelectCategory(e)}
                            />
                        </div>
                        <RButton text={tr("save")}
                            // faicon='fa-solid fa-plus'
                            disabled={currentCategoryId == null}
                            color="link"
                            className="text-primary m-0 p-0"
                            style={{ width: 'fit-content' }}
                            onClick={HandleSaveClicked} />
                    </RFlex>
                </RFlex>
                {_records && <RLister Records={_records} />}
                {/* <NewPaginator
                    firstPageUrl={first_page_url}
                    lastPageUrl={last_page_url}
                    currentPage={current_page}
                    lastPage={last_page}
                    prevPageUrl={prev_page_url}
                    nextPageUrl={next_page_url}
                    total={total}
                    data={{ category_id: currentCategoryId?.value }}
                    getAction={actions.getAllCourses}
                /> */}
            </RFlex>
    )
}

export default GCoursesCategories