import React, { useState } from "react";
import { useHistory, useParams } from "react-router-dom";

import {
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Row,
  Col,
} from "reactstrap";
import Switch from "react-bootstrap-switch";
import "./RTabs.css";
import RreactstrapSwitch from "./RreactstrapSwitch";
import tr from "./RTranslator";

const RTabsPanel = ({
  title,
  Tabs,
  SelectedIndex,
  changeHorizontalTabs,
  Url,
  hideToggle,
  subTitle = false,
  titleStyle,
  omitDefaultClasses,
  ref,
}) => {
  const { tabTitle } = useParams();
  const [horizontalTabs, setHorizontalTabs] = useState(
    tabTitle && !subTitle ? tabTitle : SelectedIndex
  );
  const [rTabPanelMode, setRTabPanelMode] = useState(true);
  const changeTab = (title, index) => {
    setHorizontalTabs(title);
    changeHorizontalTabs(index);
  };
  const history = useHistory();

  return (
    <div className="content">
      {/* {hideToggle ? null : (
        <Row>
          <Col style={{ display: "flex", justifyContent: "end" }}>
            <RreactstrapSwitch
              defaultValue={true}
              offText={<i className="fa fa-th" style={{ fontSize: 16 }} />}
              onText={<i className="fa fa-table" style={{ fontSize: 16 }} />}
              onColor="default"
              offColor="default"
              onChange={() => setRTabPanelMode(!rTabPanelMode)}
            />
          </Col>
        </Row>
      )} */}
      {rTabPanelMode == true ? (
        <Row className="scroll_hidden" style={{ height: "80vh" }}>
          <Col md="12">
            <Card>
              <CardHeader>
                <h5
                  className={omitDefaultClasses ? "" : "text-center"}
                  style={titleStyle ? titleStyle : null}
                >
                  {tr(title)}
                </h5>
              </CardHeader>
              <CardBody>
                <div className="nav-tabs-navigation">
                  <div className="nav-tabs-wrapper">
                    <Nav id="tabs" role="tablist" tabs>
                      {Tabs.map((tab, index) => {
                        return (
                          <NavItem key={index}>
                            <NavLink
                              aria-expanded={horizontalTabs === tab.title}
                              data-toggle="tab"
                              onMouseUp={() => {
                                history.replace(
                                  Url ? `${Url}${tab.title}` : "#"
                                );
                              }}
                              role="tab"
                              className={
                                horizontalTabs === tab.title
                                  ? "active font-weight-bold"
                                  : ""
                              }
                              onClick={() => {
                                changeTab(tab.title, index);
                                tab.ref;
                              }}
                              disabled={tab.disabled}
                            >
                              {tr(tab.title)}
                            </NavLink>
                          </NavItem>
                        );
                      })}
                    </Nav>
                  </div>
                </div>
                <TabContent
                  className="text-center"
                  id="my-tab-content"
                  activeTab={horizontalTabs}
                >
                  {Tabs.map((tab, index) => {
                    return (
                      <TabPane key={index} tabId={tab.title} role="tabpanel">
                        {tab.content()}
                      </TabPane>
                    );
                  })}
                </TabContent>
              </CardBody>
            </Card>
          </Col>
        </Row>
      ) : (
        <>
          <Row>
            <Col
              xl="12"
              lg="12"
              md="12"
              sm="12 "
              className="no-padding-no-margin "
            >
              <div className=" px-1 py-3  Curriculum_title justify-content-end">
                {title}
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={2} xs={12} className="no-padding-no-margin ">
              <div className="nav-tabs-navigation">
                <div className="tabs_horizontal">
                  {Tabs?.map((tab, index) => {
                    return (
                      <NavItem
                        key={index}
                        style={{ listStyleType: "none" }}
                        className="tab_padding"
                      >
                        <NavLink
                          aria-expanded={horizontalTabs === tab.title}
                          data-toggle="tab"
                          onMouseUp={() => {
                            history.replace(Url ? `${Url}${tab.title}` : "#");
                          }}
                          role="tab"
                          className={
                            horizontalTabs === tab.title
                              ? "active font-weight-bold"
                              : ""
                          }
                          style={{ color: "gray", cursor: "pointer" }}
                          onClick={() => changeTab(tab.title, index)}
                          disabled={tab.disabled}
                        >
                          {tr(tab.title)}
                          <i
                            className="fa fa-angle-right"
                            style={{ float: "right" }}
                          />
                        </NavLink>
                      </NavItem>
                    );
                  })}
                </div>
              </div>
            </Col>
            <Col xs={12} md={10}>
              <TabContent
                className="text-center tabs_horizontal tab_padding"
                id="my-tab-content"
                activeTab={horizontalTabs}
              >
                {Tabs.map((tab, index) => {
                  return (
                    <TabPane key={index} tabId={tab.title} role="tabpanel">
                      {tab.content()}
                    </TabPane>
                  );
                })}
              </TabContent>
            </Col>
          </Row>
        </>
      )}
    </div>
  );
};

export default RTabsPanel;
