import React, { useState, useEffect } from "react";
import {
	cloneCourseAsync,
	instantiateCourseAsync,
	removeCourseAsync,
	publishAndUnPublishCourseAsync,
} from "store/actions/global/coursesManager.action";
import { warningColor, successColor } from "config/constants";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import DefaultImage from "assets/img/new/course-default-cover.png";
import RCard from "components/Global/RComs/RCards/RCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import REmptyData from "components/RComponents/REmptyData";

const GCourseCard = ({ payloadData }) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const [courses, setCourses] = useState();
	const [alert, setAlert] = useState(false);
	const { categoryId } = useParams();

	const { coursesByCategory, cloneCourseLoading, instantiateCourseLoading, deleteCourseLoading } = useSelector(
		(state) => state.coursesManagerRed
	);

	useEffect(() => {
		buildCourses();
	}, [coursesByCategory]);

	const buildCourses = () => {
		let courses = [];
		coursesByCategory?.map((course, index) => {
			let data = {};

			data.id = course.id;
			data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
			data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;
			data.categoryName = course.category_name;
			data.color = course.color;
			data.name = course.name;
			data.users = course.teachers ? course.teachers : course.teachers;
			data.isFree = course?.extra?.isFree;
			data.startedAt = moment(course?.extra?.start_date).format("YYYY-MM-DD");
			data.isNew = course.isNew;
			data.isPublished = course.is_published;
			data.isCloned = course.is_cloned;
			data.isMaster = course.is_master;
			data.isOnline = course?.extra?.isOnline;
			data.isOneDayCourse = course.isOneDayCourse;
			data.comingSoon = course.comingSoon;
			data.discount = course.discount;
			data.newPrice = course.newPrice;
			data.oldPrice = course.oldPrice;
			data.isInstance = course.isInstance;

			data.actions = [
				{
					id: 0,
					name: course.is_published ? tr`unpublish` : tr`publish`,
					icon: course.is_published ? iconsFa6.pen : iconsFa6.check,
					color: course.is_published ? warningColor : successColor,
					hidden: course.is_master,
					onClick: () => {
						dispatch(publishAndUnPublishCourseAsync(course.id, course.category_id, course.is_published));
					},
				},
				{
					id: 1,
					name: tr`edit_course_information`,
					icon: iconsFa6.pen,

					onClick: () => {
						history.push(
							`${baseURL}/${genericPath}/courses-manager/editor/course/${course.id}${
								course.category_id ? `/category/${course.category_id}` : ""
							}/course-information`
						);
					},
				},
				{
					id: 2,
					name: tr`Clone the Course`,
					icon: iconsFa6.clone,
					onClick: () => {
						handleCloneCourse(course.id);
					},
					loading: cloneCourseLoading,
				},
				{
					id: 3,
					name: tr`Instantiate the Course`,
					icon: iconsFa6.copy,
					onClick: () => {
						handleInstantiateCourse(course.id);
					},
					loading: instantiateCourseLoading,
				},
				{
					id: 4,
					name: tr`delete`,
					icon: iconsFa6.delete,
					color: "red",
					onClick: () => {
						handleRemoveCourse(course.id);
					},
				},
			];
			data.link = course.category_id
				? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
				: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
			courses.push(data);
		});
		setCourses(courses);
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleCloneCourse = (courseId) => {
		dispatch(cloneCourseAsync(courseId, payloadData, categoryId));
	};

	const handleInstantiateCourse = (courseId) => {
		dispatch(instantiateCourseAsync(courseId, payloadData, categoryId));
	};

	const successDelete = async (prameters) => {
		dispatch(removeCourseAsync(prameters.courseId, hideAlert, payloadData?.isPublished, categoryId));
	};

	const handleRemoveCourse = (courseId) => {
		const prameters = {
			courseId,
		};
		const message = (
			<div>
				<h6>{tr`Are you sure? `}</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
			</div>
		);
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	return (
		<RFlex styleProps={{ flexWrap: "wrap", gap: 30 }}>
			{alert}
			{courses && courses?.length > 0 ? (
				courses.map((course) => <RCard course={course} key={course.id} />)
			) : (
				<RFlex styleProps={{ justifyContent: "center", alignItems: "center", width: "100%" }}>
					<REmptyData />
				</RFlex>
			)}
		</RFlex>
	);
};

export default GCourseCard;
