import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { primaryColor } from "config/constants";
import { Input } from "reactstrap";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RTextArea from "components/Global/RComs/RTextArea";
import RButton from "components/Global/RComs/RButton";
import styles from "../../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const TeacherFeedbackToAllQs = () => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <div className={styles.teacher__feedback}>
      <RFlex className="mb-3" styleProps={{ alignItems: "center" }}>
        <h6>{tr`teacher_mark`}</h6>{" "}
        <Input
          name="totalTeacherMark"
          type="number"
          min={0}
          max={QuestionSetSolveData?.finalMarkToStudent}
          // value={QuestionSetSolveData?.questionsCorrection?.totalTeacherMark}
          value={
            QuestionSetSolveData?.questionsCorrection?.totalTeacherMark == 0
              ? QuestionSetSolveData?.finalMarkToStudent
              : QuestionSetSolveData?.questionsCorrection?.totalTeacherMark
          }
          style={{ width: "85px" }}
          onChange={(event) => {
            QuestionSetSolveData.handleAddTeacherFeedbackToQs({
              key: event.target.name,
              value: event.target.value,
            });
          }}
        />
      </RFlex>
      <h6 className="text-muted">{tr`teacher mark is the final visible mark to students`}</h6>

      <div className="mt-4">
        <h6 style={{ color: primaryColor }}>{tr`add_feedback`}</h6>{" "}
        <RTextArea
          name="teacherFeedback"
          value={QuestionSetSolveData?.questionsCorrection?.teacherFeedback}
          onChange={(event) =>
            QuestionSetSolveData.handleAddTeacherFeedbackToQs({
              key: event.target.name,
              value: event.target.value,
            })
          }
        />
      </div>

      <RFlex
        styleProps={{
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: "60px",
        }}
      >
        <AppCheckbox
          onChange={(event) => {
            QuestionSetSolveData.handleAddTeacherFeedbackToQs({
              key: "sendNotification",
              value: event.target.checked,
            });
          }}
          label={<span>{tr`send notification to the student`}</span>}
        />
        <RButton
          text={tr`finish`}
          color="warning"
          outline
          faicon="fas fa-power-off"
          onClick={() => QuestionSetSolveData.handleCorrectionOfQuestions()}
          disabled={QuestionSetSolveData.correctionOfQuestions}
          loading={QuestionSetSolveData.correctionOfQuestions}
        />
      </RFlex>
    </div>
  );
};

export default TeacherFeedbackToAllQs;
