import React, { useState, useEffect, useReducer } from "react";
import Swal, { DANGER } from "utils/Alert";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { getUsersTypesByOrganizationAsync } from "../State/UsersAndPermissions.action";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import { get } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import Loader from "utils/Loader";
import styles from "../GUsersAndPermissions.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const UsersToPermissionLister = () => {
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	const { permissionId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);

	useEffect(() => {
		// getUsersTypesByOrganizationAsync(dispatch);
		getUsersTypesByOrganizationAsync({
			dispatch: dispatch,
		});
	}, []);

	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = async (emptySearch) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization_user/usersPermission/${permissionId}?filter=${
			emptySearch ?? searchData
		}`;
		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
		}
	};

	const handleFilterOnUserType = async (usersType) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization_user/usersPermission/${permissionId}${
			searchData ? `filter=${searchData}&user_type=${usersType.title}` : `?user_type=${usersType.title}`
		}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
		}
	};

	const handleFilterOnAccessibility = async (accessibility) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization_user/usersPermission/${permissionId}${
			searchData ? `filter=${searchData}&accessibility=${accessibility.title}` : `?accessibility=${accessibility.title}`
		}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/organization_user/usersPermission/${permissionId}`;
		let response = await get(specific_url ? specific_url : url);
		if (response?.data?.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		const data = response?.data?.data?.data.map((r) => ({
			id: r?.organization_user_id,
			table_name: "UsersToPermissionLister",
			details: [
				{
					key: tr`name`,
					value: {
						name: r.name,
						image: Services.auth_organization_management.file + r?.hash_id,
					},
					render: renderInfo,
				},
				{ key: tr`email`, value: r.email },
				{
					key: tr`user_type`,
					value: r.user_type,
					dropdown: true,
					dropdownData: usersAndPermissionsData.usersTypes,
					onClick: (usersTypeId) => {
						handleFilterOnUserType(usersTypeId);
					},
				},
				{
					key: tr`Accessibility`,
					value: r.accessibility,
					dropdown: true,
					dropdownData: [
						{ id: 1, title: "Both" },
						{ id: 2, title: "Individually" },
						{ id: 3, title: "By Role" },
					],
					onClick: (accessibility) => {
						handleFilterOnAccessibility(accessibility);
					},
				},
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords]
	);

	return (
		<React.Fragment>
			{usersAndPermissionsData.usersTypesLoading ? (
				<Loader />
			) : (
				<section className={"p-4 " + styles.box}>
					<RSearchHeader
						searchLoading={searchLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						inputPlaceholder={tr`search_a_user`}
						addNew={false}
					/>
					<RAdvancedLister {...propsLiterals.listerProps} />
				</section>
			)}
		</React.Fragment>
	);
};

export default UsersToPermissionLister;
