import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import placeholder from "assets/img/placeholder.jpg";
import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RImageStack from 'components/Global/RComs/RImageStack';
const EventCard = ({ icon, color, date, description, images, link }) => {



  return (
    <div style={{margin:"20px", width: '218px', height: '125px', backgroundColor: 'white', padding: '10px', borderRadius: '10px' }}>
      {/* First row */}
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', color: '#808080' }}>
        
          <div
            style={{
              backgroundColor: color,
              padding: '10px',
              borderRadius: '10px', // Updated border radius to 10px
              marginRight: '10px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <i className={icon} style={{ color: 'white' }} />
          </div>
          <div style={{justifySelf:"flex-end" ,fontSize:"10px" }}>
          {date}
          </div>
       
      </div>

      {/* Second row */}
      <div style={{ fontSize: '14px', fontWeight: 700, marginTop: '10px' }}>
        {description}
      </div>

      {/* Third row */}
      <div style={{ display: 'flex', marginTop: '10px', justifyContent: 'space-between' }}>
      <RImageStack images={images} imageWidth={40}/>
        <div style={{ width: '40px', display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          <a href={link} target="_blank" rel="noopener noreferrer">
            <i className="fa fa-chevron-right" style={{ color: 'black' }} />
          </a>
        </div>
      </div>
    </div>
  );
};

EventCard.propTypes = {
  icon: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
  link: PropTypes.string.isRequired,
};

export default EventCard;
