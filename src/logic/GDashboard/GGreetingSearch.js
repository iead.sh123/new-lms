import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';


import { useSelector } from 'react-redux';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText,InputGroup ,InputGroupAddon,Input  } from 'reactstrap';


const GGreetingSearch = ({ items, color ,size=30 }) => {

  const user=useSelector(s=>s.auth.user)
return       <Row className="mt-4">
<Col xs="6" md="7" style={{background:"#F9F9F9"}}>
  <div>
    👋 Hello, <span >{user?.full_name}</span>. <br /> It's so nice to have you back.
  </div>
</Col>
<Col xs="6" md="5">
  <InputGroup>
       <Input placeholder="Search..." />
          <InputGroupAddon addonType="prepend">
      <span className="input-group-text" style={{borderLeft:"0"}}>
        <FontAwesomeIcon icon={faSearch} />
      </span>
    </InputGroupAddon>

  </InputGroup>
</Col>
</Row>
}

export default GGreetingSearch;