import {
  GET_LESSONS_CONTENT_REQUEST,
  GET_LESSONS_CONTENT,
  GET_LESSONS_CONTENT_ERROR,
  ADD_BOOK_TO_LESSON,
  GET_LESSON_CONTENT,
  UPDATE_LESSONPAGEBOOK_REQUEST,
  UPDATE_LESSONPAGEBOOK_SUCCESS,
  UPDATE_LESSONPAGEBOOK_ERROR,
  SYNCHRONIZE_RAB_REQUEST,
  SYNCHRONIZE_RAB_SUCCESS,
  SYNCHRONIZE_RAB_ERROR,
} from "store/actions/teacher/teacherTypes";

const initState = {
  loading: true,
  books: [],
  lessonsContents: [],
  lessonsContent: {},
  RabContentIdDisaply: null,
  error: null,
  synchronizeRabLoading: false,
};
const lessonPageReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case SYNCHRONIZE_RAB_REQUEST:
      return {
        synchronizeRabLoading: true,
      };
    case SYNCHRONIZE_RAB_SUCCESS:
      return {
        synchronizeRabLoading: false,
      };
    case SYNCHRONIZE_RAB_ERROR:
      return {
        synchronizeRabLoading: false,
      };

    case UPDATE_LESSONPAGEBOOK_REQUEST:
      return {
        loading: true,
      };
    case UPDATE_LESSONPAGEBOOK_SUCCESS:
      return {
        ...state,
        RabContentIdDisaply: payload,
        loading: false,
      };
    case UPDATE_LESSONPAGEBOOK_ERROR:
      return {
        loading: false,
        error: payload,
      };

    case GET_LESSONS_CONTENT_REQUEST:
      return {
        loading: true,
      };

    case GET_LESSONS_CONTENT:
      return {
        ...state,
        lessonsContents: [...payload],
        loading: false,
      };

    case GET_LESSONS_CONTENT_ERROR:
      return {
        loading: false,
      };

    case GET_LESSON_CONTENT:
      return {
        ...state,
        lessonsContent: payload,
      };

    case ADD_BOOK_TO_LESSON:
      return [...state, state.lessonsContents.push(payload)];

    default:
      return state;
  }
};
export default lessonPageReducer;
