import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import { faChevronRight, faUserFriends, faUsers } from "@fortawesome/free-solid-svg-icons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import RRegularCard from "view/TermsManager/cards/RegularCard/RRegularCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import mainStyles from "../../TermManagement.module.scss";
import Loader from "utils/Loader";



const ManageTerms = ({ gradeLevels,selectedTerm , handleSelectgradeLevel,loading }) => {
    return <div>
        {loading ? <Loader /> : <React.Fragment>

        <div style={{ display: 'flex', justifyContent: 'start' }} className={mainStyles['headerText']}>
           <p>{selectedTerm?.['name']}</p>
        </div>

        <RFlex styleProps={{flexWrap: 'wrap'}}>
        {gradeLevels?.ids?.map(acId => <RRegularCard handleIconClicked={()=>handleSelectgradeLevel(acId)} key={'gradelevels-' + acId} id={acId} header={gradeLevels.byId[acId]?.title} icon={faChevronRight} items={[
            {
                text: tr('Students') +' : ' + gradeLevels.byId[acId]?.students,
                icon: faUserFriends
            },
            {
                text: tr(`Home Rooms`)+' : ' + gradeLevels.byId[acId]?.homerooms,
                icon: faUsers
            }
        ]} />)}
        </RFlex>
        </React.Fragment>}
    </div>

}



export default ManageTerms;