import React, { useContext } from "react";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Form, FormGroup, Input } from "reactstrap";
import styles from "./RDiscussion.module.scss";
import useWindowDimensions from "components/Global/useWindowDimensions";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { primaryColor } from "config/constants";

export const RDiscussionHeader = () => {
  const {
    searchQuery,
    categoryName,
    userId,
    cohortId,
    courseId,
    sectionId,
    mandatoryFlag,
  } = useParams();
  const { width } = useWindowDimensions();
  const mobile = width < 1000;
  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );
  const history = useHistory();

  return (
    <RFlex
      styleProps={{
        display: mobile ? "block" : "flex",
        width: "100%",
        alignItems: "center",
        justifyContent: "space-between",
        gap: 30,
      }}
      className="mb-4"
    >
      <RFlex styleProps={{ width: "100%" }}>
        <RButton
          text={
            (categoryName && cohortId) ||
            (categoryName && userId) ||
            (mandatoryFlag && cohortId)
              ? tr`back_to_discussion`
              : categoryName || (!categoryName && userId)
              ? tr`back_to_community`
              : tr`create_new_post`
          }
          faicon={categoryName || userId ? "fa fa-globe" : "fa fa-edit"}
          color="primary"
          onClick={() =>
            (categoryName && cohortId) ||
            (categoryName && userId) ||
            (mandatoryFlag && cohortId)
              ? history.push(
                  `${baseURL}/${genericPath}/section/${sectionId}/discussions/${cohortId}`
                )
              : categoryName || (!categoryName && userId)
              ? history.push(`${baseURL}/${genericPath}/community`)
              : DiscussionData?.handleOpenAddPost()
          }
        />
        <Form className={styles.form_input}>
          <FormGroup>
            <Input
              type="text"
              placeholder={tr`search`}
              className={styles.search_input}
              onChange={(event) =>
                DiscussionData?.setTextToSearch(event.target.value)
              }
              defaultValue={searchQuery ? searchQuery : ""}
            />
            <i
              aria-hidden="true"
              className={
                DiscussionData?.searchLoading
                  ? `fa fa-refresh fa-spin ${styles.search_input_icon}`
                  : `fa fa-search ${styles.search_input_icon}`
              }
              onClick={(event) => {
                event.preventDefault();
                if (cohortId) {
                  history.push(
                    `${baseURL}/${genericPath}/section/${sectionId}/search-discussion/${DiscussionData?.textToSearch}/${cohortId}`
                  );
                } else {
                  history.push(
                    `${baseURL}/${genericPath}/search-community/${DiscussionData?.textToSearch}`
                  );
                }

                DiscussionData?.handleSearch(DiscussionData?.textToSearch);
              }}
            />
          </FormGroup>
        </Form>
      </RFlex>

      <RFlex styleProps={{ gap: 15 }}>
        {DiscussionData?.categories.map((category, ind) => (
          <RFlex
            key={ind}
            styleProps={{ cursor: "pointer" }}
            onClick={() => {
              if (sectionId && cohortId) {
                history.push(
                  `${baseURL}/${genericPath}/section/${sectionId}/filter-discussion/${category.post_category}/${cohortId}`
                );
              } else {
                history.push(
                  `${baseURL}/${genericPath}/filter-community/${category.post_category}`
                );
              }
            }}
          >
            <span
              style={{
                color:
                  categoryName == category.post_category ? primaryColor : "",
              }}
            >
              {category.post_category}
            </span>
            <div className={styles.count}>
              <span
                style={{
                  fontSize: "12px",
                  color:
                    categoryName == category.post_category ? primaryColor : "",
                }}
              >
                {category.nb_posts}
              </span>
            </div>
          </RFlex>
        ))}
      </RFlex>
    </RFlex>
  );
};

export default RDiscussionHeader;
