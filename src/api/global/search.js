import { Services } from "engine/services";
import { get, post } from "config/api";

export const searchApi = {
	getSearch: (searchData) => get(`${Services.search.backend}api/search?term=${searchData}&from=0&size=10`),
};
