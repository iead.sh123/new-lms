import React from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import { useSelector } from "react-redux";
import styles from "./loaderSM.module.scss";

function LoaderSocialMedia() {
  const user = useSelector((state) => state?.auth?.user);

  return (
    <div
      className={
        user?.type === "student"||user?.type === "learner" || user?.type === "parent"
          ? styles.paddingTopLO
          : "content"
      }
    >
      <Row>
        <Col md="3"></Col>
        <Col md="6">
          <div className="animationLoading">
            <div id="container">
              <div id="one"></div>
              <div id="two"></div>
              <div id="three"></div>
            </div>
            <div id="four"></div>
            <div id="five"></div>
            <div id="six mb-5 pb-5"></div>
            <br /> <br /> <br /> <br />
            <div id="container mt-5 pt-5">
              <div id="one"></div>
              <div id="two"></div>
              <div id="three"></div>
            </div>
            <div id="four"></div>
            <div id="five"></div>
            <div id="six"></div>
          </div>
        </Col>
        <Col md="3"></Col>
      </Row>
    </div>
  );
}
export default LoaderSocialMedia;
