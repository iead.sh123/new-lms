/*!

=========================================================
* Paper Dashboard PRO React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

import { Doughnut, Pie, pie, Line, Bar } from "react-chartjs-2";

// reactstrap components
import { Card, CardBody, CardFooter, Row, Col, Badge } from "reactstrap";
import tr from "./RTranslator";

function RChart({ data, title }) {
  return (
    <div className="content" style={{padding:"20px"}}>
      {/* <div className="title-Details">
        {title ? <span className="ml-2 text-capitalize">{tr(title)}</span> : null}
      </div> */}
      {/* {title ?<h6>{tr(title)}</h6>:null} */}
      <Card  style={{
                borderLeft:"0 none",
                borderTop:"0 none",
                borderRight:"0 none",
                borderBottom:"0 none",
                boxShadow: "0 0px 0px 0px rgb(0 0 0 / 1%)",
                borderRadius:"0"}} 
                className="content  mt-3 pt-1 ">
        <CardBody>
          <Bar
            data={data}
            options={data}
            className="ct-chart ct-perfect-fourth"
          />
        </CardBody>
        <CardFooter>
          <hr />
          <Row>
            <Col sm="6">
              <div className="pull-left">
                <Badge style={{ backgroundColor: "#fd822b" }} pill>
                  {tr(data.labels[0])} : {data.datasets[0].data[0]}
                </Badge>
              </div>
            </Col>
            <Col sm="6">
              <div className="pull-right">
                <Badge style={{ backgroundColor: "#212529" }} pill>
                  {tr(data.labels[1])} : {data.datasets[0].data[1]}
                </Badge>
              </div>
            </Col>
          </Row>
        </CardFooter>
      </Card>
    </div>
  );
}

export default RChart;
