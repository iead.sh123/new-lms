import Helper from "components/Global/RComs/Helper";
import { produce } from "immer";
import { dataTypes } from "logic/SchoolManagement/constants";
import { types } from "logic/SchoolManagement/constants";
import { SELECT_PRINCIPAL } from "store/actions/global/globalTypes";
import { UN_SELECT_PRINCIPAL } from "store/actions/global/globalTypes";
import { REORDER_GRADE_LEVELS } from "store/actions/global/globalTypes";
import { UPDATE_EDUCATION_ORDER } from "store/actions/global/globalTypes";
import { UPDATE_CURRICULA_ORDER } from "store/actions/global/globalTypes";
import { SET_ACTIVE_COURSES } from "store/actions/global/globalTypes";
import { TOGGLE_COURSE_SELECTION } from "store/actions/global/globalTypes";
import { SELECT_CANDIDATE_PRINCIPAL } from "store/actions/global/globalTypes";
import { SELECT_CANDIDATE_STUDENT } from "store/actions/global/globalTypes";
import { UN_SELECT_CANDIDATE_STUDENT } from "store/actions/global/globalTypes";
import { GET_STUDENT_FOR_GRADE } from "store/actions/global/globalTypes";
import { ADD_STUDENTS_TO_GRADE } from "store/actions/global/globalTypes";
import { DELETE_PRINCIPALS_FOR_STAGE } from "store/actions/global/globalTypes";
import { TOGGLE_PERIOD_SAVED_STATUS } from "store/actions/global/globalTypes";
import { GET_CURRICULA_COURSES } from "store/actions/global/globalTypes";
import { EDIT_PERIOD } from "store/actions/global/globalTypes";
import { ADD_NEW_PERIOD } from "store/actions/global/globalTypes";
import { CHANGE_PERIOD_TO_TIME } from "store/actions/global/globalTypes";
import { SET_TO_TIME_TOUCHED } from "store/actions/global/globalTypes";
import { SET_GRADE_SCALE } from "store/actions/global/globalTypes";
import { SET_MIN_INPUT_SAVED } from "store/actions/global/globalTypes";
import { SET_GPA_INPUT_SAVED } from "store/actions/global/globalTypes";
import { SET_SCALE_INPUT_SAVED } from "store/actions/global/globalTypes";
import { SET_GRADE_ASSESSMENTS } from "store/actions/global/globalTypes";
import { SET_ASSESSMENT_TOUCHED } from "store/actions/global/globalTypes";
import { SET_ASSESSMENT_TIMESTAMP } from "store/actions/global/globalTypes";
import { TOGGLE_SELECT_MODE } from "store/actions/global/globalTypes";
import { TOGGLE_SELECT_MODE_OFF } from "store/actions/global/globalTypes";
import { FINISHING_PERIODS } from "store/actions/global/globalTypes";
import { FINISHING_GRADE_SCALE } from "store/actions/global/globalTypes";
import { FINISHING_PRINCIPALS } from "store/actions/global/globalTypes";
import { FINISHING_STUDENTS } from "store/actions/global/globalTypes";
import { FINISHING_ASSESSMENTS } from "store/actions/global/globalTypes";
import { GETTING_CANDIDATES_PRINCIPALS } from "store/actions/global/globalTypes";
import { FINISHING_CANDIDATES_STUDENTS } from "store/actions/global/globalTypes";
import { FINISHING_CURRICULA_CONTENT } from "store/actions/global/globalTypes";
import { FINISHING_COURSES } from "store/actions/global/globalTypes";
import { ADD_STUDENT_AFTER_CREATING } from "store/actions/global/globalTypes";
import { GETTING_SCHOOL_INFO } from "store/actions/global/globalTypes";
import { GETTING_SCHOOL_TREE } from "store/actions/global/globalTypes";
import { SAVE_PERIOD_INPUT } from "store/actions/global/globalTypes";
import { SAVE_ASSESSMENT_INPUT } from "store/actions/global/globalTypes";
import { SAVE_CATEGORY_AS_CURRICULA } from "store/actions/global/globalTypes";
import { SET_INVALIDATE_PERIOD_FALSE } from "store/actions/global/globalTypes";
import { SET_INVALIDATE_PERIOD_TRUE } from "store/actions/global/globalTypes";
import { UNSAVE_ASSESSMENT_INPUT } from "store/actions/global/globalTypes";
import { UNSAVE_PERIOD_INPUT } from "store/actions/global/globalTypes";
import { FINISHING_SCHOOL_TREE } from "store/actions/global/globalTypes";
import { FINISHING_SCHOOL_INFO } from "store/actions/global/globalTypes";
import { ADD_PRINCIAPLE_AFTER_CREATING } from "store/actions/global/globalTypes";
import { UPDATE_ASSESSMENT } from "store/actions/global/globalTypes";
import { GETTING_COURSES } from "store/actions/global/globalTypes";
import { GETTING_CURRICULA_CONTENT } from "store/actions/global/globalTypes";
import { GETTING_CANDIDATES_STUDENTS } from "store/actions/global/globalTypes";
import { FINISHING_CANDIDATES_PRINCIPALS } from "store/actions/global/globalTypes";
import { GETTING_ASSESSMENTS } from "store/actions/global/globalTypes";
import { GETTING_STUDENTS } from "store/actions/global/globalTypes";
import { GETTING_PRINCIPALS } from "store/actions/global/globalTypes";
import { GETTING_GRADE_SCALE } from "store/actions/global/globalTypes";
import { GETTING_PERIODS } from "store/actions/global/globalTypes";
import { TOGGLE_SELECT_MODE_ON } from "store/actions/global/globalTypes";
import { ADD_NEW_ASSESSMENT } from "store/actions/global/globalTypes";
import { DELETE_ASSESSMENT } from "store/actions/global/globalTypes";
import { CHANGE_ASSESSMENT_INPUT } from "store/actions/global/globalTypes";
import { SET_ASSESSMENT_SAVED } from "store/actions/global/globalTypes";
import { CHANGE_SCALE_INPUT_VALUE } from "store/actions/global/globalTypes";
import { SET_SCLAE_INPUT_TOUCHED } from "store/actions/global/globalTypes";
import { SET_MAX_INPUT_SAVED } from "store/actions/global/globalTypes";
import { SET_LETTER_INPUT_SAVED } from "store/actions/global/globalTypes";
import { SET_TITLE_TOUCHED } from "store/actions/global/globalTypes";
import { SET_FROM_TIME_TOUCHED } from "store/actions/global/globalTypes";
import { CHANGE_PERIOD_FROM_TIME } from "store/actions/global/globalTypes";
import { CHANGE_PERIOD_INPUT_VALUE } from "store/actions/global/globalTypes";
import { ADD_COURSE_TO_CURRICULA } from "store/actions/global/globalTypes";
import { DELETE_PERIOD } from "store/actions/global/globalTypes";
import { GET_ORGANIZATION_PERIODS } from "store/actions/global/globalTypes";
import { DELETE_STUDENTS_FROM_GRADE } from "store/actions/global/globalTypes";
import { GET_CANDIDATE_STUDENTS_FOR_GRADE } from "store/actions/global/globalTypes";
import { ADD_PRINCAPLE_TO_STAGE } from "store/actions/global/globalTypes";
import { UN_SELECT_CANDIDATE_PRINCIPAL } from "store/actions/global/globalTypes";
import { GET_CANDIDATE_PRINCIPALS } from "store/actions/global/globalTypes";
import { FETCHING_COURSES } from "store/actions/global/globalTypes";
import { SET_COURSES_CATEGORIES } from "store/actions/global/globalTypes";
import { UPDATE_GRADE_LEVEL_ORDER } from "store/actions/global/globalTypes";
import { REORDER_CURRICULAS } from "store/actions/global/globalTypes";
import { UN_SELECT_STUDENT } from "store/actions/global/globalTypes";
import { SELECT_STUDENT } from "store/actions/global/globalTypes";
import { CLEAR_ALL_STUDENTS } from "store/actions/global/globalTypes";
import { SELECT_ALL_STUDENTS } from "store/actions/global/globalTypes";
import { CLEAR_ALL_PRINCIPALS } from "store/actions/global/globalTypes";
import { SELECT_ALL_PRINCIPALS } from "store/actions/global/globalTypes";
import {
	GET_SCHOOL_INFO,
	EDIT_SCHOOL_INFO_SUCCESS,
	EDIT_SCHOOL_INFO_REQUEST,
	EDIT_SCHOOL_INFO_ERROR,
	GET_SCHOOL_TREE_REQUEST,
	GET_SCHOOL_TREE_ERROR,
	GET_SCHOOL_TREE_SUCCESS,
	SET_OPEN_STAGE_ID,
	SET_OPEN_GRADE_LEVEL_ID,
	SET_OPEN_CURRICULA,
	ADD_EDUCATION_STAGE,
	ADD_GRADE_LEVEL,
	ADD_CURRICULA,
	SAVE_EDUCATION_STAGE,
	SAVE_GRADE_LEVEL,
	SAVE_CURRICULA,
	DELETE_EDUCATION_STAGE,
	DELETE_GRADE_LEVEL,
	DELETE_CURRUICLA,
	DISABLE_ACTIVE_STAGE,
	DISABLE_ACTIVE_GRADE_LEVEL,
	DISABLE_ACTIVE_CURRICULA,
	GET_EDUCATION_PRINCIPALS,
	GET_DATA_FOR_TYPE,
	REORDER_EDUCATION_STAGES,
	FLAG_TO_CHECK_IF_WEEKLY_SCHEDULE_HAVE_EVENTS,
} from "store/actions/global/globalTypes";
import { addSectionToUnitPlan } from "store/actions/global/unitPlan.actions";

const InitState = {
	info: {
		name: '',
		address: '',
		url: '',
		email: '',
		image: null,
		gettingLoading: false,
		editingLoading: false
	},
	loading: false,
	typeLoading: false,
	sendingRequest: false,
	error: false,
	school: { stageIds: [], loading: true },
	stages: {},
	gradeLevels: { curriculaIds: [] },
	curriculas: {},
	principals: { ids: [], selectMode: false, loading: false },
	candidatePrincipals: { ids: [], loading: false },
	seniorTeachers: {},
	students: { ids: [], selectMode: false, loading: false },
	candidateStudents: { ids: [], loading: false },
	defaultGradeScaleId: null,
	defaultSemesterId: null,
	defaultMainCourseId: null,
	defaultSemesterName: null,
	openStageId: null,
	openGradLevelId: null,
	openCurriculaId: null,
	categories: null,
	courses: { courseIds: [], loading: false },
	periods: { ids: [], loading: false, invalidateTime: false },
	activeCourses: { ids: [], loading: false },
	gradeScale: { ids: [], loading: false },
	assessments: { ids: [], loading: false },
	weeklyScheduleHaveEvents: false,
}

const schoolManagementReducer = (state = InitState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case FLAG_TO_CHECK_IF_WEEKLY_SCHEDULE_HAVE_EVENTS:
				draft.weeklyScheduleHaveEvents = action.payload.flag;
				return draft;

			case GET_SCHOOL_INFO:
				draft.info = action?.payload?.info
				return draft
			case EDIT_SCHOOL_INFO_REQUEST:
				draft.info.editingLoading = true
				return draft
			case EDIT_SCHOOL_INFO_ERROR:
				draft.info.editingLoading = false
				return draft
			case EDIT_SCHOOL_INFO_SUCCESS:
				return {
					...draft,
					info: {
						...draft.info,
						address: action?.payload?.address,
						email: action?.payload?.contact_information,
						url: action?.payload?.facebook_page,
						image: action?.payload?.logo[0]?.url?.upload_id ?? action?.payload?.logo[0]?.hash_id,
						editingLoading: false,
						gettingLoading: false,
					},
					loading: false
				}
			case GETTING_SCHOOL_INFO:
				draft.info.gettingLoading = true
				return draft
			case FINISHING_SCHOOL_INFO:
				draft.info.gettingLoading = false
				return draft
			case GET_SCHOOL_TREE_REQUEST:
				draft.loading = true;
				return draft
			case GET_SCHOOL_TREE_ERROR:
				draft.loading = false
				draft.error = true
				return draft
			case GETTING_SCHOOL_TREE:
				draft.school.loading = true
				return draft
			case FINISHING_SCHOOL_TREE:
				draft.school.loading = false
				return draft
			case GET_SCHOOL_TREE_SUCCESS:
				draft.school = action?.payload?.school
				draft.stages = action?.payload?.stages
				draft.school.stageIds.sort((id1, id2) => draft.stages[id1].order - draft.stages[id2].order)
				draft.gradeLevels = action?.payload?.gradeLevels
				Object.keys(draft.stages).map((key, index) => draft.stages[key].gradeLevelIds.sort((id1, id2) => draft.gradeLevels[id1].grade_level_order - draft.gradeLevels[id2].grade_level_order))
				draft.curriculas = action?.payload?.curriculas
				Object.keys(draft.gradeLevels).map((key, index) => draft.gradeLevels[key].curriculaIds.sort((id1, id2) => draft.curriculas[id1].order - draft.curriculas[id2].order))
				draft.loading = false
				draft.info = {
					name: action?.payload?.school?.name,
					address: action?.payload?.school?.address,
					url: action?.payload?.school?.facebook_page,
					email: action?.payload?.school?.contact_information,
					image: action?.payload?.school?.logo,
					editingLoading: false,
					gettingLoading: false,
				}
				draft.defaultGradeScaleId = action?.payload?.defautlGradeScale?.id
				draft.defaultSemesterId = action?.payload?.defaultSemester?.id
				draft.defaultMainCourseId = action?.payload?.defaultMainCourse?.id
				draft.defaultSemesterName = action?.payload?.defaultSemester?.name
				if (Object.keys(draft.stages).length == 0) {
					draft.school.stageIds.push(0)
					draft.stages = { 0: { gradeLevelIds: [], order: 1 } }
				}
				return draft

			case SET_OPEN_STAGE_ID:
				draft.openStageId = draft.openStageId == action?.payload?.id ? null : action?.payload?.id;
				draft.openGradLevelId = null;
				draft.openCurriculaId = null;
				draft.principals = { ids: [], loading: false };
				draft.candidatePrincipals = { ids: [], loading: false };
				return draft;
			case SET_OPEN_GRADE_LEVEL_ID:
				draft.openGradLevelId = draft.openGradLevelId == action?.payload?.id ? null : action?.payload?.id;
				draft.openCurriculaId = null;
				draft.students = { ids: [], loading: false };
				draft.principals = { ...draft.principals, selectMode: false };
				draft.principals.ids.forEach((key) => {
					draft.principals[key].selected = false;
				});
				draft.assessments = { ids: [], loading: false };
				draft.candidateStudents = { ids: [], loading: false };
				return draft;
			case SET_OPEN_CURRICULA:
				draft.openCurriculaId = draft.openCurriculaId == action?.payload?.id ? null : action?.payload?.id;
				draft.activeCourses = { ids: [], loading: false };
				return draft;

			case DISABLE_ACTIVE_STAGE:
				draft.openStageId = null;
				return draft;
			case DISABLE_ACTIVE_GRADE_LEVEL:
				draft.openGradLevelId = null;
				return draft;
			case DISABLE_ACTIVE_CURRICULA:
				draft.openCurriculaId = null;
				return draft;
			case ADD_EDUCATION_STAGE:
				draft.stages = { ...draft?.stages, [action?.payload?.id]: { gradeLevelIds: [], order: action?.payload?.order } };
				draft.school.stageIds = [...draft.school.stageIds, action?.payload?.id];
				draft.openStageId = null
				return draft;
			case ADD_GRADE_LEVEL:
				draft.gradeLevels = {
					...draft?.gradeLevels,
					[action?.payload?.id]: {
						curriculaIds: [],
						education_stage_id: action?.payload?.educationStageId,
						grade_level_order: action?.payload?.order,
					},
				};
				draft.stages[action?.payload?.educationStageId].gradeLevelIds = [
					...draft.stages[action?.payload?.educationStageId].gradeLevelIds,
					action.payload.id,
				];
				draft.openGradLevelId = null

				return draft;
			case ADD_CURRICULA:
				draft.curriculas = {
					...draft?.curriculas,
					[action?.payload?.id]: { grade_level_id: action?.payload?.gradeLevelId, order: action?.payload?.order, master_courses: "[]" },
				};
				draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds = [
					...draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds,
					action?.payload?.id,
				];
				draft.openCurriculaId = null
				return draft;
			case SAVE_EDUCATION_STAGE:
				if (action?.payload?.oldId <= 0) {
					const index = draft.school.stageIds?.indexOf(action?.payload?.oldId);
					draft.school.stageIds.splice(index, 1);
				}
				if (!draft?.school?.stageIds?.includes(action?.payload?.id)) {
					draft.school.stageIds.push(action?.payload?.id);
				}
				draft.stages = {
					...draft?.stages,
					[action.payload.id]: { ...draft.stages[action?.payload?.oldId], id: action?.payload?.id, name: action?.payload?.name },
				};
				if (action?.payload?.oldId <= 0) {
					delete draft.stages[action?.payload?.oldId];
				}
				draft.openStageId = action.payload.id;
				draft.principals = { ids: [] };
				draft.candidatePrincipals = { ids: [] };
				draft.openGradLevelId = null;
				return draft;
			case SAVE_GRADE_LEVEL:
				if (action?.payload?.oldId <= 0) {
					const index = draft.stages[action?.payload?.stageId]?.gradeLevelIds?.indexOf(action?.payload?.oldId);
					draft.stages[action?.payload?.stageId]?.gradeLevelIds?.splice(index, 1);
				}
				if (!draft.stages[action?.payload?.stageId]?.gradeLevelIds.includes(action?.payload?.id)) {
					draft.stages[action?.payload?.stageId].gradeLevelIds.push(action?.payload?.id);
				}
				draft.gradeLevels = {
					...draft?.gradeLevels,
					[action?.payload?.id]: { ...draft.gradeLevels[action?.payload?.oldId], id: action?.payload?.id, title: action?.payload?.title },
				};
				if (action?.payload?.oldId <= 0) {
					draft.curriculas = {
						...draft?.curriculas,
						[action?.payload?.defaultCurricula.id]: {
							id: action?.payload?.defaultCurricula.id,
							name: action?.payload?.defaultCurricula.name,
							grade_level_id: action.payload.id,
							master_courses: "[]",
						},
					};
					draft.gradeLevels[action.payload.id].curriculaIds = [...draft.gradeLevels[action.payload.id].curriculaIds, action?.payload?.defaultCurricula.id]
					delete draft.gradeLevels[action?.payload?.oldId];
				}
				draft.openGradLevelId = action.payload.id;
				draft.students = { ids: [] };
				draft.candidateStudents = { ids: [] };

				return draft;

			case SAVE_CURRICULA:
				if (action?.payload?.oldId <= 0) {
					const index = draft.gradeLevels[action?.payload?.gradeLevelId]?.curriculaIds?.indexOf(action?.payload?.oldId);
					draft.gradeLevels[action?.payload?.gradeLevelId]?.curriculaIds?.splice(index, 1);
				}
				if (!draft.gradeLevels[action?.payload?.gradeLevelId]?.curriculaIds.includes(action?.payload?.id)) {
					draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds.push(action?.payload?.id);
				}
				draft.curriculas = {
					...draft?.curriculas,
					[action?.payload?.id]: {
						...draft.curriculas[action?.payload?.oldId],
						id: action?.payload?.id,
						name: action?.payload?.name,
						master_courses: action?.payload?.oldId <= 0 ? "[]" : draft.curriculas[action?.payload?.id].master_courses,
					},
				};
				if (action?.payload?.oldId <= 0) {
					delete draft.curriculas[action?.payload?.oldId];
					draft.activeCourses = { ids: [], loading: false };
				}

				draft.openCurriculaId = action.payload.id;

				return draft;
			case DELETE_EDUCATION_STAGE:
				delete draft.stages[action?.payload?.id];
				draft.school.stageIds = draft?.school?.stageIds?.filter((item) => item != action?.payload?.id);
				for (let key in draft.stages) {
					if (draft.stages.hasOwnProperty(key) && draft.stages[key].order > action.payload.order) {
						draft.stages[key].order -= 1;
					}
				}
				return draft;
			case DELETE_GRADE_LEVEL:
				delete draft.gradeLevels[action?.payload?.id];
				draft.stages[action?.payload?.stageId].gradeLevelIds = draft.stages[action?.payload?.stageId]?.gradeLevelIds.filter(
					(item) => item != action?.payload?.id
				);
				for (let key in draft.gradeLevels) {
					if (draft.gradeLevels.hasOwnProperty(key) && draft.gradeLevels[key].order > action.payload.order) {
						draft.gradeLevels[key].order -= 1;
					}
				}
				return draft;

			case DELETE_CURRUICLA:
				delete draft.curriculas[action?.payload?.id];
				draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds = draft.gradeLevels[
					action?.payload?.gradeLevelId
				]?.curriculaIds.filter((item) => item != action?.payload?.id);
				return draft;

			case REORDER_EDUCATION_STAGES:
				draft.school.stageIds = action?.payload?.ids;
				return draft;

			case UPDATE_EDUCATION_ORDER:
				action.payload.newOrder.map(({ id, order }) => (draft.stages[id].order = order));
				return draft;
			case REORDER_GRADE_LEVELS:
				draft.stages[action?.payload?.educationStageId].gradeLevelIds = action?.payload?.ids;
				return draft;
			case UPDATE_GRADE_LEVEL_ORDER:
				action.payload.newOrder.map(({ id, order }) => (draft.gradeLevels[id].grade_level_order = order));
				return draft;
			case REORDER_CURRICULAS:
				draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds = action?.payload?.ids;
				return draft;
			case UPDATE_CURRICULA_ORDER:
				action.payload.newOrder.map(({ id, order }) => (draft.curriculas[id].order = order));
				return draft;
			case GET_EDUCATION_PRINCIPALS:
				draft.principals = action?.payload?.principals;
				draft.typeLoading = false;
				return draft;
			case GET_CANDIDATE_PRINCIPALS:
				draft.candidatePrincipals = action?.payload?.candidatePrincipals;
				return draft;
			case GET_STUDENT_FOR_GRADE:
				draft.students = action.payload.students;
				return draft;
			case GET_CANDIDATE_STUDENTS_FOR_GRADE:
				draft.candidateStudents = action?.payload?.candidateStudents;
				return draft;

			case SELECT_ALL_PRINCIPALS:
				draft.principals.ids.map((key, index) => (draft.principals[key].selected = true));
				return draft;
			case SELECT_ALL_STUDENTS:
				draft.students.ids.map((key, index) => (draft.students[key].selected = true));
				return draft;
			case CLEAR_ALL_PRINCIPALS:
				draft.principals.ids.map((key, index) => (draft.principals[key].selected = false));
				return draft;
			case CLEAR_ALL_STUDENTS:
				draft.students.ids.map((key, index) => (draft.students[key].selected = false));
				return draft;
			case SELECT_PRINCIPAL:
				draft.principals[action?.payload?.id].selected = true;
				return draft;
			case SELECT_STUDENT:
				draft.students[action?.payload?.id].selected = true;
				return draft;
			case SELECT_CANDIDATE_PRINCIPAL:
				draft.candidatePrincipals[action.payload.id].selected = true;
				return draft;
			case SELECT_CANDIDATE_STUDENT:
				draft.candidateStudents[action?.payload?.id].selected = true;
				return draft;
			case UN_SELECT_PRINCIPAL:
				draft.principals[action?.payload?.id].selected = false;
				return draft;
			case UN_SELECT_STUDENT:
				draft.students[action?.payload?.id].selected = false;
				return draft;
			case UN_SELECT_CANDIDATE_PRINCIPAL:
				draft.candidatePrincipals[action.payload.id].selected = false;
				return draft;
			case UN_SELECT_CANDIDATE_STUDENT:
				draft.candidateStudents[action.payload.id].selected = false;
				return draft;
			case SET_COURSES_CATEGORIES:
				draft.categories = action.payload.categories;
				return draft;
			case FETCHING_COURSES:
				draft.courses.loading = true;
				return draft;
			case SET_ACTIVE_COURSES:
				draft.courses = action.payload.courses;
				return draft;
			case TOGGLE_COURSE_SELECTION:
				draft.courses.data[action.payload.courseId].selected = !draft.courses.data[action.payload.courseId].selected;
				return draft;
			case ADD_PRINCAPLE_TO_STAGE:
				action.payload.newIds.map((newId, index) => {
					//Here may be occure some problem
					draft.principals[newId] = { ...draft.candidatePrincipals[newId] };
					draft.principals[newId].selected = false;
					draft.principals.ids.push(newId);
				});
				return draft;
			case ADD_STUDENTS_TO_GRADE:
				action.payload.newIds.map((newId, index) => {
					//Here may be occure some problem
					draft.students[newId] = { ...draft.candidateStudents[newId] };
					draft.students[newId].selected = false;
					draft.students.ids.push(newId);
				});
				return draft;
			case DELETE_PRINCIPALS_FOR_STAGE:
				action.payload.deletedIds.map((deletedId, index) => {
					//Here may be occure some problem
					delete draft.principals[deletedId];
					draft.principals.ids = draft.principals.ids.filter((id, index) => deletedId != id);
				});
				return draft;
			case DELETE_STUDENTS_FROM_GRADE:
				action.payload.deletedIds.map((deletedId, index) => {
					//Here may be occure some problem
					delete draft.students[deletedId];
					draft.students.ids = draft.students.ids.filter((id, index) => deletedId != id);
				});
				return draft;
			case GET_ORGANIZATION_PERIODS:
				draft.periods = action.payload.periods;
				return draft;
			case TOGGLE_PERIOD_SAVED_STATUS:
				draft.periods[action.payload.id].saved = !draft.periods[action.payload.id].saved;
				return draft;

			case DELETE_PERIOD:
				delete draft.periods[action.payload.id];
				draft.periods.ids = draft.periods.ids.filter((id, index) => action.payload.id != id);
				return draft;
			case CHANGE_PERIOD_INPUT_VALUE:
				draft.periods[action.payload.id].title = action.payload.newTitle;
				return draft;
			case GET_CURRICULA_COURSES:
				draft.activeCourses = action?.payload?.activeCourses;
				return draft;
			case ADD_COURSE_TO_CURRICULA:
				draft.activeCourses[action?.payload?.newCourse.id] = action.payload.newCourse;
				draft.activeCourses["ids"].push(action.payload.newCourse.id);
				draft.curriculas[action.payload.curriculaId].master_courses = action.payload.master_courses;
				return draft;
			case ADD_NEW_PERIOD:
				const tempId = draft.periods.ids.length + 1;
				draft.periods[-tempId] = {
					id: -tempId,
					title: "",
					saved: false,
					invalidateTime: false,
					start_time: null,
					end_time: null,
					number: tempId,
					touched: { start_time: false, end_time: false, title: false },
				};
				draft.periods.ids.push(-tempId);
				return draft;
			case EDIT_PERIOD:
				if (action?.payload?.oldId <= 0) {
					delete draft.periods[action.payload.oldId];
					draft.periods[action?.payload?.newData?.id] = {};
					draft.periods.ids = draft.periods.ids.filter((id) => id != action.payload.oldId);
					draft.periods.ids.push(action?.payload?.newData?.id);
				}
				draft.periods[action?.payload?.newData?.id].id = action?.payload?.newData?.id;
				draft.periods[action?.payload?.newData?.id].number = action?.payload?.newData?.number;
				draft.periods[action?.payload?.newData?.id].title = action?.payload?.newData?.title;
				draft.periods[action?.payload?.newData?.id].start_time = action?.payload?.newData?.start_time.slice(0, 5);
				draft.periods[action?.payload?.newData?.id].end_time = action?.payload?.newData?.end_time.slice(0, 5);
				draft.periods[action?.payload?.newData?.id].saved = true;
				draft.periods[action?.payload?.newData?.id].invalidateTime = false;
				draft.periods[action?.payload?.newData?.id].touched = { start_time: false, end_time: false, title: true };
				return draft;
			case CHANGE_PERIOD_FROM_TIME:
				draft.periods[action.payload?.id].start_time = action?.payload?.newTime;
				return draft;
			case CHANGE_PERIOD_TO_TIME:
				draft.periods[action?.payload?.id].end_time = action?.payload?.newTime;
				return draft;
			case SET_FROM_TIME_TOUCHED:
				draft.periods[action?.payload?.id].touched["start_time"] = true;
				return draft;
			case SET_TO_TIME_TOUCHED:
				draft.periods[action?.payload?.id].touched["end_time"] = true;
				return draft;
			case SET_TITLE_TOUCHED:
				draft.periods[action?.payload?.id].touched["title"] = true;
				return draft;
			case SET_GRADE_SCALE:
				draft.gradeScale = action?.payload?.gradeScale;
				return draft;
			case CHANGE_SCALE_INPUT_VALUE:
				draft.gradeScale[action.payload.id][action.payload.type] = action?.payload?.value;
				return draft;
			case SET_SCALE_INPUT_SAVED:
				draft.gradeScale[action.payload.id]["saved"][action.payload.type] =
					!draft.gradeScale[action.payload.id]["saved"][action.payload.type];
				return draft;
			case SET_SCLAE_INPUT_TOUCHED:
				draft.gradeScale[action.payload.id]["touched"][action?.payload?.type] = true;
				return draft;
			case SET_GRADE_ASSESSMENTS:
				draft.assessments = action?.payload?.assessments;
				return draft;
			case SET_ASSESSMENT_SAVED:
				draft.assessments[action?.payload?.id].saved = !draft.assessments[action?.payload?.id].saved;
				return draft;
			case SET_ASSESSMENT_TOUCHED:
				draft.assessments[action.payload.id]["touched"][action?.payload?.type] = true;
				return draft;
			case CHANGE_ASSESSMENT_INPUT:
				draft.assessments[action?.payload?.id].name = action?.payload?.value;
				return draft;
			case SET_ASSESSMENT_TIMESTAMP:
				draft.assessments[action?.payload?.id].timestamp = action?.payload?.newValue;
				return draft;
			case DELETE_ASSESSMENT:
				delete draft.assessments[action?.payload?.id];
				draft.assessments.ids = draft.assessments.ids.filter((id, index) => id != action.payload.id);
				return draft;
			case ADD_NEW_ASSESSMENT:
				const tempAssessmentID = draft.assessments.ids.length + 1;
				draft.assessments[-tempAssessmentID] = {
					id: -tempAssessmentID,
					grade_level_id: action.payload.gradeLevelId,
					name: "",
					saved: false,
					timestamp: null,
					touched: { name: false, timestamp: false },
				};
				draft.assessments.ids.push(-tempAssessmentID);
				return draft;
			case TOGGLE_SELECT_MODE_ON:
				if (action?.payload?.type == "Principals") {
					draft.principals.selectMode = true;
				} else {
					draft.students.selectMode = true;
				}
				return draft;
			case TOGGLE_SELECT_MODE_OFF:
				if (action?.payload?.type == "Principals") {
					draft.principals.selectMode = false;
				} else {
					draft.students.selectMode = false;
				}
				return draft;
			case GETTING_PERIODS:
				draft.periods.loading = true;
				return draft;
			case FINISHING_PERIODS:
				draft.periods.loading = false;
				return draft;
			case GETTING_GRADE_SCALE:
				draft.gradeScale.loading = true;
				return draft;
			case FINISHING_GRADE_SCALE:
				draft.gradeScale.loading = false;
				return draft;
			case GETTING_PRINCIPALS:
				draft.principals.loading = true;
				return draft;
			case FINISHING_PRINCIPALS:
				draft.principals.loading = false;
				return draft;
			case GETTING_STUDENTS:
				draft.students.loading = true;
				return draft;
			case FINISHING_STUDENTS:
				draft.students.loading = false;
				return draft;
			case GETTING_ASSESSMENTS:
				draft.assessments.loading = true;
				return draft;
			case FINISHING_ASSESSMENTS:
				draft.assessments.loading = false;
				return draft;
			case GETTING_CANDIDATES_PRINCIPALS:
				draft.candidatePrincipals.loading = true;
				return draft;
			case FINISHING_CANDIDATES_PRINCIPALS:
				draft.candidatePrincipals.loading = false;
				return draft;
			case GETTING_CANDIDATES_STUDENTS:
				draft.candidateStudents.loading = true;
				return draft;
			case FINISHING_CANDIDATES_STUDENTS:
				draft.candidateStudents.loading = false;
				return draft;
			case GETTING_CURRICULA_CONTENT:
				draft.activeCourses.loading = true;
				return draft;
			case FINISHING_CURRICULA_CONTENT:
				draft.activeCourses.loading = false;
				return draft;
			case GETTING_COURSES:
				draft.courses.loading = true;
				return draft;
			case FINISHING_COURSES:
				draft.courses.loading = false;
				return draft;
			case UPDATE_ASSESSMENT:
				if (action?.payload?.oldId) {
					draft.assessments[action?.payload?.id] = {
						...draft.assessments[action?.payload?.oldId],
						...action.payload.otherData,
						id: action?.payload?.id,
					};
					draft.assessments[action?.payload?.id].saved = true
					draft.assessments.ids = draft.assessments.ids.filter((item) => item != action.payload.oldId);
					draft.assessments.ids.push(action.payload.id);
					delete draft.assessments[action.payload.oldId];
				}
				return draft;
			case ADD_STUDENT_AFTER_CREATING:
				draft.students[action.payload.student.id] = action.payload.student;
				draft.students.ids.push(action.payload.student.id);
				return draft;
			case ADD_PRINCIAPLE_AFTER_CREATING:
				draft.principals[action.payload.principale.id] = action.payload.principale;
				draft.principals.ids.push(action.payload.principale.id);
				return draft;
			case SAVE_PERIOD_INPUT:
				draft.periods[action.payload.id].saved = true
				return draft
			case UNSAVE_PERIOD_INPUT:
				draft.periods[action.payload.id].saved = false
				return draft
			case SAVE_ASSESSMENT_INPUT:
				draft.assessments[action.payload.id].saved = true
				return draft
			case UNSAVE_ASSESSMENT_INPUT:
				draft.assessments[action.payload.id].saved = false
				return draft
			case SAVE_CATEGORY_AS_CURRICULA:
				draft.curriculas = {
					...draft?.curriculas,
					[action?.payload?.curriculaId]: {
						id: action?.payload?.curriculaId,
						name: action?.payload?.category.label,
						master_courses: action.payload.master_courses,
					},
				};
				draft.gradeLevels[action?.payload?.gradeLevelId].curriculaIds.push(action?.payload?.curriculaId);
				draft.openCurriculaId = action.payload.curriculaId;

				draft.activeCourses = { ...action.payload.courses, ids: action.payload.coursesIds, loading: false }
				return draft
			case SET_INVALIDATE_PERIOD_TRUE:
				draft.periods[action.payload.id].invalidateTime = true
				return draft
			case SET_INVALIDATE_PERIOD_FALSE:
				draft.periods[action.payload.id].invalidateTime = false
				return draft
			default:
				return draft;
		}
	});

export default schoolManagementReducer;
