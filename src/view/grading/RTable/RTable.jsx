import RButton from "components/Global/RComs/RButton";
import RColor from "components/Global/RComs/RColor";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFormItem from "components/Global/RComs/RFormItem";
import { RLabel } from "components/Global/RComs/RLabel";
import RTableImage from "components/Global/RComs/RTableImage";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Col, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Button, Table } from "reactstrap";

const RTable = ({ Records, recordsArr, dir, hideTableHeader = false, firstCellImageProperty, align, noBorders = false, headers = [] }) => {
	const mapRecord = (detail, indexx) => {
		return (
			<td
				hidden={detail.hidden ? detail.hidden : false}
				key={"rd" + indexx}
				style={{
					minWidth: detail.minWidth ? detail.minWidth : "100px",
					paddingLeft: "20px",
					color: detail.color ?? "black",
					textAlign: detail.alignment ? detail.alignment : "left",
					borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
					...(detail.onClick ? { cursor: "pointer" } : {}),
				}}
				onClick={() => detail?.onClick?.()}
			>
				{["image", "main course image", "main course icon", "main course icon double", "main course svg", "logo"].includes(detail?.type) ? (
					<RTableImage detail={detail} />
				) : detail?.html || firstCellImageProperty ? (
					detail?.value
				) : detail?.type == "component" ? (
					<div> {detail?.value} </div>
				) : detail?.type == "color" ? (
					<RColor value={detail} disabled={true} />
				) : detail?.mode == "form" ? (
					<RFormItem data={detail} dir={dir} />
				) : detail.render ? (
					detail.render(detail.value)
				) : (
					<RLabel value={tr(detail?.value)} lettersToShow={50} dangerous={false} />
				)}
				{/* {Helper.js(detail?.type)} */}
			</td>
		);
	};

	const renderActions = (record, parent) => {
		return (
			record?.actions && (
				<td
					className="text-center"
					style={{
						borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
					}}
				>
					{record?.actions.length > 1 ? (
						<div className="d-flex justify-content-center">
							<UncontrolledDropdown direction="right">
								<DropdownToggle
									aria-haspopup={true}
									// caret
									color="default"
									data-toggle="dropdown"
									nav
								>
									<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer", color: "#333" }}></i>
								</DropdownToggle>
								<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
									{record?.actions?.map((action, index) => (
										<DropdownItem
											key={action.id ?? "ac" + record.id + index}
											disabled={action.disabled ? action.disabled : false}
											onClick={() => {
												action.onClick(record, parent);
											}}
											hidden={action.hidden ? action.hidden : false}
										>
											<RFlex
												styleProps={{
													alignItems: "center",
													justifyContent: "start",
													color: action.color && action.color,
												}}
											>
												<i className={action?.icon} aria-hidden="true" />
												<span>{action.name}</span>
											</RFlex>
										</DropdownItem>
									))}
								</DropdownMenu>
							</UncontrolledDropdown>
						</div>
					) : (
						<>
							{record?.actions?.map((action, index) => (
								<RButton
									hidden={action.permissions == false ? true : false}
									key={action.id ?? "ac" + record.id + index}
									className="btn-icon ml-1 mr-1"
									color={action.color ? action.color : "info"}
									size="sm"
									type="button"
									onClick={() => {
										action.onClick(record, parent);
									}}
									disabled={action.disabled ? action.disabled : false}
									outline={action.outline ? action.outline : false}
									faicon={action.faicon}
									text={action.text}
									loading={action.loading}
								/>
							))}
						</>
					)}
				</td>
			)
		);
	};

	return (
		<Col xs={12} className="p-0">
			<Table hover={!noBorders} responsive style={noBorders ? {} : { border: "1px solid #D9D9D9" }}>
				{!hideTableHeader && (
					<thead>
						<tr style={noBorders ? { border: "0px solid white" } : {}}>
							{headers?.map(({ label, value, type, headerColor }, index) => {
								return (
									<th
										key={"rhd" + index}
										style={{
											paddingLeft: "20px",
											border: noBorders ? "" : "1.5px solid #eee",
											color: headerColor,
										}}
										className="fixed_th"
									>
										<RFlex>{tr(label)}</RFlex>
									</th>
								);
							})}
							<th></th>
						</tr>
					</thead>
				)}

				<tbody>
					{Records?.map((record, index) => {
						return (
							<>
								<tr key={"rec" + index} style={noBorders ? { border: "0px solid white" } : {}}>
									{record?.details?.map(mapRecord)}
									{renderActions(record)}
								</tr>
								{record?.children?.map?.((ch, ind) => (
									<tr key={"rec" + ind} style={noBorders ? { border: "0px solid white" } : {}}>
										{ch?.details?.map(mapRecord)}
										{renderActions(ch, record)}
									</tr>
								))}
							</>
						);
					})}
				</tbody>
			</Table>
		</Col>
	);
};
export default RTable;
