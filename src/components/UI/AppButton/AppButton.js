import React from "react";
import styles from "./AppButton.Module.scss";
const AppButton = ({
  onClick,
  children,
  disabled,
  type = "primary",
  extraClasses = "",
}) => {
  return (
    <button
      onClick={onClick}
      className={`${styles["app-button"]} ${
        styles[`app-button-${type}`]
      } ${extraClasses}`}
      disabled = {disabled}
    >
      {children}
    </button>
  );
};

export default AppButton;
