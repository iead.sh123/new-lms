import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";
import RShare from "view/FileManagement/RMyFiles/RShare";
import Loader from "utils/Loader";
import { fileManagementApi } from "api/global/fileManagement";
import { useMutateData } from "hocs/useMutateData";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";

const GShare = ({ data, parentInvalidateKey, handlers }) => {
	const initialValues = {
		itemDownloadStatus: data.item?.download_available || false,
		itemStatus: data.item?.link?.is_public || false,
		itemPassword: data.item?.link?.password || "",
		sharingPrivacyToggle: false,
		usersToShared: [],
		usersSelected: [],
	};

	const { values, handleChange, setFieldValue } = useFormik({
		initialValues: initialValues,
	});
	/* - - - - - - - Queries - - - - - - - */

	const downloadFolderAvailableMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.downloadFolderAvailable({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemDownloadStatus", variables?.data.download_available);
		},
	});

	const downloadFileAvailableMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.downloadFileAvailable({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemDownloadStatus", variables?.data.download_available);
		},
	});

	const changeFolderStatusMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.changeFolderStatus({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemStatus", variables?.data.is_public);
		},
	});

	const changeFileStatusMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.changeFileStatus({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemStatus", variables?.data.is_public);
		},
	});

	const setPasswordToFolderMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.setPasswordToFolder({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemPassword", variables?.data.password);
		},
	});

	const setPasswordToFileMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.setPasswordToFile({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemPassword", variables?.data.password);
		},
	});

	const deletePasswordFromFolderMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.deletePasswordFromFolder({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			setFieldValue("itemPassword", "");
			setFieldValue("sharingPrivacyToggle", false);
		},
	});

	const deletePasswordFromFileMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.deletePasswordFromFile({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			setFieldValue("itemPassword", "");
			setFieldValue("sharingPrivacyToggle", false);
		},
	});

	const usersWhoMadeModifiedData = useFetchDataRQ({
		queryKey: ["usersToShared"],
		queryFn: () => fileManagementApi.usersToShared(),
		onSuccessFn: ({ data }) => {
			const users = data.data.map((user) => ({
				label: user?.first_name + user?.last_name,
				value: user?.id,
			}));
			setFieldValue("usersToShared", users);
		},
	});

	const addAccessToFolderMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.addAccessToFolder({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			setFieldValue("usersSelected", []);
			handlers.handleCloseShare();
		},
	});

	const addAccessToFileMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.addAccessToFile({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			setFieldValue("usersSelected", []);
			handlers.handleCloseShare();
		},
	});

	const folderAccessData = useFetchDataRQ({
		queryKey: ["folderAccess_Share", data.folderIds[0]],
		queryFn: () => fileManagementApi.folderAccess(data.folderIds[0]),
		enableCondition: values.usersToShared?.length > 0 && data.folderIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			const userMapping = values.usersToShared.filter((userToShared) => !data.data.some((user) => user.user_id === userToShared.value));
			setFieldValue("usersToShared", userMapping);
		},
	});

	const fileAccessData = useFetchDataRQ({
		queryKey: ["fileAccess_Share", data.fileIds[0]],
		queryFn: () => fileManagementApi.fileAccess(data.fileIds[0]),
		enableCondition: values.usersToShared?.length > 0 && data.fileIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			const userMapping = values.usersToShared.filter((userToShared) => !data.data.some((user) => user.user_id === userToShared.value));
			setFieldValue("usersToShared", userMapping);
		},
	});

	/* - - - - - - - Handle Sharing Privacy - - - - - - - */
	const handleSharingPrivacy = () => {
		setFieldValue("sharingPrivacyToggle", !values.sharingPrivacyToggle);
	};

	/* - - - - - - - Handle Suggest Password - - - - - - - */
	const handleSuggestPassword = () => {
		const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		let password = "";
		const length = 8; // Length of the password

		for (let i = 0; i < length; i++) {
			const randomIndex = Math.floor(Math.random() * characters.length);
			password += characters.charAt(randomIndex);
		}
		setFieldValue("itemPassword", password);
	};

	/* - - - - - - - Handle Change Download Status - - - - - - - */
	const handleChangeDownloadStatus = (event) => {
		if (data.fileIds.length > 0) {
			downloadFileAvailableMutate.mutate({ fileId: data.fileIds[0], data: { download_available: event } });
		} else {
			downloadFolderAvailableMutate.mutate({ folderId: data.folderIds[0], data: { download_available: event } });
		}
	};

	/* - - - - - - - Handle Change Status - - - - - - - */
	const handleChangeStatus = (event) => {
		if (data.fileIds.length > 0) {
			changeFileStatusMutate.mutate({ fileId: data.fileIds[0], data: { is_public: event } });
		} else {
			changeFolderStatusMutate.mutate({ folderId: data.folderIds[0], data: { is_public: event } });
		}
	};

	/* - - - - - - - Handle Set Password To Items - - - - - - - */
	const handleSetPasswordToItem = () => {
		if (data.fileIds.length > 0) {
			setPasswordToFileMutate.mutate({ fileId: data.fileIds[0], data: { password: values.itemPassword } });
		} else {
			setPasswordToFolderMutate.mutate({ folderId: data.folderIds[0], data: { password: values.itemPassword } });
		}
	};

	/* - - - - - - - Handle Delete Password From Items - - - - - - - */
	const handleDeletePasswordFromItems = () => {
		if (data.fileIds.length > 0) {
			deletePasswordFromFileMutate.mutate({ fileId: data.fileIds[0], data: { password: values.itemPassword } });
		} else {
			deletePasswordFromFolderMutate.mutate({ folderId: data.folderIds[0], data: { password: values.itemPassword } });
		}
	};

	/* - - - - - - - Handle Set Users - - - - - - - */
	const handleUsersSelected = (data) => {
		setFieldValue("usersSelected", data);
	};

	/* - - - - - - - Handle Add Users To Share - - - - - - - */
	const handleAddUsersToShare = () => {
		if (data.fileIds.length > 0) {
			addAccessToFileMutate.mutate({
				fileId: data.fileIds[0],
				data: {
					data: values.usersSelected.map((item) => ({
						user: item.value,
						permissions: [],
					})),
				},
			});
		} else {
			addAccessToFolderMutate.mutate({
				folderId: data.folderIds[0],
				data: {
					data: values.usersSelected.map((item) => ({
						user: item.value,
						permissions: [],
					})),
				},
			});
		}
	};
	if (
		usersWhoMadeModifiedData.isLoading ||
		(folderAccessData.isLoading && folderAccessData.fetchStatus !== "idle") ||
		(fileAccessData.isLoading && fileAccessData.fetchStatus !== "idle")
	)
		return <Loader />;
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RShare
				data={{ itemObj: data.item, values }}
				handlers={{
					handlers,
					handleSharingPrivacy,
					handleSuggestPassword,
					handleChangeDownloadStatus,
					handleChangeStatus,
					handleSetPasswordToItem,
					handleDeletePasswordFromItems,
					handleChange,
					handleUsersSelected,
					handleAddUsersToShare,
				}}
				loading={{
					downloadStatusLoading: downloadFolderAvailableMutate.isLoading || downloadFileAvailableMutate.isLoading,
					changeStatusLoading: changeFolderStatusMutate.isLoading || changeFileStatusMutate.isLoading,
					setPasswordLoading: setPasswordToFolderMutate.isLoading || setPasswordToFileMutate.isLoading,
					deletePasswordLoading: deletePasswordFromFolderMutate.isLoading || deletePasswordFromFileMutate.isLoading,
					addUsersToShareLoading: addAccessToFileMutate.isLoading || addAccessToFolderMutate.isLoading,
					dataInsideSelectLoading:
						usersWhoMadeModifiedData.isLoading ||
						usersWhoMadeModifiedData.isFetching ||
						(folderAccessData.isLoading && folderAccessData.fetchStatus !== "idle") ||
						(folderAccessData.isFetching && folderAccessData.fetchStatus !== "idle") ||
						(fileAccessData.isLoading && fileAccessData.fetchStatus !== "idle") ||
						(fileAccessData.isFetching && fileAccessData.fetchStatus !== "idle"),
				}}
			/>
		</RFlex>
	);
};

export default GShare;
