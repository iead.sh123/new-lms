import React from "react";
import styles from "./RResourceViewer.module.scss";
import powerPointIcon from "assets/img/png/powerpoint.png";
import excelIcon from "assets/img/png/excel.png";
import videoIcon from "assets/img/png/multimedia.png";
import audioIcon from "assets/img/png/sound.png";
import fileIcon from "assets/img/png/file.png";
import wordIcon from "assets/img/png/word.png";
import pdfIcon from "assets/img/png/pdf.png";

const fileTypes = [
	"eml",
	"log",
	"msg",
	"odt",
	"PAGES",
	"rtf",
	"txt",
	"wpd",
	"xml",
	"wav",
	"wav",
	"wmv",
	"json",
	"html",
	"htmlx",
	"js",
	"css",
	"php",
	"xhtml",
];
const fileExcel = ["xls", "xlsx", "csv", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
const fileWord = [
	"docx",
	"doc",
	"vnd.openxmlformats-officedocument.wordprocessingml.document",
	"application/vnd.openxmlformats-officedocument",
];
const filePowerPoint = ["ppt", "pptx", "vnd.openxmlformats-officedocument.presentationml.presentation"];
const filePdf = ["pdf", "application/pdf"];
const fileVideo = ["mp4", "mov"];
const fileAudio = ["mp3", "wav"];

const fileImages = ["jpg", "jpeg", "png", "gif"];

export const fileImage = (mimeType) => {
	return fileTypes.includes(mimeType)
		? fileIcon
		: fileWord.includes(mimeType)
		? wordIcon
		: fileExcel.includes(mimeType)
		? excelIcon
		: filePowerPoint.includes(mimeType)
		? powerPointIcon
		: filePdf.includes(mimeType)
		? pdfIcon
		: fileVideo.includes(mimeType)
		? videoIcon
		: fileAudio.includes(mimeType)
		? audioIcon
		: fileImages.includes(mimeType)
		? fileIcon
		: null;
};
const RFile = ({ name, src, mimeType }) => {
	const ext = src.split(".")[src.split(".").length - 1]?.toUpperCase();

	return (
		<a target="_blank" href={src} className={styles["Image"] + " " + styles["FileProxy"]}>
			<img src={fileImage(mimeType) ?? src} style={{ width: "100px", height: "100px", objectFit: "cover" }} />

			{/* {ext} */}
		</a>
	);
};

export default RFile;
