import React from "react";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";

const RCouponDetails = ({ handleSortBy, users }) => {
	const viewAsButton = (
		<RFlex
			id="view as"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr`sort_by`}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const viewAsFilterActions = [
		{ label: tr`alphabetically`, action: () => handleSortBy("alfa") },
		{ label: tr`recent`, action: () => handleSortBy("recent") },
	];

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ width: "100%", justifyContent: "flex-end" }}>
				<RDropdownIcon actions={viewAsFilterActions} component={viewAsButton} />
			</RFlex>

			<RFlex styleProps={{ flexWrap: "wrap" }}>
				{users?.map((user, index) => (
					<RProfileName
						key={user.id}
						img={user.image}
						name={`${user.firstName} ${user.lastName}`}
						flexStyleProps={{ width: "30%", gap: 5 }}
					/>
				))}
			</RFlex>
		</RFlex>
	);
};

export default RCouponDetails;
