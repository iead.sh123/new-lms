import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import React, { useState } from "react";
import * as colors from "config/constants";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLister from "components/Global/RComs/RLister";
import { RLabel } from "components/Global/RComs/RLabel";
import styles from "./sharedStyle.module.scss";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
const RUsersData = ({
	users,
	selectedUsers = [],
	selectOneFn,
	selectAllFn,
	userTypes,
	allMembersFilters,
	setAllMembersFilters,
	filterHeader = false,
	view = false,
	modal = false,
	deleteSingleMember,
	addUser,
	loadingIds,
	checkboxExists = true,
	changeSingleUserStatus,
	activeIdsLoading,
	inCreate = false,
	removeIdsLoading,
	disableRemoving = false,
}) => {
	const history = useHistory();
	const keysToShow = ["full_name", "user_types", "identifier", "email"];
	const genderActions = [
		{ title: "Male", onClick: () => setAllMembersFilters({ ...allMembersFilters, gender: "m" }) },
		{ title: "Female", onClick: () => setAllMembersFilters({ ...allMembersFilters, gender: "f" }) },
	];
	const activeActions = [
		{ title: "Active", onClick: () => setAllMembersFilters({ ...allMembersFilters, is_active: true }) },
		{ title: "Inactive", onClick: () => setAllMembersFilters({ ...allMembersFilters, is_active: false }) },
	];
	const userTypesActions = userTypes?.map((userType) => ({
		title: userType?.type?.title,
		onClick: () => setAllMembersFilters({ ...allMembersFilters, typeNames: [userType?.type.name] }),
	}));
	const records = users?.records?.map((user) => {
		return {
			details: users?.headers
				?.filter((header) => {
					if (filterHeader) {
						return keysToShow.includes(header.value);
					} else return true;
				})
				.map((header) => {
					const userKeyComponent = (
						<AppNewCheckbox
							label={tr("User_name")}
							checked={users.records.length == selectedUsers.length}
							onChange={() => {
								selectAllFn(users.records);
							}}
							idsOrderGenerator={"allUsers"}
						/>
					);
					const userValueComponent = (
						<RFlex styleProps={{ gap: "0px" }}>
							{checkboxExists && (
								<AppNewCheckbox
									checked={selectedUsers.some((u1) => u1.id == user.id)}
									onChange={() => {
										selectOneFn(user);
									}}
									idsOrderGenerator={user.id}
								/>
							)}
							<RProfileName
								name={user.full_name}
								img={user.image}
								onClick={() => {
									history.push(`${baseURL}/${genericPath}/users-management/users/${user.id}`);
								}}
								flexStyleProps={{ cursor: "pointer" }}
								className={styles.hoverLink}
							/>
						</RFlex>
					);
					const activeComponent = (
						<span
							onClick={() => (checkboxExists && !inCreate ? (activeIdsLoading?.includes(user.id) ? "" : changeSingleUserStatus(user)) : "")}
							className="p-0 m-0"
							style={{
								color: user.is_active ? colors.successColor : colors.dangerColor,
								cursor: checkboxExists && !inCreate ? "pointer" : "default",
							}}
						>
							{user.is_active ? tr("active") : tr("inactive")}
							{activeIdsLoading?.includes(user.id) && (
								<i className={`${iconsFa6.spinner}`} style={{ color: user.is_active ? colors.successColor : colors.dangerColor }} />
							)}
						</span>
					);
					const userTypesComponent = (
						<p className="p-0 m-0">
							{user.user_types.map((userType, index) => {
								return (
									<>
										<RLabel
											value={`${userType.type}}`}
											lettersToShow={userType.type.length}
											showDots={false}
											tooltipText={userType.is_active ? "active" : "inactive"}
											otherStyle={{ color: userType.is_active ? "" : colors.lightGray }}
										/>{" "}
										{index != user.user_types.length - 1 ? "," : ""}
									</>
								);
							})}
						</p>
					);
					const addressComponent = <RLabel value={user.address ?? "-"} lettersToShow={33} tooltipText={user.address} />;
					return {
						key: header.value == "full_name" && checkboxExists ? userKeyComponent : header.label,
						keyType: header.value == "full_name" ? "component" : "",
						dropdown: header.value == "user_types" || header.value == "gender" || header.value == "is_active" ? true : false,
						dropdownData:
							header.value == "user_types"
								? userTypesActions
								: header.value == "gender"
								? genderActions
								: header.value == "is_active"
								? activeActions
								: "",
						value:
							header.value == "full_name"
								? userValueComponent
								: header.value == "user_types"
								? userTypesComponent
								: header.value == "is_active"
								? activeComponent
								: header.value == "address"
								? addressComponent
								: !user[header.value]
								? "-"
								: user[header.value],
						type:
							header.value == "full_name" || header.value == "user_types" || header.value == "is_active" || header.value == "address"
								? "component"
								: "",
						color: header.value != "full_name" && header.value != "user_types" && header.value != "is_active" ? colors.lightGray : "",
					};
				}),
			actions: view
				? [
						{
							name: "",
							justIcon: true,
							color: "danger",
							icon: iconsFa6.delete,
							loading: removeIdsLoading.some((id) => id == user.id),
							disabled: removeIdsLoading.some((id) => id == user.id) || disableRemoving,
							actionIconClass: "text-danger",
							onClick: () => {
								deleteSingleMember(user);
							},
						},
				  ]
				: modal
				? [
						{
							name: "",
							justIcon: false,
							color: "primary",
							icon: iconsFa6.plus,
							loading: loadingIds.some((id) => id == user.id),
							disabled: loadingIds.some((id) => id == user.id),
							actionIconClass: "text-primary",
							onClick: () => {
								addUser(user);
							},
						},
				  ]
				: undefined,
		};
	});
	return (
		<RFlex styleProps={{ marginBottom: view ? "0px" : "100px" }}>
			<RLister disableXScroll={true} Records={records} center={false} line1={tr("no_users_yet")} />
		</RFlex>
	);
};

export default RUsersData;
