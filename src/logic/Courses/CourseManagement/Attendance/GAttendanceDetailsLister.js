import React, { useState, createContext, useMemo } from "react";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import ImageSettings from "assets/img/user-cog-solid1.svg";
import RSelect from "components/Global/RComs/RSelect";
import { FormGroup, Input, Label } from "reactstrap";
import { DummyContentForSelector, SettingsWithPermissions } from "./DummyData";
import RGroupButtons from "components/Global/RComs/RGroupButtons/RGroupButtons";
import styles from "./attendance.module.scss";
import AppModal from "components/Global/ModalCustomize/AppModal";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
// import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import RCourseAttendanceSettings from "./AttendanceSettings/RCourseAttendanceSettings";
import { attendanceApi } from "../../../../api/global/attendance";
import { useParams, useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Formik, useFormik } from "formik";
import DateUtcWithTz from "utils/dateUtcWithTz";
import { tablesTypes, settingsTable, options } from "./constants";
import RTodayDetails from "./RTodayDetails";
import ROverallDetails from "./ROverallDetails";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import * as colors from "config/constants";
import moment from "moment";
import RLoader from "components/Global/RComs/RLoader";
import { useCUDToQueryKey } from "../../../../hocs/useCUDToQueryKey";
import * as Yup from "yup";
import { useMutateData } from "hocs/useMutateData";
import GStudentAttendanceDetails from "./GStudentAttendanceDetails";

export const AttendanceContext = createContext(null);
const GAttendanceDetailsLister = () => {
	console.log("GAttendanceDetailsLister i	s rerendered");
	const { user } = useSelector((state) => state?.auth);
	const organization_id = user && user?.organization_id;
	const { courseId, cohortId, studentId } = useParams();
	const [todayfilter, setTodayFilter] = useState(true);
	const [searchData, setSearchData] = useState("");
	const [checkedAll, setCheckedAll] = useState(false);
	const [backDate, setBackDate] = useState(moment().utc().format());
	const [frontDate, setFrontDate] = useState(moment().format("YYYY-MM-DD"));
	const [attendanceSettingModal, setAttendanceSettingModal] = useState(false);
	const paragraph = checkedAll ? tr("uncheck_all") : tr("check_all_as_presented");
	const [enterPressed, setEnterPressed] = useState(false);
	const [firstTime, setFirstTime] = useState(false);
	const [creatingTemplate, setCreatingTemplate] = useState(false);
	const [assignOption, setAssignOption] = useState(options.thisCourse);
	const history = useHistory();
	const [activeModuleContent, setActiveModuleContent] = useState({ label: null, value: null });
	const [settingsCourses, setSettingsCourses] = useState([]);
	const [selectedCourses, setSelectedCourses] = useState([]);
	const [searchTerm, setSearchTerm] = useState("");
	const [localSearchTerm, setLocalSearchTerm] = useState("");
	//----------------------------- state Management --------------------------------
	const validationSchema = Yup.object({
		attendanceSettings: Yup.array().of(
			Yup.object({
				title: Yup.string().required("Title is Required"),
				points: Yup.number("Points must be a Number")
					.positive("Points is Greater than 0")
					.integer("integer")
					.required("Points is Required"),
				acronym: Yup.string().required("Acronym is Required"),
			})
		),
		maxUpdateTime: Yup.number("Time is a number").integer().positive("Must Be Greater Than 0").required("Time is Required"),
	});

	const initialAttendanceStates = {
		todayDetails: {},
		maxUpdateTime: null,
		attendanceSettings: [],
		studentDetails: [],
		studentsIds: [],
		attendanceTypesInput: [],
	};
	const {
		values: attendanceStates,
		errors,
		touched,
		setFieldValue,
		setTouched,
		setErrors,
		setFieldError,
		setFieldTouched,
		handleChange,
		handleBlur,
	} = useFormik({
		initialValues: initialAttendanceStates,
		validationSchema: validationSchema,
	});
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	//--------------------------------Course Module Content-----------------------------
	const { data: moduleContentData, isLoading: isLoadingModuleContent } = useFetchDataRQ({
		queryKey: [tablesTypes.moduleContent, backDate],
		enableCondition: !studentId,
		queryFn: () => attendanceApi.getCourseModuleContent(courseId, backDate),
		onSuccessFn: (data) => {
			setActiveModuleContent({ label: data?.data?.data?.[0]?.title, value: data?.data?.data?.[0]?.content_id });
		},
	});
	const allModules = moduleContentData?.data?.data?.map((module) => ({ label: module?.title, value: module?.content_id }));
	console.log("allModules", allModules);
	//-----------------------------Today Details----------------------------------
	const {
		data: todayDetails,
		isLoading: isLoadingTodayDetails,
		fetchStatus: todayDetailsFetchStatus,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.todayDetails, courseId, cohortId, activeModuleContent.value, searchTerm],
		queryFn: () => attendanceApi.getAllStudentsAttendance(courseId, activeModuleContent.value, searchTerm),
		enableCondition: activeModuleContent.value != null ? true : false,
		onSuccessFn: (data) => {
			console.log("Success")
			const studentsIds = [];
			const studentssAttendancetype = data?.data?.data?.students?.reduce((result, student) => {
				result[student.id] = { ...student, attendanceType: student.attendance_course_id };
				studentsIds.push(student.id);
				return result;
			}, {});
			studentssAttendancetype["ids"] = [...studentsIds];
			setFieldValue(tablesTypes.todayDetails, studentssAttendancetype);
			setFieldValue("studentsIds", studentsIds);
		},
	});

	const assignAttendanceToStudents = useMutateData({
		queryFn: ({ payload }) => attendanceApi.assignAttendanceToStudents(courseId, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.todayDetails, courseId, cohortId],
			[tablesTypes.studentDetails, studentId, backDate],
			[tablesTypes.overallDetails, courseId, searchTerm],
		],
		onSuccessFn: () => {
			console.log("Success");
		},
	});

	const handleAttendanceTypeChange = (studentId, attendanceType) => {
		setFieldValue(tablesTypes.todayDetails, {
			...attendanceStates.todayDetails,
			[studentId]: {
				...attendanceStates.todayDetails?.[studentId],
				attendanceType: attendanceType,
			},
		});
		console.log("Assign Attendance");
		const payload = {
			module_contents: [activeModuleContent.value],
			attendance: [
				{
					id: attendanceType,
					users: [studentId],
				},
			],
		};
		assignAttendanceToStudents.mutate({ payload });
	};
	//-------------------------------Overall Details--------------------------------------
	const { data: overallDetails, isLoading: isLoadingOverallDetails } = useFetchDataRQ({
		queryKey: [tablesTypes.overallDetails, courseId, searchTerm],
		queryFn: () => attendanceApi.getAllStudentsOverall(courseId, searchTerm),
		enableCondition: !studentId,
		// onSuccessFn: (data) => handleAttendanceOverallOnSuccess(data),
	});

	//----------------------------------Attendance Settings---------------------------------
	const { data: attendanceSettings, isLoading: isLoadingAttendanceSettings } = useFetchDataRQ({
		queryKey: [tablesTypes.attendanceSettings, courseId],
		enableCondition: !studentId,
		queryFn: () => attendanceApi.getAttendanceSettings(courseId),
		onSuccessFn: (data) => {
			const settings = data?.data?.data?.my_template?.map((item) => ({
				...item,
				focused: true,
				org_template: 0,
				saved: { points: true, title: true, acronym: true },
			}));
			setFieldValue(tablesTypes.attendanceSettings, settings);
			setCreatingTemplate(false);
		},
	});
	const setAttendanceSettingsMutation = useMutateData({
		queryFn: ({ payload }) => attendanceApi.setAttendanceSetting(payload),
		multipleKeys: true,
		invalidateKeys: [[tablesTypes.attendanceSettings, courseId], [tablesTypes.teacherCourses]],
		onSuccessFn: ({ data, variables }) => {
			console.log("Success");
			if (variables.other) {
				handleEditFunctionality(null, null, { id: null, propretyName: variables.other.propretyName, index: variables.other.index }, true);
			}
			setEnterPressed(false);
		},
		onErrorFn: () => {
			setEnterPressed(false);
		},
	});
	const addNewSetting = () => {
		if (errors.attendanceSettings) {
			const touchedSettings = attendanceStates.attendanceSettings.map(() => ({
				title: true,
				points: true,
				acronym: true,
			}));
			setTouched({
				...touched,
				attendanceSettings: touchedSettings,
			});
			return;
		}
		const newObject = {
			fakeId: -attendanceStates.attendanceSettings.length,
			title: "",
			points: "",
			acronym: "",
			focused: false,
			saved: { points: false, title: false, acronym: false },
			deletable: true,
			org_template: 0,
		};
		setFieldValue(tablesTypes.attendanceSettings, [...attendanceStates.attendanceSettings, newObject]);
		CUDToQueryKey({
			queryKey: [tablesTypes.attendanceSettings, courseId],
			newData: newObject,
			insertionDepth: "data.data.my_template",
			operation: operations.ADD,
		});
		setFieldTouched(tablesTypes.attendanceSettings, []);
	};

	const handleDeleteSetting = (id) => {
		const filteredArray = attendanceStates.attendanceSettings.filter((item) => {
			const itemId = item.id ?? item.fakeId;
			return itemId != id;
		});

		CUDToQueryKey({
			queryKey: [tablesTypes.attendanceSettings, courseId],
			insertionDepth: "data.data.my_template",
			operation: operations.DELETE,
			id,
		});
		setFieldValue(tablesTypes.attendanceSettings, filteredArray);
		if (id > 0) {
			const payload = {
				organization_id: organization_id,
				first: 0,
				courses: [courseId],
				attendance: filteredArray,
			};
			setAttendanceSettingsMutation.mutate({ payload });
		}
	};
	const createCourseTemplate = () => {
		setCreatingTemplate(true);
		const payload = {
			organization_id: organization_id,
			courses: [courseId],
			first: 0,
			attendance: attendanceSettings.data.data.school_template,
		};
		setAttendanceSettingsMutation.mutate({ payload });
		setFirstTime(false);
	};

	//---------------------------------Teacher Courses--------------------------------------
	const { data: teacherCourses, isLoading: isLoadingTeacherCourses } = useFetchDataRQ({
		queryKey: [tablesTypes.teacherCourses],
		enableCondition: !studentId,
		queryFn: () => attendanceApi.getTeacherCourses(courseId),
		onSuccessFn: (data) => {
			const thisCourse = (data?.data?.data?.courses_without_template?.filter((item) => item.id == courseId))[0];

			setFirstTime(thisCourse?.first == 1 ? true : false);
			if (thisCourse?.first == 1) {
				setAttendanceSettingModal(true);
			}
			const coursesOptions = data?.data?.data?.courses_without_template?.map((course, index) => ({ label: course.name, value: course.id }));
			setSettingsCourses(coursesOptions);
		},
	});
	//---------------------------------Student Details--------------------------------------
	const { data: studentDetails, isLoading: isLoadingStudentDetails } = useFetchDataRQ({
		queryKey: [tablesTypes.studentDetails, studentId, backDate],
		queryFn: () => attendanceApi.getStudentDetails(courseId, studentId, backDate),
		onSuccessFn: (data) => {
			const studentStates = data.data.data.module_contents.reduce((result, item) => {
				result[item.content_id] = { saved: true, value: { label: item.attendance, value: null } };
				return result;
			}, {});
			setFieldValue("attendanceTypesInput", studentStates);
		},
		enableCondition: studentId ? true : false,
	});

	const handleEditSelectInput = (event, item, { id, inputLabel }) => {
		setFieldValue("attendanceTypesInput", {
			...attendanceStates.attendanceTypesInput,
			[id]: { ...attendanceStates.attendanceTypesInput[id], saved: false },
		});
	};
	const handleSelectAttendanceType = (event, item, { id, inputLabel }) => {
		setFieldValue("attendanceTypesInput", {
			...attendanceStates.attendanceTypesInput,
			[id]: { ...attendanceStates.attendanceTypesInput[id], value: { ...event } },
		});

		const payload = {
			module_contents: [id],
			attendance: [
				{
					id: event.value,
					users: [studentId],
				},
			],
		};
		assignAttendanceToStudents.mutate({ payload });
	};

	const hello = () => { };
	//---------------------------------Global Function--------------------------------------
	const handleDateChange = (e) => {
		setBackDate(moment(e.target.value).utc().format());
		setFrontDate(e.target.value);

		const utcDate = moment(e.target.value).utc().format();
		console.log("utcDate", utcDate);
	};

	const closeModal = () => {
		setAttendanceSettingModal(false);
	};

	const handleSearch = (clearData) => {
		let data = localSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setSearchTerm(data);
	};
	const handleEditFunctionality = (event, item, { id, propretyName, index }, savedFlag = false) => {
		const newArray = JSON.parse(JSON.stringify(attendanceStates.attendanceSettings));
		newArray[index].saved[propretyName] = savedFlag;
		setFieldValue(tablesTypes.attendanceSettings, [...newArray]);
	};

	const handleInputDown = (event, item, { id, index, propretyName }) => {
		if (event.key == "Enter") {
			if (enterPressed || attendanceStates.attendanceSettings?.[index]?.[propretyName] == "") return;
			if (errors?.attendanceSettings) {
				if (errors?.attendanceSettings?.[index]?.[propretyName]) {
					return;
				} else {
					handleEditFunctionality(event, item, { id, propretyName, index }, true);
					return;
				}
			}
			setEnterPressed(true);
			let payload = {};
			const selectedCoursesIds = [];
			selectedCoursesIds.push(courseId);
			payload = {
				organization_id: organization_id,
				courses: selectedCoursesIds,
				first: 0,
				attendance: [...attendanceStates.attendanceSettings],
			};
			setAttendanceSettingsMutation.mutate({ payload, other: { propretyName, index } });
		}
	};
	const handleInputBlur = (event, item, { id, propretyName, index }) => {
		if (enterPressed || attendanceStates.attendanceSettings?.[index]?.[propretyName] == "") return;
		if (errors?.attendanceSettings) {
			if (errors?.attendanceSettings?.[index]?.[propretyName]) {
				return;
			} else {
				handleEditFunctionality(event, item, { id, propretyName, index }, true);
				return;
			}
		}
		setEnterPressed(true);
		let payload = {};
		const selectedCoursesIds = [];
		selectedCoursesIds.push(courseId);
		payload = {
			organization_id: organization_id,
			courses: selectedCoursesIds,
			first: 0,
			attendance: [...attendanceStates.attendanceSettings],
		};
		setAttendanceSettingsMutation.mutate({ payload, other: { propretyName, index } });
	};

	const handleSelectModuleContent = (e) => {
		setActiveModuleContent(e);
	};

	const handleCheckAllEvent = (attendance_course_id) => {
		const newStudentsAttendanceType = JSON.parse(JSON.stringify(attendanceStates.todayDetails));
		newStudentsAttendanceType?.ids?.map((id) => {
			newStudentsAttendanceType[id].attendanceType = checkedAll ? null : attendance_course_id;
		});
		setFieldValue(tablesTypes.todayDetails, newStudentsAttendanceType);
		const presentedId = todayDetails?.data?.data?.attendance.find((item) => item.title == "Presented").id;
		const payload = {
			module_contents: [activeModuleContent.value],
			attendance: [
				{
					id: checkedAll ? null : presentedId,
					users: attendanceStates.studentsIds,
				},
			],
		};
		assignAttendanceToStudents.mutate({ payload });
	};
	const buttons = [
		{
			id: 1,
			title: tr("Today"),
			color: "primary",
			onClick: () => {
				setTodayFilter(true);
			},
			outline: todayfilter ? false : true,
			className: styles.AttendanceButton,
		},
		{
			id: 2,
			title: tr("Overall"),
			color: "primary",
			outline: todayfilter ? true : false,
			onClick: () => {
				setTodayFilter(false);
			},
			className: styles.AttendanceButton,
		},
	];
	const handleSelectCourse = (e) => {
		const selectAll = e.filter((option) => option.value < 0).length > 0;
		if (selectAll) {
			setSelectedCourses(settingsCourses);
			// let payload = {}
			// const selectedCoursesIds = settingsCourses.map((course) => course.value)
			// selectedCoursesIds.push(courseId)
			// payload = {
			// 	organization_id: organization_id,
			// 	courses: selectedCoursesIds,
			// 	first: 0,
			// 	attendance: [...attendanceStates.attendanceSettings]
			// }
			// setAttendanceSettingsMutation.mutate({ payload })
		} else {
			setSelectedCourses(e);
			// if (e == [])
			// 	return
			// let payload = {}
			// payload = {
			// 	organization_id: organization_id,
			// 	courses: [e.value],
			// 	first: 0,
			// 	attendance: [...attendanceStates.attendanceSettings]
			// }
			// setAttendanceSettingsMutation.mutate({ payload })
		}
	};
	const handleAddingToAnotherCourses = () => {
		const selectedCoursesIds = selectedCourses.map((course) => course.value);
		let payload = {};
		payload = {
			organization_id: organization_id,
			courses: selectedCoursesIds,
			first: 0,
			attendance: [...attendanceStates.attendanceSettings],
		};
		setAttendanceSettingsMutation.mutate({ payload });
		setSelectedCourses([]);
	};
	return (
		// <Formik
		// 	initialValues={initialAttendanceStates}
		// 	validationSchema={validationSchema}
		// >
		// 	{({ values, touched, errors, handleBlur, setFieldValue }) => (
		<AttendanceContext.Provider
			value={{
				todayDetails: todayDetails?.data?.data,
				overallDetails: overallDetails?.data?.data,
				attendanceSettings: attendanceSettings?.data?.data,
				attendanceStates,
				touched,
				errors,
				isLoadingTodayDetails: isLoadingTodayDetails && todayDetailsFetchStatus != "idle",
				isLoadingOverallDetails,
				isLoadingAttendanceSettings,
				firstTime,
				creatingTemplate,
				teacherCourses: teacherCourses?.data?.data,
				isLoadingTeacherCourses,
				assignOption,
				history,
				courseId,
				cohortId,
				studentId: parseInt(studentId),
				isLoadingStudentDetails,
				backDate,
				frontDate,
				settingsCourses,
				selectedCourses,
				studentDetails: studentDetails?.data?.data,

				handleSelectCourse,
				handleDateChange,
				setAssignOption,
				setFieldValue,
				// handleBlur,
				handleAttendanceTypeChange,
				handleDeleteSetting,
				handleEditFunctionality,
				handleInputDown,
				handleInputBlur,
				handleChange,
				handleBlur,
				addNewSetting,
				createCourseTemplate,
				handleEditSelectInput,
				handleSelectAttendanceType,
				handleAddingToAnotherCourses,
				isAssigningAttendance: assignAttendanceToStudents.isLoading,
				isSettingAttendance: setAttendanceSettingsMutation.isLoading,
			}}
		>
			{!studentId ? (
				<>
					<RFlex className="d-flex flex-column" styleProps={{ gap: "0px", marginBottom: "0px" }}>
						<RFlex className={`justify-content-between align-items-center `}>
							<RSearchHeader
								inputPlaceholder={tr`search_a_student`}
								searchData={localSearchTerm}
								handleChangeSearch={setLocalSearchTerm}
								setSearchData={setLocalSearchTerm}
								handleSearch={handleSearch}
							/>
							<RFlex
								styleProps={{ gap: "5px" }}
								className={`align-items-center ${styles.AttendanceSettings}`}
								onClick={() => setAttendanceSettingModal(true)}
							>
								<img src={ImageSettings} alt="Settings" width={18} height={18} />
								<span style={{ fontSize: "14px", width: "max-content" }}>{tr("Attendance_Settings")}</span>
							</RFlex>
						</RFlex>

						<RFlex className={`align-items-center ${todayfilter ? "justify-content-between" : "justify-content-end"} mr-1`}>
							{todayfilter && (
								<RFlex styleProps={{ gap: "20px" }} className="align-items-center">
									<Input
										name="dueDate"
										type="date"
										placeholder={tr`due_date`}
										value={frontDate}
										onChange={handleDateChange}
										style={{ width: "180px" }}
									/>
									{!isLoadingModuleContent ? (
										moduleContentData?.data?.data?.length > 0 ? (
											<div style={{ width: "180px" }}>
												<RSelect
													option={allModules}
													closeMenuOnSelect={true}
													value={activeModuleContent}
													placeholder={tr`choose_content`}
													onChange={(e) => handleSelectModuleContent(e)}
													required
												/>
											</div>
										) : (
											<div className={styles.EmptyContent}>{tr("nothing_for_this_day")}</div>
										)
									) : (
										<RLoader mini={true} />
									)}
									{/* <AppNewCheckbox
										label={paragraph}
										paragraphStyle={{ width: "max-content" }}
										onChange={() => {
											handleCheckAllEvent((todayDetails?.data?.data?.attendance.filter((type) => type.title == "Presented"))[0]?.id);
											setCheckedAll(!checkedAll);
										}}
										disabled={!todayDetails || todayDetails?.data?.data?.students?.length <= 0}
									/> */}
									<p
										className="p-0 m-0 text-primary"
										onClick={() =>
											!todayDetails || todayDetails?.data?.data?.students?.length <= 0
												? ""
												: handleCheckAllEvent((todayDetails?.data?.data?.attendance.filter((type) => type.title == "Presented"))[0]?.id)
										}
										style={{ cursor: "pointer", minWidth: "max-content" }}
									>
										{" "}
										{tr("Mark_All_As_Presented")}
									</p>
								</RFlex>
							)}
							<RGroupButtons buttons={buttons} styleProps={{ width: "180px" }} selectedFilter={todayfilter ? "Today" : "Overall"} />
						</RFlex>
					</RFlex>

					{todayfilter ? <RTodayDetails /> : <ROverallDetails />}
					<AppModal headerSort={<RCourseAttendanceSettings />} show={attendanceSettingModal} parentHandleClose={closeModal} size="md" />
				</>
			) : (
				<GStudentAttendanceDetails />
			)}
		</AttendanceContext.Provider>
		// 	)}

		// </Formik>
	);
};

export default React.memo(GAttendanceDetailsLister);
