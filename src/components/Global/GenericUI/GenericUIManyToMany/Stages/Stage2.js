import React from "react";
import { Button } from "reactstrap";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";

const Stage2 = ({ containerTitle, containerActions, listerProps }) => (
  <Modal size="lg" style={{ maxWidth: "700px", width: "100%" }} isOpen={true}>
    <ModalHeader>{containerTitle}</ModalHeader>
    <ModalBody>
      <RAdvancedLister {...listerProps} />
    </ModalBody>
    <ModalFooter>
      {containerActions.map((ac) => (
        <Button key={ac.btnCaption} color={ac.btnColor} onClick={ac.handler}>
          {ac.btnCaption}
        </Button>
      ))}
    </ModalFooter>
  </Modal>
);

export default Stage2;
