import React, { useState, useCallback } from "react";
import { Input, FormText } from "reactstrap";
import EyeToggleButton from "./EyeToggleButton";
import tr from "../RTranslator";

const RPasswordFields = ({ onChange, onBlur, touched, errors, value, placeholder = tr("password"), name = "password" }) => {
	const [passwordVisibility, setPasswordVisibility] = useState(false);

	const togglePasswordVisibility = useCallback(() => {
		setPasswordVisibility((visible) => !visible);
	}, []);

	return (
		<>
			<div style={{ position: "relative" }}>
				<Input
					name={name}
					type={passwordVisibility ? "text" : "password"}
					value={value}
					placeholder={placeholder}
					onBlur={onBlur}
					onChange={onChange}
					className={(!!touched && !!errors) || (touched && errors) ? "input__error" : ""}
				/>

				<EyeToggleButton show={passwordVisibility} click={togglePasswordVisibility} />
			</div>
			{touched && errors && <FormText color="danger">{errors}</FormText>}
		</>
	);
};

export default RPasswordFields;
