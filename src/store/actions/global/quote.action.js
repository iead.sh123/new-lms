import {
	QUOTE_REQUEST,
	QUOTE_SUCCESS,
	QUOTE_ERROR,
	MONTHLY_TRAIT_REQUEST,
	MONTHLY_TRAIT_SUCCESS,
	MONTHLY_TRAIT_ERROR,
} from "./globalTypes";

import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { quoteApi } from "api/global/quote";
import { toast } from "react-toastify";

export const getQuoteAsync = () => async (dispatch) => {
	dispatch({ type: QUOTE_REQUEST });
	try {
		const response = await quoteApi.getQuote();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: QUOTE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: QUOTE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: QUOTE_ERROR });
		toast.error(error?.message);
	}
};

export const getMonthlyTraitAsync = () => async (dispatch) => {
	dispatch({ type: MONTHLY_TRAIT_REQUEST });
	try {
		const response = await quoteApi.getMonthlyTrait();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: MONTHLY_TRAIT_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: MONTHLY_TRAIT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: MONTHLY_TRAIT_ERROR });
		toast.error(error?.message);
	}
};
