import React from "react";
import CartError from "assets/img/new/cart-error.png";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL } from "engine/config";

const CartGuestUser = () => {
	const history = useHistory();
	return (
		<RFlex styleProps={{ flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
			<p>{tr`log_in_to_fill_your_cart`}</p>
			<img src={CartError} alt="cart-error" style={{ height: "100px" }} />
			<RButton text={tr`login`} color="primary" onClick={() => history.push(baseURL + `/auth/login`)} />
		</RFlex>
	);
};

export default CartGuestUser;
