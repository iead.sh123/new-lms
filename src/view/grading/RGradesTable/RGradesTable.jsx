import RFlex from 'components/Global/RComs/RFlex/RFlex';
import tr from 'components/Global/RComs/RTranslator';
import { DATE_FORMATE } from 'config/constants';
import { Services } from 'engine/services';
import moment from 'moment';
import React from 'react';
import { Row, Col, Table } from 'reactstrap';
import Loader from 'utils/Loader';
import styles from './RGradesTable.module.scss'
import RTable from '../RTable/RTable';
import Helper from 'components/Global/RComs/Helper';


const RGradesTable = ({ title, keyPrefix, headers, dataCollection, loading, sortRecords = {}, handleAddNew, deriveActions = ac => ac, editedId, handleUpdate, disabledFieldsForEdit = [], disableAdditon, hideTitle, collapsedChildren = {}, onRecordClick = () => null }) => {

const [addNewFormIsVisible, setAddNewFormIsVisible] = React.useState(false);
const [formItems, setFormItems] = React.useState({});
  React.useEffect(() => {
    Helper.cl(headers , "gradestbale useEffect setFormItems on headers");
  //  setFormItems(headers.filter(h => h.inForm).reduce((prev, curr) => ({ ...prev, [curr.value]: '' }), {}));
  }, [headers]);


  React.useEffect(() => {

    Helper.cl(headers , "gradestbale useEffect setFormItems on edited id");
    setFormItems(headers.filter(h => h.inForm).reduce((prev, curr) => ({ ...prev, [curr.value]: dataCollection?.byId?.[editedId]?.[curr.value] }), {}));
  }, [editedId])


  const handleFormItemChange = (e, name) => {

    Helper.cl(name+"="+e?.target?.value,"handleFormItemChange")
    Helper.cl(headers?.find(h => h.value === name)?.type === "number","headers?.find(h => h.value === name)?.type === ")

      const s={[name]: 
      headers?.find(h => h.value === name)?.type === "number" ? 
      + e?.target?.value : e?.target?.value ?? e}

    Helper.cl(s,"s");
    const newFormItem={ ...formItems,
       [name]: 
       headers?.find(h => h.value === name)?.type === "number" ? 
      + e?.target?.value : e?.target?.value ?? e }

    Helper.cl(newFormItem,"handleFormItemChange newFormItem")
    setFormItems(newFormItem);
  }


  const renderInfo = ({ name, image }) => {
    return (
      <RFlex>
        <img
          width={30}
          height={30}
          style={{ borderRadius: "100%" }}
          src={image}
        />
        <span>{name}</span>
      </RFlex>
    );
  }


  const records =
    /*React.useMemo(() => 
    {
       //-------------------------------------------------------In case of Addition Row
      return*/ [...addNewFormIsVisible ? [1].map((_) => {
    return {
      table_name: keyPrefix,
      id: keyPrefix + 'form',
      details: Object.keys(formItems)?.map((item) => ({
        key: tr(item),

        value: formItems[item],
        type: headers?.find(h => h.value === item)?.type,
        disabled: headers?.find(h => h.value === item)?.disabled,

        //select is not returning the whole event only value so name must be binded

        onChange: e => handleFormItemChange(e, item),
        option: headers?.find(h => h.value === item)?.options,
        value: headers?.find(h => h.value === item)?.defaultValue,
        name: item,
        mode: 'form'
      })),
      actions: [{
        icon: 'fa fa-plus',
        onClick: () => { handleAddNew({ ...formItems }); setAddNewFormIsVisible(false) }
      }]
    }
  }

  //---------------------------------------------------------Listing Data
  ) : [], ...dataCollection?.ids?.map((id) => {
    return {
      table_name: keyPrefix,
      id: id,
      children: collapsedChildren?.[id]?.map?.(ch => {
        return {
          ...ch,
          id: ch.id,
          actions:  ch.actions,
          details: headers?.map((header) => ({
            key: tr(header.label),
            isChild: true,
            value:
              header.type == "Date"
                ? moment(ch[header.value]).format(DATE_FORMATE)
                : header.type == "User"
                  ? renderInfo({
                    name: ch?.[header.value].full_name,
                    image:
                      Services.auth_organization_management.file +
                      ch?.[header.value].hash_id,
                  })
                  : ch?.[header?.value]?.icon ? <span style={{color: ch?.[header?.value]?.color}}><i className={ch?.[header?.value]?.icon}/> {ch?.[header?.value]?.value} </span> : ch?.[header.value],
            // onClick: () => header.clickable ? onRecordClick(header.value, dataCollection?.byId?.[id]) : null,
            type: "component",
          }))
        }
      }
      ),
      //-----------------------------------------------------------------------The Edited Line
      details: id === editedId ? Object.keys(formItems)?.map((item) => { 
        Helper.cl(item,"RGradesTable INitialize item=")
        
        Helper.cl(formItems[item],"RGradesTable INitialize formItems[item]")
        Helper.cl(formItems,"RGradesTable INitialize all formItems")
        Helper.cl("-------------------------------------------------")
        return({
        key: tr(item),
        value: formItems[item],
        type: headers?.find(h => h.value === item)?.type,
        disabled: headers?.find(h => h.value === item)?.disabled || disabledFieldsForEdit.includes(item),
        option: headers?.find(h => h.value === item)?.options,
        onChange: e => handleFormItemChange(e, item),
        name: item,
        mode: 'form'
      })}
      
      ) : headers?.map((header) => ({
        key: tr(header.label),
        value:
          header.type == "Date"
            ? moment(dataCollection?.byId?.[id]?.[header.value]).format(DATE_FORMATE)
            : header.type == "User"
              ? renderInfo({
                name: dataCollection?.byId?.[id]?.[header.value].full_name,
                image:
                  Services.auth_organization_management.file +
                  dataCollection?.byId?.[id]?.[header.value].hash_id,
              })
              : dataCollection?.byId?.[id]?.[header.value],
        onClick: () => header.clickable ? onRecordClick(header.value, dataCollection?.byId?.[id]) : null,
        type: "component",
        dropdown: Object.keys(sortRecords).includes(header.label),
        dropdownData: sortRecords?.[header.label] ?? [],

      })),

      actions: editedId === id ? [{
        icon: 'fa fa-plus',
        onClick: () => { handleUpdate({ ...formItems }); setAddNewFormIsVisible(false) }
      }] : deriveActions(dataCollection?.byId?.[id], id).map(ac => ({ ...ac, onClick: () => ac.onClick(dataCollection?.byId?.[id]) }))

    }
  }

  )];
  // }
  // , [dataCollection, addNewFormIsVisible, formItems]);

  return <div className='mt-4 mb-4'>

    {hideTitle ? <></> : <Row><p className={styles['Title']}>{title} <i className={styles['Badge']}>{records.length}</i></p></Row>}
    {loading ? (
      <Loader />
    ) : (

      <Col xs={12} className="mt-3">
        <RTable headers={headers} Records={records} />
      </Col>
    )
    }
    {disableAdditon ? <></> : <Row><i
      className={[styles['AddButtons'], 'nc-icon nc-simple-add'].join(' ')}
      onClick={() => setAddNewFormIsVisible(true)}
    >{tr`Add New ${keyPrefix}`}</i></Row>}
  </div>
};

export default RGradesTable;


/*
DELETE FROM grading_group where 1;
UPDATE `custom` SET `groupId`=null WHERE 1;
UPDATE `custom` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `assignment` SET `groupId`=null WHERE 1;
UPDATE `assignment` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `exam` SET `groupId`=null WHERE 1;
UPDATE `exam` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `poll` SET `groupId`=null WHERE 1;
UPDATE `poll` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `quiz` SET `groupId`=null WHERE 1;
UPDATE `quiz` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `project` SET `groupId`=null WHERE 1;
UPDATE `project` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;

UPDATE `survey` SET `groupId`=null WHERE 1;
UPDATE `survey` SET `parentAlternativeId`=null , `parentAlternativeType`=null WHERE 1;
*/




