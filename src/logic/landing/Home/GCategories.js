import React, { useContext } from "react";
import { LandingPageContext } from "./GHome";
import { useHistory } from "react-router-dom";
import styles from "./Home.module.scss";
import RCategoryCard from "components/Global/RComs/RCards/RCategoryCard";

const GCategories = () => {
  const history = useHistory();

  const LandingPageData = useContext(LandingPageContext);

  const handlePushToAllCourses = (categoryId) => {
    history.push(`/landing/all-courses?categoryId=${categoryId}`);
  };

  return (
    <section className={styles.padding}>
      <h6>{LandingPageData?.landingPage?.categories?.label}</h6>
      <RCategoryCard
        categories={LandingPageData?.landingPage?.categories?.categories}
        handlePushToAllCourses={handlePushToAllCourses}
      />
    </section>
  );
};

export default GCategories;
