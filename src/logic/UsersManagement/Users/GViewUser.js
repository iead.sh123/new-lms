import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useEffect, useRef, useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import * as colors from "config/constants";
import { useHistory, useParams } from "react-router-dom";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import { Terms, usersTabs } from "../constants";
import styles from "./userStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { Form, useFormik } from "formik";
import * as Yup from "yup";
import RButton from "components/Global/RComs/RButton";
import RUserBasicInfo from "./RUserBasicInfo";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { usersManagementApi } from "api/UsersManagement";
import RLoader from "components/Global/RComs/RLoader";
import { useMutateData } from "hocs/useMutateData";
import RRoles from "../Shared/RRoles";
import RPermissons from "../Shared/RPermissons";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
const GViewUser = () => {
	//---------------------------------Global-----------------------------------------------------
	const [activeTab, setActiveTab] = useState(usersTabs[0].title);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const currentOrganization = { title: "TestOrg", id: 1 };
	const { userId: paramUserId } = useParams();

	const [finsihGettingUser, setFinishGettingUser] = useState(null);
	const {
		user: userDetails,
		isLoading: isLoadingUserDetails,
		isFetching: isFetchingUserDeatils,
	} = useFetchDataRQ({
		queryKey: [Terms.Users, paramUserId],
		queryFn: () => usersManagementApi.getUserDetails(paramUserId),
		onSuccessFn: (data) => {
			const info = data?.data?.data;
			setFinishGettingUser(true);
			setValues({
				username: info.name,
				firstname: info.first_name,
				lastname: info.last_name,
				email: info.email,
				userid: info.identifier,
				address: info.details?.address,
				phone: info.details?.phone,
				emergency: info.details?.emergency,
			});
			setGender(info.gender == "m" ? "male" : "female");
			// setUserTypesCount();
			setHashId(info.image);
			setSelectedPermissions(info.current.permissions);
			setSavedPermissions(info.current.permissions);
			setSelectedRoles(info.current.roles);
			setSavedRoles(info.current.roles);
		},
	});
	//---------------------------------User Basic Info--------------------------------------------------
	const [userTypesCount, setUserTypesCount] = useState([{ id: 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	const [accountActiveStatus, setAccountActiveStatus] = useState(true);
	const [basicInfoEditMode, setBasicInfoEditMode] = useState(false);
	const [uploadId, setUploadId] = useState(null);
	const [hashId, setHashId] = useState(null);
	const [pickedOrganization, setPickedOrganization] = useState(null);
	const [gender, setGender] = useState("male");
	const initialValues = {
		username: "",
		firstname: "",
		lastname: "",
		userid: "",
		email: "",
		password: "",
		address: "",
		phone: "",
		emergency: "",
	};
	const phoneRegex = /^[+]?[(]?[0-9]{1,4}[)]?[-\s.]?[0-9]{1,3}[-\s.]?[0-9]{3,4}[-\s.]?[0-9]{3,4}$/im;
	const validationSchema = Yup.object({
		username: Yup.string().required(tr("User_Name_is_required")),
		firstname: Yup.string().required(tr("First_Name_is_required")),
		lastname: Yup.string().required(tr("Last_Name_is_required")),
		email: Yup.string().email(tr("email_is_invalid")).required(tr("email_is_required")),
		password: Yup.string().min(8, tr("Password_must_be_at_least_8_charachters")).required(tr("Password_is_required")),
		phone: Yup.string().matches(phoneRegex, "Invalide Phone Number"),
		emergency: Yup.string().matches(phoneRegex, "Invalide Phone Number"),
	});
	const { values, touched, errors, setFieldValue, setValues, setFieldTouched, handleSubmit, setFieldError, handleChange, handleBlur } =
		useFormik({
			initialValues,
			validationSchema,
			validateOnMount: true,
			onSubmit: (values) => getSaveAction(),
		});
	const handleAddingUserType = () => {
		const maxId = userTypesCount.reduce((max, type) => (max > type.id ? max : type.id), 0);
		setUserTypesCount([...userTypesCount, { id: maxId + 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	};
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(),
	});
	const handleSelectOption = (option, stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		if (!option.value) {
			userTypesCountCopy[typeIndex].pickedOption = [];
			userTypesCountCopy[typeIndex].disabledStatus = true;
			userTypesCountCopy[typeIndex].activeStatus = false;
		} else {
			userTypesCountCopy[typeIndex].pickedOption = option;
			userTypesCountCopy[typeIndex].disabledStatus = false;
			userTypesCountCopy[typeIndex].activeStatus = true;
		}
		setUserTypesCount(userTypesCountCopy);
	};
	const handleSwitchChange = (stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		userTypesCountCopy[typeIndex].activeStatus = !userTypesCountCopy[typeIndex].activeStatus;
		setUserTypesCount(userTypesCountCopy);
	};
	const handleAccountActiveChanged = () => {
		setAccountActiveStatus(!accountActiveStatus);
	};
	const generateEmailFromUsername = () => {
		if (errors.username) {
			setFieldTouched("username", true);
			return;
		}
		const domain = "@OrganizationName.org";
		const email = values.username.trim().replace(/\s/g, ".") + domain;
		setFieldValue("email", email);
	};
	const suggestPassword = () => {
		const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		let password = "";
		const length = 8; // Length of the password

		for (let i = 0; i < length; i++) {
			const randomIndex = Math.floor(Math.random() * characters.length);
			password += characters.charAt(randomIndex);
		}
		setFieldValue("password", password);
	};

	const updateUserAccountMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.updateUserAccount(paramUserId, payload),
		onSuccessFn: (data) => {
			setBasicInfoEditMode(false);
			setHashId(data?.data?.data?.image);
			scrollToTop();
		},
	});
	console.log("uploadId", uploadId);
	const handleCreatingNewUserAccount = () => {
		console.log("Running");
		if (Object.keys(errors).length > 0) {
			return;
		}
		const types = userTypesCount.map((type, index) => ({ id: type?.pickedOption?.value, active: type?.activeStatus }));
		const finalTypes = types.filter((type) => type.id);
		const payload = {
			name: values.username,
			image: uploadId.url ? [uploadId] : undefined,
			first_name: values.firstname,
			last_name: values.lastname,
			gender: gender.charAt(0),
			email: values.email,
			password: values.password,
			c_password: values.password,
			// organization_group_id: 0,
			organization_id: currentOrganization.id,
			details: {
				address: values.address,
				emergency_number: values.emergency,
				phone_number: values.phone,
			},
			is_active: accountActiveStatus,
			identifier: values.userid,
			types: finalTypes.length > 0 ? finalTypes : undefined,
			// tags: [
			// 	{
			// 		id: 0,
			// 		name: "string",
			// 		color_id: 0,
			// 	},
			// ,
		};
		updateUserAccountMutation.mutate({ payload });
	};
	const handleSelectOrganization = (e) => {
		setPickedOrganization(e);
	};
	const topRef = useRef(null);

	console.log("errors2", errors);
	//--------------------------------Roles------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState([]);
	const [savedRoles, setSavedRoles] = useState([]);
	const [rolesEditMode, setRolesEditMode] = useState(false);
	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		enableCondition: !!userDetails && finsihGettingUser,
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			const sortedRoles = getSortedRoles(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Roles, backRoleSearchTerm],
				newData: sortedRoles,
				insertionDepth: "data.data",
			});
		},
	});

	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles.some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = selectedRoles.filter((p1) => p1.id != role.id);
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = [...selectedRoles, role];
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const assignRolesToUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRolesToUsers(payload),
		onSuccessFn: () => {
			scrollToTop();
		},
		invalidateKeys: [Terms.Users, paramUserId],
	});
	const removeRolesFromUserMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeRolesFromUsers(payload),
		onSuccessFn: () => {
			scrollToTop();
		},
		invalidateKeys: [Terms.Users, paramUserId],
	});
	const getRemovedRoles = () => {
		const idsInSelectedRoles = new Set(selectedRoles.map((r) => r.id));
		const removedRoles = savedRoles.filter((r) => !idsInSelectedRoles.has(r.id));
		const removedRolesNames = removedRoles.map((r) => r.name);
		return removedRolesNames;
	};
	const getSortedRoles = (roles) => {
		const selectedPermissoinsCopy = JSON.parse(JSON.stringify(roles));
		const selectedIds = new Set(selectedRoles?.map((p) => p.id));
		const rolesArray = selectedPermissoinsCopy?.data?.data;
		rolesArray?.sort((r1, r2) => {
			const r1IsSelected = selectedIds.has(r1.id);
			const r2IsSelected = selectedIds.has(r2.id);
			if (r1IsSelected && !r2IsSelected) {
				return -1;
			}
			if (!r1IsSelected && r2IsSelected) {
				return 1;
			}
			return 0;
		});
		return rolesArray;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState([]);
	const [savedPermissions, setSavedPermissions] = useState([]);
	const [permissionsEditMode, setPermissionsEditMode] = useState();
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		keepPreviousData: true,
		enableCondition: !!userDetails && finsihGettingUser,
		onSuccessFn: (data) => {
			const sortedPermissions = getSortedPermissions(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
				newData: sortedPermissions,
				insertionDepth: "data.data",
			});
		},
	});

	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissonsToUsers(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: () => {
			history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const removePermissionsFromUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromUsers(payload),
		onSuccessFn: () => {
			history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};
	const getSortedPermissions = (permissions) => {
		const selectedPermissoinsCopy = JSON.parse(JSON.stringify(permissions));
		const selectedIds = new Set(selectedPermissions?.map((p) => p.id));
		const permissionsArray = selectedPermissoinsCopy?.data?.data;
		permissionsArray?.sort((p1, p2) => {
			const p1IsSelected = selectedIds.has(p1.id);
			const p2IsSelected = selectedIds.has(p2.id);
			if (p1IsSelected && !p2IsSelected) {
				return -1;
			}
			if (!p1IsSelected && p1IsSelected) {
				return 1;
			}
			return 0;
		});
		return permissionsArray;
	};
	//----------------------------------------Global-------------------------------------------
	const history = useHistory();

	const scrollToTop = () => {
		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
		}
	};
	const handleEditSaveClicked = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.BasicInfo:
				if (!basicInfoEditMode) {
					setBasicInfoEditMode(true);
					return;
				}
				handleCreatingNewUserAccount();
				break;
			case Terms.Roles:
				if (!rolesEditMode) {
					setRolesEditMode(true);
					return;
				}
				const removedRolesNames = getRemovedRoles();
				if (removedRolesNames.length > 0) {
					payload = {
						role_names: removedRolesNames,
						user_ids: [paramUserId],
					};
					removeRolesFromUserMutation.mutate({ payload });
				}
				if (selectedRoles.length > 0) {
					const roleNames = selectedRoles.map((role) => role.name);
					payload = {
						role_names: roleNames,
						user_ids: [paramUserId],
					};
					assignRolesToUsersMutation.mutate({ payload });
				}
				//saadaadasfjhjafgsdasdsadsadadasdaasdsasdasdasda
				const sortedRoles = getSortedRoles(roles);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Roles, backRoleSearchTerm],
					newData: sortedRoles,
					insertionDepth: "data.dataa",
				});
				setSavedRoles(selectedRoles);
				setRolesEditMode(false);
				break;
			case Terms.Permissions:
				if (!permissionsEditMode) {
					setPermissionsEditMode(true);
					return;
				}
				//----------------------------------Removed Permissions----------------------s-------
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						user_ids: [paramUserId],
					};
					removePermissionsFromUsersMutation.mutate({ payload });
				}
				//----------------------------------add Permissions--------------------------------
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						user_ids: [paramUserId],
					};
					assignPermissionsToUsersMutation.mutate({ payload });
				}
				const sortedPermissions = getSortedPermissions(permissions);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
					newData: sortedPermissions,
					insertionDepth: "data.data",
				});
				setPermissionsEditMode(false);
				setSavedPermissions(selectedPermissions);
				break;
		}
		return;
	};

	return (
		<RFlex className="flex-column" styleProps={{ position: "relative" }}>
			<RFlex className="align-items-center">
				<div
					ref={topRef}
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/users`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				{tr("Create_new_Account")}
			</RFlex>
			<RFlex className="justify-between">
				<RTabs tabs={usersTabs} setTabActive={setActiveTab} activeTab={activeTab} />
				{activeTab == Terms.BasicInfo && (
					<RButton
						text={basicInfoEditMode ? tr("save") : tr("edit")}
						faicon={basicInfoEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={basicInfoEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={updateUserAccountMutation.isLoading || assignRolesToUsersMutation.isLoading || removeRolesFromUserMutation.isLoading}
						disabled={updateUserAccountMutation.isLoading || assignRolesToUsersMutation.isLoading || removeRolesFromUserMutation.isLoading}
					/>
				)}
				{activeTab == Terms.Roles && (
					<RButton
						text={rolesEditMode ? tr("save") : tr("edit")}
						faicon={rolesEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={rolesEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={updateUserAccountMutation.isLoading || assignRolesToUsersMutation.isLoading || removeRolesFromUserMutation.isLoading}
						disabled={updateUserAccountMutation.isLoading || assignRolesToUsersMutation.isLoading || removeRolesFromUserMutation.isLoading}
					/>
				)}
				{activeTab == Terms.Permissions && (
					<RButton
						text={permissionsEditMode ? tr("save") : tr("edit")}
						faicon={permissionsEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={permissionsEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={
							updateUserAccountMutation.isLoading ||
							assignPermissionsToUsersMutation.isLoading ||
							removePermissionsFromUsersMutation.isLoading
						}
						disabled={
							updateUserAccountMutation.isLoading ||
							assignPermissionsToUsersMutation.isLoading ||
							removePermissionsFromUsersMutation.isLoading
						}
					/>
				)}
			</RFlex>
			{activeTab == Terms.BasicInfo &&
				(isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RUserBasicInfo
						values={values}
						touched={touched}
						errors={errors}
						handleChange={handleChange}
						handleBlur={handleBlur}
						userTypesCount={userTypesCount}
						handleAddingUserType={handleAddingUserType}
						userTypes={userTypes?.data?.data}
						handleSelectOption={handleSelectOption}
						handleSwitchChange={handleSwitchChange}
						handleAccountActiveChanged={handleAccountActiveChanged}
						accountActiveStatus={accountActiveStatus}
						gender={gender}
						setGender={setGender}
						generateEmailFromUsername={generateEmailFromUsername}
						suggestPassword={suggestPassword}
						setUploadId={setUploadId}
						handleSelectOrganization={handleSelectOrganization}
						currentOrganization={currentOrganization}
						pickedOrganization={pickedOrganization}
						hashId={hashId}
						scrollToTop={scrollToTop}
						editable={basicInfoEditMode}
					/>
				))}
			{activeTab == Terms.Roles &&
				(isLoadingRoles ? (
					<RLoader />
				) : (
					<RRoles
						roles={roles?.data?.data}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles}
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={permissions?.data?.data}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
					/>
				))}
		</RFlex>
	);
};

export default GViewUser;
