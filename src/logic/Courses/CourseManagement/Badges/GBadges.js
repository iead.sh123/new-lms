import React, { useEffect, useState } from "react";
import { baseURL, genericPath } from "engine/config";
import { useParams, useHistory } from "react-router-dom";
import { Row, Col } from "reactstrap";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import RBadges from "view/Courses/CourseManagement/Badges/RBadges";
import { useDispatch, useSelector } from "react-redux";
import Loader from "utils/Loader";
import { getBadgesAsync, removeBadgeAsync, storeBadgeAsync } from "store/actions/global/badges.actions";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RStoreBadge from "view/Courses/CourseManagement/Badges/RStoreBadge";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import iconsFa6 from "variables/iconsFa6";
import REmptyData from "components/RComponents/REmptyData";

const GBadges = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { courseId, curriculumId } = useParams();
	const [searchData, setSearchData] = useState("");
	const [alert, setAlert] = useState(false);
	const [openStoreBadge, setOpenStoreBadge] = useState(false);
	const [badgeById, setBadgeById] = useState(null);

	const { badges, badgesLoading, storeBadgeLoading } = useSelector((state) => state.badgesRed);
	const { user } = useSelector((state) => state.auth);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};
	const handleSearch = async (emptySearch) => {
		dispatch(getBadgesAsync(courseId, curriculumId, emptySearch !== undefined ? emptySearch : searchData));
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleOpenStoreModal = (data) => {
		setOpenStoreBadge(true);
		setBadgeById(data ? data : null);
	};
	const handleCloseStoreModal = () => {
		setOpenStoreBadge(false);
	};

	const handlePickFromCollaboration = () => {
		history.push(
			`${baseURL}/${genericPath}/collaboration_area_contents/${
				courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
			}/badges`
		);
	};

	useEffect(() => {
		dispatch(getBadgesAsync(courseId, curriculumId));
	}, []);

	const successDelete = async (prameters) => {
		dispatch(removeBadgeAsync(prameters.badgeId, hideAlert));
	};

	const handleRemoveBadge = (badgeId) => {
		const prameters = {
			badgeId,
		};
		const message = tr`Are you sure?`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleStoreBadge = (values) => {
		dispatch(storeBadgeAsync({ data: values }, handleCloseStoreModal));
	};

	return (
		<Row>
			{alert}

			<AppModal
				show={openStoreBadge}
				parentHandleClose={handleCloseStoreModal}
				// header={"Adder"}
				headerSort={<RStoreBadge storeBadgeLoading={storeBadgeLoading} badgeById={badgeById} handleStoreBadge={handleStoreBadge} />}
			/>
			<Col xs={12} md={8} className="mb-2">
				<RSearchHeader
					searchLoading={badgesLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					addNew={user?.type == "student" ? false : true}
					buttonName={tr`add_new_badge`}
					outline={false}
					handleOpenModal={handleOpenStoreModal}
				/>
			</Col>
			{user?.type !== "student" && (
				<Col xs={12} md={4} className="d-flex justify-content-end mb-2 pr-0 pl-0">
					<RButton
						text={tr`pick_from_collaboration`}
						faicon={iconsFa6.globe}
						color={"primary"}
						outline
						onClick={handlePickFromCollaboration}
					/>
				</Col>
			)}
			{badgesLoading ? (
				<Loader />
			) : (
				<Col xs={12}>
					{badges && badges.length > 0 ? (
						<RBadges
							badges={badges}
							handleRemoveBadge={handleRemoveBadge}
							handleOpenStoreModal={handleOpenStoreModal}
							userType={user?.type}
						/>
					) : (
						<REmptyData />
					)}
				</Col>
			)}
		</Row>
	);
};

export default GBadges;
