import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import RNavIcon from "components/Global/RComs/RNavIcon/RNavIcon";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { rubricApi } from "api/global/rubric";
import Loader from "utils/Loader";
import RLister from "components/Global/RComs/RLister";
import { isConstructorDeclaration } from "typescript";

const GStudentsDetailsByRubric = () => {
	const history = useHistory();
	const { courseId, curriculumId, cohortId, rubricId } = useParams();

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["studentsDetails", courseId ?? curriculumId, rubricId],
		queryFn: () => rubricApi.studentsDetails(courseId ?? curriculumId, rubricId),
		enableCondition: rubricId != null,
	});

	const _records = React.useMemo(
		() =>
			data?.data?.data[0]?.users?.map((user) => ({
				details: [{ key: tr`students`, value: user?.full_name }],
			})),
		[data?.data?.data]
	);

	if (isLoading && rubricId != null) return <Loader />;

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ alignItems: "center" }}>
				<RNavIcon linkText={tr`back_to_all_rubric`} linkIcon={iconsFa6.arrowLeft} href={""} />{" "}
				{/* <RSearchHeader
					searchLoading={searchLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
				/> */}
			</RFlex>

			<RLister Records={_records} />
		</RFlex>
	);
};

export default GStudentsDetailsByRubric;
