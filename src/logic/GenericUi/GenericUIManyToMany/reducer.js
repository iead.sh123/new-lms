import { produce, current } from "immer";
import {
  ADD_RELATION,
  DELETE_RELATION,
  CANCEL_ADD_RELATION,
  UPDATE_ATTRIBUTE,
  SET_STAGE,
  INITIALIZE_STATE,
} from "./actions";
import { STAGE1, STAGE2, STAGE3 } from "./stageManager";
import { getDefaultValue } from "./utils";

export const initialState = {
  isManyToMany: null,
  loading: false,
  currentStage: STAGE1,
  leftSideId: null, //just an id
  selectedDetailId: null,
  previousStage: null,
  pivotRecords: null, // [
  // {
  // id
  // name
  // }
  //],
  tableRecords: null, //[

  // ],
  rightSide: [], // [
  // {
  //     detailId: null,
  //     item:{
  //         name: value,
  //         name:value
  //     }
  // }
  // ],
  relationPropsSchemes: [
    // {
    //     "Field": "active",
    //     "Title": "active",
    //     "Type": "tinyint(1)",
    //     "Nullability": "NO",
    //     "IsDropDownList": true,
    //     "data": [{
    //         ID: 1,
    //         name: 'test'
    //     }]
    // }
  ],
};

const attributesIdsGenerator = (function* () {
  const i = 0;
  while (true) yield `att-${++i}`;
})();

export default (state = initialState, action) => {
  const { type, payload } = action;
  let temp = null,
    temp2 = null;
  return produce(state, (draft) => {
    switch (type) {
      //global
      case INITIALIZE_STATE:
        draft[payload.fieldName] = payload.value;
        return draft;
      //relations
      case ADD_RELATION:
        temp = draft.rightSide.find((rs) => rs.detailId === payload.detailId);
        draft.selectedDetailId = payload.detailId;
        draft.isManyToMany &&
          current(draft.relationPropsSchemes).length > 0 &&
          (draft.currentStage = STAGE3);

        if (temp) {
          return;
        }

        temp2 = {};

        if (draft.isManyToMany)
          for (let i in draft.relationPropsSchemes) {
            temp2[draft.relationPropsSchemes[i].Field] = getDefaultValue(
              draft.relationPropsSchemes[i].Type
            );
          }

        draft.rightSide.push({
          detailId: payload.detailId,
          item: temp2,
        });

        return draft;

      case CANCEL_ADD_RELATION:
        draft.rightSide.pop();
        return draft;

      case DELETE_RELATION:
        temp = draft.rightSide.findIndex(
          (rs) => rs.detailId === payload.detailId
        );
        draft.rightSide.splice(temp, 1);
        return draft;

      //attributes

      case UPDATE_ATTRIBUTE:
        temp = draft.rightSide.find((rs) => rs.detailId === payload.detailId);
        temp.item[payload.attributeName] = payload.value;
        return draft;

      case SET_STAGE:
        draft.previousStage = draft.currentStage;
        draft.currentStage = payload;
        if (payload === STAGE1) {
          draft.rightSide = [];
        }
        return draft;

      default:
        return draft;
    }
  });
};
