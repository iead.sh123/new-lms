import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSharedContentCard from "components/Global/RComs/Collaboration/RSharedContentCard";
import RCoursesTree from "view/CoursesManager/RCoursesTree";
import RCourseCard from "components/Global/RComs/Collaboration/RCourseCard";
import Select from "react-select";
import RTags from "components/Global/RComs/RTags";
import tr from "components/Global/RComs/RTranslator";
import { post } from "config/api";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { redirect } from "./collaborationUtils";
import produce from "immer";
import RMulticheckboxDropdown from "components/Global/RComs/RMulticheckboxDropdown";
import RRow from "view/RComs/Containers/RRow";
import { pick } from "highcharts";
import RButtonIcon from "components/Global/RComs/RButtonIcon";
import { faHelicopter } from "@fortawesome/free-solid-svg-icons";
import placeholder from "assets/img/placeholder.jpg";
//-----------------------------------------------------
import { useReducer } from "react";
import RSubmit from "components/Global/RComs/RSubmit";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { SUCCESS } from "utils/Alert";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GLessonPlanViewer from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanViewer";
import GCollaborationViewer from "./GCollaborationViewer";
import { ElementScrollController } from "@fullcalendar/core/internal";
import { toast } from "react-toastify";
// Define the reducer function
const reducer = (state, action) => {
	switch (action.type) {
		case "SET_SHARABLE_CONTENTS_DATA_1":
			return { ...state, sharableContentsData1: action.payload };
		case "SET_REFRESH_RECORDS":
			return { ...state, refreshRecords: action.payload };
		case "SET_SHARABLE_CONTENT_RECORDS":
			return { ...state, sharableContentRecords: action.payload };
		case "SET_SELECTED_RECORDS":
			return { ...state, selectedRecords: action.payload };
		case "TOGGLE_SELECTED":
			Helper.cl(action.payload.sel ? "true" : "false", "onclick1 sel");
			Helper.cl(state.selectedRecords, "onclick1 selectedRecords");
			Helper.cl(action.payload.id, "onclick1 action.payload.id");
			const temp = produce(state, (temp_state) => {
				if (temp_state.selectedRecords.filter((s) => s == action.payload.id)?.length)
					temp_state.selectedRecords = temp_state.selectedRecords.filter((s) => s != action.payload.id);
				else temp_state.selectedRecords = [...temp_state.selectedRecords, action.payload.id];

				temp_state.sharableContentsData1.map((s) => {
					s.selected = temp_state.selectedRecords.filter((sr) => sr == s.id).length;
				});
			});

			Helper.cl(temp, "onclick1 temp");
			return temp;

		case "SET_CURRENT_CONTENT_DATA":
			return { ...state, currentContentData: action.payload };
		case "SET_CURRENT_CONTENT":
			return { ...state, currentContent: action.payload };
		case "SET_ID":
			return { ...state, id: action.payload };
		case "SET_OPEN_MODAL_TO_VIEW":
			return { ...state, openModalToView: action.payload };
		case "SET_CATEGORY_ID":
			return { ...state, category_id: action.payload };
		case "SET_COURSES_CONTENTS_DATA":
			return { ...state, coursesContentsData: action.payload };
		case "SET_REFRESH_COURSES":
			return { ...state, refreshCourses: action.payload };
		case "SET_COURSES_RECORDS":
			return { ...state, coursesRecords: action.payload };
		case "SET_TREE_DATA":
			return { ...state, treeData: action.payload };
		case "SET_CONTENT_TYPES":
			return { ...state, contentTypes: action.payload };
		case "SET_CONTENT_TYPES_RECORDS":
			return { ...state, contentTypesRecords: action.payload };
		case "SET_REFRESH_CONTENT_TYPES":
			return { ...state, refreshContentTypes: action.payload };
		case "SET_PROVIDERS_DATA":
			return { ...state, providersData: action.payload };
		case "SET_SELECTED_PROVIDERS":
			return { ...state, selectedProviders: action.payload };
		case "SET_TAGS":
			return { ...state, tags: action.payload };
		// Add more cases for other state updates...
		default:
			return state;
	}
};

// Initial state
const initialState = {
	sharableContentsData1: [],
	refreshRecords: false,
	sharableContentRecords: [],
	selectedRecords: [],

	currentContentData: {},
	currentContent: 0,
	id: null,
	openModalToView: false,
	category_id: [],
	coursesContentsData: [],
	refreshCourses: false,
	coursesRecords: [],
	treeData: [],
	contentTypes: [],
	contentTypesRecords: [],
	refreshContentTypes: true,
	providersData: [],
	selectedProviders: [],
	tags: [],
};

//----------------------------------------------------
export const GCollaborationAreaContents = () => {
	const setCurrentContentData = (newData) => {
		dispatch({ type: "SET_CURRENT_CONTENT_DATA", payload: newData });
	};

	const setCurrentContent = (newContent) => {
		dispatch({ type: "SET_CURRENT_CONTENT", payload: newContent });
	};

	const setId = (newId) => {
		dispatch({ type: "SET_ID", payload: newId });
	};

	const setOpenModalToView = (newState) => {
		dispatch({ type: "SET_OPEN_MODAL_TO_VIEW", payload: newState });
	};

	const setCategoryId = (newCategoryId) => {
		dispatch({ type: "SET_CATEGORY_ID", payload: newCategoryId });
	};

	const setCoursesContentsData = (newData) => {
		dispatch({ type: "SET_COURSES_CONTENTS_DATA", payload: newData });
	};

	const setRefreshCourses = (newState) => {
		dispatch({ type: "SET_REFRESH_COURSES", payload: newState });
	};

	const setCoursesRecords = (newRecords) => {
		dispatch({ type: "SET_COURSES_RECORDS", payload: newRecords });
	};

	const setTreeData = (newData) => {
		dispatch({ type: "SET_TREE_DATA", payload: newData });
	};

	const setContentTypes = (newTypes) => {
		dispatch({ type: "SET_CONTENT_TYPES", payload: newTypes });
	};

	const setContentTypesRecords = (newRecords) => {
		dispatch({ type: "SET_CONTENT_TYPES_RECORDS", payload: newRecords });
	};

	const setRefreshContentTypes = (newState) => {
		dispatch({ type: "SET_REFRESH_CONTENT_TYPES", payload: newState });
	};

	const setProvidersData = (newData) => {
		dispatch({ type: "SET_PROVIDERS_DATA", payload: newData });
	};

	const setSelectedProviders = (newProviders) => {
		dispatch({ type: "SET_SELECTED_PROVIDERS", payload: newProviders });
	};

	const setTags = (newTags) => {
		dispatch({ type: "SET_TAGS", payload: newTags });
	};
	const [selectedOptions, setSelectedOptions] = useState([]);
	const [searchPayload, setSearchPayload] = useState([]);
	const [providers, setProviders] = useState([]);

	//---------------------------------------------------------------------------------
	const params = useParams();

	//Helper.cl(params,"col area prov params");
	const agreement_id = params.agreement_id;
	const context_name = params.context_name;
	const context_id = params.context_id;
	const content_type = params.content_type;
	const singleSelect = params.singleSelect && params.singleSelect != "0";

	const handleOpenModal = (ID) => {
		setOpenModalToView(true);
		setId(ID);
	};
	const handleCloseModal = () => {
		setOpenModalToView(false);
	};

	// Use the useReducer hook
	const [state, dispatch] = useReducer(reducer, initialState);

	// Destructure state variables
	const { sharableContentsData1, refreshRecords, sharableContentRecords, selectedRecords } = state;
	const {
		currentContentData,
		currentContent,
		id,
		openModalToView,
		category_id,
		coursesContentsData,
		refreshCourses,
		coursesRecords,
		treeData,
		contentTypes,
		contentTypesRecords,
		refreshContentTypes,
		providersData,
		selectedProviders,
		tags,
	} = state;
	// Example usage: dispatching actions
	const setSharableContentsData1 = (data) => dispatch({ type: "SET_SHARABLE_CONTENTS_DATA_1", payload: data });
	const setRefreshRecords = (value) => dispatch({ type: "SET_REFRESH_RECORDS", payload: value });
	const setSharableContentRecords = (data) => dispatch({ type: "SET_SHARABLE_CONTENT_RECORDS", payload: data });
	const setSelectedRecords = (data) => dispatch({ type: "SET_SELECTED_RECORDS", payload: data });

	//-
	const getSharableContentsFromBackend = (content_id) => async (specific_url) => {
		// const url = Services.collaboration.backend+`api/collaboration-area/agreement/${params.agreement_id}/sharable-contents/`;
		const tagids = tags.map((t) => t.value);
		const provids = providersData.map((p) => p.value);
		const ags2 = selectedProviders.map((p) => p.agreement_id);
		const s = {
			tag_ids: tagids,
			agreement_ids: ags2, // providersData.map((s) => s.agreement_id),
		};

		const url =
			Services.collaboration.backend +
			`api/collaboration-area/` +
			(agreement_id && content_id
				? `agreement/${agreement_id}/sharable-records/${content_id}`
				: content_id
				? `sharable-contents/${content_id}/records`
				: "sharable-contents");

		let response1;
		if (agreement_id) response1 = await get(url);
		else response1 = await post(url, s);
		if (response1 && response1.status) {
			return response1;
		}
		return null;
	};
	const setSharableContentsData = (response) => {
		Helper.cl("sh1 f2");
		if (response?.data?.data?.data) {
			setSharableContentsData1(response?.data?.data?.data.map((d) => ({ ...d, selected: false })));
		}
	};

	useEffect(() => {
		setSharableContentsData1(
			produce(sharableContentsData1, (tempData) => {
				tempData
					.filter((s) => selectedRecords.filter((ss) => ss.id == s.id).length)
					.map((f) => {
						f.selected = true;
					});
				Helper.cl(tempData.selected);
			})
		);
	}, [selectedRecords]);

	const toggleSelected = (sel, id) => {
		dispatch({
			type: "TOGGLE_SELECTED",
			payload: { id: id, sel: sel },
		});
	};

	useEffect(() => {
		Helper.cl(sharableContentsData1, "sharableContentsData1");
		setSharableContentRecords(
			sharableContentsData1.map((r, index) => {
				Helper.cl(r.tags, "ttt r.tags");
				//const actions=getActions(r.id,r.sharable_status,r.sharable_status?.pivot,r.sharable_status?.pivot?.need_approval)
				return {
					id: r.id,
					specialProps: {
						title: { text: r.name },
						tags: r?.tags?.slice(0, 2)?.map((t, i) => {
							return {
								...t,
								backgroundColor: i == 1 ? "#FEF1EC" : "#D9E5FE",
								iconColor: i == 1 ? "#F34400" : "#0051FE",
							};
						}),
						selected: r.selected,
						multiSelect: !singleSelect,
						onClick1: (sel) => {
							toggleSelected(sel, r.id);
						},

						Author: { image: r.organization_image, title: r.organization_name },
						Actions: [
							{
								onClick: () => {
									// viewContent(r.id);
									handleOpenModal(r.id);
								},
								title: "view",
								icon: "fa fa-eye",
							},

							...(!agreement_id && singleSelect
								? [
										{
											onClick: () => {
												importContent(r.id);
											},
											title: "import",
											icon: "fas fa-file-import",
										},
								  ]
								: []),
						],
					},
				};
			})
		);
	}, [sharableContentsData1]);

	//-------------------------------------------------------------------

	//----------------------------------------------------------Courses--Work Area

	const viewCourse = async (id) => {
		redirect(history, "courses", id);
		if (currentContentData.table_name == "courses") {
			// history.push(`${baseURL}/${genericPath}/course/${id}`);
			redirect(history, "courses", id);
		}
	};

	const importCourse = async (id) => {
		importContent(id);
	};
	const history = useHistory();

	const viewContent = (id) => {
		Helper.cl(id, "clickContent id");
		Helper.cl(currentContentData, "clickContent currentContentData");

		if (agreement_id) {
			// view
			if (currentContentData.table_name == "lesson_plans") {
				history.push(`${baseURL}/${genericPath}/course/${id}/lesson-plan`);
			}
			if (currentContentData.table_name == "courses") {
				history.push(`${baseURL}/${genericPath}/course/${id}/lesson-plan`);
			}
		}
	};

	const importContent = async (id) => {
		const payload = {
			record_ids: [id],
			context_name: context_name,
			context_id: context_id,
		};
		const url = Services.collaboration.backend + `api/collaboration-area/sharable_content/${currentContent}/import`;
		let response1 = await post(url, payload);
		if (response1) {
			Helper.cl("sh1 f1");
			if (response1.data.status) {
				toast.success(tr`imported successfully`);
				history.goBack();
			} else {
				toast.error(tr`Item Did Not Imported`);
			}

			return response1;
		} else toast.error(tr`Item Did Not Imported`);
	};

	const importSelected = async () => {
		const payload = {
			record_ids: selectedRecords,
			context_name: context_name,
			context_id: context_id,
		};
		const url = Services.collaboration.backend + `api/collaboration-area/sharable_content/${currentContent}/import`;
		let response1 = await post(url, payload);
		if (response1) {
			Helper.cl("sh1 f1");
			if (response1.data.status) {
				toast.error(tr`imported Successfully`);
			} else {
				toast.error(tr`Items Did Not Imported`);
			}

			return response1;
		} else toast.error(tr`Items Did Not Imported`);
	};

	const getCoursesFromBackend = (category_id) => async (specific_url) => {
		Helper.cl(category_id, "getCoursesFromBackend");
		if (!category_id || category_id == 0) return;
		if (agreement_id) {
			const url =
				Services.collaboration.backend +
				`api/collaboration-area/agreement/${params.agreement_id}/sharable-content/4/category/${category_id}`;
			let response1 = await get(specific_url ? specific_url : url);
			if (response1) {
				Helper.cl("sh1 f1");
				return response1;
			}
		} else {
			Helper.cl(treeData, "treeData");
			let agid = findAgreementIdByCategoryId(treeData, category_id); //ags2=selectedProviders.map(p=>p.agreement_id)

			Helper.cl(agid, "////////////////////////////////"); ////////////////////
			const url = Services.collaboration.backend + `api/collaboration-area/agreement/${agid}/sharable-content/4/category/${category_id}`;
			let response1 = await get(specific_url ? specific_url : url);
			if (response1) {
				Helper.cl("sh1 f1");
				return response1;
			}
		}
	};
	const setCoursesContentsData1 = (response) => {
		Helper.cl("sh1 f2");
		if (response?.data?.data) setCoursesContentsData(response?.data?.data);
	};
	useEffect(() => {
		Helper.cl(coursesContentsData, "coursesContentsData");
		setCoursesRecords(
			coursesContentsData.map((r, index) => {
				Helper.cl(r, "setCoursesRecords contents r");
				return {
					id: r.id,
					specialProps: {
						title: { text: r.name },
						//UpLeftText:"All:"+r.count,
						//Actions: actions,
						//subtitle :{ text:(r.count??0)+" all,"+(r.count??0)+" recent"},
						//bottomBorderColor:r.sharable_status?"gray":"gray",
						onClick1: () => {
							viewCourse(r.id);
						},
						tags: r.tags,
						//Author
						bgImage: Services.storage.file + r.image,
						multiSelect: !singleSelect,
						actions: [
							{
								onClick: () => {
									if (agreement_id) viewCourse(r.id);
									else importCourse(r.id);
								},
								title: agreement_id ? "view" : "import",
								text: agreement_id ? "view" : "import",
								icon: agreement_id ? "fa fa-eye" : "fas fa-file-import",
							},

							...(!agreement_id && singleSelect
								? [
										{
											onClick: () => {
												viewCourse(r.id);
											},
											title: "view",
											text: "import",
											icon: "fas fa-file-import",
										},
								  ]
								: []),
						],
					},

					name: "course2",
				};
			})
		);
	}, [coursesContentsData]);

	useEffect(() => {
		Helper.cl(category_id, "setRefreshcourss categroy id");
		setRefreshCourses(!refreshCourses);
	}, [category_id]);
	//---------------------------------------------------------------- for sepcific agreement

	const findInChidren = (children, categoryId) => {
		Helper.cl(children, "input-find1--children");
		Helper.cl(categoryId, "input-find1--categoryId");

		if (children && children.length) {
			Helper.cl("find1.1--");
			for (let i = 0; i < children.length; i++) {
				let child = children[i];
				Helper.cl(child, "find1.2--child");
				if (child?.id == categoryId) {
					Helper.cl(child?.id, "here we go ");
					return child?.id;
				} else Helper.cl(child?.id, "find2--");
				const res = findInChidren(child?.children, categoryId);
				if (res) {
					Helper.cl(res, " we go again");
					return res;
				} else Helper.cl("null--find1.2--");
			}
		} else return null;
	};

	const findAgreementIdByCategoryId = (data, categoryId) => {
		Helper.cl(categoryId, "search input categoryId");
		let cat = null;
		data.map((o) => {
			if (!cat) {
				const oid = findInChidren(o.children, categoryId);
				Helper.cl(providersData, "providers");
				Helper.cl(oid, "oid");
				if (oid) {
					cat = providersData.find((p) => p.id == o.id)?.agreement_id;
				}
				Helper.cl(cat, "cat");
			}
		});
		return cat;
	};
	const getTree = async () => {
		if (params.agreement_id) {
			const url =
				Services.collaboration.backend + `api/collaboration-area/categories-tree/agreement/${params.agreement_id}/sharable-content/4`;
			const res = await get(url);
			if (res && res.data?.status && res.data?.data) {
				setTreeData(res.data?.data);
			} else {
				Helper.cl(url, "tree1 url");
				Helper.cl(res, "tree1 res");
			}
		} else {
			const tagids = tags.map((t) => t.value);
			const provids = providersData.map((p) => p.value);
			const ags2 = selectedProviders.map((p) => p.agreement_id);
			const s = {
				tag_ids: tagids,
				agreement_ids: ags2, // providersData.map((s) => s.agreement_id),
			};
			const url = Services.collaboration.backend + `api/collaboration-area/sharable-contents/4/records`;
			let response1;
			response1 = await post(url, s);

			if (response1 && response1.data?.status && response1.data?.data) {
				setTreeData(response1.data?.data);
			}
		}
	};
	const clickContentType = (ct) => {
		if (ct == 4) {
			getTree();
		}
	};
	// useEffect(() => {

	// 	// alert(currentContent,"current contnet changedd")

	// 	Helper.cl(currentContent, "tree1 currentContent");

	// }, [currentContent]);

	const getContentTypesFromBackend = async (specific_url) => {
		if (!agreement_id) {
			setContentTypesRecords([]);
			setContentTypes([]);
			return null;
		}
		const url = Services.collaboration.backend + `api/collaboration-area/agreement/${params.agreement_id}/sharable-contents/`;
		Helper.cl(url, "co url");
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			return response1;
		}
	};

	useEffect(() => {
		Helper.cl("contnet Types Changed");
		const fill = (r, index) => {
			return {
				id: r.id,
				specialProps: {
					title: { text: r.name },
					subtitle: {
						text: r?.counts?.all + " all |" + r?.counts?.recent + " recent",
					},
					bottomBorderColor: r.sharable_status ? "#eeeeee" : "white",
					onClick1: () => {
						setCurrentContentData(r);
						setCurrentContent(r.id);
						clickContentType(r.id);
					},
					borderLess: true,
					focused: r.focused,
					padding: "1px",
					margin: "1px",
				},
			};
		};
		let recs = content_type ? contentTypes.filter((c) => c.table_name == content_type).map(fill) : contentTypes.map(fill);
		Helper.cl(recs, "colarea recs ");
		setContentTypesRecords(recs);
		//!currentContent&&
		const dd = contentTypes.filter((i) => i.id == recs?.[0]?.id);
		//!currentContent &&
		if (!currentContentData || !agreement_id) {
			setCurrentContent(dd && dd.length && dd[0].id);
			setCurrentContentData(dd && dd.length && dd[0]);
		}
		Helper.cl("set refresh records");
		setRefreshRecords(!refreshRecords);
	}, [contentTypes]);

	const setContentTypes1 = (response) => {
		if (response?.data?.data) setContentTypes(response?.data?.data);
	};
	//--------------------------------------------------Provider select

	useEffect(() => {
		const getProviders = async (specific_url) => {
			if (params.agreement_id) {
				const url1 = Services.collaboration.backend + `api/collaboration-agreement/${params.agreement_id}`;
				Helper.cl(url1, "co url");
				let response2 = await get(specific_url ? specific_url : url1);
				if (response2) {
					Helper.cl(response2, "response2");
					setProvidersData([response2?.data?.data?.agreement?.source_organization]);
					setSelectedProviders([response2?.data?.data?.agreement?.source_organization]);
					return;
				}
			} else {
				const url = Services.collaboration.backend + `api/collaboration-area/providers`;
				Helper.cl(url, "co url");
				let response1 = await get(specific_url ? specific_url : url);
				if (response1) {
					setProvidersData(response1?.data?.data);
					setSelectedProviders(response1?.data?.data);
				}
			}
		};

		getProviders();
	}, []);

	const handleChange = (selectedOptions) => {
		Helper.cl(selectedOptions, "selected options,handle change");
		setSelectedOptions(selectedOptions);
	};

	const searchAuto = async () => {
		if (agreement_id) return;
		if (ags2?.length < 1) {
			//setContentTypes([]);
			return;
		}
		const tagids = tags.map((t) => t.value);
		const provids = providersData.map((p) => p.value);
		const ags2 = selectedProviders.map((p) => p.agreement_id);
		const url = Services.collaboration.backend + `api/collaboration-area/sharable-contents`;
		const s = {
			tag_ids: tagids,
			agreement_ids: ags2, // providersData.map((s) => s.agreement_id),
		};
		let response1 = await post(url, s);

		if (response1) {
			if (response1?.data?.data) setContentTypes(response1?.data?.data?.sharable_contents);
		}
	};
	useEffect(() => {
		//if(selectedProviders&&selectedProviders.length>0)
		if (agreement_id) return;
		if (!selectedProviders || selectedProviders.length == 0) return;
		searchAuto();
	}, [tags, selectedProviders]);

	const search = async () => {
		const tagids = tags.map((t) => t.value);
		const provids = providersData.map((p) => p.value);
		const ags2 = selectedProviders.map((p) => p.agreement_id);

		const url = Services.collaboration.backend + `api/collaboration-area/sharable-contents`;

		Helper.cl(url, "co url");
		const s = {
			tag_ids: tagids,
			agreement_ids: ags2, // providersData.map((s) => s.agreement_id),
		};
		//setSearchPayload(s);
		let response1 = await post(url, s);

		if (response1) {
			Helper.cl(response1, "search Response");
			if (response1?.data?.data) setContentTypes(response1?.data?.data?.sharable_contents);
		}
	};

	useEffect(() => {
		setContentTypes(
			produce(contentTypes, (tempContentTypes) => {
				tempContentTypes.map((c) => {
					c.focused = false;
				});
				const ss = tempContentTypes.filter((c) => currentContent == c.id);
				if (ss && ss.length) ss[0].focused = true;
			})
		);
	}, [currentContent]);

	//----------------------------------------------------------------
	const setSelectedProviders1 = (new_value) => {
		//redirect somewhere

		Helper.cl(providersData, "setRecordLevels providersData");

		Helper.cl(new_value, "setRecordLevels new_value");
		Helper.cl(
			new_value.filter((ll) => ll.checked),
			"setRecordLevels new_value.filter(ll=>ll.checked)"
		);
		Helper.cl(
			new_value.filter((ll) => ll.checked)?.filter((ll) => ll.id == 1),
			"new_value.filter(ll=>ll.checked)?.filter(ll=>ll.id==p.id)"
		);
		//const selectedProivders=new_value.filter(ll=>ll.checked).map(l=>({...l,id:l.value}));
		//Helper.cl(selectedProivders,"setRecordLevels selectedProivders")

		// const ppp=produce(s,temp_data=>{
		//   const firsthit=temp_data.filter(i=>i.id==record_id)?.[0];
		//   firsthit.levels=selectedLevels;
		// });
		setSelectedProviders(providersData.filter((p) => new_value.filter((ll) => ll.checked)?.filter((ll) => ll.value == p.id).length > 0));
		//r.levels.filter(ll=>ll.id==l.id).length>0
		// Helper.cl(itemsData2,"setRecordLevels itemsData2")
		// Helper.cl(itemsData1,"setRecordLevels setItemsData1")
	};
	return (
		<div>
			{/* {currentContent} */}
			<AppModal
				size="xl"
				show={openModalToView}
				parentHandleClose={handleCloseModal}
				headerSort={<GCollaborationViewer tableName={currentContentData.table_name ?? content_type} id={id} handleImport={importContent} />}
			/>
			<div>
				<RFlex styleProps={{ alignItems: "center" }}>
					<RFlex
						id="selectedProvidersContainer"
						style={{
							height: "40px",
							width: selectedProviders?.length * 35 + 20 + "px",
						}}
					>
						{selectedProviders?.map((l, i) => (
							<div
								style={{
									position: "absolute",
									display: "inline-block",
									left: 45 + 35 * i + "px",
								}}
							>
								{i < 2 ? (
									<img
										src={l.image ? Services.storage.file + l.image : placeholder}
										style={{
											border: "blue 1px solid",
											height: "45px",
											borderRadius: "50%",
										}}
									/>
								) : i == 2 ? (
									<div
										style={{
											border: "blue 1px solid",
											height: "45px",
											width: "45px",
											borderRadius: "50%",
											fontSize: "+27px",
											background: "lightblue",
											color: "white",
											textAlign: "center",
										}}
									>
										+{selectedProviders?.length - 2}
									</div>
								) : null}
							</div>
						))}
					</RFlex>
					{agreement_id && (
						<div>
							{selectedProviders?.map((l, i) => (
								<>{l.name}</>
							))}
						</div>
					)}
					{!agreement_id && (
						<RMulticheckboxDropdown
							options={providersData.map((l) => {
								return {
									label: (
										<RFlex>
											<img src={l.image ? Services.storage.file + l.image : placeholder} style={{ height: "30px", borderRadius: "50%" }} />
											{l.name}
										</RFlex>
									),
									value: l.id,
									checked: selectedProviders?.filter((p) => p.id == l.id).length > 0,
								};
							})}
							onChange={(e) => {
								setSelectedProviders1(e);
							}}
							// disabled={state!="nominate"}
						/>
					)}
				</RFlex>

				{!agreement_id && <label>{tr("tags")}</label>}
				{!agreement_id && (
					<div style={{ display: "flex" }}>
						<div style={{ width: "95%" }}>
							<RTags
								url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
								getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data?.data : null)}
								getValueFromArrayItem={(i) => i.id}
								getLabelFromArrayItem={(i) => i.name}
								getIdFromArrayItem={(i) => i.id}
								onChange={(data) => {
									setTags(data);
								}}
							/>
						</div>
						<div style={{ margin: "10px" }}>
							<RButtonIcon
								onClick={search}
								style={{
									border: "red 0px solid",
									position: "relative",
									width: "100%",
									backgroundColor: "inherit",
								}}
								ButtonAdditionalStyle={{ border: "0px", background: "inherit" }}
								color={"red"}
								backgroundColor={"white"}
								text={"delete"}
								padding={"1px"}
								faicon={"search"}
							></RButtonIcon>
						</div>
					</div>
				)}
			</div>
			<RFlex>
				<RFlex style={{ width: "150px" }}>
					{/* shared contents
        {currentContent} */}

					<RAdvancedLister
						hideTableHeader={false}
						colorEveryOtherRow={false}
						//firstCellImageProperty={"image"}
						getDataFromBackend={getContentTypesFromBackend}
						setData={setContentTypes1}
						records={contentTypesRecords}
						refresh={refreshContentTypes}
						getDataObject={(response) => response.data?.data?.data}
						characterCount={15}
						marginT={"mt-3"}
						marginB={"mb-2"}
						align={"left"}
						SpecialCard={RCollaborationCard}
						showListerMode={"cardLister"}
						perLine={1}
					/>
				</RFlex>
				{/* second column */}
				{currentContent != 4 ? (
					<></>
				) : (
					<RFlex id="tree_container" style={{ width: "300px" }}>
						{/* {Helper.js(treeData)} */}
						<RCoursesTree
							parentId={null}
							courseCategories={treeData}
							level={1}
							handleAddCategoryToTree={() => {}}
							handleRemoveCategoryFromTree={() => {}}
							addEditCategoryToTreeLoading={() => {}}
							formGroup={null}
							handleChange={() => {
								//handleChange
							}}
							editable={false}
							courseOnClick={(course_id) => {
								//alert(course_id);
							}}
							categoryOnClick={(category_id) => {
								setCategoryId(category_id);
							}}
						/>
					</RFlex>
				)}
				{/* work area */}

				<RFlex style={{ width: "100%" }}>
					{currentContent && currentContent == 4 ? (
						<div>
							<RAdvancedLister
								hideTableHeader={false}
								colorEveryOtherRow={false}
								//firstCellImageProperty={"image"}
								getDataFromBackend={getCoursesFromBackend(category_id)}
								setData={setCoursesContentsData1}
								records={coursesRecords}
								getDataObject={(response) => response.data?.data}
								characterCount={15}
								refresh={refreshCourses}
								marginT={"mt-3"}
								marginB={"mb-2"}
								align={"left"}
								SpecialCard={RCourseCard}
								showListerMode={"cardLister"}
								perLine={2}
								getTotal={(dataObject) => {
									Helper.cl(dataObject, "total response.data?.data?");
									const t = dataObject.total;
									Helper.cl(t, "total");
									return t;
								}}
							/>
						</div>
					) : (
						<>
							{/* {Helper.js(selectedRecords)} */}
							{!agreement_id && !singleSelect ? (
								<div
									className="row"
									style={{
										display: "flex",
										justifyContent: "flex-end",
									}}
								>
									<RSubmit
										value={"import"}
										disabled={selectedRecords?.length == 0}
										icon={"fa fa-plus"}
										onClick={() => {
											importSelected();
										}}
										type="button"
										style={{ borderRadius: "2px", borderColor: "#668ad7" }}
									/>
								</div>
							) : (
								<></>
							)}
							<RAdvancedLister
								hideTableHeader={false}
								colorEveryOtherRow={false}
								//firstCellImageProperty={"image"}
								getDataFromBackend={getSharableContentsFromBackend(currentContent)}
								setData={setSharableContentsData}
								records={sharableContentRecords}
								getDataObject={(response) => response.data?.data?.data}
								characterCount={15}
								refresh={refreshRecords}
								marginT={"mt-3"}
								marginB={"mb-2"}
								align={"left"}
								SpecialCard={RSharedContentCard}
								showListerMode={"cardLister"}
								perLine={3}
								getTotal={(dataObject) => {
									Helper.cl(dataObject, "total response.data?.data?");
									const t = dataObject.total;
									Helper.cl(t, "total");
									return t;
								}}
								cardsJustifyContents="flex-start"
							/>{" "}
						</>
					)}
				</RFlex>
			</RFlex>
		</div>
	);
};
