import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import styles from "../QuestionSet.module.scss";
import tr from "components/Global/RComs/RTranslator";
import RTags from "components/Global/RComs/RTags";
import { Services } from "engine/services";
import RSelect from "components/Global/RComs/RSelect";

const RQuestionsBankFilters = () => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	const questionsTypeOptions = ({ innerProps, data }) => {
		return (
			<div key={data.value} className="pl-3 pr-3 pt-3 cursor-pointer" {...innerProps}>
				<RFlex>
					<i className={data.icon} />
					<span>{tr(data.label)}</span>
				</RFlex>
			</div>
		);
	};

	const authorOptions = ({ innerProps, label, data }) => {
		return (
			<RFlex key={data.value} className="pl-3 pr-3 pt-3 align-items-center cursor-pointer" {...innerProps}>
				{data.hash_id == null ? (
					""
				) : (
					<img className={styles.answer__image} src={Services.storage.file + data.hash_id} alt={data.label} width="40px" height="40px" />
				)}
				<p>{label}</p>
			</RFlex>
		);
	};

	return (
		<>
			<RFlex styleProps={{ flexWrap: "wrap", alignItems: "center" }}>
				{/* <div style={{ width: "300px" }}> */}
				<RSearchHeader
					searchLoading={QuestionsBankData.questionsBankLoading}
					searchData={QuestionsBankData.questionsBankFilters?.searchText}
					handleSearch={(emptySearch) => QuestionsBankData.handleFilter(emptySearch)}
					handleChangeSearch={(value) => QuestionsBankData.handleSetFilters(value, "searchText")}
					addNew={false}
					outline={false}
					inputWidth={"380px"}
					setSearchData={(value) => QuestionsBankData.handleSetFilters(value, "searchText")}
				/>
				{/* </div> */}
				<div style={{ width: "300px" }}>
					<RSelect
						option={QuestionsBankData.questionTypesEnum}
						closeMenuOnSelect={false}
						defaultValues={QuestionsBankData.questionsBankFilters?.questionTypes}
						placeholder={tr`select_question_type`}
						onChange={(values) => {
							QuestionsBankData.handleSetFilters(
								values.map((item) => item.value),
								"questionTypes"
							);
						}}
						isMulti
						customOption={questionsTypeOptions}
					/>
				</div>
				<div style={{ width: "300px" }}>
					<RSelect
						option={[]}
						closeMenuOnSelect={false}
						defaultValues={QuestionsBankData.questionsBankFilters?.creatorIds}
						// placeholder={tr`All`}//{tr`select_author`}
						placeholder={tr`select_author`}
						onChange={(values) =>
							QuestionsBankData.handleSetFilters(
								values.map((item) => item.value),
								"creatorIds"
							)
						}
						isMulti
						customOption={authorOptions}
						isLoading={QuestionsBankData.learnerContextCreatorLoading}
						disabled={true}
					/>
				</div>
				<div style={{ width: "300px" }}>
					<RTags
						url={`${Services.tag_search.backend}api/v1/tag/search/tags?prefix=`}
						getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data.models : null)}
						getValueFromArrayItem={(i) => i.value}
						getLabelFromArrayItem={(i) => i.label}
						getIdFromArrayItem={(i) => i.id}
						onChange={(data) => {
							QuestionsBankData.handleSetFilters(data, "tags");
						}}
						defaultValues={QuestionsBankData.questionsBankFilters?.tags}
						selectHeight={"43px"}
					/>
				</div>
				<RFlex styleProps={{ alignItems: "center" }}>
					<RButton
						text={tr`apply_filters`}
						color="primary"
						loading={QuestionsBankData.questionsBankLoading}
						disabled={QuestionsBankData.questionsBankLoading}
						onClick={QuestionsBankData.handleFilter}
						style={{ margin: "0px" }}
					/>
					<RButton
						text={tr`reset_all`}
						color="link"
						className={styles.reset__all}
						disabled={QuestionsBankData.questionsBankLoading}
						onClick={QuestionsBankData.handleResetAllFilters}
						style={{ margin: "0px" }}
					/>
				</RFlex>
			</RFlex>
		</>
	);
};

export default RQuestionsBankFilters;
