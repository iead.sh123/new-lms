import { faPen, faX } from "@fortawesome/free-solid-svg-icons";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Table, Col, Container, Row } from "reactstrap";
import EventCard from "view/TermsManager/cards/EventCard/EventCard";
import WeeklyScheduleEditor from "./components/WeeklyScheduleEditor";
import DroppableCell from "./components/DroppableCell";
import { DragDropContext } from "react-beautiful-dnd";
import CreateOtherModal from "./components/CreateOtherModal";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";

const WeeklySchedule = ({
	periods,
	events,
	saveDisabled,
	handleLoadWeeklyScheduleElements,
	handleDeleteWeeklyScheduleElement,
	loadAvailableWeeklyScheduleElements,
	availableWeeklyScheduleElements,
	addOtherWeeklyScheduleElement,
	updateOtherWeeklyScheduleElement,
	deleteOtherWeeklyScheduleElement,
	handleAddWeeklyScheduleElement,
	handleUpdateWeeklyScheduleElement,
	handleSaveWeeklySchedule,
	loading,
	// disableActions,
	termId,
	gradeLevelId,
	show,
	weeklyScheduleHaveEvents,
}) => {
	console.log("events 122333333", events);
	const [createModalIsOpen, setCreateModalOpen] = React.useState(false);

	const toggleCreateModal = () => {
		setElementContext({});
		setCreateModalOpen(!createModalIsOpen);
	};

	const [edited, setEdited] = React.useState(null);

	const [elementContext, setElementContext] = React.useState({});

	React.useEffect(() => {
		handleLoadWeeklyScheduleElements();
		loadAvailableWeeklyScheduleElements();
	}, [termId, gradeLevelId]);

	const updateElement = (id, payload) => {
		handleUpdateWeeklyScheduleElement(id, payload);
	};

	const eventActions = [
		{
			icon: faPen,
			id: "edit",
			onClick: (id) => {
				setEdited({ ...events.byId[id], name: events.byId[id].title });
				toggleCreateModal();
			},
		},
		{
			icon: faX,
			id: "delete",
			onClick: (elId) => handleDeleteWeeklyScheduleElement(events.byId[elId]),
		},
	];

	const onDragEnd = (result) => {
		const cell = result?.destination?.droppableId;
		if (!cell) return;

		const [, dayOfWeek, periodId] = cell.split("_");

		//edit
		if (result?.draggableId?.split?.("_")?.[0] === "event") {
			const intendedElement = events?.byId?.[+result?.draggableId?.split?.("_")?.[1]];

			if (+intendedElement?.dayOfWeek === +dayOfWeek && +intendedElement?.period?.id === +periodId) return;
			return updateElement(+result?.draggableId?.split?.("_")?.[1], {
				dayOfWeek: +dayOfWeek,
				periodId: +periodId,
			});
		}

		// toggleCreateModal();

		// handleAddWeeklyScheduleElement({ title: payload.name, ...elementContext })

		handleAddWeeklyScheduleElement({
			title: result?.draggableId?.split?.("_")?.[2],
			homeRoomId: result?.draggableId?.split?.("_")?.[0] === "homeroom" ? +result?.draggableId?.split?.("_")?.[1] : undefined,
			courseId: result?.draggableId?.split?.("_")?.[0] === "course" ? +result?.draggableId?.split?.("_")?.[1] : undefined,
			typeId: result?.draggableId?.split?.("_")?.[0] === "other" ? +result?.draggableId?.split?.("_")?.[1] : undefined,
			dayOfWeek: +dayOfWeek,
			periodId: +periodId,
		});

		// setElementContext({
		//     homeRoomId: result?.draggableId?.split?.('_')?.[0] === 'homeroom' ? +result?.draggableId?.split?.('_')?.[1] : undefined,
		//     courseId: result?.draggableId?.split?.('_')?.[0] === 'course' ? +result?.draggableId?.split?.('_')?.[1] : undefined,
		//     typeId: result?.draggableId?.split?.('_')?.[0] === 'other' ? +result?.draggableId?.split?.('_')?.[1] : undefined,
		//     dayOfWeek: +dayOfWeek,
		//     periodId: +periodId
		// });
	};

	return (
		<React.Fragment>
			{loading ? (
				<Loader />
			) : (
				<>
					{" "}
					<CreateOtherModal
						edited={edited}
						isOpen={createModalIsOpen}
						toggle={toggleCreateModal}
						handleSave={(payload) => {
							if (edited) {
								updateElement(edited.id, { title: payload.name });
								return setEdited(null);
							}

							handleAddWeeklyScheduleElement({
								title: payload.name,
								...elementContext,
							});
						}}
					/>
					<DragDropContext onDragEnd={onDragEnd}>
						<Row>
							{((show && weeklyScheduleHaveEvents) || !show) && (
								<Col md={show ? "12" : "10"}>
									<Table style={{ color: "#FFF" }} bordered responsive>
										<thead style={{ backgroundColor: "#668AD7", fontWeight: "bold" }}>
											<tr style={{ textAlign: "center" }}>
												<td></td>
												<td>{tr`Sunday`}</td>
												<td>{tr`Monday`}</td>
												<td>{tr`Tuseday`}</td>
												<td>{tr`Wednesday`}</td>
												<td>{tr`Thursday`}</td>
												<td>{tr`Friday`}</td>
												<td>{tr`Saturday`}</td>
											</tr>
										</thead>
										<tbody>
											{periods?.ids.map((pId) => (
												<tr key={"period-" + pId}>
													<td className="text-center" style={{ backgroundColor: "#668AD7", width: "100px" }}>
														{periods.byId[pId].start_time + " - " + periods.byId[pId].end_time}
													</td>

													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_0_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 0
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_1_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 1
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_2_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 2
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_3_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 3
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_4_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 4
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_5_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 5
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
													<td className="text-center" style={{ width: 228, height: 50, padding: 0 }}>
														<DroppableCell
															id={"DOF_6_" + pId}
															events={{
																ids: events.ids.filter(
																	(evId) => events.byId[evId]?.period?.id === pId && +events.byId[evId].dayOfWeek === 6
																),
																byId: events.byId,
															}}
															actions={eventActions}
															disableActions={show}
														/>
													</td>
												</tr>
											))}
										</tbody>
									</Table>
								</Col>
							)}
							{!show && (
								<Col md="2">
									<WeeklyScheduleEditor
										availableWeeklyScheduleElements={availableWeeklyScheduleElements}
										addOtherWeeklyScheduleElement={addOtherWeeklyScheduleElement}
										updateOtherWeeklyScheduleElement={updateOtherWeeklyScheduleElement}
										deleteOtherWeeklyScheduleElement={deleteOtherWeeklyScheduleElement}
									/>
								</Col>
							)}
						</Row>
					</DragDropContext>
					{!show && (
						<RButton
							text={loading ? tr`saving...` : tr`save`}
							onClick={handleSaveWeeklySchedule}
							color="primary"
							style={{ width: "112px" }}
							disabled={saveDisabled}
						/>
					)}
				</>
			)}
		</React.Fragment>
	);
};

export default WeeklySchedule;
