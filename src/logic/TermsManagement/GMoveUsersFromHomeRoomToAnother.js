import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { useFormik } from "formik";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import { boldGreyColor } from "config/constants";

const GMoveUsersFromHomeRoomToAnother = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	const initialValues = {
		sourceHomeRoomId: TermsManagementData.itemObj?.dataObj?.id,
		targetHomeRoomId: "",
	};

	const handleFormSubmit = (values) => {
		TermsManagementData.handleMoveUsersFromHomeRoomToAnotherOne(values);
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm, isValid } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
	});
	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<RFlex styleProps={{ width: "100%" }}>
					<h6>{tr`change_home_room`}</h6>
				</RFlex>
				<RFlex styleProps={{ flexDirection: "column" }}>
					{TermsManagementData.itemObj.allHomeRoomsToGradeLevel
						.filter((item) => item.id !== TermsManagementData.itemObj?.dataObj?.id)
						.map((homeRoom, index) => (
							<RFlex key={index} styleProps={{ justifyContent: "space-between" }}>
								<AppRadioButton name="radio" label={homeRoom.name} onClick={() => setFieldValue("targetHomeRoomId", homeRoom.id)} />
								<RFlex styleProps={{ justifyContent: "space-between" }}>
									<i className={iconsFa6.userGroup} style={{ color: boldGreyColor }} />
									<span>
										{homeRoom.statistics.students}
										{tr`students`}
									</span>
									<span>
										{homeRoom.statistics.teacher}
										{tr`teachers`}
									</span>
								</RFlex>
							</RFlex>
						))}
				</RFlex>
				<RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
					<RButton
						type="submit"
						text={tr`save`}
						color="primary"
						disabled={TermsManagementData.moveUsersFromHomeRoomToAnotherOneLoading}
						loading={TermsManagementData.moveUsersFromHomeRoomToAnotherOneLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						onClick={() => {
							TermsManagementData.handleCloseMoveUsersFromHomeRoomToAnother();
							resetForm();
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default GMoveUsersFromHomeRoomToAnother;
