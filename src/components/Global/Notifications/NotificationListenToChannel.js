import React, { useEffect } from "react";
import NotificationList from "./NotificationList";
import { useDispatch, useSelector } from "react-redux";
import { receiveNewNotifications } from "store/actions/global/notification.action";
import { subscribeToPusher } from "utils/Pusher";
import { subscribeToReceiver } from "views/auth/FireBaseServices";

const NotificationListenToChannel = ({
  notificationList,
  notificationsLoading,
  total
}) => {
  const dispatch = useDispatch();

  const { user } = useSelector((state) => state?.auth);

  const events = {
    [`${process.env.REACT_APP_SLOT_NAME}_${user?.id}`]: {
      new_notification: (data) =>
        dispatch(receiveNewNotifications(data)),
    },
  };

  useEffect(() => {
    if (user?.id) {
      //subscribeToPusher(events);
      subscribeToReceiver(events);
    }
  }, [user?.id]);
  return (
    <NotificationList
      notificationList={notificationList}
      notificationsLoading={notificationsLoading}
      total={total}
    />
  );
};

export default NotificationListenToChannel;
