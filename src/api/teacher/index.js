import { get, post, put, destroy } from "config/api";

import { Services } from "engine/services";

export const teacherApi = {
  getCourseContent: (rabId) =>
    get(`${Services.lesson.backend}api/lessons/${rabId}`),

  //---------------service Section Apis---------------
  getCourses: () => get(`${Services.courses_manager.backend}api/sections`),
  addCourse: (data) =>
    post(`${Services.courses_manager.backend}api/sections`, data),
  addCourse: (data) =>
    post(`${Services.courses_manager.backend}api/sections`, data),
  getSectionStudents: (sectionId) =>
    get(
      `${Services.courses_manager.backend}api/sections/${sectionId}/learners`
    ),
};
