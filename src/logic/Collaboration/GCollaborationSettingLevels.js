import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import React, { useState } from "react";
import tr from "components/Global/RComs/RTranslator";
import RFormModal from "components/Global/RComs/RFormModal";
import { post } from "config/api";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { destroy } from "config/api";
import { toast } from "react-toastify";

export const GCollaborationSettingLevels = ({ setLevelsCount }) => {
	const [data1, setData1] = useState([]);
	const [records, setRecords] = useState([]);
	useEffect(() => {
		setRecords(
			data1?.map((r, index) => {
				Helper.cl(r, "col levels r");
				let record = {};
				record.details = [
					{ key: tr`name`, value: r?.name },
					{ key: tr`description`, value: r?.description },
					{
						key: tr`creator`,
						type: "component",
						value: (
							<div>
								<img src={Services.storage.file + r.creator.image} style={{ width: "40px", borderRadius: "50px" }} />
								{"  " + r.creator.name}{" "}
							</div>
						),
					},
				];
				record.actions = [
					{
						name: tr`edit`,
						icon: "fa fa-pencil",
						color: "info",
						onClick: () => {
							Helper.cl(r.id, "rid");
							edit(r.id);
						},
					},
					{
						name: tr`delete`,
						icon: "fa fa-remove",
						color: "info",
						onClick: () => {
							remove(r.id);
						},
					},
				];
				return record;
			})
		);
		Helper.cl("RECORDS SET");
	}, [data1]);

	const setData = (response) => {
		if (response?.data?.data?.data?.data) setData1(response?.data?.data?.data?.data);
		setLevelsCount(response?.data?.data?.data?.total);
	};
	//----------------------------------

	const getDataFromBackend = async (specific_url) => {
		const url = Services.collaboration.backend + `api/organization/collaboration-levels`;
		Helper.cl(url, "co url");
		let response1 = await get(specific_url ? specific_url : url);
		Helper.cl(response1, "co levels response1");
		if (response1) {
			return response1;
		}
	};
	const add = () => {
		setOpenAdd(true);
	};

	const addSubmit = async (values) => {
		const name = values[0].value;
		const desc = values[1].value;
		//------------------------------------
		const url = Services.collaboration.backend + `api/collaboration-level/0`;
		Helper.cl(url, "co url f1");
		const payload = { name: name, description: desc, order: records.length + 1 };
		try {
			let response1 = await post(url, payload);
			Helper.cl(url, "co url f2");
			if (response1 && response1.data && response1.data.status) {
				Helper.cl(response1.data.data[0], "co url response1.data.data[0]");
				setData1([
					...data1,
					{
						name: name,
						description: desc,
						creator: {
							id: response1.data.data[0].id,
							name: response1.data.data[0].creator.name,
							image: response1.data.data[0].creator.image,
						},
					},
				]);
				setOpenAdd(false);
			} else {
				toast.error(response1.data.msg);
			}
		} catch (error) {
			toast.error(error.message);

			Helper.cl(error, "here here her error");
		}
		//-------------------------------
	};

	const edit = (id) => {
		Helper.cl(id, "rec f1 edit id");
		const rec = data1.filter((d) => d.id == id)[0];
		//Helper.cl(rec,"rec f1");

		if (rec && rec.id) {
			setEditFormItems([
				{
					type: "text",
					label: tr`Level Name`,
					name: "groupName",
					defaultValue: rec.name,
				},
				{
					type: "text",
					label: tr`Description`,
					name: "Description",
					defaultValue: rec.description,
				},

				{
					type: "hidden",
					name: "id",
					defaultValue: rec.id,
				},
			]);
			setOpenEdit(true);
		} else {
		}
	};

	const editSubmit = async (values) => {
		Helper.cl(values, "editSubmit 1 edit values");
		const name = values[0].value;
		const desc = values[1].value;
		const id = parseInt(values[2].value);

		//------------------------------------
		const url = Services.collaboration.backend + `api/collaboration-level/${id}`;

		const payload = { id: id, name: name, description: desc, order: records.length + 1 };
		//Helper.cl(payload,"editSubmit 2  payload")
		let response1 = await post(url, payload);

		if (response1 && response1.data && response1.data.status == 1) {
			//setData1( [...data1,{name:name,description:desc,creator:user}]);
			Helper.cl("editSubmit 3  payload");
			const updatedArray = data1.map((element, index) => {
				if (element.id === id) {
					{
						// id:response1.data.data[0].id,
						// name:response1.data.data[0].creator.name,
						// image:response1.data.data[0].creator.image

						return {
							...element,
							name: name,
							description: desc,
							creator: {
								id: response1.data.data[0].id,
								name: response1.data.data[0].creator.name,
								image: response1.data.data[0].creator.image,
							},
						};
					} // update the element at index 2
				}
				return element; // leave the other elements unchanged
			});

			Helper.cl(updatedArray, "editSubmit 4");
			setData1(updatedArray);

			setOpenEdit(false);
		} else {
			toast.error(tr`Something went wrong during edit`);
		}
		//-------------------------------
	};

	const remove = async (id) => {
		//------------------------------------
		const url = Services.collaboration.backend + `api/collaboration-level/` + id;
		let response1 = await destroy(url);
		if (response1 && response1.data && response1.data.status == 1) {
			const updatedArray = data1.filter((e) => e.id != id); //
			setData1(updatedArray);
		} else {
			toast.error(tr`Something went wrong during delete`);
		}
		//-------------------------------
	};

	const [editFormItems, setEditFormItems] = useState([]);

	const initialformItems = [
		{
			type: "text",
			label: tr`Level Name`,
			name: "groupName",
		},
		{
			type: "text",
			label: tr`Description`,
			name: "Description",
		},
	];

	const [openAdd, setOpenAdd] = useState(false);
	const [opeEdit, setOpenEdit] = useState(false);
	return (
		<div>
			{/* {Helper.js(data1,"data1")}  */}

			<RFormModal
				title="add Collaboration Level"
				open={openAdd}
				setOpen={setOpenAdd}
				actionButtonText={"create"}
				initialformItems={initialformItems}
				onFormSubmit={(e) => {
					addSubmit(e.target);
				}}
			/>

			<RFormModal
				title="edit Collaboration Level"
				open={opeEdit}
				setOpen={setOpenEdit}
				actionButtonText={"save"}
				initialformItems={editFormItems}
				onFormSubmit={(e) => {
					editSubmit(e.target);
				}}
			/>

			<RAdvancedLister
				hideTableHeader={false}
				colorEveryOtherRow={false}
				//firstCellImageProperty={"image"}
				actionButtons={[{ text: tr`Add new Level`, icon: "fa fa-add", color: "info", onClick: () => add() }]}
				getDataFromBackend={getDataFromBackend}
				setData={setData}
				dir1={"flex-end"}
				records={records}
				getDataObject={(response) => response.data?.data?.data}
				getTotal={(dataObject) => {
					Helper.cl(dataObject, "total response.data?.data?");
					const t = dataObject.total;
					Helper.cl(t, "total");
					return t;
				}}
				characterCount={15}
				marginT={"mt-3"}
				marginB={"mb-2"}
				align={"left"}
				// SpecialCard={RCollaborationCard}
				showListerMode="tableLister"
			/>
		</div>
	);
};
