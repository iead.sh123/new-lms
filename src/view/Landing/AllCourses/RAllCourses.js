import React, { useState, useEffect } from "react";
import { Services } from "engine/services";
import DefaultImage from "assets/img/new/course-default-cover.png";
import styles from "./AllCourses.module.scss";
import RCard from "components/Global/RComs/RCards/RCard";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const RAllCourses = ({ allCourses }) => {
	const [courses, setCourses] = useState();

	useEffect(() => {
		if (allCourses && allCourses.courses && allCourses.courses.length > 0) {
			buildMyCourses();
		}
	}, [allCourses && allCourses.courses]);

	const buildMyCourses = () => {
		let courses = [];

		allCourses.courses?.map((course, index) => {
			let data = {};
			data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
			data.icon = course.icon_id == undefined || [] ? undefined : `${Services.courses_manager.file + course.icon_id}`;

			data.id = course.id;
			data.name = course.name;
			data.color = course.color;
			data.rate = course.rating;
			data.categoryName = course.category_name;
			data.isFree = course.isFree;
			courses.push(data);
		});

		setCourses(courses);
	};
	return (
		<React.Fragment>
			<h5>{tr`get_started_from_here`}</h5>
			<section className={styles.all_courses}>
				{courses && courses?.length > 0 ? (
					courses.map((course) => <RCard course={course} hiddenFlag={true} />)
				) : (
					<div style={{ justifyContent: "center" }}>
						<REmptyData />
					</div>
				)}
			</section>
		</React.Fragment>
	);
};

export default RAllCourses;
