import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import styles from "./styles/RCardLister.Module.scss";
import RCard from '../RCard/RCard';

const RCardLister = ({ cards }) => <Container className={`content ${styles["CardLister"]}`} fluid>
    <Row>
        <Col>
            {
                cards?.slice(0, cards.length / 2).map(c => <RCard {...c} />)
            }
        </Col>

        <Col>
            {
                cards?.slice(cards.length / 2).map(c => <RCard {...c} />)
            }
        </Col>
    </Row>

</Container>

export default RCardLister;