import tr from "./RTranslator";
import { Button } from "reactstrap";
import { useHistory } from "react-router-dom";

const RNoDataLine = ({
  messageNoData ="No data",

}) => {

  return (
    <>
      <div
       style={{color: "#818181",

        fontFamily: "Cairo",
        fontSize: "16px",
        fontStyle: "normal",
        fontWeight: "300",
        lineHeight: "normal",
        textTransform: "capitalize"}}
      >

        {tr(messageNoData)}
      </div>
    </>
  );
};

export default RNoDataLine;
