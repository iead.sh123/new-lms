import React, { useEffect } from "react";
import { Input, FormGroup } from "reactstrap";
import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import { useFormik } from "formik";
import iconsFa6 from "variables/iconsFa6";
import RLoader from "components/Global/RComs/RLoader";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

const CartCoupon = ({ prices, cartItems, handleAddCouponToItems, applyCouponLoading, handleDeleteCoupon, deleteCouponMutation }) => {
	const initialValues = {
		code: (prices && prices?.couponValue) || "",
	};

	const formSchema = yup.object().shape({
		code: yup.string().required(tr`code is required`),
	});

	const handleFormSubmit = (values) => {
		const Value = { code: values.code, cartItems: cartItems };
		handleAddCouponToItems(Value);
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};

	useEffect(() => {
		if (deleteCouponMutation.isSuccess) {
			resetForm();
		}
	}, [deleteCouponMutation.isSuccess]);

	return (
		<>
			{!prices?.coupon ? (
				<form onSubmit={handleSubmit}>
					<RFlex styleProps={{ alignItems: "center", marginLeft: "6px" }}>
						<FormGroup style={{ display: "flex", flexDirection: "column", flex: 1 }}>
							<Input
								name="code"
								type="text"
								// value={prices?.coupon?.code}
								placeholder={tr("coupon_code")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "code")}
							/>
							{/* {touched.code && errors.code && <FormText color="danger">{errors.code}</FormText>} */}
						</FormGroup>
						<RButton
							type="submit"
							color="primary"
							text={tr`apply`}
							loading={applyCouponLoading}
							disabled={applyCouponLoading || cartItems?.length == 0}
						/>
					</RFlex>
				</form>
			) : (
				<RFlex styleProps={{ justifyContent: "space-between" }}>
					<RFlex styleProps={{ alignItems: "center" }}>
						{deleteCouponMutation?.isLoading ? (
							<div style={{ width: "20px", height: "20px" }}>
								<RLoader mini={25} />
							</div>
						) : (
							<i className={iconsFa6.delete} style={{ color: dangerColor, cursor: "pointer" }} onClick={() => handleDeleteCoupon()} />
						)}
						<span>{prices?.coupon?.code}</span>
					</RFlex>
					<RFlex styleProps={{ color: successColor, alignItems: "center" }}>
						<i className={iconsFa6.check} />
						<span>{tr`valid`}</span>
					</RFlex>
				</RFlex>
			)}
		</>
	);
};

export default CartCoupon;
