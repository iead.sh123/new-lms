import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import styles from "./QuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RQuestionTypes = ({ questionSetIndex, type, groupId, questionBank }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const QuestionsBankData = useContext(QuestionsBankContext);

	const questionTypesEnum = [
		{
			name: "MULTIPLE_CHOICES_MULTIPLE_ANSWERS",
			title: "multiple_choice_(MCQ)",
			icon: "fas fa-tasks pt-1",
		},
		{
			name: "MULTIPLE_CHOICES_SINGLE_ANSWER",
			title: "single_answer_(SCQ)",
			icon: "fas fa-tasks pt-1",
		},
		{
			name: "ESSAY",
			title: "essay",
			icon: "fas fa-align-left pt-1",
		},
		{
			name: "MATCH",
			title: "match",
			icon: "fas fa-grip-lines-vertical pt-1",
		},
		{
			name: "DRAW",
			title: "draw",
			icon: "fa-solid fa-pen pt-1",
		},

		{
			name: "UPLOAD_FILE",
			title: "upload_file",
			icon: "fa-regular fa-file pt-1",
		},
		// {
		//   name: "FILL_IN",
		//   title: "fill_in_blank",
		//   icon: "fa-solid fa-pen-to-square pt-1",
		// },
		{
			name: "TRUEFALSE",
			title: "true / false",
			icon: "fa-solid fa-clipboard-check pt-1",
		},
		{
			name: "SEQUENCING",
			title: "ordering",
			icon: "fa-solid fa-arrow-down-1-9 pt-1",
		},
	];

	return (
		<RFlex
			styleProps={{
				alignItems: "center",
				gap: 25,
				flexWrap: "wrap",
				margin: groupId ? "10px 20px" : "10px 0px",
			}}
		>
			<UncontrolledDropdown direction="right">
				<DropdownToggle
					aria-haspopup={true}
					caret
					color="default"
					data-toggle="dropdown"
					id="navbarDropdownMenuLink"
					nav
					style={{
						display: "flex",
						alignItems: "center",
						gap: 10,
						padding: "0.5rem 0rem",
					}}
				>
					<span>{tr`create_new_question`}</span>
				</DropdownToggle>
				<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" left className="ml-4 mt-0">
					{questionTypesEnum.map((qTE) => (
						<DropdownItem
							key={qTE.name}
							className={styles.question__set__types}
							onClick={() => {
								type == "group"
									? questionBank
										? QuestionsBankData.handleCreateNewQuestionsToQuestionGroup(qTE, questionSetIndex, groupId)
										: QuestionSetData.handleCreateNewQuestionsToQuestionGroup(qTE, questionSetIndex, groupId)
									: QuestionSetData.handleCreateNewQuestion(qTE, questionSetIndex);
							}}
						>
							<RFlex>
								<i className={qTE.icon} />
								<span>{tr(qTE.title)}</span>
							</RFlex>
						</DropdownItem>
					))}
				</DropdownMenu>
			</UncontrolledDropdown>

			{type !== "group" && !questionBank && (
				<span
					className={styles.create__question__set__text}
					onClick={() => {
						QuestionSetData.handleCreateNewGroup(questionSetIndex);
					}}
				>{tr`create_question_group`}</span>
			)}

			{!questionBank && (
				<span
					className={styles.create__question__set__text}
					onClick={() => {
						QuestionSetData.handlePushToQuestionsBank({
							isGroup: type == "group",
							groupId: groupId,
							questionSetIndex: questionSetIndex,
						});
					}}
				>{tr`choose_from_question_bank`}</span>
			)}
		</RFlex>
	);
};

export default RQuestionTypes;
