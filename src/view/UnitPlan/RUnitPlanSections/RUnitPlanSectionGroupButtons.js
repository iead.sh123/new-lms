import React, { useContext, useState } from "react";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import RButton from "components/Global/RComs/RButton";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import produce from "immer";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import iconsFa6 from "variables/iconsFa6";

const RUnitPlanSectionGroupButtons = ({ section }) => {
	const UnitPlanContextData = useContext(UnitPlanContext);

	const [attachments, setAttachments] = useState([]);
	const setSpecificAttachment = (att) => {
		const new_attachments = produce(attachments, (attachments_temp) => {
			const ind = attachments_temp.findIndex((a) => a.tempid == att.tempid);
			if (ind == -1) {
				attachments_temp.push(att);
			} else attachments_temp[ind] = att;
		});
		setAttachments(new_attachments);
	};
	return (
		<RFlex styleProps={{ alignItems: "center" }} className={styles.buttons_group}>
			<RFileSuite
				parentCallback={(event) => {
					UnitPlanContextData.handelSectionChange("attachments", event, section?.id ? section?.id : section?.fakeId);
				}}
				singleFile={true}
				disabled={section.attachments && section.attachments !== null && section.attachments.length > 0 ? true : false}
				theme="button"
				placeholder={`Replace the old selected file`}
				binary={true}
			/>

			<RButton
				className={"pick_buttons"}
				onClick={() => {
					UnitPlanContextData.setUnitPlanSectionId(section?.id ? section?.id : section?.fakeId);
					UnitPlanContextData.setTypeToPick("section");
					UnitPlanContextData.handleOpenPickALessonPlan();
				}}
				disabled={section.lesson_plan_id ? true : false}
				faicon="fa fa-cloud-upload"
			/>
			<RButton
				className={"pick_buttons"}
				onClick={() => {
					UnitPlanContextData.setUnitPlanSectionId(section?.id ? section?.id : section?.fakeId);
					UnitPlanContextData.setTypeToPick("section");
					UnitPlanContextData.handleOpenPickARubric();
				}}
				disabled={section.rubric_id ? true : false}
				faicon="fa fa-cloud-upload"
			/>
			<RButton
				className={styles.pick_buttons_danger}
				onClick={() => UnitPlanContextData.handleRemoveSection(section?.id ? section?.id : section?.fakeId)}
				faicon={iconsFa6.delete}
				outline
				color="danger"
			/>
		</RFlex>
	);
};

export default RUnitPlanSectionGroupButtons;
