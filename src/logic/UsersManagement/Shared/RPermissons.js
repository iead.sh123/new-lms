import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React, { useState } from "react";
import { permissionsHeader } from "../constants";
import styles from "./sharedStyle.module.scss";
import RLister from "components/Global/RComs/RLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RButton from "components/Global/RComs/RButton";
const RPermissons = ({
	permissions,
	selectAllFn,
	selectOneFn,
	frontSearchTerm,
	setFrontSearchTerm,
	setBackSearchTerm,
	selectedPermissions,
	isFetching,
	canEdit = true,
	view = false,
	inUser = false,
	currentUserTypes,
	pickedUserType,
	handleChangePickedUserType,
}) => {
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
	};

	const records = permissions?.map((permission, index) => {
		const permissionKeyComponent = (
			<AppNewCheckbox
				label={tr("permissions")}
				checked={selectedPermissions.length == permissions.length}
				onChange={() => {
					canEdit ? selectAllFn(permissions) : "";
				}}
				idsOrderGenerator={"allPermissions"}
			/>
		);
		return {
			details: permissionsHeader.map((header) => {
				const permissionValueComponent = (
					<AppNewCheckbox
						label={permission[header.value]}
						checked={selectedPermissions.some((p1) => p1.id == permission.id)}
						onChange={() => {
							canEdit ? selectOneFn(permission) : "";
						}}
						idsOrderGenerator={permission.id}
					/>
				);
				const privilegeValueComponent = permission.privileged == 1 ? <div className={styles.priviligeStyle}>{tr("Privilege")}</div> : "";
				return {
					key: header.name == "permissions" ? permissionKeyComponent : tr(header.key),
					value:
						header.name == "permissions"
							? permissionValueComponent
							: header.name == "privilege"
							? privilegeValueComponent
							: permission[header.value],
					keyType: header.name == "permissions" ? "component" : "",
					type: header.name == "permissions" ? "component" : header.name == "privilege" ? "component" : "",
				};
			}),
		};
	});
	return (
		<RFlex className="flex-column" styleProps={{ marginBottom: view ? "0px" : "75px" }}>
			<RSearchHeader
				searchLoading={isFetching}
				inputPlaceholder={tr`search_for_a_permission`}
				searchData={frontSearchTerm}
				handleChangeSearch={setFrontSearchTerm}
				setSearchData={setFrontSearchTerm}
				handleSearch={handleSearch}
			/>
			{inUser && (
				<RFlex className="gap-4 ">
					{currentUserTypes?.map((userType, index) => (
						<RButton
							text={userType.title}
							color="primary"
							outline={userType.id == pickedUserType.id ? false : true}
							className="m-0 rounded-[50px]"
							onClick={() => handleChangePickedUserType(userType)}
						/>
					))}
				</RFlex>
			)}
			<RLister Records={records} activeScroll={true} />
		</RFlex>
	);
};

export default RPermissons;
