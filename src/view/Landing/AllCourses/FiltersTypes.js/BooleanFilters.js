import React from "react";
import styles from "../AllCourses.module.scss";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import tr from "components/Global/RComs/RTranslator";

const BooleanFilters = ({ data, handleFilter, selectedBooleanItems }) => {
  return (
    <div>
      {data &&
        data.length > 0 &&
        data?.map((el) => (
          <div key={el.title} className={styles.container}>
            <span className={styles.title + " text-muted"}>{el.title}</span>
            {el?.items.map((item) => (
              <div key={item} className={styles.container___checkbox}>
                <AppCheckbox
                  onChange={(e) => handleFilter("boolean", item)}
                  label={tr(`${item}`)}
                  checked={selectedBooleanItems.includes(item) ? true : false}
                />
              </div>
            ))}
          </div>
        ))}
    </div>
  );
};

export default BooleanFilters;
