import React from "react";

const FolderIcon = ({ tColor, gOColor, gTColor, width = 12, height = 10 }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 12 10" fill="none">
			<path
				d="M0 1.93307H10.75C11.4404 1.93307 12 2.40653 12 2.99057V8.27809C12 8.86213 11.4404 9.3356 10.75 9.3356H6H1.25C0.559644 9.3356 0 8.86213 0 8.27809V1.93307Z"
				fill={tColor}
			/>
			<path
				d="M0 3.51932V7.85509C0 8.43913 0.559644 8.91259 1.25 8.91259H10.75C11.4404 8.91259 12 8.43913 12 7.85509V3.41357C12 2.82953 11.4404 2.35607 10.75 2.35607H5.5V2.88482C5.5 3.23525 5.16421 3.51932 4.75 3.51932H0Z"
				fill="url(#paint0_linear_3980_225569)"
			/>
			<path
				d="M0 1.51007C0 1.04283 0.447715 0.664062 1 0.664062H4.5C5.05228 0.664062 5.5 1.04283 5.5 1.51007V1.93307H0V1.51007Z"
				fill={tColor}
			/>
			<defs>
				<linearGradient id="paint0_linear_3980_225569" x1="0" y1="0.664063" x2="14.3652" y2="10.838" gradientUnits="userSpaceOnUse">
					<stop stop-color={gOColor} />
					<stop offset="1" stop-color={gTColor} />
				</linearGradient>
			</defs>
		</svg>
	);
};

export default FolderIcon;
