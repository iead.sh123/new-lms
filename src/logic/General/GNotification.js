import { faPencilRuler } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { Services } from "engine/services";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { viewNotificationsAsync } from "store/actions/global/notification.action";
import { setNotifyMe } from "store/actions/global/notification.action";
import RNotificationGroup from "view/Notifications/RnotificationGroup";

const GNotification = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const params = useParams();
  const notifyMe = useSelector((state) => state?.NotificationsRed?.notifyMe);
  const setNotify = (n) => {
    dispatch(setNotifyMe(n));
  };
  const [nots, setNots] = useState([]);
  useEffect(() => {
    const getAll = async () => {
      const allNOts = await get(
        `${Services.notficiation.backend}api/notification/${params.group_id}`
      );
      if (allNOts.data?.data) {
        
        const nots1 = allNOts.data.data.map((s) => {
          Helper.cl(s,"google");
          return {
            image:
               s.notifications?.[0]?.user?.image,
            imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
            iconbg: "#007bff",
            title: s?.description,
            description: s?.description,
            time: "10 min ago",
            seen: s.seen,
            notifications: s.notifications,
          };
        });
        setNots(nots1);
      }
    };
    getAll();
  }, []);

  const handleRemoveNotification = (notificationId) => {
    dispatch(viewNotificationsAsync(notificationId));
  };
  const moveToDetails = (notificationId) => {
    const url = "/notification/" + notificationId;
    //history.push(url);
  };

  return (
    <div style={{width:"80%"}}>
    <RNotificationGroup
      onMarkAsSeen={() => {}}
      onStopNotificationChange={(e) => {
        setNotify(e ? 0 : 1);
      }}
      allNotifications={nots?.count}
      notifyMe={notifyMe}
      withDetails={true}
      group={{
        icon: "star",
        title: "Notifications",
        description: "Notifications description",
        notifications: nots,
      }}
      handleOnClick={moveToDetails}
      handleOnSeenClick={handleRemoveNotification}
    />
    </div>
  );
};
export default GNotification;
