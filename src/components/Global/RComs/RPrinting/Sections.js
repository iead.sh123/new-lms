import React from "react";

const Sections = ({ item, index, level }) => {
  return (
    <>
      <h6
        style={{
          position: "relative",
          left: `${level * 10}px`,
        }}
      >
        {index} . {item.text}
      </h6>
      {item?.items?.map((item, index) => {
        return <Sections item={item} index={index + 1} level={level + 1} />;
      })}
    </>
  );
};

export default Sections;
