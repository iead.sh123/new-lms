import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col } from "reactstrap";

import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import RTimeLine from "./RTimeLine";
import GGreetingSearch from "./GGreetingSearch";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import { fileImage } from "components/Global/RComs/RResourceViewer/RFile";
import { relativeDate } from "utils/dateUtil";
import RHeader from "components/Global/RComs/RHeader";
import GStudentSwitch from "logic/General/GStudentSwitch";
import GRecentlyAddedCourses from "./GRecentlyAddedCourses";
import GTodayLiveSessions from "./GTodayLiveSessions";

import GCoursesLine from "./GCoursesLine";
import GFacilitatorWeekEvents from "./GFacilitatorWeekEvents";
import GStudentsSubmitedTasks from "./GStudentsSubmitedTasks";
import { Services } from "engine/services";
import GLiveSessions from "./rc/GLiveSessions";
import GUnpublished from "./rc/GUnpublished";
import GWeekEvents from "./rc/GWeekEvents";
import RRLiveSessions from "./rc/RRLiveSessions";
import RBieChart from "components/Global/RComs/RBieChart";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import dashboardReducer from "store/reducers/teacher/dashboard.reducer";
import RDiscover from "./rc/RDiscover";
import RNoDataLine from "components/Global/RComs/RNoDataLine";
import RHidable from "./RHidable";
import RPieChart from "components/Global/RComs/RPieChart";
import moment from "moment";
import { getRelativeDate } from "utils/dateUtil";
import { publishAndUnPublishModuleContentAsync } from "store/actions/global/coursesManager.action";
import REmptyData from "components/RComponents/REmptyData";

const GCourseAdminDashboard = ({ userType = "ca" /* ca , fa , cd , ad */ }) => {
	const history = useHistory();
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	const dispatch1 = useDispatch();
	useEffect(() => {
		getRecentAddedRolesAsync(dispatch);
		getRecentUserTypesAsync(dispatch);
		getRecentActivitiesAsync(dispatch);
	}, []);

	const [data, setData] = useState([]);
	const [recentLessons, setRecentLessons] = useState([]);
	const [recentTasks, setRecentTasks] = useState([]);
	const [recentlyAddedContents, setRecentlyAddedContents] = useState([]);
	const [statistics, setStatistics] = useState([]);
	const [todayActivities, setTodayActivities] = useState([]);

	const [courses1, setCourses1] = useState([]);
	const [courses2, setCourses2] = useState([]);
	const [courses3, setCourses3] = useState([]);

	const [publishedContentIDs, setPublishedContentIds] = useState([]);

	const { myCoursesLoading, myCourses } = useSelector((state) => state.coursesManagerRed);

	const courses = myCourses;
	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "response");
					setData(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-lessons",
				"fail to get api/dashboard/recent-lessons",
				(response) => {
					Helper.cl(response, "response");
					setRecentLessons(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-tasks",
				"fail to get api/dashboard/recent-tasks",
				(response) => {
					Helper.cl(response, "response");
					setRecentTasks(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recently-added-contents",
				"fail to get api/dashboard/recently-added-contents",
				(response) => {
					Helper.cl(response, "response");
					setRecentlyAddedContents(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/statistics",
				"fail to get api/dashboard/statistics",
				(response) => {
					Helper.cl(response, "response");
					setStatistics(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/today",
				"fail to get api/dashboard/today",
				(response) => {
					Helper.cl(response, "response");
					setTodayActivities(response.data?.data);
				},
				() => {}
			);

			//----------------------------Courses
			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isMaster: false },
				"success",
				"fail",
				(response) => {
					setCourses1(response?.data?.courses?.data);
				},
				() => {},
				false
			);

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isInstance: true },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setCourses2(response?.data?.courses?.data);
				},
				() => {},
				false
			);

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isPublished: true },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setCourses3(response?.data?.courses?.data);
				},
				() => {},
				false
			);
		};
		get_it();
	}, []);

	const [unpublishedContent, setUnpublishedContent] = useState();

	useEffect(() => {
		let unpublishedContent1 = [];
		// data.recentLessons[].todayOrTommorrowUnpublishedLessons.[].unpublishedContents
		data?.recentLessons?.map((l) =>
			l?.todayOrTommorrowUnpublishedLessons?.map((t) =>
				t.unpublishedContents?.map((co) => {
					unpublishedContent1.push({
						icon:
							co.type === "link"
								? "fa fa-link"
								: co.type === "live_session"
								? "fa fa-video"
								: co.type === "file"
								? "fa fa-file"
								: co.type === "text"
								? "fa fa-file-alt"
								: "fa fa-pen",
						course: l.name,
						subName: co.name,
						publishCallback: () => {
							dispatch1(publishAndUnPublishModuleContentAsync(l.module_id, co.id, true));
						},
					});
				})
			)
		);

		data?.whatWeHaveForToday?.unpublishedModuleContents.map((co) => {
			unpublishedContent1.push({
				icon:
					co.type === "polls"
						? "fa fa-file"
						: co.type === "surveys"
						? "fa fa-file"
						: co.type === "exams"
						? "fa fa-file"
						: co.type === "quizs"
						? "fa fa-file"
						: co.type === "projects"
						? "fa fa-file"
						: co.type === "assignments"
						? "fa fa-file"
						: "fa fa-pen",
				course: co.module.course.name,
				id: co.id,
				subName: co.type + ":" + co.title,
				publishCallback: () => {
					dispatch1(publishAndUnPublishModuleContentAsync(co.module_id, co.id, true));
					setPublishedContentIds([...publishedContentIDs, co.id]);
				},
			});
		});
		Helper.cl(unpublishedContent1, "unpublishedContent1");
		setUnpublishedContent(unpublishedContent1);
	}, [data?.recentLessons, publishedContentIDs]);

	const items = recentTasks?.activeTasks?.map((t) => getTimeLineItemFromTask(t));

	const [myCoursesCount, setMyCoursesCount] = useState(0);

	// In your component...
	const [consumedCount, setConsumedCount] = useState(0);
	const [providerCount, setProviderCount] = useState(0);
	const [clientCount, setClientCount] = useState(0);

	const unpubs = unpublishedContent?.filter((u) => !publishedContentIDs?.filter((p) => p == u?.id)?.length);

	return (
		<Container>
			{/* ----------------------------------------------------------------------- */}
			{/* ------------------------1-------greeting search------------------------- */}

			<GGreetingSearch />
			{/* ----------------------------------------------------------------------- */}
			{/* ------------------------2-------courss live sessions------------------------- */}

			<Row>
				{(userType == "ca" || userType == "cd" || userType == "fa") && myCoursesCount > 0 ? (
					<></>
				) : (
					<RDiscover
						lines={[
							<p style={{ color: "white", fontSize: "17px" }}>
								Seems that you're <span style={{ color: "#F58B1F" }}>new</span> hear
							</p>,

							<p style={{ color: "white", fontSize: "26px" }}>
								{userType !== "ad" ? "Start By Creating a new Course" : "Start By Adding Users and Roles"}
							</p>,
						]}
						link={userType !== "ad" ? "my-courses" : "/users-and-permissions/role/add"}
					/>
				)}
			</Row>
			{/* ----------------------------------------------------------------------- */}
			{userType !== "ad" ? (
				<Row>
					<Col md={8}>
						<RHidable count={myCoursesCount} title="My Courses" nodata={"No Created Courses Yet "}>
							<GMyCourses setCount={setMyCoursesCount} advanced={false} perView={2} />
						</RHidable>
					</Col>

					<Col md={4}>
						{/* <GTodayLiveSessions /> */}
						<RHeader title="Today's Live Sessions" count={data?.recentLessons?.length} />
						{data?.recentLessons?.length ? (
							data?.recentLessons?.map((e) => {
								return (
									<div>
										<RRLiveSessions
											meetingId={e.nextLesson?.livesession?.live_session?.meeting_id}
											userType={"teacher"}
											text="manage"
											time={moment(e?.nextLesson?.lesson_time, "HH:mm:ss").format("hh:mm")}
											AmPm={moment(e?.nextLesson?.lesson_time, "HH:mm:ss").format("A")}
											cours={"course" + e?.name}
											lessonTitle={e?.nextLesson?.subject}
											leftTime={getRelativeDate(e?.nextLesson?.lesson_date?.split(" ")[0] + "T" + e?.nextLesson?.lesson_time)}
											onClickAction={() => {
												history.push(
													`${baseURL}/${genericPath}/course-management/course/${e.id}/cohort/${e.cohort_id.cohort_id}/category/${e.category_id}/lesson/${e?.nextLesson?.id}/meeting/${e.nextLesson?.livesession?.live_session?.meeting_id}`
												);
											}}
										/>
									</div>
								);
							})
						) : (
							<RNoDataLine messageNoData={myCoursesCount < 1 ? "No Created Courses Yet" : "No Live Sessions"} />
						)}
					</Col>
				</Row>
			) : (
				<></>
			)}
			{/* ----------------------------------------------------------------------- */}
			{/* ------------------------3------statistics-------------------------- */}
			<Row>
				<GStatisticsRow
					data={
						userType != "ad"
							? {
									community: { type: "community", title: "community", value: "no new collaborated Items" },
									courses: { type: "courses", title: "Course Managment", value: "no new courses" },
									...statistics?.statistics,
							  }
							: {
									meetings: { type: "meetings", title: "Meetings", value: "no new meetings" },
									...statistics?.statistics,
							  }
					}
				/>{" "}
			</Row>
			{/* ----------------------------------------------------------------------- */}
			{/* ------------------------4-----caklender------------------------------- */}
			<Row>
				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					<GWeekEvents />
				</Col>

				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					<GCalender advanced={false} />
				</Col>
			</Row>
			{/* ----------------------------------------------------------------------- */}
			{/* ----------------------------------------------------------------------- */}

			{/* ----------------------------------------------------------------------- */}
			{/* -------------------5---unpublished and tasks-------------------------- */}

			{(userType == "ca" || userType == "cd") && myCoursesCount > 0 ? (
				<>
					<Row>
						<Col xs={12} md={6}>
							{/* {Helper.js(unpublishedContent)} */}
							<RHidable title={"Unpublished Content"} count={unpubs?.length} nodata="no unpublished Contnet">
								{/* {Helper.js(unpublishedContent.map(i=>i.id),"unpub")}
          {Helper.js(publishedContentIDs,"pubs")} */}
								<GUnpublished data={unpubs} />
							</RHidable>
							{/* <courses advanced={false}/> */}
						</Col>
						<Col xs={12} md={6}>
							<RHeader title="Tasks" count={items?.length} />
							<RHidable count={items?.length} nodata="no current tasks">
								<RTimeLine items={items} color="#668AD7" />
							</RHidable>
						</Col>

						{/* -------------------------------------------------- */}
					</Row>
				</>
			) : userType == "fa" && myCoursesCount > 0 ? (
				<Row className="mt-4">
					<Col xs="12" md="7">
						<div style={{ height: "100%" }}>
							<RHeader title="Student submited Tasks" count={data?.whatWeHaveForToday?.ungradedQuestionSets?.length} />
							{data?.whatWeHaveForToday?.ungradedQuestionSets ? (
								<GStudentsSubmitedTasks tasks={data?.whatWeHaveForToday?.ungradedQuestionSets} />
							) : (
								<REmptyData />
							)}
						</div>
					</Col>
					<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
						<div>
							<RTitle text="Days Students are most Active at" />
							<RBieChart
								data={[
									{ text: "su", count: 1 },
									{ text: "mon", count: 2 },
									{ text: "tu", count: 3 },
									{ text: "th", count: 4 },
									{ text: "fr", count: 3 },
								]}
								color={"#007bff"}
								label={"Activity"}
							/>
						</div>
					</Col>
				</Row>
			) : (
				<></>
			)}

			{/* ----------------------------------------------------------------------- */}
			{/* ------------------------6--Collaboration------------------------- */}
			{userType == "ca" || userType == "ad" ? (
				<>
					<Row>
						<RHeader title="My Providers' content " count={providerCount} />
						<RHidable count={providerCount} nodata={"No Providers"}>
							<GCollaborationAreaProviders setCount={setProviderCount} />
						</RHidable>
					</Row>
					<Row>
						<RHeader title="My Client's content " count={clientCount} />
						<RHidable count={clientCount} nodata={"No Providers"}>
							<GCollaborationAreaClients setCount={setClientCount} />
						</RHidable>
					</Row>
				</>
			) : (
				<></>
			)}
			{/* // <Row>
      //   <RHeader title="consumed contents" count={consumedCount}/>
      //   <RHidable count={consumedCount} nodata={"No Providers"}>

      //   <GCollaborationAreaConsumedContents  setCount={setConsumedCount} />
      //   </RHidable>
      // </Row> */}

			{/* ----------------------------------------------------------------------- */}
			{/* --------------------7----new published courses-------------------------- */}
			{userType == "ca" ? (
				<Row>
					<RHeader title="New Published Courses" count={courses1?.length} />
					<RHidable count={courses1?.length} nodata={"No New Published Courses"}>
						<GCoursesLine courses={courses1} perView={3} />
					</RHidable>
				</Row>
			) : (
				<></>
			)}
			{/* ----------------------------------------------------------------------- */}
			{/* --------------------8-----cousrs updates charts----------------------------- */}
			{userType == "ca" ? (
				<Row className="mt-4">
					{/* First column */}
					<Col xs="12" md="6">
						<RHeader title="Course Updates" />
						<hr />
						<div style={{ height: "100%" }}>
							We are Grouing <br />
							new Created Course
							<br />
							<div style={{ fontSize: "35px" }}>{courses?.length}</div>
							<hr />
							<RBieChart
								data={[
									{ text: "su", count: 1 },
									{ text: "mon", count: 2 },
									{ text: "tu", count: 3 },
									{ text: "th", count: 4 },
									{ text: "fr", count: 3 },
								]}
								color={"#007bff"}
								label={"Activity"}
							/>
						</div>
					</Col>
					<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
						<div>
							<RHeader title="Cloned and Instantiated courses" />
							{
								<RPieChart
									style={{ width: "300px" }}
									data={[
										{ text: "cloned courses", count: courses2?.length },
										{ text: "instantiated courses", count: courses3.length },
									]}
								/>
							}
						</div>
					</Col>
				</Row>
			) : (
				<></>
			)}

			{/* ----------------------------------------------------------------------- */}
			{/* ----------------------------------------------------------------------- */}
			{/* ----------------------------------------------------------------------- */}
			{/* ----------------------9-----cloned and newly published courses----------------------------- */}

			{userType == "ca" ? (
				<>
					<Row>
						<RHeader title="Cloned Courses" count={courses2?.length} />
						<RHidable count={courses2?.length} nodata={"No New Instantiated Courses"}>
							<GCoursesLine courses={courses2} perView={3} />
						</RHidable>
					</Row>

					<Row>
						<RHeader title="Newly published Courses" count={courses3?.length} />
						<RHidable count={courses3?.length} nodata={"No New Pubished Courses"}>
							<GCoursesLine courses={courses3} perView={3} />
						</RHidable>
					</Row>
				</>
			) : (
				<></>
			)}
			{/* --------------------------------------------------------------------- */}
			{/* -----------------------Extras---------------------------------- */}
			{userType == "ad" ? (
				<Row className="mt-4">
					<Col xs="12" md="6">
						<RFlex>
							<h6>{tr`Recently added users`}</h6>
						</RFlex>
						<RecentAddedUser />
					</Col>
					<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
						<h6 className="mb-3">{tr`Recent Added Roles`}</h6>
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{usersAndPermissionsData.recentAddedRolesLoading ? (
								<Loader />
							) : (
								usersAndPermissionsData.recentAddedRoles.map((role) => <RRole role={role} lister={true} />)
							)}
						</RFlex>
					</Col>
				</Row>
			) : (
				<></>
			)}
		</Container>
	);
};
export default GCourseAdminDashboard;
