import moment from "moment";

const dateFormat = (date, dateWithTime, dateWithOutTime, dateFormat) => {
  const dateFormatWithTime = "YYYY-MM-DD HH:mm:ss";
  const dateFormatWithoutTime = "YYYY-MM-DD";

  const parsedDate = moment(date);

  if (!parsedDate.isValid()) {
    return "Invalid date"; // Handle invalid input dates gracefully
  }

  const formattedDate = parsedDate.format(
    dateWithTime && !dateFormat
      ? dateFormatWithTime
      : dateWithTime && dateFormat
      ? dateFormat
      : dateWithOutTime && !dateFormat
      ? dateFormatWithoutTime
      : dateWithOutTime && dateFormat
      ? dateFormat
      : dateFormatWithTime
  );

  return formattedDate;
};
export default dateFormat;
