import React from "react";
import styles from "./EventType.module.scss";
import { Row, Button, Col } from "reactstrap";
import RSelect from "components/Global/RComs/RSelect";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

const EventType = ({ id, name, blocks, handleUnBlock, handleBlock, allTypes }) => {
	return (
		<div className="mb-4">
			<Row>
				<Col>
					<span>{name}</span>
				</Col>
				<Col>
					<RSelect
						option={allTypes.ids
							.filter(
								(typeId) =>
									//not the same type
									typeId !== id &&
									//not already blocked
									!blocks.includes(typeId) &&
									//not blocked by the other type
									!allTypes.byId[typeId].blocks.includes(id)
							)
							.map((tId) => ({ label: allTypes.byId[tId].name, value: tId }))}
						placeholder={"select type to block"}
						onChange={(e) => handleBlock(e.value)}
						closeMenuOnSelect
					/>
				</Col>
			</Row>
			<div>
				{blocks.map((bId) => (
					<Row key={"event-blocks" + bId}>
						<Col>{allTypes.byId[bId].name}</Col>
						<Col>
							<RButton faicon={iconsFa6.delete} onClick={() => handleUnBlock(bId)} color="danger" outline />
						</Col>
					</Row>
				))}
			</div>
		</div>
	);
};

export default EventType;
