export const backend = process.env.REACT_APP_BACKEND_URL;
export const versionServiceComponent = process.env.REACT_APP_VERSION_SERVICE_COMPONENT;
export const versionServiceUnitPlan = process.env.REACT_APP_VERSION_SERVICE_UNIT_PLAN;
export const versionServiceRubric = process.env.REACT_APP_VERSION_SERVICE_RUBRIC;
export const LoginService = process.env.LoginService;
export const genericPath = process.env.REACT_APP_GENERIC_PATH;
export const files = backend + "api/files/get/";
export const baseURL = "";
// export const appDomain = "http://localhost/scorm/";
export const appDomain = `${window !== "undefined" ? window?.location.origin : ""}/`;
export const pusher_key = "6d0ef7c934860c779f49";

export const online = true;
export const sidebarOnline = true;
export const specificOrganization = 307;
export const newAuth = false;
export const removeApisNotWorkingFromLocale = false;
export const newAuthToken =
	"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxMSIsImp0aSI6ImY1YmE1ZjE4MmYxMmNkZjhjMDQ2OGE4NWMxOGI3YzQzMGE2OGQzZWI1YjIyZmE3MWUxYzExNTUwMTc1Zjc5YWEyNDc3ZThiZDBiZWUxNzJkIiwiaWF0IjoxNzEzNzc3MjQ5LjE2OTU4NSwibmJmIjoxNzEzNzc3MjQ5LjE2OTU5MiwiZXhwIjoxNzI5NTg4NDQ5LjA2MzU0Nywic3ViIjoiODAiLCJzY29wZXMiOltdfQ.KIEdKNjWcW4Pqg0NBEUVxQZPoi7DEY1IHkxXNwkn3cLejrOFy-g4v_NFL0TJ5B0u6R6cQrqZeJvcOq6eUB5UMHBaqqda5He6iW1BsTjOernlRRBToLAeNAclwyLn8L3qkMf-sNNHWv4MDQ837l_K9Vem9fA3y7MfXzTLTWQJDk2QO-zRQGfbQIw0l84iTFDEyLWg24wX-MkyjowfXV_pgbKXoZJrAgJ9WJBzQJu9Uy2wHIEOYa2_b_qvEcalDs0HnrPccdpyra5lxJ3t7ayAKpMrPjlB6cq2mk7rBykI3v99QNrQZ7g1TeInDANvh23ZxOUMEeIYc-0934FVt8m4SvYQylXab6yublHHy7WfpxpdpPeBpEQ7gji2ti79XzlyVjPR5GQabpV1ZAcFems6tDt1k4ZGZTRREuxfqr7PKx20Rfh_8QUs_ioIWue6O8-eL92ONy039km0x9lrkeH5hcTZDhzTFS36mMgHoRxRhIb8Tg_gqM5pcf-7hQcNnM2FWQLKMWzKBddn5NKfyTk1djQhY8aKxYzJyYqfddwVhdlR7I3ZvSIhzvzym-ugVR35_bjOBdrQaygvaAGY5dTdmZBpEznO-Xjiwn8AXv8fgesswv5j5c_nHxq87t8Y_5qctxN25ZCqijVCR4e1ZA-lr2cl-CKyhYZ0Yp4GJZvmFiw";
