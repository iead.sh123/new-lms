import React from "react";
import { Button, UncontrolledTooltip, Modal, ModalBody } from "reactstrap";
import NewNote from "./NewNote";
import { openNotes } from "store/actions/IM/chatActions";
import { useState } from "react";
import tr from "components/Global/RComs/RTranslator";

function NotesButton({ tableName, id, nameToShow }) {
  const [openModal, setOpenModal] = useState(false);

  const tooltipid = "Tooltip" + Math.floor(Math.random() * 1000000);
  return (
    <>
      <Button
        className="btn-icon"
        color="info"
        id={tooltipid}
        size="sm"
        type="button"
        onClick={() => {
          const exists = openNotes(tableName, id);
          if (exists == 5) {
            return;
          } else {
            setOpenModal(true);
          }
        }}
      >
        <i className="nc-icon nc-chat-33" />
      </Button>
      <UncontrolledTooltip delay={0} target={tooltipid}>
        {tr("notes_about_this_item")}
      </UncontrolledTooltip>
      <Modal isOpen={openModal} toggle={setOpenModal} size="lg">
        <div className="modal-header justify-content-center">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
            onClick={() => setOpenModal(false)}
          >
            <span aria-hidden="true">×</span>{" "}
          </button>
        </div>
        <ModalBody>
          <NewNote
            tableName={tableName}
            id={id}
            nameToShow={nameToShow}
          ></NewNote>
        </ModalBody>
      </Modal>
    </>
  );
}

export default NotesButton;

//   import React ,{useState} from "react";
// import ChatContent from './chatContent/chatContent';
// import ChatList from './chatList/chatList';
// import NewMessage from "./newMessage/NewMessage";
// import  FloatingChat  from "./chatContent/floatingChat";
// import './chat.css';
// import { useDispatch } from "react-redux";
// import { setChat } from "store/actions/IM/chatActions";
//  const NotesButton=({tableName,rowID})=> {
//     const dispatch = useDispatch();
// //tableName="attachment" rowID
//     dispatch(setChat( tableName , rowID ));
// };
// return <button
//  onClick={(e) => showChat() } >show chat</button>
//  }
//  export default NotesButton;
