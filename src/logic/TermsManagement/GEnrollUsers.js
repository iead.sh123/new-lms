import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { termsManagementApi } from "api/global/termsManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
 import RRenderUserInfo from "components/Global/RComs/RRenderUserInfo";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import GCreateUser from "./GCreateUser";
 import iconsFa6 from "variables/iconsFa6";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RLister from "components/Global/RComs/RLister";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import { useLocation } from "react-router-dom";
import REmptyData from "components/RComponents/REmptyData";

const GEnrollUsers = ({
	enrollUsers,
	userType,
	values,
	handleSelectedUser,
	handleCloseEnrollUsers,
	setFieldValue,
	handleSelectedCourseToLoading,
}) => {
	const TermsManagementData = useContext(TermsManagementContext);
	const location = useLocation();
	const AcademicPath = location.pathname.includes("academics");

	const [search, setSearch] = useState("");
	const [createUser, setCreateUser] = useState(false);
	const [page, setPage] = useState(1);

	const handleChangePage = (pageNumber) => {
		setPage(pageNumber);
	};

	const usersData = useFetchDataRQ({
		queryKey: [userType, search, page],
		queryFn: () =>
			userType == "teachers"
				? termsManagementApi.getTeachersForOrganization({ userId: 14, searchText: search, page })
				: userType == "learners" || userType == "students"
				? termsManagementApi.getStudentsForSpecificGrade({
						gradeLevelId: AcademicPath ? -1 : TermsManagementData.itemObj.gradeLevelId,
						searchText: search,
						AcademicPath: AcademicPath,
						page: page,
				  })
				: "",
		enabledUrls: userType !== "",
		keepPreviousData: true,
	});

	const userMutation = useMutateData({
		queryFn: ({ userId, data }) => termsManagementApi.addUserToOrganization({ userId, data }),
		invalidateKeys: [userType, search],
		closeDialog: () => setCreateUser(false),
	});

	const StudentsData = AcademicPath ? usersData?.data?.data?.data?.data : usersData?.data?.data?.data?.data?.data;
	const filteredStudents = StudentsData && StudentsData.filter((item) => !values?.studentsById?.some((el) => el.user_id === item.id));

	const filteredTeachers =
		usersData?.data?.data?.data?.users?.data &&
		usersData?.data?.data?.data?.users?.data.filter((item) => !values?.teachersById?.some((el) => el.user_id === +item.id));

	const _records = React.useMemo(
		() =>
			(filteredStudents ?? filteredTeachers)?.map((user) => ({
				details: [
					{
						key: tr`name`,
						value: {
							name: user?.name ?? user?.label,
							image: user?.hash_id ?? user?.profile?.hash_id,
						},
						render: RRenderUserInfo,
					},
				],
				actions: [
					{
						name: " ",
						// icon: values[userType] && values[userType]?.some((item) => +item.user_id == +user.id) ? iconsFa6.minusSolid : iconsFa6.plus,
						icon: values[userType] && values[userType]?.some((item) => +item.user_id == +user.id) ? iconsFa6.spinner : iconsFa6.plus,
						color: "primary",
						// color: values[userType] && values[userType]?.some((item) => +item.user_id == +user.id) ? "danger" : "primary",

						// disabled:
						// 	TermsManagementData.itemObj.idsLoading.length == 0
						// 		? false
						// 		: TermsManagementData.itemObj.idsLoading.includes(user.id)
						// 		? false
						// 		: true,

						outline: true,
						onClick: () => {
							// handleSelectedCourseToLoading({ courseId: user.id });
							TermsManagementData.handleEnrollSingleUser({ userType: userType, userId: user.id });
							handleSelectedUser(userType, user.id, user.hash_id ?? user?.profile?.hash_id, user.name);
						},
					},
				],
			})),
		[filteredStudents, filteredTeachers, usersData?.data?.data?.data, values[userType], TermsManagementData.itemObj.idsLoading]
	);

	const handleSearch = (emptyData) => {
		setSearch(emptyData !== undefined ? "" : values.searchText);
	};

	const handleOpenCreateUser = () => {
		setCreateUser(true);
	};
	const handleCloseCreateUser = () => {
		setCreateUser(false);
	};

	if (usersData.isLoading) {
		return <Loader />;
	} else if (usersData.isError) {
		return <REmptyData />;
	}
	return (
		<div>
			<AppModal
				size={"md"}
				show={createUser}
				// header={<span>{tr`create_new_profile`}</span>}
				header={true}
				parentHandleClose={handleCloseCreateUser}
				headerSort={
					<GCreateUser
						userType={userType}
						addUserMutate={userMutation}
						gradeLevelId={TermsManagementData.itemObj.gradeLevelId}
						AcademicPath={AcademicPath}
					/>
				}
			/>
			<RFlex className="flex-column align-items-start">
				<RSearchHeader
					searchLoading={usersData.isLoading}
					searchData={values.searchText}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("searchText", value)}
					handleChangeSearch={(value) => setFieldValue("searchText", value)}
				/>
				<RFlex className="justify-content-end align-items-center" styleProps={{ width: "100%" }}>
					{/* <RFlex className="align-items-center">
						<span className="p-0 m-0">
							{values[userType]?.length} {tr`selected`}
						</span>
						{TermsManagementData.enrollUsersToCourseLoading || TermsManagementData.enrollUsersToHomeRoomLoading ? (
							<i className={iconsFa6.spinner} />
						) : (
							<span
								style={{ color: primaryColor, textDecoration: "underline", cursor: "pointer" }}
								onClick={() => {
									enrollUsers ? TermsManagementData.handleEnrollUsers() : handleCloseEnrollUsers();
								}}
							>{tr`save`}</span>
						)}
					</RFlex> */}

					<RButton text={tr`create_account`} faicon={iconsFa6.plus} color="primary" outline onClick={() => handleOpenCreateUser()} />
				</RFlex>
			</RFlex>

			<RLister Records={_records} info={usersData} withPagination={true} handleChangePage={handleChangePage} page={page} />
		</div>
	);
};

export default GEnrollUsers;
