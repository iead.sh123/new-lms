import React from "react";
import { DANGER } from "utils/Alert";
import { Input } from "reactstrap";
import { IMApi } from "api/IMNewCycle";
import RFileUploader from "components/Global/RComs/RFileUploader";
import MessageInput from "../chatContent/MessageInput";
import AsyncSelect from "react-select/async";
import Swal from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

function NewMessage({ handleSend, sending, loggedUserId, toggleNewMessage, forwardTo }) {
	const debounceTimer = React.useRef(null);
	const [receiverIds, setReceiversIds] = React.useState([]);
	const [searchValue, setSearchValue] = React.useState("");
	const [groupName, setGroupName] = React.useState("");

	const [groupImage, setGroupImage] = React.useState(null);

	const onSelectedUsersChange = (values) => setReceiversIds(values.map((v) => ({ id: v.id, username: v.label, rel: v.rel })));

	const debouncedLoadUsers = React.useCallback(
		(delay = 1000) => {
			const loadUsers = async () => {
				const response = await IMApi.getAllUsers(searchValue);
				if (!response.data.status) return [];

				return response.data?.data?.models
					.filter((usr) => +usr.id !== loggedUserId)
					.map((usr) => ({
						label: usr.full_name,
						value: usr.id,
						id: usr.id,
						rel: usr.label,
					}));
			};

			return new Promise((resolve) => {
				clearTimeout(debounceTimer.current);
				debounceTimer.current = setTimeout(() => {
					resolve(loadUsers());
				}, delay);
			});
		},
		[searchValue]
	);

	const validate = () => {
		if (receiverIds.length >= 2 && groupName === "") {
			toast.error(tr`group name must not be empty`);

			return false;
		}
		return true;
	};

	return (
		<div>
			<div className={"new_messege_show "}>
				<div className="new_messege_show_head">
					<button className="show_icon" onClick={toggleNewMessage}>
						<i className="fa fa-angle-left"></i>
					</button>
					{tr("new_chat")}
				</div>

				<div className="RAutocomplete" style={{ flexGrow: 1 }}>
					<AsyncSelect
						isMulti
						onInputChange={(val) => setSearchValue(val)}
						defaultValue={[]}
						loadOptions={() => debouncedLoadUsers(1000)}
						onChange={onSelectedUsersChange}
						placeholder={tr`select`}
						noOptionsMessage={() => tr`no_options`}
					/>
					<ul>
						{receiverIds.map((rc) => (
							<li key={"rc-" + rc.id}>
								<div>
									{" "}
									{rc.username} : {rc.rel}{" "}
								</div>
							</li>
						))}
					</ul>
				</div>

				<div>
					{receiverIds.length > 0 && (
						<>
							{receiverIds.length > 1 && (
								<div style={{ display: "flex", marginBottom: "10px" }}>
									<Input placeholder="group name" value={groupName ?? ""} onChange={(e) => setGroupName(e.target.value)} />

									{/* <div style={{ display: "flex", justifyContent: "center" }}> */}
									<RFileUploader parentCallback={(e) => setGroupImage(e[0])} fileSize={5} singleFile />
									{/* </div> */}
								</div>
							)}

							<MessageInput
								validateEmptyMessage={true}
								sendMessage={(message, attachments) =>
									validate() && handleSend(message, attachments, receiverIds, groupName, null, groupImage)
								}
								className="new-messege-input-area"
								sending={sending}
							/>
						</>
					)}
				</div>
			</div>
		</div>
	);
}

export default NewMessage;
