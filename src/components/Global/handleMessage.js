import React from "react";

const handleMessage = (message) => {
  const newLine = "[newline]";
  const url = "[url]";
  const closeUrl = "[/url]";
  const rightArc = "[";
  const leftArc = "]";

  let messageToLowerCase = message; //.toLocaleLowerCase();

  for (let i = 0; i <= messageToLowerCase.length; i++) {
    if (messageToLowerCase.includes(newLine)) {
      messageToLowerCase = messageToLowerCase.replaceAll(
        newLine.toLowerCase(),
        "</br>"
      );
    } else if (messageToLowerCase.includes(url)) {
      const regexUrl = /\[url\].*?\[\/url\]/g;
      // const regexReplaceUrl = /\[url\]/g;
      // const regexReplaceCloseUrl = /\[\/url\]/g;

      // const textRegexUrl = text.replaceAll(
      //   regexReplaceUrl,
      //   `<a href='${hrefUrl[0]}'>`
      // );
      // const textRegexCloseUrl = textRegexUrl.replaceAll(
      //   regexReplaceCloseUrl,
      //   "</a>"
      // );

      // messageToLowerCase = textRegexCloseUrl;

      const hrefUrl = messageToLowerCase
        .match(regexUrl)
        ?.map((el) => el.replace(/\[\/?url\]/g, ""));

      const openTag = messageToLowerCase.replaceAll(
        url.toLocaleLowerCase(),
        `<a href="${hrefUrl[0]}" target="_blank">`
      );
      messageToLowerCase = openTag.replaceAll(
        closeUrl.toLocaleLowerCase(),
        "</a>"
      );
    } else if (!messageToLowerCase.includes(newLine || url || closeUrl)) {
      const deleteRightArc = messageToLowerCase.replaceAll(
        rightArc,
        '<button id="handleMessage">#'
      );
      const deleteLeftArc = deleteRightArc.replaceAll(leftArc, "</button>");
      messageToLowerCase = deleteLeftArc;
    } else {
      return messageToLowerCase;
    }
  }

  return messageToLowerCase;
};

export default handleMessage;
