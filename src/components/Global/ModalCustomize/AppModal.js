import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

function AppModal({ show, header, footer, parentHandleClose, classStyle, headerSort, style, size, modalBodyClass }) {
	const handleClose = () => {
		parentHandleClose(false);
	};
	return (
		<div>
			<Modal style={style} isOpen={show} className={classStyle} size={size} autoFocus={false}>
				<ModalHeader toggle={handleClose} style={{ display: typeof header == "boolean" ? "block" : "" }}>
					<span className="d-flex">{header}</span>
				</ModalHeader>
				{headerSort && <ModalBody className={modalBodyClass ?? ""}>{headerSort ?? <></>}</ModalBody>}
				{footer && <ModalFooter>{footer}</ModalFooter>}
			</Modal>
		</div>
	);
}
export default AppModal;
