import React, { useEffect, useReducer } from "react";
import {
  usersAndPermissionsReducer,
  initialState,
} from "../State/UsersAndPermissions.reducer";
import { getAllCategoriesOrganizationAsync } from "../State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import { useHistory } from "react-router-dom";

const PermissionCategoriesTabs = () => {
  const history = useHistory();
  const [usersAndPermissionsData, dispatch] = useReducer(
    usersAndPermissionsReducer,
    initialState
  );

  useEffect(() => {
    if (usersAndPermissionsData.categoriesOrganization.length == 0) {
      getAllCategoriesOrganizationAsync({
        dispatch: dispatch,
        forDropDownList: false,
        history: history,
        pushToFirstTab: true,
      });
    }
  }, []);

  return (
    <React.Fragment>
      {usersAndPermissionsData.categoriesOrganizationLoading ? (
        <Loader />
      ) : (
        <RTabsPanel_Vertical
          Tabs={usersAndPermissionsData.categoriesOrganization}
        />
      )}
    </React.Fragment>
  );
};

export default PermissionCategoriesTabs;
