import React from "react";
import { Button, Input } from "reactstrap";
import RFlex from "./RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
function RButton({
	className,
	classNameText,
	disabled,
	outline,
	loading,
	onClick,
	hidden,
	faicon,
	style,
	color,
	text,
	type = "button",
	key,
	id,
	active,
	size,
	iconRight,
}) {
	return (
		<Button
			key={key ? key : ""}
			className={className}
			// value={Input}
			onClick={onClick}
			color={color}
			id={id}
			type={type}
			disabled={disabled ? disabled : false}
			outline={outline ? outline : false}
			style={style}
			hidden={hidden}
			active={active}
			size={size}
		>
			{loading ? (
				<RFlex styleProps={{ alignItems: "center", justifyContent: "center" }}>
					{text && <span>{text}</span>}
					<i className={iconsFa6.spinner + " m-0"} />
				</RFlex>
			) : faicon && !text ? (
				<i className={faicon} />
			) : text && faicon ? (
				<>
					{iconRight ? (
						<RFlex styleProps={{ gap: 5 }}>
							{text}
							<i className={faicon + " pt-1"}>&nbsp;</i>
						</RFlex>
					) : (
						<>
							<i className={faicon}>&nbsp;</i>
							{text}
						</>
					)}
				</>
			) : (
				<span className={classNameText}>{text}</span>
			)}
		</Button>
	);
}
export default RButton;
