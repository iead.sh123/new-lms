import React from "react";
import { Table } from "reactstrap";
import styles from "./RPrinting.Module.scss";
import tr from "../RTranslator";

const LessonPlanPrinting = ({ data }) => {
  return (
    <>
      {data.lesson_plan_items.map((lesson_plan_item) => (
        <Table bordered>
          <thead>
            <tr>
              <th className="ListHeading">{lesson_plan_item.subject}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p />
                <space className={styles.RPrintSpaceRubricTitle}>
                  <h6 className={styles.colorTitle}>
                    {tr`description`} : &nbsp;
                  </h6>
                  <h6
                    dangerouslySetInnerHTML={{
                      __html: lesson_plan_item.description,
                    }}
                  />
                </space>
                <space className={styles.RPrintSpaceRubricTitle}>
                  <h6 className={styles.colorTitle}>{tr`time`} : &nbsp;</h6>
                  <h6>{lesson_plan_item.time}</h6>
                </space>
              </td>
            </tr>
          </tbody>
        </Table>
      ))}
    </>
  );
};

export default LessonPlanPrinting;
