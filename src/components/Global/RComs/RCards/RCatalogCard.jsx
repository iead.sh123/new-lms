import React from "react";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { primaryColor, successColor, warningColor } from "config/constants";
import { Card, CardImg, Progress, Row, Col } from "reactstrap";
import { faPlusSquare, faStar } from "@fortawesome/free-regular-svg-icons";
import { baseURL, genericPath } from "engine/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Services } from "engine/services";
import DefaultImage from "assets/img/new/course-default-cover.png";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import iconsFa6 from "variables/iconsFa6";
import styles from "./RCatalogCard.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import Helper from "../Helper";
import TruncatedDescription from "./TruncatedDescription";
import RButton from "../RButton";
import { useHistory, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import { hexToRGBA } from "utils/hexToRGBA";
import { useSelector } from "react-redux";

const RCatalogCard = ({ course, forceStyle, studentTypes, catalogMode = "false" }) => {
	const location = useLocation();
	const history = useHistory();
	const colorRGBA = course?.color ? hexToRGBA(course?.color, 0.7) : hexToRGBA(primaryColor, 0.7);
	const color = course?.color ? course?.color : primaryColor;
	const { user } = useSelector((state) => state.auth);

	return (
		<div className={styles.card_with_options} onClick={() => course.link && history.push(course.link)}>
			<div
				className={styles.top_part}
				style={{
					background: "url('" + course?.image + "'), lightgray 50% / cover no-repeat",
					backgroundSize: "cover",
				}}
			>
				<div className={styles.top_part_col1}>
					<div className={styles.category__rect}>
						{course?.categoryName
							? course?.categoryName.length > 12
								? course?.categoryName.substring(0, 12)
								: course?.categoryName
							: uncategorized}
					</div>
					<div className={styles.status__rect} style={{ color: course?.isPublished ? successColor : warningColor }}>
						<i className={course?.isPublished ? "fa fa-check pt-1" : "fa fa-pen pt-1"} />
						<div>{course?.isPublished ? tr`Active` : "Draft"}</div>
					</div>
				</div>
				{/* <div className={styles.top_part_col2}>
		col2
	</div> */}
				<div className={styles.top_part_col3}>
					{course?.actions && course?.actions.length > 0 && (
						<UncontrolledDropdown direction="right">
							<DropdownToggle
								aria-haspopup={true}
								color="default"
								data-toggle="dropdown"
								onClick={(event) => {
									event.stopPropagation();
								}}
								nav
								style={{ padding: "0px" }}
							>
								{/* <i class="fa fa-ellipsis-v" aria-hidden="true" style={{  cursor: "pointer", color: "#fff" }}></i> */}
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect width="24" height="24" rx="12" fill="#668AD7" fill-opacity="0.7" />
									<path
										d="M12.5 9.90323C13.3292 9.90323 14 10.6173 14 11.5C14 12.3827 13.3292 13.0968 12.5 13.0968C11.6708 13.0968 11 12.3827 11 11.5C11 10.6173 11.6708 9.90323 12.5 9.90323ZM11 7.59677C11 8.47944 11.6708 9.19355 12.5 9.19355C13.3292 9.19355 14 8.47944 14 7.59677C14 6.71411 13.3292 6 12.5 6C11.6708 6 11 6.71411 11 7.59677ZM11 15.4032C11 16.2859 11.6708 17 12.5 17C13.3292 17 14 16.2859 14 15.4032C14 14.5206 13.3292 13.8065 12.5 13.8065C11.6708 13.8065 11 14.5206 11 15.4032Z"
										fill="white"
									/>
								</svg>
							</DropdownToggle>
							<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
								{course?.actions?.map((action, index) => (
									<DropdownItem
										key={action.id}
										disabled={action.disabled ? action.disabled : false}
										onClick={(e) => {
											e.stopPropagation();
											action.onClick();
										}}
										hidden={action.hidden}
									>
										<RFlex
											styleProps={{
												alignItems: "center",
											}}
										>
											<i style={{ color: action.color ?? "#000" }} className={action?.icon} aria-hidden="true" />
											<span style={{ color: action.color ?? "#000" }}>{action.name}</span>
										</RFlex>
									</DropdownItem>
								))}
							</DropdownMenu>
						</UncontrolledDropdown>
					)}
				</div>
				{/* background: url(<path-to-image>), lightgray 50% / cover no-repeat; */}
			</div>
			<div className={styles.course__title}>
				{course?.name ? (course?.name?.length > 17 ? course?.name.substring(0, 17) + ".." : course?.name) : ""}
			</div>
			<div className={styles.course__description}>
				{<TruncatedDescription description={course?.description ?? "No Description"} maxHeight={42} />}
			</div>
			<button className={styles.add__to__template__button}>add to tempate</button>
		</div>
	);

	return (
		<div className={styles.r__card} style={forceStyle}>
			<Card
				className={styles.flip__card__front}
				style={
					!catalogMode
						? { borderBottom: `4px solid ${color}`, cursor: "pointer" }
						: { borderRadius: "5px", background: "#FBFBFB", boxShadow: "0px 1px 3px 0px rgba(0, 0, 0, 0.25)", cursor: "pointer" }
				}
				key={course?.id}
				onClick={() => course.link && history.push(course.link)}
			>
				<div className={styles.r__image}>
					<CardImg
						className={styles.card_img}
						src={course?.image ?? DefaultImage}
						alt={course?.image}
						style={
							!catalogMode
								? { cursor: course.link ? "pointer" : "" }
								: { cursor: course.link ? "pointer" : "", width: "205px", height: "130px" }
						}
					/>
					<section className={styles.r__content__image}>
						{course?.categoryName ? (
							<div className={styles.category__name} style={{ background: `${colorRGBA}` }}>
								{course?.categoryName.length > 12 ? course?.categoryName.substring(0, 12) : course?.categoryName}
							</div>
						) : (
							<div className={styles.category__name} style={{ background: `${colorRGBA}` }}>
								uncategorized
							</div>
						)}

						<RFlex style={{ justifyContent: "flex-end" }}>
							{course?.rate ? (
								<div className={styles.section_stars} style={{ background: `${colorRGBA}` }}>
									<FontAwesomeIcon icon={faStar} />
									{course?.rate}
								</div>
							) : (
								""
							)}
							{/* {Helper.js(course?.actions.length,"course?.actions.length")} */}
							{course?.actions && course?.actions.length > 0 && (
								<UncontrolledDropdown direction="end">
									<DropdownToggle
										aria-haspopup={true}
										color="default"
										data-toggle="dropdown"
										nav
										style={{
											background: `${colorRGBA}`,
											// border:"3px green solid"
											//width:!catalogMode?null:"10px"
										}}
										className={styles.card__option}
										onClick={(event) => {
											event.stopPropagation();
										}}
									>
										<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer", color: "#fff" }}></i>
									</DropdownToggle>
									<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
										{course?.actions?.map((action, index) => (
											<DropdownItem
												key={action.id}
												disabled={action.disabled ? action.disabled : false}
												onClick={(e) => {
													e.stopPropagation();
													action.onClick();
												}}
												hidden={action.hidden}
											>
												<RFlex
													styleProps={{
														alignItems: "center",
													}}
												>
													<i style={{ color: action.color ?? "#000" }} className={action?.icon} aria-hidden="true" />
													<span style={{ color: action.color ?? "#000" }}>{action.name}</span>
												</RFlex>
											</DropdownItem>
										))}
									</DropdownMenu>
								</UncontrolledDropdown>
							)}
						</RFlex>
					</section>

					<RFlex className={course?.isPublished ? styles.published : styles.draft} style={{ zIndex: "9999", marginLeft: "0px" }}>
						<RFlex styleProps={{ color: course?.isPublished ? successColor : warningColor }}>
							<i className={course?.isPublished ? "fa fa-check pt-1" : "fa fa-pen pt-1"} />
							<p>{course?.isPublished ? tr`Active` : "Draft"}</p>
						</RFlex>
					</RFlex>
				</div>

				<section>
					<RFlex styleProps={{ justifyContent: "space-between", width: "100%", padding: "10px" }}>
						<RFlex styleProps={{ alignItems: "center" }}>
							{course?.name && (
								<span className={styles.card_name} style={{ color: `#000000` }}>
									{course?.name?.length > 17 ? course?.name.substring(0, 17) : course?.name}
								</span>
							)}
							{course?.icon ? (
								<img className={styles.card_icon} src={course?.icon} alt={course?.icon} />
							) : (
								<i className={iconsFa6.book + " fa-lg"} alt={"default_icon"} />
							)}
						</RFlex>
						{course?.isCloned ? <span className="pt-1" style={{ color: primaryColor, fontWeight: "bold" }}>{tr`cloned`}</span> : ""}
						<RFlex styleProps={{ alignItems: "center" }}>
							{course?.isInstance ? (
								<span className="pt-1" style={{ color: primaryColor, fontWeight: "bold" }}>{tr`instantiated`}</span>
							) : (
								" "
							)}{" "}
							{course?.add && (
								<div style={{ display: "flex", alignItems: "center" }}>
									<FontAwesomeIcon icon={faPlusSquare} color={`${color}`} size="lg" id={`test`} />
								</div>
							)}
						</RFlex>
					</RFlex>
					{course?.title && (
						<RFlex styleProps={{ padding: "0px 10px" }}>
							<p className="text-muted">{course?.title}</p>
						</RFlex>
					)}
					{!catalogMode ? (
						<div>
							<RFlex styleProps={{ padding: "0px 10px" }}>
								{course?.startedAt ? (
									<p className="text-muted">
										{tr`Start On : `}
										{course?.startedAt}
									</p>
								) : (
									""
								)}

								{!course?.isOnline ? <p>{tr`anytime, anywhere`}</p> : ""}
								{course?.comingSoon && !course?.isOneDayCourse ? <p>{tr`comingSoon`}</p> : ""}
							</RFlex>

							{course?.isOneDayCourse ? (
								<RFlex styleProps={{ padding: "0px 10px" }}>
									<p>{tr`You can complete this course in 1 day!`}</p>
								</RFlex>
							) : (
								""
							)}
							{course?.liveSession && (
								<React.Fragment>
									<RFlex styleProps={{ padding: "0px 10px" }}>
										<p className={styles.link}>
											5 Min left to start
											<span
												className={styles.join}
												onClick={() => {
													history.push(`${baseURL}/${genericPath}/`);
												}}
											>
												{tr`join`}
											</span>
										</p>
									</RFlex>
									<RFlex styleProps={{ padding: "0px 10px", flexWrap: "wrap" }}>
										<span className={styles.text}>Next Session : sat , 12:00 pm</span>
									</RFlex>
								</React.Fragment>
							)}

							{course?.currentInfo && course?.currentInfo.length > 0 && (
								<Row>
									{course.currentInfo.map((cI) => (
										<Col xs={6} className="d-flex mb-2" key={cI?.name ?? cI?.title}>
											{cI.icon && <i className={cI.icon + " pt-1 pr-2 pl-0"} />}
											<span>
												{cI?.count ?? cI?.value} {tr(cI?.name ?? cI?.title)}
											</span>
										</Col>
									))}
								</Row>
							)}

							{course.progress ? (
								<RFlex styleProps={{ padding: "0px 10px", alignItems: "center" }}>
									<Progress animated value={60} className={styles.progress_bar}>
										{course?.progress}%
									</Progress>
									<h6>you have completed {course?.Progress}%</h6>
								</RFlex>
							) : (
								""
							)}
							{course?.users && course?.users.length > 0 ? (
								<RFlex styleProps={{ flexDirection: "row", alignItems: "center" }}>
									<RFlex
										styleProps={{
											justifyContent: "space-between",
											alignItems: "center",
										}}
									>
										<div className={styles.imageDiv}>
											{course?.users.slice(0, 3).map((user, index) => {
												return (
													<div key={index}>
														<img
															src={user.image_hash_id == undefined ? UserAvatar : `${Services.storage.file}${user.image_hash_id}`}
															alt={user.image_hash_id}
															className={`${styles.userTypeImage} `}
															style={{
																// zIndex: index,
																right: index * 20,
																position: "relative",
															}}
														/>
													</div>
												);
											})}
										</div>
									</RFlex>
									<RFlex styleProps={{ flexWrap: "wrap" }}>
										{course?.users.slice(0, 1).map((user, index, array) => (
											<span key={index}>
												{user.user_name}
												{index !== array.length - 1 && ","}
											</span>
										))}
										<span>{course.users?.length > 2 && `+${course.users?.length - 1}more`}</span>
									</RFlex>
								</RFlex>
							) : (
								<span className={"pl-2 text-muted"}>{tr`no facilitators`}</span>
							)}
							<RFlex styleProps={{ padding: "10px 10px", alignItems: "center" }}>
								{course?.oldPrice && (
									<h6 className="text-muted" style={{ textDecorationLine: "line-through", fontSize: "12px" }}>
										{course?.oldPrice}$
									</h6>
								)}
								{course?.newPrice && <h6>{course?.newPrice}$</h6>}
								{course?.discount && course?.discount.length > 0 && <h6 className={styles.discount}>{course?.discount}</h6>}
								{course?.isNew && <h6 className={styles.new}>{tr`New`}</h6>}
								{course?.isFree ? <h6 className={styles.text_free}>{tr`Free`}</h6> : ""}
							</RFlex>
						</div>
					) : (
						<div style={{ marginLeft: "8px", marginRight: "8px" }}>
							{/* <div style={{		
								maxHeight:"65px",
								overflow: "hidden",
									}}
						 dangerouslySetInnerHTML={{ __html: course?.description }} /> */}

							<TruncatedDescription description={course?.description} maxHeight={42} />
							<RButton text="add to tempate" style={{ backgroundColor: "#668ad7", width: "100%" }} />
						</div>
					)}
				</section>
			</Card>
		</div>
	);
};

export default RCatalogCard;
