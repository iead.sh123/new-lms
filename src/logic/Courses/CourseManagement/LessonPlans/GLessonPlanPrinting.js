import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getBase64ToLessonPlan } from "store/actions/teacher/lessonPlan.actions";

import Loader from "utils/Loader";

const GLessonPlanPrinting = () => {
  const dispatch = useDispatch();
  const { lessonPlanBase64, lessonPlanPrintLoading } = useSelector(
    (state) => state.lessonPlan
  );

  useEffect(() => {
    dispatch(getBase64ToLessonPlan(30));
  }, []);

  return (
    <>
      {lessonPlanPrintLoading ? (
        <Loader />
      ) : (
        <div style={{ height: "80vh", background: "red" }}>
          <embed
            width="100%"
            height="100%"
            src={`data:application/pdf;base64,${lessonPlanBase64}`}
          />
        </div>
      )}
    </>
  );
};

export default GLessonPlanPrinting;
