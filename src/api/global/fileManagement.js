import { get, post, put, destroy } from "config/api";
import { Services } from "engine/services";

export const fileManagementApi = {
	// - - - - - - - - - - - - - - Shared Items - - - - - - - - - - - - - -

	sharedItems: (params) => get(`${Services.file_management.backend}api/file_management/shared-folders-files${params ? params : ""}`),

	// - - - - - - - - - - - - - - Favorite Items - - - - - - - - - - - - - -

	favoriteItems: (params) => get(`${Services.file_management.backend}api/file_management/folders-files/favorite${params ? params : ""}`),

	// - - - - - - - - - - - - - - Trashed Items - - - - - - - - - - - - - -

	trashedItems: (params) => get(`${Services.file_management.backend}api/file_management/recycle-bin${params ? params : ""}`),
	restoreItems: (data) => put(`${Services.file_management.backend}api/file_management/folders-files/un-delete`, data),

	// - - - - - - - - - - - - - - MyFiles - - - - - - - - - - - - - -

	rootFolders: (params) => get(`${Services.file_management.backend}api/file_management/folders${params ? params : ""}`),
	folderContent: (folderId, params) =>
		get(`${Services.file_management.backend}api/file_management/files/${folderId}${params ? params : ""}`),
	uploadFile: (data) => post(`${Services.file_management.backend}api/file_management/files`, data),
	fileById: (fileId, params) => get(`${Services.file_management.backend}api/file_management/files/${fileId}${params ? params : ""}`),

	fileType: (type) => get(`${Services.file_management.backend}api/file_management/filter-type?type=${type}`),
	createSubFolders: (data) => post(`${Services.file_management.backend}api/file_management/folders`, data),

	addItemsToFavoriteList: (data) => put(`${Services.file_management.backend}api/file_management/folders-files/favorite`, data),
	deleteItemsFromList: (data) => put(`${Services.file_management.backend}api/file_management/folders-files/delete`, data),

	moveItemsToAnotherFile: (data, destinationId) =>
		post(`${Services.file_management.backend}api/file_management/folders-files/${destinationId}/move`, data),

	copyItemsToAnotherItem: ({ folderId, data }) =>
		post(`${Services.file_management.backend}api/file_management/folders-files/${folderId}/copy`, data),

	// copyFolderToAnotherFolder: ({ folderId, destinationId }) =>
	// 	post(`${Services.file_management.backend}api/file_management/folders/${folderId}/copy`, { destination_folder: destinationId }),
	// copyFileToAnotherFolder: ({ fileId, destinationId }) =>
	// 	post(`${Services.file_management.backend}api/file_management/files/${fileId}/copy`, { destination_folder: destinationId }),

	updateFolder: ({ folderId, data }) => put(`${Services.file_management.backend}api/file_management/folders/${folderId}`, data),
	updateFile: ({ fileId, data }) => put(`${Services.file_management.backend}api/file_management/files/${fileId}`, data),

	folderDetails: (folderId) => get(`${Services.file_management.backend}api/file_management/folders/${folderId}/details`),
	fileDetails: (fileId) => get(`${Services.file_management.backend}api/file_management/files/${fileId}/details`),

	folderLogs: (folderId) => get(`${Services.file_management.backend}api/file_management/folders/${folderId}/logs`),
	fileLogs: (fileId) => get(`${Services.file_management.backend}api/file_management/files/${fileId}/logs`),

	folderAccess: (folderId) => get(`${Services.file_management.backend}api/file_management/folders/${folderId}/access`),
	fileAccess: (fileId) => get(`${Services.file_management.backend}api/file_management/files/${fileId}/access`),

	addAccessToFolder: ({ folderId, data }) =>
		post(`${Services.file_management.backend}api/file_management/user_folder_access/${folderId}`, data),
	addAccessToFile: ({ fileId, data }) => post(`${Services.file_management.backend}api/file_management/user_file_access/${fileId}`, data),

	downloadFile: (fileId) =>
		get(`${Services.file_management.backend}api/file_management/files/download/${fileId}`, { responseType: "blob" }),

	permissionList: () => get(`${Services.file_management.backend}api/file_management/permissions`),

	downloadFolderAvailable: ({ folderId, data }) =>
		put(`${Services.file_management.backend}api/file_management/folders/${folderId}/download-available`, data),
	downloadFileAvailable: ({ fileId, data }) =>
		put(`${Services.file_management.backend}api/file_management/files/${fileId}/download-available`, data),

	changeFolderStatus: ({ folderId, data }) => put(`${Services.file_management.backend}api/file_management/folders/${folderId}/link`, data),
	changeFileStatus: ({ fileId, data }) => put(`${Services.file_management.backend}api/file_management/files/${fileId}/link`, data),

	setPasswordToFolder: ({ folderId, data }) =>
		put(`${Services.file_management.backend}api/file_management/folders/${folderId}/link/password`, data),
	setPasswordToFile: ({ fileId, data }) =>
		put(`${Services.file_management.backend}api/file_management/files/${fileId}/link/password`, data),

	deletePasswordFromFolder: ({ folderId, data }) =>
		post(`${Services.file_management.backend}api/file_management/folders/${folderId}/link/password`, data),
	deletePasswordFromFile: ({ fileId, data }) =>
		post(`${Services.file_management.backend}api/file_management/files/${fileId}/link/password`, data),

	usersToShared: () => get(`${Services.file_management.backend}api/file_management/users`),

	breadCrumbing: (folderId) => get(`${Services.file_management.backend}api/file_management/breadcrumbing/${folderId}`),
};
