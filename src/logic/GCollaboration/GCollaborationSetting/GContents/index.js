import React from "react";
import RContents from "view/Collaboration/CollaborationSetting/RContents";
import Loader from "utils/Loader";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useFormik } from "formik";

const GContents = () => {
	// ------------------ Start Formik ------------------
	const initialStates = {
		contentShareableIds: {},
	};

	const { values, errors, touched, setFieldValue, setFieldError, handleChange, handleBlur } = useFormik({
		initialValues: initialStates,
	});
	// ------------------ End Formik ------------------

	// ------------------ Start Queries ------------------
	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["contents"],
		queryFn: () => collaborationApi.getContent(),
		onSuccessFn: ({ data }) => {
			const sharableStatusData = {};

			data?.data?.data?.forEach((item) => {
				if (item?.sharable_status) {
					sharableStatusData[item.id] = {
						content_type_id: item?.sharable_status.pivot.sharable_content_id,
						need_approval: item?.sharable_status.pivot.need_approval,
					};
				}
			});
			setFieldValue("contentShareableIds", sharableStatusData);
		},
	});

	const shareableContentMutate = useMutateData({
		queryFn: (data) => collaborationApi.shareableContent(data),
		invalidateKeys: ["contents"],
	});

	const approvalToContentMutate = useMutateData({
		queryFn: (data) => collaborationApi.approvalToContent(data),
		invalidateKeys: ["contents"],
	});
	// ------------------ End Queries ------------------

	// ------------------ Handlers ------------------
	const handleMutateShareableContent = ({ contentId }) => {
		shareableContentMutate.mutate({ sharable_content_ids: Object?.keys(values.contentShareableIds)?.map(Number).concat([+contentId]) });
	};

	const handleApprovalContent = ({ contentId }) => {
		approvalToContentMutate.mutate(values.contentShareableIds[contentId]);
	};

	const handleMakingContentShareable = ({ contentId }) => {
		const updatedArray = { ...values.contentShareableIds };

		if (!updatedArray[contentId]) {
			// If contentId doesn't exist, add it to the object with default values
			updatedArray[contentId] = { content_type_id: contentId, need_approval: true };
		} else {
			// If contentId already exists, toggle need_approval value
			updatedArray[contentId].need_approval = !updatedArray[contentId].need_approval;
		}

		setFieldValue("contentShareableIds", updatedArray);
	};

	if (isLoading) return <Loader />;

	return (
		<RContents
			data={{ content: data?.data?.data?.data, values: values }}
			handlers={{
				handleMakingContentShareable: handleMakingContentShareable,
				handleMutateShareableContent: handleMutateShareableContent,
				handleApprovalContent: handleApprovalContent,
			}}
		/>
	);
};

export default GContents;
