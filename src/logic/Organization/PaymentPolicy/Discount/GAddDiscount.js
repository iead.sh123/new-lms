import React, { useContext } from "react";
import { Input, FormText, FormGroup } from "reactstrap";
import { primaryColor, successColor } from "config/constants";
import { boldGreyColor, lightGray } from "config/constants";
import { PaymentPolicyContext } from "../GPaymentPolicy";
import { useFormik } from "formik";
import useFormattedDate from "utils/useFormattedDate";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "../../Organization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

const GAddDiscount = ({ handleCloseDiscountModal }) => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	const initialValues = {
		value: null,
		applyType: "",
		startDate: "",
		expireDate: "",
		wholeOrganization: false,
	};

	const formSchema = yup.object().shape({
		value: yup.number().required(tr`value is required`),
		// startDate: yup
		// 	.string()
		// 	.required("Start date is required")
		// 	.test({
		// 		name: "start-date-validation",
		// 		message: "Start date must be before or equal to end date and should not be earlier than today's date",
		// 		test: function (startDate) {
		// 			const currentDate = new Date();
		// 			const selectedDate = new Date(startDate);
		// 			return selectedDate >= currentDate;
		// 		},
		// 	}),
		// expireDate: yup
		// 	.string()
		// 	.required("expire date is required")
		// 	.test({
		// 		name: "expire-date-validation",
		// 		message: "expire date must be after or equal to start date and should not be earlier than today's date",
		// 		test: function (expireDate) {
		// 			const currentDate = new Date();
		// 			const selectedDate = new Date(expireDate);
		// 			return selectedDate >= currentDate;
		// 		},
		// 	}),
		startDate: yup
			.string()
			.required("Start date is required")
			.test({
				name: "start-date-validation",
				message: "Start date should not be after end date or after today's date",
				test: function (startDate) {
					const expireDate = this.parent.expireDate;
					const currentDate = new Date();

					if (!startDate) return true;

					const selectedStartDate = new Date(startDate);
					const selectedEndDate = new Date(expireDate);

					return selectedStartDate <= selectedEndDate && selectedStartDate >= currentDate;
				},
			}),
		expireDate: yup
			.string()
			.required("End date is required")
			.test({
				name: "end-date-validation",
				message: "End date should not be before start date or after today's date",
				test: function (expireDate) {
					const startDate = this.parent.startDate;
					const currentDate = new Date();

					if (!expireDate) return true;

					const selectedStartDate = new Date(startDate);
					const selectedEndDate = new Date(expireDate);

					return selectedEndDate >= selectedStartDate && selectedEndDate >= currentDate;
				},
			}),
	});

	const handleFormSubmit = (values, resetForm) => {
		PaymentPolicyData.handleAddDiscount(values.applyType !== "" ? true : false, {
			...values,
			startDate: useFormattedDate(values.startDate).formattedDateForBackend(),
			expireDate: useFormattedDate(values.expireDate).formattedDateForBackend(),
		});
		resetForm();
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};

	const handleOpenWholeOrganization = (check) => {
		if (check == false) {
			setFieldValue("wholeOrganization", false);
			setFieldValue("applyType", "");
		} else {
			setFieldValue("wholeOrganization", true);
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", paddingTop: "10px" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<div className={styles.icon}>
						<i className={iconsFa6.percentage + " fa-lg"} style={{ color: "white", rotate: "90deg" }} />
					</div>
					<span>{tr`add_new_discount`}</span>
				</RFlex>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<span style={{ color: successColor }}>{tr`this discount can be used since you share it with others`}</span>
					<RFlex styleProps={{ flexDirection: "column" }}>
						<RFlex styleProps={{ alignItems: "center" }}>
							<span>{tr`discount_amount`}</span>
							<Input
								name="value"
								type="number"
								style={{ width: "75px" }}
								value={values.value}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "value")}
							/>
							<span style={{ color: lightGray, fontSize: "16px" }}>%</span>
						</RFlex>
						{touched.value && errors.value && <FormText color="danger">{errors.value}</FormText>}
					</RFlex>
					<RFlex>
						<FormGroup style={{ width: "50%" }}>
							<label>{tr("start_at")}</label>
							<Input
								name="startDate"
								type="date"
								value={useFormattedDate(values.startDate).formattedDateForDisplay()}
								placeholder={tr("start_at")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "startDate")}
							/>
							{touched.startDate && errors.startDate && <FormText color="danger">{errors.startDate}</FormText>}
						</FormGroup>

						<FormGroup style={{ width: "50%" }}>
							<label>{tr("end_at")}</label>
							<Input
								name="expireDate"
								type="date"
								value={useFormattedDate(values.expireDate).formattedDateForDisplay()}
								placeholder={tr("end_at")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "expireDate")}
							/>
							{touched.expireDate && errors.expireDate && <FormText color="danger">{errors.expireDate}</FormText>}
						</FormGroup>
					</RFlex>

					{PaymentPolicyData?.checkIfIHaveGeneralDiscount ? (
						<RFlex styleProps={{ flexDirection: "column" }}>
							<span style={{ color: boldGreyColor }}>{tr`organization-level discount is already active`}</span>
							<span>
								<span
									style={{ color: lightGray }}
								>{tr`only one organization-level discount can be applied, you can deactivate it in`}</span>
								&nbsp;
								<span style={{ color: primaryColor }}>{PaymentPolicyData?.generalDiscountData?.value}%</span>
							</span>
						</RFlex>
					) : (
						<RFlex styleProps={{ flexDirection: "column" }}>
							<AppNewCheckbox
								onChange={(e) => handleOpenWholeOrganization(e.target.checked)}
								label={tr`the whole organization`}
								checked={values.wholeOrganization}
								paragraphStyle={{ color: boldGreyColor }}
								// disabled={PaymentPolicyData?.checkIfIHaveGeneralDiscount || false}
							/>
							<label style={{ color: lightGray }}>{tr("once you choose this you can’t change it")}</label>
							{values.wholeOrganization && (
								<RFlex>
									<span style={{ color: boldGreyColor }}>{tr`how_does_this_discount_work`}?</span>
									<RFlex styleProps={{ flexDirection: "column" }}>
										<AppRadioButton
											name={"applyType"}
											onChange={(e) => setFieldValue("applyType", "OnlyGeneral")}
											label={tr`replace_with_other_discounts`}
											checked={values.applyType == "OnlyGeneral" ? true : false}
											label__class__name={styles.radio__label}
										/>
										<AppRadioButton
											name={"applyType"}
											onChange={(e) => setFieldValue("applyType", "Both")}
											label={tr`apply_both`}
											checked={values.applyType == "Both" ? true : false}
											label__class__name={styles.radio__label}
										/>
										<AppRadioButton
											name={"applyType"}
											onChange={(e) => setFieldValue("applyType", "MaxValue")}
											label={tr`apply_the_bigger_amount`}
											checked={values.applyType == "MaxValue" ? true : false}
											label__class__name={styles.radio__label}
										/>
									</RFlex>
								</RFlex>
							)}
						</RFlex>
					)}
				</RFlex>

				{/* actions */}
				<RFlex>
					<RButton
						type="submit"
						text={tr`create`}
						color="primary"
						loading={PaymentPolicyData.addDiscountLoading}
						disabled={PaymentPolicyData.addDiscountLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						style={{ color: primaryColor }}
						onClick={() => {
							resetForm();
							handleCloseDiscountModal();
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default GAddDiscount;
