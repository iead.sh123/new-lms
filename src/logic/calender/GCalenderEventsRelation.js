import { Button, Container } from 'reactstrap';
import React from 'react';
import EventType from './EventType';
import RButton from 'components/Global/RComs/RButton';
import tr from 'components/Global/RComs/RTranslator';

const GCalenderEventsRelation = ({ eventTypes, handleBlock, handleUnBlock, goBack , handleSave }) => {
    return <Container className="content" fluid>
        {
            eventTypes.ids.map(etId => <EventType
                id={etId}
                key={'eventTypeBlockEditor' + etId} handleBlock={(blockedId) => handleBlock(etId, blockedId)}
                handleUnBlock={(blockedId) => handleUnBlock(etId, blockedId)} name={eventTypes.byId[etId].name} blocks={eventTypes.byId[etId].blocks} allTypes={eventTypes} />)
        }

        <RButton onClick={handleSave} text={tr`save`} color='primary'/>
        <RButton onClick={goBack} text={tr`back`} color='danger' outline/>
    </Container>
}

export default GCalenderEventsRelation;