import React from "react";
import RDiscountCourses from "../../RDiscountCourses";
import styles from "../../../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Loader from "utils/Loader";

const RListCourses = ({ coursesData, loading, formProperties, handlers }) => {
	if (loading.coursesLoading) return <Loader />;
	return (
		<RFlex className={styles.course__lister__scroll + " scroll_hidden mt-2"} styleProps={{ flexWrap: "wrap" }}>
			{coursesData?.courses.map((course) => {
				return <RDiscountCourses key={course.id} course={course} states={formProperties} handlers={handlers} />;
			})}
		</RFlex>
	);
};

export default RListCourses;
