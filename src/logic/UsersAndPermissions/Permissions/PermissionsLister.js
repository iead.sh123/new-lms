import React, { useEffect, useReducer, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { getPermissionsByCategoryAsync } from "../State/UsersAndPermissions.action";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import Loader from "utils/Loader";
import styles from "../GUsersAndPermissions.module.scss";
import { Row, Col } from "reactstrap";
import RPermissions from "view/UsersAndPermissions/RPermissions";
import tr from "components/Global/RComs/RTranslator";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
 import { baseURL, genericPath } from "engine/config";
import REmptyData from "components/RComponents/REmptyData";

const PermissionsLister = () => {
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	const history = useHistory();
	const { categoryId } = useParams();
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		getPermissionsByCategoryAsync(dispatch, categoryId, emptySearch ?? searchData);
	};

	useEffect(() => {
		getPermissionsByCategoryAsync(dispatch, categoryId);
	}, [categoryId]);

	const handleRedirect = (permissionId) => {
		history.push(`${baseURL}/${genericPath}/users-and-permissions/permission/${permissionId}/category/${categoryId}/properties`);
	};

	return (
		<React.Fragment>
			{usersAndPermissionsData.permissionsByCategoryLoading ? (
				<Loader />
			) : (
				<section className={"p-4 " + styles.box}>
					<RSearchHeader
						searchLoading={searchLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						inputPlaceholder={tr`search_a_permission`}
						addNew={false}
					/>
					<Row className="mt-4">
						{usersAndPermissionsData.permissionsByCategory.length == 0 ? (
							<Col>
								<REmptyData />
							</Col>
						) : (
							usersAndPermissionsData.permissionsByCategory.map((permission) => (
								<RPermissions permission={permission} redirect={true} handleRedirect={handleRedirect} />
							))
						)}
					</Row>
				</section>
			)}
		</React.Fragment>
	);
};

export default PermissionsLister;
