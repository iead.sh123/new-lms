import React, { useState } from "react";
import { Collapse } from "reactstrap";
import styles from "../RUnitPlan.module.scss";

const RUnitplanItems = ({
  items,
  searchText,
  handleJumpToItemInEditor,
  selectedItems,
  lvl = 1,
}) => {
  return (
    <>
      {items.map((item) => (
        <TreeItem
          key={item?.id ? item?.id : item?.fakeId}
          item={item}
          lvl={lvl}
          searchText={searchText}
          handleJumpToItemInEditor={handleJumpToItemInEditor}
          selectedItems={selectedItems}
        />
      ))}
    </>
  );
};

const TreeItem = ({
  key,
  item,
  lvl,
  searchText,
  handleJumpToItemInEditor,
  selectedItems,
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div key={key} style={{ paddingLeft: 10 * lvl }}>
      <div
        className={`${styles.tree__item} `}
        onClick={() => {
          toggleCollapse();
          handleJumpToItemInEditor(item?.id ? item?.id : item?.fakeId);
        }}
      >
        <i
          className={
            isOpen
              ? "fa-solid fa-chevron-down pt-1"
              : "fa-solid fa-chevron-right pt-1"
          }
        />

        <i
          className={`fa-solid fa-file pt-1 ${
            selectedItems.includes(item.id ? item.id : item.fakeId)
              ? styles.matched__item
              : styles.do__not__matched__item
          }`}
        />
        <p
          className={`${
            selectedItems.includes(item.id ? item.id : item.fakeId)
              ? styles.matched__item
              : styles.do__not__matched__item
          }`}
        >
          {item.text}
        </p>
      </div>
      <Collapse isOpen={isOpen}>
        {item?.items && item?.items?.length > 0 && (
          <RUnitplanItems
            items={item.items}
            searchText={searchText}
            handleJumpToItemInEditor={handleJumpToItemInEditor}
            selectedItems={selectedItems}
            lvl={lvl}
          />
        )}
      </Collapse>
    </div>
  );
};

export default RUnitplanItems;
