import React, { useContext } from "react";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import GDiscountDetails from "logic/Organization/PaymentPolicy/Discount/GDiscountDetails";
import RDiscountLister from "./Discount/RDiscountLister";
import GCouponDetails from "logic/Organization/PaymentPolicy/Coupon/GCouponDetails";
import RCouponLister from "./Coupon/RCouponLister";
import RDiscountList from "./Discount/RDiscountList";
import RCouponList from "./Coupon/RCouponList";
import RSearchArea from "./RSearchArea";
import RFilterArea from "./RFilterArea";
import styles from "../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RPaymentPolicy = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	return (
		<RFlex className={styles.organization__container}>
			<RFlex className={styles.organization__left__side}>
				<RSearchArea />
				<RFilterArea />
				{PaymentPolicyData.values.itemNameValue == "coupons" ? (
					PaymentPolicyData.values.switchMode ? (
						<RCouponLister />
					) : (
						<RCouponList />
					)
				) : PaymentPolicyData.values.switchMode ? (
					<RDiscountLister />
				) : (
					<RDiscountList />
				)}
			</RFlex>

			<RFlex className={styles.organization__right__side}>
				{PaymentPolicyData.values.itemNameValue == "coupons" && PaymentPolicyData.values.couponIdSelected ? (
					<GCouponDetails
						couponId={+PaymentPolicyData.values.couponIdSelected}
						couponDetails={PaymentPolicyData.values.couponOrDiscountDetails}
						handleEmptyRightSideAfterDeActivate={PaymentPolicyData.handleEmptyRightSideAfterDeActivate}
					/>
				) : PaymentPolicyData.values.itemNameValue == "discounts" && PaymentPolicyData.values.discountIdSelected ? (
					<GDiscountDetails
						discountId={PaymentPolicyData.values.discountIdSelected}
						discountDetails={PaymentPolicyData.values.couponOrDiscountDetails}
						handleEmptyRightSideAfterDeActivate={PaymentPolicyData.handleEmptyRightSideAfterDeActivate}
						generalDiscountData={PaymentPolicyData.generalDiscountData}
					/>
				) : (
					""
				)}
			</RFlex>
		</RFlex>
	);
};

export default RPaymentPolicy;
