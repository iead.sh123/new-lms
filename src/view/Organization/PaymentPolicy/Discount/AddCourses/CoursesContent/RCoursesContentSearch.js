import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../../../../ROrganization.module.scss";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";

const RCoursesContentSearch = ({ creatorsData, formProperties, loading, handlers }) => {
	return (
		<RFlex styleProps={{ flexWrap: "wrap", justifyContent: "space-between", alignItems: "center" }}>
			<RFlex>
				<RSearchHeader
					widthInput="213px"
					searchLoading={loading.coursesLoading}
					searchData={formProperties.values.search}
					handleSearch={handlers.handleSearch}
					setSearchData={(value) => formProperties.setFieldValue("search", value)}
					handleChangeSearch={(value) => formProperties.setFieldValue("search", value)}
				/>
				<div style={{}}>
					<RSelect
						className={styles.select__creators}
						closeMenuOnSelect={true}
						option={[{ label: `--${tr`no_creator`}--`, value: undefined }].concat(creatorsData)}
						isLoading={loading.creatorsLoading}
						onChange={(e) => formProperties.setFieldValue("creatorValue", e)}
						value={[formProperties.values.creatorValue]}
						isDisabled={loading.creatorsLoading}
					/>
				</div>
			</RFlex>
			<RButton
				text={tr`save`}
				color="link"
				classNameText={styles.save__courses}
				disabled={formProperties.values.allCourses?.length == 0 ? true : false || loading.applyDiscountToProductsLoading}
				onClick={handlers.handleApplyCourses}
				loading={loading.applyDiscountToProductsLoading}
			/>
			{/* <span className={styles.save__courses}>{tr`save`}</span> */}
		</RFlex>
	);
};

export default RCoursesContentSearch;
