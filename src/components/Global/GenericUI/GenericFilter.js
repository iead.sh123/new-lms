import React, { useEffect, useState } from "react";
import { Button, UncontrolledTooltip, Row, Col } from "reactstrap";
import RButton from "../RComs/RButton";
import tr from "../RComs/RTranslator";
import RSelect from "../RComs/RSelect";
import Helper from "../RComs/Helper";
import {
	ADD_NEW_FILTER,
	DELETE_NEW_FILTER,
	ADD_VALUES_TO_THE_FILTER,
	ADD_NAME_TO_THE_FILTER,
	RESET_FILTERS,
} from "store/actions/admin/adminType";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import iconsFa6 from "variables/iconsFa6";

const GenericFilter = ({ table_props, getData, localnewfilter, filterdispatch }) => {
	const [fields, setFields] = useState([]);
	const [optionsNameSelect, setOptionsNameSelect] = useState([]);
	//const { newFilter } = useSelector((state) => state.genericFilterRed);
	//Set Data Field In Field State
	useEffect(() => {
		let temp = [];
		for (let i = 0; i < table_props.length; i++) {
			if (table_props[i]?.Type?.indexOf("varchar") !== -1) {
				temp.push({
					field: table_props[i].Field,
					title: table_props[i].Title,
					type: "text",
					operation: "like",
				});
			}
			if (table_props[i].IsDropDownList) {
				temp.push({
					field: table_props[i].Field,
					title: table_props[i].Title,
					type: "dropdown",
					data: table_props[i].data,
					operation: "=",
				});
			}
		}
		setFields(temp);
	}, [table_props]);

	//HandleSearch
	const handleSearch = () => {
		getData();
	};

	//Add New Filter
	const handleAddNewFilter = (e) => {
		e.preventDefault();
		const guid = Math.floor(1000 + Math.random() * 9000);
		filterdispatch({
			type: ADD_NEW_FILTER,
			data: {
				idTemp: guid,
				field: "",
				value: "",
				operation: "",
				type: "text",
				index: null,
			},
		});
	};

	//Add Value to Filter
	const handleAddFilterValue = (id, inputValue, operation) => {
		const newData = {
			operation: operation,
			value: inputValue,
		};
		// filterdispatch(addValuesToTheFilter(newData, id));

		filterdispatch({
			type: ADD_VALUES_TO_THE_FILTER,
			inputValue: newData,
			id: id,
		});
	};

	//Add Name to Filter
	const handleAddNameToTheFilter = (id, fieldName, fieldType, fieldIndex, fieldOperation) => {
		const nameFilter = {
			field: fieldName,
			type: fieldType,
			index: fieldIndex,
			operation: fieldOperation,
		};
		//filterdispatch(addNameToTheFilter(nameFilter, id));
		filterdispatch({
			type: ADD_NAME_TO_THE_FILTER,
			nameFilter: nameFilter,
			id: id,
		});
	};

	//Delete Filter
	const handleDeleteFilterInput = (e, TempId) => {
		e.preventDefault();
		//filterdispatch(deleteNewFilter(TempId));
		filterdispatch({
			type: DELETE_NEW_FILTER,
			id: TempId,
		});
		// setTimeout(() => {
		//   if (newFilter.length == 1) getData();
		// }, 1000);
	};

	//Option Select Name Input
	const getOptionsNameInput = () => {
		const arrayDataNameInput = [];
		fields &&
			fields.map((field, index) => {
				arrayDataNameInput.push({
					value: field.field,
					label: field.title,
					type: field.type,
					index: index,
					operation: field.operation,
				});
			});

		setOptionsNameSelect(arrayDataNameInput);
	};

	useEffect(
		() =>
			//filterdispatch(resetFilters())
			filterdispatch({ type: RESET_FILTERS, payload: null }),
		[]
	);

	//Set Select Name Input
	useEffect(() => {
		getOptionsNameInput();
	}, [fields]);

	return (
		<>
			<div className="form-search">
				<div className="container">
					<div className="row">
						<div
							className="col-md-12"
							style={{
								display: "flex",
								justifyContent: "flex-end",
							}}
						>
							<div style={{ cursor: "pointer" }}>
								<FontAwesomeIcon
									icon={faFilter}
									onClick={(e) => handleAddNewFilter(e)}
									// className={styles.filter_icon}
									id="filter"
									style={{
										color: "#fd832c",
										fontSize: "20px",
									}}
								/>
								<UncontrolledTooltip delay={0} target={"filter"}>{tr`filter`}</UncontrolledTooltip>
							</div>
						</div>
					</div>
					{/* localnewfilter:{JSON.stringify(localnewfilter)} */}
					<Row>
						{localnewfilter?.newFilter?.map((el) => {
							const df = fields && fields.length > 0 && fields[el.index] && fields[el.index].data ? fields[el.index]?.data[0] : null;

							return (
								<>
									<Col md={5} sm={6} xs={12}>
										{/* <div style={{color:"red"}}>
                      index={JSON.stringify(el.index)}<br/>
                      df of select={df} el.value={el.value}<br/>
                      fields[el.index]{JSON.stringify(fields[el.index])}<br/>
                      </div> */}
										<RSelect
											option={optionsNameSelect}
											closeMenuOnSelect={true}
											placeholder={"Select Type"}
											defaultValue={el?.field}
											//value={el?.field}
											onChange={(e) => {
												handleAddNameToTheFilter(el.idTemp, e.value, e.type, e.index, e.operation);
											}}
										/>
									</Col>
									<Col md={5} sm={6} xs={12}>
										<div className="form-group">
											{el?.type == "text" ? (
												<input
													className="form-control"
													type={"text"}
													style={{ padding: 10 }}
													placeholder="Value"
													defaultValue={el.value}
													//value={el.value}
													onBlur={(e) => handleAddFilterValue(el.idTemp, e.target.value, "like")}
												/>
											) : (
												<select
													className="form-control"
													style={{ padding: 0 }}
													defaultValue={df}
													onBlur={(e) => handleAddFilterValue(el.idTemp, e.target.value, "=")}
												>
													{fields && fields.length > 0 && fields[el.index] && fields[el.index].data
														? fields[el.index].data?.map((field) => {
																return (
																	<option id={field.ID} key={field.ID} value={field.ID}>
																		{field.name}
																	</option>
																);
														  })
														: null}
												</select>
											)}
										</div>
									</Col>
									<Col md={2} sm={6} xs={12}>
										<>
											<Button
												className="btn-icon"
												color={"danger"}
												key={"ra" + el?.idTemp}
												id={"index" + el?.idTemp}
												size="sm"
												type="button"
												onClick={(e) => handleDeleteFilterInput(e, el?.idTemp)}
												style={{ top: "-5px" }}
											>
												<i className={iconsFa6.delete} />
											</Button>
											<UncontrolledTooltip delay={0} key={"rat" + el?.idTemp + "delete"} target={"index" + el?.idTemp}>
												{tr`delete`}
											</UncontrolledTooltip>
										</>
									</Col>
								</>
							);
						})}
						{/* 
            <div className="col-md-4 col-sm-6 col-xs-12">
              {localnewfilter.newFilter.length > 0 && (
                <div className="form-group">
                  <Button
                    onClick={() => {
                      handleSearch();
                    }}
                    color="primary"
                  >
                    {tr`search`}
                  </Button>
                </div>
              )}
            </div> */}
					</Row>
				</div>
			</div>
		</>
	);
};
export default GenericFilter;
