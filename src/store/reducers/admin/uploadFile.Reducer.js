import {
  UPLOAD_FILE_REQUEST,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR,
  IMPORT_FROM_FILE_REQUEST,
  IMPORT_FROM_FILE_SUCCESS,
  IMPORT_FROM_FILE_ERROR,
  INITIAL_RESPONSE,
  MAPPING_DATA,
} from "store/actions/admin/adminType";

import produce from "immer";

const InitState = {
  data: {},
  loadingData: false,
  loading: false,
  initRes: [],
  successSave: null,
};

let ind;
const UploadFileReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case UPLOAD_FILE_REQUEST:
        draft.loadingData = true;
        return draft;

      case UPLOAD_FILE_SUCCESS:
        draft.data = action.payload.AllTableData;
        draft.loadingData = false;
        return draft;

      case UPLOAD_FILE_ERROR:
        draft.loadingData = false;
        return draft;

      case IMPORT_FROM_FILE_REQUEST:
        draft.loading = true;
        return draft;

      case IMPORT_FROM_FILE_SUCCESS:
        draft.loading = false;
        draft.successSave = action.payload;
        return draft;

      case IMPORT_FROM_FILE_ERROR:
        draft.loading = false;
        return draft;

      case INITIAL_RESPONSE:
        draft.initRes = action.data;
        return draft;

      case MAPPING_DATA:
        ind = draft.initRes.findIndex((el) => el.id == action.payload.Id);
        if (ind !== -1) {
          draft.initRes[ind].table_field = action.payload.Value;
        }
        return draft;
    }
  });

export default UploadFileReducer;
