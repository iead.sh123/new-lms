import React,{useState} from "react";
import "./accordion.css";

import { Collapse, Button } from "reactstrap"
function RCollaborationAccordion({title,content}){

    const [collapse, setCollapse] = useState(false);
    const toggle = () => setCollapse(!collapse);
    return (
            <div className="accordion" >
                   <div onClick={toggle} className="accordion_title" >
                      <div className="accordion_text">  {title.text} </div>  
                      <div className="accordion_description"> {title.description} </div> 
                      <div  className="accordion_angle_icon"> <i className= {collapse==false?"fa fa-angle-right":"fa fa-angle-down"}/> </div> 
                   </div>

                     <Collapse isOpen={collapse}>
                           <div  className="accordion_collapse">
                               {content}
                           </div>      
                     </Collapse>
            </div>

)
}
export default RCollaborationAccordion