import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Row, Col, Progress, Collapse } from "reactstrap";
import { primaryColor, warningColor } from "config/constants";
import SolveMultipleChoiceSingleAnswer from "./QuestionTypesSolve/SolveMultipleChoiceSingleAnswer";
import SolveMultipleChoice from "./QuestionTypesSolve/SolveMultipleChoice";
import SolveUploadFile from "./QuestionTypesSolve/SolveUploadFile";
import SolveEssay from "./QuestionTypesSolve/SolveEssay";
import SolveMatch from "./QuestionTypesSolve/SolveMatch";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import SolveDraw from "./QuestionTypesSolve/SolveDraw";
import styles from "./SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import SolveTrueOrFalse from "./QuestionTypesSolve/SolveTrueOrFalse";
import SolveOrdering from "./QuestionTypesSolve/SolveOrdering";
import SolveFillInBlank from "./QuestionTypesSolve/SolveFillInBlank";
import RQuestionSetNavigation from "./RQuestionSetNavigation";
import { Services } from "engine/services";
import TeacherFeedbackToAllQs from "./QuestionTypesSolve/feedbacks/TeacherFeedbackToAllQs";
import RQuestionSetTitle from "./RQuestionSetTitle";
import CounterDecrease from "utils/CounterDecrease";
const RSolveQuestionSet = () => {
	const QuestionSetSolveData = useContext(QuestionSetSolveContext);

	const questionTypeSolveComponents = {
		MULTIPLE_CHOICES_MULTIPLE_ANSWERS: SolveMultipleChoice,
		MULTIPLE_CHOICES_SINGLE_ANSWER: SolveMultipleChoiceSingleAnswer,
		ESSAY: SolveEssay,
		FILL_IN: SolveFillInBlank,
		UPLOAD_FILE: SolveUploadFile,
		TRUEFALSE: SolveTrueOrFalse,
		SEQUENCING: SolveOrdering,
		DRAW: SolveDraw,
		MATCH: SolveMatch,
	};

	return (
		<>
			<Row>
				<Col xs={12} sm={2} className={styles.all__questions}>
					{QuestionSetSolveData.submitted == "solving" && QuestionSetSolveData?.doNotContainBehaviour && (
						<>
							<span style={{ color: primaryColor }}>
								{Math.round(QuestionSetSolveData.percentageOfSolvingQuestions)}% {tr`was_solved`}
							</span>
							<Progress value={QuestionSetSolveData.percentageOfSolvingQuestions} className="mb-3" />
						</>
					)}

					{QuestionSetSolveData.submitted == "solving" &&
						["quizzes", "exams"].includes(QuestionSetSolveData.questionSetType) &&
						!QuestionSetSolveData?.doNotContainBehaviour && (
							<>
								{QuestionSetSolveData.showTime ? (
									<>
										<Row>
											<Col xs={5} className="p-0" style={{ height: "20px" }}>
												<span> {tr`time_left`}</span> &nbsp;
											</Col>
											<Col xs={7} className="m-0" style={{ height: "20px" }}>
												<CounterDecrease isRunning={true} time={QuestionSetSolveData.time} setTime={QuestionSetSolveData.setTime} />
											</Col>
										</Row>
										<Row
											style={{
												width: "100%",
												justifyContent: "start",
												alignItems: "center",
											}}
										>
											<Col xs={9} className="p-0">
												<Progress value={10} style={{ color: primaryColor }} />
											</Col>
											<Col xs={1}>
												<i
													className="fa-solid fa-eye-slash cursor-pointer"
													style={{ color: primaryColor }}
													onClick={QuestionSetSolveData.handleShowTime}
												/>
											</Col>
										</Row>
									</>
								) : (
									<Row>
										<Col xs={12} className="p-0 " style={{ color: primaryColor }}>
											{tr`show_time`}
											<i
												className="fa-solid fa-eye pl-1 cursor-pointer"
												style={{ color: primaryColor }}
												onClick={QuestionSetSolveData.handleShowTime}
											/>{" "}
										</Col>
									</Row>
								)}
							</>
						)}

					{QuestionSetSolveData.submitted == "correction" && (
						<RFlex styleProps={{ color: primaryColor, cursor: "pointer" }} onClick={() => QuestionSetSolveData.history.goBack()}>
							<i className="fa-solid fa-arrow-left mt-1"></i> <span>{tr`back_to_all_students`}</span>
						</RFlex>
					)}
					{QuestionSetSolveData.submitted == "review" && (
						<RFlex styleProps={{ color: primaryColor }}>
							<span>
								{tr`your_score_is`}{" "}
								{(QuestionSetSolveData.questionsToSolve.teacherMark * QuestionSetSolveData.questionsToSolve?.questionset?.points) / 100}%
							</span>
						</RFlex>
					)}
					{QuestionSetSolveData?.questionsToSolve &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups?.length > 0 &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups?.map((questionGroup, ind) => (
							<article key={questionGroup.id}>
								{questionGroup?.isGroup ? (
									<section key={questionGroup.id}>
										<RFlex>
											<AppRadioButton
												name={`questionGroup_${questionGroup.id}`}
												disabled={true}
												checked={questionGroup.questions.every((el) => el.isSolved)}
											/>
											<p className="pt-2 cursor-pointer">{questionGroup.title}</p>
											<i
												onClick={() => QuestionSetSolveData.collapsesQuestionsGroupsToggle(questionGroup.id)}
												style={{ color: primaryColor, paddingTop: "12px" }}
												className={
													QuestionSetSolveData.openQuestionGroupCollapse == "collapse" + questionGroup.id
														? "cursor-pointer fa-regular fa-square-minus"
														: "cursor-pointer fa-regular fa-square-plus"
												}
											></i>
										</RFlex>
										<Collapse isOpen={QuestionSetSolveData.openQuestionGroupCollapse == "collapse" + questionGroup.id}>
											<RQuestionSetTitle isGroup={questionGroup?.isGroup} groupInd={ind} questionGroup={questionGroup} />
										</Collapse>
									</section>
								) : (
									<RQuestionSetTitle isGroup={questionGroup?.isGroup} groupInd={ind} questionGroup={questionGroup} />
								)}
							</article>
						))}
				</Col>
				<Col xs={12} sm={10}>
					{QuestionSetSolveData.teacherInCorrectionAndReviewMode && (
						<RFlex
							styleProps={{
								justifyContent: "space-between",
								alignItems: "center",
							}}
							className="mb-3"
						>
							<RFlex styleProps={{ alignItems: "center" }}>
								<img
									src={`${Services.storage.file}${QuestionSetSolveData?.questionsToSolve?.learner?.image}`}
									className={styles.learner__image}
								/>
								<RFlex styleProps={{ flexDirection: "column" }}>
									<h6 className="p-0 m-0">{QuestionSetSolveData?.questionsToSolve?.learner?.full_name}</h6>
									<RFlex>
										<h6 className="text-muted">{tr`submitted`}: </h6>
										<h6>{QuestionSetSolveData?.questionsToSolve?.submitDate}</h6>
									</RFlex>
								</RFlex>
							</RFlex>
							<span style={{ color: primaryColor }}>
								{tr`total_mark`}:&nbsp;
								<span>
									{QuestionSetSolveData.finalMarkToStudent}/{QuestionSetSolveData?.questionsToSolve?.questionset?.points}
								</span>
							</span>
						</RFlex>
					)}

					{QuestionSetSolveData?.questionsToSolve &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups?.length > 0 &&
						QuestionSetSolveData?.questionsToSolve?.questionGroups?.map((questionGroup, ind) => {
							return (
								<div
									key={ind}
									style={{
										display: QuestionSetSolveData.step === ind ? "block" : "none",
										paddingTop: "25px",
									}}
								>
									{questionGroup.isGroup && questionGroup?.requiredQuestionsToAnswer > 0 && QuestionSetSolveData.submitted == "solving" ? (
										<span style={{ color: warningColor }}>{tr`Choose any 3 questions to answer`}</span>
									) : (
										""
									)}
									{questionGroup?.questions?.map((question, questionIndex) => {
										const QuestionSolveComponent = questionTypeSolveComponents[question?.type?.name];

										if (QuestionSolveComponent) {
											return (
												<QuestionSolveComponent
													key={ind + 1}
													questionGroup={questionGroup}
													question={question}
													questionNumber={questionGroup.isGroup ? questionIndex + 1 : ind + 1}
													readMode={
														!["student", "learner"].includes(QuestionSetSolveData?.user?.type) || QuestionSetSolveData.submitted == "review"
															? true
															: false
													}
													correctionMode={
														QuestionSetSolveData.submitted == "correction" ||
														(QuestionSetSolveData.submitted == "review" && QuestionSetSolveData.learnerId)
													}
												/>
											);
										}
										return null;
									})}
									<RQuestionSetNavigation />
									{QuestionSetSolveData.submitted == "review" && !QuestionSetSolveData.learnerId && (
										<section className={styles.teacher__feedback__in__review}>
											<RFlex>
												<i className="fa-solid fa-comment pt-1"></i>
												<span style={{ color: "#101820", fontWeight: "bold" }}>{tr`teacher_feedback`}</span>
											</RFlex>
											<span className="text-muted pl-4">{questionGroup?.questions[0]?.teacherFeedback}</span>
										</section>
									)}
								</div>
							);
						})}
					{QuestionSetSolveData.step == QuestionSetSolveData?.questionsToSolve?.questionGroups?.length &&
						QuestionSetSolveData.teacherInCorrectionAndReviewMode && <TeacherFeedbackToAllQs />}
				</Col>
			</Row>
		</>
	);
};

export default RSolveQuestionSet;
