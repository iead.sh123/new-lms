import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const lessonContentApi = {
  getLessonContentByLessonId: (lessonId, withoutLiveSessions, published) =>
    get(
      `${Services.lesson_content.backend}api/lesson-contents/${lessonId}${
        withoutLiveSessions && !published
          ? `?withoutLiveSessions=${withoutLiveSessions}`
          : !withoutLiveSessions && published
          ? `?published=${published}`
          : withoutLiveSessions && published
          ? `?withoutLiveSessions=${withoutLiveSessions}&published=${published}`
          : ""
      }`
    ),

  deleteLessonContent: (lessonContentId, lessonContentType) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson-contents/type/${lessonContentType}/${lessonContentId}`
    ),

  addLessonText: (lessonId, data) =>
    post(
      `${Services.lesson_content.backend}api/lesson_content/text/${lessonId}`,
      data
    ),
  addLessonLink: (lessonId, data) =>
    post(
      `${Services.lesson_content.backend}api/lesson_content/link/${lessonId}`,
      data
    ),
  addLessonFile: (lessonId, data) =>
    post(
      `${Services.lesson_content.backend}api/lesson_content/attachment/${lessonId}`,
      data
    ),
  addLiveSession: (lessonId) =>
    post(
      `${Services.lesson_content.backend}api/lesson_content/live-sessions/${lessonId}`
    ),

  editLessonAttachment: (lessonContentId, data) =>
    patch(
      `${Services.lesson_content.backend}api/lesson_content/attachment/${lessonContentId}`,
      data
    ),

  editLessonLink: (lessonContentId, data) =>
    patch(
      `${Services.lesson_content.backend}api/lesson_content/link/${lessonContentId}`,
      data
    ),

  editLessonText: (lessonContentId, data) =>
    patch(
      `${Services.lesson_content.backend}api/lesson_content/text/${lessonContentId}`,
      data
    ),

  liveSessionById: (lessonContentId) =>
    get(
      `${Services.lesson_content.backend}api/lesson_content/live-sessions/${lessonContentId}`
    ),
  // ____________________________________________________________________________

  removeLessonLink: (lessonLinkId) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson_content/link/${lessonLinkId}`
    ),
  removeLessonText: (lessonTextId) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson_content/text/${lessonTextId}`
    ),
  removeLessonFile: (lessonFileId) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson_content/attachment/${lessonFileId}`
    ),
  removeLessonLiveSession: (lessonTextId) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson_content/live_session/${lessonTextId}`
    ),
  removeLessonPlan: (lessonFileId) =>
    destroy(
      `${Services.lesson_content.backend}api/lesson_content/lesson_plan/${lessonFileId}`
    ),

  showLessonText: (lessonTextId) =>
    get(
      `${Services.lesson_content.backend}api/lesson_content/text/${lessonTextId}`
    ),
};
