import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import React, { useEffect, useState } from "react";
import * as colors from "config/constants";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";
import { dataTypes } from "logic/SchoolManagement/constants";
import * as actions from "store/actions/global/schoolManagement.action";
import { useDispatch, useSelector } from "react-redux";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RLister from "components/Global/RComs/RLister";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GCreateUser from "../TermsManagement/GCreateUser";
import { termsManagementApi } from "api/global/termsManagement";
import { useMutateData } from "hocs/useMutateData";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { schoolManagementV2API } from "api/SchoolManagementV2";
const GEnrollUsersV2 = ({
	levelId,
	userTypeId,
	userType,
	closeModal
}) => {
	const [searchData, setSearchData] = useState("");
	const [openCreateUserModal, setOpenCreateUserModal] = useState(false);

	const { data: outsideUsers, isLoading: isLoadingOutsideUsers } = useFetchDataRQ({
		queryKey: ["Users", userTypeId],
		queryFn: () => schoolManagementV2API.getOutsideUsers(levelId, userTypeId),
		onSuccessFn: () => { console.log("Users Fetching Success") }
	})
	const enrollUserToLevel = useMutateData({
		queryFn: ({ payload }) => schoolManagementV2API.enrollUserToLevel(levelId, payload),
		onSuccessFn: ({ data, variables }) => {

		}
	})
	const handleCloseModal = () => {
		setOpenCreateUserModal(false);
	};
	const handleUserClicked = (user) => {
		const payload = {
			user_id: user.id,
			permission: 1,
			account_type_id: userTypeId
		}
		enrollUserToLevel.mutate({ payload })
	};

	const handleSearch = (clearData) => {
		// const data = clearData == "" ? clearData : searchData;
		// if (type == dataTypes.Principals) {
		// 	dispatch(actions.getCandidatePrincipalForEducation(null, { educationStageId: levelId, prefix: data }));
		// } else if (type == dataTypes.Students) {
		// 	dispatch(actions.getCandidateStudentsForGrade(null, { gradeLevelId: levelId, prefix: data }));
		// }
	};
	const userMutation = useMutateData({
		queryFn: ({ userId, data }) => termsManagementApi.addUserToOrganization({ userId, data }),
		// invalidateKeys: ['teachers', 'search'],
		invalidateKeys: [""],
		closeDialog: () => setOpenCreateUserModal(false),
	});
	const _records = outsideUsers?.data?.data?.map((user) => {
		const userComponent = (
			<RProfileName name={user.name}
				img={user.image ? user.image : null}
				imgStyle={{ width: "18px", height: "18px" }} />
		);
		return {
			details: [
				{
					key: userType,
					value: userComponent, type: "component"
				},
			],
			actions: [
				{
					name: " ",
					icon: iconsFa6.plus,
					color: "primary",
					outline: true,
					loading: enrollUserToLevel.isLoading,
					disabled: enrollUserToLevel.isLoading,
					onClick: () => handleUserClicked(user),
				},
			],
		};
	})


	return isLoadingOutsideUsers ? (
		<Loader />
	) : (
		<RFlex className="flex-column" styleProps={{ gap: "5px" }}>
			<RSearchHeader
				// searchLoading={data.ids}
				searchData={searchData}
				handleSearch={handleSearch}
				setSearchData={setSearchData}
				handleChangeSearch={(value) => setSearchData(value)}
			/>
			<RFlex className="justify-content-between align-items-center" styleProps={{ width: "100%" }}>
				<AppModal
					show={openCreateUserModal}
					size={"md"}
					header={true}
					headerSort={<GCreateUser userType={userTypeId} addUserMutate={userMutation} gradeLevelId={null} educationStageId={null} />}
					parentHandleClose={handleCloseModal}
				/>
				<RButton
					text="Create Account"
					onClick={() => setOpenCreateUserModal(true)}
					faicon={iconsFa6.plus}
					color="primary"
					outline
					style={{ margin: "0px", padding: "5px 10px", height: "32px" }}
				/>
			</RFlex>
			{<RLister Records={_records ?? null} stickyHeader={true} />}
		</RFlex>
	);
};

export default GEnrollUsersV2;
