import {
	ADD_EDIT_SCHOOL_EVENTS_REQUEST,
	ADD_EDIT_SCHOOL_EVENTS_SUCCESS,
	ADD_EDIT_SCHOOL_EVENTS_ERROR,
	SCHOOL_EVENTS_BY_ID_REQUEST,
	SCHOOL_EVENTS_BY_ID_SUCCESS,
	SCHOOL_EVENTS_BY_ID_ERROR,
	GET_ANNOUNCEMENTS_REQUEST,
	GET_ANNOUNCEMENTS_SUCCESS,
	GET_ANNOUNCEMENTS_ERROR,
	VOLUNTEER_IN_ANNOUNCEMENT_REQUEST,
	VOLUNTEER_IN_ANNOUNCEMENT_SUCCESS,
	VOLUNTEER_IN_ANNOUNCEMENT_ERROR,
	POST_ANNOUNCEMENTS_REQUEST,
	POST_ANNOUNCEMENTS_SUCCESS,
	POST_ANNOUNCEMENTS_ERROR,
	GET_VOLUNTEERS_REQUEST,
	GET_VOLUNTEERS_SUCCESS,
	GET_VOLUNTEERS_ERROR,
	GET_USER_TYPES_EVENT_REQUEST,
	GET_USER_TYPES_EVENT_SUCCESS,
	GET_USER_TYPES_EVENT_ERROR,
	ADD_EDIT_ANNOUNCEMENT_REQUEST,
	ADD_EDIT_ANNOUNCEMENT_SUCCESS,
	ADD_EDIT_ANNOUNCEMENT_ERROR,
	ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_REQUEST,
	ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_SUCCESS,
	ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
	SINGLE_ANNOUNCEMENT_REQUEST,
	SINGLE_ANNOUNCEMENT_SUCCESS,
	SINGLE_ANNOUNCEMENT_ERROR,
	DELETE_ANNOUNCEMENT_REQUEST,
	DELETE_ANNOUNCEMENT_SUCCESS,
	DELETE_ANNOUNCEMENT_ERROR,
	PUBLISH_ANNOUNCEMENT_REQUEST,
	PUBLISH_ANNOUNCEMENT_SUCCESS,
	PUBLISH_ANNOUNCEMENT_ERROR,
	REMOVE_ANNOUNCEMENT_ASYNC,
	ACCEPT_REJECT_VOLUNTEERS_REQUEST,
	ACCEPT_REJECT_VOLUNTEERS_SUCCESS,
	ACCEPT_REJECT_VOLUNTEERS_ERROR,
	REMOVE_IMAGE_FROM_ANNOUNCEMENT,
	REMOVE_POST_ANNOUNCEMENT_ASYNC,
	TRACK_RECORD_IN_STORE_ASYNC,
	SET_FLAG,
	REPLACE_SEEN_ANNOUNCEMENT,
} from "./adminType";

import { schoolEventsApi } from "api/admin/SchoolEvents";
import Swal from "utils/Alert";
import { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const replaceSeenAnnouncement = (announcementId) => async (dispatch) => {
	dispatch({
		type: REPLACE_SEEN_ANNOUNCEMENT,
		payload: { announcementId: announcementId },
	});
};

export const AddEditSchoolEvents = (data, history, schoolEventID) => async (dispatch) => {
	dispatch({ type: ADD_EDIT_SCHOOL_EVENTS_REQUEST });
	try {
		const response = await schoolEventsApi.addEditSchoolEvents(data);
		if (response.data.status == 1) {
			dispatch({
				type: ADD_EDIT_SCHOOL_EVENTS_SUCCESS,
			});

			if (!schoolEventID) {
				history.push(`${process.env.REACT_APP_BASE_URL}/admin/school-events`);
			}
		} else {
			dispatch({
				type: ADD_EDIT_SCHOOL_EVENTS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_EDIT_SCHOOL_EVENTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const FetchSchoolEventsById = (schoolEventId) => async (dispatch) => {
	dispatch({ type: SCHOOL_EVENTS_BY_ID_REQUEST });
	try {
		const response = await schoolEventsApi.getSchoolEventsById(schoolEventId);
		if (response.data.status == 1) {
			const SchoolEventsById = response.data.data.SchoolEvent;

			dispatch({
				type: SCHOOL_EVENTS_BY_ID_SUCCESS,
				payload: { SchoolEventsById },
			});
		} else {
			dispatch({
				type: SCHOOL_EVENTS_BY_ID_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SCHOOL_EVENTS_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

//Announcements

export const FetchAnnouncementsAsync = (page) => async (dispatch) => {
	dispatch({ type: GET_ANNOUNCEMENTS_REQUEST });
	try {
		const response = await schoolEventsApi.getAnnouncement(page);
		if (response.data.status == 1) {
			const Announcements = response.data.data.announcements.data;
			const LoadMore =
				response.data.data.announcements.next_page_url &&
				response.data.data.announcements.next_page_url.substr(+response.data.data.announcements.next_page_url.indexOf("=") + 1);

			dispatch({
				type: GET_ANNOUNCEMENTS_SUCCESS,
				Announcements,
				loadMorePage: LoadMore,
			});
		} else {
			dispatch({
				type: GET_ANNOUNCEMENTS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ANNOUNCEMENTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const trackRecordInStoreAsync = (announcementId, flag) => async (dispatch) => {
	dispatch({
		type: TRACK_RECORD_IN_STORE_ASYNC,
		payload: { announcementId, flag },
	});
};

export const PostVolunteerInAnnouncementsAsync = (announciableId, announcementId) => async (dispatch) => {
	dispatch({ type: VOLUNTEER_IN_ANNOUNCEMENT_REQUEST });
	try {
		const response = await schoolEventsApi.volunteerInAnnouncement(announciableId);
		if (response.data.status == 1) {
			dispatch({
				type: VOLUNTEER_IN_ANNOUNCEMENT_SUCCESS,
				announcementId,
			});

			window.location.reload();
		} else {
			dispatch({
				type: VOLUNTEER_IN_ANNOUNCEMENT_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: VOLUNTEER_IN_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const FetchPostAnnouncementsAsync = (data, page) => async (dispatch) => {
	dispatch({ type: POST_ANNOUNCEMENTS_REQUEST });
	try {
		const response = await schoolEventsApi.postAnnouncements(data, page);
		if (response.data.status == 1) {
			const postAnnouncements = response.data.data.announcements.data;
			const PostLoadMore =
				response.data.data.announcements.next_page_url &&
				response.data.data.announcements.next_page_url.substr(+response.data.data.announcements.next_page_url.indexOf("=") + 1);
			const add_permission = response.data.data.has_add_permission;

			dispatch({
				type: POST_ANNOUNCEMENTS_SUCCESS,
				postAnnouncements,
				PostLoadMore,
				add_permission,
			});
		} else {
			dispatch({
				type: POST_ANNOUNCEMENTS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: POST_ANNOUNCEMENTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const FetchVolunteersAsync = (schoolEventId, page) => async (dispatch) => {
	dispatch({ type: GET_VOLUNTEERS_REQUEST });
	try {
		const response = await schoolEventsApi.getVolunteers(schoolEventId, page);
		if (response.data.status == 1) {
			const Volunteers = response.data.data.volunteers.data;
			const LoadMore =
				response.data.data.volunteers.next_page_url &&
				response.data.data.volunteers.next_page_url.substr(+response.data.data.volunteers.next_page_url.indexOf("=") + 1);

			dispatch({
				type: GET_VOLUNTEERS_SUCCESS,
				Volunteers,
				LoadMorePage: LoadMore,
			});
		} else {
			dispatch({
				type: GET_VOLUNTEERS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_VOLUNTEERS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const FetchUserTypesAsync = (accessability_level_id) => async (dispatch) => {
	dispatch({ type: GET_USER_TYPES_EVENT_REQUEST });
	try {
		const response = await schoolEventsApi.getUserTypes(accessability_level_id);
		if (response.data.status == 1) {
			const UserTypes = response.data.data.models;

			dispatch({
				type: GET_USER_TYPES_EVENT_SUCCESS,
				payload: { UserTypes },
			});
		} else {
			dispatch({
				type: GET_USER_TYPES_EVENT_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_USER_TYPES_EVENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const AddEditAnnouncementAsync = (data, history) => async (dispatch) => {
	dispatch({ type: ADD_EDIT_ANNOUNCEMENT_REQUEST });
	try {
		if (data.payload.title == undefined || "") {
			dispatch({
				type: ADD_EDIT_ANNOUNCEMENT_ERROR,
			});
		} else if (data.payload.text == undefined || "") {
			dispatch({
				type: ADD_EDIT_ANNOUNCEMENT_ERROR,
			});
		} else if (data.payload.user_types == undefined || data.payload.user_types.length == 0) {
			dispatch({
				type: ADD_EDIT_ANNOUNCEMENT_ERROR,
			});
		} else {
			const response = await schoolEventsApi.addEditAnnouncement(data);
			if (response.data.status == 1) {
				dispatch({
					type: ADD_EDIT_ANNOUNCEMENT_SUCCESS,
				});
				history.goBack();
			} else {
				dispatch({
					type: ADD_EDIT_ANNOUNCEMENT_ERROR,
				});

				toast.error(response.data.msg);
			}
		}
	} catch (error) {
		dispatch({
			type: ADD_EDIT_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const AddEditAndPublishAnnouncementAsync = (data, history) => async (dispatch) => {
	dispatch({ type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_REQUEST });

	try {
		if (data.payload.title == undefined || "") {
			dispatch({
				type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
			});
		} else if (data.payload.text == undefined || "") {
			dispatch({
				type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
			});
		} else if (data.payload.user_types == undefined || data.payload.user_types.length == 0) {
			dispatch({
				type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
			});
		} else {
			const response = await schoolEventsApi.addEditAnnouncement(data);
			if (response.data.status == 1) {
				dispatch({
					type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_SUCCESS,
				});
				history.goBack();
			} else {
				dispatch({
					type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
				});

				toast.error(response.data.msg);
			}
		}
	} catch (error) {
		dispatch({
			type: ADD_EDIT_AND_PUBLISH_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const SingleAnnouncementAsync = (id) => async (dispatch) => {
	dispatch({ type: SINGLE_ANNOUNCEMENT_REQUEST });
	try {
		const response = await schoolEventsApi.singleAnnouncement(id);
		if (response.data.status == 1) {
			const data = response.data.data.announcements;
			dispatch({
				type: SINGLE_ANNOUNCEMENT_SUCCESS,
				data,
			});
		} else {
			dispatch({
				type: SINGLE_ANNOUNCEMENT_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SINGLE_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const DeleteAnnouncementAsync = (id) => async (dispatch) => {
	dispatch({ type: DELETE_ANNOUNCEMENT_REQUEST });
	try {
		const response = await schoolEventsApi.deleteAnnouncement(id);
		if (response.data.status == 1) {
			dispatch({
				type: DELETE_ANNOUNCEMENT_SUCCESS,
			});
			window.location.reload();
		} else {
			dispatch({
				type: DELETE_ANNOUNCEMENT_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: DELETE_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const PublishAnnouncementAsync = (announcement_ids) => async (dispatch) => {
	dispatch({ type: PUBLISH_ANNOUNCEMENT_REQUEST });
	try {
		const response = await schoolEventsApi.publishAnnouncement(announcement_ids);

		if (response.data.status == 1) {
			dispatch({
				type: PUBLISH_ANNOUNCEMENT_SUCCESS,
			});
			window.location.reload();
		} else {
			dispatch({
				type: PUBLISH_ANNOUNCEMENT_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: PUBLISH_ANNOUNCEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

//use this in useEffect because empty postAnnouncement Before callback endpoint scroll loading
export const RemovePostAnnouncementAsync = () => async (dispatch) => {
	dispatch({
		type: REMOVE_POST_ANNOUNCEMENT_ASYNC,
		payload: { emptyAnnouncement: [], emptyAnnouncementDone: true },
	});
};

//use this in useEffect because empty Announcement Before callback endpoint scroll loading
export const RemoveAnnouncementAsync = () => async (dispatch) => {
	dispatch({
		type: REMOVE_ANNOUNCEMENT_ASYNC,
		payload: [],
	});
};

export const RemoveImageFromAnnouncement = (id) => async (dispatch) => {
	dispatch({
		type: REMOVE_IMAGE_FROM_ANNOUNCEMENT,
		payload: { id },
	});
};

export const AcceptRejectVolunteersAsync = (data) => async (dispatch) => {
	dispatch({ type: ACCEPT_REJECT_VOLUNTEERS_REQUEST });
	try {
		const response = await schoolEventsApi.acceptRejectVolunteers(data);
		if (response.data.status == 1) {
			dispatch({
				type: ACCEPT_REJECT_VOLUNTEERS_SUCCESS,
			});
			window.location.reload();
		} else {
			dispatch({
				type: ACCEPT_REJECT_VOLUNTEERS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ACCEPT_REJECT_VOLUNTEERS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const setFlag = (data) => async (dispatch) => {
	dispatch({
		type: SET_FLAG,
		payload: { data },
	});
};
