import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { userLogsAsync } from "store/actions/global/track.action";

const withLogs = (Com, data) => {
  return (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
      // Code to be executed on component mount
      dispatch(userLogsAsync(data));
      return () => {
        // Code to be executed on component unmount
      };
    }, [data]);

    return <Com {...props} />;
  };
};

export default withLogs;
