import {
	GET_ALL_CARDS_REQUEST,
	GET_ALL_CARDS_SUCCESS,
	GET_ALL_CARDS_ERROR,
	CARD_TRACK_LAST_SEEN_REQUEST,
	CARD_TRACK_LAST_SEEN_SUCCESS,
	CARD_TRACK_LAST_SEEN_ERROR,
	TRACK_RECORDS_REQUEST,
	TRACK_RECORDS_SUCCESS,
	TRACK_RECORDS_ERROR,
	USER_INTERACTIONS_REQUEST,
	USER_INTERACTIONS_SUCCESS,
	USER_INTERACTIONS_ERROR,
	USER_LOGS_REQUEST,
	USER_LOGS_SUCCESS,
	USER_LOGS_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import { trackApi } from "api/global/track";
import { replaceSeenAnnouncement } from "../admin/schoolEvents";
import { trackableRecordAsync } from "./notification.action";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const getAllCardsAsync = () => async (dispatch) => {
	dispatch({ type: GET_ALL_CARDS_REQUEST });
	try {
		const response = await trackApi.getAllCards();
		if (response.data.status === 1) {
			const data = response.data.data.cards;

			dispatch({
				type: GET_ALL_CARDS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_ALL_CARDS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_ALL_CARDS_ERROR });
		toast.error(error?.message);
	}
};

export const trackLastSeenAsync = (table_name) => async (dispatch) => {
	dispatch({ type: CARD_TRACK_LAST_SEEN_REQUEST });
	try {
		const response = await trackApi.cardTrackLastSeen(table_name);
		if (response.data.status === 1) {
			dispatch({
				type: CARD_TRACK_LAST_SEEN_SUCCESS,
			});
		} else {
			dispatch({ type: CARD_TRACK_LAST_SEEN_ERROR });
		}
	} catch (error) {
		dispatch({ type: CARD_TRACK_LAST_SEEN_ERROR });
	}
};

export const trackRecordsAsync = (data) => async (dispatch) => {
	dispatch({ type: TRACK_RECORDS_REQUEST });
	try {
		const response = await trackApi.trackRecords(data);
		if (response.data.status === 1) {
			dispatch({
				type: TRACK_RECORDS_SUCCESS,
			});

			const unSeenNotifications = response.data.data.notifications;
			const unSeenCountNotifications = response.data.data.nb_unseen_notification;

			dispatch(trackableRecordAsync(unSeenNotifications, unSeenCountNotifications));
			if (data.payload.trackable_type == "announcements") {
				dispatch(replaceSeenAnnouncement(data.payload.trackable_id[0]));

				// dispatch(
				//   trackRecordInStoreAsync(
				//     data.payload.trackable_id[0],
				//     data.payload.flag
				//   )
				// );
			}
		} else {
			dispatch({ type: TRACK_RECORDS_ERROR });
		}
	} catch (error) {
		dispatch({ type: TRACK_RECORDS_ERROR });
	}
};

//-------------Services--------------

export const userInteractionsAsync = (learningObjectId, data) => async (dispatch) => {
	dispatch({ type: USER_INTERACTIONS_REQUEST });
	try {
		const response = await trackApi.userInteractions(learningObjectId, data);
		if (response.data.status === 1) {
			dispatch({
				type: USER_INTERACTIONS_SUCCESS,
			});
		} else {
			dispatch({ type: USER_INTERACTIONS_ERROR });
		}
	} catch (error) {
		dispatch({ type: USER_INTERACTIONS_ERROR });
	}
};

export const userLogsAsync = (data) => async (dispatch) => {
	dispatch({ type: USER_LOGS_REQUEST });
	try {
		const response = await trackApi.userLogs(data);
		if (response.data.status === 1) {
			dispatch({
				type: USER_LOGS_SUCCESS,
			});
		} else {
			dispatch({ type: USER_LOGS_ERROR });
		}
	} catch (error) {
		dispatch({ type: USER_LOGS_ERROR });
	}
};
