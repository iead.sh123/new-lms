import React from "react";
import RUploadMedia from "./RUploadMedia";
import styles from "../../FileManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUploadFile = ({ data, handlers }) => {
	return (
		<RFlex className={styles.upload__file__container}>
			<RFlex className={!data.values.showFileContent ? styles.upload__file__full__width : styles.upload__file}>
				<span>{tr`upload_media`}</span>
				<RUploadMedia data={data} handlers={handlers} />
			</RFlex>
			{data.values.showFileContent && <RFlex className={styles.specific__file}>HERE</RFlex>}
		</RFlex>
	);
};

export default RUploadFile;
