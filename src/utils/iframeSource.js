import { Services } from "engine/services";
export const iframeFriendlySource = (src) => {
  if (src?.includes("https://")) return src;
  else if (src?.includes("http://")) return src;
  else return Services.storage.flipBookFile + src;
};
