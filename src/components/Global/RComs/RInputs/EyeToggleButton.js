import React from "react";

const EyeToggleButton = ({ show, click }) => {
  return (
    <div
      onClick={click}
      style={{
        position: "absolute",
        top: "15px",
        right: "10px",
        cursor: "pointer",
        color: show ? "#333" : "#ddd",
      }}
    >
      {show ? (
        <i class="fa-regular fa-eye"></i>
      ) : (
        <i class="fa-regular fa-eye-slash"></i>
      )}
    </div>
  );
};

export default EyeToggleButton;
