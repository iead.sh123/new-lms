import React, { useEffect, useState } from "react";
import {
  Route,
  Switch as routesSwitch,
  useHistory,
  useParams,
} from "react-router-dom";

import {
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Row,
  Col,
} from "reactstrap";

import Helper from "./Helper";

import "./RTabs.css";

import RreactstrapSwitch from "./RreactstrapSwitch";
// import { bindDynamicNodes } from "store/reducers/global/NavigationReducer";
// import { useDispatch } from "react-redux";

const RRoutesTabsPanel = ({
  title,
  Tabs,
  SelectedIndex,
  changeHorizontalTabs,
  Url,
  baseUrl,
  baseRoute,
}) => {
  const { tabTitle } = useParams();
  // const dispatch  = useDispatch();
  const history = useHistory();
  if (!tabTitle) {
    history.push(baseUrl + "/" + Tabs[0].title);
    return <></>;
  }
  //? tabTitle : Tabs[0].title

  const [horizontalTabs, setHorizontalTabs] = useState(tabTitle);
  useEffect(() => {}, []);

  const [rTabPanelMode, setRTabPanelMode] = useState(true);

  const changeTab = (title, index) => {
    setHorizontalTabs(title);
    // changeHorizontalTabs(index);
    const target =
      baseUrl +
      "/" +
      Tabs[index].title +
      "/" +
      (Tabs[index].extraRouteParams ? Tabs[index].extraRouteParams : "");
    history.push(target);
  };

  function getRoutes() {
    const breadcrumbNodes = [];
    const mapped = Tabs.map((s, key) => {
      const tempPath =
        baseRoute + "/" + s.title + "/" + (s.RouteParams ? s.RouteParams : "");
      breadcrumbNodes.push({ name: s.title, link: tempPath });

      return (
        <Route
          //extraRouteParams:tab_data.masterTableName+"/"+
          //tab_data.detailTableName+"/"+
          //tab_data.pivotTableName+"/"+
          //(tab_data.manyToMany?"1":"0"),
          //extraRouteParams1:
          // path={window.location.href+"/"+s.extraRouteParams1+"/" }
          path={tempPath}
          render={s.content}
          key={key}
        />
      );
    });

    // bindDynamicNodes(dispatch,breadcrumbNodes)

    return mapped;
  }

  const [switcher, setSwitcher] = useState([]);

  useEffect(() => {
    const switchRoutes = getRoutes();
    setSwitcher(<routesSwitch>{switchRoutes}</routesSwitch>);
  }, []);

  //return       ;
  return (
    <div className="content">
      <Row>
        <Col style={{ display: "flex", justifyContent: "flex-end" }}>
          <RreactstrapSwitch
            defaultValue={true}
            offText={<i className="fa fa-th" style={{ fontSize: 16 }} />}
            onText={<i className="fa fa-table" style={{ fontSize: 16 }} />}
            onColor="default"
            offColor="default"
            onChange={() => setRTabPanelMode(!rTabPanelMode)}
          />
        </Col>
      </Row>
      {rTabPanelMode == true ? (
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <h5 className="text-center">{title}</h5>
              </CardHeader>
              <CardBody>
                <div className="nav-tabs-navigation">
                  <div className="nav-tabs-wrapper">
                    <Nav id="tabs" role="tablist" tabs>
                      {Tabs.map((tab, index) => {
                        return (
                          <NavItem key={index}>
                            <NavLink
                              aria-expanded={horizontalTabs === tab.title}
                              data-toggle="tab"
                              onMouseUp={() => {
                                history.replace(
                                  Url ? `${Url}${tab.title}` : "#"
                                );
                              }}
                              role="tab"
                              className={
                                horizontalTabs === tab.title
                                  ? "active font-weight-bold"
                                  : ""
                              }
                              onClick={() => changeTab(tab.title, index)}
                              disabled={tab.disabled}
                            >
                              {tab.title}
                            </NavLink>
                          </NavItem>
                        );
                      })}
                    </Nav>
                  </div>
                </div>
                <TabContent
                  className="text-center"
                  id="my-tab-content"
                  activeTab={horizontalTabs}
                >
                  {switcher
                    ? switcher
                    : Tabs.map((tab, index) => {
                        return (
                          <TabPane
                            key={index}
                            tabId={tab.title}
                            role="tabpanel"
                          >
                            {tab.content()}
                          </TabPane>
                        );
                      })}
                </TabContent>
              </CardBody>
            </Card>
          </Col>
        </Row>
      ) : (
        <>
          {" "}
          <Row>
            <Col
              xl="12"
              lg="12"
              md="12"
              sm="12 "
              className="no-padding-no-margin "
            >
              <div className=" px-1 py-3  Curriculum_title justify-content-end">
                {title}
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={2} xs={12} className="no-padding-no-margin ">
              <div className="nav-tabs-navigation">
                <div className="tabs_horizontal">
                  {Tabs?.map((tab, index) => {
                    return (
                      <NavItem
                        key={index}
                        style={{ listStyleType: "none" }}
                        className="tab_padding"
                      >
                        <NavLink
                          aria-expanded={horizontalTabs === tab.title}
                          data-toggle="tab"
                          onMouseUp={() => {
                            history.replace(Url ? `${Url}${tab.title}` : "#");
                          }}
                          role="tab"
                          className={
                            horizontalTabs === tab.title
                              ? "active font-weight-bold"
                              : ""
                          }
                          style={{ color: "gray", cursor: "pointer" }}
                          onClick={() => changeTab(tab.title, index)}
                          disabled={tab.disabled}
                        >
                          {tab.title}
                          <i
                            className="fa fa-angle-right"
                            style={{ float: "right" }}
                          />
                        </NavLink>
                      </NavItem>
                    );
                  })}
                </div>
              </div>
            </Col>
            <Col xs={12} md={10}>
              <TabContent
                className="text-center tabs_horizontal tab_padding"
                id="my-tab-content"
                activeTab={horizontalTabs}
              >
                {switcher
                  ? switcher
                  : Tabs.map((tab, index) => {
                      return (
                        <TabPane
                          key={index}
                          tabId={tab.title}
                          role="tabpanel"
                        ></TabPane>
                      );
                    })}
              </TabContent>
            </Col>
          </Row>
        </>
      )}
    </div>
  );
};

export default RRoutesTabsPanel;
