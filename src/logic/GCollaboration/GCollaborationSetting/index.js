import React from "react";
import GContents from "./GContents";
import GLevels from "./GLevels";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const GCollaborationSetting = () => {
	return (
		<RFlex styleProps={{ gap: 35, flexDirection: "column", marginBottom: 10 }}>
			<GContents />
			<GLevels />
		</RFlex>
	);
};

export default GCollaborationSetting;
