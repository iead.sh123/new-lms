import RAdvancedLister from "components/Global/RComs/RAdvancedLister"; 
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get,post } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React,{useState} from 'react';
import RTip from "components/Global/RComs/Collaboration/RTip";
import produce from "immer";
import { useEffect } from "react";
import tip_image from "assets/img/tip_image.png";
import switch_image from "assets/img/switch_image.png";
import RFlex from "components/Global/RComs/RFlex/RFlex";

   

export const GCollaborationSettingContents=({setContentCount})=>{
 
 
  const [data1,setData1]=useState([]);
  const [records, setRecords] = useState([]);

    const getDataFromBackend = async (specific_url) => {
      const url = Services.collaboration.backend+`api/organization/contents`;
      Helper.cl(url,"co url")
      let response1 = await get(specific_url ? specific_url : url);
  
      if (response1) {
        return response1;
      }
    };


    useEffect(()=>{
      let sharableIds=[];
      setRecords(
        data1.map((r, index) => {
        Helper.cl(r,"col contents r");
        Helper.cl(r?.sharable_status,"r.sharable_status?.pivot?.need_approval");
        const actions=getActions(r.id,r.sharable_status,r.sharable_status?.pivot,r.sharable_status?.pivot?.need_approval)
        if(!r.sharable_status) sharableIds.push(r.id);
        return {
            id:r.id,
            specialProps:
              { title : { text: r.name},
                UpLeftText:"All:"+r.count, 
                Actions: actions, bottomBorderColor:r.sharable_status?"green":"blue"},
        }}));
        setContentCount(data1?.length);
    },[data1])
   
    const setData = (response) =>
      {
        if(response?.data?.data?.data) setData1(response?.data?.data?.data);
    }
//---------------------------------------------------------------------------
    
    const collaborate=async (content_id,collaborate)=>{
      Helper.cl(content_id,"content_id");
      let newSharableContents=[];

      let toSend=[];
          if(collaborate)
          toSend=[...data1.filter(d=>d.sharable_status).map(d=>d.id),content_id];
          else
          toSend=data1.filter(d=>d.sharable_status&&d.id!==content_id).map(d=>d.id);
            
      //------------------------set to shareable
      const url = Services.collaboration.backend+`api/organization/sharable-contents`;
      Helper.cl(url,"co url")
    
      let response1 = await post( url,{"sharable_content_ids":toSend});

      if (response1&&response1.data&&response1.data.status==1) {
        Helper.cl(data1,"before produce") 
          const data2=produce(data1,tempdata=>{
            if(collaborate)
              {  
                const rec=tempdata.filter(r=>r.id==content_id)[0];
                rec.sharable_status={pivot:{need_approval:true}};
              }
        else
              {
                const rec=tempdata.filter(r=>r.id==content_id)[0];
                rec.sharable_status=null;
              }
          //Helper.cl(rec,"col rec");
          //rec.actions=getActions(content_id,collaborate,true);
        });
        Helper.cl(data2,"after produce") 
        setData1(data2);
      // Helper.cl(newRecords,"new records")
      }
      else
      {
        Helper.cl(response1,"response1")
      }


    }

    const change_need_approval=async (content_id,value)=>{

      //------------------------set to shareable
      const url = Services.collaboration.backend+`api/organization/set-need-approval`;
      Helper.cl(url,"co url")
      const toSend={"content_type_id":content_id,"need_approval":value}
      let response1 = await post( url,toSend);
      if (response1&&response1.data&&response1.data.status==1) {
          const data2=produce(data1,tempdata=>{
           const rec=tempdata.filter(r=>r.id==content_id)[0];
          rec.sharable_status={pivot:{need_approval:false}};
        });
        Helper.cl(data2,"after produce") 
        setData1(data2);
      }

      else
      {


        const data2=produce(data1,tempdata=>{
        const rec=tempdata.filter(r=>r.id==content_id)[0];
        if(rec.sharable_status.pivot)
        {
          rec.sharable_status.pivot.need_approval = value;
        }
        else
        {
          rec.sharable_status={pivot:{need_approval:value}};

        }
        });
        Helper.cl(data2,"after produce need approval") 
        setData1(data2);
      }
    }
    const getActions =(id,sharable,pivot,need_approval )=>{

              let actions=[];
              if(!sharable) 
              actions.push    ({title: "Collaborate",
                    icon: "fa fa-share",
                    color:"blue" , 
                    onClick:()=>{collaborate(id,true)},
                  withBorder:false
                  });
                  else {
                  actions.push  (
                    {
                      title: "Sharable",
                      icon: "fa fa-check",
                      color:"green" , 
                      onClick:()=>{collaborate(id,false)},
                      
                      withBorder:false,
                  })
                  
                  if(pivot)
                  actions.push  (
                    {
                      title: "need approval",
                      type:"checkbox",
                      color:"black" , 
                      value:need_approval,
                      onClick:(e)=>{
                        
                        Helper.cl(e,"e appcheckbox");
                        change_need_approval(id,e)},
                      withBorder:false,
                  })
                  }
                  return actions;
    }

//--------------------------------------------------------------------
      return <div>
        {/* {Helper.js(records,"records")} */}
    <RAdvancedLister
        hideTableHeader={false}
        colorEveryOtherRow={false}
        //firstCellImageProperty={"image"}
        getDataFromBackend={getDataFromBackend}
        setData={setData}
        records={records}
        getDataObject={(response) => response.data?.data?.data}
        characterCount={15}
        marginT={"mt-3"}
        marginB={"mb-2"}
        align={"left"}
        SpecialCard={RCollaborationCard}
        showListerMode={"cardLister"}
        perLine={4}
  />
  <RFlex styleProps={{justifyContent:"flex-end"}}>
  <RTip 
  image ={tip_image}
  component={<div style={{textAlign:"center"}}> 
  Use the toggle
  <img src={switch_image}  style={{width:"40px",padding:"5px"}}/>
  to request an approval before sharing
  <br/><br/>
  Click on <span style={{color:"blue",textDecoration:"underlined"}}>Collaborate </span>to share Content

    
    </div>}
  />
  </RFlex>

      </div>



};
