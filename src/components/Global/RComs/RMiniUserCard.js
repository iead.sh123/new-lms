import React from "react";

import tr from "./RTranslator";
import RTableImage from "./RTableImage";
import { RLabel } from "./RLabel";
import RButtonIcon from "./RButtonIcon";
import RFlex from "./RFlex/RFlex";
import RImage from "./RImage";
function RMiniUserCard({name,image,role,onDelete,onClick,
  border="solid 1px #19A7CD",imageBorder="solid 1px #19A7CD",withDelete,disabled=false}) {
  return ( disabled?<></> :<div onClick={disabled?null:onClick} style={{display:"inline-block",padding:"4px",margin:"4px",border:border}}>
    <RFlex styleProps={{justifyContent:"center",alignItems:"center",width:"fit-content"}}>
    <RImage src={image} width="30" height="30"  style={{border:imageBorder,borderRadius:"50%"}}/> 
    
    <RLabel 
    value={name}   
    tooltiped = {false}
    lettersToShow = {30} >
  </RLabel>

  {disabled?<RLabel 
    value={"added"}   
    tooltiped = {false}
    >
  </RLabel>:null}
  {role?.length>1? <><svg xmlns="http://www.w3.org/2000/svg" style={{width:"6px",height:"6px"}}>
    <circle cx="3" cy="3" r="3" fill="darkgray" />
</svg><RLabel 
    value={role}   
    tooltiped = {false}
    lettersToShow = {50}
    color={"darkgray"}
    >
  </RLabel></>:<></>}
  {withDelete?
    <RButtonIcon onClick={onDelete} style={ {border: "red 0px solid",
                  position: "relative",
                  width: "100%",
                  backgroundColor: "inherit",
                }  }
                
            
          ButtonAdditionalStyle={{border: "0px", background: "inherit"}}
          color={"red"}
          backgroundColor={"white"}
          text={"delete"}
          padding={"1px"}
          faicon={"close"}></RButtonIcon>:<></>}
  </RFlex></div> );
}
export default RMiniUserCard;
