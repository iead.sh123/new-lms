import GenericUILister from "components/Global/GenericUI/GenericUILister";
import Dashboard from "views/Admin/Dashboard";
const routes = [
  {
    path: "/school_groups",
    component: GenericUILister,
    icon: "nc-icon nc-layout-11",
    layout: process.env.REACT_APP_BASE_URL + "/admin",
  },
  {
    path: "/schools",
    component: Dashboard,
    icon: "nc-icon nc-layout-11",
    layout: process.env.REACT_APP_BASE_URL + "/admin",
  },
];

export default routes;
