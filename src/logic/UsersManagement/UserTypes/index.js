import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useState } from "react";
import RHeader from "../Shared/RHeader";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Terms, userTypesHeader } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import RLister from "components/Global/RComs/RLister";
import RLoader from "components/Global/RComs/RLoader";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { useMutateData } from "hocs/useMutateData";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import styles from "./userTypes.module.scss";
import GCreateUserType from "./GCreateUserType";
const GUserTypes = () => {
	const [alert1, setAlert] = useState(false);
	const [backendSearchTerm, setBackSearchTerm] = useState("");
	const [page, SetPage] = useState(1);
	const [typesIdsDeleteing, setTypesIdsDeleteing] = useState([]);
	const history = useHistory();
	const handleChangePage = (page) => {
		SetPage(page);
	};

	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
		refetch: refetchUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes, backendSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationUserTypes(backendSearchTerm),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const deleteUserTypeMutation = useMutateData({
		queryFn: ({ id }) => usersManagementApi.deleteOrganizationUserType(id),
		invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: ({ variables }) => {
			CUDToQueryKey({
				queryKey: [Terms.UserTypes, backendSearchTerm],
				insertionDepth: `data.data`,
				id: variables.id,
				operation: operations.DELETE,
			});
			setTypesIdsDeleteing(typesIdsDeleteing.filter((id) => id != variables?.id));
		},
		onErrorFn: (error, variables) => {
			setTypesIdsDeleteing(typesIdsDeleteing.filter((id) => id != variables?.id));
		},
	});
	const handleDeleteUserType = ({ userType }) => {
		deleteUserTypeMutation.mutate({ id: userType.id });
		setTypesIdsDeleteing([...typesIdsDeleteing, userType.id]);
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const deleteUserType = (userType) => {
		const confirm = tr`Yes_delete_it`;
		const message = (
			<div>
				<h6>
					{tr`Are_you_sure_to_delete`} <span className="p-0 m-0">{userType.type.title}</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This_can't_be_undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleDeleteUserType, { userType }, message, confirm);
	};
	if (isLoadingUserTypes) return <RLoader />;

	const records = userTypes?.data?.data?.map((userType, index) => {
		const userTypesCount = userTypes?.data?.data?.length;
		const DescriptionComponent = (
			<RFlex className="align-items-center" styleProps={{ minHeight: "52px", maxWidth: "400px" }}>
				{"this is Description"}
			</RFlex>
		);
		const userTypesNameComponent = (
			<RFlex
				styleProps={{ cursor: "pointer" }}
				className={`${styles.hoverLink}`}
				onClick={() => {
					history.push(`${baseURL}/${genericPath}/users-management/user-types/${userType.id}`);
				}}
			>
				<span className="p-0 m-0">{userType?.type?.name}</span>
			</RFlex>
		);
		return {
			details: userTypesHeader.map((header, index) => ({
				key:
					header.name == "userType" ? (
						<RFlex className="align-items-center">
							{tr(`${header.key}`)}{" "}
							<span className="text-primary" style={{ fontSize: "8px" }}>
								{userTypesCount}
							</span>
						</RFlex>
					) : (
						header.key
					),
				keyType: header.name == "userType" ? "component" : "",
				value:
					header.name == "userType"
						? userTypesNameComponent
						: header.name == "description"
						? DescriptionComponent
						: userType?.[header.value],
				type: header.name == "userType" || header.name == "description" ? "component" : "",
			})),
			actions: [
				{
					name: "",
					icon: iconsFa6.delete,
					justIcon: true,
					loading: typesIdsDeleteing.some((id) => id == userType.id),
					disabled: typesIdsDeleteing.some((id) => id == userType.id),
					onClick: () => {
						deleteUserType(userType);
					},
					actionIconClass: "text-danger",
				},
			],
		};
	});
	return (
		<>
			<RFlex className="flex-column">
				<RHeader
					createText={"New_User_Type"}
					history={history}
					setBackSearchTerm={setBackSearchTerm}
					isFetching={isFetchingUserTypes}
					refetchFunction={refetchUserTypes}
					showBulk={false}
					dataType={Terms.UserTypes}
					onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/user-types/add`)}
					searchPlaceholder={"Search_For_a_User_Type"}
				/>
				<RLister
					Records={records}
					// info={userTypes}
					// withPagination={true}
					// handleChangePage={handleChangePage}
					// page={page}
					line1={"No_User_Types_Yet"}
				/>
			</RFlex>
			{alert1}
		</>
	);
};

export default GUserTypes;
