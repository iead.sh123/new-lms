import React from "react";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { TransformedTags } from "utils/TransformedTags";
import iconsFa6 from "variables/iconsFa6";

const RUnitPlanMainData = ({ unitPlan, width }) => {
	return (
		<RFlex className={styles.up__viewer__main__data} style={{ width: width ? width : "" }}>
			<RFlex className={styles.up__viewer__main__data__container}>
				<RFlex>
					<span className="font-weight-bold"> {tr`title`}</span>: {unitPlan?.title}
				</RFlex>
				<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
					{unitPlan?.unit_number && (
						<span>
							<span className="font-weight-bold">{tr`unit_number`}</span>: {unitPlan?.unit_number}
						</span>
					)}
					{unitPlan?.time && (
						<span>
							<i className={iconsFa6.clockSolid} />
							&nbsp;
							<span className="font-weight-bold">{tr`unit_time`} :</span> {unitPlan?.time}
						</span>
					)}
				</RFlex>

				{unitPlan?.tags?.length > 0 && (
					<RFlex styleProps={{ alignItems: "center", width: "100%", flexWrap: "wrap" }}>
						<span className="font-weight-bold">
							<i className={iconsFa6.tag} />
							&nbsp;{tr`tags`} :
						</span>
						<TransformedTags tags={unitPlan?.tags} />
					</RFlex>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RUnitPlanMainData;
