import React, { useState, useEffect, useReducer } from "react";
import RPermissions from "view/UsersAndPermissions/RPermissions";
import styles from "../GUsersAndPermissions.module.scss";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import useWindowDimensions from "components/Global/useWindowDimensions";
import tr from "components/Global/RComs/RTranslator";
import { Form, FormGroup, Input, Row, Col } from "reactstrap";
import { getPermissionsBySpecificUserAsync } from "../State/UsersAndPermissions.action";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { useParams } from "react-router-dom";
import Loader from "utils/Loader";
import AppModal from "components/Global/ModalCustomize/AppModal";
import AddPermissionsToUserOrRoleLister from "./AddPermissionsToUserOrRoleLister";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { post } from "config/api";

import { Services } from "engine/services";
import { primaryColor } from "config/constants";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const GPermissionsToUser = () => {
	const { organizationUserId, tabTitle } = useParams();
	const [alert, setAlert] = useState(false);

	const [addUsersToPermission, setAddUsersToPermission] = useState(false);
	const [addUsersToPermissionLoading, setAddUsersToPermissionLoading] = useState(false);
	const [payloadData, setPayloadData] = useState({
		user_organization_id: organizationUserId,
		scopes: [],
	});

	const { width } = useWindowDimensions();
	const mobile = width < 1000;
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	const [searchData, setSearchData] = useState("");

	useEffect(() => {
		getPermissionsBySpecificUserAsync(dispatch, organizationUserId);
	}, []);

	const handleSearch = (emptySearch) => {
		getPermissionsBySpecificUserAsync(dispatch, organizationUserId, emptySearch ?? searchData);
	};

	const handleOpenForm = () => setAddUsersToPermission(true);
	const handleCloseForm = () => setAddUsersToPermission(false);

	const handleAddUsersToPermission = async () => {
		setAddUsersToPermissionLoading(true);
		const response = await post(
			`${Services.auth_organization_management.backend}api/organization_user/addScopesToOrganizationUser`,
			payloadData
		);

		if (response) {
			if (response.data.status == 1) {
				setAddUsersToPermissionLoading(false);
				handleCloseForm();
				getPermissionsBySpecificUserAsync(dispatch, organizationUserId);
				setPayloadData({
					user_organization_id: organizationUserId,
					scopes: [],
				});
			}
		} else {
			setAddUsersToPermissionLoading(false);
			toast.error(response.data.msg);
		}
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const successDelete = async (prameters) => {
		let response;
		const data = {
			organization_user_id: organizationUserId,
			scopes_ids: [prameters.id],
		};
		response = await post(`${Services.auth_organization_management.backend}api/organization_user/removeScopesFromOrganizationUser`, data);
		if (response) {
			if (response.data.status == 1) {
				hideAlert();
				getPermissionsBySpecificUserAsync(dispatch, organizationUserId);
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};
	return (
		<React.Fragment>
			{alert}
			<AppModal
				show={addUsersToPermission}
				parentHandleClose={handleCloseForm}
				header={
					<RFlex>
						<span>
							{payloadData.scopes.length} {tr`selected_permissions`}
						</span>
						{addUsersToPermissionLoading ? (
							<i className="fa fa-refresh fa-spin"></i>
						) : payloadData.scopes.length > 0 ? (
							<span
								className={styles.btn_hover}
								style={{
									color: primaryColor,
									textDecoration: "underline",
									cursor: "pointer",
								}}
								onClick={() => {
									handleAddUsersToPermission();
								}}
							>
								{tr`save`}
							</span>
						) : (
							""
						)}
					</RFlex>
				}
				size={"lg"}
				headerSort={<AddPermissionsToUserOrRoleLister payloadData={payloadData} setPayloadData={setPayloadData} />}
			/>
			<div className={styles.box}>
				<RFlex
					styleProps={{
						display: mobile ? "block" : "flex",
						width: "100%",
						alignItems: "center",
						gap: 30,
						marginBottom: "20px",
					}}
				>
					<RButton color="primary" text={tr`add_permission_to_user`} faicon="fa fa-plus" outline onClick={() => handleOpenForm()} />
					<Form className={styles.form_input}>
						<FormGroup>
							<Input
								type="text"
								placeholder={tr`search`}
								className={styles.search_input}
								defaultValue={searchData}
								onChange={(event) => {
									setSearchData(event.target.value);
								}}
								// disabled={
								//   usersAndPermissionsData.permissionsBySpecificUser.length == 0
								// }
							/>
							<React.Fragment>
								<i
									aria-hidden="true"
									className={
										usersAndPermissionsData.permissionsBySpecificUserLoading
											? `fa fa-refresh fa-spin ${styles.search_input_icon}`
											: `fa fa-search ${styles.search_input_icon}`
									}
									onClick={() => searchData !== "" && handleSearch()}
								/>
								<i
									aria-hidden="true"
									className={`fa fa-close ${styles.clear_input_icon}`}
									onClick={() => {
										if (searchData !== "") {
											setSearchData("");
											handleSearch("");
										}
									}}
								/>
							</React.Fragment>
						</FormGroup>
					</Form>
				</RFlex>

				{usersAndPermissionsData.permissionsBySpecificUserLoading ? (
					<Loader />
				) : usersAndPermissionsData.permissionsBySpecificUser.length > 0 ? (
					<Row>
						{usersAndPermissionsData.permissionsBySpecificUser.map((permission) => (
							<RPermissions permission={permission} handleRemove={handleRemove} />
						))}
					</Row>
				) : (
					<REmptyData />
				)}
			</div>
		</React.Fragment>
	);
};

export default GPermissionsToUser;
