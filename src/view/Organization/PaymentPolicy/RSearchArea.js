import React, { useContext } from "react";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RSearchArea = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);
	return (
		<RFlex styleProps={{ flexWrap: "wrap", justifyContent: "space-between" }}>
			<div style={{ width: PaymentPolicyData.tabType == "archive" ? "30%" : "40%" }}>
				<RSearchHeader
					widthInput="213px"
					searchLoading={PaymentPolicyData.discountLoading || PaymentPolicyData.couponLoading}
					searchData={PaymentPolicyData.values.search}
					handleSearch={PaymentPolicyData.handleSearch}
					setSearchData={(value) => PaymentPolicyData.setFieldValue("search", value)}
					handleChangeSearch={(value) => PaymentPolicyData.setFieldValue("search", value)}
				/>
			</div>

			<RFlex styleProps={{ width: PaymentPolicyData.tabType == "archive" ? "65%" : "58%", height: "100%" }}>
				<RSelect
					className={styles.search__select}
					closeMenuOnSelect={true}
					option={PaymentPolicyData.values.selectItems.filter((el) => el.value !== PaymentPolicyData.values.itemNameValue)}
					onChange={(e) => PaymentPolicyData.handleSelectItemName(e.value)}
					value={[{ label: PaymentPolicyData.values.itemNameValue, value: PaymentPolicyData.values.itemNameValue }]}
				/>
				{PaymentPolicyData.tabType !== "archive" && (
					<RFlex styleProps={{ gap: 0 }}>
						<RButton
							text={tr`new_discount`}
							onClick={() => PaymentPolicyData.handleOpenDiscountModal()}
							color="primary"
							faicon={iconsFa6.tag}
							outline
							className={styles.search__button + " p-1"}
						/>
						<RButton
							text={tr`new_coupon`}
							onClick={() => PaymentPolicyData.handleOpenCouponModal()}
							color="primary"
							faicon={iconsFa6.percent}
							outline
							className={styles.search__button + " p-1"}
						/>
					</RFlex>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RSearchArea;
