import React from "react";
import moment from "moment";
import { Row, Col } from "reactstrap";
import styles from "./styles/default.Module.scss";

const Notification_default = ({ isSeen, onClick, brief, padge, time, dir }) => (
  <>
    <Row
      onClick={onClick}
      style={{
        marginBottom: 5,
        cursor: "pointer",
        backgroundColor: isSeen ? "#edf1f7" : "#ffd9bf",
      }}
      className={styles.notification_layout_1 + " notification_item"}
    >
      <Col md={1} sm={0} style={{ display: dir ? "" : "flex" }}>
        <div
          style={{ fontSize: "50px" }}
          className={
            (isSeen ? "fa fa-check-circle " : "fa fa-check-circle-o ") +
            "new class"
          }
        />
      </Col>
      <Col md={4} sm={6} xs={12} className=" NotificationText">
        <div>
          <p> {brief} </p>
          <p> {moment(time).format("MM-DD-YYYY hh:mm a")}</p>
        </div>
      </Col>
      <Col md={4} sm={6} xs={12} className=" NotificationText">
        {padge && (
          <div
            style={{
              display: "inline",
              border: "solid gray 1px",
              background: "#f6aa77",
              padding: "3px",
              //   margin: "20px",
              borderRadius: "10%",
            }}
          >
            {padge}
          </div>
        )}
      </Col>
    </Row>
  </>
);

export default Notification_default;
