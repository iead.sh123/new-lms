import React, { useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { get } from "config/api";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import styles from "../GUsersAndPermissions.module.scss";
import { toast } from "react-toastify";

const GRolesByPermission = () => {
	const { permissionId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = async (emptySearch) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/role/rolesPermission/${permissionId}?filter=${
			emptySearch ?? searchData
		}`;
		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/role/rolesPermission/${permissionId}`;

		let response = await get(specific_url ? specific_url : url);

		if (response?.data?.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		const data = response?.data?.data?.data.map((r) => ({
			id: r?.id,

			table_name: "GRolesByPermissionLister",

			details: [
				{ key: tr`role_name`, value: r.name },
				{ key: tr`description`, value: r.description },
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords]
	);
	return (
		<section className={"p-4 " + styles.box}>
			<RSearchHeader
				searchLoading={searchLoading}
				searchData={searchData}
				handleSearch={handleSearch}
				setSearchData={setSearchData}
				handleChangeSearch={handleChangeSearch}
				inputPlaceholder={tr`search_a_role`}
				addNew={false}
			/>
			<RAdvancedLister {...propsLiterals.listerProps} />
		</section>
	);
};

export default GRolesByPermission;
