import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Row, Col } from "reactstrap";
import SolveImage from "assets/img/new/solve-qs.png";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import moment from "moment";
import { DATE_FORMATE } from "config/constants";
import { dangerColor } from "config/constants";
import { relativeDate } from "utils/dateUtil";
import { successColor } from "config/constants";

const RStartSolveQuestionSet = () => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  function isDateBetween(startDate, endDate) {
    const myDate = new Date();
    const start = new Date(startDate);
    const end = new Date(endDate);

    return myDate >= start && myDate <= end;
  }

  const enableSolving =
    QuestionSetSolveData.questionSetType == "assignments"
      ? isDateBetween(
          QuestionSetSolveData.questionsToSolve.dueDate,
          QuestionSetSolveData.questionsToSolve.cutoffDate
        )
      : ["polls", "surveys"].includes(QuestionSetSolveData.questionSetType)
      ? isDateBetween(
          QuestionSetSolveData.questionsToSolve.displayDate,
          QuestionSetSolveData.questionsToSolve.closeDate
        )
      : isDateBetween(
          QuestionSetSolveData.questionsToSolve.startDate,
          QuestionSetSolveData.questionsToSolve.endDate
        );

  return (
    <Row className="mt-4">
      <Col xs={12} sm={8}>
        <h4>
          {QuestionSetSolveData.questionSetType}&nbsp;
          {tr`title`} -{" "}
          {QuestionSetSolveData?.questionsToSolve?.questionset?.name}
        </h4>
        <p
          className="text-muted"
          dangerouslySetInnerHTML={{
            __html:
              QuestionSetSolveData?.questionsToSolve?.questionset?.description,
          }}
        />
        {QuestionSetSolveData.submitted == "review" ? (
          <div>
            {QuestionSetSolveData?.questionsToSolve?.isGraded ? (
              <div>
                <RFlex>
                  <i className="fa-solid fa-clock pt-1"></i>
                  <span>
                    {tr`was_reviewed on `}
                    {moment(
                      QuestionSetSolveData.questionSetType == "assignments"
                        ? QuestionSetSolveData.questionsToSolve.cutoffDate
                        : ["polls", "surveys"].includes(
                            QuestionSetSolveData.questionSetType
                          )
                        ? QuestionSetSolveData.questionsToSolve.closeDate
                        : QuestionSetSolveData.questionsToSolve.endDate
                    ).format(DATE_FORMATE)}
                  </span>
                </RFlex>
                {!enableSolving ? (
                  <RFlex styleProps={{ color: dangerColor }}>
                    <i className="fa-solid fa-ban pt-1"></i>{" "}
                    {relativeDate(
                      QuestionSetSolveData.questionSetType == "assignments"
                        ? QuestionSetSolveData.questionsToSolve.cutoffDate
                        : ["polls", "surveys"].includes(
                            QuestionSetSolveData.questionSetType
                          )
                        ? QuestionSetSolveData.questionsToSolve.closeDate
                        : QuestionSetSolveData.questionsToSolve.endDate
                    )}
                  </RFlex>
                ) : (
                  <RFlex>
                    <span style={{ color: successColor }}>
                      {tr`your_grade_is`} %
                    </span>
                  </RFlex>
                )}
              </div>
            ) : (
              <RFlex>
                <i className="fa-solid fa-clock pt-1"></i>
                <span>{tr`not_graded_yet`}</span>
              </RFlex>
            )}
          </div>
        ) : QuestionSetSolveData.submitted == "solving" &&
          !enableSolving &&
          !["quizzes", "exams"].includes(
            QuestionSetSolveData.questionSetType
          ) ? (
          <RFlex styleProps={{ color: dangerColor }}>
            <i className="fa-solid fa-ban pt-1"></i>
            <span>
              {tr`overdue`}&nbsp;
              {relativeDate(
                QuestionSetSolveData.questionSetType == "assignments"
                  ? QuestionSetSolveData.questionsToSolve.cutoffDate
                  : ["polls", "surveys"].includes(
                      QuestionSetSolveData.questionSetType
                    )
                  ? QuestionSetSolveData.questionsToSolve.closeDate
                  : QuestionSetSolveData.questionsToSolve.endDate
              )}
            </span>
          </RFlex>
        ) : QuestionSetSolveData.submitted == "solving" &&
          enableSolving &&
          ["quizzes", "exams"].includes(
            QuestionSetSolveData.questionSetType
          ) ? (
          <RFlex styleProps={{ color: dangerColor }}>
            <i className="fa-solid fa-clock pt-1"></i>
            <span>
              {tr`you have`}&nbsp;
              {QuestionSetSolveData.formattedTime}&nbsp;
              {tr`to complete the quiz since you start`}&nbsp;
            </span>
          </RFlex>
        ) : QuestionSetSolveData.submitted == "review" &&
          !enableSolving &&
          ["quizzes", "exams"].includes(
            QuestionSetSolveData.questionSetType
          ) ? (
          ""
        ) : (
          ""
        )}
        <RButton
          text={`${tr("start_the_")}${QuestionSetSolveData.questionSetType}`}
          color="primary"
          faicon="fa-solid fa-pen-to-square"
          onClick={QuestionSetSolveData.handlePushToSolveQuestionSet}
          disabled={
            QuestionSetSolveData?.doNotContainBehaviour
              ? false
              : QuestionSetSolveData?.questionsToSolve?.isGraded
              ? false
              : !enableSolving
          }
        />
      </Col>
      <Col xs={12} sm={4}>
        <img
          src={SolveImage}
          style={{ width: "340px", height: "340px" }}
          alt="solveImage"
        />
      </Col>
    </Row>
  );
};

export default RStartSolveQuestionSet;
