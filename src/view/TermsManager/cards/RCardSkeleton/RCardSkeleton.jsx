import React from "react";
import styles from './RCardSkeleton.module.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";


const RCardSkeleton = ({ content, onClick }) => {


  return <div className={styles.Card} onClick={()=>onClick?.()} role="button">
    {content}
  </div>
}

export default RCardSkeleton;