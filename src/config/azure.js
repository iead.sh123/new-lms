export const azureConfig = {
  appId: "d1c216ba-86f1-4e11-985b-a827cfd4a086",
//    appId_guidance1: "d1c216ba-86f1-4e11-985b-a827cfd4a086",
    // appId_old: "d1ec59a2-b21c-4380-834e-ac2fe7b8381a",
    // redirectUri_old: "http://localhost:3000/teacher/courses/",
  redirectUri: "https://ics.guidance.alrowad-tech.com/onedrive.php",
  scopes: [
    "files.Read.All",
    "files.ReadWrite",
    "files.ReadWrite.All",
    "files.Read.Selected",
    "files.Read",
  ],
  tenantId: "53991e3e-c591-459d-acd1-009a68f750cb",
};
