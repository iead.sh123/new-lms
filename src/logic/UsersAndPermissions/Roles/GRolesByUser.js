import React, { useState, useEffect, useReducer } from "react";
import RRole from "view/UsersAndPermissions/RRole";
import styles from "../GUsersAndPermissions.module.scss";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import useWindowDimensions from "components/Global/useWindowDimensions";
import tr from "components/Global/RComs/RTranslator";
import { Form, FormGroup, Input } from "reactstrap";
import { getRolesBySpecificUserAsync } from "../State/UsersAndPermissions.action";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import Loader from "utils/Loader";
import { useParams } from "react-router-dom";
import AppModal from "components/Global/ModalCustomize/AppModal";
import AddRolesToUserLister from "./AddRolesToUserLister";
import { post } from "config/api";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const GRolesByUser = () => {
	const { organizationUserId } = useParams();
	const [alert, setAlert] = useState(false);

	const [addUsersToRole, setAddUsersToRole] = useState(false);
	const [payloadData, setPayloadData] = useState({
		org_user_id: organizationUserId,
		roles_ids: [],
	});
	const [addRolesToUserLoading, setAddRolesToUserLoading] = useState(false);
	const [searchData, setSearchData] = useState("");

	const { width } = useWindowDimensions();
	const mobile = width < 1000;

	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	useEffect(() => {
		getRolesBySpecificUserAsync(dispatch, organizationUserId);
	}, []);

	const handleSearch = (emptySearch) => {
		getRolesBySpecificUserAsync(dispatch, organizationUserId, emptySearch ?? searchData);
	};

	const handleOpenForm = () => setAddUsersToRole(true);
	const handleCloseForm = () => setAddUsersToRole(false);

	const handleAddRolesToUser = async () => {
		setAddRolesToUserLoading(true);
		const response = await post(
			`${Services.auth_organization_management.backend}api/organization_user/addRolesToOrganizationUser`,
			payloadData
		);

		if (response) {
			if (response.data.status == 1) {
				setAddRolesToUserLoading(false);
				handleCloseForm();
				getRolesBySpecificUserAsync(dispatch, organizationUserId);
				setPayloadData({
					org_user_id: organizationUserId,
					roles_ids: [],
				});
			}
		} else {
			setAddRolesToUserLoading(false);
			toast.error(response.data.msg);
		}
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const successDelete = async (prameters) => {
		let response;
		const data = {
			user_organization_id: organizationUserId,
			roles_ids: [prameters.id],
		};
		response = await post(`${Services.auth_organization_management.backend}api/role/removeRolesFromUserOrganization`, data);
		if (response) {
			if (response.data.status == 1) {
				hideAlert();
				getRolesBySpecificUserAsync(dispatch, organizationUserId);
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	return (
		<React.Fragment>
			{alert}

			<AppModal
				show={addUsersToRole}
				parentHandleClose={handleCloseForm}
				header={
					<RFlex styleProps={{ alignItems: "center" }}>
						<span>
							{payloadData.roles_ids.length} {tr`selected_roles`}
						</span>
						{addRolesToUserLoading ? (
							<i className="fa fa-refresh fa-spin"></i>
						) : payloadData.roles_ids.length > 0 ? (
							<span
								className={styles.btn_hover}
								style={{
									color: primaryColor,
									textDecoration: "underline",
									cursor: "pointer",
								}}
								onClick={() => {
									handleAddRolesToUser();
								}}
							>
								{tr`save`}
							</span>
						) : (
							""
						)}
					</RFlex>
				}
				size={"lg"}
				headerSort={<AddRolesToUserLister payloadData={payloadData} setPayloadData={setPayloadData} />}
			/>
			<div className={styles.box}>
				<RFlex
					styleProps={{
						display: mobile ? "block" : "flex",
						width: "100%",
						alignItems: "center",
						gap: 30,
						marginBottom: "20px",
					}}
				>
					<RButton color="primary" text={tr`add_role_to_user`} faicon="fa fa-plus" outline onClick={() => handleOpenForm()} />
					<Form className={styles.form_input}>
						<FormGroup>
							<Input
								type="text"
								placeholder={tr`search`}
								className={styles.search_input}
								defaultValue={searchData}
								onChange={(event) => {
									setSearchData(event.target.value);
								}}
								// disabled={
								//   usersAndPermissionsData.rolesBySpecificUser.length == 0
								// }
							/>
							<React.Fragment>
								<i
									aria-hidden="true"
									className={
										usersAndPermissionsData.rolesBySpecificUserLoading
											? `fa fa-refresh fa-spin ${styles.search_input_icon}`
											: `fa fa-search ${styles.search_input_icon}`
									}
									onClick={() => searchData !== "" && handleSearch()}
								/>
								<i
									aria-hidden="true"
									className={`fa fa-close ${styles.clear_input_icon}`}
									onClick={() => {
										if (searchData !== "") {
											setSearchData("");
											handleSearch("");
										}
									}}
								/>
							</React.Fragment>
						</FormGroup>
					</Form>
				</RFlex>

				{usersAndPermissionsData.rolesBySpecificUserLoading ? (
					<Loader />
				) : usersAndPermissionsData.rolesBySpecificUser.length > 0 ? (
					<RFlex styleProps={{ flexWrap: "wrap" }}>
						{usersAndPermissionsData.rolesBySpecificUser.map((role) => (
							<RRole role={role} handleRemove={handleRemove} lister={false} />
						))}
					</RFlex>
				) : (
					<REmptyData />
				)}
			</div>
		</React.Fragment>
	);
};

export default GRolesByUser;
