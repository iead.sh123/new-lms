import React from "react";
import Slider from "./Slider";
const RGridSlider = ({
  children,
  rowb,
  rowe,
  colb,
  cole,
  overflowX,
  height,
  additionalStyle,
  additionalClasses,
}) => {
  let width = 100 / children.length;
  return (
    <div
      style={Object.assign({}, additionalStyle, {
        display: "inline-block",
        gridColumnStart: colb,
        gridColumnEnd: cole,
        gridRowStart: rowb,
        gridRowEnd: rowe,
        backgroundColor: "white",
        borderRadius: "20px",
        overflow: "hidden",
        height: height,
      })}
      className={additionalClasses}
    >
      <Slider>{children}</Slider>
    </div>
  );
};
export default RGridSlider;
