import * as yup from "yup";

export const validationLessonPlanSchema = yup.object().shape({
	name: yup.string().required("Name is required"),
	// lesson_nb: yup
	//   .string()
	//   .min(1, "Minimum is a number 1")
	//   .max(100, "Maximum is a number 100")
	//   .typeError("Lesson plan is required")
	//   .required("Lesson plan is required"),
	// unit_nb: yup
	//   .number()
	//   .typeError("Unit number is required")
	//   .required("Unit number is required"),

	lesson_plan_items: yup
		.array()
		.of(
			yup.object().shape({
				subject: yup.string().required("Subject is required"),
				// time: yup.string().required("Time is required"),
				// description: yup.string().required("Description is required"),
			})
		)
		.min(1, "At least 1 item is required"),
});
// age: yup.number().positive().integer().required(),
