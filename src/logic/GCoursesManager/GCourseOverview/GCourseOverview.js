import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import {
	courseOverviewByIdAsync,
	courseByIdAsync,
	setAddItemsToOverviewFields,
	changeItemsInCourseOverView,
	deleteItemsInCourseOverView,
	saveCourseOverViewAsync,
	categoryAncestorsAsync,
	addFeedbackAsync,
} from "store/actions/global/coursesManager.action";
import Loader from "utils/Loader";
import RCourseOverView from "view/CoursesManager/RCourseOverView/RCourseOverView";

const GCourseOverview = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { categoryId, courseId } = useParams();

	const { courseById, courseByIdLoading } = useSelector((state) => state.coursesManagerRed);

	useEffect(() => {
		if (courseId) {
			dispatch(courseByIdAsync(courseId));
			dispatch(addFeedbackAsync(courseId));
		}
	}, [courseId]);

	useEffect(() => {
		if (categoryId) {
			dispatch(categoryAncestorsAsync(categoryId));
		}
	}, [categoryId]);

	const handelSetItemsToCourseOverView = ({ key }) => {
		dispatch(setAddItemsToOverviewFields(key));
	};
	const handelChangeItemsInCourseOverView = ({ name, value, id, key }) => {
		dispatch(changeItemsInCourseOverView(name, value, id, key));
	};
	const handelDeleteItemsInCourseOverView = ({ id, key }) => {
		dispatch(deleteItemsInCourseOverView(id, key));
	};

	const handleSaveCourseOverView = () => {
		dispatch(saveCourseOverViewAsync(courseId, courseById?.overview, history));
	};

	return (
		<div>
			{courseByIdLoading ? (
				<Loader />
			) : (
				<RCourseOverView
					handelSetItemsToCourseOverView={handelSetItemsToCourseOverView}
					handelChangeItemsInCourseOverView={handelChangeItemsInCourseOverView}
					handelDeleteItemsInCourseOverView={handelDeleteItemsInCourseOverView}
					handleSaveCourseOverView={handleSaveCourseOverView}
				/>
			)}
		</div>
	);
};

export default GCourseOverview;
