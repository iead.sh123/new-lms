import React from "react";
import RButton from "components/Global/RComs/RButton";
import styles from "./RLessonPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RLessonPlanHeader = ({ previewMode, setPreviewMode, lessonPlan }) => {
	return (
		<RFlex styleProps={{ justifyContent: "space-between", alignItems: "center" }}>
			{!previewMode && <RButton text={tr`pick_a_lesson_plan`} color="primary" />}
			<RFlex styleProps={{ alignItems: "center", gap: "20px" }}>
				{!previewMode && (
					<h6 className={styles.icon_text} style={{ color: lessonPlan?.published ? "#19AF00" : "#dd0000" }}>
						{lessonPlan?.published ? tr`publish` : tr`draft`}
					</h6>
				)}
				{lessonPlan?.id && (
					<RButton
						text={!previewMode ? tr`preview` : tr`back_to_editor`}
						color="primary"
						faicon={!previewMode ? "fa fa-eye" : "fa fa-pencil"}
						outline
						onClick={() => setPreviewMode(!previewMode)}
					/>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RLessonPlanHeader;
