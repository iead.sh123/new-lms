import RButton from "components/Global/RComs/RButton";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../usersManagementStyle.module.scss";
import React, { useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import * as colors from "config/constants";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
const RHeader = ({
	createText,
	searchPlaceholder,
	actions = [],
	dataType,
	setBackSearchTerm,
	refetchFunction,
	isFetching,
	onCreateClick,
	showColumnSettings = false,
	showSearchHeader = true,
	showCreate = true,
	showRefresh = true,
	showBulk = true,
	showDeactivate = false,
	showCreateGroup = false,
	showAddToGroup = false,
	showSendEmail = false,
	showDelete = false,
	showImportCSV = false,
	showCreateAccount = false,
	deleteText = "Delete",
	showDetails = true,
	deactivateFn,
	createGroupFn,
	addToGroupFn,
	sendEmailFn,
	deleteFn,
	importCSVFn,
	createAccountFn,
	columnSettingsFn,
	deactivateLoading,
	deleteLoading,
	createGroupLoading,
	addToGroupLoading,
	sendEmailLoading,
	disableCreate = false,
	loadingCreate = false,
	addSearchToFilters = false,
	setAllMembersFilters,
	allMembersFilters,
	downloadTemplateFn,
}) => {
	const [frontSearchTerm, setFrontSearchTerm] = useState("");
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
		if (addSearchToFilters) {
			setAllMembersFilters({ ...allMembersFilters, searchText: data });
		}
	};
	const dropDownComponent = (
		<RButton text={tr(`Bulk_Operations`)} color="primary" outline faicon={iconsFa6.chevronDown} iconRight className="m-0" />
	);

	return (
		<RFlex className="flex-column" id="header" styleProps={{ width: "100%" }}>
			<RFlex className="justify-content-between" styleProps={{ width: "100%" }} id="Top">
				<RFlex id="Top Left" styleProps={{ gap: "15px" }}>
					{showCreate && (
						<RButton
							text={tr(`${createText}`)}
							color="primary"
							onClick={() => {
								onCreateClick();
							}}
							className="m-0"
							disabled={disableCreate}
							loading={loadingCreate}
						/>
					)}
					{showBulk && (
						<RDropdownIcon
							component={dropDownComponent}
							actions={actions}
							menuClassname={styles.BulkOperationMenu}
							itemStyle={{ padding: "7px 10px" }}
						/>
					)}
					{showImportCSV && (
						<RButton
							text={tr(`import_csv`)}
							color="primary"
							outline
							onClick={() => {
								importCSVFn();
							}}
							className="m-0"
						/>
					)}
					{showRefresh && (
						<RButton
							faicon={iconsFa6.rotateRight}
							color="primary"
							outline
							className="m-0"
							loading={isFetching}
							onClick={refetchFunction}
							text={tr("Refresh")}
						/>
					)}
					{showCreateAccount && <RButton color="primary" outline className="m-0" onClick={createAccountFn} text={tr("create_account")} />}
					{showColumnSettings && (
						<RButton text={tr(`Column_settings`)} color="primary" outline className="m-0" onClick={columnSettingsFn} />
					)}
					{showDeactivate && (
						<RButton
							text={tr("Deactivate")}
							color="link"
							className="text-primary m-0"
							onClick={() => deactivateFn()}
							loading={deactivateLoading}
							disabled={deactivateLoading}
						/>
					)}
					{showCreateGroup && (
						<RButton
							text={tr("Create_user_group")}
							color="link"
							className="text-primary m-0"
							onClick={() => createGroupFn()}
							loading={createGroupLoading}
						/>
					)}
					{showAddToGroup && (
						<RButton
							text={tr("add_to_group")}
							color="link"
							className="text-primary m-0"
							onClick={() => addToGroupFn()}
							loading={addToGroupLoading}
						/>
					)}
					{showSendEmail && (
						<RButton
							text={tr("send_notification_email")}
							color="link"
							className="text-primary m-0"
							onClick={() => sendEmailFn()}
							loading={sendEmailLoading}
						/>
					)}
					{showDelete && (
						<RButton
							text={tr(`${deleteText}`)}
							color="link"
							className="m-0"
							onClick={() => deleteFn()}
							loading={deleteLoading}
							disabled={deleteLoading}
							faicon={iconsFa6.delete}
							style={{ color: colors.dangerColor }}
						/>
					)}
				</RFlex>
				{showDetails && (
					<RFlex id="Top right" className="align-items-center">
						<i className={iconsFa6.info} style={{ color: colors.boldGreyColor }} />
						<span className="m-0 p-0" style={{ color: colors.boldGreyColor }}>
							{tr("Details")}
						</span>
					</RFlex>
				)}
			</RFlex>
			{showSearchHeader && (
				<RSearchHeader
					searchLoading={isFetching}
					inputPlaceholder={tr(searchPlaceholder)}
					searchData={frontSearchTerm}
					handleChangeSearch={setFrontSearchTerm}
					setSearchData={setFrontSearchTerm}
					handleSearch={handleSearch}
				/>
			)}
		</RFlex>
	);
};

export default RHeader;
