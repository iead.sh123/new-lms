import { pages } from "../constants/pages.constant";
import React from "react";

export const pageMapping = {
	[pages.ACADEMIC_YEARS]: React.lazy(() => import("./AcademicYears/AcademicYears")),
	[pages.TERMS]: React.lazy(() => import("./Terms/Terms")),
	[pages.MANAGE_TERMS]: React.lazy(() => import("./ManageTerms/ManageTerms")),
	[pages.HOME_ROOMS]: React.lazy(() => import("./HomeRooms/HomeRooms")),
	[pages.CURRICULA]: React.lazy(() => import("./Curricula/Curricula")),
	[pages.WEEKLY_SCHEDULE]: React.lazy(() => import("./WeeklySchedule/WeeklySchedule")),
	[pages.HOME_ROOM_EDITOR]: React.lazy(() => import("./HomeRoomAndCurriculaEditor/HomeRoomsAndCurriculaEditor")),
	[pages.CURRICULA_EDITOR]: React.lazy(() => import("./HomeRoomAndCurriculaEditor/HomeRoomsAndCurriculaEditor")),
};
