import { lessonSupplementApi } from "api/teacher/lessonSupplement";
import { GET_LESSON_SUPPLEMENT_REQUEST, GET_LESSON_SUPPLEMENT, GET_LESSON_SUPPLEMENT_ERROR, GET_SUPPLEMENT_RECORD } from "./teacherTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const fetchLessonSupplement = (lessonId) => async (dispatch) => {
	dispatch({
		type: GET_LESSON_SUPPLEMENT_REQUEST,
	});
	try {
		const response = await lessonSupplementApi.getLessonSupplement(lessonId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_LESSON_SUPPLEMENT,
				payload: response.data.data.data,
			});
		} else {
			dispatch({
				type: GET_LESSON_SUPPLEMENT_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_LESSON_SUPPLEMENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchSupplementRecord = (lessonId) => async (dispatch) => {
	try {
		const response = await lessonSupplementApi.getSupplementRecord(lessonId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_SUPPLEMENT_RECORD,
				payload: response.data.data,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};
