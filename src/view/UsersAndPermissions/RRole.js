import React, { useState, useTransition } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./UsersAndPermissions.module.scss";
import { Label } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { successColor } from "config/constants";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { dangerColor } from "config/constants";
import { RLabel } from "components/Global/RComs/RLabel";
import iconsFa6 from "variables/iconsFa6";

const RRole = ({ role, handleRemove, lister }) => {
	const history = useHistory();
	const [isHovered, setIsHovered] = useState(false);
	return (
		<section className={styles.role} key={role.id} onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<h6 className="text-left">{role.name}</h6>
				{isHovered && !lister && (
					<i onClick={() => handleRemove(role.id)} className={iconsFa6.delete} style={{ color: dangerColor, cursor: "pointer" }} />
				)}
			</RFlex>

			<RFlex>
				<Label className="text-muted">{tr`description`}</Label>
				<RLabel value={tr(role.description)} icon={`fa fa-info-circle text-muted`} lettersToShow={0} />
			</RFlex>
			<RFlex>
				<span>
					{role.number_of_permission} {tr`permissions`}
				</span>
				<span style={{ color: role.status ? successColor : primaryColor }}>{role.status ? tr`active` : tr`inactive`}</span>
				<i
					className={`fa fa-chevron-right cursor-pointer ${styles.arrow}`}
					onClick={() => history.push(`${baseURL}/${genericPath}/users-and-permissions/role/${role.id}/properties`)}
				/>
			</RFlex>
		</section>
	);
};

export default RRole;
