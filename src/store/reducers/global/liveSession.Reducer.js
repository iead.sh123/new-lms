import {
  START_LIVE_SESSION_REQUEST,
  START_LIVE_SESSION_SUCCESS,
  START_LIVE_SESSION_ERROR,
  END_LIVE_SESSION_REQUEST,
  END_LIVE_SESSION_SUCCESS,
  END_LIVE_SESSION_ERROR,
  JOIN_MEETING_TO_GET_ATTEND_LINK_REQUEST,
  JOIN_MEETING_TO_GET_ATTEND_LINK_SUCCESS,
  JOIN_MEETING_TO_GET_ATTEND_LINK_ERROR,
} from "store/actions/global/globalTypes";
import produce from "immer";

const initState = {
  startLiveSessionLoading: false,
  endLiveSessionLoading: false,
  joinLiveSessionLoading: false,
};

const liveSessionReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case START_LIVE_SESSION_REQUEST:
        draft.startLiveSessionLoading = true;
        return draft;
      case START_LIVE_SESSION_SUCCESS:
        draft.startLiveSessionLoading = false;
        return draft;
      case START_LIVE_SESSION_ERROR:
        draft.startLiveSessionLoading = false;
        return draft;
      case END_LIVE_SESSION_REQUEST:
        draft.endLiveSessionLoading = true;
        return draft;
      case END_LIVE_SESSION_SUCCESS:
        draft.endLiveSessionLoading = false;
        return draft;
      case END_LIVE_SESSION_ERROR:
        draft.endLiveSessionLoading = false;
        return draft;
      case JOIN_MEETING_TO_GET_ATTEND_LINK_REQUEST:
        draft.joinLiveSessionLoading = true;
        return draft;
      case JOIN_MEETING_TO_GET_ATTEND_LINK_SUCCESS:
        draft.joinLiveSessionLoading = false;
        return draft;
      case JOIN_MEETING_TO_GET_ATTEND_LINK_ERROR:
        draft.joinLiveSessionLoading = false;
        return draft;

      default:
        return draft;
    }
  });

export default liveSessionReducer;
