import React, { useDebugValue, useEffect, useState } from "react";
import "./chat.css";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import { get } from "config/api";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import RForm from "components/Global/RComs/RForm";
import { useRef } from "react";
const CreateGroup=()=>{
  const [open,setOpen]=useState(true);


  const initialformItems = [
    {
      type: "user-autocomplete",
      label: tr`first_name`,
      name: "users",
      labelInfo:"label info label info label info"
    },
    {
      type: "text",
      label: tr`group name`,
      name: "group name",
      labelInfo:"label info label info label info"
    },
    {
      type: "textarea",
      label: tr`description`,
      name: "description",
      labelInfo:"label info label info label info"
    },

    
  ];
  const formRef = useRef();
  const submitForm=()=>{if(formRef.current)
    {
      formRef.current.dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }))
    }
    }
   return <div>
  <button onClick={()=>{setOpen(true)}}></button>
  <button onClick={()=>{setOpen(false)}}></button>
   <RModal
        isOpen={open}
        toggle={() => setOpen(!open)}
        header={<h6 className="text-capitalize">Details</h6>}
        footer={
        <div> 
         <RButton text={tr`Create Group`} onClick={submitForm()}
          color="primary"
          
        /> 
          <RButton text={tr`cancel`} onClick={() => {
         setOpen(false)
          }}
          color="white"
          style={{color:"blue",background:"white",border:"0px solid"}}
          
        /> 
        </div>
      }
        body={
      <div><><RForm
      formItems={initialformItems}
      // submitLabel={"Update Profile"}
      withSubmit={false}
      onSubmit={(e)=>{
        Helper.cl(e,"rf2");
      alert("submit");}}
      formRef={formRef}
    />
    </></div>}
        //  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
        size="lg"
    />
    </div>
}

export default withDirection(CreateGroup);


