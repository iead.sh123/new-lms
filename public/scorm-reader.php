<?php

$actual_link = /*'http://localhost:5000/scorm-reader.php/http://192.168.1.71:8020/storage/47b8661f-d40d-4210-90f6-05466bbca36b/shared/launchpage.html'*/ $_SERVER['REQUEST_URI'];;

$filelink = substr($actual_link, strpos($actual_link, "scorm-reader.php/") + strlen("scorm-reader.php/"));

$filePath = $filelink;

$proxy_path = strstr($actual_link, 'scorm-reader.php', true) . 'scorm-reader.php/';

$remote_path = $proxy_path . substr($filePath, 0, strrpos($filePath, '/'));

ini_set('memory_limit', '256M');

ob_end_flush(); // Disable output buffering
error_reporting(E_ALL);
ini_set('display_errors', 1);

set_time_limit(0);
// Initialize cURL session


$ch = curl_init($filePath);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Set options
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


// Execute cURL session
$html_content = curl_exec($ch);

// Get the 'Content-Length' header from the response
$file_size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

// Get the MIME type
$mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

// Close cURL session
curl_close($ch);



if ($file_size !== false && $mime_type !== false) {
    // Set headers
    header("Content-Type: $mime_type");
    header("Content-Length: $file_size");



    if ($html_content === false) {
        die('Failed to fetch HTML content.');
    }



    echo($html_content);




} else {
    // Unable to get file details
    header("HTTP/1.0 500 Internal Server Error");
}
