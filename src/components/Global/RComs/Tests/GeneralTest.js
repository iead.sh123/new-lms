import { useState } from "react";
import Helper from "../Helper";
import RPlus from "../Rplus ";
import RInlineTextArea from "../RInlineTextArea";
import RInlineTextbox from "../RInlineTextbox";
import RRoundImageEdit from "../RRoundImageEdit";


export const GeneralTest = ()=>{
  

  const [images,setImages]=useState([{
    "hash_id": "qkkgodzjmeraezqblpvqynwxm",
    "extension": "jpg",
    "mime_type": "image/jpeg",
    "name": "4.jpg"
  }]) ;

//--------------------------plus and inline

const [text,setText]=useState();

const label="hey";
  return <div>
    <RRoundImageEdit images={images} setImages={setImages} ></RRoundImageEdit>
    <RPlus/>
    <RInlineTextArea text={text} setText={setText} label={label}/>
    <RInlineTextbox text={text} setText={setText} label={label}/>
  </div>;
};

