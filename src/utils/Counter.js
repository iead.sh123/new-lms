import React, { useEffect } from "react";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const Counter = ({ isRunning, time, setTime }) => {
  useEffect(() => {
    let interval;

    if (isRunning) {
      interval = setInterval(() => {
        setTime((prevTime) => {
          const newSeconds = prevTime.seconds + 1;
          const newMinutes = prevTime.minutes + Math.floor(newSeconds / 60);
          const newHours = prevTime.hours + Math.floor(newMinutes / 60);

          return {
            hours: newHours,
            minutes: newMinutes % 60,
            seconds: newSeconds % 60,
          };
        });
        return () => {
          clearInterval(interval);
        };
      }, 1000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [isRunning]);

  return (
    <RFlex>
      <i className="mt-1 fa-solid fa-stopwatch"></i>
      <div>
        <span>{time.hours < 10 ? `0${time.hours}` : time.hours}</span>:
        <span>{time.minutes < 10 ? `0${time.minutes}` : time.minutes}</span>:
        <span>{time.seconds < 10 ? `0${time.seconds}` : time.seconds}</span>
      </div>
      <p>{tr`has_passed`}</p>
    </RFlex>
  );
};

export default Counter;
