import React, { useEffect, useContext } from "react";
import { getSemestersByOrganizationAsync } from "store/actions/global/schoolInitiation.actions";
import { baseURL, genericPath } from "engine/config";
import { useDispatch, useSelector } from "react-redux";
import { SchoolInitiationContext } from "./GSchoolInitiation";
import { useParams } from "react-router-dom";
import GSchoolInitiationListers from "./GSchoolInitiationListers";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import GGradeLevelForm from "./Modals/GGradeLevelForm";
import GEducationStage from "./Modals/GEducationStage";
import GCurriculaForm from "./Modals/GCurriculaForm";
import GUsersLister from "./Users/GUsersLister";
import GBooksLister from "./Books/GBooksLister";
import tr from "components/Global/RComs/RTranslator";

const GSchoolInitiationTabs = () => {
  const dispatch = useDispatch();
  const { sIType, sITypeId, tabTitle } = useParams();
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  useEffect(() => {
    if (sIType == "grade_levels") {
      dispatch(getSemestersByOrganizationAsync(true));
    }
  }, []);

  const EducationStageTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/properties`,
      content: () => {
        return <GEducationStage />;
      },
    },
    {
      name: "grade_levels",
      title: "Grade Levels",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/grade_levels`,
      content: () => {
        return <GSchoolInitiationListers />;
        // return <UserManagementLister />;
      },
    },
    {
      name: "principal",
      title: "Principal",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/principal`,
      content: () => {
        return <GUsersLister />;
      },
    },
  ];

  const GradeLevelTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/properties`,
      content: () => {
        return <GGradeLevelForm />;
      },
    },
    {
      name: "student",
      title: "Students",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/student`,
      content: () => {
        return <GUsersLister />;
      },
    },
    {
      name: "curricula",
      title: "Curricula",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/curricula`,
      type: "collapse",
      data: SchoolInitiationData?.semesters,
      loading: SchoolInitiationData?.semestersLoading,
      content: () => {
        return <GSchoolInitiationListers />;
      },
    },
  ];

  const CurriculaTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/properties`,
      content: () => {
        return <GCurriculaForm />;
      },
    },
    {
      name: "course_books",
      title: "Course Books",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/course_books`,
      content: () => {
        return <GBooksLister />;
      },
    },
    {
      name: "senior_teacher",
      title: "Senior Teacher",
      url: `${baseURL}/${genericPath}/school-initiation/${sIType}/${sITypeId}/senior_teacher`,
      content: () => {
        return <GUsersLister />;
      },
    },
  ];

  return (
    <RTabsPanel_Vertical
      title={`${SchoolInitiationData?.namePartOfSchool?.label} (${tr(
        sIType
      )}) Editor`}
      Tabs={
        sIType == "education_stage"
          ? EducationStageTabs
          : sIType == "grade_levels"
          ? GradeLevelTabs
          : CurriculaTabs
      }
    />
  );
};

export default GSchoolInitiationTabs;
