import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import {
	learnerContextCreatorAsync,
	questionsBanksAsync,
	setFiltersData,
	createNewQuestionGroup,
	initiateQuestionSetFormate,
	createNewQuestionsToQuestionGroup,
	emptyQuestionsFromQuestionSet,
	fillDataToQuestionType,
	removeQuestionGroup,
	createQuestionToQuestionsBank,
	addDataToQuestionsGroup,
	fillDataInQuestionSetToEdit,
	updateQuestionsBankAsync,
	addChoicesToQuestion,
	fillDataInChoicesAnswers,
	fillDataInCorrectAnswerToSomeQuestions,
	removeChoiceFromQuestion,
	addPairsToMatchQuestion,
	removeQuestionsBankAsync,
} from "store/actions/global/questionSet.actions";
import RQuestionsBank from "view/Courses/CourseManagement/QuestionSet/QuestionsBank";
import AppModal from "components/Global/ModalCustomize/AppModal";
import styles from "./QuestionSet.module.scss";
import UseImg from "assets/img/new/Hands_Pinches.png";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import { useLocation } from "react-router-dom";
import { exportQuestionBankToQuestionEditor } from "store/actions/global/questionSet.actions";
import RQuestionTabsViewer from "view/Courses/CourseManagement/QuestionSet/QuestionsBank/QuestionViewer";
import RQuestionBankEditor from "view/Courses/CourseManagement/QuestionSet/QuestionsBank/QuestionEditor/RQuestionBankEditor";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";

export const QuestionsBankContext = React.createContext();
const GQuestionsBank = () => {
	// #PS :  For Question Bank Editor i reuse create question group actions in question bank editor ,
	// same functionality between tow editor but in question bank editor we need to send first object from group
	// That's why you see you've been called questionSet on this page

	const Tabs = [{ title: `question` }, { title: `details` }];
	const location = useLocation();
	const history = useHistory();
	const dispatch = useDispatch();
	const [open, setOpen] = useState(false);
	const [openViewQuestion, setOpenViewQuestion] = useState(false);
	const [openQuestionEditor, setOpenQuestionEditor] = useState(false);
	const [questionsBankSelected, setQuestionsBankSelected] = useState(null);
	const [tabActive, setTabActive] = useState(Tabs[0]?.title);
	const [alert, setAlert] = useState(false);

	const isGroup = location?.state?.isGroup;

	const questionTypesEnum = [
		{
			value: "MULTIPLE_CHOICES_MULTIPLE_ANSWERS",
			label: tr("multiple_choice_(MCQ)"),
			icon: "fas fa-tasks pt-1",
		},
		{
			value: "MULTIPLE_CHOICES_SINGLE_ANSWER",
			label: tr("single_answer_(SCQ)"),
			icon: "fas fa-tasks pt-1",
		},
		{
			value: "ESSAY",
			label: "essay",
			icon: "fas fa-align-left pt-1",
		},
		{
			value: "MATCH",
			label: "match",
			icon: "fas fa-grip-lines-vertical pt-1",
		},
		{
			value: "DRAW",
			label: "draw",
			icon: "fa-solid fa-pen pt-1",
		},

		{
			value: "UPLOAD_FILE",
			label: tr("upload_file"),
			icon: "fa-regular fa-file pt-1",
		},
		// {
		// 	value: "FILL_IN",
		// 	label: "fill_in_blank",
		// 	icon: "fa-solid fa-pen-to-square pt-1",
		// },
		{
			value: "TRUEFALSE",
			label: "true / false",
			icon: "fa-solid fa-clipboard-check pt-1",
		},
		{
			value: "SEQUENCING",
			label: "ordering",
			icon: "fa-solid fa-arrow-down-1-9 pt-1",
		},
	];

	const { courseId, cohortId, curriculumId, categoryId, questionSetType } = useParams();

	const {
		questionsBank,
		questionsBankLoading,
		questionsBankFilters,
		learnerContextCreator,
		questionSet,
		questionsBankPagination,
		learnerContextCreatorLoading,
		createQuestionToQuestionsBankLoading,
		removeQuestionsBankLoading,
	} = useSelector((state) => state.questionSetRed);

	const ChooseFromQuestionBank = location.pathname.includes("choose-questions-bank");

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const successDelete = async (prameters) => {
		dispatch(removeQuestionsBankAsync(prameters.questionBankId, hideAlert));
	};

	const handleRemoveQuestionBank = (questionBankId) => {
		const prameters = {
			questionBankId,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleOpenModal = (question) => {
		setOpen(true);
		setQuestionsBankSelected(question);
	};
	const handleCloseModal = () => {
		setOpen(false);
		setQuestionsBankSelected(null);
	};

	const handleOpenQuestionModal = (question) => {
		setOpenViewQuestion(true);
		setQuestionsBankSelected(question);
	};
	const handleCloseQuestionModal = () => {
		setOpenViewQuestion(false);
		setQuestionsBankSelected(null);
		setTabActive("question");
	};

	const handlePushToQsEditor = () => {
		history.goBack();
	};

	useEffect(() => {
		dispatch(
			questionsBanksAsync(null, courseId ? { courseId: +courseId } : { curriculumId: +curriculumId }, ChooseFromQuestionBank ? false : true)
		);
	}, []);

	useEffect(() => {
		dispatch(learnerContextCreatorAsync());
		if (courseId) {
			dispatch(setFiltersData("courseId", courseId));
		} else {
			dispatch(setFiltersData("curriculumId", curriculumId));
		}
		if (location?.state?.isGroup) {
			dispatch(
				setFiltersData(
					"questionTypes",
					questionTypesEnum.map((questionType) => questionType.value)
				)
			);
		}
	}, [location?.state?.isGroup]);

	useEffect(() => {
		if (!location?.state?.isGroup) {
			questionTypesEnum.push({
				value: "GROUP",
				label: "questions_group",
			});
		}
	}, [questionTypesEnum]);

	const handleSetFilters = (value, key) => {
		dispatch(setFiltersData(key, value));
	};

	const handleResetAllFilters = () => {
		dispatch(
			questionsBanksAsync(null, courseId ? { courseId: +courseId } : { curriculumId: +curriculumId }, ChooseFromQuestionBank ? false : true)
		);
	};

	const handleFilter = (emptySearch) => {
		dispatch(
			questionsBanksAsync(
				null,
				{
					...questionsBankFilters,
					searchText: emptySearch == "" ? emptySearch : questionsBankFilters.searchText,
				},
				ChooseFromQuestionBank ? false : true
			)
		);
	};

	const handleExportQuestionFromQuestionBankToQuestionEditor = () => {
		dispatch(
			exportQuestionBankToQuestionEditor(
				location?.state?.isGroup,
				location.state.groupId,
				location.state.questionSetIndex,
				questionsBankSelected
			)
		);
		history.goBack();
		setQuestionsBankSelected(null);
		setTabActive("question");
	};
	useEffect(() => {
		if (questionSet?.groups?.length > 0) {
		} else {
			dispatch(initiateQuestionSetFormate(courseId, curriculumId));
		}
	}, []);

	const handleOpenQuestionEditor = (question) => {
		setOpenQuestionEditor(true);
		dispatch(createNewQuestionGroup(null, question));
	};
	const handleCloseQuestionEditor = () => {
		setOpenQuestionEditor(false);
		dispatch(emptyQuestionsFromQuestionSet());
	};

	const handleOpeModalToEdit = (question) => {
		setOpenQuestionEditor(true);
		dispatch(fillDataInQuestionSetToEdit(question));
	};

	const handleFillDataToQuestionType = ({ questionGroupId, questionTypeId, key, value }) => {
		dispatch(fillDataToQuestionType(questionGroupId, questionTypeId, key, value));
	};

	const handleRemoveQuestionGroup = ({ questionGroupId, questionTypeId, isGroup }) => {
		dispatch(removeQuestionGroup(questionGroupId, questionTypeId, isGroup));
	};

	const handleChangeQuestionsGroup = ({ questionGroupId, key, value }) => {
		dispatch(addDataToQuestionsGroup(questionGroupId, key, value));
	};

	const handleCreateNewQuestionsToQuestionGroup = (data, questionSetIndex, groupId) => {
		dispatch(createNewQuestionsToQuestionGroup(data, questionSetIndex, groupId));
	};

	const handleAddChoicesToQuestion = ({ questionGroupId, questionTypeId, choicesLength, questionType }) => {
		dispatch(addChoicesToQuestion(questionGroupId, questionTypeId, choicesLength, questionType));
	};

	const handleFillDataInChoicesAnswers = ({ questionGroupId, questionTypeId, choiceId, key, value, questionType }) => {
		dispatch(fillDataInChoicesAnswers(questionGroupId, questionTypeId, choiceId, key, value, questionType));
	};

	const handleFillDataInCorrectAnswerToSomeQuestions = ({ questionGroupId, questionTypeId, questionType, value }) => {
		dispatch(fillDataInCorrectAnswerToSomeQuestions(questionGroupId, questionTypeId, questionType, value));
	};

	const handleRemoveChoiceFromQuestion = ({ questionGroupId, questionTypeId, choiceId, questionType, choiceOrder }) => {
		dispatch(removeChoiceFromQuestion(questionGroupId, questionTypeId, choiceId, questionType, choiceOrder));
	};

	const handleAddPairsToMatchQuestion = ({ questionGroupId, questionTypeId, parisLength }) => {
		dispatch(addPairsToMatchQuestion(questionGroupId, questionTypeId, parisLength));
	};

	const handleCreateQuestionToQuestionBank = () => {
		dispatch(
			createQuestionToQuestionsBank(
				courseId ? "course" : "curriculum",
				courseId ? courseId : curriculumId,
				questionSet.groups[0],
				handleCloseQuestionEditor
			)
		);
	};

	const handleUpdateQuestionBank = () => {
		dispatch(updateQuestionsBankAsync(questionSet.groups[0].id, questionSet.groups[0], handleCloseQuestionEditor));
	};

	return (
		<div>
			<QuestionsBankContext.Provider
				value={{
					questionsBank,
					questionSetType,
					questionTypesEnum,
					questionsBankFilters,
					learnerContextCreator,
					questionsBankSelected,
					isGroup,
					Tabs,
					questionSet,
					ChooseFromQuestionBank,

					handleResetAllFilters,
					handlePushToQsEditor,
					handleSetFilters,
					handleOpenModal,
					handleFilter,
					handleOpenQuestionModal,
					handleOpenQuestionEditor,
					handleOpeModalToEdit,
					handleRemoveQuestionBank,

					handleFillDataToQuestionType,
					handleRemoveQuestionGroup,
					handleChangeQuestionsGroup,
					handleCreateNewQuestionsToQuestionGroup,
					handleAddChoicesToQuestion,
					handleFillDataInChoicesAnswers,
					handleFillDataInCorrectAnswerToSomeQuestions,
					handleRemoveChoiceFromQuestion,
					handleAddPairsToMatchQuestion,

					tabActive,
					setTabActive,

					learnerContextCreatorLoading,
					questionsBankLoading,
				}}
			>
				{alert}
				<AppModal
					size={"md"}
					show={open}
					parentHandleClose={handleCloseModal}
					headerSort={
						<section className={styles.export__question__bank__container}>
							<section className={styles.export__question__bank}>
								<img src={UseImg} alt="image" />
								<h6>{tr`use “What are you thinking about the climate change” question ?`}</h6>
								<RFlex>
									<RButton color="primary" outline text={tr`cancel`} onClick={handleCloseModal} />
									<RButton color="primary" text={tr`use`} onClick={handleExportQuestionFromQuestionBankToQuestionEditor} />
								</RFlex>
							</section>
						</section>
					}
				/>
				<AppModal
					size={"lg"}
					show={openViewQuestion}
					parentHandleClose={handleCloseQuestionModal}
					headerSort={
						<React.Fragment>
							<RQuestionTabsViewer />
						</React.Fragment>
					}
				/>
				<AppModal
					size={"xl"}
					show={openQuestionEditor}
					parentHandleClose={handleCloseQuestionEditor}
					headerSort={
						<React.Fragment>
							<section>
								<h6 className="mb-4">{tr`create_new_question`}</h6>
								<RQuestionBankEditor />
								<RFlex styleProps={{ justifyContent: "flex-end" }} className="mt-4">
									<RButton
										color="primary"
										text={tr`save`}
										onClick={() => (questionSet.groups[0].id ? handleUpdateQuestionBank() : handleCreateQuestionToQuestionBank())}
										loading={createQuestionToQuestionsBankLoading}
										disabled={createQuestionToQuestionsBankLoading}
									/>
									<RButton
										color="link"
										text={tr`cancel`}
										className={styles.cancel__button}
										onClick={handleCloseQuestionEditor}
										disabled={createQuestionToQuestionsBankLoading}
									/>
								</RFlex>
							</section>
						</React.Fragment>
					}
				/>
				<RQuestionsBank />
				{questionsBankPagination.total > 0 ? (
					<NewPaginator
						firstPageUrl={questionsBankPagination.firstPageUrl}
						lastPageUrl={questionsBankPagination.lastPageUrl}
						currentPage={questionsBankPagination.currentPage}
						lastPage={questionsBankPagination.lastPage}
						prevPageUrl={questionsBankPagination.prevPageUrl}
						nextPageUrl={questionsBankPagination.nextPageUrl}
						total={questionsBankPagination.total}
						data={questionsBankFilters}
						getAction={questionsBanksAsync}
					/>
				) : (
					""
				)}
			</QuestionsBankContext.Provider>
		</div>
	);
};

export default GQuestionsBank;
