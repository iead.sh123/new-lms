import React, { useEffect, useState } from "react";
import { validationLessonPlanSchema } from "./GLessonPlanValidation";
import { useDispatch, useSelector } from "react-redux";
import {
	addDataToLessonPlanItem,
	changeLessonPlanValue,
	addNewLessonPlanItems,
	removeLessonPlanItem,
	clearLessonPlanItem,
	addLessonPlanAsync,
	addValidationToLessonPlan,
	getValidationLessonPlan,
	deleteRubricFromLessonPlanItem,
	getLessonPlanByLessonPlanId,
	createLessonPlanStore,
} from "store/actions/teacher/lessonPlan.actions";
import { getLessonPlans } from "store/actions/teacher/lessonPlan.actions";
import { useParams } from "react-router-dom";
import RLessonPlan from "view/LessonPlan/RLessonPlan";
import GPickRubricLister from "./GPickRubricLister";
import RButton from "components/Global/RComs/RButton";

import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";
import { useHistory } from "react-router-dom";

const GLessonPlan = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { courseId, curriculumId, lessonId, lessonPlanId } = useParams();
	const [previewMode, setPreviewMode] = useState(false);
	const [activeTab, setActiveTab] = useState("Item");
	const [buttonDisable, setButtonDisable] = useState(true);
	const [openPickARubric, setOpenPickARubric] = useState(false);
	const [lessonPlanItemId, setLessonPlanItemId] = useState(null);

	const { lessonPlanLoading, lessonPlan, isEdit, saveLessonPlanLoading, lessonPlanErrors, test, exportLessonPlanAsPdfLoading } =
		useSelector((state) => state.lessonPlan);

	useEffect(() => {
		if (lessonPlanId) {
			dispatch(getLessonPlanByLessonPlanId(lessonPlanId));
		} else {
			dispatch(createLessonPlanStore());
		}
		dispatch(getValidationLessonPlan());
		// dispatch(getLessonPlans(58));
	}, [lessonPlanId]);

	const handelLessonPlanChange = (name, value) => {
		dispatch(changeLessonPlanValue(name, value));

		if (name == "name") {
			yup
				.reach(validationLessonPlanSchema, name)
				.validate(value)
				.then(() => {
					dispatch(addValidationToLessonPlan(name, undefined, undefined));
				})
				.catch((err) => {
					dispatch(addValidationToLessonPlan(name, undefined, err.errors[0]));
				});
		}
	};

	const handelLessonPlanItemChange = (value, name, id) => {
		dispatch(addDataToLessonPlanItem(value, name, id));
		// if (name !== "tags" || name !== "attachments") {
		//   yup
		//     .reach(validationLessonPlanSchema, `lesson_plan_items[].${name}`)
		//     .validate(value)
		//     .then(() => {
		//       dispatch(addValidationToLessonPlan(name, id, undefined));
		//     })
		//     .catch((err) => {
		//       dispatch(addValidationToLessonPlan(name, id, err.errors[0]));
		//     });
		// }
	};

	const handleAddNewLessonPlanItems = (indexAdd) => {
		dispatch(addNewLessonPlanItems(indexAdd));
	};

	const handleRemoveLessonPlanItem = (id) => {
		dispatch(removeLessonPlanItem(id));
	};

	const handleClearLessonPlanItem = (id) => {
		dispatch(clearLessonPlanItem(id));
	};

	const handleSaveAndPublishLessonPlan = () => {
		const publishLessonPlan = { ...lessonPlan, published: true };

		validationLessonPlanSchema
			.validate(publishLessonPlan, { abortEarly: false })
			.then(() => {
				dispatch(
					addLessonPlanAsync(
						lessonId
							? {
									payload: {
										lessonPlan: publishLessonPlan,
										lesson_id: lessonId,
									},
							  }
							: curriculumId
							? {
									payload: {
										lessonPlan: publishLessonPlan,
										curriculum_id: curriculumId,
									},
							  }
							: courseId
							? {
									payload: {
										lessonPlan: publishLessonPlan,
										course_id: courseId,
									},
							  }
							: "",
						lessonPlanId,
						history
					)
				);
			})
			.catch((err) => {
				const validationErrors = {};
				err.inner.forEach((error) => {
					validationErrors[error.path] = error.message;
				});
				dispatch(addValidationToLessonPlan(undefined, undefined, validationErrors));
			});
	};

	useEffect(() => {
		validationLessonPlanSchema
			.isValid(lessonPlan)
			.then((valid) => {
				// `valid` is true if `data` is valid according to the schema, false otherwise

				if (valid) {
					// enable button
					setButtonDisable(valid);
				} else {
					// disable button
					setButtonDisable(valid);
				}
			})
			.catch((err) => {
				// handle errors
			});
	}, [lessonPlan]);

	const handleSaveAsADraftLessonPlan = () => {
		const publishLessonPlan = { ...lessonPlan, published: false };
		dispatch(
			addLessonPlanAsync(
				lessonId
					? {
							payload: {
								lessonPlan: publishLessonPlan,
								lesson_id: lessonId,
							},
					  }
					: curriculumId
					? {
							payload: {
								lessonPlan: publishLessonPlan,
								curriculum_id: curriculumId,
							},
					  }
					: courseId
					? {
							payload: {
								lessonPlan: publishLessonPlan,
								course_id: courseId,
							},
					  }
					: "",
				lessonPlanId,
				history
			)
		);
	};

	const handleOpenPickARubric = () => setOpenPickARubric(true);
	const handleClosePickARubric = () => setOpenPickARubric(false);

	const handleRemoveRubricFromLessonPlanItem = (id) => {
		dispatch(deleteRubricFromLessonPlanItem(id));
	};
	return (
		<>
			{lessonPlanLoading ? (
				<Loader />
			) : (
				<>
					<GPickRubricLister handleClosePickARubric={handleClosePickARubric} openPickARubric={openPickARubric} itemId={lessonPlanItemId} />
					<RLessonPlan
						lessonPlan={lessonPlan}
						lessonPlanErrors={lessonPlanErrors}
						isEdit={isEdit}
						previewMode={previewMode}
						setPreviewMode={setPreviewMode}
						handelLessonPlanChange={handelLessonPlanChange}
						handelLessonPlanItemChange={handelLessonPlanItemChange}
						handleAddNewLessonPlanItems={handleAddNewLessonPlanItems}
						handleRemoveLessonPlanItem={handleRemoveLessonPlanItem}
						handleClearLessonPlanItem={handleClearLessonPlanItem}
						setActiveTab={setActiveTab}
						activeTab={activeTab}
						handleOpenPickARubric={handleOpenPickARubric}
						setLessonPlanItemId={setLessonPlanItemId}
						handleRemoveRubricFromLessonPlanItem={handleRemoveRubricFromLessonPlanItem}
					/>
					{!previewMode && (
						<RFlex styleProps={{ justifyContent: "start" }}>
							<RButton
								text={tr`save_and_publish`}
								onClick={handleSaveAndPublishLessonPlan}
								color="primary"
								disabled={saveLessonPlanLoading || !buttonDisable}
								loading={saveLessonPlanLoading}
							/>
							<RButton
								text={tr`save_and_draft`}
								onClick={handleSaveAsADraftLessonPlan}
								color="primary"
								outline
								className={`draft_button`}
								disabled={saveLessonPlanLoading || !buttonDisable}
								loading={saveLessonPlanLoading}
							/>
						</RFlex>
					)}
				</>
			)}
		</>
	);
};

export default GLessonPlan;
