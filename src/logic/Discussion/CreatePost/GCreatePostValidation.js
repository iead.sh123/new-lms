import * as yup from "yup";

export const validationCreatePostSchema = yup.object().shape({
  text: yup.string().required("Text is required"),
  cohort_id: yup.number().required("Cohort Id is required"),
});

//  const validationCreatePostSchema = yup.object().shape({
//    text: yup.string().required("Text is required"),
//    cohort_id: yup.number().when(cohortId, {
//      is: (cohortId) => cohortId,
//      then: yup.number().required("Cohort Id is required"),
//      otherwise: yup.number().nullable(),
//    }),
//  });
