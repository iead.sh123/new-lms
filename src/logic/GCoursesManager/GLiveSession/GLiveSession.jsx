import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import RLiveSession from "view/CoursesManager/RLiveSession/RLiveSession";
import {
  startLiveSessionAsync,
  endLiveSessionAsync,
} from "store/actions/global/liveSession.actions";
import AppModal from "components/Global/ModalCustomize/AppModal";
import REndLiveSessionModal from "view/CoursesManager/RLiveSession/REndLiveSessionModal";

export const LiveSessionContext = React.createContext();
const GLiveSession = () => {
  const dispatch = useDispatch();
  const { meetingId } = useParams();

  const [isRunning, setIsRunning] = useState(false);
  const [open, setOpen] = useState(false);

  const {
    startLiveSessionLoading,
    endLiveSessionLoading,
    joinLiveSessionLoading,
  } = useSelector((state) => state.liveSessionRed);

  const { user } = useSelector((state) => state.auth);

  const [time, setTime] = useState({ hours: 0, minutes: 0, seconds: 0 });
  const startCounter = () => setIsRunning(true);
  const stopCounter = () => setIsRunning(false);
  const resetCounter = () => {
    setIsRunning(false);
    setTime({ hours: 0, minutes: 0, seconds: 0 });
  };
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);

  const handleStartScheduleLiveSession = () => {
    dispatch(
      startLiveSessionAsync(
        meetingId,
        startCounter,
        handleCloseModal,
        user?.type
      )
    );
  };
  const handleEndScheduleLiveSession = () => {
    dispatch(endLiveSessionAsync(meetingId, resetCounter, handleOpenModal));
  };

  return (
    <React.Fragment>
      <LiveSessionContext.Provider
        value={{
          meetingId,
          isRunning,
          time,

          handleStartScheduleLiveSession,
          handleEndScheduleLiveSession,
          handleCloseModal,

          startCounter,
          stopCounter,
          resetCounter,

          setTime,

          startLiveSessionLoading,
          endLiveSessionLoading,
          joinLiveSessionLoading,
        }}
      >
        <AppModal
          size="lg"
          show={open}
          parentHandleClose={handleCloseModal}
          // header={tr`create_a_lesson`}
          headerSort={<REndLiveSessionModal />}
        />
        <section>
          <RLiveSession />
        </section>
      </LiveSessionContext.Provider>
    </React.Fragment>
  );
};

export default GLiveSession;
