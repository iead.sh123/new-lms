import React, { useState, useEffect, useMemo } from "react";
import { allQuestionSetAsync, publishAndUnPublishQuestionSetAsync, deleteQuestionSetAsync } from "store/actions/global/questionSet.actions";
import { useLocation, useHistory, useParams } from "react-router-dom";
import { initiateQuestionSetFormate } from "store/actions/global/questionSet.actions";
import { useSelector, useDispatch } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { relativeDate } from "utils/dateUtil";
import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import { Row, Col } from "reactstrap";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GQuestionSetLister = () => {
	const { courseId, cohortId, curriculumId, categoryId } = useParams();
	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();
	const segments = location.pathname.split("/");
	const lastWord = segments[segments.length - 1];

	const [searchData, setSearchData] = useState("");
	const [filterData, setFilterData] = useState("");
	const [alert, setAlert] = useState(false);

	const { allQuestionSet, filterTabs, allQuestionSetLoading } = useSelector((state) => state.questionSetRed);

	useEffect(() => {
		dispatch(allQuestionSetAsync(lastWord, courseId, curriculumId));
	}, [lastWord, courseId, curriculumId]);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		dispatch(allQuestionSetAsync(lastWord, courseId, curriculumId, emptySearch !== undefined ? emptySearch : searchData, filterData));
	};

	const handlePushToQuestionSetEditor = () => {
		if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/${lastWord}`);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/${lastWord}`);
		}
		dispatch(initiateQuestionSetFormate(courseId, curriculumId));
	};

	const handlePickFromCollaboration = () => {
		history.push(
			`${baseURL}/${genericPath}/collaboration_area_contents/${
				courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
			}/${lastWord}`
		);
	};

	const handleFilterOnTabs = (tab) => {
		setFilterData(tab);
		dispatch(allQuestionSetAsync(lastWord, courseId, searchData, tab));
	};

	const handlePublishQS = (questionSetId, publish) => {
		dispatch(publishAndUnPublishQuestionSetAsync(lastWord, questionSetId, publish));
	};

	const handleRemoveQuestionSet = (questionSetId) => {
		const prameters = {
			questionSetId,
		};
		const message = tr`Are you sure?`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const successDelete = async (prameters) => {
		dispatch(deleteQuestionSetAsync(lastWord, prameters.questionSetId, hideAlert));
	};

	const handlePushToStudentsSubmittedSolution = (moduleContextId, label) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleContextId}/student/${lastWord}/${
					label == "Submitted Students" ? "submittedStudents" : "gradedStudents"
				}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleContextId}/student/${lastWord}/${
					label == "Submitted Students" ? "submittedStudents" : "gradedStudents"
				}`
			);
		}
	};

	const handlePushToQsEditor = (questionSetId) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/editor/${lastWord}/${questionSetId}`
			);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/${lastWord}/${questionSetId}`);
		}
	};

	const records = useMemo(() => {
		return (
			allQuestionSet &&
			allQuestionSet?.records &&
			allQuestionSet?.records.length > 0 &&
			allQuestionSet?.records?.map((record) => ({
				table_name: "questionSetLister",
				id: record.id,
				details: allQuestionSet?.headers?.map((header) => ({
					key: tr(header.label),
					value:
						header.label == "Status" ? (
							record[header.value] ? (
								"published"
							) : (
								"draft"
							)
						) : header.label == "Date" ? (
							relativeDate(record[header.value])
						) : header.label == "Title" ? (
							record[header.value]
						) : header.label == "Submitted Students" || header.label == "Graded Students" ? (
							<span className="d-flex ">
								<div
									className="cursor-pointer"
									onClick={() => handlePushToStudentsSubmittedSolution(record.module_context_id, header.label)}
								>
									<i className="fa-solid fa-user-group pt-1 pr-1"></i>
									{record[header.value]}
								</div>
							</span>
						) : header.label.toLowerCase() == "description" ? (
							<div
								dangerouslySetInnerHTML={{
									__html: record[header.value]?.length > 30 ? record[header.value].substring(0, 26) + " ..." : record[header.value],
								}}
							/>
						) : (
							record[header.value]
						),
					// gradedStudents
					type: "component",
					color: header.label == "Status" ? (record[header.value] ? successColor : dangerColor) : "",
					dropdown: header.label == "Date" ? true : false,
					dropdownData:
						header.label == "Date"
							? [
									{
										id: 1,
										title: "overdue",
									},
									{ id: 2, title: "dated" },
							  ]
							: [],
				})),
				actions: [
					{
						name: tr("details"),
						icon: "fa fa-edit",
						onClick: () => {
							handlePushToQsEditor(record.id);
						},
					},
					{
						name: tr("log"),
						icon: "fas fa-history",
						onClick: () => {},
					},
					{
						name: record.is_published ? tr("draft") : tr("publish"),
						icon: "fa fa-check",
						color: record.is_published ? dangerColor : successColor,
						onClick: () => {
							handlePublishQS(record.id, record.is_published);
						},
					},
					{
						name: tr("remove"),
						icon: iconsFa6.delete,
						color: "danger",
						onClick: () => {
							handleRemoveQuestionSet(record.id);
						},
					},
				],
			}))
		);
	}, []);

	// Cannot convert undefined or null to object

	return (
		<Row>
			{alert}
			<Col xs={12} md={8} className="mb-2">
				<RSearchHeader
					searchLoading={allQuestionSetLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					addNew={true}
					buttonName={`${tr(`add_new_${lastWord}`)}`}
					outline={false}
					handlePushToAnotherRouteWhenAdd={handlePushToQuestionSetEditor}
					alignItems="flex-end"
					widthInput="300px"
				/>
			</Col>
			<Col xs={12} md={4} className="d-flex justify-content-end mb-2 pr-0 pl-0">
				<RButton
					text={tr`pick_from_collaboration`}
					faicon={iconsFa6.globe}
					color={"primary"}
					outline
					onClick={handlePickFromCollaboration}
				/>
			</Col>
			{!curriculumId && filterTabs && filterTabs.length > 0 && (
				<Col xs={12}>
					<RFilterTabs tabs={filterTabs} handleSearchOnTabs={handleFilterOnTabs} />
				</Col>
			)}
			{allQuestionSetLoading ? (
				<Loader />
			) : (
				<>
					{records?.length > 0 ? (
						<Col xs={12}>
							<RLister Records={records} />
						</Col>
					) : (
						<RFlex styleProps={{ width: "100%", justifyContent: "center" }}>
							<REmptyData />
						</RFlex>
					)}
				</>
			)}
		</Row>
	);
};

export default GQuestionSetLister;
