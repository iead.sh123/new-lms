import { DATE_FORMATE_WITHOUT_TIME } from "config/constants";
import moment from "moment";

const useFormattedDate = (date, format = DATE_FORMATE_WITHOUT_TIME) => {
	const formattedDateForBackend = () => {
		return date ? moment.utc(date).format() : null;
	};

	const formattedDateForDisplay = () => {
		return date ? moment(date).format(format) : "";
	};

	return {
		formattedDateForBackend,
		formattedDateForDisplay,
	};
};

export default useFormattedDate;
