import React from "react";

import tr from "components/Global/RComs/RTranslator";
import { IMApi } from "api/IMNewCycle";
import AsyncSelect from "react-select/async";
import MessageInput from "../chatContent/MessageInput";
import { Button } from "reactstrap";

function ForwardMessage({ handleForward, loggedUserId, cancelForwarding }) {
  const debounceTimer = React.useRef(null);
  const [rc, setRc] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState("");

  // const [GroupImage, setGroupImage] = React.useState(null);

  const onSelectedUsersChange = (value) => setRc(value);

  const debouncedLoadUsers = React.useCallback(
    (delay = 1000) => {
      const loadUsers = async () => {
        const response = await IMApi.searchForwardTo(searchValue);
        if (!response.data.status) return [];

        return [
          ...response.data?.data?.data?.contacts
            ?.filter((usr) => +usr.userId !== +loggedUserId)
            .map((usr) => ({
              label: usr.full_name,
              chatId: usr.chat_id,
              value: usr.userId,
              contact_id: usr.userId,
              rel: usr.label,
            })),
          ...response.data?.data?.data?.groups?.map((gr) => ({
            label: "group : " + gr.name,
            value: gr.id,
            chatId: gr.id,
          })),
        ];
      };

      return new Promise((resolve) => {
        clearTimeout(debounceTimer.current);
        debounceTimer.current = setTimeout(() => {
          resolve(loadUsers());
        }, delay);
      });
    },
    [searchValue]
  );

  return (
    <div>
      <div className={"new_messege_show "}>
        <div className="new_messege_show_head">
          <button className="show_icon" onClick={cancelForwarding}>
            <i className="fa fa-angle-left"></i>
          </button>
          {tr("forward message")}
        </div>
        <div className="RAutocomplete">
          <AsyncSelect
            // isMulti
            onInputChange={(val) => setSearchValue(val)}
            defaultValue={[]}
            loadOptions={() => debouncedLoadUsers(1000)}
            onChange={onSelectedUsersChange}
          />
        </div>
        <Button
          className="ml-4"
          color="primary"
          onClick={() => handleForward(rc.chatId ?? rc.contact_id, !!rc.chatId)}
        >
          {tr`forward`}
        </Button>
      </div>
    </div>
  );
}

export default ForwardMessage;
