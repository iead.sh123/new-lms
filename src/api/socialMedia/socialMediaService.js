import { get, post, put, destroy } from "config/api";

export const socialMediaServiceApi = {
  getAllPost: (pageNumber) =>
    get(`social/QueryAccessibilityPosts?page=${pageNumber}`),

  AccessLevels: (text, id) =>
    get(`social/all/AccessLevelsByUser${id ? `/${text}/${id}` : `/${text}`}`),

  searchPost: (query, pageNumber) =>
    get(`social/posts/search?query=${query}&page=${pageNumber}`),

  specificpost: (urlPostId) =>
    get(`social/posts/getPost?url_post_id=${urlPostId}`),

  filtringTag: (tag, pageNumber) =>
    get(`social/tag/posts?tag_name=${tag}&page=${pageNumber}`),

  postByUser: (UserId, pageNumber) =>
    get(`social/posts/getPostsUserId?user_id=${UserId}&page=${pageNumber}`),

  NewPost: (data) => post("social/me/savePost", data),

  moreComments: (data) => post(`social/getComments`, data),

  moreLikes: (data) => post(`social/fetchnextLikes`, data),

  addComment: (data) => post("social/me/saveComment", data),

  writeLike: (data) => post("social/me/saveLike", data),

  editPost: (FormData, PostId) => put(`social/post/update/${PostId}`, FormData),

  removePost: (PostId) => destroy(`social/post/${PostId}`),

  removeComment: (CommentId) => destroy(`social/me/deleteComment/${CommentId}`),

  editComment: (FormData, CommentId) =>
    put(`social/me/updateComment/${CommentId}`, FormData),
};
