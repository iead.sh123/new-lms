import React from "react";
import styles from "./AllCourses.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { successColor } from "config/constants";

const RCourseCategories = ({ allCourses, handleSearch, categoryIdParams }) => {
  return (
    <React.Fragment>
      {allCourses?.topCategories && (
        <div className={styles.categories}>
          <p className="text-muted">{tr`top_categories`}</p>
          {allCourses?.topCategories.map((tCategory) => (
            <div className={styles.categories__items} key={tCategory.id}>
              <div className="d-flex ">
                {categoryIdParams == tCategory.id && (
                  <i
                    className="fa-solid fa-check pt-1 pr-1"
                    style={{ color: successColor, cursor: "pointer" }}
                    onClick={() => handleSearch()}
                  ></i>
                )}
                <p
                  className="cursor-pointer"
                  style={{
                    color: categoryIdParams == tCategory.id ? "#d00" : "#333",
                  }}
                  onClick={() => handleSearch(tCategory.id)}
                >
                  <span>{tCategory.name}</span>
                </p>
              </div>
              <p className="text-muted">
                {tCategory.courses}&nbsp;
                {tr`courses`}
              </p>
            </div>
          ))}
        </div>
      )}
      <div className={styles.divider}></div>
      {allCourses?.allCategories && (
        <div>
          <div className={styles.categories}>
            <p className="text-muted">{tr`all_categories`}</p>
            {allCourses?.allCategories.map((aCategory) => (
              <div className={styles.categories__items} key={aCategory.id}>
                <div className="d-flex">
                  {categoryIdParams == aCategory.id && (
                    <i
                      className="fa-solid fa-check pt-1 pr-1"
                      style={{ color: successColor, cursor: "pointer" }}
                      onClick={() => handleSearch()}
                    ></i>
                  )}
                  <p
                    className="cursor-pointer"
                    style={{
                      color: categoryIdParams == aCategory.id ? "#d00" : "#333",
                    }}
                    onClick={() => handleSearch(aCategory.id)}
                  >
                    {aCategory.name}
                  </p>
                </div>
                <p className="text-muted">
                  {aCategory.courses}&nbsp;
                  {tr`courses`}
                </p>
              </div>
            ))}
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default RCourseCategories;
