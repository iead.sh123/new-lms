import { get, destroy, put, post } from "config/api";

export const questionsSetApi = {
  getCourseQsInfo: (courseId) =>
    get(`api/question-set/course-for-management/${courseId}/get-question-sets`), //tch-api/question-set/get-course-qs-info/${courseId}`),
  deleteQuestionS: (qs_id, courseId) =>
    destroy(`api/question-set/${qs_id}/${courseId}`),
  pagenation: (url) => get(url),
  getQuestionSetsRead: () =>
    get(`api/permissions/user-table-permission/question_sets/read`),
  getCourseSingleQs: (courseId, qsId) =>
    get(`tch-api/question-set/get-course-single-qs/${courseId}/${qsId}`),
  getCourses: () => get(`api/courses`),
  getallowedApp: () => get(`tch-api/question-set/allowed-apps`),
  updateQsPatterns: (QuestionSetId, request) =>
    put(`api/qs-patterns/${QuestionSetId}`, request),
  addNewQsPatterns: (request) => post(`api/qs-patterns`, request),
  getCourseTopics: (targetUrl) => get(targetUrl),
  deleteQsPatterns: (paternId) => post(`api/qs-patterns/${paternId}`, paternId),
  SaveQs: (Qset) => post(`api/question-set/store`, Qset),
  createCopy: (qsId, courseId) =>
    post(`api/question-set/copy-qs/${qsId}/${courseId}`),

  getQsPattern: (rabId, patternId) =>
    get(`tch-api/question-set/pattern/${rabId}/${patternId}`),

  getQsStudentDetails: (rabId, patternId, QuestionId) =>
    get(
      `tch-api/question-set/studentDetails/${rabId}/${patternId}/${QuestionId}`
    ),

  updateStudentDetails: (QuestionId, data) =>
    post(`tch-api/question-set/correct/${QuestionId}/studentDetails`, data),
  getQsPatternByQuestion: (RabID, QuestionId) =>
    get(`tch-api/question-set/getPatterns/${RabID}/${QuestionId}`),
};
