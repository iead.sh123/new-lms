import React, { useContext } from "react";
import { FormGroup, Input, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { Services } from "engine/services";
import RDescription from "components/Global/RComs/RDescription/RDescription";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import styles from "./Rubric.module.scss";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RFormBasic = () => {
	const RubricData = useContext(RubricContext);

	return (
		<RFlex className={styles.rubric__editor__basic}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<RFlex styleProps={{ gap: 66 }}>
					<FormGroup style={{ flex: 1 }}>
						<label>{tr("title")}</label>
					</FormGroup>
					<FormGroup style={{ flex: 1 }}>
						<label>{tr("tags")}</label>
					</FormGroup>
					<FormGroup style={{ flex: 1 }}></FormGroup>
				</RFlex>
				<RFlex styleProps={{ gap: 66 }}>
					<FormGroup style={{ flex: 1 }}>
						<Input
							name="title"
							type="text"
							onChange={RubricData.handleChange}
							value={RubricData.values?.title}
							className={
								(!!RubricData.touched.title && !!RubricData.errors.title) || (RubricData.touched.title && RubricData.errors.title)
									? "input__error"
									: ""
							}
						/>
						{RubricData.touched.title && RubricData.errors.title && <FormText color="danger">{RubricData.errors.title}</FormText>}
					</FormGroup>
					<FormGroup style={{ flex: 1 }}>
						<RTags
							url={`${Services.tag_search.backend}api/v1/tag/search/tags?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data.models : null)}
							getValueFromArrayItem={(i) => i.value}
							getLabelFromArrayItem={(i) => i.label}
							getIdFromArrayItem={(i) => i.id}
							onChange={(data) => {
								RubricData.setFieldValue("tags", data);
							}}
							defaultValues={RubricData.values?.tags}
						/>
						{RubricData.errors.tags && <FormText color="danger">{RubricData.errors.tags}</FormText>}
					</FormGroup>
					<FormGroup style={{ display: "flex", flex: 1, alignItems: "center" }}>
						<AppCheckbox
							name="pointed"
							onClick={(event) => RubricData.handleChangePointedValue(event.target.checked, RubricData.setFieldValue, RubricData.values)}
							label={<span>{tr`pointed`}</span>}
							checked={RubricData.values.pointed}
						/>
					</FormGroup>
				</RFlex>
			</RFlex>

			<RDescription
				value={RubricData.values?.description}
				handleChange={(data) => RubricData.setFieldValue("description", data)}
				error={RubricData.errors.description}
				openCollapseValue={RubricData.values?.openDescriptionCollapse}
				collapseToggle={() => RubricData.setFieldValue("openDescriptionCollapse", !RubricData.values.openDescriptionCollapse)}
			/>
		</RFlex>
	);
};

export default RFormBasic;
