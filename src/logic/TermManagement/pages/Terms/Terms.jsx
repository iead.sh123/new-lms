import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import { faChevronRight, faUserFriends, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import RRegularCard from "view/TermsManager/cards/RegularCard/RRegularCard";
import CreateAcademicYearModal from "./components/CreateTermModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import CreateTermModal from "./components/CreateTermModal";
import Loader from "utils/Loader";
import REmptyData from "components/RComponents/REmptyData";

const Terms = ({
	handleLoadTerms,
	terms,
	handleSave,
	handleUpdate,
	handleManage,
	handleDeactivate,
	handleActivate,
	handleFinalize,
	loading,
}) => {
	React.useEffect(() => {
		handleLoadTerms();
	}, []);

	const [createModalIsOpen, setCreateModalIsOpen] = React.useState(false);
	const [edited, setEdited] = React.useState(null);

	const handleEdit = (id) => {
		setEdited(id);
		handleToggle();
	};

	const handleToggle = () => setCreateModalIsOpen(!createModalIsOpen);

	return (
		<div>
			{loading ? (
				<Loader />
			) : (
				<React.Fragment>
					<div style={{ display: "flex", justifyContent: "end" }}>
						<RButton
							text={tr`create_new_term`}
							faicon="fa fa-plus"
							color="primary"
							outline
							onClick={handleToggle}
							// style={{alignSelf: 'end'}}
						/>
					</div>

					<CreateTermModal
						isOpen={createModalIsOpen}
						toggle={handleToggle}
						handleSave={edited ? (data) => handleUpdate(edited, data) && setEdited(null) : handleSave}
						edited={terms?.byId?.[edited]}
					/>
					{terms?.ids?.length ? (
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{terms?.ids?.map((tId) => (
								<RRegularCard
									key={"term-" + tId}
									id={tId}
									header={terms.byId[tId]?.name}
									items={[
										{
											text:
												moment(terms.byId[tId]?.startDate).format("DD/mm/yyyy") +
												" - " +
												moment(terms.byId[tId]?.endDate).format("DD/mm/yyyy"),
											icon: faCalendarCheck,
										},
									]}
									status={
										terms.byId[tId]?.isFinalized
											? { text: tr`Finalized`, color: "#D00" }
											: terms.byId[tId]?.isActive
											? { text: tr`Active`, color: "#19AF00" }
											: { text: tr`Inactive`, color: "#668AD7" }
									}
									contextMenuActions={[
										{
											id: tId + "-term-manage",
											disabled: false,
											onClick: () => handleManage(tId),
											icon: "fa fa-pencil-square-o",
											name: tr`Manage`,
										},
										...(!!!terms.byId[tId]?.isFinalized
											? [
													{
														id: tId + "-term-activate-de",
														disabled: false,
														onClick: terms.byId[tId]?.isActive
															? () => console.log("deactivate", tId) || handleDeactivate(tId)
															: () => handleActivate(tId),
														icon: "fa fa-power-off",
														name: terms.byId[tId]?.isActive ? tr`Deactivate` : tr`Activate`,
													},
													{
														id: tId + "-term-finalize",
														disabled: terms.byId[tId]?.isFinalized,
														onClick: () => handleFinalize(tId),
														icon: "fa fa-check",
														name: tr`Finalize`,
														color: "red",
													},
											  ]
											: []),
										{
											id: tId + "-edit-info",
											disabled: false,
											onClick: () => handleEdit(tId),
											icon: "fa fa-pencil",
											name: tr`Edit Information`,
										},
									]}
								/>
							))}
						</RFlex>
					) : (
						<REmptyData
							line2={
								<p>
									click on <span style={{ color: "#668AD7" }}>+ Create new term </span>to start creating terms
								</p>
							}
						/>
					)}
				</React.Fragment>
			)}
		</div>
	);
};

export default Terms;
