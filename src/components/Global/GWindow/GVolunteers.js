import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import { FetchVolunteersAsync } from "store/actions/admin/schoolEvents";
import Loader from "utils/Loader";
import RVolunteers from "../RComs/RVolunteers/RVolunteers";
import tr from "../RComs/RTranslator";

const GVolunteers = () => {
  const dispatch = useDispatch();
  const { schoolEventID } = useParams();

  const { volunteers, LoadMoreVolunteersPage, volunteersLoading } = useSelector(
    (state) => state.SchoolEventsRed
  );

  useEffect(() => {
    dispatch(FetchVolunteersAsync(schoolEventID, 1));
  }, []);

  const fetchData = () => {
    LoadMoreVolunteersPage !== null &&
      dispatch(FetchVolunteersAsync(schoolEventID, LoadMoreVolunteersPage));
  };

  return (
    <div className="content">
      <div
        id="scrollableDivV"
        className="content"
        style={{
          height: "50vh",
          overflow: "auto",
        }}
      >
        <InfiniteScroll
          dataLength={volunteers?.length}
          next={fetchData}
          hasMore={LoadMoreVolunteersPage !== null ? true : false}
          loader={<Loader />}
          scrollableTarget="scrollableDivV"
          style={{ overflow: "hidden" }}

          // endMessage={
          //   <h4 style={{ textAlign: "center" }}>{tr`no_data_to_show`}</h4>
          // }
        >
          <RVolunteers Volunteers={volunteers} />
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default GVolunteers;

//  <div className="alert alert-info text-center">
//    <strong>{tr("no_data")}</strong>
//  </div>;
