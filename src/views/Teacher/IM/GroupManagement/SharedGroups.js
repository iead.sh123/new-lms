import Loader from 'utils/Loader'
import React from 'react'
import RLister from 'components/Global/RComs/RLister'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import UserImg from '../userImg'
import { Services } from "engine/services";

const SharedGroups = ({ response }) => {
    return (
        !response ? <Loader /> :
            (
                <RFlex className="flex-column" id="Shared Groups">
                    {response?.data?.data?.map((group, index) => {

                        return (
                            <RFlex id="Group">
                                <img src={Services?.storage?.file +  group.resource?.[0]?.hash_id} width={32} height={32} />
                                <span className='p-0 m-0'>{group.name}</span>
                            </RFlex>
                        )
                    })}
                </RFlex>
            )
    )
}

export default SharedGroups