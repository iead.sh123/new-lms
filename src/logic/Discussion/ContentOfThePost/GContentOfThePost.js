import React, { useEffect, useContext } from "react";
import { getPostByIdAsync } from "store/actions/global/discussions.actions";
import { useDispatch } from "react-redux";
import RContentOfThePost from "view/Discussion/RContentOfThePost";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { useParams, useHistory } from "react-router-dom";

const GContentOfThePost = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { sectionId, cohortId, searchQuery, categoryName, userId } =
    useParams();

  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  useEffect(() => {
    dispatch(getPostByIdAsync(DiscussionData.postId));
  }, []);

  return <RContentOfThePost />;
};

export default GContentOfThePost;
