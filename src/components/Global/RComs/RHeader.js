import React from "react";
import RFlexBetween from "./RFlex/RFlexBetween";


import RBill from "./RBill";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import RTitle from "./Collaboration/RTitle";
const RHeader = ({
  title,
  count,
  link,
  linkTitle="See All",
  color
}) => {

  return (  <RFlexBetween style={{marginTop:"30px"}} >
    <div style={{display:"flex",flexDirection:"row",justifyContent:"flex-start",alignItems:"center"}}>
        <RTitle text={title}/>&nbsp;&nbsp;
       {count? <RBill   fontColor="#668ad7" color="#F3F3F3" count={count}/>:<></>}
    </div>

    <div>
    {link?<Link to={link}>{linkTitle}</Link>:<></>}
    </div>
    </RFlexBetween>
  );
};
export default RHeader;
