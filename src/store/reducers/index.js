import { combineReducers } from "redux";

import authReducer from "store/reducers/global/authReducer";
import teacherReducer from "./teacher/teacherReducer";

import studentsReducer from "./teacher/students.reducer";
import IMReducer from "./global/IM.reducer";

import questionSetsReducer from "./teacher/questionsSets.reducer.js";
import gradingReducer from "./teacher/grading.reducer";
import { loadingBarReducer } from "react-redux-loading-bar";
import lessonPlanReducer from "./teacher/lessonPlan.reducer";
import unitPlanReducer from "./global/unitPlan.reducer";

import NavigationReducer from "./global/NavigationReducer";
import lessonPageReducer from "./teacher/lessonPage.reducer";
import lessonRecordingReducer from "./teacher/lessonRecording.reducer";
import lessonSupplementReducer from "./teacher/lessonSupplement.reducer";
import lessonAssignmentExamReducer from "./teacher/lessonAssignmentExams.reducer";

import dashboardReducer from "./teacher/dashboard.reducer";

import genericFilterReducer from "./admin/genericFilter.Reducer";
import IM2Reducer from "./global/IM2.reducer";
import UploadFileReducer from "./admin/uploadFile.Reducer";

import NotificationsReducer from "./global/notifications.reducer";
import TrackServiceReducer from "./global/track.reducer";
import IMNewCycleReducer from "./global/IMNewCycle.reducer";

import lessonContentReducer from "./teacher/lessonContent.reducer";
import learningObjectReducer from "./global/learningObjects.reducer";
import certificateReducer from "./global/certificate.reducer";
import teacherCoursesReducer from "store/reducers/teacher/courses.reducer";
import courseContentReducer from "store/reducers/teacher/courseContents.reducer";
import viewReducer from "./global/viewReducer";
import discussionsReducer from "./global/discussions.reducer";
import cohortsReducer from "./global/cohorts.reducer";
import schoolInitiationReducer from "./global/schoolInitiation.reducer";
import coursesManagerReducer from "./global/coursesManager.reducer";
import lessonReducer from "./global/lesson.reducer";
import liveSessionReducer from "./global/liveSession.Reducer";
import badgesReducer from "./global/badges.reducer";
import questionSetReducer from "./global/questionSet.Reducer";
import rubricReducer from "./global/rubric.reducer";
import attendanceReducer from "./global/attendanceReducer.reducer";
import schoolManagementReducer from "./global/schoolManagement.reducer";

const rootReducer = combineReducers({
	attendanceRed: attendanceReducer,
	rubricRed: rubricReducer,
	questionSetRed: questionSetReducer,
	badgesRed: badgesReducer,
	liveSessionRed: liveSessionReducer,
	coursesManagerRed: coursesManagerReducer,
	lessonRed: lessonReducer,
	schoolInitiationRed: schoolInitiationReducer,
	discussionsRed: discussionsReducer,
	cohortsRed: cohortsReducer,
	certificateRed: certificateReducer,
	learningObjectRed: learningObjectReducer,
	lessonContentRed: lessonContentReducer,
	auth: authReducer,
	courses: teacherCoursesReducer,
	courseContent: courseContentReducer,
	schoolManagementRed: schoolManagementReducer,
	teacherReducer: teacherReducer,

	students: studentsReducer,
	chat: IMReducer,
	questionSets: questionSetsReducer,
	grading: gradingReducer,
	loadingBar: loadingBarReducer,

	lessonPlan: lessonPlanReducer,
	unitPlan: unitPlanReducer,
	navigation: NavigationReducer,
	lessonPageRed: lessonPageReducer,
	lessonRecording: lessonRecordingReducer,
	lessonSupplement: lessonSupplementReducer,
	lessonAssignmentExamRed: lessonAssignmentExamReducer,

	dashboard: dashboardReducer,

	genericFilterRed: genericFilterReducer,

	//	im: IM2Reducer,
	UploadFileRed: UploadFileReducer,

	NotificationsRed: NotificationsReducer,
	TrackServiceRed: TrackServiceReducer,
	iMcycle2: IMNewCycleReducer,
	view: viewReducer,
});

export default rootReducer;
