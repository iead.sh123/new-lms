import React from "react";
import iconsFa6 from "variables/iconsFa6";
import styles from "../Navbar.module.scss";
import { Collapse, NavItem, NavLink } from "reactstrap";
import IM from "views/Teacher/IM/IM";
import { lang } from "components/Global/RComs/RTranslator";

const Chat = ({ handleCloseIMCollapse, handleMarkChatsAsSeen, handleSelectedChatId, toggleIMCollapse, chatsNumber, imCollapseOpen }) => {
	return (
		<NavItem>
			<NavLink
				className="btn-magnify"
				href="#"
				onBlur={() => handleCloseIMCollapse()}
				onClick={(e) => {
					chatsNumber !== 0 && handleMarkChatsAsSeen();
					toggleIMCollapse();
				}}
			>
				<i className={iconsFa6.comments + " fa-lg"} />
				{chatsNumber !== 0 && (
					<h6 className={lang == "english" ? styles.fixedCircle : styles.fixedCircleRight}>
						<span className="ml-1 pt-3">{chatsNumber}</span>
					</h6>
				)}
				<Collapse style={{ position: "absolute", right: "5%", top: "75%", width: "35%", zIndex: "99" }} isOpen={imCollapseOpen}>
					{/* <ChatList floating={true} setSelectedId={(id) => handleSelectedChatId(id)}></ChatList> */}
					<IM floating={true} />
				</Collapse>
			</NavLink>
		</NavItem>
	);
};

export default Chat;
