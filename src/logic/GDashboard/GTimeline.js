import React, { useEffect, useState } from "react";
import RTimeLine from "./RTimeLine";



const GTimeline = ({tasks}) => {

  
//   "outDatedTasks": [
//     {
//         "id": 3,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:47:58.722Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:47:58.722Z",
//         "isClosed": false,
//         "exam": {
//             "id": 7,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:47:55.698Z",
//             "updated_at": "2023-10-04T07:47:55.698Z"
//         },
//         "type": "exams"
//     },
//     {
//         "id": 2,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:45:22.296Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:45:22.296Z",
//         "isClosed": false,
//         "exam": {
//             "id": 5,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:45:19.937Z",
//             "updated_at": "2023-10-04T07:45:19.937Z"
//         },
//         "type": "exams"
//     },
//     {
//         "id": 1,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:22:03.001Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:22:03.001Z",
//         "isClosed": false,
//         "exam": {
//             "id": 3,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:22:00.719Z",
//             "updated_at": "2023-10-04T07:22:00.719Z"
//         },
//         "type": "exams"
//     }
// ],
// "activeTasks": [
//     {
//         "id": 2,
//         "maximumLearnersAllowed": 200,
//         "passRequired": true,
//         "autoGrade": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 1,
//         "dueDate": null,
//         "cutoffDate": null,
//         "isDefault": true,
//         "created_at": "2023-10-03T08:11:05.333Z",
//         "updated_at": "2023-10-03T08:11:05.333Z",
//         "isClosed": false,
//         "assignment": {
//             "id": 5,
//             "name": "string",
//             "description": "string",
//             "points": 10,
//             "files": null,
//             "questionsCount": 1,
//             "is_published": true,
//             "created_at": "2023-10-03T08:11:01.032Z",
//             "updated_at": "2023-10-03T08:11:01.032Z"
//         },
//         "type": "assignments"
//     },
//     {
//         "id": 1,
//         "maximumLearnersAllowed": 200,
//         "passRequired": true,
//         "autoGrade": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 1,
//         "dueDate": null,
//         "cutoffDate": null,
//         "isDefault": true,
//         "created_at": "2023-10-03T08:10:54.742Z",
//         "updated_at": "2023-10-03T08:10:54.742Z",
//         "isClosed": false,
//         "assignment": {
//             "id": 3,
//             "name": "string",
//             "description": "string",
//             "points": 10,
//             "files": null,
//             "questionsCount": 1,
//             "is_published": true,
//             "created_at": "2023-10-03T08:10:53.803Z",
//             "updated_at": "2023-10-03T08:10:53.803Z"
//         },
//         "type": "assignments"
//     },
//     {
//         "id": 3,
//         "canContinue": false,
//         "maxAttemptsAllowed": 1,
//         "passRequired": true,
//         "autoGrade": true,
//         "isDefault": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 0,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-03T08:16:18.010Z",
//         "updated_at": "2023-10-03T08:16:18.010Z",
//         "isClosed": false,
//         "quiz": {
//             "id": 14,
//             "name": "string",
//             "description": "string",
//             "files": null,
//             "is_published": true,
//             "questionsCount": 5,
//             "points": 50,
//             "created_at": "2023-10-03T08:16:16.243Z",
//             "updated_at": "2023-10-03T08:16:16.243Z"
//         },
//         "type": "quizzes"
//     }
// ],
// "allTasksCount": 11
  

const fake=[
  { icon: 'fa fa-pen', title: 'First Event', description: 'This is the first event', image: null },
  { icon: 'fa fa-camera', title: 'Second Event', description: <div>This is the second event <i className="fas fa-clock clock-icon"></i> <span >10:00 - 11:00</span>
  <div> lesson number 10 <span style={{color:"orange",cursor: "pointer" }} onClick={()=>{setLesson({id:40}),setLessonModalOpen(true);}}> &#9888; Not published Yet</span></div>
  </div>, image: null },
  { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
]

  const [timeLineItems,setTimeLineItems] = useState([]);

  useEffect(()=>{
    const tempItems= tasks?.activeTasks?.map(s=>({

       icon: 'fa fa-pen',
       title: s.type,
       description: 'This is the first event',
       image: null 

    }))
    
    
    },[]);

  return <div><RTimeLine  items={timeLineItems} color="#668AD7" />

</div>

};

export default GTimeline;
