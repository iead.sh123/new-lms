export const Services_local1 = {
  courses_manager: {
    backend: "http://localhost:8008/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  lesson: {
    backend: "http://localhost:8005/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  question_set: {
    backend: "http://localhost:3002/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  grading: {
    backend: "http://192.168.1.71:3005/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  scorm: {
    backend: "http://localhost:8020/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  certificate: {
    backend: "http://192.168.1.71:8010/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  unit_plan: {
    backend: "http://192.168.1.72:15003/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },
  rubric: {
    backend: "http://192.168.1.72:15005/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  badge: {
    backend: "http://192.168.1.72:15002/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  lesson_plan: {
    backend: "http://192.168.1.72:15007/",
    print: "http://192.168.1.9:9003/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  tag_search: {
    backend: "http://192.168.1.72:15004/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  logs: {
    backend: "http://192.168.1.72:8009/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  lesson_content: {
    backend: "http://localhost:8034/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  authentication: {
    backend: "http://localhost:7001/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  auth_organization_management: {
    backend: "http://localhost:7000/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  discussion: {
    backend: "http://192.168.1.70:8004/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },
  cohorts: {
    backend: "http://192.168.1.70:8006/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  // cohort: {
  //   backend: "http://192.168.1.70:8006/",
  //   file: "http://localhost:8001/api/files/get-by-id/",
  // },

  notficiation: {
    backend: "http://localhost:8002/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  storage: {
    backend: "http://localhost:8001/",
    file: "http://localhost:8001/api/files/get-by-id/",
    flipBookFile: "http://localhost:8001/api/files/get/",
  },

  im: {
    backend: "http://localhost:7002/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  terms: {
    backend: "http://localhost:3003/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  calender: {
    backend: "http://192.168.1.71:3001",
    file: "http://localhost:8001/api/files/get-by-id/",
  },
  collaboration: {
    backend: "http://localhost:7003/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },

  meeting: {
    backend: "http://localhost:8004/",
    file: "http://localhost:8001/api/files/get-by-id/",
  },
};
