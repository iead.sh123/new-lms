import React, { useEffect, useReducer, useState } from "react";
import Loader from "utils/Loader";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { getPermissionsByRoleAsync } from "../State/UsersAndPermissions.action";
import { Row, Col } from "reactstrap";
import { useParams } from "react-router-dom";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";
import { post } from "config/api";
import Swal, { DANGER } from "utils/Alert";
import AddPermissionsToUserOrRoleLister from "./AddPermissionsToUserOrRoleLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RPermissions from "view/UsersAndPermissions/RPermissions";
import AppModal from "components/Global/ModalCustomize/AppModal";
import styles from "../GUsersAndPermissions.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const GPermissionsToRole = () => {
	const { roleId } = useParams();
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [openModal, setOpenModal] = useState(false);
	const [payloadData, setPayloadData] = useState({
		role_id: roleId,
		scopes: [],
	});
	const [addPermissionToRoleLoading, setAddPermissionToRoleLoading] = useState(false);
	const [alert, setAlert] = useState(false);

	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	//Hide Alert
	const hideAlert = () => {
		setAlert(null);
	};
	//Show Alert
	const showAlerts = (child) => {
		setAlert(child);
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	useEffect(() => {
		getPermissionsByRoleAsync(dispatch, roleId);
	}, []);

	const handleSearch = (emptySearch) => {
		getPermissionsByRoleAsync(dispatch, roleId, emptySearch ?? searchData);
	};

	const successDelete = async (prameters) => {
		let response;
		const data = {
			role_id: roleId,
			scopes_ids: [prameters.id],
		};
		response = await post(`${Services.auth_organization_management.backend}api/role/removePermissionsFromRole`, data);
		if (response) {
			if (response.data.status == 1) {
				hideAlert();
				getPermissionsByRoleAsync(dispatch, roleId);
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleAddPermissionsToRole = async () => {
		setAddPermissionToRoleLoading(true);
		const response = await post(`${Services.auth_organization_management.backend}api/role/addPermissionsToRole`, payloadData);

		if (response) {
			if (response.data.status == 1) {
				setAddPermissionToRoleLoading(false);
				handleCloseModal();
				getPermissionsByRoleAsync(dispatch, roleId);
				setPayloadData({
					role_id: roleId,
					scopes: [],
				});
			}
		} else {
			setAddPermissionToRoleLoading(false);
			toast.error(response.data.msg);
		}
	};

	return (
		<section>
			{usersAndPermissionsData.permissionByRoleLoading ? (
				<Loader />
			) : (
				<React.Fragment>
					{alert}
					<AppModal
						size="xl"
						show={openModal}
						parentHandleClose={handleCloseModal}
						header={
							<RFlex>
								<span>
									{payloadData.scopes.length} {tr`permissions`}
								</span>
								{addPermissionToRoleLoading ? (
									<i className="fa fa-refresh fa-spin"></i>
								) : payloadData.scopes.length > 0 ? (
									<span
										className={styles.btn_hover}
										style={{
											color: primaryColor,
											textDecoration: "underline",
											cursor: "pointer",
										}}
										onClick={() => {
											handleAddPermissionsToRole();
										}}
									>
										{tr`save`}
									</span>
								) : (
									""
								)}
							</RFlex>
						}
						headerSort={<AddPermissionsToUserOrRoleLister payloadData={payloadData} setPayloadData={setPayloadData} />}
					/>
					<section className={"p-4 mb-4 " + styles.box}>
						<RSearchHeader
							searchLoading={searchLoading}
							searchData={searchData}
							handleSearch={handleSearch}
							setSearchData={setSearchData}
							handleChangeSearch={handleChangeSearch}
							inputPlaceholder={tr`search_a_permission`}
							buttonName={tr`add_permissions`}
							handleOpenModal={handleOpenModal}
							addNew={usersAndPermissionsData?.permissionByRole.can_be_edit ? true : false}
						/>
						{usersAndPermissionsData?.permissionByRole?.scopes_with_category !== undefined ? (
							<Row className="mt-3">
								{Object?.keys(usersAndPermissionsData?.permissionByRole?.scopes_with_category).length == 0 ? (
									<REmptyData />
								) : (
									Object.keys(usersAndPermissionsData?.permissionByRole?.scopes_with_category).map((key) => {
										return (
											<Col>
												<h6 style={{ textAlign: "left" }}>{key}</h6>
												{usersAndPermissionsData?.permissionByRole?.scopes_with_category[key].map((item) => (
													<RPermissions
														permission={item}
														handleRemove={handleRemove}
														can_be_edit={usersAndPermissionsData?.permissionByRole.can_be_edit ? true : false}
													/>
												))}
											</Col>
										);
									})
								)}
							</Row>
						) : (
							""
						)}
					</section>
					{usersAndPermissionsData?.permissionByRole &&
						usersAndPermissionsData?.permissionByRole?.scopes_without_category &&
						usersAndPermissionsData?.permissionByRole?.scopes_without_category.length > 0 && (
							<section className={"p-4 " + styles.box}>
								<Row>
									{usersAndPermissionsData.permissionByRole?.scopes_without_category?.map((per) => (
										<RPermissions
											permission={per}
											handleRemove={handleRemove}
											can_be_edit={usersAndPermissionsData?.permissionByRole.can_be_edit ? true : false}
										/>
									))}
								</Row>
							</section>
						)}
				</React.Fragment>
			)}
		</section>
	);
};

export default GPermissionsToRole;
