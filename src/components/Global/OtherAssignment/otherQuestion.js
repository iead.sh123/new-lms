import React, { useState, useEffect } from "react";
import { Row, Col, FormGroup, Input, Button } from "reactstrap";
import tr from "../RComs/RTranslator";
import ImageEnlarge from "utils/ImageEnlarge";
import { resourcesBaseUrl } from "config/constants";
import FreeTextQuestion from "./OtherQuestionType/FreeTextQuestion";
import DrawQuestion from "./OtherQuestionType/DrawQuestion";
import UploadFileQuestion from "./OtherQuestionType/UploadFileQuestion";
import MatchQuestion from "./OtherQuestionType/MatchQuestion/MatchQuestion";
import MultiAnswerQuestion from "./OtherQuestionType/MultiAnswerQuestion";
import SingleAnswerQuestion from "./OtherQuestionType/SingleAnswerQuestion";
import { useSelector, useDispatch } from "react-redux";
import Loader from "../../../utils/Loader";
import {
  getQuestionSetStudentDetails,
  updateQuestionSetStudentDetails,
  updateStudentsDetailFields,
  setStudentId,
  updateStudentsMarkFields,
} from "../../../store/actions/teacher/questionsSets.actions";

import RButton from "../../Global/RComs/RButton";
import { Swiper, SwiperSlide } from "swiper/react/swiper-react.js";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";

import "swiper/swiper.scss";
import "swiper/modules/navigation/navigation.min.css";
import "swiper/modules/pagination/pagination.min.css";
import "swiper/modules/scrollbar/scrollbar.min.css";

function toOrder(num) {
  if (num == 1) return "1st";
  if (num == 2) return "2nd";
  if (num == 3) return "3rd";
  if (num > 3) return num + "th";
}
function Counter(num, singular) {
  if (num == 1) return num + " " + singular;
  else return num + " " + singular + "s";
}

const OtherQuestion = ({
  assignmentReadMode,
  question,
  currentActiveQuestion,
  currentActiveTopic,
  currentActiveGroup,
  patternLength,
  topicLength,
  topiccLength,
  questionLength,
  studentQsPatternId,
  studentQsQuestionId,
  rabId,
  assignmentState,
  groupId,
  StudentQsFirstQuestionId,
  dispatch,
}) => {
  const dispatch1 = useDispatch();

  const {
    studentDetailsLoading,
    updateStudentDetailsLoading,
    studentDetails,
    studentId,
  } = useSelector((state) => state.questionSets);

  useEffect(() => {
    if (studentQsPatternId && studentQsQuestionId) {
      dispatch1(
        getQuestionSetStudentDetails(
          rabId,
          studentQsPatternId,
          studentQsQuestionId
        )
      );
    }
  }, [studentQsPatternId, studentQsQuestionId]);

  const [studentInfo, setStudentInfo] = useState({
    feedback: "",
    teacher_mark: null,
    questionFeedback: "",
  });

  const [studentMark, setStudentMark] = useState({ mark: null });

  const HandleSetStudentInfo = (e) => {
    setStudentInfo({ ...studentInfo, [e.target.name]: e.target.value });
  };

  const HandleStudentMark = (e) => {
    setStudentMark({ mark: e.target.value });
  };

  const HandleUpdateStudentsDetailFields = (StdId) => {
    const temp = {
      feedback: studentInfo.feedback,
      teacher_mark: studentInfo.teacher_mark,
      questionFeedback: studentInfo.questionFeedback,
      student_id: StdId,
    };

    dispatch1(updateStudentsDetailFields(temp, studentInfo));
  };

  const HandleUpdateStudentsMarkFields = (StdId) => {
    const temp = {
      mark: studentMark.mark,
      student_id: StdId,
    };

    dispatch1(updateStudentsMarkFields(temp, studentMark));
  };

  return (
    <>
      {studentDetailsLoading || updateStudentDetailsLoading ? (
        <Loader />
      ) : (
        <Row>
          <Col xs={12}>
            <Row style={{ marginBottom: 10 }}>
              <Col md={8} xs={12}>
                <h6 style={{ display: "flex" }}>
                  Question Number :
                  <h6 className="text-muted" style={{ paddingLeft: 5 }}>
                    {toOrder(currentActiveQuestion + 1)} /{" "}
                    {Counter(topicLength, "Question")}
                  </h6>
                </h6>
              </Col>

              <Col md={4} xs={12}>
                <h6 style={{ display: "flex" }}>
                  Question Mark :
                  <h6 style={{ paddingLeft: 5 }} className="text-muted">
                    {" "}
                    {question?.mark.toFixed(2)}
                  </h6>
                </h6>
              </Col>

              <Col xs={12}>
                <h6 style={{ display: "flex" }}>
                  Question Text :
                  <h6 className="text-muted">
                    {question?.question_text &&
                    question?.question_text.includes("<math") ? (
                      <>
                        <MathJax math={question?.question_text} />
                      </>
                    ) : (
                      <>
                        <h6
                          style={{ paddingLeft: 5 }}
                          dangerouslySetInnerHTML={{
                            __html: question?.question_text,
                          }}
                        />
                      </>
                    )}
                  </h6>
                </h6>
              </Col>
            </Row>
          </Col>

          <Col xs={12} style={{ zIndex: 1 }}>
            <Row>
              {question?.attachments && (
                <Col xs={12} style={{ textAlign: "center" }}>
                  {["png", "jpeg"].includes(question?.attachments.mime_type) ? (
                    <ImageEnlarge
                      imageSrc={resourcesBaseUrl + question?.attachments.url}
                    />
                  ) : (
                    <a
                      url={resourcesBaseUrl + question?.attachments.url}
                      alt="Download"
                      class="btn btn-primary"
                    >
                      {tr`Download`}
                    </a>
                  )}
                </Col>
              )}
            </Row>
          </Col>

          <Col xs={12} style={{ zIndex: 0 }}>
            <>
              <h6 style={{ marginBottom: 15, color: "#fd832c" }}>
                {tr`answers`} ({studentDetails.length})
              </h6>

              <Swiper
                className="swiper_container"
                modules={[Pagination, Navigation]}
                spaceBetween={50}
                slidesPerView={1}
                navigation
                pagination={{ clickable: true }}
                // scrollbar={{ draggable: true }}
                // onSwiper={(swiper) => }
                // onSlideChange={() => }
              >
                {studentDetails.map((student) => (
                  <SwiperSlide>
                    <Row>
                      <Col
                        md={12}
                        style={{
                          padding: "0px 20px 0px 0px",
                        }}
                      >
                        <Col xs={12}>
                          <h6 style={{ display: "flex" }}>
                            Student Name :
                            <h6
                              className="text-muted"
                              style={{ paddingLeft: 5 }}
                            >
                              {student.name}
                            </h6>
                          </h6>
                        </Col>

                        <Col xs={12}>
                          <Row>
                            <Col lg={6} xs={12}>
                              <label>{tr("Overall Feedback")}:</label>
                              <FormGroup>
                                <Input
                                  className="w-100"
                                  readOnly={false}
                                  id="feedback"
                                  name="feedback"
                                  type="text"
                                  defaultValue={
                                    student.feedback && student.feedback
                                  }
                                  placeholder="Overall Feedback"
                                  onChange={(e) => HandleSetStudentInfo(e)}
                                  onBlur={() =>
                                    HandleUpdateStudentsDetailFields(
                                      student?.id
                                    )
                                  }
                                />
                              </FormGroup>
                            </Col>

                            {questionLength - 1 == currentActiveQuestion &&
                              topiccLength - 1 == currentActiveTopic &&
                              patternLength - 1 == currentActiveGroup && (
                                <Col lg={3} xs={12}>
                                  <label>{tr("Final Mark")}:</label>
                                  <FormGroup>
                                    <Input
                                      className="w-100"
                                      readOnly={false}
                                      id="teacher_mark"
                                      name="teacher_mark"
                                      type="number"
                                      defaultValue={
                                        student.teacher_mark &&
                                        student.teacher_mark
                                      }
                                      placeholder="Final Mark"
                                      onChange={(e) => HandleSetStudentInfo(e)}
                                      onBlur={() =>
                                        HandleUpdateStudentsDetailFields(
                                          student?.id
                                        )
                                      }
                                    />
                                  </FormGroup>
                                </Col>
                              )}

                            <Col lg={3} xs={12}>
                              <label>{tr("Total Mark")}:</label>
                              <FormGroup>
                                <Input
                                  className="w-100"
                                  readOnly={true}
                                  id="total_mark"
                                  name="total_mark"
                                  type="number"
                                  min={0}
                                  value={student?.total_mark}
                                  placeholder="Total Mark"
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                        </Col>

                        <Col xs={12} style={{ marginBottom: 15 }}>
                          {question?.question_type == "free_text" ? (
                            <FreeTextQuestion
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                              studentDetails={studentDetails}
                            />
                          ) : question?.question_type == "draw" ? (
                            <DrawQuestion
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                              studentDetails={studentDetails}
                            />
                          ) : question?.question_type == "upload_file" ? (
                            <UploadFileQuestion
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                            />
                          ) : question?.question_type == "match" ? (
                            <MatchQuestion
                              choices={question?.inst_choices}
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                            />
                          ) : question?.question_type ==
                            "multiple_answer_mc" ? (
                            <MultiAnswerQuestion
                              choices={question?.inst_choices}
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                            />
                          ) : question?.question_type == "single_answer_mc" ? (
                            <SingleAnswerQuestion
                              choices={question?.inst_choices}
                              assignmentReadMode={assignmentReadMode}
                              student={student}
                            />
                          ) : (
                            ""
                          )}
                        </Col>

                        <Col xs={12}>
                          <Row>
                            <Col lg={6} xs={12}>
                              {student?.detail?.feedback ? (
                                <Row>
                                  <Col xs={12} style={{ padding: 0 }}>
                                    <label>{tr("Question Feedback")}:</label>
                                    <FormGroup>
                                      <Input
                                        className="w-100"
                                        readOnly={false}
                                        id="questionFeedback"
                                        name="questionFeedback"
                                        type="text"
                                        defaultValue={
                                          student?.detail?.feedback
                                            ? student?.detail?.feedback
                                            : ""
                                        }
                                        placeholder="Question Feedback"
                                        onChange={(e) =>
                                          HandleSetStudentInfo(e)
                                        }
                                        onBlur={() =>
                                          HandleUpdateStudentsDetailFields(
                                            student?.id
                                          )
                                        }
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                              ) : studentId === student?.id &&
                                !student?.detail?.feedback ? (
                                <Row>
                                  <Col xs={12} style={{ padding: 0 }}>
                                    <label>{tr("Question Feedback")}:</label>
                                    <FormGroup>
                                      <Input
                                        className="w-100"
                                        readOnly={false}
                                        id="questionFeedback"
                                        name="questionFeedback"
                                        type="text"
                                        defaultValue={
                                          student?.detail?.feedback
                                            ? student?.detail?.feedback
                                            : ""
                                        }
                                        placeholder="Question Feedback"
                                        onChange={(e) =>
                                          HandleSetStudentInfo(e)
                                        }
                                        onBlur={() =>
                                          HandleUpdateStudentsDetailFields(
                                            student?.id
                                          )
                                        }
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                              ) : (
                                <Button
                                  color="info"
                                  outline={true}
                                  onClick={() => {
                                    dispatch1(setStudentId(student?.id));
                                  }}
                                >
                                  Add Question Feedback
                                </Button>
                              )}
                            </Col>
                            <Col md={6} xs={12}>
                              <label>{tr("Question Mark")}:</label>
                              <FormGroup>
                                <div style={{ display: "flex" }}>
                                  <Input
                                    className="w-50"
                                    readOnly={false}
                                    id="mark"
                                    name="mark"
                                    type="number"
                                    min="0"
                                    max={question?.mark.toFixed(2)}
                                    defaultValue={
                                      student.detail.mark && student.detail.mark
                                    }
                                    placeholder="Question Mark"
                                    onChange={(e) => HandleStudentMark(e)}
                                    onBlur={() =>
                                      HandleUpdateStudentsMarkFields(
                                        student?.id
                                      )
                                    }
                                  />
                                  <h6
                                    style={{
                                      position: "relative",
                                      top: "10px",
                                      left: "5px",
                                      fontSize: "16px",
                                    }}
                                  >
                                    {" "}
                                    / {question?.mark.toFixed(2)}
                                  </h6>
                                </div>
                              </FormGroup>
                            </Col>
                          </Row>
                        </Col>
                      </Col>
                    </Row>
                  </SwiperSlide>
                ))}
              </Swiper>

              {questionLength - 1 == currentActiveQuestion &&
                topiccLength - 1 == currentActiveTopic &&
                patternLength - 1 == currentActiveGroup && (
                  <RButton
                    text="Save"
                    color="info"
                    onClick={() =>
                      dispatch1(
                        updateQuestionSetStudentDetails(studentQsQuestionId, {
                          payload: { data: studentDetails },
                        })
                      )
                    }
                  />
                )}
            </>
          </Col>
        </Row>
      )}
    </>
  );
};

export default OtherQuestion;
