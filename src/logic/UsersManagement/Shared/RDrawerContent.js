import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RFileUploaderAsChunks from "components/RComponents/RFileUploaderAsChunks";
import { Services } from "engine/services";
import React, { useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import { Terms } from "../constants";
import AppModal from "components/Global/ModalCustomize/AppModal";

const RDrawerContent = ({ operationType, downloadTemplateFn, currentType }) => {
	const [openUploader, setOpenUploader] = useState(false);
	const handleCloseUploader = () => {
		setOpenUploader(false);
	};
	return (
		<RFlex className="flex-column">
			<span className="p-0 m-0" style={{ fontSize: "14px" }}>
				{operationType == "delete" ? tr("Bulk_delete") : tr("Bulk_export_import")}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "20px" }}>
				<RFlex styleProps={{ gap: "5px" }} className="flex-column">
					<span className="p-0 m-0">
						<span className="font-weight-bold m-0 p-0">1.</span> {tr("Download_CSV_template_optional")}
					</span>
					<RButton
						color="primary"
						text={tr("Download_Template")}
						style={{ width: "fit-content" }}
						className="m-0"
						onClick={() => downloadTemplateFn()}
					/>
				</RFlex>
				<span className="p-0 m-0">
					<span className="font-weight-bold m-0 p-0">2.</span> {tr("Edit_your_CSV_file")}
				</span>
				<RFlex className="align-items-center">
					<span className="p-0 m-0">
						<span className="font-weight-bold m-0 p-0">3.</span> {tr("Upload_your_CSV_file")}
					</span>
					<RButton faicon={iconsFa6.paperclip} color="primary" onClick={() => setOpenUploader(true)} className="m-0 p-0 w-fit" />
					<AppModal
						size={"lg"}
						show={openUploader}
						header={"Upload Files"}
						parentHandleClose={handleCloseUploader}
						headerSort={
							<RFileUploaderAsChunks
								typeFile={[".xlsx"]}
								apiURL={
									currentType == Terms.Users
										? `${Services.users_management.backend}api/import/users`
										: `${Services.users_management.backend}api/import/roles`
								}
							/>
						}
					/>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RDrawerContent;
