import { get, post } from "config/api";

export const OneDriveApi = {
  getFiles: (accessToken) => get(`api/drive/files?token=${accessToken}`),
  importFiles: (data) => post(`api/drive/files`, data),
};
