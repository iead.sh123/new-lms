import React, { useState, useRef, useEffect } from "react";

import UserFirstLetter from "../userFirstLetter";
import ChatListItem from "./chatListItem";
import ChatFolder from "./chatFolder";
import { chat_Folders } from "../constants/ChatFolders";
import { useDispatch, useSelector } from "react-redux";
import { ChatTags } from "../constants/ChatTags";
import tr from "components/Global/RComs/RTranslator";
import RRow from "view/RComs/Containers/RRow";
import RGrid from "components/Global/RComs/RFlex/RGrid";
import RDropdown from "components/Global/RComs/RDropdown";
import ChatListFilter from "./chatListFilter";
import Helper from "components/Global/RComs/Helper";
import CreateGroupModal1 from "./CreateGroupModal1";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import {
	faCircle,
	faPlusSquare,
	faTasks,
	faUser,
	faEdit,
	faBookOpen,
	faStar,
	faAlternateArrowCircleDown,
} from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ContextMenu from "../ContextMenu/ContextMenu";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import Settings from "../settings/Settings";
import SearchMultipleChats from "../searchMultipleChats/SearchMultipleChats";
import { NavLink, Link } from "react-router-dom";
import RSides from "components/Global/RComs/RSides";
import { pushChat } from "store/actions/IMCycle2.actions";
import settingIcon from "../../../../assets/img/settings2.svg";
import editIcon from "../../../../assets/img/edit.svg";
import styles from "./chatList.module.scss";
import editIcon2 from "../../../../assets/img/edit.svg";
import REmptyData from "components/RComponents/REmptyData";

function ChatList({
	chatActions,
	selectedMultipleChats,
	chats,
	setSelectedChat,
	floating,
	toggleNewMessageVisible,
	loggedUserId,
	selectedChat,
	toggleSettings,
	toggleSearch,
	groupAsFolders,
	handleSelectGroup,
	selectedGroup,
	handleGoToMessage,
	unread = 0,
}) {
	// chats.ids.map(()=>{
	//   chats.byId[chid]?.
	// })
	const dispatch = useDispatch();
	const [createGroup, setCreateGroup] = useState(true);
	const [showCreateGroup, setShowCreateGroup] = useState(false);
	const [allFilter, setAllFilter] = useState(false);
	const buttonStyle = {
		fontSize: "20px",
		paddingLeft: "0",
		background: "white",
		color: "gray",
		marginLeft: "0px",
		marginRight: "7px",
		padding: "0px",
		paddingLeft: "0px",
		paddingRight: "5px",
	};
	const firstButtonStyle = {
		fontSize: "20px",
		background: "white",
		paddingLeft: "0",
		color: "gray",
		marginLeft: "0px",
		marginRight: "7px",
		paddingLeft: "0px",
		paddingRight: "5px",
	};

	const auth = useSelector((s) => s.auth);

	const [hasScrollBar, setHasScrollBar] = useState(false);
	const divRef = useRef(null);

	const NewChat = () => {
		setCreateGroup(false);
		setShowCreateGroup(true);
	};
	const NewGroup = () => {
		setCreateGroup(true);
		setShowCreateGroup(true);
	};
	const NewBroadCast = () => {};

	const CreateChatActions = [
		{
			label: "New Chat",
			action: NewChat,
			// icon: CHAT_ACTIONS_ICONS[ChatTags.Starred],
		},
		{
			label: "New Group",
			action: NewGroup,
			//  icon: CHAT_ACTIONS_ICONS[ChatTags.Urgent],
		},
		// {
		//   label: "New BroadCast",
		//   action: NewBroadCast,
		//   //icon: CHAT_ACTIONS_ICONS[ChatTags.Archived],
		// },
	];
	const [showNewChatDropDown, setShowNewChatDropDown] = useState(false);
	const [showSettingsModal, setShowSettingsModal] = useState(false);
	useEffect(() => {
		const divElement = divRef.current;

		if (divElement) {
			// Check if scrollHeight is greater than clientHeight
			const isScrollable = divElement.scrollHeight > divElement.clientHeight;
			setHasScrollBar(isScrollable);
		}
		return () => {};
	});

	let unreadCount = 0;

	chats?.byId &&
		Object.values(chats?.byId)?.map((c) => {
			let pointer = 0;
			let lastmessage = 0;
			c?.entities?.users?.byId &&
				Object.values(c?.entities?.users?.byId).map((u) => {
					if (u.userId == loggedUserId) pointer = u?.pointer;
				});

			lastmessage = c?.entities?.messages?.byId[Math.max(...Object.keys(c?.entities?.messages?.byId).map(Number))];
			//Helper.cl(c?.id+","+pointer+","+lastmessage?.id+","+(((lastmessage&&!pointer)||(lastmessage&&pointer&& ( lastmessage?.id!=pointer)))?"unread":"read"),"c.id,pointer,lastmessage.id")
			if (lastmessage && pointer && lastmessage?.id != pointer) {
				unreadCount++;
			}
		});
	return (
		<div
			id="Wrapped Div"
			ref={divRef}
			style={{
				display: "flex",
				width: !floating ? "26%" : "100%",
				gap: !floating ? "5px" : "10px",
				padding: floating ? "15px 15px" : "0px 10px 0px 0px ",
				position: "relative",
			}}
			className={!floating ? "flex-column NormalChatList" : "floating-chatlist"}
		>
			<RFlex className="flex-column" id="chatlist">
				<CreateGroupModal1
					group={createGroup}
					open={showCreateGroup}
					setOpen={setShowCreateGroup}
					loggedUserId={loggedUserId}
				></CreateGroupModal1>

				{!floating ? (
					<RFlex id="Top Section" className="flex-column" styleProps={{ padding: "5px 3px" }}>
						<RFlex id="First Part of Top Section" className="align-items-center justify-content-between">
							<RFlex id="Chat with Count" styleProps={{ gap: "5px" }} className="align-items-center">
								<span className="p-0 m-0" style={{ color: "black" }}>
									Chats
								</span>
								{
									<div id="Chat Count" className={styles.ChatCount}>
										{chats?.ids?.length}
									</div>
								}
							</RFlex>
							<RFlex id="buttons" className="align-items-center" styleProps={{ gap: "10px" }}>
								<img src={settingIcon} width={16} height={16} style={{ cursor: "pointer" }} onClick={toggleSettings} />
								<ContextMenu padding={"0px"} icon={editIcon2} class1="c_icon_button" actions={CreateChatActions} />
							</RFlex>
						</RFlex>

						<SearchMultipleChats
							hideSearchMultiple={() => {}}
							handleSearchMessage={(chatId, messageId) => {
								handleGoToMessage(messageId, chatId);
							}}
						/>
					</RFlex>
				) : (
					<>
						<RFlex id="floating header" className="justify-content-between align-items-center">
							<RFlex id="left side of floating header" className="align-items-center" styleProps={{ gap: "5px" }}>
								<span onClick={(event) => event.stopPropagation()} className="p-0 m-0" style={{ color: "black" }}>
									Chat
								</span>
								<img
									onClick={(event) => {
										event.stopPropagation();
										toggleSettings();
									}}
									src={settingIcon}
									width={16}
									height={16}
									style={{ cursor: "pointer" }}
								/>
								<ContextMenu padding={"0px"} icon={editIcon2} class1="c_icon_button" actions={CreateChatActions} />
							</RFlex>
							<Link to="/g/IM" style={{ color: "#668AD7", cursor: "pointer", textDecoration: "underline", padding: "0px" }} src>
								See All
							</Link>
						</RFlex>
						<RFlex id="floating filter" onClick={(event) => event.stopPropagation()}>
							<RFlex className="align-items-center" styleProps={{ gap: "3px" }} id="unread with count" onClick={() => setAllFilter(false)}>
								<span
									className="p-0 m-0"
									style={{ color: allFilter ? "" : "#668AD7", textDecoration: allFilter ? "" : "underline", cursor: "pointer" }}
								>
									Unread
								</span>
								{
									<div
										id="floating Chat Count"
										//className={styles?.FloatingChatCount}
									>
										{unreadCount}
									</div>
								}
							</RFlex>
							<RFlex className="align-items-center" styleProps={{ gap: "3px" }} id="all with count" onClick={() => setAllFilter(true)}>
								<span
									className="p-0 m-0"
									style={{ color: allFilter ? "#668AD7" : "", textDecoration: allFilter ? "underline" : "", cursor: "pointer" }}
								>
									All
								</span>
								{
									<div id="floating Chat Count" className={styles.FloatingChatCount}>
										{chats?.ids?.length}
									</div>
								}
							</RFlex>
						</RFlex>
					</>
				)}
				{/* {(groupAsFolders)?"folders":"non-folders"} */}

				<RFlex id="BottomSection" className={`flex-column ${floating ? "floating-content" : ""}`} styleProps={{ minHeight: "fit-content" }}>
					{chats?.ids.length ? (
						<RFlex id="Chat Folders" className="flex-column" styleProps={{ height: "100%", gap: floating ? "5px" : "0px" }}>
							{groupAsFolders == true && !floating ? (
								// <div className={styles.chat_list}>

								<ChatListFilter
									customStyling={{ padding: "0px", margin: "0px", width: "150px", height: "fit-content" }}
									items={chat_Folders.map((chf, chi) => {
										// chf.label.toLowerCase() == selectedGroup.toLowerCase()
										return { ...chf, name: chf.label };
									})}
									itemCallback={(item) => handleSelectGroup(item.id)}
									selectedItemIndex={chat_Folders.findIndex((s) => selectedGroup?.toLowerCase() == s?.id?.toLowerCase())}
								></ChatListFilter>
							) : (
								<></>
							)}
							{/* <RSides start={
              <div id="chatfolders">
                {groupAsFolders ? (
                  // <div className={styles.chat_list}>
                  
                  <ChatListFilter

                  customStyling={{padding:'0px',margin:'0px',width:'10px',height:'10px'}}
                    items={chat_Folders.map((chf,chi) => {
                      // chf.label.toLowerCase() == selectedGroup.toLowerCase()
                      return { ...chf, name: chf.label };
                    })}
                    itemCallback={(item) => handleSelectGroup(item.id)}
                    selectedItemIndex={chat_Folders.findIndex(
                      (s) => selectedGroup?.toLowerCase() == s?.id?.toLowerCase()
                    )}
                  ></ChatListFilter>

                ) : (
                  <></>
                )}
              </div>
            }
              end={<div id="mutualactions">

                {(selectedMultipleChats.length > 1) && chatActions.map(a => {

                  return <button
                    className="c_icon_button c_first_button"
                    // id="settings_im"
                    // className="add_new_but"
                    onClick={a.action}
                  >
                    <i
                      className={a.icon}
                      data-toggle="tooltip"
                      data-placement="top"
                      title={tr(a.label)}
                    ></i>
                  </button>

                })}
              </div>
              }
            /> */}

							{chats.ids
								.filter(
									(chid) =>
										//belongs to the intended folder and not archived
										(groupAsFolders &&
											selectedGroup &&
											chats.byId[chid]?.tags?.some((tg) => tg === selectedGroup) &&
											!chats.byId[chid]?.tags?.some((tg) => tg === ChatTags.Archived) &&
											selectedGroup !== ChatTags.Archived) ||
										//archived
										(chats.byId[chid]?.tags?.some((tg) => tg === ChatTags.Archived) && selectedGroup === ChatTags.Archived) ||
										//no group selected or all
										!groupAsFolders ||
										(!selectedGroup && chats.byId[chid].tags.length === 0) ||
										(selectedGroup === ChatTags.All && !chats?.byId?.[chid]?.tags?.some((tg) => tg === ChatTags.Archived))
								)
								.map((ch_id, ch_index) => (
									<ChatListItem
										key={"chat" + ch_index}
										chat={chats.byId[ch_id]}
										/* userImg={userImg} */
										floating={floating}
										selectChat={() => setSelectedChat(ch_id)}
										loggedUserId={loggedUserId}
										selectedChat={selectedChat}
										isGroupSelected={selectedMultipleChats?.some((id) => ch_id === id)}
										groupSelectionIsEnabled={selectedMultipleChats.length}
										chatActions={chatActions.map((ac) => ({
											...ac,
											action: () => ac.action(ch_id),
										}))}
									/>
								))}
						</RFlex>
					) : (
						<div
							id="No-Data"
							style={{
								display: "flex",
								alignItems: "center",
								justifyContent: "center",
								top: "25%",
								position: "relative",
							}}
						>
							<REmptyData line1={tr`No Chats Yet`} line2={tr`Start chatting with other people`} />
						</div>
					)}
				</RFlex>
				{!floating && !hasScrollBar && <div className="blue-vertical-line" />}
			</RFlex>
		</div>
	);
}

export default ChatList;
