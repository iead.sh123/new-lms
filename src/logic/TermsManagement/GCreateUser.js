import RFlex from "components/Global/RComs/RFlex/RFlex";
import RCreateUser from "view/TermsManagement/RCreateUser";
import RButton from "components/Global/RComs/RButton";
import React from "react";
import tr from "components/Global/RComs/RTranslator";
import { useFormik } from "formik";
import * as yup from "yup";

const GCreateUser = ({ userType, addUserMutate, gradeLevelId, educationStageId, AcademicPath }) => {
	const initialValues = {
		name: "",
		first_name: "",
		last_name: "",
		email: "",
		password: "",
		active: true,
		// active_account_type: true,
		education_stage_id: userType == "Principals" ? educationStageId : null,
		grade_level_id: userType == "teachers" || AcademicPath ? null : gradeLevelId,
		profile: [],
	};

	const formSchema = yup.object().shape({
		name: yup.string().required(tr`name is required`),
		first_name: yup.string().required(tr`first name is required`),
		last_name: yup.string().required(tr`last name is required`),
		email: yup
			.string()
			.email(tr`invalid email`)
			.required(tr`email address is required`),
		password: yup
			.string()
			.matches(/^(?=.{1,})/, tr`must contain 1 character`)
			.required(tr`password is required`),
	});

	const handleFormSubmit = (values) => {
		addUserMutate.mutate({ userId: userType == "Principals" ? 5 : userType == "teachers" ? 14 : 1, data: { payload: values } });
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};
	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", gap: "13px" }}>
				{/* Form */}
				<RCreateUser
					values={values}
					errors={errors}
					touched={touched}
					handleChange={handleChange}
					handleBlur={handleBlur}
					getInputClass={getInputClass}
					setFieldValue={setFieldValue}
				/>
				{/* Actions */}
				<RFlex styleProps={{ width: "100%" }}>
					<RButton
						type="submit"
						text={tr`create_account`}
						color="primary"
						loading={addUserMutate.isLoading}
						disabled={addUserMutate.isLoading}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default GCreateUser;
