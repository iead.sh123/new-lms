import React from 'react';
import { Doughnut } from 'react-chartjs-2';

const RRoundGraph = ({ title, data,width }) => {
  // Extract the percentages and colors from the data array
  const percentages = data.map((item) => item.percentage);
  const colors = data.map((item) => item.color);

  // Create the dataset for the doughnut chart
  const dataset = {
    data: percentages,
    backgroundColor: colors,
  };

  // Create the chart data
  const chartData = {
    datasets: [dataset],
  };

  return (
    <div style={{width:width}}>
      <h6>{title}</h6>
      <Doughnut data={chartData} />
    </div>
  );
};

export default RRoundGraph;