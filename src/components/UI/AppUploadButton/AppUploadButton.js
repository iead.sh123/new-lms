import React from "react";
import styles from "./AppUploadButton.Module.scss";
const AppUploadButton = ({
  handleAttachmentChange,
  attachmentName = "",
  ...props
}) => {
  return (
    <label className={styles.attachment_label}>
      <input
        onChange={(event) => handleAttachmentChange(event)}
        type="file"
        {...props}
      />
      <div>
        <i className="fa fa-paperclip fa-lg" aria-hidden="true"></i>
      </div>
    </label>
  );
};

export default AppUploadButton;
