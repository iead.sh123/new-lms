import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import { Terms, viewUserTypeTabs } from "../constants";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { usersManagementApi } from "api/UsersManagement";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLoader from "components/Global/RComs/RLoader";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import { useMutateData } from "hocs/useMutateData";
import RPermissons from "../Shared/RPermissons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RUsersData from "../Shared/RUsersData";
import { useFormik } from "formik";
import * as Yup from "yup";
import RCSVDrawer from "../Shared/RCSVDrawer";
import GFirstMembersHeader from "../Shared/GFirstMembersHeader";
import RHeader from "../Shared/RHeader";
import RUserTypeSettings from "./RUserTypeSettings";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GAddMembersModal from "../Shared/GAddMembersModal";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import styles from "./userTypes.module.scss";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import * as colors from "config/constants";
import { useHistory } from "react-router-dom";
import RRoles from "../Shared/RRoles";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";

const GViewUserType = () => {
	//--------------------------------Global--------------------------------------------
	const { organizationUserTypeId } = useParams();
	const [currentUserTypeId, setCurrentUserTypeId] = useState(null);
	const [activeTab, setActiveTab] = useState(viewUserTypeTabs[0].title);
	const [permissionsEditMode, setPermissionsEditMode] = useState(false);
	const [rolesEditMode, setRolesEditMode] = useState(false);
	const [settingsEditMode, setSettingsEditMode] = useState(false);
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const [currentUserTypeName, setCurrentUserTypeName] = useState("");
	const [addMembersModal, setAddMembersModal] = useState(false);
	const [finishPermissons, setFinishPermissions] = useState(false);
	const [finishRoles, setFinishRoles] = useState(false);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const history = useHistory();
	const handleCloseModal = () => {
		setAddMembersModal(false);
	};
	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};
	const dropdownActions = [
		{
			label: tr("Bulk_delete"),
			action: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "delete" });
			},
		},
		{
			label: tr("Import_CSV"),
			action: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{ label: tr("Export_CSV"), action: () => {} },
	];
	//----------------------------------userType Info-----------------------------------------
	const {
		data: userType,
		isLoading: isLoadingUserType,
		isFetching: isFetchingUserType,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes, organizationUserTypeId],
		queryFn: () => usersManagementApi.getUserTypeDetails(organizationUserTypeId),
		onSuccessFn: (data) => {
			setFinishPermissions(true);
			setFinishRoles(true);
			setCurrentUserTypeId(data?.data?.data?.user_type_id);
			setUserTypeValue("userTypeName", data?.data?.data?.type?.name);
			setUserTypeValue("description", data?.data?.data?.type?.description);
			setAllMembersFilters({ ...allMembersFilters, typeNames: [data?.data?.data?.type?.name] });
			setCurrentUserTypeName(data?.data?.data?.type?.name);
			setSelectedPermissions(data?.data?.data?.permissions);
			setSavedPermissions(data?.data?.data?.permissions);
			setSelectedRoles(data?.data?.data?.roles);
			setSavedRoles(data?.data?.data?.roles);
		},
	});
	//------------------------------Settings-------------------------------------------------
	const initialValues = {
		userTypeName: null,
		description: "",
	};
	const validationSchema = Yup.object({
		userTypeName: Yup.string().required("UserTypeName is Required"),
		description: Yup.string(),
	});
	const {
		values: settingsValues,
		setFieldValue: setUserTypeValue,
		handleBlur,
		handleChange,
		setFieldTouched,
		setFieldError,
		errors: settingsErrors,
		touched,
	} = useFormik({ initialValues, validationSchema });

	const updateUserTypeMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.updateOrganizationUserType(id, payload),
		invalidateKeys: [Terms.UserTypes, organizationUserTypeId],
		onSuccessFn: ({ data }) => {
			setAllMembersFilters({ ...allMembersFilters, typeNames: [settingsValues.userTypeName] });
			setCurrentUserTypeName(settingsValues.userTypeName);
			setSettingsEditMode(false);
		},
	});
	//----------------------------Roles--------------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState([]);
	const [savedRoles, setSavedRoles] = useState([]);

	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			const sortedRoles = getSortedRoles(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Roles, backRoleSearchTerm],
				newData: sortedRoles,
				insertionDepth: "data.data",
			});
		},
		enableCondition: !!userType && finishRoles,
		keepPreviousData: true,
	});
	//-----------------------------------Sort Roles---------------------------------------
	const getSortedRoles = (roles) => {
		const rolesCopy = JSON.parse(JSON.stringify(roles));
		const selectedIds = new Set(selectedRoles?.map((r) => r.id));
		const rolesArray = rolesCopy?.data?.data;
		rolesArray?.sort((r1, r2) => {
			const r1IsSelected = selectedIds.has(r1.id);
			const r2IsSelected = selectedIds.has(r2.id);
			if (r1IsSelected && !r2IsSelected) {
				return -1;
			}
			if (!r1IsSelected && r2IsSelected) {
				return 1;
			}
			return 0;
		});
		return rolesArray;
	};
	//-------------------------------------Select Roles----------------------------------
	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles.some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = selectedRoles.filter((p1) => p1.id != role.id);
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = [...selectedRoles, role];
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const assignRolesToUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRolesToUserType(payload),
		invalidateKeys: [Terms.UserTypes, organizationUserTypeId],
	});
	const removeRolesFromUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeRolesFromUserType(payload),
		invalidateKeys: [Terms.UserTypes, organizationUserTypeId],
	});
	const getRemovedRoles = () => {
		const idsInSelectedRoles = new Set(selectedRoles.map((p) => p.id));
		const removedRoles = savedRoles.filter((p) => !idsInSelectedRoles.has(p.id));
		const removedRolesNames = removedRoles.map((p) => p.name);
		return removedRolesNames;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState([]);
	const [savedPermissions, setSavedPermissions] = useState([]);
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		onSuccessFn: (data) => {
			const sortedPermissions = getSortedPermissions(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
				newData: sortedPermissions,
				insertionDepth: "data.data",
			});
		},
		enableCondition: !!userType && finishPermissons,
		keepPreviousData: true,
	});
	//-----------------------------------Sort Permissions---------------------------------------
	const getSortedPermissions = (permissions) => {
		const permissionsCopy = JSON.parse(JSON.stringify(permissions));
		const selectedIds = new Set(selectedPermissions?.map((p) => p.id));
		const permissionsArray = permissionsCopy?.data?.data;
		permissionsArray?.sort((p1, p2) => {
			const p1IsSelected = selectedIds.has(p1.id);
			const p2IsSelected = selectedIds.has(p2.id);
			if (p1IsSelected && !p2IsSelected) {
				return -1;
			}
			if (!p1IsSelected && p1IsSelected) {
				return 1;
			}
			return 0;
		});
		return permissionsArray;
	};
	//-------------------------------------Select Permissons----------------------------------
	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissionsToUserType(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
	});
	const removePermissionsFromUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromUserType(payload),
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};

	//------------------------------------Members----------------------------------------------
	const [alert1, setAlert] = useState(false);
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	// const phoneNumberRegex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
	const [activeIdsLoading, setActiveIdsLoading] = useState([]);
	const [removeIdsLoading, setRemoveIdsLoading] = useState([]);
	const [frontMembersSearchTerm, setFrontMembersSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [filters, setFilters] = useState([{ id: 1, active: false, pickedOption: null, isApplied: false }]);
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const initialValuesMembers = {
		email: "",
		address: "",
		phoneNumber: "",
	};

	const validationSchemaMembers = Yup.object({
		email: Yup.string(),
		address: Yup.string(),
		phoneNumber: Yup.string(),
	});
	const {
		values: membersValues,
		touched: membersTouched,
		errors: membersErrors,
		handleChange: handleChangeMembers,
		handleBlur: handleBlurMembers,
	} = useFormik({
		initialValues: initialValuesMembers,
		validationSchema: validationSchemaMembers,
	});
	//------------------------------------get users with filters applyed-----------------------

	const {
		data: members,
		isLoading: isLoadingMembers,
		isFetching: isFetchingMembers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		enableCondition: currentUserTypeName != "" ? true : false,
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);
			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	//------------------------------------get User types to put it in dropdown----------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	//------------------------------------Remove Users From Role---------------------------------
	const handleRemoveMembers = ({ members }) => {
		const userIds = members.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		removeUsersMutation.mutate({ id: organizationUserTypeId, payload });
		setRemoveIdsLoading([...removeIdsLoading, ...userIds]);
	};
	const deleteSingleMember = (member) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`}{" "}
					<span className="p-0 m-0">
						{member.full_name} {tr("from")} {currentUserTypeName}
					</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members: [member] }, message, confirm);
	};
	const deleteMultipleMembers = (members) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`}{" "}
					<span className="p-0 m-0">
						{tr("the_selected_users")} {tr("from")} {currentUserTypeName}
					</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members }, message, confirm);
	};
	const removeUsersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.removeUsersFromUserType(id, payload),
		onSuccessFn: ({ variables }) => {
			setSelectedMembers(selectedMembers.filter((member) => !variables?.payload?.user_ids?.includes(member.id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.DELETE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					insertionDepth: "data.data.records",
				});
			});
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		onErrorFn: (error, variables) => {
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		invalidateKeys: [Terms.Members, allMembersFilters],
	});
	console.log("LoadingLoading", removeIdsLoading);
	//----------------------------Deactivate and activate Users--------------------------------
	const deactivateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.deactivateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: false },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const activateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.activateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: true },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const changeMultipleUserStatus = () => {
		if (selectedMembers.length <= 0) return;
		const userIds = selectedMembers.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		deactivateUsersMutation.mutate({ payload });
		setActiveIdsLoading([...activeIdsLoading, ...userIds]);
	};
	const changeSingleUserStatus = (user) => {
		const payload = {
			user_ids: [user.id],
		};
		if (user.is_active) {
			deactivateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		} else {
			activateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		}
	};
	//---------------------------------------Handle Selecting members--------------------------
	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};

	//------------------------------------Global-----------------------------------------------
	const handleEditSaveClicked = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.Roles:
				if (!rolesEditMode) {
					setRolesEditMode(true);
					return;
				}
				const removedRolesNames = getRemovedRoles();
				if (removedRolesNames.length > 0) {
					payload = {
						role_names: removedRolesNames,
						organization_user_type_ids: [organizationUserTypeId],
					};
					removeRolesFromUserTypeMutation.mutate({ payload });
				}
				if (selectedRoles.length > 0) {
					const roleNames = selectedRoles.map((role) => role.name);
					payload = {
						role_names: roleNames,
						organization_user_type_ids: [organizationUserTypeId],
					};
					assignRolesToUserTypeMutation.mutate({ payload });
				}
				const sortedRoles = getSortedRoles(roles);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Roles, backRoleSearchTerm],
					newData: sortedRoles,
					insertionDepth: "data.data",
				});
				setSavedRoles(selectedRoles);
				setRolesEditMode(false);
				break;
			case Terms.Permissions:
				if (!permissionsEditMode) {
					setPermissionsEditMode(true);
					return;
				}
				//----------------------------------Removed Permissions----------------------------
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						organization_user_type_ids: [organizationUserTypeId],
					};
					removePermissionsFromUserTypeMutation.mutate({ payload });
				}
				//----------------------------------add Permissions--------------------------------
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						organization_user_type_ids: [organizationUserTypeId],
					};
					assignPermissionsToUserTypeMutation.mutate({ payload });
				}
				const sortedPermissions = getSortedPermissions(permissions);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
					newData: sortedPermissions,
					insertionDepth: "data.data",
				});
				setPermissionsEditMode(false);
				setSavedPermissions(selectedPermissions);
				break;
			case Terms.Settings:
				if (!settingsEditMode) {
					setSettingsEditMode(true);
					return;
				}
				if (!settingsErrors.userTypeName) {
					payload = {
						id: currentUserTypeId,
						name: settingsValues.userTypeName,
						description: settingsValues.description,
					};
					updateUserTypeMutation.mutate({ id: currentUserTypeId, payload });
				}
				break;
		}
		return;
	};
	if (isLoadingUserType) return <RLoader />;
	return (
		<RFlex className="flex-column">
			<RFlex className="align-items-center">
				<div
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/user-types`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				<span style={{ fontSize: "15px" }} className="p-0 m-0">
					{userType?.data?.data?.type?.name}
				</span>
			</RFlex>

			<RFlex className="justify-content-between">
				<RTabs tabs={viewUserTypeTabs} setTabActive={setActiveTab} activeTab={activeTab} />
				{activeTab == Terms.Roles && (
					<RButton
						text={rolesEditMode ? tr("save") : tr("edit")}
						faicon={rolesEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={rolesEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={
							updateUserTypeMutation.isLoading || assignRolesToUserTypeMutation.isLoading || removeRolesFromUserTypeMutation.isLoading
						}
						disabled={
							updateUserTypeMutation.isLoading || assignRolesToUserTypeMutation.isLoading || removeRolesFromUserTypeMutation.isLoading
						}
					/>
				)}
				{activeTab == Terms.Settings && (
					<RButton
						text={settingsEditMode ? tr("save") : tr("edit")}
						faicon={settingsEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={settingsEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={updateUserTypeMutation.isLoading}
						disabled={updateUserTypeMutation.isLoading}
					/>
				)}
				{activeTab == Terms.Permissions && (
					<RButton
						text={permissionsEditMode ? tr("save") : tr("edit")}
						faicon={permissionsEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={permissionsEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={
							assignPermissionsToUserTypeMutation.isLoading ||
							removePermissionsFromUserTypeMutation.isLoading ||
							updateUserTypeMutation.isLoading
						}
						disabled={
							assignPermissionsToUserTypeMutation.isLoading ||
							removePermissionsFromUserTypeMutation.isLoading ||
							updateUserTypeMutation.isLoading
						}
					/>
				)}
			</RFlex>
			{activeTab == Terms.Roles &&
				(isLoadingRoles || isLoadingUserType ? (
					<RLoader />
				) : (
					<RRoles
						roles={roles?.data?.data}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAsllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles}
						canEdit={rolesEditMode}
						view
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions || isLoadingUserType ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={permissions?.data?.data}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
						canEdit={permissionsEditMode}
						view
					/>
				))}
			{activeTab == Terms.Members &&
				(isLoadingMembers || isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RFlex className="flex-column">
						<RHeader
							actions={dropdownActions}
							disableCreate={updateUserTypeMutation.isLoading}
							loadingCreate={updateUserTypeMutation.isLoading}
							showCreate={true}
							createText={tr("Add_Members")}
							showSearchHeader={false}
							showBulk={true}
							showColumnSettings={true}
							showRefresh={false}
							onCreateClick={() => setAddMembersModal(true)}
							showDeactivate={false}
							showDelete={selectedMembers.length > 0}
							deleteText="remove"
							deleteFn={() => deleteMultipleMembers(selectedMembers)}
							deactivateFn={changeMultipleUserStatus}
							deleteLoading={removeUsersMutation.isLoading}
							deactivateLoading={deactivateUsersMutation.isLoading || activateUsersMutation.isLoading}
						/>
						<GFirstMembersHeader
							allUsersLength={members?.data?.data?.records?.length}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							filters={filters}
							setFilters={setFilters}
							frontSearchTerm={frontMembersSearchTerm}
							setFrontSearchTerm={setFrontMembersSearchTerm}
							isFetching={isFetchingMembers}
							selectedUsers={selectedMembers}
							values={membersValues}
							touched={membersTouched}
							errors={membersErrors}
							handleChange={handleChangeMembers}
							handleBlur={handleBlurMembers}
							showAddUsers={false}
						/>
						<RUsersData
							users={members?.data?.data}
							userTypes={userTypes?.data?.data}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							selectedUsers={selectedMembers}
							selectOneFn={handleSelectMember}
							selectAllFn={handleSelectAllMembers}
							deleteSingleMember={deleteSingleMember}
							isRemoving={removeUsersMutation.isLoading}
							changeSingleUserStatus={changeSingleUserStatus}
							activeIdsLoading={activeIdsLoading}
							removeIdsLoading={removeIdsLoading}
							disableRemoving={updateUserTypeMutation.isLoading}
							inCreate
							view
						/>
						<RCSVDrawer isOpen={isDrawerOpen.isOpen} operationType={isDrawerOpen.operationType} toggleDrawer={toggleDrawer} />
					</RFlex>
				))}
			{activeTab == Terms.Settings && (
				<RUserTypeSettings
					values={settingsValues}
					errors={settingsErrors}
					touched={touched}
					review={!settingsEditMode}
					handleBlur={handleBlur}
					handleChange={handleChange}
					showSettings={false}
					savedSettingsValues={settingsValues}
					view
				/>
			)}
			<AppModal
				show={addMembersModal}
				size={"lg"}
				header={true}
				headerSort={
					<GAddMembersModal
						currentUserTypeId={organizationUserTypeId}
						currentUserTypeName={currentUserTypeName}
						keyToInvalidate={[Terms.Members, allMembersFilters]}
					/>
				}
				parentHandleClose={handleCloseModal}
			/>
			{alert1}
		</RFlex>
	);
};

export default GViewUserType;
