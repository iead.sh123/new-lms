import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import RRoundGraph from 'components/Global/RComs/RRoundGraph';
import { useState } from 'react';
import { useEffect } from 'react';
import Helper from 'components/Global/RComs/Helper';
import { Services } from 'engine/services';
import RSwiper from 'components/Global/RComs/RSwiper/RSwiper';
import { SwiperSlide } from 'swiper/react';

export const GCollaborationCenterDashboard=()=>{
const [data,setData]=useState([]);
useEffect (()=>{
  const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/sharable-statistics","fail to get",(response)=>{setData(response.data?.data)},()=>{})}
  get_it();
  },[])

  const response={
    "status": 1,
    "code": 200,
    "msg": null,
    "data": [
        {
            "table_name": "rubrics",
            "name": "Rubrics",
            "app_id": "rubrics",
            "id": 3,
            "statics": {
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "nominated_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0
            },
            "user_statics": {
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "nominated_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0
            },
            "pivot": {
                "organization_id": 1,
                "sharable_content_id": 3,
                "need_approval": 1
            }
        },
        {
            "table_name": "courses",
            "name": "Courses",
            "app_id": "courses",
            "id": 4,
            "statics": {
                "organization_id": 1,
                "content_type_id": 4,
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0,
                "candidate_count_last_24": 0,
                "nominated_count_last_24": 0,
                "updated_at": "2023-08-19T10:58:51.000000Z",
                "created_at": "2023-08-19T10:58:51.000000Z",
                "id": 2
            },
            "user_statics": {
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "nominated_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0
            },
            "pivot": {
                "organization_id": 1,
                "sharable_content_id": 4,
                "need_approval": 1
            }
        },
        {
            "table_name": "unit_plans",
            "name": "Unit Plans",
            "app_id": "unit_plans",
            "id": 2,
            "statics": {
                "organization_id": 1,
                "content_type_id": 2,
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0,
                "candidate_count_last_24": 0,
                "nominated_count_last_24": 0,
                "updated_at": "2023-08-19T10:58:51.000000Z",
                "created_at": "2023-08-19T10:58:51.000000Z",
                "id": 3
            },
            "user_statics": {
                "id": 12,
                "user_id": 1,
                "content_type_id": 1,
                "organization_id": 1,
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "nominated_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0,
                "candidate_count_last_24": 0,
                "nominated_count_last_24": 0,
                "created_at": "2023-08-19T10:58:51.000000Z",
                "updated_at": "2023-08-19T10:58:51.000000Z"
            },
            "pivot": {
                "organization_id": 1,
                "sharable_content_id": 2,
                "need_approval": 1
            }
        },
        {
            "table_name": "lesson_plans",
            "name": "Lesson Plans",
            "app_id": "lesson_plans",
            "id": 1,
            "statics": {
                "organization_id": 1,
                "content_type_id": 1,
                "approved_count": 16,
                "refused_count": 11,
                "candidate_count": 11,
                "approved_count_last_24": 16,
                "refused_count_last_24": 11,
                "candidate_count_last_24": 11,
                "nominated_count_last_24": 0,
                "updated_at": "2023-08-19T10:58:51.000000Z",
                "created_at": "2023-08-19T10:58:51.000000Z",
                "id": 4
            },
            "user_statics": {
                "approved_count": 0,
                "refused_count": 0,
                "candidate_count": 0,
                "nominated_count": 0,
                "approved_count_last_24": 0,
                "refused_count_last_24": 0
            },
            "pivot": {
                "organization_id": 1,
                "sharable_content_id": 1,
                "need_approval": 1
            }
        }
    ],
    "other": null,
    "other1": null
};



   
  return (    <div>
    {/* {Helper.js(data,"data")} */}
    <h6>Last 24 hours</h6>
     <RSwiper >
      {data?.map(d=>
       <SwiperSlide>
         <RRoundGraph width="150px" title={d.table_name} data={[
          { percentage: d.statics.approved_count_last_24, color: '#668AD7'  },
          { percentage: d.user_statics.approved_count_last_24, color: 'white' }
         ]} /></SwiperSlide>
        ) }
</RSwiper>
<h6>All Time</h6>
<RSwiper >
      {data?.map(d=><SwiperSlide>
         <RRoundGraph   width="150px" title={d.name} data={[
          { percentage: d.statics.approved_count, color: '#668AD7' },
          { percentage: d.user_statics.approved_count, color: 'white' }
         ]} /></SwiperSlide>
        ) }
            </RSwiper>
    </div>

  );
};




