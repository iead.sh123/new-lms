import React from "react";
import { Input } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
const RRoleSettings = ({ values, errors, touched, handleBlur, handleChange, review, showSettings = true, savedSettingsValues }) => {
	return (
		<RFlex className="flex-column">
			<RFlex className="flex-column">
				{review && !savedSettingsValues.roleName && showSettings && (
					<span className="p-0 m-0" style={{ fontWeight: "bold", width: "fit-content" }}>
						{tr("settings")}
					</span>
				)}
				<RFlex className="flex-column">
					<RFlex className="align-items-center" styleProps={{ width: "100%" }}>
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Role_name")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.roleName ?? "-"}
							</span>
						) : (
							<Input
								name="roleName"
								placeholder={tr("Role_name")}
								className={`${errors.roleName && touched.roleName ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.roleName}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
					<RFlex className="align-items-center">
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Description")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.description ?? "-"}
							</span>
						) : (
							<Input
								name="description"
								className={`${errors.description && touched.description ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RRoleSettings;
