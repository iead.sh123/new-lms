import React, { useState } from 'react'
import { Input } from 'reactstrap';
import styles from './RHoverSelect.module.scss'
import * as colors from 'config/constants'
import RSelect from 'components/Global/RComs/RSelect';
import tr from 'components/Global/RComs/RTranslator';
const RHoverSelect = (
    {
        saved,
        item,
        inputValue,
        handleEditFunctionality,
        inputWidth = "200px",
        extraInfo,
        divClassName = 'justify-content-start',
        textCenter = false,
        focusOnInput = true,
        options,
        value,
        handleSelectItem,
        spanColor,
        customOption = null,
    }
) => {

    // const [currentGroup, setCurrentGroup] = useState(value.label ? { label: value.label, value: value.value } : { label: '-', value: '-' })
    const [isHovering, setIsHovering] = React.useState(false)
    const inputRef = React.useRef(null)

    // const handleInputChange = (event) => { setInputValue(event.target.value) }

    return (
        <div onMouseEnter={() => setIsHovering(true)}
            onMouseLeave={() => setIsHovering(false)} className={`${styles.HoverSelect}${divClassName}`} style={{ width: inputWidth }}>
            {saved ? (
                <p
                    style={{
                        display: 'flex', flexWrap: 'wrap', alignContent: 'center',
                        justifyContent: textCenter ? 'center' : "start",
                        padding: '7px 10px', margin: '0px', minWidth: inputWidth,
                        height: '30px', border: isHovering ? '1px solid ' + colors.strokeColor : '1px solid white', borderRadius: '2px'
                    }}
                    className={`${styles.AnimateBorder}`}
                    onClick={(event) => { event.stopPropagation(); handleEditFunctionality(event, item, extraInfo) }}
                >
                    <span
                        style={{ color: spanColor ? spanColor : colors.boldGreyColor }}
                    >
                        {value.label ?? "NoGroup"}
                    </span>
                </p>
            ) : (
                <RSelect
                    option={options}
                    closeMenuOnSelect={true}
                    value={value}
                    placeholder={tr`category`}
                    onChange={(e) => { handleSelectItem(e, item, extraInfo) }}
                    customOption={customOption}
                    style={{overFlowY:'hidden'}}
                />
            )
            }
        </div >
    )
}

export default RHoverSelect