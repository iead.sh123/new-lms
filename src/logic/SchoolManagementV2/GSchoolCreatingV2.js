import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import styles from './SchoolCreating.module.scss'
import RButton from 'components/Global/RComs/RButton';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import RSchoolCreatingV2 from './RSchoolCreatingV2';
import { useSelector, useDispatch } from 'react-redux';
import Loader from 'utils/Loader';
import { useHistory } from "react-router-dom";
import tr from 'components/Global/RComs/RTranslator';
import { useMutateData } from 'hocs/useMutateData';
import { schoolManagementV2API } from '../../api/SchoolManagementV2/index';
import { baseURL, genericPath } from 'engine/config';
import { allTypes, types } from './constants';
export const SchoolContextV2 = React.createContext()
const GSchoolCreatingV2 = ({ isModal = false, closeModal }) => {
    const [uploadedImage, setUploadedImage] = useState(null);
    const { info, loading } = useSelector((state) => state?.schoolManagementRed)
    const [customFields, setCustomFields] = useState([])
    const dispatch = useDispatch()
    const history = useHistory()
    const initialValues = {
        email: '',
        address: '',
        url: '',
    }
    const user = useSelector((state) => state?.auth);
    // const organization_id = user && user?.user && user?.user?.organization_id;
    const organization_id = 2

    const validationSchema = Yup.object().shape({
        // logo: Yup.mixed().required('logo is required'),
        address: Yup.string(),
        email: Yup.string(),
        url: Yup.string()
    });
    const updateSchoolInformationMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.updateSchoolInforamtion(payload),
        onSuccessFn: () => {
            if (!isModal) {
                history.replace(`${baseURL}/${genericPath}/school-management`)
            }
            else {
                closeModal()
            }
        }
    })

    const editSchoolInfo = async (organization_id, settings, upload_id = null) => {
        let payload = {}
        if (upload_id) {
            payload = {
                org_id: organization_id,
                type_id: allTypes[types.School],
                org_name: "MoaedSchool",
                attachments: [
                    {
                        tag: "icon",
                        url: {
                            upload_id: upload_id
                        }
                    },
                ],
                settings: [...settings]
            }
        }
        else {
            payload = {
                org_id: organization_id,
                org_name: "MoaedSchool",
                type_id: allTypes[types.School],
                settings: [...settings]
            }
        }
        const { org_id, ...finalPayload } = payload
        updateSchoolInformationMutation.mutate({ payload: isModal ? payload : finalPayload })
    }

    const handleAddNewField = () => {
        const oldFieldsArray = JSON.parse(JSON.stringify(customFields))
        oldFieldsArray.push({ name: '', description: '', saved: false })
        setCustomFields(oldFieldsArray)
    }
    const handleCustomFieldChange = (event, item, { index, propertyName }) => {
        const oldFieldsArray = JSON.parse(JSON.stringify(customFields))
        oldFieldsArray[index][propertyName] = event.target.value
        setCustomFields(oldFieldsArray)
    }
    const handleEditFunctionality = (event, item, { index }, savedFlag = false) => {
        const oldFieldsArray = JSON.parse(JSON.stringify(customFields))
        oldFieldsArray[index].saved = savedFlag
        setCustomFields(oldFieldsArray)
    }
    const handleFieldSaved = (event, item, { index, propertyName }) => {
        if (event.key == 'Enter' || !event.key) {
            if (customFields[index][propertyName] == '')
                return
            handleEditFunctionality(event, item, { index }, true)
        }
    }
    const handleDelete = (index) => {
        setCustomFields(customFields.filter((field, currentIndex) => currentIndex != index))
    }
    const submitHandler = (values) => {
        console.log("Values", values)
        const firstPart = Object.keys(values).map((key) => ({
            key: key,
            value: values[key]
        }))
        const secondPart = customFields.filter((field, index) => field.name != '')
            .map((field, index) => ({ key: field.name, value: field.description }))
        const allFields = firstPart.concat(secondPart)
        console.log("settings", allFields)
        editSchoolInfo(organization_id, allFields, uploadedImage?.id)
        closeModal()
    }
    return <RFlex className={`${isModal ? styles.CreateContainerModal : styles.CreateContainer}`} styleProps={{ gap: '20px' }}>
        {!isModal ?
            <div style={{ marginBottom: '0px', fontSize: '18px' }}>{tr("Let's_Start_By_Getting")} <strong>{info?.name ?? "name"} </strong>Info</div>
            : <p className='p-0 m-0'>{tr("Edit_School_Information")}</p>
        }

        <Formik
            validateOnChange={true}
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={submitHandler}>
            {({ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue, initialValues }) => {
                return (
                    <SchoolContextV2.Provider
                        value={{
                            values,
                            touched,
                            errors,
                            handleBlur,
                            handleChange,
                            setFieldValue,
                            uploadedImage,
                            setUploadedImage,
                            info,
                            customFields,
                            setCustomFields,
                            handleAddNewField,
                            handleFieldSaved,
                            handleEditFunctionality,
                            handleCustomFieldChange,
                            handleDelete
                        }}>

                        <Form className={`d-flex flex-column ${isModal ? "align-items-start" : 'align-items-center'}`} style={{ gap: '10px' }} onSubmit={handleSubmit}>
                            <RSchoolCreatingV2 isModal={isModal} />
                            {!isModal ? (
                                <RButton
                                    text={tr("Save_And_Start_My_School")}
                                    className={`btn-magnify ${styles.SaveButton}`}
                                    faicon="fa-solid fa-arrow-right btn-magnify"
                                    iconRight={true}
                                    color='primary'
                                    style={{ padding: "7px 15px" }}
                                    type='submit'
                                    loading={updateSchoolInformationMutation.isLoading}
                                />
                            ) : (
                                <RFlex styleProps={{ gap: '16px' }}>
                                    <RButton
                                        text={tr("Save")}
                                        color='primary'
                                        className="m-0"
                                        type="submit"
                                        loading={updateSchoolInformationMutation.isLoading} />
                                    <RButton
                                        text={tr("Cancel")}
                                        className="m-0"
                                        onClick={closeModal}
                                        outline
                                        color="primary" />
                                </RFlex>
                            )
                            }

                        </Form>
                    </SchoolContextV2.Provider>

                )
            }}

        </Formik>
    </RFlex>
}

export default GSchoolCreatingV2