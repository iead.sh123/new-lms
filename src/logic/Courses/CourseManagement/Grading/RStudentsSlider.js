import React, { useContext, useState } from 'react'
import { GradingContext } from './GGradingManagement'
import iconsFa6 from 'variables/iconsFa6'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import Loader from 'utils/Loader'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import tr from 'components/Global/RComs/RTranslator'
import RGroupItem from './RGroupItem'
import RFinalStudentDetails from './RFinalStudentDetails'
import { baseURL, genericPath } from "engine/config";
const RStudentsSlider = ({ courseId, cohortId, studentId, studentsIds }) => {
    const gradingData = useContext(GradingContext)
    const [openGroupCollapseId, setOpenGroupCollapseId] = useState(null)
    const handleToggleGroupCollapse = (groupId) => {
        openGroupCollapseId == null ? setOpenGroupCollapseId(groupId) : setOpenGroupCollapseId(null)
    }
    const allItemIds = studentsIds
    if (gradingData.isLoadingStudentDetailsSlider || gradingData.isLoadingGradableItems || gradingData.isLoadingFinalMarks)
        return <Loader />
    console.log("RStudentSlider", gradingData.gradingState)
    const studentDetailsData = gradingData?.gradingState?.studentSlider
    console.log("StudentSlider", gradingData.gradingState)
    const currentIndexId = allItemIds?.indexOf(studentId)
    let prevId = currentIndexId - 1 < 0 ? null : allItemIds?.[currentIndexId - 1]
    let nextId = currentIndexId + 1 < allItemIds?.length ? allItemIds?.[currentIndexId + 1] : null
    return (
        <RFlex className="flex-column" styleProps={{ paddingTop: "0px", gap: '20px', paddingLeft: '0px' }}>
            <RFlex className='justify-content-between' >
                <RButton
                    text={tr("Back_To_Student_Details")}
                    faicon={iconsFa6.arrowLeft}
                    color='link'
                    className="text-primary p-0 m-0"
                    onClick={() => { gradingData.history.goBack() }}
                    style={{ fontSize: '16px', width: 'fit-content' }} />
                <RFlex>
                    <RButton
                        text={tr("Previous_Student")}
                        faicon={iconsFa6.arrowLeft}
                        color='link'
                        className="text-primary p-0 m-0"
                        disabled={prevId ? false : true}
                        onClick={() => { gradingData.history.replace(`${baseURL}/${genericPath}/course-management/course/${gradingData.courseId}/cohort/${gradingData.cohortId}/gradebook/studentDetails/${prevId}`) }}
                        style={{ fontSize: '14px', width: 'fit-content' }} />
                    <RButton
                        text={tr("Next_Student")}
                        faicon={iconsFa6.arrowRight}
                        color='link'
                        iconRight
                        className="text-primary p-0 m-0"
                        disabled={nextId ? false : true}
                        onClick={() => { gradingData.history.replace(`${baseURL}/${genericPath}/course-management/course/${gradingData.courseId}/cohort/${gradingData.cohortId}/gradebook/studentDetails/${nextId}`) }}
                        style={{ fontSize: '14px', width: 'fit-content' }} />
                </RFlex>
            </RFlex>
            <RFlex className='flex-column' styleProps={{ gap: '25px' }}>
                <RProfileName
                    name={studentDetailsData?.studentInfo?.full_name}
                    textStyle={{ fontSize: '18px' }}
                    imgStyle={{ width: '60px', height: '60px' }}
                    img={studentDetailsData?.studentInfo?.image} />
                <RFlex className='flex-column ' styleProps={{ gap: '30px' }}>
                    {studentDetailsData?.groups?.ids?.map((id, index) => {
                        const currentGroup = studentDetailsData?.groups?.[id]
                        return <RGroupItem
                            isStudent={false}
                            groupId={id}
                            studentId={studentId}
                            activeCollapse={openGroupCollapseId == id}
                            groupCollapseDetails={currentGroup}
                            handleToggleGroupCollapse={handleToggleGroupCollapse}
                        />
                    })}
                </RFlex>
                <RFinalStudentDetails />
            </RFlex>
        </RFlex>
    )
}

export default RStudentsSlider