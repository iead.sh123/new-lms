import React from "react";
import styles from "./RText.Module.scss";
import { Row, Col } from "reactstrap";
import withDirection from "hocs/withDirection";

const baseURL = process.env.REACT_APP_RESOURCE_URL;

const RQuote = ({
  color,
  image,
  text,
  author,
  basicStyle = false,
  minHeight = "90%",
  dir,
}) => {
  if (basicStyle)
    return (
      <div
        style={{
          fontFamily: "Montserrat, Helvetica Neue, Arial, sans-serif",
          fontSize: "14px",
          fontWeight: "700",
          //margin: "auto",
          padding:"15px",
          paddingTop: basicStyle ? "auto" : "20px",
          minHeight: basicStyle && minHeight ? minHeight : "auto",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div style={{ padding: "10px", paddingRight: "20px" }}>
          <img
            src={`${baseURL}${image}`}
            width="50px"
            height="50px"
            alt="No Image"
            style={{ top: author ? 10 : 1, position: "relative" }}
          />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {author ? `"${text}"` : text}
        </div>

        {author ? <div style={{ alignSelf: "flex-end" }}>{author}</div> : null}
      </div>
    );

  return (
    <Row
      style={
        basicStyle
          ? {
              borderRadius: "20px",
              marginTop: "1%",
              height: "90%",
              marginBottom: "1%",
            }
          : {
              boxShadow: "inset 0 0 5px #000000",
              borderRadius: "20px",
              marginTop: "15px",
              marginBottom: "15px",
            }
      }
      ///className="mt-3 shadow-lg pl-3 pr-3 mb-0 "
      className={
        basicStyle
          ? null
          : [
              styles.glasseffect,
              styles.dashboardCard,
              "mt-3 shadow-lg pl-3 pr-3 mb-0 ",
            ]
      }
    >
      <Col
        xs={12}
        className={basicStyle ? null : styles.content}
        //  style={{ backgroundColor: color }}
      >
        <Row>
          <Col
            xl={1}
            md={2}
            xs={4}
            className={dir ? styles.image : styles.image_rtl}
          >
            <img
              src={`${baseURL}${image}`}
              width="50px"
              height="50px"
              alt="No Image"
              style={{ top: author ? 10 : 1, position: "relative" }}
            />
          </Col>
          <Col xl={11} md={10} xs={8} style={{ top: 15 }}>
            <Row>
              <Col xs={12}>
                <p
                  className={styles.paragraph}
                  style={{ fontWeight: "bold", fontStyle: "italic" }}
                >
                  {author ? `"${text}"` : text}
                </p>
              </Col>
              <Col
                xs={12}
                style={{
                  display: "flex",
                  justifyContent: "end",
                  fontWeight: "bold",
                }}
              >
                <p className={styles.paragraph}>{author}</p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default withDirection(RQuote);
