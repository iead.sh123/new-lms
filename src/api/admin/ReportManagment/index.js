import { post, get } from "config/api";

export const adminReportManagmentApi = {
  AllReports: () => post("emp-api/reports"),

  Reports: (data) => post("api/reports/store", data),

  ReportsParameter: (data) => post("api/reports/storeParameter", data),

  ReportById: (id) => get(`api/reports/report/${id}`),

  ParameterById: (id) => get(`api/reports/parameter/${id}`),
};
