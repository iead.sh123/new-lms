import React from "react";
import RUploadFile from "view/FileManagement/RMyFiles/RUploadFile";
import { useFormik } from "formik";

const GUploadFile = ({ folderId, handleAddDataToList }) => {
	const initialValues = {
		showFileContent: false,
	};
	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	const handleShowFileContent = () => {
		setFieldValue("showFileContent", true);
	};

	const handleCallback = (data) => {};

	return <RUploadFile data={{ values, folderId }} handlers={{ handleShowFileContent, handleCallback, handleAddDataToList }} />;
};

export default GUploadFile;
