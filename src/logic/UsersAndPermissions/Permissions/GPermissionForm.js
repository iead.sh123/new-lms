import React, { useEffect, useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../GUsersAndPermissions.module.scss";
import {
  usersAndPermissionsReducer,
  initialState,
} from "../State/UsersAndPermissions.reducer";
import Loader from "utils/Loader";
import { useParams } from "react-router-dom";
import { getPermissionByIdAsync } from "../State/UsersAndPermissions.action";

const GPermissionForm = () => {
  const { permissionId } = useParams();
  const [usersAndPermissionsData, dispatch] = useReducer(
    usersAndPermissionsReducer,
    initialState
  );
  useEffect(() => {
    getPermissionByIdAsync(dispatch, permissionId);
  }, []);

  return (
    <section className={`p-4 ${styles.box}`}>
      {usersAndPermissionsData.permissionByIdLoading ? (
        <Loader />
      ) : (
        <React.Fragment>
          <section>
            <RFlex styleProps={{ alignItems: "center" }}>
              <h6>{tr`permission_name`}</h6>
              <i className="fa fa-question-circle-o" aria-hidden="true"></i>
            </RFlex>
            <p className="text-muted" style={{ textAlign: "left" }}>
              {usersAndPermissionsData.permissionById?.name}
            </p>
          </section>
          <section className="mb-4">
            <RFlex styleProps={{ alignItems: "center" }}>
              <h6>{tr`description`}</h6>
              <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </RFlex>
            <p className="text-muted" style={{ textAlign: "left" }}>
              {usersAndPermissionsData.permissionById?.title}
            </p>
          </section>
        </React.Fragment>
      )}
    </section>
  );
};

export default GPermissionForm;
