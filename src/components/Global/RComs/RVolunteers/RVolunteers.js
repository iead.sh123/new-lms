import React, { useState } from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { AcceptRejectVolunteersAsync } from "store/actions/admin/schoolEvents";
import ReactBSAlert from "react-bootstrap-sweetalert";
import styles from "./../RAnnouncement/RAnnouncement.Module.scss";
import avatar from "assets/img/avatar.png";
import tr from "../RTranslator";

const RVolunteers = ({ Volunteers }) => {
  const { schoolEventID } = useParams();
  const dispatch = useDispatch();

  const [alert, setAlert] = useState(false);

  //Hide Alert
  const hideAlert = () => {
    setAlert(null);
  };

  //Show Alert
  const showAlerts = (child) => {
    setAlert(child);
  };

  //Success Delete
  const successAcceptReject = (e, VolunteerId, FlagId) => {
    dispatch(
      AcceptRejectVolunteersAsync({
        payload: {
          accept: FlagId == 1 ? true : false,
          message: e,
          school_event_id: schoolEventID,
          user_ids: [VolunteerId],
        },
      })
    );
    hideAlert();
  };

  //Handle Accept Reject Volunteer
  const handelAcceptRejectVolunteer = (VolunteerId, FlagId) => {
    showAlerts(
      <ReactBSAlert
        input
        title={FlagId == 1 ? tr("message") : tr("message")}
        onConfirm={(e) => successAcceptReject(e, VolunteerId, FlagId)}
        onCancel={() => hideAlert()}
        confirmBtnBsStyle="info"
        cancelBtnBsStyle="danger"
        confirmBtnText={"yes"}
        cancelBtnText={tr("cancel")}
        showCancel
        btnSize=""
      />
    );
  };

  return (
    <>
      {Volunteers && Volunteers.length !== 0 && (
        <>
          {alert}
          <Row>
            {Volunteers.map((volunteer) => (
              <Col xs={10} style={{ margin: "auto" }}>
                <Card className="gedf-card ">
                  <CardHeader className={styles.card_header}>
                    <div className="d-flex justify-content-between align-items-center">
                      <div className="d-flex justify-content-between align-items-center">
                        <div className="mr-2">
                          <Link to="#">
                            <img
                              className="rounded-circle"
                              width="45"
                              src={
                                volunteer.image != null
                                  ? process.env.REACT_APP_RESOURCE_URL +
                                    volunteer.image
                                  : avatar
                              }
                              alt=""
                            />
                          </Link>
                        </div>
                        <div className="ml-2">
                          <Link to="#">
                            <div className="title-Details m-0 text-dark">
                              {volunteer.full_name}
                            </div>
                          </Link>
                          <div className="h7 text-muted">
                            <i className="fa fa-user"></i>{" "}
                            <Link to="#">
                              <span className="text-muted h7 mb-2">
                                {volunteer.type}
                              </span>
                            </Link>
                          </div>
                        </div>
                      </div>

                      {volunteer.accepted == 0 && volunteer.rejected == 0 ? (
                        <div>
                          <Button
                            outline
                            color="info"
                            onClick={(e) =>
                              handelAcceptRejectVolunteer(volunteer.user_id, 1)
                            }
                          >{tr`accept`}</Button>

                          <Button
                            outline
                            color="info"
                            onClick={(e) =>
                              handelAcceptRejectVolunteer(volunteer.user_id, 0)
                            }
                          >{tr`reject`}</Button>
                        </div>
                      ) : (
                        <div>
                          {volunteer.accepted == 1 &&
                          volunteer.rejected != 1 ? (
                            <Button
                              color="info"
                              disabled
                            >{tr`accepted`}</Button>
                          ) : volunteer.accepted != 1 &&
                            volunteer.rejected == 1 ? (
                            <Button
                              outline
                              color="info"
                              disabled
                            >{tr`rejected`}</Button>
                          ) : (
                            ""
                          )}
                        </div>
                      )}
                    </div>
                  </CardHeader>
                  <CardBody className={styles.card_body}></CardBody>
                </Card>
              </Col>
            ))}
          </Row>
        </>
      )}

      {Volunteers && Volunteers.length === 0 && (
        <div className="alert alert-info text-center">
          <strong>{tr("no_data")}</strong>
        </div>
      )}
    </>
  );
};

export default RVolunteers;
