import React from "react";
import { NavItem, NavLink } from "reactstrap";
import styles from "../Navbar.module.scss";
import iconsFa6 from "variables/iconsFa6";

const Notification = ({ togglePostsCollapse, notifications, notificationsTotalRecord }) => {
	return (
		<NavItem>
			<NavLink
				className="btn-magnify"
				href="#"
				onClick={() => {
					togglePostsCollapse();
				}}
				style={{ position: "relative" }}
			>
				<i className={iconsFa6.bell + " fa-lg"} />
				{notifications?.length !== 0 && notificationsTotalRecord && notificationsTotalRecord > 0 ? (
					<h6 className={styles.notification}>
						<span
							style={{
								position: "relative",
								textAlign: "center",
							}}
						>
							{notificationsTotalRecord}
						</span>
					</h6>
				):<></>}
			</NavLink>
		</NavItem>
	);
};

export default Notification;
