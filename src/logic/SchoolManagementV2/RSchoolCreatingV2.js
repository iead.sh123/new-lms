import React from 'react'
import { SchoolContextV2 } from './GSchoolCreatingV2'
import RFileSuite from 'components/Global/RComs/RFile/RFileSuite';
import styles from './SchoolCreating.module.scss'
import { Input, FormGroup, FormText } from "reactstrap";
import { Services } from 'engine/services';
import { ErrorMessage } from 'formik';
import tr from 'components/Global/RComs/RTranslator';
import RButton from 'components/Global/RComs/RButton';
import RFlex from 'components/Global/RComs/RFlex/RFlex';
import { fieldProperties } from './constants';
import RHoverComponent from 'components/Global/RComs/RHoverComponent/RhoverComponent';
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput';
import iconsFa6 from 'variables/iconsFa6';
const RSchoolCreatingV2 = ({ isModal = false }) => {
    const SchoolData = React.useContext(SchoolContextV2)
    const addressInputRef = React.useRef(null)
    React.useEffect(() => {
        if (addressInputRef) {
            addressInputRef.current.focus()
        }
        return () => { }
    }, [])
    console.log(SchoolData)
    return (
        <div className={`p-0 m-0 d-flex flex-column ${isModal ? "align-items-start" : "align-items-center"}`} style={{ gap: '10px' }}>
            <div>
                <label style={{ margin: '0px' }}>{tr("School_Logo")}</label>
                <div>
                    <RFileSuite
                        parentCallback={(e) => { console.log("upload id", e); SchoolData?.setUploadedImage(e[0]) }}
                        roundWidth={{ width: "63px", height: '63px', type: "school" }}
                        singleFile={true}
                        fileType={["image/*"]}
                        removeButton={false}
                        // value={[{ hash_id: SchoolData?.info?.image }]}
                        uploadName="upload"
                        showReplace={false}
                        showDelete={true}
                        showFileList={false}
                        showFileAdd={true}
                        setSpecificAttachment={() => { }}
                        theme="round"
                        binary={true}
                    ></RFileSuite>

                </div>
            </div>

            <div className={styles.InputsContainer}>
                <FormGroup style={{ margin: '0px' }}>
                    <label className={styles.Label} htmlFor="address">{tr("School_Address")}</label>
                    <Input
                        name="address"
                        type="text"
                        innerRef={addressInputRef}
                        placeholder='Address'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        value={SchoolData?.values?.address}
                        className={
                            (SchoolData?.touched.address && SchoolData?.errors?.address)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.address && SchoolData?.errors?.address && <FormText color="danger">{SchoolData?.errors?.address}</FormText>}
                </FormGroup>



            </div>

            <div className={styles.InputsContainer}>
                <FormGroup style={{ margin: '0px' }}>
                    <label className={styles.Label} htmlFor="email">{tr("Contact_Information")}</label>
                    <Input
                        name="email"
                        type="text"
                        placeholder='Contact Information'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        value={SchoolData?.values?.email}
                        className={
                            (!!SchoolData?.touched?.email && !!SchoolData?.errors?.email) || (SchoolData?.touched?.email && SchoolData?.errors?.email)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.email && SchoolData?.errors?.email && <FormText color="danger">{SchoolData?.errors?.email}</FormText>}

                </FormGroup>

            </div>

            <div className={styles.InputsContainer}>
                <FormGroup style={{ margin: '0px' }}>
                    <label className={styles.Label} htmlFor="url">{tr("Facebook_Page")}</label>
                    <Input
                        name="url"
                        type="text"
                        placeholder='Facebook Page'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        value={SchoolData?.values?.url}
                        className={
                            (!!SchoolData?.touched?.url && !!SchoolData?.errors?.url) || (SchoolData?.touched?.url && SchoolData?.errors?.url)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.url && SchoolData?.errors?.url && <FormText color="danger">{SchoolData?.errors?.url}</FormText>}
                </ FormGroup>
            </div>
            {SchoolData.customFields.length > 0 &&
                <RFlex styleProps={{ width: '100%' }} className="flex-column">
                    {SchoolData.customFields.map((field, index) => (
                        <RFlex styleProps={{ gap: "12px" }} className='align-items-center'>
                            <RFlex className='flex-column' styleProps={{ gap: "5px" }}>
                                {fieldProperties.map((key) => {
                                    const nameComponent = <RHoverInput
                                        // name={field.name}
                                        inputValue={field.name}
                                        handleInputDown={SchoolData.handleFieldSaved}
                                        inputWidth={'370px'}
                                        type={key.label}
                                        focusOnInput={true}
                                        inputPlaceHolder="Name"
                                        handleInputChange={SchoolData.handleCustomFieldChange}
                                        inputIsNotValidate={false}
                                        item={field}
                                        extraInfo={{ index, propertyName: key.value }}
                                        handleOnBlur={SchoolData.handleFieldSaved}
                                        handleEditFunctionality={SchoolData.handleEditFunctionality}
                                        saved={field.saved}
                                    />
                                    const descriptionComponent = <Input
                                        name={field.description}
                                        type="text"
                                        placeholder='descripiton'
                                        style={{ width: '370px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                                        onChange={(e) => SchoolData.handleCustomFieldChange(e, field, { index, propertyName: key.value })}
                                        value={field.value}
                                    />
                                    return key.value == 'name' ? nameComponent : descriptionComponent
                                })}
                            </RFlex>
                            <i
                                onClick={() => SchoolData.handleDelete(index)}
                                className={`${iconsFa6.delete} text-danger`}
                                style={{ cursor: 'pointer' }} />
                        </RFlex>
                    ))}
                </RFlex>
            }
            <RFlex styleProps={{ width: '100%' }}>
                <RButton
                    text={tr("Add_Custom_Field")}
                    color='link'
                    style={{ height: 'fit-content' }}
                    className={`text-primary ${styles.addNewFieldButton}`}
                    onClick={() => { SchoolData.handleAddNewField() }}
                />
            </RFlex>
        </div>
    )
}

export default RSchoolCreatingV2