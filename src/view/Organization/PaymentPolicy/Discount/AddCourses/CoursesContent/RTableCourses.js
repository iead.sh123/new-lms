import React, { useMemo } from "react";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import Hands_Give from "assets/img/new/Hands_Give.png";
import RLister from "components/Global/RComs/RLister";
import styles from "../../../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";

const RTableCourses = ({ coursesData, loading, formProperties, handlers }) => {
	const checkBoxComponent = ({ item }) => {
		return (
			<AppNewCheckbox
				onChange={(e) => {
					e.stopPropagation();
					handlers.handleSelectCourses({ courseId: item.id });
				}}
				checked={formProperties.values.allCourses.includes(+item.id)}
				idsOrderGenerator={item.id}
			/>
		);
	};
	const courseNameComponent = ({ item }) => {
		return (
			<RFlex>
				<span>{item.name}</span>
			</RFlex>
		);
	};

	const priceComponent = ({ item }) => {
		return (
			<RFlex>
				<span>{item.paymentInfo.price}</span>
				<span>{item.paymentInfo.orgCurrency}</span>
			</RFlex>
		);
	};

	const creatorComponent = ({ item }) => {
		return <RProfileName key={1} img={item.image_id} name={item.creator.full_name} />;
	};

	const _records = useMemo(
		() =>
			coursesData?.courses?.map((item) => ({
				details: [
					{
						key: tr``,
						value: checkBoxComponent({ item: item }),
						type: "component",
						minWidth: "10px",
					},
					{
						key: tr`course_name`,
						value: courseNameComponent({ item: item }),
						type: "component",
					},
					{
						key: tr`price`,
						value: priceComponent({ item: item }),
						type: "component",
					},
					{
						key: tr`creator`,
						value: creatorComponent({ item: item }),
						type: "component",
					},
				],
			})),
		[coursesData, formProperties.values.allCourses]
	);

	if (loading.coursesLoading) return <Loader />;
	return (
		<RFlex className="mt-2" styleProps={{ flexDirection: "column" }}>
			<RFlex className={styles.course__lister__scroll + " scroll_hidden"} styleProps={{ flexDirection: "column" }}>
				<RLister Records={_records} Image={Hands_Give} line1={tr`no_discounts_yet`} />
			</RFlex>
		</RFlex>
	);
};

export default RTableCourses;
