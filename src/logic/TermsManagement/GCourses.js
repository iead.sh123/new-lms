import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import iconsFa6 from "variables/iconsFa6";
import RSelect from "components/Global/RComs/RSelect";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GCourses = ({ academicPath }) => {
	const TermsManagementData = useContext(TermsManagementContext);
	const [search, setSearch] = useState("");

	const initialValues = {
		name: "",
		category_id: null,
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	const coursesData = useFetchDataRQ({
		queryKey: ["courses", values.category_id, search],
		queryFn: () =>
			coursesManagerApi.getCoursesByCategoryId({
				name: search,
				category_id: values.category_id,
				course_types: ["isMaster"],
				isPublished: true,
				noPagination: false,
			}),
	});

	const allCategoriesData = useFetchDataRQ({
		queryKey: ["allCategories"],
		queryFn: () => coursesManagerApi.allCategories(true),
	});

	const _records = React.useMemo(
		() =>
			coursesData?.data?.data?.data?.courses?.data?.map((course) => ({
				details: [
					{ key: tr`course_name`, value: course.name },
					{ key: tr`course_code`, value: course?.code ?? "-" },
					{ key: tr`credit`, value: course.credits },
				],
				actions: [
					{
						name: " ",
						icon: TermsManagementData.itemObj.idsLoading.includes(course.id) ? iconsFa6.spinner : iconsFa6.plus,
						color: "primary",
						outline: true,
						disabled:
							TermsManagementData?.itemObj?.idsLoading?.length == 0
								? false
								: TermsManagementData.itemObj.idsLoading.includes(course.id)
								? false
								: true,
						onClick: () => {
							TermsManagementData.handleSelectedCourseToLoading({ courseId: course.id });

							TermsManagementData.handleMakeCourseCloned({
								courseId: course.id,
								termId: TermsManagementData?.itemObj?.termId,
								curriculumId: TermsManagementData?.itemObj?.firstCurriculumID,
							});
						},
					},
				],
			})),
		[coursesData?.data?.data?.data?.courses, TermsManagementData.itemObj.idsLoading]
	);

	//Actions
	const handleSearch = (emptyData) => {
		setSearch(emptyData !== undefined ? "" : values.name);
	};

	const handleSelectCategory = (categoryId) => {
		setFieldValue("category_id", categoryId);
	};

	if (coursesData.isError) {
		return <REmptyData />;
	}

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<RSearchHeader
					searchLoading={coursesData.isLoading}
					searchData={values.name}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("name", value)}
					handleChangeSearch={(value) => setFieldValue("name", value)}
				/>

				<div style={{ width: "48%" }}>
					<RSelect
						option={[{ label: `--${academicPath ? tr`no_program` : tr`no_category`}--`, value: null }].concat(
							allCategoriesData?.data?.data?.data
						)}
						closeMenuOnSelect={true}
						placeholder={academicPath ? tr`program` : tr`category`}
						onChange={(e) => handleSelectCategory(e.value)}
						isLoading={allCategoriesData.isLoading}
					/>
				</div>
			</RFlex>
			{coursesData.isLoading ? <Loader /> : <RLister Records={_records} />}
		</RFlex>
	);
};

export default GCourses;
