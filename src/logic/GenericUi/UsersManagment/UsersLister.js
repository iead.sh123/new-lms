import React, { useState } from "react";
import { post } from "config/api";
import { useHistory } from "react-router-dom";
import { DANGER } from "utils/Alert";
import { useSelector } from "react-redux";

import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RButton from "components/Global/RComs/RButton";
import Swal from "utils/Alert";

import tr from "components/Global/RComs/RTranslator";

import ExcelPage from "../ExcelPage";
import UsersAddFromSeniorTeacher from "./UsersAddFromSeniorTeacher";
import AppModal from "components/Global/ModalCustomize/AppModal";
import UserLogs from "./UserLogs";
import filterReducer from "components/Global/RComs/filterState/filterReducer";
import { filterState } from "components/Global/RComs/filterState/filterReducer";
import UserAddExisting from "./UserAddExisting";

const UsersLister = (props) => {
	const history = useHistory();
	const [processsedRecords, setProcessedRecords] = useState([]);
	const [table_props, setTableProps] = useState([]);
	const [showExcelPage, setShowExcelPage] = useState(false);
	const [importExcel, setImportExcel] = useState(null);
	const [addExistingShow, setAddExistingShow] = useState(false);
	const [addExistingBackend, setAddExistingBackend] = useState(false);
	const [show, setShow] = useState(false);
	const [showLog, setShowLog] = useState(false);
	const [userId, setUserId] = useState(null);

	//const { newFilter } = useSelector((state) => state.genericFilterRed);
	const [localnewfilter, filterdispatch] = React.useReducer(filterReducer, filterState);

	//------------------------Advanced Lister Stuff

	const getDataFromBackend = async (specific_url) => {
		let Request = {
			payload: {
				Filters: /*newFilter.length == 1 ? "" : */ localnewfilter.newFilter ?? [],
			},
		};

		const url = `api/users/index/${props.specific_name}`;
		let response1 = await post(specific_url ? specific_url : url, Request);

		if (response1 && response1.data && response1.data.data && response1.data.data.data && response1.data.data.data.data) {
			let props_data = response1.data.data;
			if (props_data.props && props_data.props.data.data) {
				setTableProps(props_data.props.data.data);
				setAddExistingBackend(response1.data.data.add_existing);
				return response1;
			}
		} else {
			toast.error(response1.data.msg);
		}
	};

	const keysNotToShow = [
		"id",
		"created_at",
		"updated_at",
		"email1",
		"email2",
		"username",
		"pass=sword",
		"first_name",
		"last_name",
		"password",
	];

	const notExempted = (key) => {
		if (keysNotToShow.includes(key)) return false;
		return true;
	};

	const setData = (response) => {
		let processsedRecords1 = [];
		response?.data?.data?.data?.data.map((r) => {
			let record = {};
			record.allTitle = r?.school_user?.user?.full_name;
			record.title = r?.school_user?.user?.full_name;
			record.initialCheckValue = true;
			record.id = r.id;
			record.Titles = [r.name ? r.name : r.title ? r.title : ""];
			record.table_name = "userLister";

			record.details = [];

			response?.data?.data?.props;

			response.data?.data?.props?.data?.data?.map((key) => {
				if (notExempted(key.Field))
					record.details.push({
						key: key.Field,
						value: r?.school_user?.user[key.Field],
					});
			});

			record.images = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${r.image}`,
					placeholder: "11",
				},
			];

			record.icons = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${r.icon}`,
					placeholder: "11",
				},
			];

			record.actions = [
				{
					name: tr`manage`,
					icon: "fa fa-edit",
					color: "info",
					showAction: true,
					onClick: () => {
						history.push(`${process.env.REACT_APP_BASE_URL}/admin/manage/${props.specific_name}/${r.id}/Properties`);
					},
				},

				{
					permissions: props.specific_name == "employees" ? true : false,
					name: tr`log`,
					icon: "fa fa-history",
					color: "info",

					onClick: () => {
						setUserId(r.school_user.user_id);
						showModalLog();
					},
				},
			];

			processsedRecords1.push(record);
		});
		setImportExcel(response?.data?.data?.ok_to_upload_from_file);
		setProcessedRecords(processsedRecords1);
	};

	const getFilteredData = async () => {
		const response = await getDataFromBackend();
		if (response) {
			setData(response);
		}
	};

	const addExistingShowModal = () => {
		setAddExistingShow(true);
	};
	const addExistingHandleClose = () => {
		setAddExistingShow(false);
	};

	const showModal = () => {
		setShow(true);
	};
	const handleClose = () => {
		setShow(false);
	};

	const showModalLog = () => {
		setShowLog(true);
	};
	const handleCloseLog = () => {
		setShowLog(false);
	};

	return (
		<div className="content">
			<AppModal
				size="xl"
				show={showLog}
				parentHandleClose={handleCloseLog}
				header={tr`logs`}
				headerSort={<UserLogs userId={userId} recordId={null} tableName={null} />}
			/>

			{/* <AppModal
        size="lg"
        show={show}
        parentHandleClose={handleClose}
        header={tr`add_from_senior_teacher`}
        headerSort={
          <UsersAddFromSeniorTeacher
            handleClose={handleClose}
            specificName={props.specific_name}
          />
        }
      /> */}

			<AppModal
				size="xl"
				show={addExistingShow}
				parentHandleClose={addExistingHandleClose}
				header={tr`add_existing`}
				headerSort={<UserAddExisting addExistingHandleClose={addExistingHandleClose} specificName={props.specific_name} />}
			/>

			<RButton
				text={tr`add`}
				onClick={() => history.push(`${process.env.REACT_APP_BASE_URL}/admin/add/${props.specific_name}/Properties`)}
				color="primary"
				outline
			/>

			{addExistingBackend && <RButton text={tr`add_existing`} onClick={() => addExistingShowModal()} color="primary" outline />}

			{importExcel !== null && importExcel?.upload_from_file !== false && (
				<>
					<RButton text={importExcel.button_text} onClick={() => setShowExcelPage(!showExcelPage)} color="primary" outline />

					{showExcelPage && (
						<ExcelPage
							master_table={props.specific_name}
							master_id=""
							detail_table=""
							setShowExcelPage={setShowExcelPage}
							typeFile={importExcel?.allowed_file_types.toString()}
						/>
					)}
				</>
			)}

			{!showExcelPage && (
				<RAdvancedLister
					getDataFromBackend={getDataFromBackend}
					setData={setData}
					records={processsedRecords}
					getDataObject={(response) => response.data.data.data}
					characterCount={15}
					marginT={"mt-3"}
					marginB={"mb-2"}
					table_props={table_props}
					getData={getFilteredData}
					// showListerMode={"both"}
					localnewfilter={localnewfilter}
					filterdispatch={filterdispatch}
				></RAdvancedLister>
			)}
		</div>
	);
};

export default UsersLister;
