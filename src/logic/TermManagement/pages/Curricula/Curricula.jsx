import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import { faChevronRight, faUserFriends, faEllipsisV, faChalkboardTeacher, faPlus, faChalkboard } from "@fortawesome/free-solid-svg-icons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import RRegularCard from "view/TermsManager/cards/RegularCard/RRegularCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RCardSkeleton from "view/TermsManager/cards/RCardSkeleton/RCardSkeleton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RImageCard from "view/TermsManager/cards/ImageCard/RImageCard";
import { Services } from "engine/services";
import Loader from "utils/Loader";

const Curricula = ({ handleLoadCurricula, curricula, handleManageCurriculum, loading }) => {
	React.useEffect(() => {
		handleLoadCurricula();
	}, []);

	return (
		<div>
			{loading ? (
				<Loader />
			) : (
				<RFlex styleProps={{ flexWrap: "wrap" }}>
					{curricula?.ids?.map((tId) => (
						<RImageCard
							id={tId}
							image={Services.storage.file + curricula.byId[tId].main_course_image}
							icon={faChevronRight}
							handleIconClicked={() => handleManageCurriculum(tId)}
							header={curricula.byId[tId].name}
							statistics={[
								{
									text: curricula.byId[tId].courses.length + " " + tr`Classes`,
									icon: faChalkboard,
								},
							]}
						/>
					))}
				</RFlex>
			)}
		</div>
	);
};

export default Curricula;
