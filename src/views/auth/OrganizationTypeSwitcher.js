import React from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { SWITCH_TYPE } from "store/actions/global/globalTypes";
import { useHistory } from "react-router-dom";
import Helper from "components/Global/RComs/Helper";

const OrganizationTypeSwitcher = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const selected_school = useSelector(
    (state) => state?.auth?.user?.current_organization
  );
  const schools = useSelector((state) => state?.auth?.schoolsAndTypes);
  const currentType = useSelector((state) => state?.auth?.user?.current_type);

  const swicthSchool = (type, school_id) => {
    dispatch({ type: SWITCH_TYPE, payload: { type, school_id } });

    const ur = `${process.env.REACT_APP_BASE_URL}/auth/loader`;

    history.push(ur); // }
  };

  return (
    <div>
      <div style={{ color: "white" }}>
        {schools?.length ?? "Empty Orgnizations"}
      </div>
      {schools?.map((elem) => {
        return (
          <div
            key={elem.id}
            onClick={() => swicthSchool(elem.type, elem.organization.id)}
            disabled={
              selected_school == elem.organization?.id &&
              elem?.type?.toLowerCase() == currentType?.toLowerCase()
            }
            style={
              selected_school == elem.organization?.id &&
              elem?.type?.toLowerCase() == currentType?.toLowerCase()
                ? { backgroundColor: "gray", Color: "gray" }
                : { backgroundColor: "lightgray", Color: "black" }
            }
          >
            <i>
              {selected_school == elem.organization?.id &&
              elem?.type?.toLowerCase() == currentType?.toLowerCase() ? (
                <span class="dot"></span>
              ) : (
                <></>
              )}
              {elem.organization_name} as {elem.type}
              {/* {elem.organization.id } |cur  {selected_school} {currentType}                   {!((selected_school == elem.organization.id )&&                (elem?.type?.toLowerCase()==currentType?.toLowerCase()))?" active":" not active"}
               */}{" "}
            </i>
          </div>
        );
      })}
    </div>
  );
};

export default OrganizationTypeSwitcher;
