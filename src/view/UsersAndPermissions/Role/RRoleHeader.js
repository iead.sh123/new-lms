import React from "react";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RButton from "components/Global/RComs/RButton";
import styles from "../UsersAndPermissions.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Form, FormGroup, Input } from "reactstrap";

const RRoleHeader = ({
  searchData,
  setSearchData,
  handleChangeSearch,
  handleSearch,
  usersAndPermissionsData,
  handleOpenModal,
}) => {
  const { width } = useWindowDimensions();
  const mobile = width < 1000;
  return (
    <RFlex
      styleProps={{
        width: "100%",
        alignItems: "center",
        flexColumn: mobile ? "column" : "row",
        gap: 30,
      }}
    >
      <RButton
        color="primary"
        text={tr`create_new_role`}
        faicon="fa fa-plus"
        onClick={() => {
          handleOpenModal();
        }}
        outline
      />
      <Form className={styles.form_input}>
        <FormGroup>
          <Input
            type="text"
            placeholder={tr`search`}
            className={styles.search_input}
            value={searchData}
            onChange={(event) => {
              handleChangeSearch(event.target.value);
            }}
          />
          <i
            aria-hidden="true"
            className={
              usersAndPermissionsData.organizationRolesLoading
                ? `fa fa-refresh fa-spin ${styles.search_input_icon}`
                : `fa fa-search ${styles.search_input_icon}`
            }
            onClick={() => searchData !== "" && handleSearch()}
          />
          <i
            aria-hidden="true"
            className={`fa fa-close ${styles.clear_input_icon}`}
            onClick={() => {
              if (searchData !== "") {
                setSearchData("");
                handleSearch("");
              }
            }}
          />
        </FormGroup>
      </Form>
    </RFlex>
  );
};

export default RRoleHeader;
