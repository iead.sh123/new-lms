import React, { useRef, useState } from "react";
import Helper from "components/Global/RComs/Helper";
import RForm from "components/Global/RComs/RForm";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";

export default function RFormModal({
  open,
  setOpen,
  actionButtonText,
  title,
  initialformItems,
  onFormSubmit,
  submitLabel,
  withCloseIcon=true,
  modalSize,
  alignActionButtons,
  actionButtonDisabled
}) {
  const formRef = useRef();
  const submitForm = () => {
    if (formRef.current) {
      formRef.current.dispatchEvent(
        new Event("submit", { cancelable: true, bubbles: true })
      );
    }
  };

  return (
    <>
      {/* {open?"open true":"open false"} */}
      <RModal
        isOpen={open}
        withCloseIcon={withCloseIcon}
        toggle={() => setOpen(false)}
        backdrop="static"
        header={<div className="text-capitalize" style={{fontSize:"16px",margin:"15px"}}>{title}</div>}
        footer={
          <div style={{float:alignActionButtons??"left",    margin: "0px"}}>
            <RButton
              text={actionButtonText}
              onClick={submitForm}
              color="primary"
              disabled={actionButtonDisabled}
            />
            <RButton
              text={tr`cancel`}
              onClick={() => {
                setOpen(false);
              }}
              color="white"
              style={{
                color: "#668ad7",
                background: "white",
                border: "0px solid",
              }}
            />
          </div>
        }
        footerStyle={{ justifyContent: 'flex-start',marginLeft:"20px" }}
        body={
          <div id="crateGroupForm">
            <RForm
              formItems={initialformItems}
              submitLabel={submitLabel??"Create"}
              withSubmit={false}
              onSubmit={onFormSubmit}
              formRef={formRef}
            />
          </div>
        }
        size={modalSize??"lg"}
      />
    </>
  );
}
