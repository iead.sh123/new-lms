import styles from "./styles/default.Module.scss";
import React from "react";
import { Col, ButtonGroup, Button } from "reactstrap";
import UserFirstLetter from "views/Teacher/IM/userFirstLetter";
// import UserFirstLetter from "components/Global/IM/Components/userFirstLetter";

const ButtonGroup_default = ({ buttons, activeButtonId, countEvent }) => (
  <>
    {buttons?.map((child) => (
      // <Col key={child.id} md={3} xs={6}>
      <Button
        className={
          child.id === activeButtonId
            ? styles["active-button"]
            : styles["button"]
        }
        key={child.id}
        active={child.id === activeButtonId}
        onClick={child.onClick}
        style={{ display: "flex" }}
        color={"primary"}
      >
        <UserFirstLetter str={child.text} width={30} top={5} />
        <p
          style={{
            top: "6px",
            position: "relative",
            padding: "0px 5px 0px 5px",
            color: "black",
          }}
        >
          {child.text}
        </p>
        {countEvent !== 0 && (
          <p
            style={{
              top: "6px",
              position: "relative",
              padding: "0px 3px 0px 3px",
              background: "white",
              color: "black",
            }}
          >
            {child.id === activeButtonId && countEvent}
          </p>
        )}
      </Button>
      // {/* </Col> */}
    ))}
  </>
);

export default ButtonGroup_default;
