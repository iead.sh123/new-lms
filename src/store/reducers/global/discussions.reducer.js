import {
  INITIAL_DATA_TO_CREATE_POST,
  GET_VALIDATION_CREATE_POST,
  GET_HOME_PAGE_POSTS_REQUEST,
  GET_HOME_PAGE_POSTS_SUCCESS,
  GET_HOME_PAGE_POSTS_ERROR,
  SET_CREATE_POST_VALUE,
  SET_VALIDATION_TO_POST,
  SET_CREATE_COMMENT,
  SET_EDIT_CONTENT_VALUE,
  ADD_POST_REQUEST,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
  EDIT_POST_REQUEST,
  EDIT_POST_SUCCESS,
  EDIT_POST_ERROR,
  REMOVE_POST_REQUEST,
  REMOVE_POST_SUCCESS,
  REMOVE_POST_ERROR,
  GET_POST_BY_ID_REQUEST,
  GET_POST_BY_ID_SUCCESS,
  GET_POST_BY_ID_ERROR,
  EMPTY_POSTS_ARRAY,
  EMPTY_PEOPLE_LIKES,
  SEARCH_IN_REQUEST,
  SEARCH_IN_SUCCESS,
  SEARCH_IN_ERROR,
  GET_ALL_CATEGORIES_REQUEST,
  GET_ALL_CATEGORIES_SUCCESS,
  GET_ALL_CATEGORIES_ERROR,
  CHOOSE_SPECIFIC_CATEGORY_REQUEST,
  CHOOSE_SPECIFIC_CATEGORY_SUCCESS,
  CHOOSE_SPECIFIC_CATEGORY_ERROR,
  GET_ALL_REPORT_CATEGORIES_REQUEST,
  GET_ALL_REPORT_CATEGORIES_SUCCESS,
  GET_ALL_REPORT_CATEGORIES_ERROR,
  SAVE_REPORT_ON_CONTENT_REQUEST,
  SAVE_REPORT_ON_CONTENT_SUCCESS,
  SAVE_REPORT_ON_CONTENT_ERROR,
  ACCEPT_CONTENT_REQUEST,
  ACCEPT_CONTENT_SUCCESS,
  ACCEPT_CONTENT_ERROR,
  REJECT_CONTENT_REQUEST,
  REJECT_CONTENT_SUCCESS,
  REJECT_CONTENT_ERROR,
  IGNORE_REPORTED_CONTENT_REQUEST,
  IGNORE_REPORTED_CONTENT_SUCCESS,
  IGNORE_REPORTED_CONTENT_ERROR,
  DELETE_REPORTED_CONTENT_REQUEST,
  DELETE_REPORTED_CONTENT_SUCCESS,
  DELETE_REPORTED_CONTENT_ERROR,
  DELETE_REJECTED_CONTENT_REQUEST,
  DELETE_REJECTED_CONTENT_SUCCESS,
  DELETE_REJECTED_CONTENT_ERROR,
  GET_CONTENT_BY_CONTENT_ID_REQUEST,
  GET_CONTENT_BY_CONTENT_ID_SUCCESS,
  GET_CONTENT_BY_CONTENT_ID_ERROR,
  EDIT_CONTENT_REQUEST,
  EDIT_CONTENT_SUCCESS,
  EDIT_CONTENT_ERROR,
  SET_REPORTING_ON_PORT,
  SAVE_REPORTING_ON_POST_REQUEST,
  SAVE_REPORTING_ON_POST_SUCCESS,
  SAVE_REPORTING_ON_POST_ERROR,
  EMPTY_REPORTING_ON_POST,
  GET_POSTS_CREATED_BY_SPECIFIC_USER_REQUEST,
  GET_POSTS_CREATED_BY_SPECIFIC_USER_SUCCESS,
  GET_POSTS_CREATED_BY_SPECIFIC_USER_ERROR,
  ADD_LIKE_ON_POET_OR_COMMENT_REQUEST,
  ADD_LIKE_ON_POET_OR_COMMENT_SUCCESS,
  ADD_LIKE_ON_POET_OR_COMMENT_ERROR,
  SEE_ALL_PEOPLE_WHO_LIKE_REQUEST,
  SEE_ALL_PEOPLE_WHO_LIKE_SUCCESS,
  SEE_ALL_PEOPLE_WHO_LIKE_ERROR,
  GET_ALL_MANDATORY_POSTS_POSTS_REQUEST,
  GET_ALL_MANDATORY_POSTS_POSTS_SUCCESS,
  GET_ALL_MANDATORY_POSTS_POSTS_ERROR,
  ADD_COMMENT_REQUEST,
  ADD_COMMENT_SUCCESS,
  ADD_COMMENT_ERROR,
  UPDATE_COMMENT_REQUEST,
  UPDATE_COMMENT_SUCCESS,
  UPDATE_COMMENT_ERROR,
  REMOVE_COMMENT_REQUEST,
  REMOVE_COMMENT_SUCCESS,
  REMOVE_COMMENT_ERROR,
  EMPTY_COMMENT,
  LOAD_PREVIOUS_COMMENTS_REQUEST,
  LOAD_PREVIOUS_COMMENTS_SUCCESS,
  LOAD_PREVIOUS_COMMENTS_ERROR,
  EMPTY_LOAD_PREVIOUS_COMMENT,
  EMPTY_POST_BY_ID,
  CHANGE_FLAG_SPECIFIC_CONTENT_LOAD,
  GET_COMMENT_BY_ID_REQUEST,
  GET_COMMENT_BY_ID_SUCCESS,
  GET_COMMENT_BY_ID_ERROR,
} from "store/actions/global/globalTypes";
import produce from "immer";

const InitState = {
  posts: [],
  mostReactedPost: {},
  mandatoryPosts: [],
  createPost: {},
  createComment: {},
  editPost: {},
  reportingOnPost: {},
  createPostValidation: {},
  categories: [],
  allReportCategories: [],
  specificPosts: [],
  seeAllPeople: [],
  addComment: {},
  previousComments: {
    comments: [],
    content_id: null,
    id: null,
    last_comment_id: null,
  },
  test: null,

  loadMorePage: 1,
  loadMoreComment: null,
  loadMoreLikePage: 1,
  postsLoading: false,
  addPostLoading: false,
  getPostByIdLoading: false,
  categoriesLoading: false,
  searchLoading: false,
  allReportCategoriesLoading: false,
  saveReportOnContentLoading: false,
  acceptLoading: false,
  rejectLoading: false,
  ignoreReportedLoading: false,
  deleteReportedLoading: false,
  deleteRejectedLoading: false,
  editPostLoading: false,
  reportingOnPostLoading: false,
  specificPostsLoading: false,
  saveEditToContent: false,
  addCommentLoading: false,
  updateCommentLoading: false,
  removeCommentLoading: false,
  loadPreviousLoading: false,
  specificContentLoad: false,
  getCommentByIdLoading: false,
};

let ind;
let postInd;
let commentInd;

const Search = (items, contentId) => {
  let result = false;
  for (let ii = 0; ii < items.length; ii++) {
    const i = items[ii];

    if (i.content_id == contentId) {
      result = i;
      return i;
    } else if (i.comments && i.comments.length > 0) {
      const s = Search(i.comments, contentId);
      if (s) {
        result = s;
        return s;
      }
    }
  }
  result = false;
  return result;
};

const discussionsReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case INITIAL_DATA_TO_CREATE_POST:
        draft.createPost = action.payload.post;
        return draft;

      case GET_VALIDATION_CREATE_POST:
        draft.createPostValidation = action.payload.initialValidationData;
        return draft;

      case SET_CREATE_POST_VALUE:
        draft.createPost[action.payload.name] = action.payload.value;
        return draft;

      case SET_EDIT_CONTENT_VALUE:
        draft.editPost[action.payload.name] = action.payload.value;
        return draft;

      case SET_REPORTING_ON_PORT:
        draft.reportingOnPost[action.payload.name] = action.payload.value;
        return draft;

      case SET_VALIDATION_TO_POST:
        if (action.payload.name == undefined) {
          draft.createPostValidation = action.payload.message;
        } else {
          draft.createPostValidation[action.payload.name] =
            action.payload.message;
        }
        return draft;

      case GET_HOME_PAGE_POSTS_REQUEST:
        draft.postsLoading = true;
        return draft;
      case GET_HOME_PAGE_POSTS_SUCCESS:
        draft.postsLoading = false;
        draft.loadMorePage = action.payload.loadMorePage;
        draft.mostReactedPost = action.payload.mostReactedPost;
        draft.mandatoryPosts = action.payload.mandatoryPosts;
        draft.posts.push(...action.payload.posts);
        return draft;
      case GET_HOME_PAGE_POSTS_ERROR:
        draft.postsLoading = false;
        return draft;

      case SEARCH_IN_REQUEST:
        draft.searchLoading = true;
        return draft;
      case SEARCH_IN_SUCCESS:
        draft.searchLoading = false;
        draft.loadMorePage = action.payload.loadMorePage;
        draft.posts.push(...action.payload.posts);
        return draft;
      case SEARCH_IN_ERROR:
        draft.searchLoading = false;
        return draft;

      case ADD_POST_REQUEST:
        draft.addPostLoading = true;
        return draft;
      case ADD_POST_SUCCESS:
        draft.addPostLoading = false;
        return draft;
      case ADD_POST_ERROR:
        draft.addPostLoading = false;
        return draft;

      case EDIT_POST_REQUEST:
        draft.addPostLoading = true;
        return draft;
      case EDIT_POST_SUCCESS:
        draft.addPostLoading = false;
        return draft;
      case EDIT_POST_ERROR:
        draft.addPostLoading = false;
        return draft;

      case GET_POST_BY_ID_REQUEST:
        draft.getPostByIdLoading = true;
        return draft;
      case GET_POST_BY_ID_SUCCESS:
        draft.getPostByIdLoading = false;
        draft.createPost = action.payload.data;
        return draft;
      case GET_POST_BY_ID_ERROR:
        draft.getPostByIdLoading = false;
        return draft;

      case GET_COMMENT_BY_ID_REQUEST:
        draft.getCommentByIdLoading = true;
        return draft;
      case GET_COMMENT_BY_ID_SUCCESS:
        draft.getCommentByIdLoading = false;
        draft.createComment = action.payload.data;
        return draft;
      case GET_COMMENT_BY_ID_ERROR:
        draft.getCommentByIdLoading = false;
        return draft;

      case EMPTY_POSTS_ARRAY:
        draft.posts = action.payload.data;
        return draft;

      case EMPTY_PEOPLE_LIKES:
        draft.seeAllPeople = action.payload.data;
        return draft;

      case GET_ALL_CATEGORIES_REQUEST:
        draft.categoriesLoading = true;
        return draft;
      case GET_ALL_CATEGORIES_SUCCESS:
        draft.categoriesLoading = false;
        draft.categories = action.payload.data;
        return draft;
      case GET_ALL_CATEGORIES_ERROR:
        draft.categoriesLoading = false;
        return draft;

      case CHOOSE_SPECIFIC_CATEGORY_REQUEST:
        draft.postsLoading = true;
        return draft;
      case CHOOSE_SPECIFIC_CATEGORY_SUCCESS:
        draft.postsLoading = false;
        draft.loadMorePage = action.payload.loadMorePage;
        draft.posts.push(...action.payload.posts);
        return draft;
      case CHOOSE_SPECIFIC_CATEGORY_ERROR:
        draft.postsLoading = false;
        return draft;

      case GET_ALL_REPORT_CATEGORIES_REQUEST:
        draft.allReportCategoriesLoading = true;
        return draft;
      case GET_ALL_REPORT_CATEGORIES_SUCCESS:
        draft.allReportCategoriesLoading = false;
        draft.allReportCategories = action.payload.data;
        return draft;
      case GET_ALL_REPORT_CATEGORIES_ERROR:
        draft.allReportCategoriesLoading = false;
        return draft;

      case SAVE_REPORT_ON_CONTENT_REQUEST:
        draft.saveReportOnContentLoading = true;
        return draft;
      case SAVE_REPORT_ON_CONTENT_SUCCESS:
        draft.saveReportOnContentLoading = false;
        return draft;
      case SAVE_REPORT_ON_CONTENT_ERROR:
        draft.saveReportOnContentLoading = false;
        return draft;

      case ACCEPT_CONTENT_REQUEST:
        draft.acceptLoading = true;
        return draft;
      case ACCEPT_CONTENT_SUCCESS:
        draft.acceptLoading = false;
        return draft;
      case ACCEPT_CONTENT_ERROR:
        draft.acceptLoading = false;
        return draft;

      case REJECT_CONTENT_REQUEST:
        draft.rejectLoading = true;
        return draft;
      case REJECT_CONTENT_SUCCESS:
        draft.rejectLoading = false;
        return draft;
      case REJECT_CONTENT_ERROR:
        draft.rejectLoading = false;
        return draft;

      case IGNORE_REPORTED_CONTENT_REQUEST:
        draft.ignoreReportedLoading = true;
        return draft;
      case IGNORE_REPORTED_CONTENT_SUCCESS:
        draft.ignoreReportedLoading = false;
        return draft;
      case IGNORE_REPORTED_CONTENT_ERROR:
        draft.ignoreReportedLoading = false;
        return draft;

      case DELETE_REPORTED_CONTENT_REQUEST:
        draft.deleteReportedLoading = true;
        return draft;
      case DELETE_REPORTED_CONTENT_SUCCESS:
        draft.deleteReportedLoading = false;
        return draft;
      case DELETE_REPORTED_CONTENT_ERROR:
        draft.deleteReportedLoading = false;

      case DELETE_REJECTED_CONTENT_REQUEST:
        draft.deleteRejectedLoading = true;
        return draft;
      case DELETE_REJECTED_CONTENT_SUCCESS:
        draft.deleteRejectedLoading = false;
        return draft;
      case DELETE_REJECTED_CONTENT_ERROR:
        draft.deleteRejectedLoading = false;
        return draft;

      case GET_CONTENT_BY_CONTENT_ID_REQUEST:
        draft.editPostLoading = true;
        return draft;
      case GET_CONTENT_BY_CONTENT_ID_SUCCESS:
        draft.editPostLoading = false;
        draft.editPost = action.payload.data;
        return draft;
      case GET_CONTENT_BY_CONTENT_ID_ERROR:
        draft.editPostLoading = false;
        return draft;

      case EDIT_CONTENT_REQUEST:
        draft.saveEditToContent = true;
        return draft;
      case EDIT_CONTENT_SUCCESS:
        draft.saveEditToContent = false;
        return draft;
      case EDIT_CONTENT_ERROR:
        draft.saveEditToContent = false;
        return draft;

      case SAVE_REPORTING_ON_POST_REQUEST:
        draft.reportingOnPostLoading = true;
        return draft;
      case SAVE_REPORTING_ON_POST_SUCCESS:
        draft.reportingOnPostLoading = false;
        return draft;
      case SAVE_REPORTING_ON_POST_ERROR:
        draft.reportingOnPostLoading = false;
        return draft;

      case EMPTY_REPORTING_ON_POST:
        draft.reportingOnPost = action.payload.data;
        return draft;

      case GET_POSTS_CREATED_BY_SPECIFIC_USER_REQUEST:
        // draft.specificPostsLoading = true;
        draft.postsLoading = true;
        return draft;
      case GET_POSTS_CREATED_BY_SPECIFIC_USER_SUCCESS:
        // draft.specificPostsLoading = false;
        // draft.loadMorePage = action.payload.loadMorePage;
        // draft.specificPosts.push(...action.payload.posts);

        draft.postsLoading = false;
        draft.loadMorePage = action.payload.loadMorePage;
        draft.posts.push(...action.payload.posts);

        return draft;
      case GET_POSTS_CREATED_BY_SPECIFIC_USER_ERROR:
        // draft.specificPostsLoading = false;
        draft.postsLoading = false;
        return draft;

      case ADD_LIKE_ON_POET_OR_COMMENT_REQUEST:
        draft.addLikeLoading = true;
        return draft;
      case ADD_LIKE_ON_POET_OR_COMMENT_SUCCESS:
        draft.addLikeLoading = false;

        const itemObj = Search(
          action.payload.data.postType == "mandatory"
            ? draft.mandatoryPosts
            : action.payload.data.postType == "mostReacted"
            ? [draft.mostReactedPost]
            : action.payload.data.postType == "all"
            ? draft.posts
            : [],
          action.payload.data.content_id
        );

        itemObj.nb_likes = action.payload.likeCount;
        itemObj.is_liked = action.payload.isLiked;

        return draft;
      case ADD_LIKE_ON_POET_OR_COMMENT_ERROR:
        draft.addLikeLoading = false;
        return draft;

      case SEE_ALL_PEOPLE_WHO_LIKE_REQUEST:
        draft.seeAllPeopleLoading = true;
        return draft;
      case SEE_ALL_PEOPLE_WHO_LIKE_SUCCESS:
        draft.seeAllPeopleLoading = false;
        draft.loadMoreLikePage = action.payload.loadMoreLikePage;
        draft.seeAllPeople.push(...action.payload.seeAllPeople);
        return draft;
      case SEE_ALL_PEOPLE_WHO_LIKE_ERROR:
        draft.seeAllPeopleLoading = false;
        return draft;

      case GET_ALL_MANDATORY_POSTS_POSTS_REQUEST:
        draft.postsLoading = true;
        return draft;
      case GET_ALL_MANDATORY_POSTS_POSTS_SUCCESS:
        draft.postsLoading = false;
        draft.loadMorePage = action.payload.loadMorePage;
        draft.posts.push(...action.payload.posts);
        return draft;
      case GET_ALL_MANDATORY_POSTS_POSTS_ERROR:
        draft.postsLoading = false;
        return draft;

      case ADD_COMMENT_REQUEST:
        draft.addCommentLoading = true;
        return draft;
      case ADD_COMMENT_SUCCESS:
        draft.addCommentLoading = false;
        return draft;
      case ADD_COMMENT_ERROR:
        draft.addCommentLoading = false;
        return draft;

      case UPDATE_COMMENT_REQUEST:
        draft.updateCommentLoading = true;
        return draft;
      case UPDATE_COMMENT_SUCCESS:
        draft.updateCommentLoading = false;
        return draft;
      case UPDATE_COMMENT_ERROR:
        draft.updateCommentLoading = false;
        return draft;

      case REMOVE_COMMENT_REQUEST:
        draft.removeCommentLoading = true;
        return draft;
      case REMOVE_COMMENT_SUCCESS:
        draft.removeCommentLoading = false;
        return draft;
      case REMOVE_COMMENT_ERROR:
        draft.removeCommentLoading = false;
        return draft;

      case SET_CREATE_COMMENT:
        draft.addComment[action.payload.name] = action.payload.value;
        return draft;

      case EMPTY_COMMENT:
        draft.addComment = action.payload.data;
        return draft;

      case LOAD_PREVIOUS_COMMENTS_REQUEST:
        draft.loadPreviousLoading = true;
        return draft;
      case LOAD_PREVIOUS_COMMENTS_SUCCESS:
        // draft.loadPreviousLoading = false;
        // draft.loadMoreComment = action.payload.lastCommentId;
        // if (action.payload.specificPost) {
        //   draft.createPost.comments.push(...action.payload.data);
        // } else if (action.payload.specificContent) {
        //   if (!draft.specificContentLoad) {
        //     draft.previousComments = action.payload.allData;
        //     draft.specificContentLoad = true;
        //   } else {

        //     draft.previousComments.comments.push(...action.payload.data);
        //   }
        // }

        // draft.previousComments.comments.push(
        //   ...draft.previousComments.comments,
        //   action.payload.allData.comments
        // );
        // draft.previousComments.content_id =
        //   action.payload.allData.content_id;
        // draft.previousComments.id = action.payload.allData.id;
        // draft.previousComments.last_comment_id =
        //   action.payload.allData.last_comment_id;

        draft.loadPreviousLoading = false;
        draft.loadMoreComment = action.payload.lastCommentId;
        if (action.payload.specificPost) {
          if (action.payload.typeSeeMore == "comment") {
            draft.createPost.comments.push(...action.payload.data);
            draft.createPost.last_comment_id = action.payload.lastCommentId;
          } else {
            ind = draft.createPost.comments.findIndex(
              (el) => el.content_id == action.payload.contentId
            );
            if (ind !== -1) {
              draft.createPost.comments[ind].comments.push(
                ...action.payload.data
              );
              draft.createPost.comments[ind].last_comment_id =
                action.payload.lastCommentId;
            }
          }
        } else {
          if (action.payload.typeSeeMore == "comment") {
            draft.createPost.comments.push(...action.payload.data);
            draft.createPost.last_comment_id = action.payload.lastCommentId;
          } else {
            ind = draft.createPost.comments.findIndex(
              (el) => el.content_id == action.payload.contentId
            );
            if (ind !== -1) {
              draft.createPost.comments[ind].comments.push(
                ...action.payload.data
              );
              draft.createPost.comments[ind].last_comment_id =
                action.payload.lastCommentId;
            }
          }
          // if (draft.specificContentLoad == false) {

          //   draft.previousComments = action.payload.allData;
          //   draft.specificContentLoad = true;
          // } else if (draft.specificContentLoad == true) {
          //   draft.previousComments.comments.push(...action.payload.data);
          //   draft.previousComments.last_comment_id =
          //     action.payload.lastCommentId;
          // }
        }
        return draft;
      case LOAD_PREVIOUS_COMMENTS_ERROR:
        draft.loadPreviousLoading = false;
        return draft;

      case EMPTY_LOAD_PREVIOUS_COMMENT:
        draft.previousComments = {};
        return draft;

      case EMPTY_POST_BY_ID:
        draft.createPost = {};
        return draft;

      case CHANGE_FLAG_SPECIFIC_CONTENT_LOAD:
        draft.specificContentLoad = action.payload.flag;
        return draft;

      default:
        return draft;
    }
  });

export default discussionsReducer;
