import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RUnpublished.css'
function RUnpublished({icon,course,subName,publishCallback}){
  
  const tri=()=>               <svg xmlns="http://www.w3.org/2000/svg" width="15" height="14" viewBox="0 0 15 14" fill="none">
  <g clip-path="url(#clip0_10524_88718)">
  <path d="M14.7091 11.613C15.1578 12.4149 14.5946 13.4173 13.6986 13.4173H2.03469C1.13691 13.4173 0.576376 12.4133 1.02413 11.613L6.85615 1.18517C7.305 0.382862 8.42921 0.384316 8.87726 1.18517L14.7091 11.613ZM7.86671 9.45703C7.24922 9.45703 6.74865 9.97325 6.74865 10.61C6.74865 11.2468 7.24922 11.763 7.86671 11.763C8.48419 11.763 8.98476 11.2468 8.98476 10.61C8.98476 9.97325 8.48419 9.45703 7.86671 9.45703ZM6.80521 5.31262L6.98551 8.72147C6.99394 8.88099 7.12184 9.00586 7.27674 9.00586H8.45667C8.61157 9.00586 8.73947 8.88099 8.7479 8.72147L8.9282 5.31262C8.93732 5.14032 8.80429 4.99544 8.63697 4.99544H7.09641C6.92909 4.99544 6.79609 5.14032 6.80521 5.31262Z" fill="#F58B1F"/>
  </g>
  <defs>
  <clipPath id="clip0_10524_88718">
  <rect width="14" height="12.8333" fill="white" transform="translate(0.866699 0.583984)"/>
  </clipPath>
  </defs>
  </svg>



    return(
        <div style={{margin:'1rem'}}>
    {/* {icon},{course},{subName} */}
        <Row className="main_Row" > 
           <Col  xs lg="2" className="unpublished_icon_container">
              <div className="unpublished_icon"> 
              <div>
              <i className={icon}></i>  
              </div>
              </div>
           </Col>
           <Col   lg="8" >
              <Row className="course"  > <Col>{course}  </Col></Row>
              <Row> 
                <Col >
                   {tri()}
                    <span  className="Lesson_title1">{subName}</span> 
                   <div class={"publish_button"} onClick={publishCallback}>
                   publish
                   </div>
               </Col>
            
              </Row>
              <Row> 
                {/* <Col >
                  {leftTime?<span className="left_time">{leftTime} Left !</span> :''}  
                </Col> */}
            
              </Row>
           </Col>
           <Col lg="2" className="join" style={{display:"flex",alignItems:"center",justifyContent:"center" ,fontSize:"30px",color:'rgb(143,143,143)'}}>
            <i className="fas fa-angle-right"> </i> 
           </Col>
          </Row>
         
        </div>
    )
}
export default RUnpublished;