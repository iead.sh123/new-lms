import Helper from "components/Global/RComs/Helper";
import RCard1 from "components/Global/RComs/RCards/RCard1";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import REmptyData from "components/RComponents/REmptyData";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import { Services } from "engine/services";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { SwiperSlide } from "swiper/react";
import { relativeDate } from "utils/dateUtil";

const MeetingLine = ({ meetingid }) => {
	const history = useHistory();
	const [link, setLink] = useState("");

	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.meeting.backend + `api/meeting/${meetingid}/status`,
				"fail to get",
				async (response) => {
					Helper.cl(response, "meeting 2 resposne");
					//----------------------------------------
					await Helper.fastGet(
						Services.meeting.backend + `api/meeting/${meetingid}/link/attendee`,
						"fail to get",
						(response1) => {
							Helper.cl(response1, "meetin 1 response1");
							setLink(response1.data?.data);
						},
						() => {}
					);
				},
				//---
				() => {}
			);
		};
		get_it();
	}, []);

	//  return <> {Helper.js(meetingid)} </>
	return link && link.length > 0 ? (
		<button
			style={{ border: "0px solid black", color: "red", textDecoration: "underline" }}
			onClick={() => {
				// history.push(link);
				window.open(link, "_blank");
			}}
		>
			join link
		</button>
	) : (
		<></>
	);
};
const GTodayLessons = ({ lessons, withProgress, mode }) => {
	const join = () => {
		alert("join");
	};
	//   {
	//     "_id": "64ca17ba69bccdcf79031892",
	//     "name": "string",
	//     "creator_id": "1",
	//     "organization_id": "1",
	//     "is_master": true,
	//     "is_published": false,
	//     "color": null,
	//     "date_Synchronized": [],
	//     "updated_at": "2023-08-02T08:45:45.000000Z",
	//     "created_at": "2023-08-02T08:45:43.000000Z",
	//     "id": 296,
	//     "cohort_id": 204,
	//     "category_id": null,
	//     "term_id": 1,
	//     "curriculum_id": 1,
	//     "extra": {
	//         "synchronous": false,
	//         "isFree": false,
	//         "closed": false,
	//         "isOneDayCourse": false,
	//         "comingSoon": false,
	//         "language": [],
	//         "isOnline": false,
	//         "numberOfHours": null,
	//         "isPrivate": false,
	//         "start_date": null,
	//         "end_date": null
	//     },
	//     "tags": [],
	//     "image_id": null,
	//     "icon_id": null,
	//     "nextLesson": {
	//         "id": 501,
	//         "subject": "lesson2",
	//         "lesson_date": "2023-10-17 00:00:00",
	//         "end_date": null,
	//         "lesson_order": 2,
	//         "lesson_time": "11:26:00",
	//         "published": 1,
	//         "module_id": 138,
	//         "lesson_plan_id": null,
	//         "creator_id": 1,
	//         "created_at": "2023-10-17T05:26:44.000000Z",
	//         "updated_at": "2023-10-17T05:26:44.000000Z"
	//     },
	//     "todayOrTommorrowUnpublishedLessons": [],
	//     "students": 1,
	//     "lessons": 2,
	//     "posts": 0,
	//     "curriculum_name": null,
	//     "grade_level_name": null,
	//     "main_course_name": null
	// }
	const history = useHistory();
	Helper.cl(lessons, "lessons");
	const lessonCards =
		lessons &&
		lessons
			.filter((l) => l.nextLesson)
			.map((l) => {
				// Extract the date and time values from the JSON object
				const lessonDate = l?.nextLesson?.lesson_date.split(" ")[0];
				const lessonTime = l?.nextLesson?.lesson_time;

				// Combine the date and time
				const actual_time = moment(`${lessonDate} ${lessonTime}`).format("YYYY-MM-DD HH:mm:ss");
				Helper.cl(actual_time, "actual_time");
				// Create the output object
				// Output the result

				const startDate = moment();

				const endDate = startDate ? moment(actual_time) : null;
				const relativeTime = relativeDate(endDate); //endDate? startDate.from(endDate):null;
				//+actual_time;
				return {
					name: l.name,
					link: "/course/" + l.id,
					color: l.color ?? "black",
					users: l.students,
					notes: [
						<div style={{ color: "green" }}>
							{relativeTime ?? "rel time"}

							{!mode || mode == "teacher" ? (
								l.nextLesson?.livesession?.live_session?.meeting_id ? (
									<button
										style={{ border: "0px solid black", color: "red", textDecoration: "underline" }}
										onClick={() => {
											history.push(
												`${baseURL}/${genericPath}/course-management/course/${l.id}/cohort/${l.cohort_id}/lesson/${
													l.nextLesson?.id ?? l?.id
												}/meeting/${l.nextLesson?.livesession?.live_session?.meeting_id ?? l.livesession?.live_session?.meeting_id}`
											);
										}}
									>
										Manage
										{/* {l.nextLesson?.livesession?.live_session?.meeting_id} */}
										{/* {Helper.js(l.nextLesson)} */}
									</button>
								) : (
									<></>
								)
							) : mode == "student" ? (
								<MeetingLine meetingid={l.nextLesson?.livesession?.live_session?.meeting_id} />
							) : (
								// ((l.nextLesson?.livesession?.live_session?.meeting_id)?
								// <button style={{border:"0px solid black",color:"red",textDecoration:"underline"}}
								// onClick={()=>join(l.nextLesson?.livesession?.live_session?.meeting_id)}
								// >
								// JOIN
								// </button>:
								// <>

								// </>)
								<></>
							)}
						</div>,

						<div> </div>,
					],
					currentInfo: "",
					details: [
						{ icon: "fas fa-tasks", text: l.todayOrTommorrowUnpublishedLessons?.length + "tasks" },
						{ icon: "fas fa-person", text: l.students + "students" },
						{ icon: "fas fa-edit", text: l.posts + " posts" },
						{ icon: "fas fa-book", text: l.lessons + " lessons" },
					],

					id: l.id,
					image: l.image_id,
					categories: [<i className="fas fa-pen" />], // l.category_id,
					progress: withProgress ? 30 : null,
					lesson_name: l.nextLesson.subject,
					lesson: l.nextLesson,
					//rate: 4.5,
					// Add other properties as needed for your component
				};
			});
	// {
	//   link: '/course/3',
	//   color: 'red',
	//   users: 150,
	//   currentInfo: 'Enroll now',
	//   id: 3,
	//   image: 'course3.jpg',
	//   categoryName: 'Mobile App Development',
	//   rate: 4.7,
	//   // Add other properties as needed for your component
	// },
	// {
	//   link: '/course/4',
	//   color: 'purple',
	//   users: 80,
	//   currentInfo: 'Enroll now',
	//   id: 4,
	//   image: 'course4.jpg',
	//   categoryName: 'Machine Learning',
	//   rate: 4.6,
	//   // Add other properties as needed for your component
	// },
	//];

	return (
		<div style={{ display: "flex", gap: "20px", width: "100%", margin: 0, padding: 0 }}>
			{/* {Helper.js(lessonCards)} */}
			{/* {lessonCards?.length} */}

			{lessonCards && lessonCards.length ? (
				<RSwiper style={{ width: "100%" }}>
					{lessonCards?.map((item, i) => (
						<div>
							{/* lesson card {i} */}
							rcard1
							<SwiperSlide>
								<RCard1 key={item.id} {...item} />
							</SwiperSlide>
							{/* <SwiperSlide>
              <RCard1 key={item.id} {...item} />
            </SwiperSlide> */}
						</div>
					))}
				</RSwiper>
			) : (
				<REmptyData />
			)}
		</div>
	);
};

export default GTodayLessons;
