import React, { useState } from "react";
import Drawer from "react-modern-drawer";
import styles from "./sharedStyle.module.scss";
import RDrawerContent from "./RDrawerContent";
const RCSVDrawer = ({ toggleDrawer, isOpen, operationType, downloadTemplateFn, currentType }) => {
	return (
		<div>
			<Drawer open={isOpen} onClose={toggleDrawer} direction="right" className={styles.CSVDrawer}>
				<RDrawerContent operationType={operationType} downloadTemplateFn={downloadTemplateFn} currentType={currentType} />
			</Drawer>
		</div>
	);
};

export default RCSVDrawer;
