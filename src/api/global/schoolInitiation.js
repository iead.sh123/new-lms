import { Services } from "engine/services";
import { get, post } from "config/api";

export const schoolInitiationApi = {
  getSemestersByOrganization: (withOutPagination) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/organization/semesters${
        withOutPagination ? `?withOutPagination=${withOutPagination}` : ""
      }`
    ),

  educationStage: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/education-stage/getByOrganization?forDropDownList=${forDropDownList}`
    ),

  educationStageById: (educationStageId) =>
    get(
      `${Services.auth_organization_management.backend}api/education-stage/get/${educationStageId}`
    ),

  gradeLevelsById: (gradeLevelId) =>
    get(
      `${Services.auth_organization_management.backend}api/grade-level/view/${gradeLevelId}`
    ),

  curriculaById: (curriculumId) =>
    get(
      `${Services.auth_organization_management.backend}api/curricula/view/${curriculumId}`
    ),

  getGradeScales: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/grade-level/getGradeScalesList?forDropDownList=${forDropDownList}`
    ),

  getAppGradeLevels: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/grade-level/getAppGradeLevelsList?forDropDownList=${forDropDownList}`
    ),

  getMainCourses: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/main-courses/getAll?forDropDownList=${forDropDownList}`
    ),

  saveEducationStage: (data) =>
    post(
      `${Services.auth_organization_management.backend}api/education-stage/store`,
      data
    ),

  saveGradeLevel: (data) =>
    post(
      `${Services.auth_organization_management.backend}api/grade-level/store`,
      data
    ),

  saveCurricula: (data) =>
    post(
      `${Services.auth_organization_management.backend}api/curricula/store`,
      data
    ),

  tree: () =>
    get(
      `${Services.auth_organization_management.backend}api/organization/orgnizationInitiationTree`
    ),
};
