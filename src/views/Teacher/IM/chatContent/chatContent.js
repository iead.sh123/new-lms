import React from "react";
import MessageInput from "./MessageInput";
import MessegeItem from "./messegeItem";
import ChatHeader from "./chatHeader";
import moment from "moment";
import { MessageStatus } from "store/actions/IMCycle2.actions";
import Helper from "components/Global/RComs/Helper";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const ChatContent = ({
  showIcons,
  selectedMultipleMessages,
  chat,
  newMessageVisible,
  loggedUserId,
  loadMoreMessages,
  floating,
  sending,
  changePointer,
  sendMessage,
  handleSearch,
  searchedMessage,
  messageActions,
  setRepliedOnMessageId,
  repliedMessageId,
  toggleManagement,
}) => {
  const [fetchingMoreMessages, setFetchingMoreMessages] = React.useState(false);

  const divContentRef = React.useRef(null);
  React.useEffect(() => {
    Helper.cl("inital scroll down");
    setTimeout(() => {
      if (divContentRef.current) {
        // Scroll to the bottom by setting scrollTop to the maximum scroll height
        divContentRef.current.scrollTop = divContentRef.current.scrollHeight;
      }
    }, 1000);
  }, [chat]);

  React.useEffect(() => {
    Helper.cl("inital scroll down change of last id in messages".chat?.entities?.messages?.byId?.[chat.entities?.messages?.byId?.length - 1]);
    setTimeout(() => {
      if (divContentRef.current) {
        // Scroll to the bottom by setting scrollTop to the maximum scroll height
        divContentRef.current.scrollTop = divContentRef.current.scrollHeight;
      }
    }, 1000);
  }, [chat?.entities?.messages?.byId?.[chat.entities?.messages?.byId?.length - 1]]);

  React.useEffect(() => {
    if (sending && !newMessageVisible && divContentRef.current)
      divContentRef.current.scrollTop =
        divContentRef.current?.scrollHeight - 200;
  }, [sending]);

  React.useEffect(() => {
    chat?.id && !chat.loaded && handleFetchMoreMessages();
    changePointer?.();
  }, [chat?.id]);

  const handleFetchMoreMessages = async (e) => {

    if (
      Math.floor(e?.target.scrollHeight - e?.target.scrollTop) <=
      e?.target.clientHeight
    ) {


      setFetchingMoreMessages(true);
      const hasNext = await loadMoreMessages();
      setFetchingMoreMessages(false);
      hasNext &&
        divContentRef.current &&
        (divContentRef.current.scrollTop =
          divContentRef.current?.scrollHeight - 200);
    }

    if ((e && e.target.scrollTop !== 0) || fetchingMoreMessages) {

      return;
    }

    setFetchingMoreMessages(true); 
    const hasNext = await loadMoreMessages(true);
    setFetchingMoreMessages(false);

    hasNext && divContentRef.current && (divContentRef.current.scrollTop = 100);

  };

  const activeChunk = chat?.entities?.chunks?.byId[chat.activeChunk];

  let messageTimeGrouper = null;
  return (
    <RFlex id="Header and Chat Content" className="flex-column chatContent" styleProps={{margin:'0px', width: "74%", height: "100%" ,gap:'0px'}}>
      <ChatHeader
        loggedUserId={loggedUserId}
        users={
          chat?.name
            ? [{ name: chat?.name, image: chat?.image, id: chat?.id }]
            : Object.values(chat?.entities?.users?.byId ?? {})
        }
        floating={floating}
        chatId={chat?.id}
        chat={chat}
        handleSearch={handleSearch}
        openGroupManagementModal={() => toggleManagement()}
        close={close}
        messageActions={messageActions}
        selectedMultipleMessages={selectedMultipleMessages}
      />

      {/* {chat?.id && <GroupManagement isGroup={chat.name} chatId={chat?.id} isAdmin={chat.isAdmin} toggleModal={toggleManagementModal} loggedUserId={loggedUserId} isOpen={managementModalOpen} />} */}

      {/* {CurrentChat ? (
        CurrentChat.hideLoadMore ? null : (
          // <Button className="btn_load_more" onClick={() => loadmore()}>
            {/* {tr("Load More")} 
            <i className="fa fa-spinner fa-lg" aria-hidden="true"></i>
           </Button>
        )
      ) : null} */}
      <RFlex id="Only Chat Content" className="flex-column" styleProps={{height:'95%'}}  >
        <div
          id="Chat Content"
          className="Content_items"
          ref={divContentRef}
          onScroll={handleFetchMoreMessages}
        >
          {fetchingMoreMessages ? (
            <>
              {/* <Loader /> */}
              <i
                className="fa fa-refresh fa-spin"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  fontSize: "30px",
                }}
              />
            </>
          ) : (
            <></>
          )}
          <div id="messages_Containter" style={{paddingRight:'15px'}}>
            <div id="messages_Containter2">
              {chat?.entities?.messages?.ids
                // .filter(
                //   (id, ind) =>
                //     (chat?.entities?.messages?.byId[id].status !==
                //       MessageStatus.DELETED &&
                //       ind >=
                //         chat?.entities?.messages?.ids.indexOf(
                //           activeChunk.start
                //         ) &&
                //       ind <=
                //         chat?.entities.messages.ids.indexOf(activeChunk.end)) ||
                //     chat?.entities.messages.byId[id].status ===
                //       MessageStatus.PENDING ||
                //     chat?.entities?.messages?.byId[id].status ===
                //       MessageStatus.ERROR
                // )
                .map((i) => {
                  let messageYYMMDD = moment(
                    chat?.entities?.messages?.byId[i]?.created_at
                  ).format("YYYY:MM:DD");
                  let show = false;
                  if (messageYYMMDD !== messageTimeGrouper) {
                    show = true;
                    messageTimeGrouper = messageYYMMDD;
                  }
                  return (
                    <React.Fragment key={"msg-" + i}>
                      {/* mid={i}
                     { Helper.js(Object.values(
                          chat?.entities.users.byId
                        ).filter((usr) => 1==1).map(x=>x.pointer))} */}
                      {show ? (
                        <div
                          style={{
                            textAlign: "center",
                            fontWeight: "bold",
                            padding: '0px',
                            margin: '0px'
                          }}
                        >
                          <div
                            style={{ fontSize: "11px", margin: "auto", transform: "translateY(25px)", background: "white", width: "fit-content" }}
                          >
                            {messageYYMMDD}

                          </div>
                          <hr />
                        </div>
                      ) : (
                        <></>
                      )}
                      <MessegeItem
                        isGroupSelected={selectedMultipleMessages?.some(
                          (msid) => msid === i
                        )}
                        message={chat?.entities.messages.byId[i]}
                        searchedMessage={searchedMessage}
                        loggeduserid={loggedUserId}
                        user_pointers={Object.values(
                          chat?.entities.users.byId
                        ).filter((usr) => usr.pointer === i)}
                        user_name={
                          chat?.entities.users.byId?.[
                            chat?.entities?.messages?.byId?.[i]?.user_id
                          ]?.name
                        }
                        user_online={
                          chat?.entities.users.byId?.[
                            chat.entities.messages?.byId?.[i]?.user_id
                          ]?.online
                        }
                        userImg={
                          chat?.entities.users.byId?.[
                            chat.entities?.messages?.byId?.[i]?.user_id
                          ]?.image
                        }
                        actions={messageActions.map((ac) => ({
                          ...ac,
                          action: () => ac.action(chat.id, i),
                        }))}
                        showUsr={chat?.entities?.messages?.byId?.[i]?.user_id != loggedUserId

                          && chat?.entities?.messages?.byId?.[i - 1]?.user_id != chat?.entities?.messages.byId?.[i]?.user_id}
                      />
                    </React.Fragment>
                  );
                })}
            </div>
          </div>

          <div id="additional"></div>
        </div>

        <MessageInput
          sendMessage={(text, attachments) => {

            const f = sendMessage(text, attachments, null, null, repliedMessageId)

            setTimeout(() => {
              if (divContentRef.current) {
                // Scroll to the bottom by setting scrollTop to the maximum scroll height
                divContentRef.current.scrollTop = divContentRef.current.scrollHeight;
              }
            }, 1000);

            return f;
          }}
          sending={sending}
          chatId={chat?.id}
          repliedMessageId={repliedMessageId}
          chat={chat}
          setRepliedOnMessageId={setRepliedOnMessageId}
        />
      </RFlex>
    </RFlex>
  );
};

export default ChatContent;
