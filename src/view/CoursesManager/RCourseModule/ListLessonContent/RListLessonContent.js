import React, { useContext } from "react";
import Swal, { WARNING } from "utils/Alert";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RListLessonContent = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	const ListLessonContent = [
		{ title: "file", type: "attachment", icon: "fa fa-file-text" },
		{ title: "links", type: "link", icon: "fa fa-link" },
		{ title: "essay", type: "text", icon: "fa fa-file-text" },
		{ title: "live_session", type: "live_session", icon: "fa fa-headphones" },
		{ title: "lesson_plan", type: "lesson_plan", icon: "fa fa-plus" },
	];

	const confirmReplace = (list) => {
		CourseModuleData.handleSelectLessonContentType(list.type);
		CourseModuleData.handleAddItemsToStore(content, list.type, list.title);
		CourseModuleData.collapsesLessonContentToggle(0);
	};

	return (
		<RFlex className="m-4">
			{ListLessonContent.map((list, index) => (
				<RFlex
					styleProps={{ alignItems: "center" }}
					key={list.title}
					onClick={() => {
						if (!CourseModuleData.shouldNotBeAddedLessonContent) {
							if (content.contents.some((el) => el.type == "lesson_plan") && list.type == "lesson_plan") {
								Swal.confirm(
									tr`You already have a lesson plan for this lesson , Do you want to replace it ?`,
									tr``,
									WARNING,
									tr`ok`,
									() => confirmReplace(list),
									() => null
								);
							} else if (content.contents.some((el) => el.type == "live_session") && list.type == "live_session") {
								toast.warning(tr`You already have a live session for this lesson`);
							} else {
								CourseModuleData.handleSelectLessonContentType(list.type);
								CourseModuleData.handleAddItemsToStore(content, list.type, list.title);
								CourseModuleData.collapsesLessonContentToggle(0);
								// CourseModuleData.handleCreateQuestionSet(list.type);
							}
						}
						CourseModuleData.handleShowWarningMessage();
					}}
				>
					<i className={`${list.icon} fa-lg `} />
					<span>{tr(list.title)}</span>
					{index !== ListLessonContent.length - 1 && <span>|</span>}
				</RFlex>
			))}
		</RFlex>
	);
};

export default RListLessonContent;
