import React, { useEffect, useState } from "react";
import { TYPES } from "../assignmentReducer";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import MathJax from "react-mathjax-preview";
import { translate } from "../../../../utils/translate";

const MultiAnswerQuestion = ({
  choices,
  answers,
  question,
  groupId,
  dispatch,
  assignmentReadMode,
  mode,
  student,
}) => {
  const [selectedChoices, setSelectedChoices] = useState([]);
  const [selectedAnswerCorrectChoices, setSelectedAnswerCorrectChoices] =
    useState([]);

  useEffect(() => {
    if (assignmentReadMode && mode !== "multi") {
      if (question.notSolved === 1) {
        setSelectedChoices([]);
        setSelectedAnswerCorrectChoices([]);
        return;
      }
      setSelectedChoices(question.student_choices.map((choice) => choice.id));
      setSelectedAnswerCorrectChoices(
        choices.filter((choice) => choice.is_correct === 1)
      );
      return;
    }
    if (mode === "multi") {
      if (student.notSolved === 1 || student.notSolved === null) {
        setSelectedChoices([]);
        setSelectedAnswerCorrectChoices([]);

        return;
      }
      setSelectedChoices(student.student_choices.map((choice) => choice.id));
      setSelectedAnswerCorrectChoices(
        choices.filter((choice) => choice.is_correct === 1)
      );
      return;
    }

    if (answers && answers.length > 0) {
      let answer = answers.filter(
        (answer) => answer.QuestionId === question.id
      )[0];
      if (answer !== undefined) setSelectedChoices([...answer.Choices]);
    }
  }, [question]);

  useEffect(() => {
    if (assignmentReadMode) return;
    dispatch({
      type: TYPES.SAVE_ANSWER,
      payload: {
        QuestionId: question.id,
        GroupId: groupId,
        Choices: selectedChoices,
        FreeAnswer: null,
      },
    });
  }, [selectedChoices]);

  const changeMultiChoiceHandler = (e) => {
    const value = +e.target.value;

    let isChoiceSelected = selectedChoices.findIndex((selectedChoice) => {
      return selectedChoice === value;
    });

    if (isChoiceSelected !== -1) {
      setSelectedChoices(
        selectedChoices.filter((selectedChoice) => selectedChoice !== value)
      );
      return;
    }

    setSelectedChoices([...selectedChoices, value]);
  };

  const choiceColor = (choice) => {
    if (!assignmentReadMode) return;

    if (selectedChoices.length > 0 && mode !== "multi") {
      const selectChoice = question.student_choices.find(
        (selectedChoice) => selectedChoice.id === choice.id
      );

      if (selectChoice !== undefined) {
        return selectChoice.is_correct ? "correct_answer" : "wrong_answer";
      }
      const correctAnswers = selectedAnswerCorrectChoices.find(
        (correctAnswer) => correctAnswer.id !== choice.id
      );

      if (correctAnswers !== undefined) {
        return correctAnswers.id !== choice.id ? "correct_answer" : "";
      }

      return;
    }

    if (selectedChoices.length > 0 && mode === "multi") {
      const selectChoice = student.student_choices.find(
        (selectedChoice) => selectedChoice.id === choice.id
      );

      if (selectChoice !== undefined) {
        return selectChoice.is_correct ? "correct_answer" : "wrong_answer";
      }

      const correctAnswers = selectedAnswerCorrectChoices.find(
        (correctAnswer) => correctAnswer.id !== choice.id
      );

      if (correctAnswers !== undefined) {
        return correctAnswers.id !== choice.id ? "correct_answer" : "";
      }

      return;
    }

    return choice.is_correct ? "correct_answer" : "";
  };

  return choices.map((choice, index) => {
    return (
      <div
        key={index + choice.id}
        className="col-md-12 form-group row mt-2
            "
      >
        {/* {choice.is_correct} ////
        {choice.id} */}
        <div className={`${choice.attachments ? "col-md-7" : "col-md-11"}`}>
          <div className={`form-control ${choiceColor(choice)}`}>
            {choice.choice && choice.choice.includes("<math") ? (
              <MathJax math={choice.choice} />
            ) : (
              <div dangerouslySetInnerHTML={{ __html: choice.choice }} />
            )}
          </div>
        </div>
        <div className="col-md-1 text-center">
          <div className="custom-control custom-checkbox custom-control-inline mx-0 pl-3">
            <input
              name={`question${question.id}IsCorrect${choice.id}`}
              id={`question${question.id}IsCorrect${choice.id}`}
              type="checkbox"
              onChange={(e) => changeMultiChoiceHandler(e)}
              checked={selectedChoices.includes(choice.id)}
              className="custom-control-input assignment_checkbox"
              value={choice.id}
              disabled={assignmentReadMode}
            />
            <label
              htmlFor={`question${question.id}IsCorrect${choice.id}`}
              className="custom-control-label"
            ></label>
          </div>
        </div>
        {choice.attachments && (
          <div className="col-md-3">
            {["png", "jpeg"].includes(choice.attachments.mime_type) ? (
              <ImageEnlarge
                imageSrc={resourcesBaseUrl + choice.attachments.url}
              />
            ) : (
              <a
                href={resourcesBaseUrl + choice.attachments.url}
                alt=""
                class="btn btn-primary"
              >
                {translate("Download")}
              </a>
            )}
          </div>
        )}
      </div>
    );
  });
};

export default MultiAnswerQuestion;
