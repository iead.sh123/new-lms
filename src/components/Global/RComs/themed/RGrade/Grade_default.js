import { Col, Row } from "reactstrap";
import styles from "./styles/default.Module.scss";
import tr from "../../RTranslator";
import React from "react";

const Grade_default = ({ color, course_name, details, dir }) => (
  <Col md={6} sm={12}>
    <div className="card" dir={dir ? "ltr" : "rtl"}>
      <div
        className={dir ? styles.cardIndex_ltr : styles.cardIndex_rtl}
        style={{
          background: color,
        }}
      >
        <span className={dir ? "" : styles.text_rtl}>{course_name}</span>
      </div>
      <div className="card-body">
        <Row>
          {details.length != 0 ? (
            details?.map((detail) => (
              <React.Fragment key={detail.rab_id + detail.name}>
                <Col xs={6}>{detail.name}:</Col>
                <Col xs={6}>{detail.mark}</Col>
                <hr />
              </React.Fragment>
            ))
          ) : (
            <div
              style={{
                // top: "50%",
                // left: "30%",
                // position: "absolute",
                textAlign: "center",
                margin: "auto",
              }}
            >
              <h3
                style={{
                  color: color,
                }}
              >{tr`no_grade_yet`}</h3>
            </div>
          )}
        </Row>
      </div>
    </div>
  </Col>
);

export default Grade_default;
