import React from 'react';
import styles from "./styles/RCardLister_theme1.Module.scss";
import { Container, Col, Row } from "reactstrap";
import RCard from '../RCard/RCard';

const RCardListerTheme1 = ({ cards }) => {


    return <Container className={`content ${styles["CardLister"]}`} fluid>
        <Row>
            <Col>
                {
                    cards?.slice(0, cards.length / 2).map(c => <RCard {...c} />)
                }
            </Col>

            <Col>
                {
                    cards?.slice(cards.length / 2).map(c => <RCard {...c} />)
                }
            </Col>
        </Row>

    </Container>
}

export default RCardListerTheme1;