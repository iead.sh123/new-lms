import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { Rating } from "react-simple-star-rating";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { addFeedbackAsync } from "store/actions/global/coursesManager.action";
import { useDispatch } from "react-redux";
import tr from "components/Global/RComs/RTranslator";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import { Services } from "engine/services";
import styles from "../CoursesManager.module.scss";
import { relativeDate } from "utils/dateUtil";

const RFeedBack = ({ feedbacks, allFeedbackLoading }) => {
  const dispatch = useDispatch();
  const [searchData, setSearchData] = useState("");

  const handleSearch = (emptySearch) => {
    dispatch(addFeedbackAsync(courseId, emptySearch ?? searchData));
  };
  const handleChangeSearch = (text) => {
    setSearchData(text);
  };

  return (
    <Row>
      <Col xs={12} sm={4}>
        {Object.keys(feedbacks?.percentage).map((key) => {
          return (
            <div key={key}>
              <RFlex key={key + 1}>
                <Rating initialValue={key} size={25} readonly={true} />
                <p>{feedbacks?.percentage[key]}%</p>
              </RFlex>
            </div>
          );
        })}
      </Col>
      <Col xs={12} sm={8}>
        <RSearchHeader
          searchLoading={allFeedbackLoading}
          searchData={searchData}
          handleSearch={handleSearch}
          setSearchData={setSearchData}
          handleChangeSearch={handleChangeSearch}
          inputPlaceholder={tr`search`}
        />
        {feedbacks?.feedbacks?.map((feedback) => (
          <section
            key={feedback.id}
            className={`${styles.feedback_border} mt-4`}
          >
            <RFlex styleProps={{ justifyContent: "space-between" }}>
              <RFlex>
                <img
                  src={`${Services.storage.file}${feedback.user.image_hash_id}`}
                  alt={feedback.user.image_hash_id}
                  className={styles.user_img}
                />
                <h6 className="mt-3">{feedback.user.full_name}</h6>
              </RFlex>

              <p className="text-muted d-flex align-items-center pt-3">
                {relativeDate(feedback?.created_at)}
              </p>
            </RFlex>
            <RFlex>
              <p>{feedback?.feedback}</p>
            </RFlex>
            <RFlex className={"mb-3"}>
              {" "}
              <Rating
                initialValue={feedback.rating}
                size={25}
                readonly={true}
              />
            </RFlex>
          </section>
        ))}
      </Col>
    </Row>
  );
};

export default RFeedBack;
