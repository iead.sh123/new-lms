import React from "react";

const RTextBox = (props) => {


const {  id,
  onClick, onKeyDown,placeholder,required,style,Input,msgOnChange, ClassNames, ...otherProps } = props;


  return (
    <input
    {...otherProps}
      style={style??{}}
      required={required}
      type="text"
      className={ClassNames}
      value={Input}
      placeholder={placeholder}
      onChange={msgOnChange}
      onKeyDown={onKeyDown}
      {... onClick ? {onClick: onClick} : {}}
      id={id ? id : ""}
    />
  );
};

export default RTextBox;
