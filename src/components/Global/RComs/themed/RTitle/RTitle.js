import React from 'react';
import withTheme from 'hocs/withTheme';
import withDirection from 'hocs/withDirection';

const RTitle = theme => {

    let themedComponent = React.lazy(()=>
        (async ()=>{
            try{
                return await import(`./Title_${theme}`);
            }
            catch(err){
                return await import(`./Title_default`);;
            }
        })()
    )
    return themedComponent;
}



export default withDirection(withTheme(RTitle));