import React from "react";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import Loader from "utils/Loader";

const GClients = () => {
	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["clients"],
		queryFn: () => collaborationApi.getClients(),
	});
	if (isLoading) return <Loader />;

	return <div>GClients</div>;
};

export default GClients;
