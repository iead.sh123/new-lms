import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Services } from "engine/services";
import RCkEditor from "components/Global/RComs/RCkEditor";
import RButton from "components/Global/RComs/RButton";
import styles from "../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";

const SolveFillInBlank = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveFillInBlank;
