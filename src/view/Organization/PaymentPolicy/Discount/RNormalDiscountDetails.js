import React from "react";
import RDiscountCourses from "./RDiscountCourses";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor, successColor, dangerColor } from "config/constants";

const RNormalDiscountDetails = ({ discountDetails, coursesByDiscountId, handlers, states, loading }) => {
	const viewAsButton = (
		<RFlex
			id="view as"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr`sort_by`}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const viewAsFilterActions = [
		{ label: tr`alphabetically`, action: () => handlers.handleSortBy("alfa") },
		{ label: tr`recent`, action: () => handlers.handleSortBy("recent") },
	];

	const selectFilterActions = [
		{
			label: tr`all`,
			action: () => {
				handlers.handleSelectedAllCourse();
				handlers.handleShowActions(discountDetails?.id);
			},
		},
		{
			label: tr`clear_selection`,
			action: () => {
				handlers.handleClearSelectedCourses();
			},
		},
	];

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
				<RFlex styleProps={{ gap: "8px", alignItems: "center" }}>
					<span
						onClick={() => handlers.handleShowActions(discountDetails.id)}
						className="p-0 m-0 text-success"
						style={{ fontSize: "12px", cursor: "pointer" }}
					>
						{tr`select`}
					</span>
					<RDropdownIcon actions={selectFilterActions} iconContainerStyle={{ border: "1px solid" + successColor, borderRadius: "100%" }} />
					{states.showActions == discountDetails.id && (
						<RButton
							onClick={() => handlers.handleDeleteCourses(discountDetails.id)}
							className="m-0"
							text={tr`remove`}
							faicon={iconsFa6.delete}
							color="link"
							style={{ color: dangerColor }}
							disabled={states.values.allCourses.length > 0 ? false : true}
							loading={loading.deleteDiscountFromProductsLoading}
						/>
					)}
				</RFlex>
				<RDropdownIcon actions={viewAsFilterActions} component={viewAsButton} />
			</RFlex>

			{/* Courses */}
			<RFlex styleProps={{ flexDirection: "column" }}>
				<RFlex styleProps={{ justifyContent: "space-between" }}>
					<RFlex>
						<span style={{ fontWeight: "bold" }}>{tr`courses`}</span>
						<div className={styles.courses__count}>{coursesByDiscountId?.data?.length}</div>
					</RFlex>
					<span onClick={() => handlers.handleOpenCourseModal()} style={{ cursor: "pointer", color: primaryColor }}>{tr`add_courses`}</span>
				</RFlex>
				<RFlex styleProps={{ flexWrap: "wrap" }}>
					{coursesByDiscountId.data.map((course) => (
						<RDiscountCourses key={course.id} course={course} states={states} handlers={handlers} discountDetails={discountDetails} />
					))}
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RNormalDiscountDetails;
