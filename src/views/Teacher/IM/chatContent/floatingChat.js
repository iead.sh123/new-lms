import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SHOW_FLOATING_CHAT } from "store/actions/global/globalTypes";
import ChatContent from "./chatContent";

export default function FloatingChat(props) {
  const tn = useSelector((state) => {
    return state.chatReducer ? state.chatReducer.TableName : null;
  });
  const ri = useSelector((state) => {
    return state.chatReducer ? state.chatReducer.RowID : null;
  });
  const show = useSelector((state) => state.chat.showFloatingChat);
  const dispatch = useDispatch();
  const close = () => {
    dispatch({ type: SHOW_FLOATING_CHAT, state: false });
  };

  return (
    <div className={show ? "floating_chat_show" : "floating_chat_hide"}>
      <ChatContent
        floating="true"
        close={() => close()}
        selectedID={props.selectedID}
      />
      {/* <button onClick={()=>{
               dispatch({type:SHOW_FLOATING_CHAT,state:true})
           }}> float chat : {show}</button> */}
    </div>
  );
}
