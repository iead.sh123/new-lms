import React from "react";
import styles from './ImageCard.module.scss';
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const RImageCard = ({ id, image, icon, handleIconClicked, header , statistics }) => {


  return <div className={styles.Card}>
    <RFlex styleProps={{ flexDirection: 'column', fontSize: '16px' }}>
      <RFlex styleProps={{justifyContent: 'space-between'}}>
        <RFlex >
          <img src={image}
            style={{ objectFit: "cover", with: "47px", height: "47px" }}>
          </img>
        <p>{header}</p>
        </RFlex>
        {/* <div style={{alignSelf: 'end'}}>ss</div> */}
        {icon ? <FontAwesomeIcon icon={icon} onClick={handleIconClicked} /> : <></>}
      </RFlex>
      <RFlex styleProps={{ flexDirection: 'column', fontSize: '16px' , marginTop: '15px'  ,marginLeft: '5px'}}>
      {
        statistics.map((item, ind) => <RFlex key={'ImageCard-' + id + ind}>
          {item.icon ? <FontAwesomeIcon icon={item.icon} className="mt-1" /> : <></>}
          <p>{item.text}</p>
        </RFlex>)
      }
    </RFlex>
    </RFlex>

  </div>
}

export default RImageCard;