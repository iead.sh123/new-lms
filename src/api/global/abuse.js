import { get, post } from "config/api";

export const abuseApi = {
  getSingleAbuse: (abuseId) => get(`api/abuse-reports/${abuseId}`),
  addAbuse: (data) => post(`api/abuse-reports/store`, data),
  getUserTypes: () => get(`api/abuse-reports/users-types`),
  users: (usersTypes) => post(`api/abuse-reports/users`, usersTypes),
  getAbuseReports: () => get(`api/abuse-reports/cards `),
  seenAbuse: (reportId) => post(`api/abuse-reports/mark-seen`, reportId),
};
