import React from "react";
import EventCard from "view/TermsManager/cards/EventCard/EventCard";
import { Droppable } from "react-beautiful-dnd";

const DroppableCell = ({ id, events, actions, disableActions }) => {
	return (
		<Droppable droppableId={id} key={id} isDropDisabled={disableActions}>
			{(provided, snapshot) => {
				return (
					<div
						{...provided.droppableProps}
						ref={provided.innerRef}
						style={{
							background: snapshot.isDraggingOver ? "lightblue" : "#FFF",
							padding: 4,
							width: "100%",
							height: "100%",
						}}
					>
						{events.ids.map((evId, ind) => (
							<EventCard
								index={ind}
								id={evId}
								key={"event-" + evId}
								text={events.byId[evId].title}
								rgbColor={events.byId[evId]?.type?.color}
								actions={actions.map((ac) => ({ ...ac, onClick: () => ac.onClick(evId) }))}
								disableActions={disableActions}
							/>
						))}
						{provided.placeholder}
					</div>
				);
			}}
		</Droppable>
	);
};

export default DroppableCell;
