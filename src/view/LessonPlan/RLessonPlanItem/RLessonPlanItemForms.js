import React from "react";
import { Row, Col, Form, FormGroup, Input, Card, FormText } from "reactstrap";
import { faPaperclip, faDownload, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { primaryColor } from "config/constants";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import { Services } from "engine/services";
import RLessonPlanAddButton from "./RLessonPlanAddButton";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RButton from "components/Global/RComs/RButton";
import styles from "../RLessonPlan.module.scss";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RLessonPlanItemForms = ({
	lessonPlan,
	lessonPlanErrors,
	handelLessonPlanItemChange,
	handleAddNewLessonPlanItems,
	handleRemoveLessonPlanItem,
	handleClearLessonPlanItem,
	handleOpenPickARubric,

	setLessonPlanItemId,
	handleRemoveRubricFromLessonPlanItem,
	dir,
}) => {
	return (
		<>
			{lessonPlan && lessonPlan?.lesson_plan_items?.length > 0 ? (
				lessonPlan?.lesson_plan_items.map((lessonPlanItem, index) => {
					return (
						<div key={index}>
							<RLessonPlanAddButton handleAddNewLessonPlanItems={handleAddNewLessonPlanItems} index={index} position="up" />

							<Card className={styles.form_card}>
								<Form className={`mt-5 mb-3 hover-div`}>
									<Row>
										<Col sm={6} xs={12}>
											<FormGroup>
												<RFlex>
													<label className={dir ? styles.text_ltr : styles.text_rtl}>{tr("subject")}</label>

													<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
												</RFlex>
												<Input
													required
													type="text"
													onChange={(event) =>
														handelLessonPlanItemChange(
															event.target.value,
															"subject",
															lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp
														)
													}
													placeholder={tr`subject`}
													value={lessonPlanItem?.subject}
												/>
												{lessonPlanErrors && lessonPlanErrors.lesson_plan_items && lessonPlanErrors.lesson_plan_items[index]?.subject && (
													<FormText color="danger">{lessonPlanErrors.lesson_plan_items[index].subject}</FormText>
												)}
											</FormGroup>
										</Col>
										<Col sm={6} xs={12}>
											<RFlex>
												<label className={dir ? styles.text_ltr : styles.text_rtl}>{tr("item_time")} :</label>
												<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
											</RFlex>
											<FormGroup>
												<Input
													required
													type="text"
													onChange={(event) =>
														handelLessonPlanItemChange(
															event.target.value,
															"time",
															lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp
														)
													}
													placeholder={tr`item_time`}
													value={lessonPlanItem?.time}
												/>

												{lessonPlanErrors && lessonPlanErrors.lesson_plan_items && lessonPlanErrors.lesson_plan_items[index]?.time && (
													<FormText color="danger">{lessonPlanErrors.lesson_plan_items[index].time}</FormText>
												)}
											</FormGroup>
										</Col>
										<Col sm={6} xs={12}>
											<FormGroup>
												<RFlex>
													<label className={dir ? styles.text_ltr : styles.text_rtl}>{tr("tags")}</label>
													<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
												</RFlex>

												<RTags
													url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
													getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
													getValueFromArrayItem={(i) => i.id}
													getLabelFromArrayItem={(i) => i.name}
													getIdFromArrayItem={(i) => i.id}
													onChange={(event) =>
														handelLessonPlanItemChange(event, "tags", lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp)
													}
													defaultLabel={lessonPlanItem?.tags}
													// defaultValues={lessonPlanItem?.tags.map(
													//   (lessonPlanItem) => {
													//     return {
													//       label: lessonPlanItem.name,
													//       value: lessonPlanItem.id,
													//     };
													//   }
													// )}
													defaultValues={lessonPlanItem?.tags}
												/>
											</FormGroup>
											<FormGroup>
												<RFlex>
													<label className={dir ? styles.text_ltr : styles.text_rtl}>{tr("description")}</label>
													<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
												</RFlex>
												<CKEditor
													editor={ClassicEditor}
													onChange={(event, editor) => {
														const data = editor.getData();
														handelLessonPlanItemChange(
															data,
															"description",
															lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp
														);
													}}
													data={lessonPlanItem.description}
												/>
												{lessonPlanErrors &&
													lessonPlanErrors.lesson_plan_items &&
													lessonPlanErrors.lesson_plan_items[index]?.description && (
														<FormText color="danger">{lessonPlanErrors.lesson_plan_items[index].description}</FormText>
													)}
											</FormGroup>
										</Col>
										<Col sm={6} xs={12}>
											<FormGroup>
												<RFlex>
													<FontAwesomeIcon icon={faPaperclip} />
													<label className={dir ? styles.text_ltr : styles.text_rtl} style={{ textDecoration: "underline" }}>
														{tr("attachments")}
													</label>
												</RFlex>
												<div className={styles.attachment_border}>
													{/* <RCardSlider /> */}

													<RFileSuite
														parentCallback={(event) => {
															handelLessonPlanItemChange(
																event,
																"attachments",
																lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp
															);
														}}
														value={lessonPlanItem?.attachments}
														singleFile={false}
														binary={true}
													/>
													<Row>
														<Col md={4} xs={12}>
															<RButton
																text={tr`Pick a Rubric`}
																color="primary"
																outline
																onClick={() => {
																	handleOpenPickARubric();
																	setLessonPlanItemId(lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp);
																}}
															/>
															{lessonPlanItem?.rubric?.title && (
																<RFlex>
																	<FontAwesomeIcon
																		icon={faTimesCircle}
																		style={{
																			color: "#DD0000",
																			cursor: "pointer",
																		}}
																		onClick={() =>
																			handleRemoveRubricFromLessonPlanItem(lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp)
																		}
																	/>{" "}
																	<h6>{lessonPlanItem?.rubric?.title}</h6>
																</RFlex>
															)}
														</Col>
														<Col md={8} xs={12} className={styles.choose_resource}>
															<FontAwesomeIcon icon={faDownload} style={{ color: primaryColor }} />
															<h6 className={styles.name_choose_resource} style={{ color: primaryColor }}>
																choose from Resources
															</h6>
														</Col>
													</Row>
												</div>
											</FormGroup>
										</Col>
									</Row>
								</Form>

								<RFlex
									styleProps={{
										gap: 20,
										justifyContent: "end",
										alignItems: "center",
										padding: "0px 20px",
									}}
								>
									<RButton
										text={tr`Clear form`}
										color="primary"
										outline
										onClick={() => handleClearLessonPlanItem(lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp)}
									/>
									<i
										onClick={() => handleRemoveLessonPlanItem(lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp)}
										className={iconsFa6.delete}
										style={{ color: "red", cursor: "pointer" }}
									/>
								</RFlex>
							</Card>

							<RLessonPlanAddButton handleAddNewLessonPlanItems={handleAddNewLessonPlanItems} index={index + 1} position="down" />
						</div>
					);
				})
			) : (
				<>
					<RLessonPlanAddButton handleAddNewLessonPlanItems={handleAddNewLessonPlanItems} />
					<RFlex styleProps={{ justifyContent: "center" }}>
						<p>{tr`no_items_yet`}</p>
					</RFlex>
					{/* <RFlex
						styleProps={{
							justifyContent: "center",

							marginBottom: "40px",
						}}
					>
						<p>click on the</p>
						<div className={styles.addition_container_plus} style={{ background: primaryColor }}>
							<i className={`fa fa-plus  ${styles.addition_icon_plus}`} />
						</div>
						<p>to start creating items</p>
					</RFlex> */}
					{lessonPlanErrors && lessonPlanErrors.lesson_plan_items && (
						<RFlex
							styleProps={{
								justifyContent: "center",

								marginBottom: "40px",
							}}
						>
							<FormText color="danger">{lessonPlanErrors.lesson_plan_items}</FormText>
						</RFlex>
					)}
				</>
			)}
		</>
	);
};

export default RLessonPlanItemForms;
