import React, { useEffect } from 'react'
import RCollapsedDivV2 from './RCollapsedDivV2'
import { useState } from 'react'
import GGradeLevelV2 from './GGradeLevelV2'
import styles from './SchoolLister.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import { organizationTypes, types } from './constants'
import Swal, { DANGER } from 'utils/Alert'
import { deleteSweetAlert } from 'components/Global/RComs/RAlert2'
import tr from "components/Global/RComs/RTranslator";
import { useMutateData } from 'hocs/useMutateData'
import { schoolManagementV2API } from 'api/SchoolManagementV2'
const GEducationStageV2 = (
    {
        id,
        index,
        onDelete,
        gradeLevels,
        currentStage,
        allCurriculas,
        openedEducationStageId,
        openedGradeLevelId,
        handleChangingLevelsProperties,
        reOrderEducationStageMutation,
        educationStages,
        allGradeLevels,
        setFieldValue,
        levelsProperites,
        reOrderGradeLevelMutation,
        reOrderCurriculaMutation,
        courses
    }
) => {
    const dispatch = useDispatch()

    const user = useSelector((state) => state?.auth);
    // const organization_id = user && user?.user && user?.user?.organization_id;
    const organization_id = 1
    const [saved, setSaved] = useState(id > 0 ? true : false)
    const [saveClicked, setSaveClicked] = useState(false)
    const [addClicked, setAddClicked] = useState(false)
    const [enterPressed, setEnterPressed] = useState(false)
    const [alert1, setAlert] = useState(false);

    const createUpdateEducationStageMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.createUpdateLevel(payload, organization_id),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ data, variables }) => {
            if (variables.payload?.fakeId) {
                const newStages = JSON.parse(JSON.stringify(educationStages))
                const stageIndex = educationStages.findIndex((stage) => stage.fakeId == id)
                newStages[stageIndex] = { ...data?.data }
                console.log("newStagesnewStages", newStages)
                setFieldValue(types.EducationStages, newStages)
                const filteredArray = newStages.filter((stage) => stage.id)
                reOrderEducationStageMutation.mutate({ payload: { levels: [...filteredArray] } })
            }
            setSaved(true)
        }
    })

    const [inputValue, setInputValue] = useState(currentStage?.name ?? '')

    // if (educationOpenIndex == educationCurrentIndex) {
    //     dispatch(actions.setActiveId(types.Stage, id))
    // }
    const isItActive = (openedEducationStageId == id && openedGradeLevelId == null) ? true : false
    const editEducation = () => {
        console.log("Edit Triggered")
        // dispatch(actions.setOpenId(types.Stage, id))
        setEnterPressed(false)
        setSaved(!saved)
    }

    const hideAlert = () => setAlert(null);
    const showAlerts = (child) => setAlert(child);
    const deleteEducation = (id, order) => {
        const confirm = tr`Yes, delete it`;
        const message = (
            <div>
                <h6>{tr`Are you sure? `}</h6>
                <p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
            </div>
        );
        deleteSweetAlert(showAlerts, hideAlert, onDelete, { stageId: id, order }, message, confirm);
    }

    const handleInputChange = (event) => {
        setSaveClicked(false)
        setAddClicked(false)
        setEnterPressed(false)
        setInputValue(event.target.value)
    }

    const handleDivClicked = (id) => {
        console.log("Div Clicked");
        // handleEducationCollapse(index)
        const newProperties = {
            [types.OpenStageId]: openedEducationStageId == id ? null : id,
            [types.OpenStageIndex]: openedEducationStageId == id ? null : index,
            [types.OpenGradeLevelId]: null,
            [types.OpenGradeLevelIndex]: null
        }
        handleChangingLevelsProperties(newProperties)

    }


    const handleInputDown = (event, item, extraInfo, inputRef) => {
        // if (event.key === 'Enter') {
        //     if (enterPressed)
        //         return
        //     console.log("InputDown", inputRef)
        //     setEnterPressed(true)
        //     setSaveClicked(true)
        //     if (inputValue != "") {
        //         const { parent_level_id, type, attributes, ...other } = currentStage
        //         const payload = {
        //             ...other,
        //             name: inputValue
        //         }
        //         createUpdateEducationStageMutation.mutate({ payload })
        //         // inputRef?.current?.blur()
        //     }
        // }
    }

    const handleSaveButton = async () => {
        if (enterPressed) {
            return
        }
        setEnterPressed(true)
        setSaveClicked(true)
        if (inputValue != '') {
            const { parent_level_id, type, attributes, ...other } = currentStage
            const payload = {
                ...other,
                name: inputValue
            }
            createUpdateEducationStageMutation.mutate({ payload })
        }
        // event.stopPropagation()
    }

    const generateGradeLevelComponent = () => {
        if (id <= 0) {
            setAddClicked(true)
            return
        }
        const heighstOrder = gradeLevels.reduce((max, grade) => grade.order > max ? grade.order : max, 0)
        const lastId = allGradeLevels?.length + 1
        setFieldValue(types.GradeLevels, [
            ...allGradeLevels,
            { fakeId: -lastId, order: heighstOrder + 1, name: '', content_type_id: organizationTypes[types.GradeLevels], parent_level_id: id }
        ])
    };
    const deleteGradeLevelMutation = useMutateData({
        queryFn: ({ levelId }) => schoolManagementV2API.deleteLevel(levelId),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ variables }) => {
            const filteredArray = allGradeLevels.filter((grade) => grade.id != variables.levelId)
            setFieldValue(types.GradeLevels, filteredArray)
        }
    })
    const handleDeleteGradeLevel = ({ gradeLevelId, order }) => {
        if (gradeLevelId > 0) {
            deleteGradeLevelMutation.mutate({ levelId: gradeLevelId })
        } else {
            const filteredArray = allGradeLevels.filter((grade) => grade.fakeId ? grade.fakeId != gradeLevelId : true)
            setFieldValue(types.GradeLevels, filteredArray)
        }
    };



    const showAlert = (gradeLevelId, order) => {
        Swal.input({
            title: tr`are_you_sure_to_delete_it`,
            message: tr``,
            type: DANGER,
            placeHolder: "",
            onConfirm: () => handleDeleteGradeLevel(gradeLevelId, order),
            onCancel: () => null,
        });
    }
    const gradeLevelComponents = gradeLevels.map((gradeLevel, index) => {
        const currentGradeCurriculas = allCurriculas.filter((curricula) => curricula.parent_level_id == gradeLevel.id)
        return {
            component: <GGradeLevelV2
                id={gradeLevel.fakeId ?? parseInt(gradeLevel.id)}
                currentGrade={gradeLevel}
                index={index}
                allCurriculas={allCurriculas}
                curriculas={currentGradeCurriculas}
                openedGradeLevelId={levelsProperites?.[types.OpenGradeLevelId]}
                openCurriculaId={levelsProperites?.[types.OpenCurriculaId]}
                setFieldValue={setFieldValue}
                handleChangingLevelsProperties={handleChangingLevelsProperties}
                reOrderGradeLevelMutation={reOrderGradeLevelMutation}
                onDelete={handleDeleteGradeLevel}
                allGradeLevels={allGradeLevels}
                courses={courses}
                reOrderCurriculaMutation={reOrderCurriculaMutation}
            />,
            id: gradeLevel.fakeId ?? parseInt(gradeLevel.id)
        }
    })


    return (
        <>
            <RCollapsedDivV2
                handleInputChange={handleInputChange}
                handleInputDown={handleInputDown}
                handleDivClicked={handleDivClicked}
                handleSaveButton={handleSaveButton}
                generateChildComponent={generateGradeLevelComponent}
                handleEditFunctionality={editEducation}
                handleDeleteFunctionality={deleteEducation}
                setSaved={setSaved}
                id={id}
                index={index}
                collapseChilds={gradeLevelComponents}
                order={currentStage.order}
                isOpen={id == openedEducationStageId ? true : false}
                openLevelId={openedEducationStageId}
                inputValue={inputValue}
                number={gradeLevelComponents?.length}
                saved={saved}
                addClicked={addClicked}
                saveClicked={saveClicked}
                inputPlaceHolder="education stage"
                buttonText="Add Grade Level"
                currentType={types.EducationStages}
                childType="Grade Levels"
                childPrefix={'childPrefix'}
                isItActive={isItActive}
                isLoading={createUpdateEducationStageMutation?.isLoading}

            />
            {alert1}
        </>
    )
}

export default GEducationStageV2