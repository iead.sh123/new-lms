import {
	GET_COURSES_MANAGER_TREE_REQUEST,
	GET_COURSES_MANAGER_TREE_SUCCESS,
	GET_COURSES_MANAGER_TREE_ERROR,
	ADD_CATEGORY_TO_TREE_REQUEST,
	ADD_CATEGORY_TO_TREE_SUCCESS,
	ADD_CATEGORY_TO_TREE_ERROR,
	REMOVE_CATEGORY_FROM_TREE_REQUEST,
	REMOVE_CATEGORY_FROM_TREE_SUCCESS,
	REMOVE_CATEGORY_FROM_TREE_ERROR,
	GET_COURSES_BY_CATEGORY_ID_REQUEST,
	GET_COURSES_BY_CATEGORY_ID_SUCCESS,
	GET_COURSES_BY_CATEGORY_ID_ERROR,
	CLONE_COURSE_REQUEST,
	CLONE_COURSE_SUCCESS,
	CLONE_COURSE_ERROR,
	INSTANTIATE_COURSE_REQUEST,
	INSTANTIATE_COURSE_SUCCESS,
	INSTANTIATE_COURSE_ERROR,
	REMOVE_COURSE_REQUEST,
	REMOVE_COURSE_SUCCESS,
	REMOVE_COURSE_ERROR,
	GET_CATEGORY_ANCESTORS_REQUEST,
	GET_CATEGORY_ANCESTORS_SUCCESS,
	GET_CATEGORY_ANCESTORS_ERROR,
	SAVE_COURSE_REQUEST,
	SAVE_COURSE_SUCCESS,
	SAVE_COURSE_ERROR,
	COURSE_BY_ID_REQUEST,
	COURSE_BY_ID_SUCCESS,
	COURSE_BY_ID_ERROR,
	SET_COURSE_VALUE,
	GET_LEVELS_REQUEST,
	GET_LEVELS_SUCCESS,
	GET_LEVELS_ERROR,
	GET_USER_TYPE_REQUEST,
	GET_USER_TYPE_SUCCESS,
	GET_USER_TYPE_ERROR,
	SET_COURSE_OVERVIEW,
	SET_ADD_ITEMS_TO_OVERVIEW_FIELDS,
	CHANGE_ITEMS_IN_COURSE_OVERVIEW,
	DELETE_ITEMS_FROM_COURSE_OVERVIEW,
	FILL_DATA_IN_OVERVIEW,
	SAVE_COURSE_OVERVIEW_REQUEST,
	SAVE_COURSE_OVERVIEW_SUCCESS,
	SAVE_COURSE_OVERVIEW_ERROR,
	VIEW_COURSE_MODE,
	EMPTY_DATA_WHEN_ADD_NEW_COURSE,
	PUBLISH_AND_UND_PUBLISH_COURSE_REQUEST,
	PUBLISH_AND_UND_PUBLISH_COURSE_SUCCESS,
	PUBLISH_AND_UND_PUBLISH_COURSE_ERROR,
	ALL_FEEDBACK_REQUEST,
	ALL_FEEDBACK_SUCCESS,
	ALL_FEEDBACK_ERROR,
	COURSE_OVERVIEW_BY_ID_REQUEST,
	COURSE_OVERVIEW_BY_ID_SUCCESS,
	COURSE_OVERVIEW_BY_ID_ERROR,
	GET_COURSE_MODULE_REQUEST,
	GET_COURSE_MODULE_SUCCESS,
	GET_COURSE_MODULE_ERROR,
	GET_MODULE_CONTENT_REQUEST,
	GET_MODULE_CONTENT_SUCCESS,
	GET_MODULE_CONTENT_ERROR,
	CREATE_MODULE_REQUEST,
	CREATE_MODULE_SUCCESS,
	CREATE_MODULE_ERROR,
	FILL_DATA_TO_SPECIFIC_MODULE,
	SELECT_LESSON_CONTENT,
	ADD_ITEMS_TO_LESSON_CONTENT,
	SELECT_MODULE_CONTENT,
	ADD_ITEMS_TO_MODULE_CONTENT,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_REQUEST,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_SUCCESS,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_ERROR,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_REQUEST,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_SUCCESS,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_ERROR,
	ADD_CONTENT_TO_LESSON_CONTENT,
	DELETE_MODULE_CONTENT_SUCCESS,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_REQUEST,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_SUCCESS,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_ERROR,
	ADD_LINKS_TO_LESSON_CONTENT,
	HANDLE_SHOULD_NOT_BE_ADDED_lESSON_CONTENT,
	ADD_NEW_LESSON_CONTENT_TO_MODULE_CONTENT,
	DELETE_MODULE_SUCCESS,
	DELETE_NEW_LESSON_CONTENT,
	REMOVE_LINK_FROM_LESSON_CONTENT_LOCALE,
	EDIT_LESSON_CONTENT_FROM_MODULE_CONTENT,
	RELOAD_LIVE_SESSION,
	GET_MY_COURSES_REQUEST,
	GET_MY_COURSES_SUCCESS,
	GET_MY_COURSES_ERROR,
	ALL_FILTERS_TO_COURSES_REQUEST,
	ALL_FILTERS_TO_COURSES_SUCCESS,
	ALL_FILTERS_TO_COURSES_ERROR,
	ALL_COURSES_REQUEST,
	ALL_COURSES_SUCCESS,
	ALL_COURSES_ERROR,
	LANDING_PAGE_REQUEST,
	LANDING_PAGE_SUCCESS,
	LANDING_PAGE_ERROR,
	GET_MY_CURRICULA_REQUEST,
	GET_MY_CURRICULA_SUCCESS,
	GET_MY_CURRICULA_ERROR,
	ALL_STUDENTS_REQUEST,
	ALL_STUDENTS_SUCCESS,
	ALL_STUDENTS_ERROR,
	HANDLE_SHOULD_NOT_BE_ADDED_MODULE_CONTENT,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
	UPDATE_TITLE_TO_QUESTION_SET,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
	CHANGE_BEHAVIOR_IN_MODULE_CONTENT,
	DELETE_NEW_MODULE_CONTENT,
	ENROLL_IN_COURSE_REQUEST,
	ENROLL_IN_COURSE_SUCCESS,
	ENROLL_IN_COURSE_ERROR,
	PUSH_DATA_TO_COURSE_MODULE,
} from "store/actions/global/globalTypes";
import produce from "immer";
import { Services } from "engine/services";
import DefaultImage from "assets/img/new/course-default-cover.png";
import {
	DELETE_LESSON_CONTENT_REQUEST,
	DELETE_LESSON_CONTENT_SUCCESS,
	DELETE_LESSON_CONTENT_ERROR,
} from "store/actions/teacher/teacherTypes";
import fairLevel from "assets/img/new/svg/fair.svg";
import expertLevel from "assets/img/new/svg/expert.svg";
import intermediateLevel from "assets/img/new/svg/intermediate.svg";
import beginnerLevel from "assets/img/new/svg/beginner.svg";

const InitState = {
	courseManagerTree: [],
	coursesByCategory: [],
	coursesStatistics: [],
	categoryAncestors: {},
	courseById: {},
	levels: [],
	facilitators: [],
	directors: [],
	administrators: [],
	courseOverView: {},
	allFeedback: {},
	courseModule: {},
	moduleContent: {},
	specificModule: {},
	myCourses: [],
	allFiltersToCourses: {},
	allCourses: [],
	landingPage: {},
	banners: [],
	myCurricula: [],
	students: {},
	shouldNotBeAddedLessonContent: false,
	shouldNotBeAddedModuleContent: false,
	lessonContentType: null,
	moduleContentType: null,
	test: null,
	coursesByCategoryLoading: false,
	courseManagerTreeLoading: false,
	addEditCategoryToTreeLoading: false,
	removeCategoryFromTreeLoading: false,
	cloneCourseLoading: false,
	instantiateCourseLoading: false,
	deleteCourseLoading: false,
	categoryAncestorsLoading: false,
	saveCourseLoading: false,
	courseByIdLoading: false,
	levelsLoading: false,
	facilitatorsLoading: false,
	courseOverViewLoading: false,
	saveCourseOverViewLoading: false,
	courseStudentMode: false,
	publishAndUnPublishCourseLoading: false,
	allFeedbackLoading: false,
	courseModuleLoading: false,
	moduleContentLoading: false,
	createModuleLoading: false,
	createModuleContentLoading: false,
	publishAndUnPublishModuleContentLoading: false,
	myCoursesLoading: false,
	allCoursesLoading: false,
	allFiltersToCoursesLoading: false,
	landingPageLoading: false,
	myCurriculaLoading: false,
	studentsLoading: false,
	updateTitleLoading: false,
	saveBehaviorLoading: false,
	enrollInCourseLoading: false,
};

let ind;
let index;
let moreIndex;
let linkIndex;
let lessonPlanIndex;
let liveSessionIndex;
let fakeObjInd;
const coursesManagerReducer = (state = InitState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case GET_COURSES_MANAGER_TREE_REQUEST:
				draft.courseManagerTreeLoading = true;
				return draft;
			case GET_COURSES_MANAGER_TREE_SUCCESS:
				draft.courseManagerTreeLoading = false;
				draft.courseManagerTree = action.payload.data;
				return draft;
			case GET_COURSES_MANAGER_TREE_ERROR:
				draft.courseManagerTreeLoading = false;
				return draft;

			case ADD_CATEGORY_TO_TREE_REQUEST:
				draft.addEditCategoryToTreeLoading = true;
				return draft;
			case ADD_CATEGORY_TO_TREE_SUCCESS:
				draft.addEditCategoryToTreeLoading = false;
				return draft;
			case ADD_CATEGORY_TO_TREE_ERROR:
				draft.addEditCategoryToTreeLoading = false;
				return draft;

			case REMOVE_CATEGORY_FROM_TREE_REQUEST:
				draft.removeCategoryFromTreeLoading = true;
				return draft;
			case REMOVE_CATEGORY_FROM_TREE_SUCCESS:
				draft.removeCategoryFromTreeLoading = false;
				return draft;
			case REMOVE_CATEGORY_FROM_TREE_ERROR:
				draft.removeCategoryFromTreeLoading = false;
				return draft;

			case GET_COURSES_BY_CATEGORY_ID_REQUEST:
				draft.coursesByCategoryLoading = true;
				return draft;

			case GET_COURSES_BY_CATEGORY_ID_SUCCESS:
				draft.coursesByCategoryLoading = false;
				draft.coursesByCategory = action.payload.data;
				draft.coursesStatistics = [];
				for (let i = 0; i < Object.keys(action.payload.statistics).length; i++) {
					draft.coursesStatistics.push({
						title: Object.keys(action.payload.statistics)[i],
						count: Object.values(action.payload.statistics)[i],
					});
				}
				return draft;
			case GET_COURSES_BY_CATEGORY_ID_ERROR:
				draft.coursesByCategoryLoading = false;
				return draft;

			case CLONE_COURSE_REQUEST:
				draft.cloneCourseLoading = true;
				return draft;
			case CLONE_COURSE_SUCCESS:
				draft.cloneCourseLoading = false;
				return draft;
			case CLONE_COURSE_ERROR:
				draft.cloneCourseLoading = false;
				return draft;

			case INSTANTIATE_COURSE_REQUEST:
				draft.instantiateCourseLoading = true;
				return draft;
			case INSTANTIATE_COURSE_SUCCESS:
				draft.instantiateCourseLoading = false;
				return draft;
			case INSTANTIATE_COURSE_ERROR:
				draft.instantiateCourseLoading = false;
				return draft;

			case REMOVE_COURSE_REQUEST:
				draft.deleteCourseLoading = true;
				return draft;
			case REMOVE_COURSE_SUCCESS:
				draft.deleteCourseLoading = false;
				ind = draft.coursesByCategory.findIndex((el) => el.id == action.payload.courseId);
				if (ind !== -1) {
					draft.coursesByCategory.splice(ind, 1);
					draft.coursesStatistics[0].count = draft.coursesStatistics[0].count - 1;
					if (action.payload.isPublished) {
						draft.coursesStatistics[1].count = draft.coursesStatistics[1].count - 1;
					} else if (!action.payload.isPublished) {
						draft.coursesStatistics[2].count = draft.coursesStatistics[2].count - 1;
					}
				}
				return draft;
			case REMOVE_COURSE_ERROR:
				draft.deleteCourseLoading = false;
				return draft;

			case GET_CATEGORY_ANCESTORS_REQUEST:
				draft.categoryAncestorsLoading = true;
				return draft;
			case GET_CATEGORY_ANCESTORS_SUCCESS:
				draft.categoryAncestorsLoading = false;
				draft.categoryAncestors = action.payload.data;
				return draft;
			case GET_CATEGORY_ANCESTORS_ERROR:
				draft.categoryAncestorsLoading = false;
				return draft;

			case SAVE_COURSE_REQUEST:
				draft.saveCourseLoading = true;
				return draft;
			case SAVE_COURSE_SUCCESS:
				draft.saveCourseLoading = false;
				draft.courseById = action.payload.data;
				return draft;
			case SAVE_COURSE_ERROR:
				draft.saveCourseLoading = false;
				return draft;

			case COURSE_BY_ID_REQUEST:
				draft.courseByIdLoading = true;
				return draft;
			case COURSE_BY_ID_SUCCESS:
				draft.courseByIdLoading = false;
				draft.courseById = action.payload.data;
				return draft;
			case COURSE_BY_ID_ERROR:
				draft.courseByIdLoading = false;
				return draft;

			case SET_COURSE_VALUE:
				draft.courseById[action.payload.name] = action.payload.value;
				return draft;

			case GET_LEVELS_REQUEST:
				draft.levelsLoading = true;
				return draft;
			case GET_LEVELS_SUCCESS:
				draft.levelsLoading = false;
				draft.levels = [];
				for (let i = 0; i < action.payload.data.length; i++) {
					draft.levels.push({
						label: action.payload.data[i]["name"],
						value: action.payload.data[i]["id"],
						image:
							action.payload.data[i]["name"].toLowerCase() == "intermediate"
								? intermediateLevel
								: action.payload.data[i]["name"].toLowerCase() == "fair"
								? fairLevel
								: action.payload.data[i]["name"].toLowerCase() == "expert"
								? expertLevel
								: beginnerLevel,
					});
				}
				return draft;
			case GET_LEVELS_ERROR:
				draft.levelsLoading = false;
				return draft;

			case GET_USER_TYPE_REQUEST:
				draft.facilitatorsLoading = true;
				return draft;
			case GET_USER_TYPE_SUCCESS:
				draft.facilitatorsLoading = false;
				if (action.payload.userType == "teachers") {
					draft.facilitators = action.payload.data;
				} else if (action.payload.userType == "course-administrators") {
					draft.administrators = action.payload.data;
				} else if (action.payload.userType == "directors") {
					draft.directors = action.payload.data;
				}
				return draft;
			// for (let i = 0; i < action.payload.data.length; i++) {
			// 	draft.facilitators.push({
			// 		label: action.payload.data[i]["label"],
			// 		value: action.payload.data[i]["value"],
			// 		image_hash_id:
			// 			action.payload.data[i]["hash_id"] == undefined
			// 				? DefaultImage
			// 				: `${Services.auth_organization_management.file}${action.payload.data[i]["hash_id"]}`,
			// 		userTypeId: 1,
			// 	});
			// }
			case GET_USER_TYPE_ERROR:
				draft.facilitatorsLoading = false;
				return draft;

			case SET_ADD_ITEMS_TO_OVERVIEW_FIELDS:
				if (action.payload.key == "what_you_will_learn") {
					draft.courseById.overview.what_you_will_learn.push(action.payload.data);
				} else if (action.payload.key == "who_is_this_course_for") {
					draft.courseById.overview.who_is_this_course_for.push(action.payload.data);
				} else if (action.payload.key == "schedule") {
					draft.courseById.overview.schedule.push(action.payload.dataValue);
				} else if (action.payload.key == "previous_knowledge") {
					draft.courseById.overview.previous_knowledge.push(action.payload.data);
				} else if (action.payload.key == "faq") {
					draft.courseById.overview.faq.push(action.payload.dataFaq);
				} else if (action.payload.key == "requirements") {
					draft.courseById.overview.requirements.push(action.payload.data);
				} else {
					return;
				}
				return draft;

			case CHANGE_ITEMS_IN_COURSE_OVERVIEW:
				if (action.payload.key == null) {
					draft.courseById.overview[action.payload.name] = action.payload.value;
				} else {
					ind = draft.courseById.overview[action.payload.key].findIndex((el) => (el?.id ?? el?.fakeId) == action.payload.id);
					if (ind !== -1) {
						draft.courseById.overview[action.payload.key][ind][action.payload.name] = action.payload.value;
					}
				}
				return draft;
			case DELETE_ITEMS_FROM_COURSE_OVERVIEW:
				ind = draft.courseById.overview[action.payload.key].findIndex((el) => (el?.id ?? el?.fakeId) == action.payload.id);
				if (ind !== -1) {
					draft.courseById.overview[action.payload.key].splice(ind, 1);
				} else {
					return;
				}
				return draft;

			case FILL_DATA_IN_OVERVIEW:
				const data = {
					description: "",
					video: [],
					policy: "",
					schedule: [],
					requirements: [],
					previous_knowledge: [],
					faq: [],
					who_is_this_course_for: [],
					what_you_will_learn: [],
					contents: [],
				};

				draft.courseById.overview = data;
				return draft;

			case SAVE_COURSE_OVERVIEW_REQUEST:
				draft.saveCourseOverViewLoading = true;
				return draft;
			case SAVE_COURSE_OVERVIEW_SUCCESS:
				draft.saveCourseOverViewLoading = false;
				return draft;
			case SAVE_COURSE_OVERVIEW_ERROR:
				draft.saveCourseOverViewLoading = false;
				return draft;

			case VIEW_COURSE_MODE:
				draft.courseStudentMode = action.payload.mode;
				return draft;

			case EMPTY_DATA_WHEN_ADD_NEW_COURSE:
				draft.courseById = { name: "" };
				return draft;

			case PUBLISH_AND_UND_PUBLISH_COURSE_REQUEST:
				draft.publishAndUnPublishCourseLoading = true;
				return draft;
			case PUBLISH_AND_UND_PUBLISH_COURSE_SUCCESS:
				draft.publishAndUnPublishCourseLoading = false;
				return draft;
			case PUBLISH_AND_UND_PUBLISH_COURSE_ERROR:
				draft.publishAndUnPublishCourseLoading = false;
				return draft;

			case ALL_FEEDBACK_REQUEST:
				draft.allFeedbackLoading = true;
				return draft;
			case ALL_FEEDBACK_SUCCESS:
				draft.allFeedbackLoading = false;
				draft.allFeedback = action.payload.data;
				return draft;
			case ALL_FEEDBACK_ERROR:
				draft.allFeedbackLoading = false;
				return draft;

			case COURSE_OVERVIEW_BY_ID_REQUEST:
				draft.courseByIdLoading = true;
				return draft;
			case COURSE_OVERVIEW_BY_ID_SUCCESS:
				draft.courseByIdLoading = false;
				draft.courseById = action.payload.data;
				return draft;
			case COURSE_OVERVIEW_BY_ID_ERROR:
				draft.courseByIdLoading = false;
				return draft;

			case GET_COURSE_MODULE_REQUEST:
				draft.courseModuleLoading = true;
				return draft;
			case GET_COURSE_MODULE_SUCCESS:
				draft.courseModuleLoading = false;
				draft.courseModule = action.payload.data;
				return draft;
			case GET_COURSE_MODULE_ERROR:
				draft.courseModuleLoading = false;
				return draft;

			case GET_MODULE_CONTENT_REQUEST:
				draft.moduleContentLoading = true;
				return draft;
			case GET_MODULE_CONTENT_SUCCESS:
				draft.moduleContentLoading = false;
				draft.moduleContent = action.payload.data;
				return draft;
			case GET_MODULE_CONTENT_ERROR:
				draft.moduleContentLoading = false;
				return draft;

			case CREATE_MODULE_REQUEST:
				draft.createModuleLoading = true;
				return draft;
			case CREATE_MODULE_SUCCESS:
				draft.createModuleLoading = false;
				if (action.payload.edit) {
					ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.data.id);
					if (ind !== -1) {
						draft.courseModule.modules[ind] = action.payload.data;
						draft.specificModule = action.payload.data;
					}
				} else {
					draft.courseModule.modules.push(action.payload.data);
				}
				return draft;
			case CREATE_MODULE_ERROR:
				draft.createModuleLoading = false;
				return draft;

			case DELETE_MODULE_SUCCESS:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					draft.courseModule.modules.splice(ind, 1);
				}
				return draft;

			case FILL_DATA_TO_SPECIFIC_MODULE:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.data.id);
				if (ind !== -1) {
					draft.specificModule = draft.courseModule.modules[ind];
				}
				return draft;

			case SELECT_LESSON_CONTENT:
				draft.lessonContentType = action.payload.lessonContentType;
				return draft;

			case ADD_ITEMS_TO_LESSON_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.module_id);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.content.id);

					if (index !== -1) {
						let data = draft.courseModule.modules[ind].contents[index].contents;
						let specificData = draft.specificModule.contents[index].contents;

						if (action.payload.type == "text") {
							let obj = {
								id: 0,
								newData: 1,
								type: action.payload.type,
								title: action.payload.title,
								text: { text: "" },
								order: data.length + 1,
							};
							data.push(obj);
							specificData.push(obj);
							draft.shouldNotBeAddedLessonContent = true;
						} else if (action.payload.type == "link") {
							let obj = {
								id: 0,
								newData: 1,
								type: action.payload.type,
								title: action.payload.title,
								links: [],
								order: data.length + 1,
							};
							data.push(obj);
							specificData.push(obj);
							draft.shouldNotBeAddedLessonContent = true;
						} else if (action.payload.type == "attachment") {
							let obj = {
								id: 0,
								newData: 1,
								type: action.payload.type,
								title: action.payload.title,
								files: [],
								order: data.length + 1,
							};
							data.push(obj);
							specificData.push(obj);
							draft.shouldNotBeAddedLessonContent = true;
						} else if (action.payload.type == "live_session") {
							let obj = {
								id: 0,
								newData: 1,
								type: action.payload.type,
								title: action.payload.title,
								order: data.length + 1,
							};
							data.push(obj);
							specificData.push(obj);
							draft.shouldNotBeAddedLessonContent = true;
						} else if (action.payload.type == "lesson_plan") {
							let obj = {
								id: 0,
								newData: 1,
								type: action.payload.type,
								title: action.payload.title,
								order: data.length + 1,
							};
							data.push(obj);
							specificData.push(obj);
							draft.shouldNotBeAddedLessonContent = true;
						} else {
							return;
						}
					}
				}
				return draft;

			case ADD_CONTENT_TO_LESSON_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.module_id);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.content.id);

					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action?.payload?.lessonContent?.id
						);

						if (moreIndex !== -1) {
							let data = draft.courseModule.modules[ind].contents[index].contents[moreIndex];
							let specificData = draft.specificModule.contents[index].contents[moreIndex];
							if (action.payload.type == "text") {
								if (action.payload.name == "name") {
									data.title = action.payload.value;
									specificData.title = action.payload.value;
								} else {
									data.text.text = action.payload.value;
									specificData.text.text = action.payload.value;
								}
							} else if (action.payload.type == "link") {
								//add link
								if (action?.payload?.lessonContent.newData) {
									data.links[action.payload.linkIndex][action.payload.name] = action.payload.value;
									specificData.links[action.payload.linkIndex][action.payload.name] = action.payload.value;
								}
							} else if (action.payload.type == "attachment") {
								data.files = action.payload.value;
								specificData.files = action.payload.value;
							} else {
								return;
							}
						}
					}
				}

				return draft;

			case ADD_LINKS_TO_LESSON_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.module_id);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.content.id);
					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action.payload.lessonContent.id
						);

						if (moreIndex !== -1) {
							let data = draft.courseModule.modules[ind].contents[index].contents[moreIndex];
							let specificData = draft.specificModule.contents[index].contents[moreIndex];
							let linkObj = {
								fakeId: Math.random(),
								link_url: "",
								link_title: "",
							};
							data.links.push(linkObj);
							specificData.links.push(linkObj);
						}
					}
				}
				return draft;

			case SELECT_MODULE_CONTENT:
				draft.moduleContentType = action.payload.moduleContentType;
				return draft;

			case ADD_ITEMS_TO_MODULE_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.id);
				if (ind !== -1) {
					let obj = {
						id: 0,
						newData: 1,
						type: action.payload.type,
						title: action.payload.title,
					};
					draft.courseModule.modules[ind].contents.push(obj);
					draft.specificModule.contents.push(obj);
					draft.shouldNotBeAddedModuleContent = true;
				}
				return draft;

			case CREATE_OR_EDIT_MODULE_CONTENT_LESSON_REQUEST:
				draft.createModuleContentLoading = true;
				return draft;

			case CREATE_OR_EDIT_MODULE_CONTENT_LESSON_SUCCESS:
				draft.createModuleContentLoading = false;
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					//ps: 1/2/2024 i remove this line because we have a issue when edit lesson
					// index = draft.courseModule.modules[ind].contents.findIndex((ell) => (ell.content_id ?? ell.id) == action.payload.data.id);
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.data.id);
					if (index !== -1) {
						draft.courseModule.modules[ind].contents[index] = action.payload.data;
						draft.specificModule.contents[index] = action.payload.data;
					} else {
						draft.courseModule.modules[ind].contents.push({
							...action.payload.data,
							type: action.payload.type,
							contents: [],
						});
						draft.specificModule.contents.push({
							...action.payload.data,
							type: action.payload.type,
							contents: [],
						});
					}
				}
				return draft;
			case CREATE_OR_EDIT_MODULE_CONTENT_LESSON_ERROR:
				draft.createModuleContentLoading = false;
				return draft;

			case DELETE_MODULE_CONTENT_SUCCESS:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);
					if (index !== -1) {
						draft.courseModule.modules[ind].contents.splice(index, 1);
						draft.specificModule.contents.splice(index, 1);
					}
				}
				return draft;

			case DELETE_NEW_MODULE_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);
					if (index !== -1) {
						draft.courseModule.modules[ind].contents.splice(index, 1);
						draft.specificModule.contents.splice(index, 1);
					}
				}
				return draft;

			case PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_REQUEST:
				draft.publishAndUnPublishModuleContentLoading = true;
				return draft;
			case PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_SUCCESS:
				draft.publishAndUnPublishModuleContentLoading = false;
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);
					if (index !== -1) {
						draft.courseModule.modules[ind].contents[index].is_published = action.payload.publish ? true : false;
						draft.specificModule.contents[index].is_published = action.payload.publish ? true : false;
					}
				}

				return draft;

			case PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_ERROR:
				draft.publishAndUnPublishModuleContentLoading = false;
				return draft;

			case HANDLE_SHOULD_NOT_BE_ADDED_lESSON_CONTENT:
				draft.shouldNotBeAddedLessonContent = action.payload.flag;
				return draft;

			case HANDLE_SHOULD_NOT_BE_ADDED_MODULE_CONTENT:
				draft.shouldNotBeAddedModuleContent = action.payload.flag;
				return draft;

			case ADD_NEW_LESSON_CONTENT_TO_MODULE_CONTENT:
				draft.publishAndUnPublishModuleContentLoading = false;
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.courseModule.id);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContent.id);

					if (index !== -1) {
						draft.courseModule.modules[ind].contents[index].contents.splice(-1);
						draft.specificModule.contents[index].contents.splice(-1);

						if (action.payload.type == "attachment") {
							for (let i = 0; i < action.payload.lessonContent.length; i++) {
								draft.courseModule.modules[ind].contents[index].contents.push(action.payload.lessonContent[i]);
								draft.specificModule.contents[index].contents.push(action.payload.lessonContent[i]);
							}
						} else if (action.payload.type == "link") {
							for (let i = 0; i < action.payload.lessonContent.length; i++) {
								draft.courseModule.modules[ind].contents[index].contents.push(action.payload.lessonContent[i]);
								draft.specificModule.contents[index].contents.push(action.payload.lessonContent[i]);
							}
						} else if (action.payload.type == "lesson_plan") {
							if (draft.courseModule.modules[ind].contents[index].contents.some((el) => el.type == "lesson_plan")) {
								lessonPlanIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
									(lessonPlan) => lessonPlan.type == "lesson_plan"
								);
								if (lessonPlanIndex !== -1) {
									draft.courseModule.modules[ind].contents[index].contents.splice(lessonPlanIndex, 1);
									draft.specificModule.contents[index].contents.splice(lessonPlanIndex, 1);

									draft.courseModule.modules[ind].contents[index].contents.push(action.payload.lessonContent);
									draft.specificModule.contents[index].contents.push(action.payload.lessonContent);
								}
							} else {
								draft.courseModule.modules[ind].contents[index].contents.push(action.payload.lessonContent);
								draft.specificModule.contents[index].contents.push(action.payload.lessonContent);
							}
						} else {
							draft.courseModule.modules[ind].contents[index].contents.push(action.payload.lessonContent);
							draft.specificModule.contents[index].contents.push(action.payload.lessonContent);
						}
					}
				}
				return draft;

			case DELETE_LESSON_CONTENT_SUCCESS:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);
					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action.payload.lessonContentId
						);
						if (moreIndex !== -1) {
							draft.courseModule.modules[ind].contents[index].contents.splice(moreIndex, 1);
							draft.specificModule.contents[index].contents.splice(moreIndex, 1);
						}
					}
				}
				return draft;

			case DELETE_NEW_LESSON_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);
					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action.payload.lessonContentId
						);
						if (moreIndex !== -1) {
							draft.courseModule.modules[ind].contents[index].contents.splice(moreIndex, 1);
							draft.specificModule.contents[index].contents.splice(moreIndex, 1);
						}
					}
				}
				return draft;

			case REMOVE_LINK_FROM_LESSON_CONTENT_LOCALE:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.module_id);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.content.id);

					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action?.payload?.lessonContent?.id
						);

						if (moreIndex !== -1) {
							linkIndex = draft.courseModule.modules[ind].contents[index].contents[moreIndex].links.findIndex(
								(link) => link.fakeId == action.payload.fakeId
							);
							if (linkIndex !== -1) {
								let data = draft.courseModule.modules[ind].contents[index].contents[moreIndex];
								let specificData = draft.specificModule.contents[index].contents[moreIndex];
								data.links.splice(linkIndex, 1);
								specificData.links.splice(linkIndex, 1);
							}
						}
					}
				}
				return draft;

			case EDIT_LESSON_CONTENT_FROM_MODULE_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);

					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action.payload.lessonContentId
						);

						if (moreIndex !== -1) {
							let data = draft.courseModule.modules[ind].contents[index].contents[moreIndex];
							let specificData = draft.specificModule.contents[index].contents[moreIndex];

							if (action.payload.lessonContentType == "attachment") {
								data.title = action.payload.data.title;
								specificData.title = action.payload.data.title;
							} else if (action.payload.lessonContentType == "link") {
								data.title = action.payload.data.title;
								specificData.title = action.payload.data.title;
								data.link.link_title = action.payload.data.link.link_title;
								specificData.link.link_title = action.payload.data.link.link_title;
								data.link.link_url = action.payload.data.link.link_url;
								specificData.link.link_url = action.payload.data.link.link_url;
							} else if (action.payload.lessonContentType == "text") {
								data.title = action.payload.data.title;
								specificData.title = action.payload.data.title;
								data.text.text = action.payload.data.text.text;
								specificData.text.text = action.payload.data.text.text;
							}
						}
					}
				}
				return draft;

			case RELOAD_LIVE_SESSION:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.content.module_id);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.content.id);

					if (index !== -1) {
						moreIndex = draft.courseModule.modules[ind].contents[index].contents.findIndex(
							(lessonContent) => lessonContent.id == action?.payload?.lessonContent?.id
						);

						if (moreIndex !== -1) {
							let data = draft.courseModule.modules[ind].contents[index].contents[moreIndex];
							let specificData = draft.specificModule.contents[index].contents[moreIndex];

							data.live_session = action.payload.data;
							specificData.live_session = action.payload.data;
						}
					}
				}
				return draft;

			case GET_MY_COURSES_REQUEST:
				draft.myCoursesLoading = true;
				return draft;
			case GET_MY_COURSES_SUCCESS:
				draft.myCoursesLoading = false;
				draft.myCourses = action.payload.data;
				return draft;
			case GET_MY_COURSES_ERROR:
				draft.myCoursesLoading = false;
				return draft;

			case GET_MY_CURRICULA_REQUEST:
				draft.myCurriculaLoading = true;
				return draft;
			case GET_MY_CURRICULA_SUCCESS:
				draft.myCurriculaLoading = false;
				draft.myCurricula = action.payload.data;
				return draft;
			case GET_MY_CURRICULA_ERROR:
				draft.myCurriculaLoading = false;
				return draft;

			case ALL_FILTERS_TO_COURSES_REQUEST:
				draft.allFiltersToCoursesLoading = true;
				return draft;
			case ALL_FILTERS_TO_COURSES_SUCCESS:
				draft.allFiltersToCoursesLoading = false;
				draft.allFiltersToCourses = action.payload.data;
				return draft;
			case ALL_FILTERS_TO_COURSES_ERROR:
				draft.allFiltersToCoursesLoading = false;
				return draft;

			case ALL_COURSES_REQUEST:
				draft.allCoursesLoading = true;
				return draft;
			case ALL_COURSES_SUCCESS:
				draft.allCoursesLoading = false;
				draft.allCourses = action.payload.data;
				return draft;
			case ALL_COURSES_ERROR:
				draft.allCoursesLoading = false;
				return draft;

			case LANDING_PAGE_REQUEST:
				draft.landingPageLoading = true;
				return draft;
			case LANDING_PAGE_SUCCESS:
				draft.landingPageLoading = false;
				draft.landingPage = action.payload.data;
				return draft;
			case LANDING_PAGE_ERROR:
				draft.landingPageLoading = false;
				return draft;

			case ALL_STUDENTS_REQUEST:
				draft.studentsLoading = true;
				return draft;
			case ALL_STUDENTS_SUCCESS:
				draft.studentsLoading = false;
				draft.students = action.payload.data;
				return draft;
			case ALL_STUDENTS_ERROR:
				draft.studentsLoading = false;
				return draft;

			case UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST:
				draft.updateTitleLoading = true;
				return draft;
			case UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS:
				draft.updateTitleLoading = false;
				return draft;
			case UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_ERROR:
				draft.updateTitleLoading = false;
				return draft;

			case UPDATE_TITLE_TO_QUESTION_SET:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);

					if (index !== -1) {
						let data = draft.courseModule.modules[ind].contents[index];
						let specificData = draft.specificModule.contents[index];

						data[action.payload.key] = action.payload.value;
						specificData[action.payload.key] = action.payload.value;
					}
				}
				return draft;

			case ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST:
				draft.saveBehaviorLoading = true;
				return draft;
			case ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS:
				draft.saveBehaviorLoading = false;
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);

				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);

					if (index !== -1) {
						let data = draft.courseModule.modules[ind].contents[index];
						let specificData = draft.specificModule.contents[index];

						data.behavior = action.payload.newResponse;
						data.hasBehaviour = true;
						specificData.behavior = action.payload.newResponse;
						specificData.hasBehaviour = true;
					}
				}
				return draft;
			case ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_ERROR:
				draft.saveBehaviorLoading = false;
				return draft;

			case CHANGE_BEHAVIOR_IN_MODULE_CONTENT:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					index = draft.courseModule.modules[ind].contents.findIndex((ell) => ell.id == action.payload.moduleContentId);

					if (index !== -1) {
						let data = draft.courseModule.modules[ind].contents[index].behavior;
						let specificData = draft.specificModule.contents[index].behavior;

						data[action.payload.key] = action.payload.value;
						specificData[action.payload.key] = action.payload.value;
					}
				}
				return draft;

			case ENROLL_IN_COURSE_REQUEST:
				draft.enrollInCourseLoading = true;
				return draft;
			case ENROLL_IN_COURSE_SUCCESS:
				draft.enrollInCourseLoading = false;
				draft.courseById.isEnrolled = true;
				return draft;
			case ENROLL_IN_COURSE_ERROR:
				draft.enrollInCourseLoading = false;
				return draft;

			case PUSH_DATA_TO_COURSE_MODULE:
				ind = draft.courseModule.modules.findIndex((el) => el.id == action.payload.moduleId);
				if (ind !== -1) {
					fakeObjInd = draft.courseModule.modules[ind].contents.findIndex((el) => el.id == 0);
					draft.courseModule.modules[ind].contents.push(action.payload.data);
					console.log("fakeObjIndfakeObjIndfakeObjInd", ind, fakeObjInd);
					if (fakeObjInd !== -1) {
						draft.courseModule.modules[ind].contents.splice(fakeObjInd, 1);
					}
				}
				return draft;

			default:
				return draft;
		}
	});

export default coursesManagerReducer;
