import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const RAddBook = ({
  handleChangeBook,
  handleCloseAddBook,
  handleSaveBook,
  addBook,
  addBookLoading,
}) => {
  return (
    <Form>
      <Row>
        <Col xs={12}>
          <RFlex>
            <label>{tr("title")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="title"
              type="text"
              placeholder={tr`title`}
              value={addBook.title}
              onChange={(event) => {
                handleChangeBook(event.target.name, event.target.value);
              }}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <RFlex>
            <label>{tr("url")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="url"
              type="text"
              placeholder={tr`url`}
              value={addBook.url}
              onChange={(event) => {
                handleChangeBook(event.target.name, event.target.value);
              }}
            />
          </FormGroup>
        </Col>

        <Col xs={12}>
          <RFlex>
            <label>{tr("start_page")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="start_page"
              type="number"
              placeholder={tr`start_page`}
              value={addBook.start_page}
              onChange={(event) => {
                handleChangeBook(event.target.name, event.target.value);
              }}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <RFlex>
            <label>{tr("file")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RFileSuite
              parentCallback={(event) => handleChangeBook("attachment", event)}
              singleFile={true}
              binary={true}
              fileSize={8000}
            />
          </FormGroup>
        </Col>
      </Row>

      <RFlex styleProps={{ justifyContent: "end" }}>
        <RButton
          text={tr`save`}
          color="primary"
          onClick={() => {
            handleSaveBook();
          }}
          loading={addBookLoading}
          disabled={addBookLoading ? true : false}
        />

        <RButton
          text={tr`cancel`}
          color="link"
          onClick={() => handleCloseAddBook()}
        />
      </RFlex>
    </Form>
  );
};

export default RAddBook;
