import React, { useState } from "react";
import { convertBase64 } from "../../../../utils/convertToBase64";
import { TYPES } from "../assignmentReducer";
import { resourcesBaseUrl } from "config/constants";
import { baseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import { translate } from "../../../../utils/translate";

const UploadFileQuestion = ({
  question,
  groupId,
  dispatch,
  answers,
  assignmentReadMode,
  mode,
  student,
}) => {
  const [filename, setFilename] = useState("");
  const changeUploadAnswerAttachmentsHandler = async (e) => {
    const files = e.target.files;

    setFilename(files[0].name);
    let Attachments = [];
    for (const file of Array.from(files)) {
      const newAttachment = await convertBase64(file);

      Attachments.push({
        url: newAttachment,
        file_name: files[0].name,
        type: files[0].type,
      });
    }

    dispatch({
      type: TYPES.SAVE_ANSWER,
      payload: {
        QuestionId: question.id,
        GroupId: groupId,
        Attachments,
        Choices: [],
        FreeAnswer: null,
      },
    });
  };

  return (
    <div className="col-sm-10 col-lg-8">
      <label className="question_file_upload">
        <input
          onChange={(event) => changeUploadAnswerAttachmentsHandler(event)}
          type="file"
          id="questionAttachment"
          name="questionAttachment"
          className="form-control"
          disabled={assignmentReadMode}
          style={{ display: "none" }}
        />
        {translate("Upload")}
      </label>
      <p>{filename}</p>
      {answers &&
        answers.map((answer) => {
          if (answer.QuestionId === question.id) {
            return (
              answer.Attachments.length > 0 &&
              answer.Attachments.map((attachment) => {
                return;
              })
            );
          }
        })}

      {mode !== "multi" && assignmentReadMode && question.notSolved === 0 && (
        <div className="col-md-3" style={{ textAlign: "center" }}>
          {["png", "jpeg"].includes(question.student_attachments.mime_type) ? (
            <ImageEnlarge
              imageSrc={resourcesBaseUrl + question.student_attachments.url}
            />
          ) : (
            <a
              href={resourcesBaseUrl + question.student_attachments.url}
              alt=""
              class="btn btn-primary"
            >
              {translate("Download")}
            </a>
          )}
        </div>
      )}
      {mode === "multi" && student.notSolved === 0 && (
        <div className="col-md-3" style={{ textAlign: "center" }}>
          {["png", "jpeg"].includes(student.student_attachments.mime_type) ? (
            <ImageEnlarge
              imageSrc={resourcesBaseUrl + student.student_attachments.url}
            />
          ) : (
            <a
              href={resourcesBaseUrl + student.student_attachments.url}
              alt=""
              class="btn btn-primary"
            >
              Download
            </a>
          )}
        </div>
      )}
    </div>
  );
};

export default UploadFileQuestion;
