import React, { useState } from "react";
import RGradeStudentByStudent from "view/Courses/CourseManagement/Rubric/GradeStudentByStudent/RGradeStudentByStudent";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { rubricApi } from "api/global/rubric";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import Loader from "utils/Loader";
import { useMutateData } from "hocs/useMutateData";

const GGradeStudentByStudent = () => {
	const { courseId, curriculumId, rubricId } = useParams();

	const [userIndex, setUserIndex] = useState(0);

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["rubricById", rubricId],
		queryFn: () => rubricApi.getRubricById(rubricId),
		enableCondition: rubricId != null,
	});

	const usersData = useFetchDataRQ({
		queryKey: ["users", courseId ?? curriculumId, rubricId],
		queryFn: () => rubricApi.studentsDetails(courseId ?? curriculumId, rubricId),
		enableCondition: rubricId != null,
	});

	const rubricMutation = useMutateData({
		queryFn: (rId, sId) => rubricApi.exportStudentEvaluationsAsPdf(rId, sId),
		invalidateKeys: ["exportStudentEvaluationsAsPdf"],
		downloadFile: true,
	});

	const addRatingToUserMutation = useMutateData({
		queryFn: (data) => rubricApi.addRatingsToUser(data),
		invalidateKeys: ["addRatingToUser"],
	});

	const handleExportStudentAsPdf = () => {
		rubricMutation.mutate(rubricId);
	};

	if ((isLoading || usersData.isLoading) && rubricId != null) return <Loader />;

	return (
		<RGradeStudentByStudent
			data={data?.data?.data}
			users={usersData?.data?.data?.data}
			handleExportStudentAsPdf={handleExportStudentAsPdf}
			loadingPdf={rubricMutation.isLoading}
			userIndex={userIndex}
			setUserIndex={setUserIndex}
		/>
	);
};

export default GGradeStudentByStudent;
