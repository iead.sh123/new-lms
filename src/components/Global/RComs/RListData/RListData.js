import React from 'react'
import { dataTypes } from 'logic/SchoolManagement/constants'
import RProfileName from '../RProfileName/RProfileName'
import RIconDiv from '../RIconDiv/RIconDiv'
import RFlex from '../RFlex/RFlex'
import iconsFa6 from 'variables/iconsFa6'
import { types } from 'logic/SchoolManagement/constants'
const RListData = (
    {
        label,
        data,
        levelType,
        onClick,
        inSelectMode,
        setEnrollUsersOpen,
        elementsToShow = 12,

    }
) => {
    const [showAll, setShowAll] = React.useState(false)
    return (
        <RFlex className='flex-column' styleProps={{ width: '100%' }}>
            <RFlex className="justify-content-between align-items-center" styleProps={{ width: '100%' }}>
                <RFlex>
                    {<span className='p-0 m-0 font-weight-bold'>{label}</span>}
                    <RIconDiv
                        text={data?.ids?.length}
                        divStyle={{
                            borderRadius: '100%', width: '19px', height: '19px',
                            fontSize: '12px', cursor: 'default', margin: '0px'
                        }} />
                </RFlex>
                <span className='p-0 m-0 text-primary' style={{ cursor: 'pointer' }} onClick={() => setEnrollUsersOpen(true)}>Enroll {label}</span>
            </RFlex>
            <RFlex styleProps={{ flexWrap: 'wrap', gap: '20px 10px' }}>
                {data.ids.map((key, index) => {
                    const entity = data[key]

                    if (index+1 <= elementsToShow || showAll) {
                        return (
                            <RFlex id="flex" key={index} className="justify-content-center align-items-center" styleProps={{position:'relative'}} onClick={() => { inSelectMode ? onClick(label, entity) : '' }}>
                                <RProfileName
                                    key={key}
                                    name={entity.name}
                                    img={entity.image ? entity.image : null}

                                    flexStyleProps={{ width: '149px', height: '32px', gap: '5px', cursor: inSelectMode ? 'pointer' : 'default' }} />
                                {inSelectMode && entity.selected &&
                                    <i
                                        className={`${iconsFa6.check} text-success fa-sm`}
                                        style={{ position: 'absolute', right: '0px', bottom: '40%' }}
                                    />}
                            </RFlex>
                        )
                    }
                    else {
                        return null
                    }
                })}
            </RFlex>
            {data?.ids?.length > elementsToShow ?
                <span className='p-0 m-0 text-primary'
                    style={{ cursor: 'pointer' }}
                    onClick={() => setShowAll(!showAll)}
                >{showAll ? "showLess" : "ShowMore"}</span> : ""}
        </RFlex>
    )
}

export default RListData