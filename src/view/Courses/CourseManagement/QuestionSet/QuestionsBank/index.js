import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import RQuestionBankHeader from "./RQuestionBankHeader";
import RQuestionsBankFilters from "./RQuestionsBankFilters";
import RQuestionsCard from "./RQuestionsCard";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";

const RQuestionsBank = () => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<div>
			{QuestionsBankData.ChooseFromQuestionBank && <RQuestionBankHeader />}
			<RQuestionsBankFilters />

			<h6 className="mt-2">{tr`recently_added_in_question_bank`}</h6>

			{QuestionsBankData.questionsBankLoading ? <Loader /> : <RQuestionsCard />}
		</div>
	);
};
1;
export default RQuestionsBank;
