// const DateUtcWithTz = ({ dateTime }) => {
//   const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
//   const date = new Date(`${dateTime}`);
//   const utcDate = date.toLocaleString("en-US", { timeZone: "UTC" });

//   return utcDate;
// };

// export default DateUtcWithTz;

import { DATE_FORMATE } from "config/constants";
import moment from "moment";
const DateUtcWithTz = ({ dateTime, dateFormate }) => {
  const date = moment(new Date(dateTime)).format(
    dateFormate ? dateFormate : DATE_FORMATE
  );

  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    // timeZone: "UTC",
    timeZoneName: "short",
  };

  const localDate = date.toLocaleString("en-US", options);

  return localDate;
};

export default DateUtcWithTz;
