import reactSelect from "react-select";
import { MONITOR_DATA } from "store/actions/global/globalTypes";

export const monitor=(dispatch,s,label="")=> {
    //if you don't want to use it just set debug to false .
    dispatch({type:MONITOR_DATA,label:label,value:s});
  }

