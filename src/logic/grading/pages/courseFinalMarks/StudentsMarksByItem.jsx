import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import { useHistory } from "react-router-dom";
import { get } from "config/api";
import { useSelector, useDispatch } from "react-redux";

import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import Helper from "components/Global/RComs/Helper";
import RBackend from "./RBackend";
import { Services } from "engine/services";


const StudentsMarksByItem = ({  }) => {


  const {courseId}=useParams();

  const [groups, setGroups] = useState([]);
  const [items, setItems] = useState([]);


  useEffect(()=>{

 //   await Helper.fastGet(Services.grading.backend + `student-gradable-items/my-details/type/${type}/${itemId}`,"fail to get api/dashboard",p,p,false);

   // await Helper.fastGet(Services.grading.backend + `student-gradable-items/students-details-list/type/${type}/${itemId}`,"fail to get api/dashboard",p,p,false);
  
  },[])

const dispatch = useDispatch();
const history = useHistory();
const [processsedRecords, setProcessedRecords] = useState([]);


const { user } = useSelector((state) => state?.auth);

const getDataFromBackend = async (specific_url) => {
    const url = Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}`;
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };


  const userCard=(i,n)=>n;//<div>{i}{n}</div>;//<img src={Services.storage.files+i} width={"30px"}/>{n}</div>
  const setData = (response) => {
    // setProcessedRecords(response?.data?.data);
    // return;

    Helper.cl(response,"setData response");
    let processsedRecords1 = [];

    response?.data?.data?.map((r) => {
      let record = {};
      record.allTitle = "allTitle";//r?.creation_date;
      record.title = "title";//r?.description;
      record.table_name = "table_name";//"abuse";

      record.details = [
        // "full_name": "l16 l16",
        // "image": null,
        // "student_id": 862,
        // "id": 862,
        // "teacherMark": "0.00",
        // "calculatedMark": "0.00",
        // "relation_id": 278,
        // "latterGrade": "A+",
        // "GPA": "4.0"
        
        
        
        { key: tr`student`, value: userCard(r.image,r.full_name)},
        { key: tr`teacherMark`, value: r?.teacherMark },
        { key: tr`calculatedMark`, value: r?.calculatedMark },
        { key: tr`latterGrade`, value: r?.latterGrade },
        { key: tr`GPA`, value: r?.GPA },
        
      ];
      record.actions = [
        {
          name: tr`mark details`,
          icon: "fa fa-eye",
          color: "info",

          onClick: () => {
            history.push(
              `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/gradebook/mark-details"
              }`);
          
          }
        },
        {
          name: tr`delete`,
          icon: "fa fa-print",
          color: "info",

          onClick: () => {
           alert("delete")
          },
        },
      ];

      processsedRecords1.push(record);
    });
    setProcessedRecords(processsedRecords1);
  };

const calculate=()=>{
  


	Helper.fastPost(
    Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}/re-calculate`,
    {},
    "calculated successfully",
    "error while calculating",
    () => {      Helper.cl("agreement delte suceess")},
    () => {                 }
    
  );


}

  const flag = true;
  // return <>{Helper.js(processsedRecords)}</>;
  // return <RBackend url={Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}`}/>

  return (
    <div
      className={
        !user?.useNewTheme && user?.type == "parent" ? "container" : "content"
      }
      style={{
        paddingTop: !user?.useNewTheme && user?.type == "parent" && "18vh",
      }}
    >

      <RButton OnClick={calculate}/>
    
    

      {Helper.cl(processsedRecords,"processsedRecords")}
      <RAdvancedLister
        getDataFromBackend={getDataFromBackend}
        setData={setData}
        records={processsedRecords}
        getDataObject={(response) => {
          Helper.cl(response.data.data,"advancedlister response.data")
          return response.data.data}}
        characterCount={15}
        marginT={"mt-3"}
        marginB={"mb-2"}
        showListerMode="tableLister"
      ></RAdvancedLister>
    </div>
  );
};

export default StudentsMarksByItem;
