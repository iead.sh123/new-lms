import React from "react";
import intermediateLevel from "assets/img/new/svg/intermediate.svg";
import beginnerLevel from "assets/img/new/svg/beginner.svg";
import expertLevel from "assets/img/new/svg/expert.svg";
import fairLevel from "assets/img/new/svg/fair.svg";
import iconsFa6 from "variables/iconsFa6";
import RButton from "../RButton";
import styles from "./RCard.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { primaryColor } from "config/constants";
import { dangerColor } from "config/constants";
import { Card } from "reactstrap";

const RCardBack = ({ course, color, user, history }) => {
	return (
		<Card
			className={styles.flip__card__back}
			style={{ borderBottom: `4px solid ${color}`, cursor: "pointer" }}
			key={course?.id}
			onClick={(event) => {
				event.stopPropagation();
				course.link && history.push(course.link);
			}}
		>
			<RFlex className={styles.flip__card__back__container}>
				<RFlex className={styles.flip__card__back__first__container}>
					<RFlex className={styles.flip__card__back__first__content}>
						<RFlex className={styles.flip__card__back__first__content_info}>
							<span className={styles.flip__card__back__first__content__span}>
								{course.name?.length > 30 ? course.name.substring(0, 30) + "..." : course.name}
							</span>
							{user && <i className={iconsFa6.heart} style={{ fontSize: "20px", color: dangerColor, paddingTop: "3px" }} />}
						</RFlex>
						<RFlex styleProps={{ justifyContent: "space-evenly", alignItems: "center" }}>
							{course?.levels?.length > 0 && (
								<RFlex>
									<img
										src={
											course?.levels[0]?.name.toLowerCase() == "intermediate"
												? intermediateLevel
												: course?.levels[0]?.name.toLowerCase() == "fair"
												? fairLevel
												: course?.levels[0]?.name.toLowerCase() == "expert"
												? expertLevel
												: beginnerLevel
										}
										alt={course?.levels[0]?.name}
										width={"14px"}
										height={"14px"}
										style={{ position: "relative", top: "3px", color: primaryColor }}
									/>
									<p className="m-0">{course?.levels[0]?.name}</p>
								</RFlex>
							)}
							{course?.extra?.numberOfHours && (
								<RFlex>
									<i className={iconsFa6.clock + " pt-1"} />
									<p className="m-0"> {+course?.extra?.numberOfHours}</p>
								</RFlex>
							)}
							<RFlex>
								<i className={iconsFa6.globe + " pt-1"} />
								<p className="m-0">{course?.extra?.isOnline == 1 ? `${tr`online`}` : `${tr`offline`}`}</p>
							</RFlex>
						</RFlex>
					</RFlex>
					<RFlex>
						<span className="m-0">
							{course?.overview?.description?.length > 200 ? (
								<RFlex>
									<span>
										{course?.overview?.description.substring(0, 200)}
										{"..."}&nbsp;
										<span style={{ color: primaryColor }}>{tr`read_more`}</span>
									</span>
								</RFlex>
							) : (
								course?.overview?.description
							)}
						</span>
					</RFlex>
				</RFlex>
				<RFlex className={styles.flip__card__back__second__container}>
					{user && course?.extra?.isFree ? (
						<RButton
							text={course?.isEnroll ? tr`enrolled` : tr`enroll_now`}
							disabled={(course.enrollCourse && course.enrollCourse.loading) || course?.isEnroll ? true : false}
							loading={course.enrollCourse && course.enrollCourse.loading}
							onClick={(event) => {
								event.stopPropagation();
								course.enrollCourse && course.enrollCourse.onClick && course.enrollCourse.onClick(course.id);
							}}
							color="primary"
							style={{ width: "100%" }}
						/>
					) : (
						""
					)}
					{user && !course?.extra?.isFree && !course?.isEnroll ? (
						<RFlex className={styles.add__to__cart}>
							<RButton
								className={"ml-0"}
								text={course && course.addToCart && course.addToCart.disabled ? tr`added` : tr`add_to_cart`}
								onClick={(event) => {
									event.stopPropagation();
									course.addToCart && course.addToCart.onClick && course.addToCart.onClick();
								}}
								color={course && course.addToCart && course.addToCart.disabled ? "success" : "primary"}
								loading={course.addToCart && course.addToCart.loading && course.addToCart.loading}
								// disabled={course.addToCart && course.addToCart.loading && course.addToCart.loading}
								disabled={course && course.addToCart && course.addToCart.disabled}
							/>
						</RFlex>
					) : user && !course?.extra?.isFree && course?.isEnroll ? (
						<RButton text={tr`enrolled`} disabled={true} color="success" style={{ width: "100%" }} />
					) : (
						""
					)}

					{!user && course?.extra?.isFree ? <RButton text={tr`enroll_now`} disabled={true} color="primary" /> : ""}
					{!user && !course?.extra?.isFree ? (
						<RFlex className={styles.add__to__cart}>
							<RButton className={"ml-0"} text={tr`add_to_cart`} disabled={true} color="primary" />
						</RFlex>
					) : (
						""
					)}
				</RFlex>
			</RFlex>
		</Card>
	);
};

export default RCardBack;
