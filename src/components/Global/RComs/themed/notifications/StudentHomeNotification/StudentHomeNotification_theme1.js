import React from 'react';
import { Row, Col } from 'reactstrap';
import moment from 'moment';
import styles from './styles/theme1.Module.scss';

const StudentHomeNotification_theme1 = ({ style, content, onClick, time, actions, dir }) =>
    <div className={`col-md-12 col-xs-12 list list-o`}>
        <div
            style={style}
            className={`notificationBody ${styles["NightMode"]}`}
        >
            <a
                onClick={onClick}
            >
                <p className={!dir ? "bubbleNotifar" : "bubbleNotif"}>
                    {content}
                </p>
                <p className="bubbleInfo">{time}</p>
            </a>
            {actions.map(btn => <Button key={`hn-${btn.id}`} {...btn} link>{btn.caption}</Button>)}
        </div>
    </div>


export default StudentHomeNotification_theme1;