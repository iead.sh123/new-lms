import React, { useState } from 'react'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import { Input } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import Loader from 'utils/Loader'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import * as colors from 'config/constants'
import moment from 'moment'
import tr from 'components/Global/RComs/RTranslator'
import * as actions from 'store/actions/global/schoolManagement.action'
import iconsFa6 from 'variables/iconsFa6'
import PlusIcon from 'assets/img/plus_icon.svg'
import RButton from 'components/Global/RComs/RButton'
import { SET_FROM_TIME_TOUCHED, SET_TO_TIME_TOUCHED, SET_TITLE_TOUCHED } from 'store/actions/global/globalTypes'
import { SAVE_PERIOD_INPUT } from 'store/actions/global/globalTypes'
import { UNSAVE_PERIOD_INPUT } from 'store/actions/global/globalTypes'
import { SET_INVALIDATE_PERIOD_FALSE } from 'store/actions/global/globalTypes'
import { SET_INVALIDATE_PERIOD_TRUE } from 'store/actions/global/globalTypes'
const GPeriods = () => {
    const dispatch = useDispatch()
    const [enterPressed, setEnterPressed] = useState(false)
    const { periods } = useSelector((state) => state.schoolManagementRed)
    const isPeriodValidate = (title, start_time, end_time) => {
        if (title == "" || !start_time || !end_time) {
            return false
        }
        return true

    }
    const canIAdd = () => {
        let canAdd = true
        periods.ids.forEach(key => {
            if (!isPeriodValidate(periods[key].title, periods[key].start_time, periods[key].end_time)) {
                canAdd = false
            }
        })
        return canAdd
    }
    const isTimeSmaller = (first_time, second_time) => {
        const [firstHours, firstMinutes] = first_time.split(':').map(Number);
        const [secondHours, secondMinutes] = second_time.split(':').map(Number);

        // Compare hours first
        if (firstHours < secondHours) return true;
        if (firstHours > secondHours) return false;

        // If hours are equal, compare minutes
        return firstMinutes <= secondMinutes;
    }
    const handleNewPeriod = () => {
        if (canIAdd()) {
            setEnterPressed(false)
            dispatch(actions.addNewPeriod())
        }
        else {
            periods.ids.forEach(key => {
                dispatch({ type: SET_TITLE_TOUCHED, payload: { id: periods[key].id } })
                dispatch({
                    type: SET_FROM_TIME_TOUCHED, payload: { id: periods[key].id }
                })
                dispatch({ type: SET_TO_TIME_TOUCHED, payload: { id: periods[key].id } })
            })
            console.log("You Can't Add")
        }
    }
    const handleOnClick = (event, period) => {
        // dispatch({ type: SET_TITLE_TOUCHED, payload: { id: period.id } })
    }
    const handleInputChange = (event, item) => {
        dispatch(actions.changePeriodInputValue(item.id, event.target.value))
    }
    const handleOnBlur = async (event, period) => {
        // dispatch({
        //     type:SET_TITLE_TOUCHED,
        //     payload:{id:period.id}
        // })
        if (enterPressed) {
            return
        }
        let success = false
        dispatch({ type: SET_TITLE_TOUCHED, payload: { id: period.id } })
        if (period.title == "")
            return
        if (period.id > 0) {
            success = await dispatch(actions.addOrEditPeriod({ id: period.id, title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))
        }
        else {
            if (isPeriodValidate(period.title, period.start_time, period.end_time)) {
                success = await dispatch(actions.addOrEditPeriod({ title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))
            }
            else {
                dispatch({ type: SAVE_PERIOD_INPUT, payload: { id: period.id } })

            }

        }
        setEnterPressed(false)

        if (success) {
            dispatch({ type: SAVE_PERIOD_INPUT, payload: { id: period.id } })
        }
    }
    const handleInputDown = async (event, period) => {
        if (event.key === 'Enter') {
            if (enterPressed) {
                return
            }
            setEnterPressed(true)
            let success = false
            dispatch({ type: SET_TITLE_TOUCHED, payload: { id: period.id } })

            if (period.title == "")
                return
            if (period.id > 0) {
                success = await dispatch(actions.addOrEditPeriod({ id: period.id, title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))

            }
            else {
                if (isPeriodValidate(period.title, period.start_time, period.end_time)) {

                    success = await dispatch(actions.addOrEditPeriod({ title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))

                }
                else {
                    dispatch({ type: SAVE_PERIOD_INPUT, payload: { id: period.id } })

                }

            }
            setEnterPressed(false)
            if (success) {
                dispatch({ type: SAVE_PERIOD_INPUT, payload: { id: period.id } })

            }
        }
    }
    const handleFromTimeChange = async (period, value) => {
        dispatch({ type: SET_INVALIDATE_PERIOD_FALSE, payload: { id: period.id } })
        if (period.end_time) {
            if (isTimeSmaller(period.end_time, value)) {
                dispatch({ type: SET_INVALIDATE_PERIOD_TRUE, payload: { id: period.id } })

                return
            }
        }
        dispatch(actions.changePeriodFromTime(period, value))
    }

    const handleFromTimeBlur = async (period) => {
        if (period.title == "") {
            dispatch({ type: SET_TITLE_TOUCHED, payload: { id: period.id } })
            return
        }
        if (period.invalidateTime)
            return
        dispatch({
            type: SET_FROM_TIME_TOUCHED, payload: { id: period.id }
        })

        let success = false
        if (period.id > 0) {
            success = await dispatch(actions.addOrEditPeriod({ id: period.id, title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))

        }
        else {
            if (isPeriodValidate(period.title, period.start_time, period.end_time)) {
                success = await dispatch(actions.addOrEditPeriod({ title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))
            }
        }
    }

    const handleToTimeChange = async (period, value) => {
        dispatch({ type: SET_INVALIDATE_PERIOD_FALSE, payload: { id: period.id } })
        if (period.start_time) {
            if (isTimeSmaller(value, period.start_time)) {
                dispatch({ type: SET_INVALIDATE_PERIOD_TRUE, payload: { id: period.id } })
                return
            }
        }
        dispatch(actions.changePeriodToTime(period, value))
    }

    const handleToTimeBlur = async (period) => {
        if (period.title == "") {
            dispatch({ type: SET_TITLE_TOUCHED, payload: { id: period.id } })
            return
        }
        if (period.invalidateTime)
            return
        dispatch({
            type: SET_TO_TIME_TOUCHED, payload: { id: period.id }
        })

        let success = false
        if (period.id > 0) {
            success = await dispatch(actions.addOrEditPeriod({ id: period.id, title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))

        }
        else {
            if (isPeriodValidate(period.title, period.start_time, period.end_time)) {
                success = await dispatch(actions.addOrEditPeriod({ title: period.title, start_time: period.start_time, end_time: period.end_time, number: period.number }, period.id))
            }
        }

    }
    const handleEditFunctionality = (event, period) => {
        setEnterPressed(false)
        dispatch({ type: UNSAVE_PERIOD_INPUT, payload: { id: period.id } })
        console.log("handleEditFunctionality")
    }
    const deletePeriod = (period) => {
        dispatch(actions.removePeriod(period.id))
    }
    return (
        periods.loading ? <Loader /> :
            periods.ids.length <= 0 ? <RFlex className="flex-column" styleProps={{ gap: '5px', width: '100%' }}>
                <span className='font-weight-bold p-0 m-0'>{tr("Periods")}</span>
                <RFlex className="justify-content-start" styleProps={{ width: '100%' }}>
                    <RButton text={tr("New Period")}
                        faicon='fa-solid fa-plus'
                        color="link"
                        className="text-primary m-0 p-0"
                        style={{ width: 'fit-content' }}
                        onClick={handleNewPeriod} />

                </RFlex>
            </RFlex> :
                <RFlex className="flex-column" styleProps={{ gap: '5px', width: '100%' }}>
                    <RFlex className="justify-content-between" styleProps={{ width: '100%' }}>
                        <span className='font-weight-bold p-0 m-0'>{tr("Periods")}</span>
                        <RButton text={tr("New Period")}
                            faicon='fa-solid fa-plus'
                            color="link"
                            className="text-primary m-0 p-0"
                            style={{ width: 'fit-content' }}
                            onClick={handleNewPeriod} />

                    </RFlex>

                    <RFlex styleProps={{ gap: '7px', width: '100%' }}>
                        <RFlex className="flex-column" styleProps={{ width: '100%', gap: '5px' }}>
                            <RFlex id="text" className="justify-content-between" styleProps={{ width: '64%' }}>
                                <span style={{ color: colors.boldGreyColor, margin: '0px', padding: '0px' }}>{tr("Period Title")}</span>
                                <span style={{ color: colors.boldGreyColor, margin: '0px', marginRight: '25px', padding: '0px' }}>{tr("From")}</span>
                                <span style={{ color: colors.boldGreyColor, margin: '0px', padding: '0px' }}>{tr("To")}</span>
                            </RFlex>
                            {periods.ids?.map((key, index) => {
                                const period = periods[key]
                                const isError = period.touched['title'] && period.title == ""
                                return (
                                    <RFlex
                                        className={`justify-content-between ${isError ? "align-items-start" : "align-items-start"}`}
                                        styleProps={{ width: '95%', gap: '7px' }}>
                                        <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                            <RHoverInput
                                                inputIsNotValidate={period.touched['title'] && period.title == ""}
                                                inputWidth="120px"
                                                item={periods[key]}
                                                inputValue={periods[key].title}
                                                saved={periods[key].saved}
                                                handleOnBlur={handleOnBlur}
                                                handleOnClick={handleOnClick}
                                                handleEditFunctionality={handleEditFunctionality}
                                                handleInputDown={handleInputDown}
                                                handleInputChange={handleInputChange}
                                            />
                                            {period.touched['title'] && period.title == "" && <p className='p-0 m-0 text-danger'>Title is Required</p>}
                                        </RFlex>
                                        <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                            <Input
                                                name="dueDate"
                                                type="time"
                                                // onFocus={() => dispatch({ type: SET_FROM_TIME_TOUCHED, payload: { id: period.id } })}
                                                placeholder={tr`due_time`}
                                                onBlur={() => { handleFromTimeBlur(periods[key]); }}
                                                value={periods[key].start_time || null}
                                                onChange={(e) => handleFromTimeChange(periods[key], e.target.value)}
                                                style={{ height: "40px", width: "120px" }}
                                                className={period.touched['start_time'] && !period.start_time ? "input__error" : ""}
                                            />
                                            {period.touched['start_time'] && !period.start_time && !period.invalidateTime && <p className='p-0 m-0 text-danger'>Time is Required</p>}
                                            {period.invalidateTime && <p className='p-0 m-0 text-danger' style={{ width: '120px' }}>Start Time can't be in the future</p>}

                                        </RFlex>
                                        <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                            <Input
                                                name="dueDate"
                                                type="time"
                                                // onFocus={() => dispatch({ type: SET_TO_TIME_TOUCHED, payload: { id: period.id } })}
                                                placeholder={tr`due_time`}
                                                onBlur={() => { handleToTimeBlur(periods[key]) }}
                                                value={periods[key].end_time || null}
                                                onChange={(e) => handleToTimeChange(periods[key], e.target.value)}
                                                style={{ height: "40px", width: "120px" }}
                                                className={period.touched['end_time'] && !period.end_time ? "input__error" : ""}

                                            />
                                            {period.touched['end_time'] && !period.end_time && !period.invalidateTime && <p className='p-0 m-0 text-danger'>Time is Required</p>}
                                            {period.invalidateTime && <p className='p-0 m-0 text-danger' style={{ width: '120px' }}>End Time can't be in the past</p>}

                                        </RFlex>
                                        <RFlex styleProps={{ height: "100%", marginTop: "12px" }} className="align-items-start">
                                            <i
                                                className={`${iconsFa6.delete} text-danger`}
                                                style={{ cursor: 'pointer' }}
                                                onClick={() => deletePeriod(periods[key])} />

                                        </RFlex>
                                    </RFlex>
                                )
                            }
                            )}
                        </RFlex>

                    </RFlex>
                </RFlex >

    )
}

export default GPeriods

