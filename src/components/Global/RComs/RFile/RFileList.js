import React from "react";
import stylesDropZone from "../RDropzoneComponent.module.scss";
import RSwiper from "../RSwiper/RSwiper";
import tr from "../RTranslator";
import { SwiperSlide } from "swiper/react";
import { Services } from "engine/services";
import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	powerPointIcon,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
	pdfIcon,
} from "config/mimeTypes";

function RFileList({ removeFile, replaceFile, files = [], theme = "default", showReplace, swiperStyle, slideStyle, dynamicSlide }) {
	const thumbs = files?.map((file, index) => {
		const type = file?.mime_type ?? file.hash_id ? file.mime_type : file.type;

		const fSource = imageTypes.includes(type)
			? file.hash_id
				? `${Services.storage.file + file?.hash_id}`
				: file.preview1
			: fileTypes.includes(type)
			? fileIcon
			: fileWord.includes(type)
			? wordIcon
			: fileExcel.includes(type)
			? excelIcon
			: filePowerPoint.includes(type)
			? powerPointIcon
			: filePdf.includes(type)
			? pdfIcon
			: fileVideo.includes(type)
			? videoIcon
			: fileAudio.includes(type)
			? audioIcon
			: Services.storage.file + file.hash_id;
		return (
			<SwiperSlide key={index} className={stylesDropZone.thumbs}>
				<div style={slideStyle}>
					<div className="d-flex justify-content-end">
						<i
							className={stylesDropZone.remove__icon + ` fa-solid fa-circle-xmark`}
							style={{
								color: "#DD0000",
								cursor: "pointer",
								position: "absolute",
								right: "11px",
								top: "0px",
							}}
							onClick={() => removeFile(file?.hash_id ? file?.hash_id : file.id)}
						/>
					</div>

					<div style={{ width: imageTypes.includes(type) ? "130px" : "100px", height: imageTypes.includes(type) ? "100px" : "50px" }}>
						<img
							style={{
								width: imageTypes.includes(type) ? "130px" : "100px",
								height: imageTypes.includes(type) ? "100px" : "50px",
								objectFit: "cover",
								objectPosition: "center top",
							}}
							src={fSource}
							alt={file.file_name ?? file.name}
						/>
					</div>
					{imageTypes.includes(type) ? (
						""
					) : (
						<div className="text-center">
							<span>{file?.file_name ? file?.file_name?.substring(0, 10) : file?.name?.substring(0, 10)}</span>
						</div>
					)}
				</div>

				{showReplace && (
					<button type="button" className={stylesDropZone.btn} onClick={() => replaceFile(file?.hash_id ? file?.hash_id : file.id)}>
						<i className="fa fa-refresh pt-1 pr-1" aria-hidden="true" />
						{tr`replace`}
					</button>
				)}
			</SwiperSlide>
		);
	});

	return (
		<>
			{theme == "default" || theme == "row" ? (
				<>
					{files?.length > 0 && (
						<RSwiper slidesPerView={3} style={swiperStyle} spaceBetween={10}>
							{thumbs}
						</RSwiper>
					)}
				</>
			) : theme == "button_with_replace" ? (
				<div>{thumbs}</div>
			) : theme == "row1" ? (
				<>
					{" "}
					{files?.length > 0 && (
						<RSwiper slidesPerView={5} style={swiperStyle} spaceBetween={10}>
							{thumbs}
						</RSwiper>
					)}
				</>
			) : (
				""
			)}
		</>
	);
}

export default RFileList;
