import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../Rubric.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import { Services } from "engine/services";

const RUsers = ({ users, userIndex, setUserIndex }) => {
	return (
		<RFlex styleProps={{ justifyContent: "space-between", alignItems: "center", width: "100%" }}>
			<RFlex styleProps={{ gap: 20 }}>
				<img src={`${Services.storage.file}${users?.users[userIndex]?.image}`} className={styles.user__image} />
				<RFlex styleProps={{ flexDirection: "column" }}>
					<span>{users?.users[userIndex]?.full_name}</span>
					<span>
						<span>{tr`total`}</span>
						<span style={{ color: primaryColor }}>
							{users?.users[userIndex]?.total_points}/{users.total_point}
						</span>
					</span>
				</RFlex>
			</RFlex>

			<RFlex styleProps={{ gap: 25 }}>
				<RTextIcon
					icon={iconsFa6.chevronLeft}
					text={tr`previous_student`}
					flexStyle={{ color: primaryColor, cursor: "pointer" }}
					onClick={() => setUserIndex((el) => el - 1)}
				/>
				<RTextIcon icon={iconsFa6.userGroup} text={`${userIndex + 1}/${users?.users?.length}`} />
				<RTextIcon
					icon={iconsFa6.chevronRight}
					text={tr`next_student`}
					iconOnRight={true}
					flexStyle={{ color: primaryColor, cursor: "pointer" }}
					onClick={() => setUserIndex((el) => el + 1)}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RUsers;
