import React from "react";
import RCourseCard from "./RCourseCard";
function RCourseCardTest(){
    const tags=[{name:"Grade1",iconColor:"rgb(125,166,254)",backgroundColor:"rgb(217,229,254)"},
               {name:"tag tag",iconColor:"rgb(245,95,37)",backgroundColor:"rgb(254,241,236)"}]
    const  actions=[{title: "View", icon: "fa fa-eye", color:"blue" , onClick:""}]         
    return(
    <>
    <RCourseCard
    bgImage="url(ch.jpg)"
    title="Chemistry"
     tags={tags}
     actions ={actions}
    />
    </>
    )
}
export default RCourseCardTest