import { useState } from "react";
import Helper from "../Helper";
import RFileSuite from "../RFile/RFileSuite";

export const FileTest = () => {
  const [files, setFiles] = useState([
    {
      hash_id: "mopqgkvxdnlbkxyaremzoyjwy",
      extension: "jpg",
      mime_type: "image/jpeg",
      name: "1.jpg",
    },
    {
      hash_id: "dmodgvwxqzlbjxjbrpykmenjw",
      extension: "jpg",
      mime_type: "image/jpeg",
      name: "2.jpg",
    },
    {
      hash_id: "wmjydkonmqpbgzyagzrwelvxk",
      extension: "jpg",
      mime_type: "image/jpeg",
      name: "3.jpg",
    },
    {
      hash_id: "qkkgodzjmeraezqblpvqynwxm",
      extension: "jpg",
      mime_type: "image/jpeg",
      name: "4.jpg",
    },
  ]);
  return (
    <div>
      {" "}
      <RFileSuite
        parentCallback={(event) => {
          setFiles();
        }}
        value={files}
        singleFile={false}
        binary={true}
      />
    </div>
  );
  return (
    <div>
      File Test
      <RFileUploader2
        parentCallback={(event) =>
          UnitPlanContextData.handleAddItemsToSection(
            sectionId,
            item?.id ? item?.id : item?.fakeId,
            "attachments",
            event,
            "parent",
            true
          )
        }
        value={item?.attachments}
        singleFile={true}
        binary={true}
      />
      <RFileUploader2
        parentCallback={(event) => {
          // return;
          UnitPlanContextData.handelSectionChange(
            "attachments",
            event,
            section?.id ? section?.id : section?.fakeId
          );
        }}
        singleFile={true}
        disabled={
          section.attachments &&
          section.attachments !== null &&
          section.attachments.length > 0
            ? true
            : false
        }
        theme="second_theme"
        binary={true}
        placeholder={`Replace the old selected file`}
        // typeFile={["*"]}
        uploadName={"upload name 1"}
        setSpecificAttachment={setSpecificAttachment}
        typeFile={[
          "image/*",
          ".docx",
          ".doc",
          ".pptx",
          "video/mp4",
          "text/*",
          "application/*",
          "*",
        ]}
      />
      <RFileUploader2
        parentCallback={(event) =>
          UnitPlanContextData.handelSectionChange(
            "attachments",
            event,
            section?.id ? section?.id : section?.fakeId
          )
        }
        singleFile={true}
        theme="button_replace_theme"
      />
      <RFileUploader2
        parentCallback={(event) =>
          handelLessonPlanItemChange(
            event,
            "attachments",
            lessonPlanItem?.id ? lessonPlanItem?.id : lessonPlanItem?.idTemp
          )
        }
        value={lessonPlanItem?.attachments}
        singleFile={false}
      />
      <RFileUploader2
        parentCallback={(event) =>
          DiscussionData.handleChangeEditContent("attachments", event)
        }
        value={
          DiscussionData.ePost.attachments &&
          DiscussionData.ePost?.attachments
            .filter((val) => !val.url)
            ?.map((attach) => {
              return {
                ...attach,
                hash_id: Services.discussion.file + attach.hash_id,
              };
            })
        }
        singleFile={false}
      />
      <RFileUploader2
        parentCallback={(event) =>
          DiscussionData.handleChangeCreatePost("attachments", event)
        }
        value={DiscussionData.cPost.attachments
          .filter((val) => !val.url)
          .map((attach) => {
            return {
              ...attach,
              hash_id: Services.discussion.file + attach.hash_id,
            };
          })}
        singleFile={false}
      />
      <RFileUploader2
        parentCallback={handleCallback}
        placeholder={tr`Replace the old selected file`}
      />
      <RFileUploader2
        parentCallback={handleCall}
        placeholder={"Replace the old selected file "}
      />
    </div>
  );
};
