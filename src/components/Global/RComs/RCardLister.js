import RCardHorizontal from "./RCardHorizontal";
import Carousel from "react-carousel-light";
import RCard from "./RCard";
import Loader, { LIST } from "utils/Loader";
import { Link } from "react-router-dom";
import { Row } from "reactstrap";
import REmptyData from "components/RComponents/REmptyData";

const RCardLister = ({
	user,
	cards,
	title,
	linkTitle,
	cardWidth,
	carouselStyle,
	events,
	marginR,
	classButtonSession,
	dataStore,
	messageNoData,
	flag,
	carouselListStyle,
	center,
}) => {
	return (
		<div className="pr-3" style={{ height: "100%" }}>
			{title || linkTitle ? (
				<div className="narrow-title-Details">
					{title ? <span className="ml-2">{title}</span> : null}
					{linkTitle?.title != null && cards?.length != 0 ? (
						<Link to={linkTitle.link} className="float-right mr-3">
							<u>
								{linkTitle.title} <i className="fa fa-angle-double-right"></i>
								<br />
							</u>
						</Link>
					) : null}
				</div>
			) : null}

			{dataStore ? (
				events ? (
					cards?.length != 0 ? (
						<RCardHorizontal carouselStyle={carouselStyle} cardInputs={cards} />
					) : (
						<div
							className="pr-3"
							style={{
								display: "flex",
								justifyContents: "center",
								alignItems: "center",
								height: "80%",
							}}
						>
							<REmptyData line1={messageNoData} line2={"Try again later , please."} />
						</div>
					)
				) : cards?.length != 0 ? (
					<Row className="content pt-3 ">
						<Carousel
							navButtonStyle={
								carouselStyle
									? carouselStyle
									: {
											width: "35px",
											height: "35px",
											margin: "0 -15px",
											top: "50%",
									  }
							}
							wrapperStyle={{
								overflowY: "hidden",
								display: center ? "flex" : "block",
								justifyContent: center ? "center" : "left",
							}}
							listStyle={
								carouselListStyle ?? {
									flexDirection: "row",
									JustifyContent: "space-arround",
								}
							}
							//   navButtonIconColor="#fd822b"
							//   navButtonIconSize="25"
							//   navButtonScrollLength={400}
							//   navButtonScrollDuration={20}
							//   easing="in-out-elastic"
						>
							{cards?.map((cardInput) => {
								return (
									<RCard
										cardInput={cardInput}
										cardWidth={cardWidth}
										carouselStyle={carouselStyle}
										link={cardInput.link}
										events={events}
										marginR={marginR}
										classButtonSession={classButtonSession}
										linkTitle={cardInput.linkTitle}
										user={user}
										messageNoData={messageNoData}
										flag={flag}
										notifications={cardInput.notifications}
										banner={cardInput.banner}
									/>
								);
							})}
						</Carousel>
					</Row>
				) : (
					<div
						style={{
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
							minHeight: "80%",
						}}
					>
						<REmptyData line1={messageNoData} line2={"Try again later , please."} />
					</div>
				)
			) : (
				Loader(LIST)
			)}
		</div>
	);
};
export default RCardLister;
