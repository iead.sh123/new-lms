import React from "react";
import { Button, Row, Col, Form, FormGroup, Input, Label } from "reactstrap";
import RSelect from "components/Global/RComs/RSelect";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./RStateMachine.Module.scss";
import { useDispatch, useSelector } from "react-redux";
import {
	addConstraintToNode,
	removeConstraintFromNode,
	addDataToConstraint,
	getInteractionConstraintType,
	addResponseConstraintTypeToConstraint,
	addResponseConstraint,
	removeAutoCorrectAnswer,
} from "store/actions/global/learningObjects";
import FormByConstraintType from "./FormByConstraintType";
import iconsFa6 from "variables/iconsFa6";

const Interactions = ({ interactions, node, statuses, responses }) => {
	const dispatch = useDispatch();

	const handleAddConstraintToNode = (nodeID) => {
		dispatch(addConstraintToNode(nodeID));
	};

	const handleRemoveConstraintToNode = (nodeID, constraintId) => {
		dispatch(removeConstraintFromNode(nodeID, constraintId));
	};

	const handleAddInteractionToConstraint = (name, value, nodeId, constraintId) => {
		dispatch(addDataToConstraint(name, value, nodeId, constraintId));
		dispatch(getInteractionConstraintType("constraint_types_for_interaction", value, nodeId, constraintId));
	};

	const handleAddDataToConstraint = (name, value, nodeId, constraintId) => {
		dispatch(addDataToConstraint(name, value, nodeId, constraintId));
	};

	const handleAddResponseConstraintTypeToConstraint = (name, value, nodeId, constraintId) => {
		dispatch(addResponseConstraintTypeToConstraint(name, value, nodeId, constraintId));
	};

	const handleAddResponseConstraint = (name, value, nodeId, constraintId, responseScoreType) => {
		dispatch(addResponseConstraint(name, value, nodeId, constraintId, responseScoreType));
	};

	const handleRemoveAutoCorrectAnswer = (ind, nodeId, constraintId) => {
		dispatch(removeAutoCorrectAnswer(ind, nodeId, constraintId));
	};

	return (
		<>
			<Col xs={12}>
				<RFlex className={styles.add_status} onClick={() => handleAddConstraintToNode(node?.id ? node?.id : node?.fakeId)}>
					<i className="fa fa-plus"></i>
					<h6>{tr`add_new_interaction`}</h6>
				</RFlex>
			</Col>

			{node.constraints.map((constraint) => (
				<div key={constraint?.id ? constraint?.id : constraint?.fakeConstraintID} className={styles.constraints}>
					<RFlex>
						<Col xs={4}>
							<FormGroup>
								<Label>{tr`interactions`}</Label>
								<RSelect
									option={interactions}
									closeMenuOnSelect={true}
									placeholder={tr`interactions`}
									onChange={(e) => {
										handleAddInteractionToConstraint(
											"interaction_type_name",
											e.value,
											node?.id ? node?.id : node?.fakeId,
											constraint?.id ? constraint?.id : constraint?.fakeConstraintID
										);
									}}
									defaultValue={[
										{
											label: constraint.interaction_type_name,
											value: constraint.interaction_type_name,
										},
									]}
								/>
							</FormGroup>
						</Col>
						{constraint.constraint_types_for_interaction && (
							<Col xs={4}>
								<FormGroup>
									<Label>{tr`constraint_types`}</Label>
									<RSelect
										option={constraint.constraint_types_for_interaction}
										closeMenuOnSelect={true}
										placeholder={tr`constraint_types`}
										onChange={(e) => {
											handleAddDataToConstraint(
												"type",
												e.value,
												node?.id ? node?.id : node?.fakeId,
												constraint?.id ? constraint?.id : constraint?.fakeConstraintID
											);
										}}
										defaultValue={[
											{
												label: constraint.type,
												value: constraint.type,
											},
										]}
									/>
								</FormGroup>
							</Col>
						)}

						<Col xs={2} className={styles.delete_button}>
							<Button
								className={"btn-icon " + styles.action_button}
								color="danger"
								size="sm"
								onClick={() =>
									handleRemoveConstraintToNode(
										node?.id ? node?.id : node?.fakeId,
										constraint?.id ? constraint?.id : constraint?.fakeConstraintID
									)
								}
							>
								<i className={iconsFa6.delete + " text-white"} />
							</Button>
						</Col>
					</RFlex>
					<Form>
						{constraint.type && (
							<FormByConstraintType
								constraintData={constraint}
								nodeId={node?.id ? node?.id : node?.fakeId}
								constraintId={constraint?.id ? constraint?.id : constraint?.fakeConstraintID}
								statuses={statuses}
								responses={responses}
								handleAddDataToConstraint={handleAddDataToConstraint}
								handleAddResponseConstraintTypeToConstraint={handleAddResponseConstraintTypeToConstraint}
								handleAddResponseConstraint={handleAddResponseConstraint}
								handleRemoveAutoCorrectAnswer={handleRemoveAutoCorrectAnswer}
							/>
						)}
					</Form>
				</div>
			))}
		</>
	);
};

export default Interactions;
