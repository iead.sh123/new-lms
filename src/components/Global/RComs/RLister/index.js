import React from "react";
import withDirection from "hocs/withDirection";
import RTableLister from "../RTableLister";
import RQPaginator from "../RQPaginator/RQPaginator";
import RSwiper from "../RSwiper/RSwiper";
import RCard from "../RCard";
import { SwiperSlide } from "swiper/react";
import { Row, Col } from "reactstrap";
import RNewTableLister from "../RNewTableLister";
import REmptyData from "components/RComponents/REmptyData";

function RLister({
	Records,
	info,
	withPagination = false,
	handleChangePage,
	page,
	showListerMode = "tableLister",
	showCheckIcon,
	hideTableHeader = false,
	firstCellImageProperty,
	align = "center",
	SpecialCard,
	perLine,
	swiper = false,
	overflow,
	overflowX,
	center = false,
	emptyTable = false,
	fixedWidth,
	fixedHeight,
	noBorders,
	tableStyle,
	activeScroll = false,
	maxHeight = "50vh",
	Image,
	line1,
	minHeight,
	disableXScroll = false,
	removeImage,
}) {
	const cards = Records?.map((recordCard, index) => {
		const crd = SpecialCard ? (
			<SpecialCard {...recordCard?.specialProps} /> // React.cloneElement(SpecialCard , { recordCard })
		) : (
			<RCard
				cardInput={recordCard}
				cardWidth="260px"
				marginR={"mr-5"}
				messageNoData="No Courses"
				recordsArr={Records}
				showCheckIcon={showCheckIcon}
				link={recordCard.link}
			/>
		);
		return (
			<>
				{swiper && Records?.length > 3 ? (
					<SwiperSlide>{crd}</SwiperSlide>
				) : (
					<Col
						xl={perLine ? 12 / perLine : 6}
						md={perLine ? 12 / perLine : 6}
						xs={12}
						style={{ display: "flex", justifyContent: "center" }}
					>
						<div> {crd}</div>
					</Col>
				)}
			</>
		);
	});

	return (
		<Row className={"content"} style={{ width: "100%" }}>
			{showListerMode == "tableLister" &&
				(Records?.length > 0 ? (
					<>
						<RNewTableLister
							Records={Records}
							recordsArr={Records}
							hideTableHeader={hideTableHeader}
							firstCellImageProperty={firstCellImageProperty}
							align={align}
							center={center}
							emptyTable={emptyTable}
							fixedWidth={fixedWidth}
							fixedHeight={fixedHeight}
							noBorders={noBorders}
							tableStyle={tableStyle}
							activeScroll={activeScroll}
							maxHeight={maxHeight}
							minHeight={minHeight}
							disableXScroll={disableXScroll}
						/>

						{info && withPagination && <RQPaginator info={info} handleChangePage={handleChangePage} page={page} />}
					</>
				) : (
					<REmptyData Image={Image} removeImage={removeImage} line1={line1} />
				))}

			{showListerMode == "cardLister" && (
				<>
					{swiper && cards.length > 3 ? (
						<RSwiper slidesPerView={4} spaceBetween={60}>
							{cards}
						</RSwiper>
					) : (
						<div
							id="cards_container_stuff"
							style={{
								width: "100%",
								display: "flex",
								flexWrap: "wrap", // height: (perLine?(100/perLine): "59")+"vw",
								overflow: overflow ?? "visible",
								overflowX: overflowX ?? "visible",
							}}
						>
							{cards}
						</div>
					)}
				</>
			)}
		</Row>
	);
}
export default withDirection(RLister);
