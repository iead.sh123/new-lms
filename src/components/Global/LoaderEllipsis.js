import React from "react";

function LoaderEllipsis() {
  return (
    <div className="content text-center">
      <br /> <br />
      <div className="content d-flex justify-content-center align-items-center ">
        <div className="lds-ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
}

export default LoaderEllipsis;
