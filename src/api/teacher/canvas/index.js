import { get, post } from "config/api";

export const CanvasApi = {
  getCourses: () => get("api/canvas/courses"),
  getCourseFiles: (courseId) => get(`api/canvas/courses/${courseId}/files`),
  getCourseQuizzes: (courseId) => get(`api/canvas/courses/${courseId}/quizzes`),
  getQuizQuestions: ({ courseId, quizId }) =>
    get(`api/canvas/courses/${courseId}/quizzes/${quizId}/questions`),

     importQuizQuestions: ({ courseId, quizId,data }) =>
    post(`api/canvas/courses/${courseId}/quizzes/${quizId}/questions`,data),

  importFiles: (data) => post(`api/canvas/courses/files`, data),
};
