import withDirection from "hocs/withDirection";
import React, { useState } from "react";
import {
  Form,
  Collapse,
  FormGroup,
  Button,
  Col,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import RFilterItem from "./RFilterItem";
import RSubmit from "./RSubmit";
import tr from "./RTranslator";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import styles from "./RFilter.Module.scss";

const RFilter = ({ title, filterItems, onFilterClicked, submitValue, dir }) => {
  const [openedCollapses, setOpenedCollapses] = useState(false);

  const collapsesToggle = () => {
    setOpenedCollapses(!openedCollapses);
  };

  return (
    <Col md="12" style={{ top: "-10px" }} dir={dir ? "ltr" : "rtl"}>
      <Row>
        <Col md="4" className="pt-4">
          {" "}
          <strong>{title}</strong>
        </Col>
        <Col md="4"></Col>
        <Col md="4" className={dir ? "float-right" : "float-left"}>
          <FontAwesomeIcon
            icon={faFilter}
            onClick={() => collapsesToggle(1)}
            className={styles.filter_icon}
            id="filter"
            style={{ border: "none" }}
          />
        </Col>
        <UncontrolledTooltip
          delay={0}
          target={"filter"}
        >{tr`filter`}</UncontrolledTooltip>
      </Row>
      <Collapse isOpen={openedCollapses}>
        <Form>
          <Row>
            {filterItems?.map((filterItem) => {
              return (
                // eslint-disable-next-line react/jsx-key
                <Col md="4">
                  <label>{filterItem.placeHolderText} </label>
                  <FormGroup>
                    <RFilterItem
                      Items={filterItem.Items}
                      setSelectedItems={filterItem.setSelectedItems}
                      placeHolderText={filterItem.placeHolderText}
                      isMulti={filterItem.isMulti}
                      required={filterItem.required}
                      selected={filterItem.selected}
                    />
                  </FormGroup>
                </Col>
              );
            })}
            <Col md="4" style={{ lineHeight: "normal" }}>
              <label></label>
              <FormGroup>
                <RSubmit
                  text="filter"
                  value={submitValue ? submitValue : tr("Apply")}
                  onClick={onFilterClicked}
                  type="button"
                ></RSubmit>{" "}
                &nbsp;
                <span hidden={filterItems ? true : false}>
                  <RSubmit
                    text="filter"
                    value={"Reset Filter"}
                    onClick={onFilterClicked}
                    type="button"
                  ></RSubmit>
                </span>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </Collapse>
    </Col>
  );
};
export default withDirection(RFilter);
