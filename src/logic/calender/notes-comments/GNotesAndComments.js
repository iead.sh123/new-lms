import RModal from 'components/Global/RComs/RModal';
import React from 'react';
import { Container, Row, Col, Input, UncontrolledTooltip } from 'reactstrap';
import Comment from '../Comment';
import CommentsAndNotesInput from '../CommentsAndNotesInput';


const GNotesAndComments = ({ comments, handleAdd, mode, handleToggle, isOpen }) => {

    return <RModal
        isOpen={isOpen}
        toggle={handleToggle}
        header={<h6 className="text-capitalize">{mode}</h6>}
        body={
            <Container style={{ overflow: 'scroll', maxHeight: '70vh' }}>
                {comments?.ids?.map?.((cId, index) => <Row key={'calender_comments_notes' + cId}>
                    <Col>
                        <Comment scrollToView={index === comments?.ids.length - 1} text={comments.byId[cId].text} creatorName={comments.byId[cId].user_name} date={comments.byId[cId].created_at} note={mode === 'notes'} />

                    </Col>
                </Row>)}
            </Container>
        }
        footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
        size="lg"
    />

    // <RTabsPanel
    //       title={data.title}
    //       Tabs={data.tabs}
    //       SelectedIndex={data.tabIndex}
    //       changeHorizontalTabs={data.changeHorizontalTabs}
    //     ></RTabsPanel>
}


export default GNotesAndComments;