import React from "react";
import { Card, CardBody, CardHeader, Row, Col } from "reactstrap";
import { timeDiffCalc } from "utils/CurrentDate";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import styles from "./AllDiscussions.Module.scss";
import avatar from "assets/img/avatar.png";
import withDirection from "hocs/withDirection";
import moment from "moment";
import DiscussionSetting from "./DiscussionSetting";

const AllDiscussions = ({ discussion, dir }) => {
  const { user } = useSelector((state) => state?.auth);

  return (
    <Card>
      <CardHeader className={styles.card_header}>
        <Row>
          <Col md={1} xs={2}>
            <Link
              // onClick={() =>
              //   idInUrl != post.content.user["id"]
              //     ? dispatch(emptyPostsByUser())
              //     : ""
              // }
              to={
                user?.type == "teacher"
                  ? `${process.env.REACT_APP_BASE_URL}/teacher/all-posts/${discussion.content.user["id"]}`
                  : user?.type == "student"
                  ? `${process.env.REACT_APP_BASE_URL}/student/all-posts/${discussion.content.user["id"]}`
                  : user?.type == "parent"
                  ? `${process.env.REACT_APP_BASE_URL}/parent/all-posts/${discussion.content.user["id"]}`
                  : user?.type == "employee"
                  ? `${process.env.REACT_APP_BASE_URL}/admin/all-posts/${discussion.content.user["id"]}`
                  : `${process.env.REACT_APP_BASE_URL}/senior-teacher/all-posts/${discussion.content.user["id"]}`
              }
            >
              <img
                className="rounded-circle"
                width="45"
                src={
                  discussion.content.user.image != null
                    ? process.env.REACT_APP_RESOURCE_URL +
                      discussion.content.user.image.url
                    : avatar
                }
                alt=""
              />
            </Link>
          </Col>
          <Col md={7} xs={8}>
            <div className="ml-2">
              <Link
                // onClick={() =>
                //   idInUrl != post.content.user["id"]
                //     ? dispatch(emptyPostsByUser())
                //     : ""
                // }
                to={
                  user?.type == "teacher"
                    ? `${process.env.REACT_APP_BASE_URL}/teacher/all-posts/${discussion.content.user["id"]}`
                    : user?.type == "student"
                    ? `${process.env.REACT_APP_BASE_URL}/student/all-posts/${discussion.content.user["id"]}`
                    : user?.type == "parent"
                    ? `${process.env.REACT_APP_BASE_URL}/parent/all-posts/${discussion.content.user["id"]}`
                    : user?.type == "employee"
                    ? `${process.env.REACT_APP_BASE_URL}/admin/all-posts/${discussion.content.user["id"]}`
                    : `${process.env.REACT_APP_BASE_URL}/senior-teacher/all-posts/${discussion.content.user["id"]}`
                }
              >
                <div className="title-Details m-0 text-dark">
                  {discussion.content.user["full_name"]}
                </div>
              </Link>
              <div
                className={dir ? "text-muted" : styles.time_rtl + " text-muted"}
              >
                <i className="fa fa-clock-o" style={{ padding: 3 }}></i>{" "}
                <Link
                  //   onClick={() =>
                  //     idInUrl != post.url_id ? dispatch(emptySpecificPost()) : ""
                  //   }
                  to={
                    user?.type == "teacher"
                      ? `${process.env.REACT_APP_BASE_URL}/teacher/post/${discussion.url_id}`
                      : user?.type == "student"
                      ? `${process.env.REACT_APP_BASE_URL}/student/post/${discussion.url_id}`
                      : user?.type == "parent"
                      ? `${process.env.REACT_APP_BASE_URL}/parent/post/${discussion.url_id}`
                      : user?.type == "employee"
                      ? `${process.env.REACT_APP_BASE_URL}/admin/post/${discussion.url_id}`
                      : `${process.env.REACT_APP_BASE_URL}/senior-teacher/post/${discussion.url_id}`
                  }
                >
                  <span className="text-muted h7 mb-2">
                    {timeDiffCalc(
                      new Date(
                        moment(
                          new Date(discussion.created_at).toString()
                        ).format("MM-DD-YYYY hh:mm a")
                      )
                    )}
                  </span>
                </Link>
              </div>
            </div>
          </Col>
          <Col
            md={4}
            xs={2}
            style={{ display: "flex", justifyContent: "flex-end" }}
          >
            {/* <DiscussionSetting
              discussionData={discussion}
              marginTop="8px"
              showAccess={
                discussion.content?.user["id"] == user?.id ? true : false
              }
            /> */}
          </Col>
        </Row>
        {/* {discussion.title} */}
      </CardHeader>
    </Card>
  );
};

export default withDirection(AllDiscussions);
