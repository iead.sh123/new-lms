import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Services } from "engine/services";
import { DANGER } from "utils/Alert";
import { post } from "config/api";
import ReactBSAlert from "react-bootstrap-sweetalert";
import RAddEdit from "components/Global/RComs/RAddEdit";
import Loader from "utils/Loader";
import Swal from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";

const GenericUIAddEdit = (props) => {
	const [hiddenColumns, setHiddenColumns] = useState(["id", "password"]);
	const { entity_specific_name, entity_specific_id } = useParams();
	let record_id = entity_specific_id;

	let table_name = "";
	if (props && props.prop_entity_specific_name) {
		table_name = props.prop_entity_specific_name;
		record_id = false;
	} else table_name = entity_specific_name;

	const history = useHistory();

	const [initialformItems, setInitialFormItems] = useState([]);
	const [tableProps, setTableProps] = useState({});
	const [alerts, setAlerts] = useState(false);
	const [record, setRecord] = useState(null);

	const [loading, setLoading] = useState(false);
	const [addLoading, setAddLoading] = useState(false);

	const TypeEnum = {
		text: "text",
		number: "number",
		time: "time",
		checkbox: "checkbox",
		select: "select",
		color: "color",
		file: "file",
	};

	const showAlerts = (child) => {
		setAlerts(child);
	};

	//handle options to set in select
	const getOptionsFromData = (data) => {
		const output = [];
		data.map((d) => {
			if (d.ID) output.push({ value: d.ID, label: d.name });
		});

		return output;
	};

	//handleChange type:text
	const handleTextChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:number
	const handleValueChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:select
	const handleSelectValueChanged = (e, column) => {
		let temp = record;
		temp[column] = e.value;
		setRecord(temp);
	};

	//handleChange type:checkbox
	const handleChecked = (e, column) => {
		let temp = record;
		temp[column] = e.target.checked;
		setRecord({ ...temp });
	};

	//handleChange type:file
	//use this way because the RDropzone Component return callback (contain data)
	const handleFileChanged = (childData, column) => {
		if (childData) {
			// let listDataAdded = [];
			// childData.map((item) => {
			//   listDataAdded.push({
			//     url: item.url,
			//     file_name: item.file_name,
			//     type: item.type,
			//   });
			// });

			let temp = record;
			temp[column] = childData;
			setRecord({ ...temp });
		}
	};

	//routing to function to set data
	const getFormItemOnChange = (fieldtype, c) => {
		if (fieldtype == TypeEnum.text) {
			return (e) => handleTextChanged(e, c);
		} else if (fieldtype == TypeEnum.select) {
			return (e) => handleSelectValueChanged(e, c);
		} else if (fieldtype == TypeEnum.number) {
			return (e) => handleValueChanged(e, c);
		} else if (fieldtype == TypeEnum.checkbox) {
			return (e) => handleChecked(e, c);
		} else {
			return (e) => handleTextChanged(e, c);
		}
	};

	//to set type in object
	const getFormItemType = (field) => {
		let type = TypeEnum.text;
		if (field.IsDropDownList) {
			type = TypeEnum.select;
		} else if (field.Title?.toLowerCase().includes("color")) {
			type = TypeEnum.color;
		} else if (field?.Type == "file") {
			type = TypeEnum.file;
		} else {
			const fieldType = field?.Type;
			if (fieldType.includes("varchar") || fieldType.includes("text")) {
				type = TypeEnum.text;
			}
			if (fieldType.includes("bigint") || fieldType.includes("int") || fieldType.includes("smallint")) {
				type = TypeEnum.number;
			}
			if (fieldType.includes("time")) {
				type = TypeEnum.time;
			}
			if (fieldType.includes("tinyint")) {
				type = TypeEnum.checkbox;
			}
		}
		return type;
	};

	//call api to set record ...
	useEffect(() => {
		const getData = async () => {
			setLoading(true);
			let Request = record_id
				? {
						payload: {
							TableName: table_name,
							id: record_id,
						},
				  }
				: {
						payload: { TableName: table_name },
				  };

			let response1 = await post(`${Services.auth_organization_management.backend}api/generic/getTableData`, Request);
			//setHasAddPermission(response1.data?.data?.has_add_permission);
			if (response1 && response1.data.status && response1.data && response1.data.data) {
				setLoading(false);
				let data = response1.data.data;
				let init = {};
				if (props.preSetValues) {
					props.preSetValues.map((psv) => {
						init[psv.field] = psv.value;
					});
				}

				setRecord(record_id ? data.records : init);
				setTableProps(data.table_props);
				if (data.hidden_fields && data.hidden_fields.length > 0) setHiddenColumns(data.hidden_fields);
			}
		};
		getData();
	}, [table_name]);

	//initialFormItems
	useEffect(() => {
		let form_data = [];

		tableProps?.data?.map((tp, key) => {
			if (hiddenColumns.findIndex((t) => t == tp.Field) !== -1) return null;
			let t = getFormItemType(tp);
			let formitem = {};
			let selectOptions = getOptionsFromData(tp.data);
			let ss = selectOptions.filter((i) => i.value == record[tp.Field]);
			let selected = record ? ss[0] : null;

			if (t == TypeEnum.select) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					required: false,
					isMulti: false,
					option: selectOptions,
					onChange: getFormItemOnChange(t, tp.Field),
					// defaultValue: tp.description
					value: selected,
					defaultValue: selected,
				};
			} else if (t == TypeEnum.checkbox) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					onChange: getFormItemOnChange(t, tp.Field),
					checked: record ? record[tp.Field] : null,
				};
			} else if (t == TypeEnum.file) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					handleData: (file) => handleFileChanged(file, tp.Field),
					fileSize: 200,
					binary: true,
					value: record ? record[tp.Field] : null,
				};
			} else {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					required: false,
					onChange: getFormItemOnChange(t, tp.Field),
					// defaultValue: tp.description
					defaultValue: record ? record[tp.Field] : null,
				};
			}
			form_data.push(formitem);
		});
		setInitialFormItems(form_data);
	}, [record, tableProps]);

	//action
	const onAddEditSubmit = async (e) => {
		setAddLoading(true);
		e.preventDefault();

		const payload = {
			payload: {
				TableName: table_name,
				record: record,
			},
		};

		const res = await post(`${Services.auth_organization_management.backend}api/generic/store`, payload);

		if (res && res.data && res.data.status == 1) {
			setAddLoading(false);

			showAlerts(
				<ReactBSAlert
					success
					title={!record_id ? tr`Added Successfully` : "Edited Successfully"}
					onConfirm={() => {
						history.goBack();
					}}
				></ReactBSAlert>
			);
		} else {
			setAddLoading(false);
		}

		return false;
	};

	return (
		<>
			{loading ? (
				<Loader />
			) : (
				<>
					{alerts}

					<RAddEdit
						initialFormItems={initialformItems}
						onSubmit={(e) => {
							onAddEditSubmit(e);
							if (props.onSubmit) props.onSubmit();
						}}
						oldObject={!(props && props.preSetValues) && record_id && initialformItems}
						allowEdit={record?.has_edit_permission}
						preSetValues={props && props.preSetValues}
						addLoading={addLoading}
					/>
				</>
			)}
		</>
	);
};

export default GenericUIAddEdit;
