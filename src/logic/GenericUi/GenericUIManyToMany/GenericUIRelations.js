import React, { useState } from "react";
import reducer, { initialState } from "./reducer";
import { useHistory, useParams } from "react-router-dom";
import { STAGE1, STAGE2, STAGE3 } from "./stageManager";
import tr from "components/Global/RComs/RTranslator";
import { manyToManyAPI, oneToManyAPI } from "api/admin/genericUI";
import { getStageComponent } from "./stageManager";
import {
	getPivotTablePayload,
	getTableDataPayload,
	manyToManyCreateDeleteRelationPayload,
	manyToManyCreateAddRelationPayload,
	oneToManyCreateAddRelationPayload,
	oneToManyCreateDeleteRelationPayload,
} from "./utils";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import Loader from "utils/Loader";
import { post } from "config/api";
import { useDispatch } from "react-redux";

import { INITIALIZE_STATE, ADD_RELATION, CANCEL_ADD_RELATION, DELETE_RELATION, UPDATE_ATTRIBUTE, SET_STAGE } from "./actions";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GenericAddContainer from "../GenericAddContainer";
import ExcelPage from "../ExcelPage";
import filterReducer from "components/Global/RComs/filterState/filterReducer";
import { filterState } from "components/Global/RComs/filterState/filterReducer";
import StudentsErrors from "../StudentsErrors";
import { studentsErrorsAsync } from "store/actions/admin/studentsErrors.Action";
import { genericPath } from "engine/config";
import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const GenericUIRelations = () => {
	const dispatchGlobalState = useDispatch();
	const history = useHistory();
	const [uiData, dispatch] = React.useReducer(reducer, initialState);
	const [openedCollapses, setOpenedCollapses] = useState(false);
	const [showExcelPage, setShowExcelPage] = useState(false);
	const [openExcelPage, setOpenExcelPage] = useState(null);
	const [openStudentNotify, setOpenStudentNotify] = useState(null);
	const [openStudentError, setOpenStudentError] = useState(null);
	const [importExcel, setImportExcel] = useState(null);

	/*------------------url params ------------------*/
	const {
		entity_specific_id,
		masterTableName: entity_specific_name,
		detailTableName: detailsTable,
		pivotTableName: pivotTable,
		manyToMany,
	} = useParams();

	const isManyToMany = manyToMany != "0" && manyToMany;
	const tabTitle = entity_specific_name;

	/*------------------ refs  ------------------*/
	const mounted = React.useRef(false);
	const [localnewfilter, filterdispatch] = React.useReducer(filterReducer, filterState);

	/*------------------store handlers ------------------*/
	//initialize_state
	const handleMutateGlobalState = (fieldName, value) =>
		dispatch({
			type: INITIALIZE_STATE,
			payload: { fieldName: fieldName, value: value },
		});

	//relation
	const handleAddRelation = (detailId) => dispatch({ type: ADD_RELATION, payload: { detailId: detailId } });
	const handleDeleteRelation = (detailId) => dispatch({ type: DELETE_RELATION, payload: { detailId: detailId } });
	const handleCancelAddRelation = () => dispatch({ type: CANCEL_ADD_RELATION });

	//attribute
	const handleUpdateAttribute = (detailId, attributeName, value) =>
		dispatch({
			type: UPDATE_ATTRIBUTE,
			payload: { detailId, attributeName, value },
		});

	//stage
	const handleSetStage = (stage) => dispatch({ type: SET_STAGE, payload: stage });

	/*------------------ util functions ------------------*/
	const handleSubmitRelations = async () => {
		handleMutateGlobalState("loading", true);
		try {
			let payload = null,
				response = null;
			if (isManyToMany) {
				payload = manyToManyCreateAddRelationPayload(
					pivotTable /*params*/,
					tabTitle,
					entity_specific_id,
					detailsTable /*params*/,
					uiData.rightSide
				);
				response = await manyToManyAPI.storeRelation(payload);
			} else {
				// tableName,masterTable,detaildId,masterId
				payload = oneToManyCreateAddRelationPayload(detailsTable, tabTitle, uiData.rightSide, entity_specific_id);

				response = await oneToManyAPI.storeRelation(payload);
			}
			if (!response.data.status) {
				throw new Error(response.data.msg);
			}
			toast.success(tr`relation_added`);

			mounted.current && handleMutateGlobalState("currentStage", STAGE1);
			mounted.current && handleMutateGlobalState("rightSide", []);
		} catch (err) {
			toast.error(tr(err.message));
		} finally {
			mounted.current && handleMutateGlobalState("loading", false);
		}
	};

	const handleDeleteRelationStage1 = async (rel_id) => {
		handleMutateGlobalState("loading", true);
		try {
			let response = null;
			let payload = null;

			if (isManyToMany) {
				payload = manyToManyCreateDeleteRelationPayload(pivotTable /*params*/, rel_id);
				response = await manyToManyAPI.deleteRelation(payload);
			} else {
				// tableName, masterTable, detaildId
				payload = oneToManyCreateDeleteRelationPayload(detailsTable /*params*/, tabTitle /*params*/, rel_id);
				response = await oneToManyAPI.deleteRelation(payload);
			}

			if (!response.data.status) {
				throw new Error(response.data.msg);
			}
		} catch (err) {
			toast.error(tr(err.message));
		} finally {
			mounted.current && handleMutateGlobalState("loading", false);
		}
	};

	const getPivotDataFromBackend = async (specificUrl) => {
		const payload = getPivotTablePayload(tabTitle, pivotTable, entity_specific_id, detailsTable /*params*/);

		try {
			const response = specificUrl ? await post(specificUrl, payload) : await manyToManyAPI.getRightSide(payload);
			if (!response.data.status) throw new Error(response.data.msg);

			mounted.current && isManyToMany && handleMutateGlobalState("relationPropsSchemes", response.data.data.props);
			mounted.current && handleMutateGlobalState("pivotRecords", response);
			toast.error(err?.message);
			return response;
		} catch (err) {
			toast.error(err?.message);
			return [];
		}
	};

	const setPivotData = (response) => {
		if (!response) {
			return [];
		}

		setImportExcel(response.data.data.ok_to_upload_from_file);

		const data = response?.data?.data?.records?.data ? response.data.data.records.data : response.data.data.records;

		const keys = Object.keys(response.data.data.fields).filter((k) => k.toUpperCase() !== "ID");

		return data.map((rc) => {
			return {
				allTitle: rc.name,
				title: rc.name,
				initialCheckValue: true,
				details: keys.map(
					(k) => ({
						key: k,
						value: rc[k],
					})
					// details: keys.map(k => ({    key:k.Title+"ss",
					// value:k.IsDropDownList?k.data.filter(s=>s.ID==rc[k.Field])[0].name:rc[k.Field]
				),

				actions: [
					{
						permissions: response?.data?.data?.has_delete_permission && response?.data?.data?.can_be_deleted ? true : false,
						name: "remove",
						icon: iconsFa6.delete,
						id: "remove" + rc.id,
						onClick: () => {
							handleDeleteRelationStage1(rc.id);
						}, // handle Delete relation (id)
					},
					{
						permissions: tabTitle == "school_class" && detailsTable == "students" ? true : false,
						name: "card",
						icon: "fa fa-id-card",
						id: "template" + rc.id,
						onClick: () => {
							history.push({
								pathname: `${Services.auth_organization_management.backend}/${genericPath}/report-card-template/student/${rc.id}`,
								state: { mode: "student" },
							});
						},
					},
					{
						permissions: tabTitle == "school_class" && detailsTable == "students" ? true : false,
						name: "print",
						icon: "fa fa-print",
						id: "print" + rc.id,
						onClick: () => {
							history.push({
								pathname: `${Services.auth_organization_management.backend}/${genericPath}/report-card-template/student/print/${rc.id}`,
								state: {
									mode: "print",
									classId: rc.class_id,
									publishToParentFlag: response.data.data.publish_report_to_all_students,
								},
							});
						},
					},
				],
			};
		});
	};

	const getTableDataFromBackend = async (specificURL) => {
		if (uiData.previousStage === STAGE3 && !specificURL) {
			return uiData.tableRecords;
		}

		const payload = getTableDataPayload(detailsTable, entity_specific_name, entity_specific_id /*params*/);

		payload.payload.Filters = localnewfilter.newFilter; //.length == 1 ? "" : filters

		try {
			const response = specificURL ? await post(specificURL, payload) : await manyToManyAPI.getTableData(payload);
			if (!response.data.status) throw new Error(response.data.msg);

			mounted.current && handleMutateGlobalState("tableRecords", response);
			return response;
		} catch (err) {
			toast.error(err?.message);
			return [];
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		const data = response?.data?.data.records?.data ? response.data.data.records.data : response.data.data.records;
		const keys = Object.keys(response.data.data.fields).filter((k) => k.toUpperCase() !== "ID");

		return data.map((rc) => ({
			allTitle: rc.name || rc.full_name,
			title: rc.name || rc.full_name,
			initialCheckValue: true,
			details: keys.map((k) => ({
				key: [response.data.data.fields[k]],
				value: rc[k],
			})),

			actions:
				uiData.rightSide.findIndex((rs) => rs.detailId == rc.id) < 0
					? uiData.pivotRecords.data?.data?.has_add_permission
						? [
								{
									name: "add",
									id: "add" + rc.id,
									icon: "nc-icon nc-simple-add ",
									color: "primary",
									onClick: () => handleAddRelation(rc.id),
								},
						  ]
						: []
					: uiData.pivotRecords.data?.data?.has_delete_permission
					? [
							{
								name: "remove",
								icon: iconsFa6.delete,
								id: "remove-td" + rc.id,
								color: "danger",
								onClick: () => handleDeleteRelation(rc.id),
							},
					  ]
					: [],
		}));
	};

	/*------------------ effects ------------------*/
	React.useEffect(() => {
		mounted.current = true;
		handleMutateGlobalState("isManyToMany", isManyToMany);
		return () => (mounted.current = false);
	}, []);

	React.useEffect(() => {
		// mounted.current && (uiData.currentStage === STAGE1) && getPivotTable();
		mounted.current && handleMutateGlobalState("leftSideId", entity_specific_id);
	}, [entity_specific_id, tabTitle, uiData.currentStage]);

	const handleCheckStudents = () => {
		dispatchGlobalState(studentsErrorsAsync(entity_specific_id, setOpenStudentError, dispatchGlobalState, setOpenStudentNotify));
	};

	/*------------------ prerender functions and vars ------------------*/
	const propsLiterals = React.useMemo(
		() => ({
			[STAGE1]: {
				actions: {
					handleAdd: () => {
						handleSetStage(STAGE2);
					},
					handleAddNew: () => {
						setOpenedCollapses(true);
					},
					handleShowExcel: () => {
						setOpenExcelPage(true);
					},
					handlePublishReport: () => {
						handleCheckStudents();
					},
				}, //handle call the second api
				hasAddPermission: uiData.pivotRecords?.data?.data?.allow_add_existed,
				hasAddNewChildPermission: uiData.pivotRecords?.data?.data?.allow_add_new,
				hasPublishReport: uiData.pivotRecords?.data?.data?.publish_report_to_all_students,
				listerProps: {
					records: setPivotData(uiData.pivotRecords) || [],
					getDataFromBackend: getPivotDataFromBackend,
					setData: setPivotData,
					getDataObject: (response) => response.data.data.records,
					marginTStandard: "mb-4",
					marginBStandard: "mt-4",
					submitValue: "Filter",
					localnewfilter: localnewfilter,
					filterdispatch: filterdispatch,
				},
				ok_to_upload_from_file: uiData.pivotRecords?.data?.data?.ok_to_upload_from_file,
			},

			[STAGE2]: {
				containerTitle: `Adding`, //${detailsTable.split(/\_|\-|\./).join(" ")}`,
				containerActions: [
					{
						btnColor: "Danger",
						handler: () => handleSetStage(STAGE1),
						btnCaption: tr`cancel`,
					},
					{
						btnColor: "Primary",
						handler: handleSubmitRelations,
						btnCaption: tr`store`,
					},
				],
				listerProps: {
					records: setTableData(uiData.tableRecords) || [],
					getDataFromBackend: getTableDataFromBackend,
					setData: setTableData,
					getDataObject: (response) => response.data.data.records,
					marginTStandard: "mb-4",
					marginBStandard: "mt-4",
					submitValue: "Filter",
					table_props: uiData.tableRecords?.data?.data?.table_props?.data,
					localnewfilter: localnewfilter,
					filterdispatch: filterdispatch,
					// getData: () => getTableDataFromBackend(null, true)
				},
			},
			[STAGE3]: isManyToMany
				? {
						//attrs
						scheme: uiData.relationPropsSchemes,
						handleUpdateProps: (attributeName, value) => handleUpdateAttribute(uiData.selectedDetailId, attributeName, value),
						containerTitle: tr`relation_builder`,
						containerActions: [
							{
								btnColor: "Danger",
								handler: () => {
									handleSetStage(STAGE2);
									handleCancelAddRelation();
								},
								btnCaption: tr`cancel`,
							},
						],
						handleSubmit: () => handleSetStage(STAGE2),
						relationItems: uiData.rightSide.find((rs) => rs.detailId === uiData.selectedDetailId)?.item,
				  }
				: null,
		}),
		[
			uiData.currentStage,
			uiData.pivotRecords,
			uiData.tableRecords,
			uiData.rightSide,
			uiData.loading,
			detailsTable,
			localnewfilter.newFilter,
			importExcel,
		]
	);

	const StageComponent = getStageComponent(uiData.currentStage);

	return (
		<>
			<AppModal
				show={openedCollapses}
				parentHandleClose={() => {
					setOpenedCollapses(false);
				}}
				header={"Adder"}
				headerSort={
					<GenericAddContainer
						prop_entity_specific_name={detailsTable}
						preSetValues={[
							{
								field: uiData.pivotRecords?.data?.data?.master_field,
								value: entity_specific_id,
								disable: true,
							},
						]}
						onSubmit={() => {
							// alert("finished");
						}}
					/>
				}
			></AppModal>

			<AppModal
				show={openExcelPage}
				parentHandleClose={() => {
					setOpenExcelPage(false);
				}}
				header={"Excel Page"}
				// style={{ width: 1000 }}
				size="lg"
				headerSort={
					<ExcelPage
						master_table={entity_specific_name}
						master_id={entity_specific_id}
						detail_table={detailsTable}
						setShowExcelPage={setShowExcelPage}
						typeFile={importExcel?.allowed_file_types?.toString()}
					/>
				}
			></AppModal>

			<AppModal
				show={openStudentError}
				parentHandleClose={() => {
					setOpenStudentError(false);
				}}
				header={"Students"}
				size="lg"
				headerSort={<StudentsErrors setShowStudents={setOpenStudentError} />}
			></AppModal>
			{/* 
      <AppModal
        show={openStudentNotify}
        parentHandleClose={() => {
          setOpenStudentNotify(false);
        }}
        header={"Notify"}
        size="lg"
        headerSort={<h6>{tr`We will notify you after finish the process`}</h6>}
      ></AppModal> */}

			{uiData.loading ? (
				Loader()
			) : (
				<>
					<StageComponent {...propsLiterals[uiData.currentStage]} />
				</>
			)}
		</>
	);
};

export default GenericUIRelations;
