import React from "react";
import tr from "./RComs/RTranslator";

function Loader() {
  return (
    <div className="content text-center">
      <br /> <br />
      <div className="content d-flex justify-content-center align-items-center mt-5">
        <div className="loader mb-3"></div>
      </div>
      {tr`loading`}....
    </div>
  );
}

export default Loader;
