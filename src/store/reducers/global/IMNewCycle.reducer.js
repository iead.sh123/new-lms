import Helper from "components/Global/RComs/Helper";
import { produce, current } from "immer";
import { MessageStatus } from "store/actions/IMCycle2.actions";
import { DELETE_MESSAGE } from "store/actions/IMCycle2.actions";
import { UNSEND_MESSAGE } from "store/actions/IMCycle2.actions";
import { SET_REPLIED_MESSAGE } from "store/actions/IMCycle2.actions";
import { SELECT_MULTIPLE_CHATS } from "store/actions/IMCycle2.actions";
import { CHAT_SEEN } from "store/actions/IMCycle2.actions";
import { UPDATE_GROUP } from "store/actions/IMCycle2.actions";
import { UPDATE_IMAGE_THUMBNAIL } from "store/actions/IMCycle2.actions";
import { SELECT_MULTIPLE_MESSAGES } from "store/actions/IMCycle2.actions";
import { SELECT_GROUP } from "store/actions/IMCycle2.actions";
import { SET_FORWARDED_MESSAGE } from "store/actions/IMCycle2.actions";
import { TAG_MESSAGE } from "store/actions/IMCycle2.actions";
import { GET_MESSSAGES } from "store/actions/IMCycle2.actions";
import { SELECT_CHAT } from "store/actions/IMCycle2.actions";
import {
  INITIALIZE_IM,
  SET_SETTINGS,
  PUSH_MESSAGE,
  CHANGE_MESSAGE_STATUS,
  PUSH_CONVERSATION,
  SET_SEARCHED_MESSAGE,
  SET_CHUNK,
  DELETE_CHAT,
  TAG_CHAT,
} from "store/actions/IMCycle2.actions";
import { ChatTags } from "views/Teacher/IM/constants/ChatTags";
import { MessageTags } from "views/Teacher/IM/constants/MessageTags";

const initialState = {
  activeChat: null,
  forwardedMessage: null,
  selectedGroup: ChatTags.All,
  lastMessageChatId: null,
  storeIsInitialized: false,
  settings: {},
  searchedMessage: null,
  repliedMessage: null,
  selectedMultipleChats: [],
  selectedMultipleMessages: [],
  loggedUserId:0,
  entities: {
    // chats: {
    //     byId: {
    //         1: {
    //             activeChunk: null,
    //             entities: {
    //                 messages: {
    //                     byId: {
    //                     },
    //                     ids: []
    //                 },
    //                 chunks: {
    //                     byId: {
    //                     },
    //                     ids: []
    //                 },
    //                 users: {
    //                     byId: {
    //                     },
    //                     ids: []
    //                 }
    //             }
    // //         }
    //     },
    //     ids: []
    // }
  },
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  let temp, temp1, temp2, temp3, temp4;

  return produce(state, (draft) => {
    switch (type) {
      case INITIALIZE_IM:
        draft.storeIsInitialized = true;
        draft.activeChat = payload.chats.ids[0];
        draft.entities = payload;
        draft.unread=payload.unread;
        draft.loggedUserId=payload.loggedUserId;
        return draft;

      case SET_SETTINGS:
        draft.settings = { ...draft.settings, ...payload };
        return draft;

      case SELECT_CHAT:
        draft.activeChat = payload;
        if (payload) draft.entities.chats.byId[payload].unreadMessages = 0;
        return draft;

      case GET_MESSSAGES:
        //intended chat
        temp = draft.entities.chats.byId[payload.chatId];
        temp.loaded = true;
        //add messages and update old messages if duplicated
        temp.entities.messages.byId = {
          ...temp.entities.messages.byId,
          ...payload.messages.byId,
        };

        temp.entities.messages.ids = [
          ...new Set([...temp.entities.messages.ids, ...payload.messages.ids]),
        ].sort((a, b) => a - b);

        //intended chunks collection
        temp1 = temp.entities.chunks;
        if (payload.direction) {
          //load previous
          //chunk that can be merged as end to start with the paylaod chunk-payload

          temp2 = temp1.ids.find(
            (c_id) =>
              c_id !== temp.activeChunk &&
              payload.start >= temp1.byId[c_id].start &&
              payload.start <= temp1.byId[c_id].end
          );
          if (temp2) {
            temp1.byId[temp.activeChunk] = {
              start: temp1.byId[temp2].start,
              end: temp1.byId[temp.activeChunk].end,
              prevPage: temp1.byId[temp.activeChunk].prevPage,
              nextPage: temp1.byId[temp2].nextPage,
            };
            return draft;
          }

          temp1.byId[temp.activeChunk] = {
            start: payload.start,
            end: temp1.byId[temp.activeChunk].end,
            prevPage: temp1.byId[temp.activeChunk].prevPage,
            nextPage: payload.nextPage,
          };
          delete temp1.byId[temp2];
          temp1.ids = temp1.ids.filter((i) => i != temp2);
          return draft;
        } else {
          //load next

          temp2 = temp1.ids.find(
            (c_id) =>
              c_id !== temp.activeChunk &&
              payload.end >= temp1.byId[c_id].start &&
              payload.end <= temp1.byId[c_id].end
          );
          if (temp2) {
            temp1.byId[temp.activeChunk] = {
              start: temp1.byId[temp.activeChunk].start,
              end: temp1.byId[temp2].end,
              prevPage: temp1.byId[temp2].prevPage,
              nextPage: temp1.byId[temp.activeChunk].nextPage,
            };
            delete temp1.byId[temp2];
            temp1.ids = temp1.ids.filter((i) => i != temp2);
            return draft;
          }
          temp1.byId[temp.activeChunk] = {
            start: temp1.byId[temp.activeChunk].start,
            end: payload.end,
            prevPage: payload.prevPage,
            nextPage: temp1.byId[temp.activeChunk].nextPage,
          };
          return draft;
        }

      case PUSH_MESSAGE:
        Helper.cl(payload,"pm1 payload");
        // if (!draft.entities.chats.ids.some(id => id === payload.chatId) && payload.chatId) {
        //     draft.unknownGroupMessages.push(payload)
        //     return draft;
        // }
        if (!draft.entities.chats.byId[payload.chatId]) 
            return draft;

        draft.lastMessageChatId = payload.chatId;
        Helper.cl("pm1")
        //rise on top
        draft.entities.chats.ids.splice(
          draft.entities.chats.ids.indexOf(payload.chatId),
          1
        );
        Helper.cl("pm1")
      
        draft.entities.chats.ids.unshift(payload.chatId);

        //push to messages entity
        temp1 = draft.entities.chats.byId[payload.chatId];
       
       
        //find latest chunk and set it to active
        temp1.activeChunk = temp1.entities.chunks.ids[0];
        Helper.cl("pm1")



        if(payload.message.user_id==draft.loggedUserId && payload.message.status == MessageStatus.SUCCESS) {
        
           //real id also means recieved by pusher
           if (payload.message.status === MessageStatus.SUCCESS) {
            temp1.entities.chunks.byId[temp1.entities.chunks.ids[0]].end =
              payload.message.id;
           
          }
        }
        else{   
        Helper.cl(draft.loggedUserId," pm1 loggedUserId");
        Helper.cl(payload.message.user_id," pm1 payload.message.user_id");
        
        const s=temp1.entities?.messages?.ids?.filter(i=>i==payload?.message?.id)
        Helper.cl(s," pm1 s");
     
         if(!s||s.length==0)
        {
          temp1.entities.messages.ids.push(payload.message.id);
        temp1.entities.messages.byId[payload.message.id] = payload.message;

           //real id also means recieved by pusher
           if (payload.message.status === MessageStatus.SUCCESS) {
            temp1.entities.chunks.byId[temp1.entities.chunks.ids[0]].end =
              payload.message.id;
            payload.chatId !== draft.activeChat && temp1.unreadMessages++;
          }
        }
         }
        
      

      
     
        Helper.cl("pm1")
      
        return draft;

      case CHANGE_MESSAGE_STATUS:
        temp2 = draft.entities.chats.byId[payload.chatId];

        temp1 = temp2.entities.messages;

        if (payload.status !== MessageStatus.SUCCESS) {
          temp1.byId[payload.id].status = payload.status;
          return draft;
        }

        temp1.ids.splice(temp1.ids.indexOf(payload.id), 1, payload.newId);
        temp1.byId[payload.newId] = temp1.byId[payload.id];
        temp1.byId[payload.newId].status = payload.status;
        temp1.byId[payload.newId].attachments = payload.attachments;
        temp1.byId[payload.newId].id = payload.newId;
        temp2.entities.chunks.byId[temp2.activeChunk].end = payload.newId;

        delete temp1.byId[payload.id];
        return draft;

      case PUSH_CONVERSATION:
        // return draft;
        Helper.cl("PUSH_CONVERSATION")
        temp1 = draft.entities.chats.ids.findIndex((i) => i == payload.id);

        if (temp1 >= 0) {
          draft.entities.chats.ids.splice(temp1, 1);
          temp1 = draft.entities.chats.byId[payload.id];
          temp1.entities.messages.byId[payload.entities.messages.ids[0]] =
            payload.entities.messages.byId[payload.entities.messages.ids[0]];
          temp1.entities.messages.ids.push(payload.entities.messages.ids[0]);
          temp1.entities.chunks.byId[temp1.activeChunk].end =
            payload.entities.messages.ids[0];
        } else {
          draft.entities.chats.byId[payload.id] = payload;
        }
        Helper.cl("PUSH_CONVERSATION")
        draft.entities.chats.ids.unshift(payload.id);

        //when we get a new chat event no need to set active
        if (!payload.shouldNotBeActive) {
          draft.activeChat = payload.id;
        } else {
          draft.entities.chats.byId[payload.id].unreadMessages = 0;
        }

        return draft;
      case SET_SEARCHED_MESSAGE:
        //intended chat
        temp1 = draft.entities.chats.byId[payload.chatId ?? draft.activeChat];

        draft.searchedMessage = payload.msgId;

        temp1.activeChunk = payload.chunkId;

        payload.chatId && (draft.activeChat = payload.chatId);

        return draft;

      case SET_CHUNK:
        //set activeChat if provided
        payload.chatId && (draft.activeChat = payload.chatId);

        draft.searchedMessage = payload.msgId;

        //merge flag
        temp4 = false;

        //intended chat
        temp1 = draft.entities.chats.byId[payload.chatId ?? draft.activeChat];

        //chunk can be merged with from start

        temp2 = temp1.entities.chunks.ids.find(
          (t) =>
            temp1.entities.chunks.byId[t].start >= payload.start &&
            temp1.entities.chunks.byId[t].start <= payload.end
        );

        if (temp2) {
          //merge from start
          temp4 = true;
          temp1.entities.chunks.byId[temp2].start = payload.start;
          temp1.entities.chunks.byId[temp2].nextPage = payload.nextPage;
        }

        //can be merged from end
        temp3 = temp1.entities.chunks.ids.find(
          (t) =>
            temp1.entities.chunks.byId[t].end >= payload.start &&
            temp1.entities.chunks.byId[t].end <= payload.end
        );

        if (temp3) {
          temp4 = true;
          temp1.entities.chunks.byId[temp3].end = payload.end;
          temp1.entities.chunks.byId[temp3].start = temp2
            ? temp1.entities.chunks.byId[temp2].start
            : temp1.entities.chunks.byId[temp3].start;
          temp1.entities.chunks.byId[temp3].nextPage = temp2
            ? temp1.entities.chunks.byId[temp2].nextPage
            : temp1.entities.chunks.byId[temp3].nextPage;
          temp1.entities.chunks.byId[temp3].prevPage = payload.prevPage;
          if (temp2) {
            delete temp1.entities.chunks.byId[temp2];
            temp1.entities.chunks.ids = temp1.entities.chunks.ids.filter(
              (i) => i != temp2
            );
          }
        }

        //find chunk proper start index to splice

        temp2 = temp1.entities.messages.ids.findIndex(
          (i) => i > payload.chunk[0].id
        );

        //nothing greater
        if (temp2 === -1) {
          temp1.entities.messages.ids = temp1.entities.messages.ids.concat(
            payload.chunk.map((i) => i.id)
          );
          // return draft;
        }

        //all elements are greater
        else if (temp2 === 0) {
          temp1.entities.messages.ids = payload.chunk
            .map((i) => i.id)
            .concat(temp1.entities.messages.ids);
          // return draft;
        } else {
          //specific index
          temp1.entities.messages.ids.splice(
            temp2 - 1,
            0,
            ...payload.chunk.map((i) => i.id)
          );
        }

        temp1.entities.messages.byId = {
          ...temp1.entities.messages.byId,
          ...payload.chunk.reduce(
            (prev, curr) => ({
              ...prev,
              [curr.id]: curr,
            }),
            {}
          ),
        };

        //chunk is merged with other one so no need to add new chunk
        if (temp4) return draft;

        //add chunk and set active
        temp1.entities.chunks.ids.push(payload.chunkId);
        temp1.entities.chunks.byId[payload.chunkId] = {
          start: payload.start,
          end: payload.end,
          prevPage: payload.prev,
          nextPage: payload.next,
        };
        temp1.activeChunk = payload.chunkId;

        return draft;

      case TAG_MESSAGE:
        //intended message tags
        temp =
          draft.entities.chats.byId[payload.chatId].entities.messages.byId[
            payload.messageId
          ].tags;

        if (temp.some((t) => t === payload.tag)) {
          draft.entities.chats.byId[payload.chatId].entities.messages.byId[
            payload.messageId
          ].tags = temp.filter((t) => t !== payload.tag);
          return draft;
        }

        temp.push(payload.tag);

        return draft;

      case UPDATE_GROUP:
        //                dispatch({ type: UPDATE_GROUP, payload: {chatId: chatId,groupName:groupName, image:image } })

        draft.entities.chats.byId[payload.chatId].name = payload.groupName;
        draft.entities.chats.byId[payload.chatId].image = payload.image;
        draft.entities.chats.byId[payload.chatId].description = payload.description;
        return draft;

      case DELETE_MESSAGE:
        //messages collection
        draft.entities.chats.byId[payload.chatId].entities.messages.byId[
          payload.messageId
        ].status = MessageStatus.DELETED;

        return draft;
      case SET_FORWARDED_MESSAGE:
        draft.forwardedMessage = payload;
        return draft;
      case UNSEND_MESSAGE:
        if (
          !draft.entities.chats.byId[payload.chatId]?.entities.messages.byId[
            payload.msgId
          ]
        ) {
          return draft;
        }

        draft.entities.chats.byId[payload.chatId].entities.messages.byId[
          payload.msgId
        ].tags.push(MessageTags.Unsend);
        draft.entities.chats.byId[payload.chatId].entities.messages.byId[
          payload.msgId
        ].message = "";
        draft.entities.chats.byId[payload.chatId].entities.messages.byId[
          payload.msgId
        ].attachments = [];

        return draft;
      case SET_REPLIED_MESSAGE:
        draft.repliedMessage = payload;
        return draft;
      case DELETE_CHAT:
        draft.entities.chats.ids.splice(
          draft.entities.chats.ids.indexOf(payload),
          1
        );
        delete draft.entities.chats.byId[payload];

        if (draft.activeChat === payload)
          draft.activeChat = draft.entities.chats.ids[0];

        return draft;
      case TAG_CHAT:
        temp = draft.entities.chats.byId[payload.chatId].tags;

        if (temp?.some((t) => t === payload.tag)) {
          draft.entities.chats.byId[payload.chatId].tags = temp.filter(
            (t) => t !== payload.tag
          );
          return draft;
        }

        temp.push(payload.tag);
        return draft;
      case SELECT_GROUP:
        draft.selectedGroup = payload;
        return draft;

      case SELECT_MULTIPLE_CHATS:
        if (payload) {
          if (draft.selectedMultipleChats.some((ch) => ch === payload)) {
            draft.selectedMultipleChats = draft.selectedMultipleChats.filter(
              (id) => id !== payload
            );
            return draft;
          }
          draft.selectedMultipleChats.push(payload);
          return draft;
        }
        draft.selectedMultipleChats = [];
        return draft;
      case SELECT_MULTIPLE_MESSAGES:
        if (payload) {
          if (draft.selectedMultipleMessages.some((ch) => ch === payload)) {
            draft.selectedMultipleMessages =
              draft.selectedMultipleMessages.filter((id) => id !== payload);
            return draft;
          }
          draft.selectedMultipleMessages.push(payload);
          return draft;
        }
        draft.selectedMultipleMessages = [];
        return draft;
      case CHAT_SEEN:
        if (
          !draft.entities.chats.byId[payload.chatId]?.entities.users.byId[
            payload.userId
          ]
        )
          return draft;

        draft.entities.chats.byId[payload.chatId].entities.users.byId[
          payload.userId
        ].pointer = payload.pointer;
        return draft;
      case UPDATE_IMAGE_THUMBNAIL:
        //intended message
        temp =
          draft?.entities?.chats?.byId?.[payload.chatId]?.entities?.messages
            ?.byId?.[payload.msgId];
        if (!temp) return draft;
        //image index
        temp2 = temp.attachments?.findIndex?.(
          (atc) => atc.id === payload.resourceId
        );
        if (temp2 < 0) return draft;

        temp3 = temp.attachments.splice(temp2, 1);

        temp.attachments.push({ ...temp3[0], thumbUrl: payload.newThumNail });

        return draft;
      default:
        return draft;
    }
  });
}
