import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { dangerColor } from "config/constants";
import { Col } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

const RPermissions = ({ permission, handleRemove, redirect, handleRedirect, can_be_edit }) => {
	const [isHovered, setIsHovered] = useState(false);

	return (
		<Col xs={12} sm={6} lg={4} key={permission.id} onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
			<RFlex styleProps={{ alignItems: "center", cursor: "pointer" }}>
				<li style={{ textAlign: "left" }} onClick={() => redirect && handleRedirect(permission.id)}>
					{permission.title}
				</li>
				{isHovered && (permission.direct_with_user || can_be_edit) && (
					<i onClick={() => handleRemove(permission.id)} className={iconsFa6.delete} style={{ color: dangerColor }} />
				)}
			</RFlex>
		</Col>
	);
};

export default RPermissions;
