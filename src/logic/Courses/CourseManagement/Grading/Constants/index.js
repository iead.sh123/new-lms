export const tablesTypes = {
    gradingGroups: "gradingGroups",
    gradableItems: "gradableItems",
    finalMarks: "finalMarks",
    itemSlider: "itemSlider",
    studentSlider: "studentSlider",
    Alternatives: 'alternatives',
    AlternativesIdsLoading: 'alternativesIdsLoading',
    FilteredAlternatives: 'filteredAlternatives',
    AlternativeSlider: "alternativesSlider"
}

export const gradingGroupStructure = {
    "Group Name": { touched: false, saved: false, value: '', type: 'text', notFocused: false },
    "Weight": { touched: false, saved: false, value: '', type: 'number', notFocused: true },
    "Extra Points": { touched: false, saved: false, value: '', type: 'number', notFocused: true },
    "Items": { touched: false, saved: false, value: '0', type: 'number', notFocused: true }
}
export const gradableItemsStructure = {
    "Group": { touched: false, saved: false, value: { label: 'NoGroup', value: null }, type: 'select', notFocused: true },
    "Type": { touched: false, saved: false, value: 'custom', type: 'text', notFocused: true },
    "Name": { touched: false, saved: false, value: '', type: 'text', notFocused: false },
    "Points": { touched: false, saved: false, value: '', type: 'number', notFocused: true },
    "Weight": { touched: false, saved: false, value: '', type: 'number', notFocused: true },
    "% Of Course": { touched: false, saved: false, value: '0', type: 'text', notFocused: true },
    "Alternatives": { touched: false, saved: false, value: '0', type: 'text', notFocused: true },
    "Graded Students": { touched: false, saved: false, value: { numberOFStudents: 0, numberOFGradedStudents: 0 }, type: 'text', notFocused: true },
}
export const finalMarksTable = [
    { label: "Students", value: '', type: "text" },
    { label: "Final Mark", value: "calculatedMark", type: "number" },
    { label: "Teacher Mark", value: "teacherMark", type: "number" },
    { label: "GPA", value: "GPA", type: "number" },
    { label: "Letter Mark", value: "latterGrade", type: "text" },
]
export const itemSliderTable = [
    { label: "Students", value: '', type: 'text' },
    { label: "Mark", value: 'grade', type: 'number' },
    { label: "Final Mark", value: 'calculatedFinalGrade', type: 'number' },
]
export const editableFields = ["Group Name", "Weight", "Extra Points", "Group", "Name", "Points", "Teacher Mark"]

export const itemDetails = [
    { label: null, value: "name" },
    { label: "Mark", value: "mark" },
    { label: null, value: "points" },
    { label: "Submiting Date", value: "11/2/2024" },
    { label: "Letter Mark", value: "A+" },
    { label: "Teacher Mark", value: "teacherGrade" },
]
export const termTableData = [
    { label: "Final Mark", value: "finalMark" },
    // {label:"GPA",value:"GPA"},
    { label: "Letter Mark", value: "letterMark" },
    { label: "Pass\\Fail", value: "pass" },
]
export const alternativesTableData = [
    { label: "Type", value: 'type' },
    { label: "Name", value: "name" },
    { label: "Points", value: "points" },
    { label: "% Of Course", value: "percentage" },
    { label: "Assigned To", value: "numberOfGradedStudents" },
    { label: 'parentAlternativeId', value: "parentAlternativeId", hidden: true },
    { label: 'parentAlternativeType', value: "parentAlternativeType", hidden: true },
]