import React from "react";
import CollaborationSettingCard from "./CollaborationSettingCard";
import RTitle from "components/RComponents/RTitle";
import styles from "../Collaboration.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RContents = ({ data, handlers }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RTitle text={tr`collaborated_content`} count={data?.content?.length} />
			<RFlex className={styles.container__rectangle__card} styleProps={{ flexWrap: "wrap" }}>
				{data?.content?.map((item) => (
					<CollaborationSettingCard key={item.id} item={item} handlers={handlers} data={data} />
				))}
			</RFlex>
		</RFlex>
	);
};

export default RContents;
