import React from "react";
import { INITITALIZE, DOWN, LEFT, RIGHT, UP, GO } from "./layoutNavigation.actions";
import { Button, Container, Row, Col } from "reactstrap";
import Compass from "./Compass";

import reducer, { initialState } from "./layoutNavigation.reducer";

import styles from "./layoutNavigation.Module.css";
import Hammer from "react-hammerjs";
import { Switch, Route, useHistory, useLocation } from "react-router-dom";
import withDirection from "hocs/withDirection";

/*
map:[
   {
    x: i,
    y: i % 4,
    url: prop.layout + rt.path,
    component: rt.component,
    key: key
  }
]
*/

const RLayoutNavigation = ({ mapping, routes, activeNode = { x: 0, y: 0 }, withCompass = true, dir }) => {
	//refs and vars
	const [navigationData, dispatch] = React.useReducer(reducer, initialState);
	const mounted = React.useRef(false);

	//state manipulation functions
	const handleNavigate = (direction) => dispatch({ type: direction, payload: null });
	const handleGo = (x, y) => dispatch({ type: GO, payload: { x, y } });
	const handleInitialize = (mapping, activeNode) => dispatch({ type: INITITALIZE, payload: { mapping, activeNode } });

	const history = useHistory();
	const _location = useLocation();

	// effects

	React.useEffect(() => {
		mounted.current = true;

		return () => {
			mounted.current = false;
		};
	}, []);

	React.useEffect(() => {
		let splitted = _location.pathname.split("/");
		const a = ["teacher", "senior-teacher", "parent", "student", "admin"];
		let index = splitted.findIndex((sp) => a.indexOf(sp) >= 0);

		splitted = splitted[splitted.length - 1] === "" ? splitted.slice(index, splitted.length - 1) : splitted.slice(index); //splitted[splitted.length-1] === "" ? splitted.length-1 : null)
		const link = "/" + splitted.join("/");
		const linkRegex = new RegExp(`${link}\/?`, "gi");
		const nd = mapping.find((mp) => linkRegex.test(mp.url));

		handleInitialize(
			mapping.map((item) => ({
				...item,
				url: item.url,
			})),
			nd ? nd : null
		);
	}, [_location]);

	React.useEffect(() => {
		navigationData.currentNode && history.push(navigationData.currentNode.url);
	}, [navigationData.nav]);

	return (
		<Hammer
			direction="DIRECTION_ALL"
			onSwipeLeft={() => handleNavigate(RIGHT)}
			onSwipeRight={() => handleNavigate(LEFT)}
			onSwipeDown={() => handleNavigate(UP)}
			onSwipeUp={() => handleNavigate(DOWN)}
		>
			<div dir={dir ? "ltr" : "rtl"} className={styles["container"]}>
				<Switch>
					{[
						...mapping.map((mp, i) => {
							return <Route path={mp.url} component={mp.com} key={"ln" + i} />;
						}),
						...routes.map((rt, i) => <Route path={rt.layout + rt.path} component={rt.component} key={"rt" + i} />),
					]}
				</Switch>
				{navigationData.mapping.findIndex((mp) => mp.x === navigationData.currentNode?.x + 1 && mp.y === navigationData.currentNode?.y) >=
					0 && (
					<div className={`${styles["link-right"]} ${styles["animation_arrow_link"]}`} onClick={() => handleNavigate(RIGHT)}>
						<img src={""} className={styles["animation_arrow"]} />
					</div>
				)}
				{navigationData.currentNode?.x > 0 && (
					<div onClick={() => handleNavigate(LEFT)} className={`${styles["link-left"]} ${styles["animation_arrow_link"]}`}>
						<img src={""} className={styles["animation_arrow"]} />
					</div>
				)}
				{navigationData.currentNode?.y > 0 && (
					<div
						className={`${styles["link-up"]} ${styles["animation_arrow_link"]}`}
						// style={{marginBottom: 10}}
						onClick={() => handleNavigate(UP)}
					>
						<img src={""} className={styles["animation_arrow"]} />
					</div>
				)}
				{navigationData.mapping.findIndex((mp) => mp.x === navigationData.currentNode?.x && mp.y === navigationData.currentNode?.y + 1) >=
					0 && (
					<div className={`${styles["link-bottom"]} ${styles["animation_arrow_link"]}`} onClick={() => handleNavigate(DOWN)}>
						<img src={""} className={styles["animation_arrow"]} />
					</div>
				)}

				{withCompass && (
					<>
						<Compass
							selected={navigationData.currentNode?.url}
							points={navigationData.mapping.map((nd) => ({
								id: nd.url,
								x: nd.x,
								y: nd.y,
								icon: nd.icon,
								onClick: () => handleGo(nd.x, nd.y),
							}))}
						/>
					</>
				)}
			</div>
		</Hammer>
	);
};

export default withDirection(RLayoutNavigation);
