import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";
import RTitle from "components/Global/RComs/themed/RTitle/RTitle";
import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import GGreetingSearch from "./GGreetingSearch";
import GLessonLister from "logic/Courses/CourseManagement/Lessons/SectionLessonLister/GLessonLister";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import GTodayLessons from "./GTodayLessons";
import RLessonModal from "./RLessonModal";
import GStudentsSubmitedTasks from "./GStudentsSubmitedTasks";
import GTimeline from "./GTimeline";
import { Services } from "engine/services";

const GRecentLessons = ({ lessons }) => {
	return <>null null null </>;

	return (
		<div>
			recent lessons
			<RLessonModal lesson={lesson} publish={publishLesson} open={lessonModalOpen} setOpen={setLessonModalOpen} />
			{/* {Helper.jstree(lessons)} */}
		</div>
	);
};

export default GRecentLessons;
