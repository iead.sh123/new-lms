import LoaderD from "components/Global/Loader";
import LoaderEllipsis from "components/Global/LoaderEllipsis";
import LoaderEvents from "components/Global/LoaderEvents";
import LoaderList from "components/Global/LoaderList";
import LoaderSocialMedia from "components/Global/LoaderSocialMedia";
import RLoader from "components/Global/RComs/RLoader";


//inject type
/**
 * @param { ELLIPSIS|LIST|EVENTS|SOCIALMEDIA } type .
 */

const Loader = (type) => {
  // switch (type) {
  //   case ELLIPSIS:
  //     return <LoaderEllipsis />;
  //   case LIST:
  //     return <LoaderList />;
  //   case EVENTS:
  //     return <LoaderEvents />;
  //   case SOCIALMEDIA:
  //     return <LoaderSocialMedia />;
  //   default:
  //     return <LoaderD />;
  // }
  return <RLoader></RLoader>
};

export const ELLIPSIS = "ELLIPSIS";
export const LIST = "LIST";
export const EVENTS = "EVENTS";
export const SOCIALMEDIA = "SOCIALMEDIA";
export default Loader;
