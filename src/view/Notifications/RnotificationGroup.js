import React from "react";
import { useState } from "react";
import "./Rnotification.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilRuler, faCheck } from "@fortawesome/free-solid-svg-icons";
import { RLabel } from "components/Global/RComs/RLabel";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { Services } from "engine/services";
import { useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import RBill from "components/Global/RComs/RBill";
import Switch from "react-bootstrap-switch";
import defaultImage from "assets/img/default-avatar.png";

const RGroup = (props) => {
	const history = useHistory();
	const ng = props.notification;
	const onClick = () => {
		if (props.handleOnClick) {
			props.handleOnClick(ng.id);
		}
	};
	return (
		<div>
			<div className="notification_faild" style={{ background: ng.seen ? "white" : "#EEF2FB" }}>
				<div className="notification_faild_image" onClick={onClick}>
					<div className="notification_faild_image_container">
						{/* {ng?.image} */}
						<img src={ng?.image ? Services.notficiation.file + ng?.image : defaultImage} />
						<div className="notification_faild_image_icon" style={{ backgroundColor: ng.iconbg }}>
							{ng.imageIcon}
						</div>
					</div>
				</div>
				<div className="notification_faild_text">
					<div className="notification_faild_row" onClick={onClick}>
						<span className="notification_faild_title">
							{" "}
							{ng.title + (ng.otherNots ? " and " + ng?.otherNots + " other notifications" : "")}
						</span>
						{/* +" "+(ng.seen?("seen"+ng.id):"unseen")} */}
						<span className="notification_faild_time"> {ng.time}</span>
					</div>
					<div className="notification_faild_row">
						<div className="notification_faild_description" onClick={onClick}>
							<RLabel
								tooltiped={false}
								value={ng.description + (ng.otherNots ? " and " + ng?.otherNots + " other notifications" : "")}
								lettersToShow={10}
							></RLabel>
						</div>
						<div className="notification_faild_seen">
							{ng.seen ? (
								""
							) : (
								<button onClick={() => props.handleOnSeenClick()}>
									<FontAwesomeIcon icon={faCheck} />
								</button>
							)}
						</div>
					</div>
				</div>
			</div>

			<div style={{ paddingLeft: "30px" }}>
				{props.widthDetails ? (
					ng.notifications.map((n) => {
						const n1 = {
							image: n?.user?.image,
							imageIcon: <FontAwesomeIcon icon={faPencilRuler} />,
							iconbg: "#007bff",
							title: n?.title,
							description: n?.description,
							time: "10 min ago",
							seen: ng.seen,
						};

						return <RNotification notification={n1}></RNotification>;
					})
				) : (
					<></>
				)}
			</div>
		</div>
	);
};

const RNotification = (props) => {
	const ng = props.notification;
	return (
		<div>
			<div className="notification_faild">
				<div className="notification_faild_image">
					<div className="notification_faild_image_container">
						<img src={ng?.image ? Services.notficiation.file + ng?.image : defaultImage} />
						<div className="notification_faild_image_icon" style={{ backgroundColor: ng.iconbg }}>
							{ng.imageIcon}
						</div>
					</div>
				</div>
				<div className="notification_faild_text">
					<div className="notification_faild_row">
						<span className="notification_faild_title"> {ng.title}</span>
						<span className="notification_faild_time"> {ng.time}</span>
					</div>
					<div className="notification_faild_row">
						<div className="notification_faild_description">
							<RLabel tooltiped={false} value={ng.description} lettersToShow={10}></RLabel>
						</div>
						<div className="notification_faild_seen">{ng.seen ? <FontAwesomeIcon icon={faPencilRuler} /> : ""}</div>
					</div>
				</div>
			</div>

			<div style={{ paddingLeft: "30px" }}></div>
		</div>
	);
};

//---------------------------------------------------------------------
const RNotificationGroup = (props) => {
	const history = useHistory();
	const Group = props.group;
	const [boxClass, setBoxClass] = useState("hide_notifications_box");
	return (
		<div
			className="notification_container"
			style={{
				backgroundColor: props.backgroundColor ? props.backgroundColor : "#fff",
			}}
		>
			<div className="notification_header">
				<div className="notification_box_head" /*  onClick={toggleNotificationBox} */>
					<div className="head_title">
						<div style={{ display: "block-inline", width: "fit-content" }}>Notifications </div>
						<div>
							{" "}
							<div style={{ display: "block-inline", width: "fit-content" }}>
								{props.allNotifications ? <RBill count={props.allNotifications} color={"F3F3F3"} /> : <></>}
							</div>
						</div>
					</div>
					<div className=" head_toggle_label">
						<span> Stop Notifications </span>
					</div>

					<Switch
						onColor={"green"}
						offColor={"gray"}
						onText="On"
						offText="Off"
						value={!props.notifyMe}
						onChange={(e) => {
							props.onStopNotificationChange(e.state.value);
						}}
					/>
					<Link to={`${baseURL}/${genericPath}/notifications`} className="tag" style={{ color: "blue", cursor: "pointer" }}>
						<div className="head_text">
							<span>See all</span>
						</div>
					</Link>
				</div>
			</div>
			<div className="notification_body">
				{Group?.notifications?.map((ng, ngi) => (
					<>
						{" "}
						<RGroup
							key={ngi}
							notification={ng}
							widthDetails={props.withDetails}
							handleOnSeenClick={() => props.handleOnSeenClick(ng.id)}
							handleOnClick={props.handleOnClick}
						></RGroup>
					</>
				))}
			</div>
			{/*     <div  className={boxClass}>
           {Group.notifications.map(ng=>(
                  <div  className="notification_faild">
                     <div  className="faild_title">
                        {ng.title}
                     </div>
                     <div  className="faild_description">
                        {ng.description}
                    </div>
                   </div>
               )) }
       </div> */}
		</div>
	);
};

export default RNotificationGroup;
