import React from "react";
import './RSharedContentCard.css';
import RTagViewer from "./RTagViewer";
import { Services } from "engine/services";
import RLiveAction from "./RLiveAction";
import Helper from "../Helper";
import placeholder from "assets/img/placeholder.jpg";
const RSharedContentCard=(props)=>{
  // Helper.cl(props,"RCollaborationCard props");
   // return <div> dfgs dfg sdfg sdf g</div>
  const {title,icon="fa fa-folder",Actions,tags,selected,Author,onClick1,multiSelect}=props;
  
 // Actions: null
 // Author: {image: 'tea.png', text: 'Canvas'}
 // Description: null
 // UpLeftIcon: "fa fa-angle-right"
 // UpLeftText: null
 // additionalComponent: null
 // bottomBorderColor: null
 // onClick: ƒ onClick()
 // subtitle: {text: 'Unit Plan'}
 // title: {text: 'Arabic Letters'}
 const imgsrc =(path)=>{
 const b=Services.storage.file;
 if(path.includes(b))
 {
   return path;
 }
 else return b+path;
 };

return(<div onClick={()=>{if(multiSelect) onClick1(selected)}} style={(multiSelect&&selected)?{background:"#eeeeff"}:{}} className="shared_content_card">


  
    <div 
      id="col_checkbox" 
      style={{position:"relative",float:"right",width:"25px",height:"25px",top:"-25px",right:"-173px"}}
    >
  {(multiSelect&&selected)?  <span className="fa fa-check-square check_icon" style={{width:"25px",height:"25px"}}></span >:null  }
    </div>
  
  {/* {selected?"selected":"not selected"} */}

  {/* {Helper.js(props)} */}
      <div className="shared_icon_title">
       {
        icon&&<div className="shared_icon"><i className={icon}></i></div>
       }
        <div className="shared_title_tags">
          {
            title&& <div className="shared_title">{title.text}</div>
         }
        {tags&&tags.length>0? 
        <div className="shared_content_tags" >
        
        {
        tags.map(i=> 
          <RTagViewer 
          name={i.name}
          iconColor={i.iconColor}
          backgroundColor={i.backgroundColor}
        />
          )
      }
        </div>:<></>}
        </div>
      </div>
      
      {
             Author&&
              <div className="shared_content_Author">
                <div className="shared_Author_img"><img 
               src={Author.image?imgsrc(Author.image):placeholder}
                
                ></img> </div> 
                {Author.title&& <div className="shared_Author_text"><span>{Author.title}</span> </div> }
             </div>
            }
                    {Actions && (
          <div className="card_actions">
           
            {Actions.map((action) => (
              // {if(action.type=="checkbox")
              //  return <AppCheckbox
              //     onChange={action.onClick }
              //     label={action.title}
              //     checked={action.value}
              //     />
              //      else return <button className="action_btn" onClick={action.onClick} style={{color:action.color,border:action.withBorder==true?`1px solid ${action.color}`:'none'}}>
              //       {action.icon&&<i className={action.icon}/>}
              //       {action.title&&<span> {action.title} </span>}
              //   </button>}
              <div>
                 {/* {Helper.js(action,"action")} */}
              <RLiveAction
                title={action.title}
                icon={action.icon}
                color={action.color}
                text={action.title}
                value={action.value}
                loading={action.loading}
                type={action.type}
                onClick={action.onClick}
                disabled={action.disabled}
                disabledNote={action.disabledNote}
              />
              </div>
            ))}
          </div>
        )}

    </div>
)
}
export default RSharedContentCard;