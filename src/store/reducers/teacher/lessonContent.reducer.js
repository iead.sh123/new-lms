import {
  ADD_LESSON_LINK_REQUEST,
  ADD_LESSON_LINK_SUCCESS,
  ADD_LESSON_LINK_ERROR,
  REMOVE_LESSON_LINK_REQUEST,
  REMOVE_LESSON_LINK_SUCCESS,
  REMOVE_LESSON_LINK_ERROR,
  SHOW_LESSON_LINK_REQUEST,
  SHOW_LESSON_LINK_SUCCESS,
  SHOW_LESSON_LINK_ERROR,
  ADD_LESSON_TEXT_REQUEST,
  ADD_LESSON_TEXT_SUCCESS,
  ADD_LESSON_TEXT_ERROR,
  REMOVE_LESSON_TEXT_REQUEST,
  REMOVE_LESSON_TEXT_SUCCESS,
  REMOVE_LESSON_TEXT_ERROR,
  SHOW_LESSON_TEXT_REQUEST,
  SHOW_LESSON_TEXT_SUCCESS,
  SHOW_LESSON_TEXT_ERROR,
  ADD_LESSON_FILE_REQUEST,
  ADD_LESSON_FILE_SUCCESS,
  ADD_LESSON_FILE_ERROR,
  REMOVE_LESSON_FILE_REQUEST,
  REMOVE_LESSON_FILE_SUCCESS,
  REMOVE_LESSON_FILE_ERROR,
  ADD_LIVE_SESSION_REQUEST,
  ADD_LIVE_SESSION_SUCCESS,
  ADD_LIVE_SESSION_ERROR,
  REFRESH_LISTER,
  REFRESH_LISTER_TEXT,
  REFRESH_LISTER_FILE,
  SET_LESSON_LINK_ID,
  SET_LESSON_TEXT_ID,
  EDIT_LESSON_ATTACHMENT_REQUEST,
  EDIT_LESSON_ATTACHMENT_SUCCESS,
  EDIT_LESSON_ATTACHMENT_ERROR,
  EDIT_LESSON_LINK_REQUEST,
  EDIT_LESSON_LINK_SUCCESS,
  EDIT_LESSON_LINK_ERROR,
  EDIT_LESSON_TEXT_REQUEST,
  EDIT_LESSON_TEXT_SUCCESS,
  EDIT_LESSON_TEXT_ERROR,
  LIVE_SESSION_BY_ID_REQUEST,
  LIVE_SESSION_BY_ID_SUCCESS,
  LIVE_SESSION_BY_ID_ERROR,
  GET_LESSON_CONTENT_BY_LESSON_ID_REQUEST,
  GET_LESSON_CONTENT_BY_LESSON_ID_SUCCESS,
  GET_LESSON_CONTENT_BY_LESSON_ID_ERROR,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const initState = {
  showLessonLink: {},
  showLessonText: {},
  lessonContentByLessonId: {},
  lessonLinkId: null,
  lessonTextId: null,

  refreshLister: false,
  refreshListerText: false,
  refreshListerFile: false,
  loading: false,
  showLessonLinkLoading: false,
  showLessonTextLoading: false,
  lessonTextLoading: false,
  lessonFileLoading: false,
  lessonLinkLoading: false,
  liveSessionLoading: false,
  reloadLiveSessionLoading: false,
  lessonContentByLessonIdLoading: false,
};

const lessonContentReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_LESSON_CONTENT_BY_LESSON_ID_REQUEST:
        draft.lessonContentByLessonIdLoading = true;
        return draft;
      case GET_LESSON_CONTENT_BY_LESSON_ID_SUCCESS:
        draft.lessonContentByLessonIdLoading = false;
        draft.lessonContentByLessonId = action.payload.data;
        return draft;
      case GET_LESSON_CONTENT_BY_LESSON_ID_ERROR:
        draft.lessonContentByLessonIdLoading = false;
        return draft;

      case ADD_LESSON_TEXT_REQUEST:
        draft.lessonTextLoading = true;
        return draft;
      case ADD_LESSON_TEXT_SUCCESS:
        draft.lessonTextLoading = false;
        return draft;
      case ADD_LESSON_TEXT_ERROR:
        draft.lessonTextLoading = false;
        return draft;

      case ADD_LESSON_FILE_REQUEST:
        draft.lessonFileLoading = true;
        return draft;
      case ADD_LESSON_FILE_SUCCESS:
        draft.lessonFileLoading = false;
        return draft;
      case ADD_LESSON_FILE_ERROR:
        draft.lessonFileLoading = false;
        return draft;

      case ADD_LIVE_SESSION_REQUEST:
        draft.liveSessionLoading = true;
        return draft;
      case ADD_LIVE_SESSION_SUCCESS:
        draft.liveSessionLoading = false;
        return draft;
      case ADD_LIVE_SESSION_ERROR:
        draft.liveSessionLoading = false;
        return draft;

      case ADD_LESSON_LINK_REQUEST:
        draft.lessonLinkLoading = true;
        return draft;
      case ADD_LESSON_LINK_SUCCESS:
        draft.lessonLinkLoading = false;
        return draft;
      case ADD_LESSON_LINK_ERROR:
        draft.lessonLinkLoading = false;
        return draft;

      case EDIT_LESSON_ATTACHMENT_REQUEST:
        draft.editLessonAttachmentLoading = true;
        return draft;
      case EDIT_LESSON_ATTACHMENT_SUCCESS:
        draft.editLessonAttachmentLoading = false;
        return draft;
      case EDIT_LESSON_ATTACHMENT_ERROR:
        draft.editLessonAttachmentLoading = false;
        return draft;

      case EDIT_LESSON_LINK_REQUEST:
        draft.lessonLinkLoading = true;
        return draft;
      case EDIT_LESSON_LINK_SUCCESS:
        draft.lessonLinkLoading = false;
        return draft;
      case EDIT_LESSON_LINK_ERROR:
        draft.lessonLinkLoading = false;
        return draft;

      case EDIT_LESSON_TEXT_REQUEST:
        draft.lessonTextLoading = true;
        return draft;
      case EDIT_LESSON_TEXT_SUCCESS:
        draft.lessonTextLoading = false;
        return draft;
      case EDIT_LESSON_TEXT_ERROR:
        draft.lessonTextLoading = false;
        return draft;

      // ______________________________________________________________________________

      case REMOVE_LESSON_LINK_REQUEST:
        draft.loading = true;
        return draft;
      case REMOVE_LESSON_LINK_SUCCESS:
        draft.loading = false;
        return draft;
      case REMOVE_LESSON_LINK_ERROR:
        draft.loading = false;
        return draft;

      case SHOW_LESSON_LINK_REQUEST:
        draft.showLessonLinkLoading = true;
        return draft;
      case SHOW_LESSON_LINK_SUCCESS:
        draft.showLessonLinkLoading = false;
        draft.showLessonLink = action.payload.showLessonLinkData;
        return draft;
      case SHOW_LESSON_LINK_ERROR:
        draft.showLessonLinkLoading = false;
        return draft;

      case REMOVE_LESSON_TEXT_REQUEST:
        draft.loadingLessonText = true;
        return draft;
      case REMOVE_LESSON_TEXT_SUCCESS:
        draft.loadingLessonText = false;
        return draft;
      case REMOVE_LESSON_TEXT_ERROR:
        draft.loadingLessonText = false;
        return draft;

      case SHOW_LESSON_TEXT_REQUEST:
        draft.showLessonTextLoading = true;
        return draft;
      case SHOW_LESSON_TEXT_SUCCESS:
        draft.showLessonTextLoading = false;
        draft.showLessonText = action.payload.showLessonTextData;
        return draft;
      case SHOW_LESSON_TEXT_ERROR:
        draft.showLessonTextLoading = false;
        return draft;

      case REMOVE_LESSON_FILE_REQUEST:
        draft.loadingLessonFile = true;
        return draft;
      case REMOVE_LESSON_FILE_SUCCESS:
        draft.loadingLessonFile = false;
        return draft;
      case REMOVE_LESSON_FILE_ERROR:
        draft.loadingLessonFile = false;
        return draft;

      case REFRESH_LISTER:
        draft.refreshLister = action.payload.refreshLister;
        return draft;

      case REFRESH_LISTER_TEXT:
        draft.refreshListerText = action.payload.refreshListerText;
        return draft;

      case REFRESH_LISTER_FILE:
        draft.refreshListerFile = action.payload.refreshListerFile;
        return draft;

      case SET_LESSON_LINK_ID:
        draft.lessonLinkId = action.payload.lessonLinkId;
        return draft;

      case SET_LESSON_TEXT_ID:
        draft.lessonTextId = action.payload.lessonTextId;
        return draft;

      case LIVE_SESSION_BY_ID_REQUEST:
        draft.reloadLiveSessionLoading = true;
        return draft;
      case LIVE_SESSION_BY_ID_SUCCESS:
        draft.reloadLiveSessionLoading = false;
        return draft;
      case LIVE_SESSION_BY_ID_ERROR:
        draft.reloadLiveSessionLoading = false;
        return draft;
    }
  });

export default lessonContentReducer;
