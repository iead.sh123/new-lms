import React from "react";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import Loader from "utils/Loader";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFormik } from "formik";
import RAddCourses from "view/Organization/PaymentPolicy/Discount/AddCourses/RAddCourses";
import { paymentApi } from "api/global/payment";
import { useMutateData } from "hocs/useMutateData";

const GAddCourses = ({ handleCloseCourseModal, discountId }) => {
	const initialValues = {
		search: "",
		categoryId: "uncategorized",
		creatorValue: null,
		filterValue: [],
		groupingOfSearchFields: { search: "" },

		showFilter: false,
		switchMode: true,
		maxPrice: null,

		minP: null,
		maxP: null,

		allCourses: [],
		categoryIds: [],
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	const categoriesTreeData = useFetchDataRQ({
		queryKey: ["categoryTree"],
		queryFn: () => coursesManagerApi.getCourseCategoriesTree({ withoutCourses: true, withoutDiscounts: true }),
	});

	const data =
		values?.creatorValue && !values.maxP
			? {
					name: values.search,
					category_id: values.categoryId,
					creator_id: values.creatorValue.value,
					course_types: ["isInstance"],
					isPublished: true,
					noPagination: true,
					withoutDiscount: true,
			  }
			: values.maxP && !values?.creatorValue
			? {
					name: values.search,
					category_id: values.categoryId,
					min_price: values.minP,
					max_price: values.maxP,
					course_types: ["isInstance"],
					isPublished: true,
					noPagination: true,
					withoutDiscount: true,
			  }
			: values.maxP && values.creatorValue
			? {
					name: values.search,
					category_id: values.categoryId,
					creator_id: values.creatorValue.value,
					min_price: values.minP,
					max_price: values.maxP,
					course_types: ["isInstance"],
					isPublished: true,
					noPagination: true,
					withoutDiscount: true,
			  }
			: {
					name: values.search,
					category_id: values.categoryId,
					course_types: ["isInstance"],
					isPublished: true,
					noPagination: true,
					withoutDiscount: true,
			  };

	const coursesData = useFetchDataRQ({
		queryKey: ["courses", values.categoryId, values.groupingOfSearchFields, values.creatorValue, values.minP, values.maxP],
		queryFn: () => coursesManagerApi.getCoursesByCategoryId(data),
	});

	const creatorsData = useFetchDataRQ({
		queryKey: ["creators"],
		queryFn: () => coursesManagerApi.getCreators(),
	});

	const checkGeneralDiscountData = useFetchDataRQ({
		queryKey: ["checkGeneralDiscount"],
		queryFn: () => paymentApi.checkGeneralDiscount({}),
	});

	const applyDiscountToProductsMutate = useMutateData({
		queryFn: (data) => paymentApi.applyDiscountToProducts(data),
		invalidateKeys: ["coursesByDiscountId", discountId],
		onSuccessFn: () => {
			setFieldValue("allCourses", []);
			handleCloseCourseModal();
		},
	});

	const minAndMaxPricesToUsingInFilterData = useFetchDataRQ({
		queryKey: ["minAndMaxPricesToUsingInFilter"],
		queryFn: () => paymentApi.minAndMaxPricesToUsingInFilter(),
		onSuccessFn: ({ data }) => {
			setFieldValue("maxPrice", data?.data?.maxPrice);
		},
	});

	const handleApplyCourses = () => {
		applyDiscountToProductsMutate.mutate({
			discountId,
			data: {
				products: values?.allCourses,
				productType: coursesData?.data?.data?.data?.courses[0]?.productType,
			},
		});
	};

	const handleSearch = (emptyData, emptyFilter) => {
		setFieldValue("groupingOfSearchFields.search", emptyData !== undefined ? "" : values.search);
	};

	const handleApplyFilter = () => {
		setFieldValue("minP", values.filterValue[0]);
		setFieldValue("maxP", values.filterValue[1]);
	};

	const handleResetFilter = () => {
		setFieldValue("filterValue", []);
		setFieldValue("showFilter", false);
		setFieldValue("minP", null);
		setFieldValue("maxP", null);
	};

	const handleShowFilter = () => {
		setFieldValue("showFilter", !values.showFilter);
	};

	const handleSwitchMode = () => {
		setFieldValue("switchMode", !values.switchMode);
	};

	const handleChangeCAtegory = (categoryId) => {
		setFieldValue("categoryId", categoryId);
	};

	const handleSelectAndUnSelectAllCourses = ({ categoryId: categoryId }) => {
		let updatedCategoryArray = [...values.categoryIds];
		let updatedArray = [...values.allCourses];
		let arr = [];

		const categoryIndex = updatedCategoryArray.indexOf(categoryId);
		if (categoryIndex == -1) {
			arr = coursesData?.data?.data?.data?.courses?.map((course) => course?.id);
			updatedArray = [...updatedArray, ...arr];
			updatedCategoryArray.push(categoryId);
		} else {
			updatedArray = updatedArray.filter((courseId) => {
				return !coursesData?.data?.data?.data?.courses.some((course) => course.id === courseId);
			});
			updatedCategoryArray = updatedCategoryArray.filter((cId) => cId !== categoryId);
		}
		setFieldValue("allCourses", updatedArray);
		setFieldValue("categoryIds", updatedCategoryArray);
	};

	const handleSelectCourses = ({ courseId }) => {
		let updatedArray = [...values.allCourses];

		const courseIndex = updatedArray.indexOf(courseId);
		if (courseIndex == -1) {
			updatedArray.push(courseId);
		} else {
			updatedArray = updatedArray.filter((cId) => cId !== courseId);
		}
		setFieldValue("allCourses", updatedArray);
	};

	if (categoriesTreeData.isLoading) return <Loader />;
	return (
		<RAddCourses
			formProperties={{ values: values, setFieldValue: setFieldValue }}
			data={{
				categoriesTree: categoriesTreeData?.data?.data?.data,
				creators: creatorsData?.data?.data?.data,
				courses: coursesData?.data?.data?.data,
				checkIfIHaveGeneralDiscount: checkGeneralDiscountData?.data?.data?.data !== null ? true : false,
				generalDiscountData: checkGeneralDiscountData?.data?.data?.data,
			}}
			loading={{
				coursesLoading: coursesData.isLoading,
				creatorsLoading: creatorsData?.isLoading,
				applyDiscountToProductsLoading: applyDiscountToProductsMutate.isLoading,
			}}
			handlers={{
				handleSearch: handleSearch,
				handleResetFilter: handleResetFilter,
				handleApplyFilter: handleApplyFilter,
				handleShowFilter: handleShowFilter,
				handleSwitchMode: handleSwitchMode,
				handleChangeCAtegory: handleChangeCAtegory,
				handleSelectAndUnSelectAllCourses: handleSelectAndUnSelectAllCourses,
				handleSelectCourses: handleSelectCourses,
				handleApplyCourses: handleApplyCourses,
			}}
		/>
	);
};

export default GAddCourses;
