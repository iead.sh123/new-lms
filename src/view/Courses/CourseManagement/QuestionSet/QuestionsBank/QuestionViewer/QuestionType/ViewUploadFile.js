import React from "react";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Label } from "reactstrap";

const ViewUploadFile = ({ question, isGroup }) => {
	return (
		<Row className="mt-4 d-flex align-items-center">
			{!isGroup && (
				<Col xs={12} className="p-0">
					<RFlex>
						<i className="far fa-file pt-1" />
						<Label className="p-0 m-0">{tr`Upload file`}</Label>
					</RFlex>
				</Col>
			)}
			<Col xs={12} className="p-0 pt-3">
				{isGroup ? (
					<ul>
						<li>
							<h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
						</li>
					</ul>
				) : (
					<h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
				)}
				<RFlex className="mb-3 mt-3">
					{question?.files.map((file) => (
						<RFlex key={file.hash_id} styleProps={{ alignItems: "center" }}>
							<RImageViewer mimeType={file?.mime_type} url={file?.hash_id} imageName={file?.name} width={"40px"} height={"40px"} />
							<span>{file.name}</span>
						</RFlex>
					))}
				</RFlex>
			</Col>
		</Row>
	);
};

export default ViewUploadFile;
