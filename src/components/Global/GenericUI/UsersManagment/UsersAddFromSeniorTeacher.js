import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserFromUserAsync } from "store/actions/admin/manageSchool.Action";
import { Row, Col, Button } from "reactstrap";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import { checkUser, saveUsersFromUserAsync } from "store/actions/admin/manageSchool.Action";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const UsersAddFromSeniorTeacher = ({ handleClose, specificName }) => {
	const dispatch = useDispatch();
	const {
		allUsersFromUser,
		userChecked,
		FirstPageUrl,
		NextPageUrl,
		PrevPageUrl,
		LastPageUrl,
		CurrentPage,
		LastPage,
		Total,
		allUsersFromUserLoading,
		saveUsersFromUserLoading,
	} = useSelector((state) => state.manageSchoolRed);

	useEffect(() => {
		dispatch(fetchUserFromUserAsync(specificName, null));
	}, []);

	const _records = React.useMemo(
		() =>
			allUsersFromUser?.map((rc) => ({
				title: rc?.label,
				details: [{ key: "Name", value: rc?.label }],
				actions: [
					{
						name: "check",
						icon: rc.check == true ? iconsFa6.delete : "fa fa-plus",
						color: rc.check == true && rc.color ? rc.color : "info",
						onClick: () => dispatch(checkUser(rc.value)),
					},
				],
			})),
		[allUsersFromUser]
	);

	const handleSave = (e) => {
		e.preventDefault();
		dispatch(saveUsersFromUserAsync({ payload: { table_name: specificName, items: userChecked } }, handleClose));
	};
	return (
		<div className="content">
			{allUsersFromUserLoading ? (
				<Loader />
			) : (
				<Row>
					<Col xs={12}>
						<RLister Records={_records} />
					</Col>

					<Col xs={12}>
						<NewPaginator
							firstPageUrl={FirstPageUrl}
							lastPageUrl={LastPageUrl}
							currentPage={CurrentPage}
							lastPage={LastPage}
							prevPageUrl={PrevPageUrl}
							nextPageUrl={NextPageUrl}
							total={Total}
							getAction={fetchUserFromUserAsync}
						/>
					</Col>

					<Col xs={12} style={{ display: "flex", justifyContent: "flex-end" }}>
						<Button
							block
							className="btn-round ml-4 mb-2 mt-1 "
							color="primary"
							type="submit"
							disabled={userChecked.length == 0 ? true : false}
							onClick={(e) => handleSave(e)}
							style={{ width: 100 }}
						>
							{saveUsersFromUserLoading ? (
								<>
									{tr`save`} <i className="fa fa-refresh fa-spin"></i>
								</>
							) : (
								tr("save")
							)}
						</Button>

						<RButton text={tr`cancel`} onClick={handleClose} color="primary" outline />
					</Col>
				</Row>
			)}
		</div>
	);
};

export default UsersAddFromSeniorTeacher;
