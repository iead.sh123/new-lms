import React, { useEffect, useReducer } from "react";
import {
  usersAndPermissionsReducer,
  initialState,
} from "../State/UsersAndPermissions.reducer";
import { getUsersTypesByOrganizationAsync } from "../State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import { useHistory } from "react-router-dom";

const Users = () => {
  const history = useHistory();
  const [usersAndPermissionsData, dispatch] = useReducer(
    usersAndPermissionsReducer,
    initialState
  );

  useEffect(() => {
    if (usersAndPermissionsData.usersTypes.length == 0) {
      getUsersTypesByOrganizationAsync({
        dispatch: dispatch,
        forDropDownList: false,
        history: history,
        pushToFirstTab: true,
      });
    }
  }, []);

  return (
    <React.Fragment>
      {usersAndPermissionsData.usersTypesLoading ? (
        <Loader />
      ) : (
        <RTabsPanel_Vertical Tabs={usersAndPermissionsData.usersTypes} />
      )}
    </React.Fragment>
  );
};

export default Users;
