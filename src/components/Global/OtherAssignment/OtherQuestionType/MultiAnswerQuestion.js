import React, { useEffect, useState } from "react";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import MathJax from "react-mathjax-preview";
const MultiAnswerQuestion = ({ choices, assignmentReadMode, student }) => {
  const [selectedChoices, setSelectedChoices] = useState([]);
  const [correctAnswer, setCorrectAnswer] = useState([]);

  useEffect(() => {
    if (assignmentReadMode) {
      if (student.detail.notSolved === 1) {
        setSelectedChoices([]);
        setCorrectAnswer([]);
        return;
      }
      setSelectedChoices(student.detail.choices.map((choice) => choice));
      setCorrectAnswer(choices.filter((choice) => choice.is_correct === 1));
      return;
    }
  }, []);

  return choices.map((choice, index) => {
    return (
      <div
        key={index + choice.id}
        className="col-md-12 form-group row mt-2"
        style={{ width: "600px" }}
      >
        <div className={`${choice.attachments ? "col-md-7" : "col-md-11"}`}>
          <div
            className={`form-control ${
              correctAnswer.find((el) => el.id === choice.id)
                ? "correct_answer"
                : student.detail.choices.find((el) => el === choice.id) &&
                  correctAnswer.find((el) => el.id !== choice.id)
                ? "wrong_answer"
                : ""
            }`}
          >
            {choice.choice && choice.choice.includes("<math") ? (
              <MathJax math={choice.choice} />
            ) : (
              <div dangerouslySetInnerHTML={{ __html: choice.choice }} />
            )}
          </div>
        </div>
        <div className="col-md-1 text-center">
          <div className="custom-control custom-checkbox custom-control-inline mx-0 pl-3">
            <input
              name={`student${student.id}IsCorrect${choice.id}`}
              id={`student${student.id}IsCorrect${choice.id}`}
              type="checkbox"
              checked={selectedChoices.includes(choice.id)}
              className="custom-control-input assignment_checkbox"
              value={choice.id}
              disabled={assignmentReadMode}
            />
            <label
              htmlFor={`student${student.id}IsCorrect${choice.id}`}
              className="custom-control-label"
            ></label>
          </div>
        </div>
        {choice.attachments && (
          <div className="col-md-3">
            {["png", "jpeg"].includes(choice.attachments.mime_type) ? (
              <ImageEnlarge
                imageSrc={resourcesBaseUrl + choice.attachments.url}
              />
            ) : (
              <a
                href={resourcesBaseUrl + choice.attachments.url}
                alt=""
                class="btn btn-primary"
              >
                {translate("Download")}
              </a>
            )}
          </div>
        )}
      </div>
    );
  });
};

export default MultiAnswerQuestion;
