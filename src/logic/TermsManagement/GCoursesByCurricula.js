import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { primaryColor } from "config/constants";
import { useFormik } from "formik";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import iconsFa6 from "variables/iconsFa6";
import RSelect from "components/Global/RComs/RSelect";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GCoursesByCurricula = ({ academicPath }) => {
	const TermsManagementData = useContext(TermsManagementContext);
	const [search, setSearch] = useState("");

	const initialValues = {
		name: "",
		category_id: null,
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	const coursesData = useFetchDataRQ({
		queryKey: ["coursesByCurricula", values.category_id, search],
		queryFn: () =>
			coursesManagerApi.getCoursesByCategoryId({
				name: search,
				category_id: values.category_id,
				course_types: ["isMaster"],
				isPublished: true,
				noPagination: false,
			}),
		enableCondition: values.category_id ? true : false,
		onSuccessFn: (data) => TermsManagementData.handleSelectedAllCourses(data?.data?.data?.courses?.data),
	});
	const allCategoriesData = useFetchDataRQ({
		queryKey: ["allCategories"],
		queryFn: () => coursesManagerApi.allCategories(),
		onSuccessFn: (data) => setFieldValue("category_id", data?.data?.data?.[0]?.value),
	});

	const _records = React.useMemo(
		() =>
			coursesData?.data?.data?.data?.courses?.data?.map((course) => ({
				details: [
					{ key: tr`course_name`, value: course.name },
					{ key: tr`course_code`, value: course?.code ?? "-" },
					{ key: tr`credit`, value: course.credits },
				],
				actions: [
					{
						name: " ",
						icon: iconsFa6.plus,
						color: "primary",

						icon: TermsManagementData.itemObj.coursesByCurricula.some((item) => +item.id == +course.id)
							? iconsFa6.minusSolid
							: iconsFa6.plus,
						color: TermsManagementData.itemObj.coursesByCurricula.some((item) => +item.id == +course.id) ? "danger" : "primary",
						outline: true,
						loading: TermsManagementData.makeCourseClonedLoading,
						onClick: () => {
							TermsManagementData.handleSelectedCoursesByCurricula({
								courseId: course.id,
								termId: TermsManagementData?.itemObj?.termId,
								curriculumId: TermsManagementData?.itemObj?.firstCurriculumID,
							});
						},
					},
				],
			})),
		[coursesData?.data?.data?.data?.courses, TermsManagementData.itemObj.coursesByCurricula]
	);

	//Actions
	const handleSearch = (emptyData) => {
		setSearch(emptyData !== undefined ? "" : values.name);
	};

	const handleSelectCategory = (categoryId) => {
		setFieldValue("category_id", categoryId);
	};

	if (coursesData.isError) {
		return <REmptyData />;
	}

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<RSearchHeader
					searchLoading={coursesData.isLoading && coursesData.fetchStatus != "idle"}
					searchData={values.name}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("name", value)}
					handleChangeSearch={(value) => setFieldValue("name", value)}
				/>

				<div style={{ width: "48%" }}>
					<RSelect
						// option={[{ label: `--${tr`no_category`}--`, value: null }].concat(allCategoriesData?.data?.data?.data)}
						option={allCategoriesData?.data?.data?.data}
						closeMenuOnSelect={true}
						placeholder={academicPath ? tr`program` : tr`category`}
						onChange={(e) => handleSelectCategory(e.value)}
						isLoading={allCategoriesData.isLoading}
					/>
				</div>
			</RFlex>
			<RFlex className="align-items-center">
				{TermsManagementData?.itemObj?.coursesByCurricula?.length > 0 && (
					<>
						{TermsManagementData.copyBulkCoursesMutationLoading ? (
							<i className={iconsFa6.spinner} />
						) : (
							<span
								style={{ color: primaryColor, textDecoration: "underline", cursor: "pointer" }}
								onClick={() => {
									TermsManagementData.handleCopyBulkCoursesMutation();
								}}
							>{tr`save`}</span>
						)}
					</>
				)}
			</RFlex>
			{coursesData.isLoading && coursesData.fetchStatus != "idle" ? <Loader /> : <RLister Records={_records} />}
		</RFlex>
	);
};

export default GCoursesByCurricula;
