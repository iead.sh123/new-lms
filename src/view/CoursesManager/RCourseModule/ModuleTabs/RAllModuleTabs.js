import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import RSpecificModuleContent from "../SpecificModuleContent/RSpecificModuleContent";
import RAllModuleContent from "../AllModuleContent/RAllModuleContent";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";

const RAllModuleTabs = () => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<section>
			<RButton
				className="btn-round"
				key={"all"}
				text={tr`all_module`}
				color="primary"
				// style={{
				//   background: " linear-gradient(94deg, #1C95DD 0%, #668AD7 100%)",
				// }}
				onClick={() => {
					CourseModuleData.setSelectedButton(0);
					CourseModuleData.handleCloseAllCollapses();
					CourseModuleData.handlePushToAnotherRoute({ ModuleId: null });
				}}
				outline={!CourseModuleData.moduleIdParams ? false : true}
			/>
			{CourseModuleData?.courseModule &&
				CourseModuleData?.courseModule?.modules &&
				CourseModuleData?.courseModule?.modules?.map((module) => (
					<RButton
						className="btn-round"
						key={module.id}
						text={module.title}
						color="primary"
						onClick={() => {
							CourseModuleData.setSelectedButton(module.id);
							CourseModuleData.handleFillDataToSpecificModule(module);
							CourseModuleData.setModuleData({
								title: module.title,
								order: module.order_id,
							});
							CourseModuleData.handleCloseAllCollapses();
							CourseModuleData.handlePushToAnotherRoute({
								ModuleId: module.id,
							});
						}}
						outline={CourseModuleData.moduleIdParams == module.id ? false : true}
					/>
				))}
		</section>
	);
};

export default RAllModuleTabs;
