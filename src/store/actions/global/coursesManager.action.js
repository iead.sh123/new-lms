import { coursesManagerApi } from "api/global/coursesManager";
import {
	GET_COURSES_MANAGER_TREE_REQUEST,
	GET_COURSES_MANAGER_TREE_SUCCESS,
	GET_COURSES_MANAGER_TREE_ERROR,
	ADD_CATEGORY_TO_TREE_REQUEST,
	ADD_CATEGORY_TO_TREE_SUCCESS,
	ADD_CATEGORY_TO_TREE_ERROR,
	REMOVE_CATEGORY_FROM_TREE_REQUEST,
	REMOVE_CATEGORY_FROM_TREE_SUCCESS,
	REMOVE_CATEGORY_FROM_TREE_ERROR,
	GET_COURSES_BY_CATEGORY_ID_REQUEST,
	GET_COURSES_BY_CATEGORY_ID_SUCCESS,
	GET_COURSES_BY_CATEGORY_ID_ERROR,
	CLONE_COURSE_REQUEST,
	CLONE_COURSE_SUCCESS,
	CLONE_COURSE_ERROR,
	INSTANTIATE_COURSE_REQUEST,
	INSTANTIATE_COURSE_SUCCESS,
	INSTANTIATE_COURSE_ERROR,
	REMOVE_COURSE_REQUEST,
	REMOVE_COURSE_SUCCESS,
	REMOVE_COURSE_ERROR,
	GET_CATEGORY_ANCESTORS_REQUEST,
	GET_CATEGORY_ANCESTORS_SUCCESS,
	GET_CATEGORY_ANCESTORS_ERROR,
	SAVE_COURSE_REQUEST,
	SAVE_COURSE_SUCCESS,
	SAVE_COURSE_ERROR,
	COURSE_BY_ID_REQUEST,
	COURSE_BY_ID_SUCCESS,
	COURSE_BY_ID_ERROR,
	SET_COURSE_VALUE,
	GET_LEVELS_REQUEST,
	GET_LEVELS_SUCCESS,
	GET_LEVELS_ERROR,
	GET_USER_TYPE_REQUEST,
	GET_USER_TYPE_SUCCESS,
	GET_USER_TYPE_ERROR,
	SET_ADD_ITEMS_TO_OVERVIEW_FIELDS,
	CHANGE_ITEMS_IN_COURSE_OVERVIEW,
	DELETE_ITEMS_FROM_COURSE_OVERVIEW,
	FILL_DATA_IN_OVERVIEW,
	SAVE_COURSE_OVERVIEW_REQUEST,
	SAVE_COURSE_OVERVIEW_SUCCESS,
	SAVE_COURSE_OVERVIEW_ERROR,
	VIEW_COURSE_MODE,
	EMPTY_DATA_WHEN_ADD_NEW_COURSE,
	PUBLISH_AND_UND_PUBLISH_COURSE_REQUEST,
	PUBLISH_AND_UND_PUBLISH_COURSE_SUCCESS,
	PUBLISH_AND_UND_PUBLISH_COURSE_ERROR,
	ALL_FEEDBACK_REQUEST,
	ALL_FEEDBACK_SUCCESS,
	ALL_FEEDBACK_ERROR,
	COURSE_OVERVIEW_BY_ID_REQUEST,
	COURSE_OVERVIEW_BY_ID_SUCCESS,
	COURSE_OVERVIEW_BY_ID_ERROR,
	GET_COURSE_MODULE_REQUEST,
	GET_COURSE_MODULE_SUCCESS,
	GET_COURSE_MODULE_ERROR,
	GET_MODULE_CONTENT_REQUEST,
	GET_MODULE_CONTENT_SUCCESS,
	GET_MODULE_CONTENT_ERROR,
	CREATE_MODULE_REQUEST,
	CREATE_MODULE_SUCCESS,
	CREATE_MODULE_ERROR,
	FILL_DATA_TO_SPECIFIC_MODULE,
	DELETE_MODULE_REQUEST,
	DELETE_MODULE_SUCCESS,
	DELETE_MODULE_ERROR,
	DELETE_MODULE_CONTENT_REQUEST,
	DELETE_MODULE_CONTENT_SUCCESS,
	DELETE_MODULE_CONTENT_ERROR,
	SELECT_LESSON_CONTENT,
	ADD_ITEMS_TO_LESSON_CONTENT,
	SELECT_MODULE_CONTENT,
	ADD_ITEMS_TO_MODULE_CONTENT,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_REQUEST,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_SUCCESS,
	CREATE_OR_EDIT_MODULE_CONTENT_QUESTION_SET_ERROR,
	ADD_CONTENT_TO_LESSON_CONTENT,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_REQUEST,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_SUCCESS,
	PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_ERROR,
	ADD_LINKS_TO_LESSON_CONTENT,
	HANDLE_SHOULD_NOT_BE_ADDED_lESSON_CONTENT,
	ADD_NEW_LESSON_CONTENT_TO_MODULE_CONTENT,
	DELETE_NEW_LESSON_CONTENT,
	REMOVE_LINK_FROM_LESSON_CONTENT_LOCALE,
	EDIT_LESSON_CONTENT_FROM_MODULE_CONTENT,
	RELOAD_LIVE_SESSION,
	GET_MY_COURSES_REQUEST,
	GET_MY_COURSES_SUCCESS,
	GET_MY_COURSES_ERROR,
	ALL_FILTERS_TO_COURSES_REQUEST,
	ALL_FILTERS_TO_COURSES_SUCCESS,
	ALL_FILTERS_TO_COURSES_ERROR,
	ALL_COURSES_REQUEST,
	ALL_COURSES_SUCCESS,
	ALL_COURSES_ERROR,
	LANDING_PAGE_REQUEST,
	LANDING_PAGE_SUCCESS,
	LANDING_PAGE_ERROR,
	GET_MY_CURRICULA_REQUEST,
	GET_MY_CURRICULA_SUCCESS,
	GET_MY_CURRICULA_ERROR,
	ALL_STUDENTS_REQUEST,
	ALL_STUDENTS_SUCCESS,
	ALL_STUDENTS_ERROR,
	HANDLE_SHOULD_NOT_BE_ADDED_MODULE_CONTENT,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
	UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
	UPDATE_TITLE_TO_QUESTION_SET,
	CHANGE_BEHAVIOR_IN_MODULE_CONTENT,
	DELETE_NEW_MODULE_CONTENT,
	ENROLL_IN_COURSE_REQUEST,
	ENROLL_IN_COURSE_SUCCESS,
	ENROLL_IN_COURSE_ERROR,
	PUSH_DATA_TO_COURSE_MODULE,
} from "./globalTypes";
import tr from "components/Global/RComs/RTranslator";
import { baseURL, genericPath } from "engine/config";
import { toast } from "react-toastify";

export const getCourseCategoriesTreeAsync =
	({ forDropDown, searchText }) =>
	async (dispatch) => {
		dispatch({ type: GET_COURSES_MANAGER_TREE_REQUEST });
		try {
			const response = await coursesManagerApi.getCourseCategoriesTree({ forDropDown, searchText });
			if (response.data.status === 1) {
				const data = response.data.data;

				dispatch({
					type: GET_COURSES_MANAGER_TREE_SUCCESS,
					payload: { data },
				});
			} else {
				dispatch({ type: GET_COURSES_MANAGER_TREE_ERROR });

				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: GET_COURSES_MANAGER_TREE_ERROR });
			toast.error(error?.message);
		}
	};

export const addCategoryToTreeAsync = (data, handleCloseAddCategory, setFormGroup) => async (dispatch) => {
	dispatch({ type: ADD_CATEGORY_TO_TREE_REQUEST });
	try {
		const response = await coursesManagerApi.addCategoryToTree(data);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ADD_CATEGORY_TO_TREE_SUCCESS,
				payload: { data },
			});
			dispatch(getCourseCategoriesTreeAsync({}));
			handleCloseAddCategory && handleCloseAddCategory();
			setFormGroup && setFormGroup({ id: null, name: "", parent_id: null });
		} else {
			dispatch({ type: ADD_CATEGORY_TO_TREE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ADD_CATEGORY_TO_TREE_ERROR });
		toast.error(error?.message);
	}
};

export const removeCategoryFromTreeAsync = (categoryId, hideAlert) => async (dispatch) => {
	dispatch({ type: REMOVE_CATEGORY_FROM_TREE_REQUEST });
	try {
		const response = await coursesManagerApi.deleteCategoryFromTree(categoryId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: REMOVE_CATEGORY_FROM_TREE_SUCCESS,
				payload: { data },
			});

			hideAlert();
			dispatch(getCourseCategoriesTreeAsync({}));
		} else {
			dispatch({ type: REMOVE_CATEGORY_FROM_TREE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: REMOVE_CATEGORY_FROM_TREE_ERROR });
		toast.error(error?.message);
	}
};

export const getCoursesByCategoryIdAsync = (data) => async (dispatch) => {
	dispatch({ type: GET_COURSES_BY_CATEGORY_ID_REQUEST });
	try {
		const response = await coursesManagerApi.getCoursesByCategoryId(data);
		if (response.data.status === 1) {
			const data = response.data.data.courses;
			const statistics = response.data.data.statistics;

			dispatch({
				type: GET_COURSES_BY_CATEGORY_ID_SUCCESS,
				payload: { data, statistics },
			});
		} else {
			dispatch({ type: GET_COURSES_BY_CATEGORY_ID_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_COURSES_BY_CATEGORY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const searchCoursesCatalogAsync = (data) => async (dispatch) => {
	dispatch({ type: GET_COURSES_BY_CATEGORY_ID_REQUEST });
	try {
		const response = await coursesManagerApi.getCoursesByCategoryId(data);
		if (response.data.status === 1) {
			const data = response.data.data.courses;
			const statistics = response.data.data.statistics;

			dispatch({
				type: GET_COURSES_BY_CATEGORY_ID_SUCCESS,
				payload: { data, statistics },
			});
		} else {
			dispatch({ type: GET_COURSES_BY_CATEGORY_ID_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_COURSES_BY_CATEGORY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const cloneCourseAsync = (courseId, payloadData, categoryId) => async (dispatch) => {
	dispatch({ type: CLONE_COURSE_REQUEST });
	try {
		const response = await coursesManagerApi.cloneToCourse(courseId);
		if (response.data.status === 1) {
			dispatch({
				type: CLONE_COURSE_SUCCESS,
			});
			dispatch(
				getCoursesByCategoryIdAsync({
					category_id: categoryId ?? null,
					noPagination: true,
				})
			);
		} else {
			dispatch({ type: CLONE_COURSE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: CLONE_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const instantiateCourseAsync = (courseId, payloadData, categoryId) => async (dispatch) => {
	dispatch({ type: INSTANTIATE_COURSE_REQUEST });
	try {
		const response = await coursesManagerApi.instantiateToCourse(courseId);
		if (response.data.status === 1) {
			dispatch({
				type: INSTANTIATE_COURSE_SUCCESS,
			});
			dispatch(
				getCoursesByCategoryIdAsync({
					category_id: categoryId ?? null,
					noPagination: true,
				})
			);
		} else {
			dispatch({ type: INSTANTIATE_COURSE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: INSTANTIATE_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const removeCourseAsync = (courseId, hideAlert, isPublished, categoryId) => async (dispatch) => {
	dispatch({ type: REMOVE_COURSE_REQUEST });
	try {
		const response = await coursesManagerApi.removeCourse(courseId);
		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_COURSE_SUCCESS,
				payload: { courseId, isPublished },
			});
			hideAlert && hideAlert();
			// dispatch(
			// 	getCoursesByCategoryIdAsync({
			// 		category_id: categoryId ?? null,
			// 		noPagination: true,
			// 	})
			// );
		} else {
			dispatch({ type: REMOVE_COURSE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: REMOVE_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const categoryAncestorsAsync = (categoryId) => async (dispatch) => {
	dispatch({ type: GET_CATEGORY_ANCESTORS_REQUEST });
	try {
		const response = await coursesManagerApi.categoryAncestors(categoryId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_CATEGORY_ANCESTORS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_CATEGORY_ANCESTORS_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_CATEGORY_ANCESTORS_ERROR });
		toast.error(error?.message);
	}
};

export const createCourseAsync = (data, client, courseId, categoryId, history, doNotGoToCourseOverView) => async (dispatch) => {
	if (!data.name) {
		toast.warning(tr`name_course_is_required`);
	} else {
		dispatch({ type: SAVE_COURSE_REQUEST });
		try {
			const response = await coursesManagerApi.saveCourse(data, client);
			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: SAVE_COURSE_SUCCESS,
					payload: { data },
				});

				history &&
					!doNotGoToCourseOverView &&
					history.push(
						`${baseURL}/${genericPath}/courses-manager/editor${data.course_id ? `/course/${data.course_id}` : ""}${
							categoryId ? `/category/${categoryId}` : ""
						}/course-overview`
					);
			} else {
				dispatch({ type: SAVE_COURSE_ERROR });

				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: SAVE_COURSE_ERROR });
			toast.error(error?.message);
		}
	}
};

export const patchCourseAsync = (data, client, courseId, categoryId, history, doNotGoToCourseOverView) => async (dispatch) => {
	if (!data.name) {
		toast.warning(tr`name_course_is_required`);
	} else {
		dispatch({ type: SAVE_COURSE_REQUEST });
		try {
			const response = await coursesManagerApi.patchOnCourseInformation(data, client, courseId);
			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: SAVE_COURSE_SUCCESS,
					payload: { data },
				});
				history &&
					!doNotGoToCourseOverView &&
					history.push(
						`${baseURL}/${genericPath}/courses-manager/editor${data.course_id ? `/course/${data.course_id}` : ""}${
							categoryId ? `/category/${categoryId}` : ""
						}/course-overview`
					);
			} else {
				dispatch({ type: SAVE_COURSE_ERROR });

				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: SAVE_COURSE_ERROR });
			toast.error(error?.message);
		}
	}
};

export const courseByIdAsync = (courseId) => async (dispatch) => {
	dispatch({ type: COURSE_BY_ID_REQUEST });
	try {
		const response = await coursesManagerApi.courseById(courseId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: COURSE_BY_ID_SUCCESS,
				payload: { data },
			});
			if (response.data.data.overview == null) {
				dispatch(fillDataInOverview());
			}
		} else {
			dispatch({ type: COURSE_BY_ID_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: COURSE_BY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const courseOverviewByIdAsync = (courseId) => async (dispatch) => {
	dispatch({ type: COURSE_OVERVIEW_BY_ID_REQUEST });
	try {
		const response = await coursesManagerApi.getCourseOverview(courseId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: COURSE_OVERVIEW_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: COURSE_OVERVIEW_BY_ID_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: COURSE_OVERVIEW_BY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const setCourseValue = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_COURSE_VALUE,
			payload: { name, value },
		});
	};
};

export const getLevelsAsync = () => async (dispatch) => {
	dispatch({ type: GET_LEVELS_REQUEST });
	try {
		const response = await coursesManagerApi.levels();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_LEVELS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_LEVELS_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LEVELS_ERROR });
		toast.error(error?.message);
	}
};

export const getUserTypeAsync = (forDropDownList, userType) => async (dispatch) => {
	dispatch({ type: GET_USER_TYPE_REQUEST });
	try {
		const response = await coursesManagerApi.getUserType(forDropDownList, userType);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_USER_TYPE_SUCCESS,
				payload: { data, userType },
			});
		} else {
			dispatch({ type: GET_USER_TYPE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_USER_TYPE_ERROR });
		toast.error(error?.message);
	}
};

export const setAddItemsToOverviewFields = (key) => {
	return (dispatch) => {
		const data = { fakeId: Math.random(), item: "" };
		const dataValue = { fakeId: Math.random(), value: "" };
		const dataFaq = { fakeId: Math.random(), question: "", answer: "" };

		dispatch({
			type: SET_ADD_ITEMS_TO_OVERVIEW_FIELDS,
			payload: { key, data, dataValue, dataFaq },
		});
	};
};

export const changeItemsInCourseOverView = (name, value, id, key) => {
	return (dispatch) => {
		dispatch({
			type: CHANGE_ITEMS_IN_COURSE_OVERVIEW,
			payload: { name, value, id, key },
		});
	};
};
export const deleteItemsInCourseOverView = (id, key) => {
	return (dispatch) => {
		dispatch({
			type: DELETE_ITEMS_FROM_COURSE_OVERVIEW,
			payload: { id, key },
		});
	};
};

export const fillDataInOverview = () => {
	return (dispatch) => {
		dispatch({
			type: FILL_DATA_IN_OVERVIEW,
		});
	};
};

export const viewCourseMode = (mode) => {
	return (dispatch) => {
		dispatch({
			type: VIEW_COURSE_MODE,
			payload: { mode },
		});
	};
};

export const emptyDataWhenAddNewCourse = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_DATA_WHEN_ADD_NEW_COURSE,
		});
	};
};

export const saveCourseOverViewAsync = (courseId, data, history) => async (dispatch) => {
	dispatch({ type: SAVE_COURSE_OVERVIEW_REQUEST });
	try {
		const response = await coursesManagerApi.saveCourseOverview(courseId, data);
		if (response.data.status === 1) {
			dispatch({
				type: SAVE_COURSE_OVERVIEW_SUCCESS,
			});
			history && history.push(`${baseURL}/${genericPath}/courses-manager`);
		} else {
			dispatch({ type: SAVE_COURSE_OVERVIEW_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: SAVE_COURSE_OVERVIEW_ERROR });
		toast.error(error?.message);
	}
};

export const publishAndUnPublishCourseAsync = (courseId, categoryId, isPublish) => async (dispatch) => {
	dispatch({ type: PUBLISH_AND_UND_PUBLISH_COURSE_REQUEST });
	try {
		const response = isPublish ? await coursesManagerApi.unPublishCourse(courseId) : await coursesManagerApi.publishCourse(courseId);
		if (response.data.status === 1) {
			dispatch({
				type: PUBLISH_AND_UND_PUBLISH_COURSE_SUCCESS,
			});
			dispatch(
				getCoursesByCategoryIdAsync({
					category_id: categoryId,
					noPagination: true,
				})
			);
		} else {
			dispatch({ type: PUBLISH_AND_UND_PUBLISH_COURSE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: PUBLISH_AND_UND_PUBLISH_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const addFeedbackAsync = (courseId, searchText) => async (dispatch) => {
	dispatch({ type: ALL_FEEDBACK_REQUEST });
	try {
		const response = await coursesManagerApi.allFeedbacks(courseId, searchText);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ALL_FEEDBACK_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: ALL_FEEDBACK_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ALL_FEEDBACK_ERROR });
		toast.error(error?.message);
	}
};

export const getCourseModuleAsync = (courseId, viewAsStudent) => async (dispatch) => {
	dispatch({ type: GET_COURSE_MODULE_REQUEST });
	try {
		const response = await coursesManagerApi.getCourseModule(courseId, viewAsStudent);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_COURSE_MODULE_SUCCESS,
				payload: { data },
			});
			// dispatch(getLearningObjectMyProgressAsync(data.course.learning_object_id));
		} else {
			dispatch({ type: GET_COURSE_MODULE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_COURSE_MODULE_ERROR });
		toast.error(error?.message);
	}
};

export const getModuleContentAsync = (moduleId) => async (dispatch) => {
	dispatch({ type: GET_MODULE_CONTENT_REQUEST });
	try {
		const response = await coursesManagerApi.getModuleContent(moduleId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_MODULE_CONTENT_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_MODULE_CONTENT_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_MODULE_CONTENT_ERROR });
		toast.error(error?.message);
	}
};

export const createModuleAsync =
	(courseId, data, edit, handleCloseCreateModuleForm, handleRedirectToSpecificModule) => async (dispatch) => {
		dispatch({ type: CREATE_MODULE_REQUEST });
		try {
			const response = await coursesManagerApi.createModule(courseId, data);

			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: CREATE_MODULE_SUCCESS,
					payload: { data, edit },
				});
				handleCloseCreateModuleForm();
				handleRedirectToSpecificModule && handleRedirectToSpecificModule(data);
			} else {
				dispatch({ type: CREATE_MODULE_ERROR });

				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: CREATE_MODULE_ERROR });
			toast.error(error?.message);
		}
	};

export const updateModuleAsync = (moduleId, data, edit, handleCloseEditModuleForm) => async (dispatch) => {
	dispatch({ type: CREATE_MODULE_REQUEST });
	try {
		const response = await coursesManagerApi.updateModule(moduleId, data);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: CREATE_MODULE_SUCCESS,
				payload: { data, edit },
			});
			handleCloseEditModuleForm();
		} else {
			dispatch({ type: CREATE_MODULE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: CREATE_MODULE_ERROR });
		toast.error(error?.message);
	}
};

export const fillDataToSpecificModule = (data) => {
	return (dispatch) => {
		dispatch({
			type: FILL_DATA_TO_SPECIFIC_MODULE,
			payload: { data },
		});
	};
};

export const deleteModuleAsync = (moduleId, courseId, hideAlert) => async (dispatch) => {
	dispatch({ type: DELETE_MODULE_REQUEST });
	try {
		const response = await coursesManagerApi.deleteModule(moduleId);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_MODULE_SUCCESS,
				payload: { moduleId },
			});

			hideAlert();
		} else {
			dispatch({ type: DELETE_MODULE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: DELETE_MODULE_ERROR });
		toast.error(error?.message);
	}
};

export const deleteModuleContentAsync = (moduleId, moduleContentId, hideAlert) => async (dispatch) => {
	dispatch({ type: DELETE_MODULE_CONTENT_REQUEST });
	try {
		const response = await coursesManagerApi.deleteModuleContent(moduleContentId);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_MODULE_CONTENT_SUCCESS,
				payload: { moduleId, moduleContentId },
			});
			hideAlert && hideAlert();
		} else {
			dispatch({ type: DELETE_MODULE_CONTENT_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: DELETE_MODULE_CONTENT_ERROR });
		toast.error(error?.message);
	}
};

export const deleteNewModuleContent = (moduleId, moduleContentId, hideAlert) => {
	return (dispatch) => {
		dispatch({
			type: DELETE_NEW_MODULE_CONTENT,
			payload: {
				moduleId,
				moduleContentId,
				hideAlert,
			},
		});
		dispatch(handleShouldNotBeAddedModuleContent(false));
		hideAlert && hideAlert();
	};
};

export const deleteNewLessonContent = (moduleId, moduleContentId, lessonContentId, lessonContentType, hideAlert) => {
	return (dispatch) => {
		dispatch({
			type: DELETE_NEW_LESSON_CONTENT,
			payload: {
				moduleId,
				moduleContentId,
				lessonContentId,
				lessonContentType,
				hideAlert,
			},
		});
		dispatch(handleShouldNotBeAddedLessonContent(false));
		hideAlert && hideAlert();
	};
};

export const removeLinkFromLessonContentLocale = (content, lessonContent, fakeId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_LINK_FROM_LESSON_CONTENT_LOCALE,
			payload: {
				content,
				lessonContent,
				fakeId,
			},
		});
	};
};

export const selectLessonContent = (lessonContentType) => {
	return (dispatch) => {
		dispatch({
			type: SELECT_LESSON_CONTENT,
			payload: { lessonContentType },
		});
	};
};

export const addItemsToLessonContent = (content, type, title) => {
	return (dispatch) => {
		dispatch({
			type: ADD_ITEMS_TO_LESSON_CONTENT,
			payload: { content, type, title },
		});
	};
};

export const selectModuleContent = (moduleContentType) => {
	return (dispatch) => {
		dispatch({
			type: SELECT_MODULE_CONTENT,
			payload: { moduleContentType },
		});
	};
};

export const addItemsToModuleContent = (content, type, title) => {
	return (dispatch) => {
		dispatch({
			type: ADD_ITEMS_TO_MODULE_CONTENT,
			payload: { content, type, title },
		});
	};
};

export const addContentToLessonContent = (content, lessonContent, name, value, type, linkIndex) => {
	return (dispatch) => {
		dispatch({
			type: ADD_CONTENT_TO_LESSON_CONTENT,
			payload: { content, lessonContent, name, value, type, linkIndex },
		});
	};
};
export const addLinksToLessonModule = (content, lessonContent) => {
	return (dispatch) => {
		dispatch({
			type: ADD_LINKS_TO_LESSON_CONTENT,
			payload: { content, lessonContent },
		});
	};
};

export const publishAndUnPublishModuleContentAsync = (moduleId, moduleContentId, publish) => async (dispatch) => {
	dispatch({ type: PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_REQUEST });

	try {
		const response = publish
			? await coursesManagerApi.publishModuleContent(moduleContentId)
			: await coursesManagerApi.unPublishModuleContent(moduleContentId);

		if (response.data.status === 1) {
			dispatch({
				type: PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_SUCCESS,
				payload: { moduleId, moduleContentId, publish },
			});
		} else {
			dispatch({ type: PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: PUBLISH_AND_UN_PUBLISH_MODULE_CONTENT_ERROR });
		toast.error(error?.message);
	}
};

export const handleShouldNotBeAddedLessonContent = (flag) => {
	return (dispatch) => {
		dispatch({
			type: HANDLE_SHOULD_NOT_BE_ADDED_lESSON_CONTENT,
			payload: { flag },
		});
	};
};

export const handleShouldNotBeAddedModuleContent = (flag) => {
	return (dispatch) => {
		dispatch({
			type: HANDLE_SHOULD_NOT_BE_ADDED_MODULE_CONTENT,
			payload: { flag },
		});
	};
};

export const addNewLessonContentToModuleContent = (courseModule, moduleContent, lessonContent, type) => {
	return (dispatch) => {
		dispatch({
			type: ADD_NEW_LESSON_CONTENT_TO_MODULE_CONTENT,
			payload: { courseModule, moduleContent, lessonContent, type },
		});
	};
};

export const editLessonContentFromModuleContent = (moduleId, moduleContentId, lessonContentId, lessonContentType, data) => {
	return (dispatch) => {
		dispatch({
			type: EDIT_LESSON_CONTENT_FROM_MODULE_CONTENT,
			payload: {
				moduleId,
				moduleContentId,
				lessonContentId,
				lessonContentType,
				data,
			},
		});
	};
};

export const reloadLiveSession = (content, lessonContent, data) => {
	return (dispatch) => {
		dispatch({
			type: RELOAD_LIVE_SESSION,
			payload: { content, lessonContent, data },
		});
	};
};

export const getMyCoursesAsync = (searchText, termId) => async (dispatch) => {
	dispatch({ type: GET_MY_COURSES_REQUEST });
	try {
		const response = await coursesManagerApi.getMyCourses(searchText, termId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_MY_COURSES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_MY_COURSES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_MY_COURSES_ERROR });
		toast.error(error?.message);
	}
};

export const getMyCurriculaAsync = (searchText, semesterId) => async (dispatch) => {
	dispatch({ type: GET_MY_CURRICULA_REQUEST });
	try {
		const response = await coursesManagerApi.getMyCurricula(searchText, semesterId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_MY_CURRICULA_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_MY_CURRICULA_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_MY_CURRICULA_ERROR });
		toast.error(error?.message);
	}
};

export const allFiltersToCoursesAsync = () => async (dispatch) => {
	dispatch({ type: ALL_FILTERS_TO_COURSES_REQUEST });
	try {
		const response = await coursesManagerApi.allFiltersToCourses();
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: ALL_FILTERS_TO_COURSES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: ALL_FILTERS_TO_COURSES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ALL_FILTERS_TO_COURSES_ERROR });
		toast.error(error?.message);
	}
};

export const allCoursesAsync = (user, organizationId, data) => async (dispatch) => {
	dispatch({ type: ALL_COURSES_REQUEST });
	try {
		const response = await coursesManagerApi.allCoursesFilter(user, organizationId, data);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: ALL_COURSES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: ALL_COURSES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ALL_COURSES_ERROR });
		toast.error(error?.message);
	}
};

export const landingPageAsync = (user, organizationId) => async (dispatch) => {
	dispatch({ type: LANDING_PAGE_REQUEST });
	try {
		const response = await coursesManagerApi.landingPage(user, organizationId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: LANDING_PAGE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: LANDING_PAGE_ERROR });
			//
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: LANDING_PAGE_ERROR });
		//toast.error(error?.message);
	}
};

export const allStudentsAsync = (courseId) => async (dispatch) => {
	dispatch({ type: ALL_STUDENTS_REQUEST });
	try {
		const response = await coursesManagerApi.allStudents(courseId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: ALL_STUDENTS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: ALL_STUDENTS_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ALL_STUDENTS_ERROR });
		toast.error(error?.message);
	}
};

export const updateTitleToQuestionSet = (moduleId, moduleContentId, key, value) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_TITLE_TO_QUESTION_SET,
			payload: { moduleId, moduleContentId, key, value },
		});
	};
};
export const updateQuestionSetNameFromModuleContentAsync = (type, contentId, data) => async (dispatch) => {
	dispatch({ type: UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST });
	try {
		const response = await coursesManagerApi.updateQuestionSetNameFromModuleContent(type, contentId, data);
		if (response.data.status === 1) {
			dispatch({
				type: UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
			});
		} else {
			dispatch({ type: UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: UPDATE_QUESTION_SET_FROM_MODULE_CONTENT_ERROR });
		toast.error(error?.message);
	}
};

export const changeBehaviorInModuleContent = (moduleId, moduleContentId, key, value) => {
	return (dispatch) => {
		dispatch({
			type: CHANGE_BEHAVIOR_IN_MODULE_CONTENT,
			payload: { moduleId, moduleContentId, key, value },
		});
	};
};

export const enrollInCourseAsync = (courseId) => async (dispatch) => {
	dispatch({ type: ENROLL_IN_COURSE_REQUEST });
	try {
		const response = await coursesManagerApi.enrollInCourse(courseId);
		if (response.data.status === 1) {
			dispatch({
				type: ENROLL_IN_COURSE_SUCCESS,
				payload: { courseId },
			});
		} else {
			dispatch({ type: ENROLL_IN_COURSE_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ENROLL_IN_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const pushDataToCourseModule = (data, moduleId) => {
	return (dispatch) => {
		dispatch({
			type: PUSH_DATA_TO_COURSE_MODULE,
			payload: { data, moduleId },
		});
		dispatch(handleShouldNotBeAddedModuleContent(false));
	};
};
