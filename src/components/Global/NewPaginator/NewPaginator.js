import styles from "./NewPaginator.Module.scss";
import { CardBody, PaginationItem, PaginationLink, Pagination } from "reactstrap";
import { useDispatch } from "react-redux";

const NewPaginator = ({ firstPageUrl, lastPageUrl, currentPage, lastPage, prevPageUrl, nextPageUrl, total, data, getAction }) => {
	const dispatch = useDispatch();
	const handleFirstClicked = () => {
		dispatch(getAction(firstPageUrl, data ? { ...data } : ""));
	};

	const handleLastClicked = () => {
		dispatch(getAction(lastPageUrl, data ? { ...data } : ""));
	};

	const handlePrevClicked = () => {
		if (currentPage == 1) dispatch(getAction(lastPageUrl, data ? { ...data } : ""));
		else dispatch(getAction(prevPageUrl, data ? { ...data } : ""));
	};

	const handleNextClicked = () => {
		if (currentPage == lastPage) dispatch(getAction(firstPageUrl, data ? { ...data } : ""));
		else dispatch(getAction(nextPageUrl, data ? { ...data } : ""));
	};

	return (
		<div className="form-group text-center page-num">
			<CardBody className="text-center">
				<nav aria-label="Page navigation example">
					<Pagination className={styles.pagenation_center}>
						<PaginationItem disabled={currentPage == 1 ? true : false}>
							<PaginationLink
								aria-label="Previous"
								onClick={() => {
									handleFirstClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem disabled={nextPageUrl == "" ? true : false}>
							<PaginationLink
								aria-label="Previous"
								onClick={() => {
									handlePrevClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink className={styles.agenation_font}>
								<span className={styles.current_style}>{currentPage}</span>
								{"  "} / {lastPage}
							</PaginationLink>
						</PaginationItem>
						<PaginationItem disabled={prevPageUrl == "" ? true : false}>
							<PaginationLink
								aria-label="Next"
								onClick={() => {
									handleNextClicked();
								}}
							>
								<strong aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-right font-weight-bold" />
								</strong>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem disabled={currentPage == total ? true : false}>
							<PaginationLink
								aria-label="Next"
								onClick={() => {
									handleLastClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-right font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
					</Pagination>
				</nav>
			</CardBody>
		</div>
	);
};
export default NewPaginator;
