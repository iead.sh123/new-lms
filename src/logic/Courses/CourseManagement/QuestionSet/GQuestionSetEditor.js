import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Loader from "utils/Loader";
import RQuestionSetEditor from "view/Courses/CourseManagement/QuestionSet/RQuestionSetEditor";
import {
	initiateQuestionSetFormate,
	fillQuestionSetInfo,
	createNewQuestion,
	removeQuestionGroup,
	removeQuestionType,
	fillDataToQuestionType,
	createQuestionSetAsync,
	addChoicesToQuestion,
	fillDataInChoicesAnswers,
	removeChoiceFromQuestion,
	fillDataInCorrectAnswerToSomeQuestions,
	addPairsToMatchQuestion,
	calculatePoints,
	questionSetByIdAsync,
	updateQuestionSetAsync,
	createNewQuestionGroup,
	addDataToQuestionsGroup,
	createNewQuestionsToQuestionGroup,
	changeAllRequiredAnswerToFalse,
} from "store/actions/global/questionSet.actions";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { handleShouldNotBeAddedModuleContent, handleShouldNotBeAddedLessonContent } from "store/actions/global/coursesManager.action";

export const QuestionSetContext = React.createContext();

const GQuestionSetEditor = () => {
	const history = useHistory();
	const [openedCollapses, setOpenedCollapses] = useState([]);
	const [requiredAnswerQuestionsGroup, setRequiredAnswerQuestionsGroup] = useState(false);

	const { questionSetId, courseId, curriculumId, moduleId, categoryId, cohortId, questionSetType } = useParams();
	const dispatch = useDispatch();
	const { questionSet, questionSetByIdLoading, createQuestionSetLoading } = useSelector((state) => state.questionSetRed);

	useEffect(() => {
		dispatch(calculatePoints());
	}, [questionSet, questionSet?.groups]);

	useEffect(() => {
		if (!questionSetId) {
			//i use this condition because when create from question bank i don't need to initiate store one more time
			if (questionSet?.groups?.length > 0) {
			} else {
				dispatch(initiateQuestionSetFormate(courseId, curriculumId));
			}
		} else {
			if (questionSet?.groups?.length > 0) {
			} else {
				dispatch(questionSetByIdAsync(questionSetType, questionSetId));
			}
		}
	}, []);

	const collapsesQuestionSetGroupToggle = (questionSetGroupId) => {
		const isOpen = openedCollapses.includes(questionSetGroupId);
		if (isOpen) {
			setOpenedCollapses(openedCollapses.filter((id) => id !== questionSetGroupId));
		} else {
			setOpenedCollapses([...openedCollapses, questionSetGroupId]);
		}
	};

	const handleChangeQuestionSetInfo = ({ key, value }) => {
		dispatch(fillQuestionSetInfo(key, value));
	};

	const handleCreateNewQuestion = (data, questionSetIndex) => {
		dispatch(createNewQuestion(data, questionSetIndex));
	};

	const handleCreateNewQuestionsToQuestionGroup = (data, questionSetIndex, groupId) => {
		dispatch(createNewQuestionsToQuestionGroup(data, questionSetIndex, groupId));
	};

	const handleCreateNewGroup = (questionSetIndex) => {
		dispatch(createNewQuestionGroup(questionSetIndex));
	};

	const handleRemoveQuestionGroup = ({ questionGroupId, questionTypeId, isGroup }) => {
		dispatch(removeQuestionGroup(questionGroupId, questionTypeId, isGroup));
	};

	const handleRemoveQuestionType = (questionGroupId, questionTypeId) => {
		dispatch(removeQuestionType(questionGroupId, questionTypeId));
	};

	const handleFillDataToQuestionType = ({ questionGroupId, questionTypeId, key, value }) => {
		dispatch(fillDataToQuestionType(questionGroupId, questionTypeId, key, value));
	};

	const handleCreateQuestionSet = () => {
		if (!questionSetId) {
			dispatch(createQuestionSetAsync(questionSetType, questionSet, moduleId, history));
		} else {
			dispatch(updateQuestionSetAsync(questionSetType, questionSetId, questionSet, history));
		}
	};

	const handleAddChoicesToQuestion = ({ questionGroupId, questionTypeId, choicesLength, questionType }) => {
		dispatch(addChoicesToQuestion(questionGroupId, questionTypeId, choicesLength, questionType));
	};

	const handleFillDataInChoicesAnswers = ({ questionGroupId, questionTypeId, choiceId, key, value, questionType }) => {
		dispatch(fillDataInChoicesAnswers(questionGroupId, questionTypeId, choiceId, key, value, questionType));
	};
	const handleRemoveChoiceFromQuestion = ({ questionGroupId, questionTypeId, choiceId, questionType, choiceOrder }) => {
		dispatch(removeChoiceFromQuestion(questionGroupId, questionTypeId, choiceId, questionType, choiceOrder));
	};

	const handleFillDataInCorrectAnswerToSomeQuestions = ({
		questionGroupId,
		questionTypeId,
		questionType,
		value,
		// key,
	}) => {
		dispatch(
			fillDataInCorrectAnswerToSomeQuestions(
				questionGroupId,
				questionTypeId,
				questionType,
				value
				// key,
			)
		);
	};
	const handleAddPairsToMatchQuestion = ({ questionGroupId, questionTypeId, parisLength }) => {
		dispatch(addPairsToMatchQuestion(questionGroupId, questionTypeId, parisLength));
	};

	const handleRequiredAnswerQuestionsGroup = () => {
		setRequiredAnswerQuestionsGroup(!requiredAnswerQuestionsGroup);
	};

	const handleChangeQuestionsGroup = ({ questionGroupId, key, value }) => {
		dispatch(addDataToQuestionsGroup(questionGroupId, key, value));
	};

	const handleChangeAllRequiredAnswerToFalse = ({ questionGroupId, key, value }) => {
		dispatch(changeAllRequiredAnswerToFalse(questionGroupId, key, value));
	};

	const handlePushToQuestionsBank = ({ isGroup, groupId, questionSetIndex }) => {
		if (categoryId) {
			history.push({
				pathname: `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/choose-questions-bank/${questionSetType}`,
				state: {
					isGroup: isGroup,
					groupId: groupId,
					questionSetIndex: questionSetIndex,
				},
			});
		} else {
			history.push({
				pathname: `${baseURL}/${genericPath}/course-management/${
					courseId ? `course/${courseId}` : `curricula/${curriculumId}`
				}/cohort/${cohortId}/choose-questions-bank/${questionSetType}`,
				state: {
					isGroup: isGroup,
					groupId: groupId,
					questionSetIndex: questionSetIndex,
				},
			});
		}
	};

	const handleGoBack = () => {
		history.goBack();
		dispatch(handleShouldNotBeAddedModuleContent(false));
		dispatch(handleShouldNotBeAddedLessonContent(false));
	};

	return (
		<React.Fragment>
			{questionSetByIdLoading ? (
				<Loader />
			) : (
				<QuestionSetContext.Provider
					value={{
						questionSet,
						requiredAnswerQuestionsGroup,
						moduleId,

						handleChangeQuestionSetInfo,
						handleCreateNewQuestionsToQuestionGroup,
						handleCreateNewQuestion,
						handleRemoveQuestionGroup,
						handleRemoveQuestionType,
						handleFillDataToQuestionType,
						handleCreateQuestionSet,
						handleAddChoicesToQuestion,
						handleFillDataInChoicesAnswers,
						handleRemoveChoiceFromQuestion,
						handleFillDataInCorrectAnswerToSomeQuestions,
						handleAddPairsToMatchQuestion,
						handleCreateNewGroup,
						handleChangeQuestionsGroup,
						handleChangeAllRequiredAnswerToFalse,
						handleRequiredAnswerQuestionsGroup,
						handlePushToQuestionsBank,
						handleGoBack,

						collapsesQuestionSetGroupToggle,
						openedCollapses,
						setRequiredAnswerQuestionsGroup,

						createQuestionSetLoading,
					}}
				>
					<RQuestionSetEditor />
				</QuestionSetContext.Provider>
			)}
		</React.Fragment>
	);
};

export default GQuestionSetEditor;
