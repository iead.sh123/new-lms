import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const questionSetApi = {
	createQuestionSet: (questionSetType, questionSetData) =>
		post(`${Services.question_set.backend}question-sets/${questionSetType}`, questionSetData),

	questionSetById: (questionSetType, questionSetId) =>
		get(`${Services.question_set.backend}question-sets/${questionSetType}/${questionSetId}`),

	updateQuestionSet: (questionSetType, questionSetId, questionSetData) =>
		patch(`${Services.question_set.backend}question-sets/${questionSetType}/${questionSetId}`, questionSetData),

	submissionsQuestionSet: (questionSetType, moduleContextId, searchData, graded) =>
		get(
			`${Services.question_set.backend}learner-context/${questionSetType}/${moduleContextId}/all-submissions${
				searchData && !graded
					? `?searchText=${searchData}`
					: !searchData && graded
					? `?graded=${graded}`
					: searchData && graded
					? `?searchText=${searchData}&graded=${graded}`
					: ""
			}`
		),

	deleteSubmission: (questionSetType, questionSetId) => destroy(`${Services.question_set.backend} `),

	createQuestionSetToModule: (moduleId, questionSetType, questionSetData) =>
		post(`${Services.question_set.backend}module-context/create-and-link/modules/${moduleId}/type/${questionSetType}`, questionSetData),

	addBehaviorToQuestionSetFromModuleContent: (questionSetType, contentId, data) =>
		patch(`${Services.question_set.backend}module-context/${questionSetType}/${contentId}`, data),

	allQuestionSet: (questionSetType, courseId, searchData, filterTab) =>
		get(
			`${Services.question_set.backend}module-context/for-course-lister/${questionSetType}/courses/${courseId}${
				searchData && filterTab == "RECENTLY_CREATED"
					? `?search=${searchData}&mode=${"unlinked"}`
					: !searchData && filterTab == "RECENTLY_CREATED"
					? `?filter=${"ALL"}&mode=${"unlinked"}`
					: searchData && filterTab && filterTab !== "RECENTLY_CREATED"
					? `?search=${searchData}&filter=${filterTab}&mode=${"linked"}`
					: searchData && filterTab !== "RECENTLY_CREATED"
					? `?search=${searchData}&mode=${"linked"}`
					: !searchData && filterTab
					? `?filter=${filterTab}&mode=${"linked"}`
					: `?filter=${"ALL"}&mode=${"linked"}`
			}`
		),

	allQuestionSetForCourse: (URL, courseId, query) =>
		get(
			URL
				? `${Services.question_set.backend}${URL}`
				: `${Services.question_set.backend}module-context/all-for-course-lister/courses/${courseId}${query ? query : ""}`
		),

	pickQuestionSetToModule: (moduleId, questionSetType, questionSetId) =>
		put(`${Services.question_set.backend}module-context/${moduleId}/${questionSetType}/${questionSetId}`),

	allQuestionSetByCurriculum: (questionSetType, curriculumId, searchData) =>
		get(
			`${Services.question_set.backend}question-sets/curricula/${curriculumId}/${questionSetType}${
				searchData ? `?searchText=${searchData}` : ""
			}`
		),

	//to solve question by student
	getQuestionsToSolve: (questionSetType, moduleContextId) =>
		get(`${Services.question_set.backend}module-context/${questionSetType}/${moduleContextId}`),

	//to correct question by teacher
	getLearnerSolutionToSolve: (questionSetType, questionSetContextId, learnerId) =>
		get(
			`${Services.question_set.backend}learner-context/type/${questionSetType}/question-set/${questionSetContextId}/learner/${learnerId}`
		),

	//to review marks by (student)
	getQuestionsToReview: (questionSetType, questionSetContextId) =>
		get(`${Services.question_set.backend}learner-context/grade/${questionSetType}/question-set/${questionSetContextId}`),

	//to review marks by (teacher)
	getQuestionsToReviewTeacher: (questionSetType, questionSetContextId, learnerId) =>
		get(
			`${Services.question_set.backend}learner-context/type/${questionSetType}/question-set/${questionSetContextId}/learner/${learnerId}`
		),

	//to send learner solution
	solveQuestions: (questionSetType, questionSetContextId, data) =>
		post(`${Services.question_set.backend}learner-context/type/${questionSetType}/question-sets/${questionSetContextId}`, data),
	//to correction question set
	correctionOfQuestions: (questionSetType, learnerQuestionSetId, data) =>
		put(`${Services.question_set.backend}learner-context/grade/${questionSetType}/learner-question-set/${learnerQuestionSetId}`, data),

	publishQuestionSet: (questionSetType, questionSetId) =>
		post(`${Services.question_set.backend}question-sets/${questionSetType}/${questionSetId}/publish`),

	unPublishQuestionSet: (questionSetType, questionSetId) =>
		post(`${Services.question_set.backend}question-sets/${questionSetType}/${questionSetId}/un-publish`),

	deleteQuestionSet: (questionSetType, questionSetId) =>
		destroy(`${Services.question_set.backend}question-sets/${questionSetType}/${questionSetId}`),

	//Questions Bank

	questionsBank: (URL, data, allData) =>
		post(
			URL
				? `${Services.question_set.backend}${URL}`
				: `${Services.question_set.backend}question-groups/question-bank${
						allData ? `?all=${allData}&page=1&limit=12` : "?page=1&limit=12"
				  }`,
			data
		),

	leanerContextCreator: (searchText) =>
		get(`${Services.question_set.backend}learner-context/creators${searchText ? "?searchText=${searchText}" : ""}`),

	//like (course,courseId)
	createQuestionToQuestionsBank: (type, parentId, data) =>
		post(`${Services.question_set.backend}question-groups/${type}/${parentId}`, data),

	updateQuestionsBank: (questionGroupId, data) => patch(`${Services.question_set.backend}question-groups/${questionGroupId}`, data),

	deleteQuestionsBank: (questionGroupId) => destroy(`${Services.question_set.backend}question-groups/${questionGroupId}`),
};
