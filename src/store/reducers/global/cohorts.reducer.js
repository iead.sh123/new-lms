import {
  GET_ALL_COHORT_REQUEST,
  GET_ALL_COHORT_SUCCESS,
  GET_ALL_COHORT_ERROR,
  GET_ROLE_COHORT_BY_USER_ID_REQUEST,
  GET_ROLE_COHORT_BY_USER_ID_SUCCESS,
  GET_ROLE_COHORT_BY_USER_ID_ERROR,
} from "store/actions/global/globalTypes";
import produce from "immer";

const InitState = {
  cohorts: [],
  roleActive: null,

  cohortsLoading: false,
  roleActiveLoading: false,
};

let ind;
const cohortsReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_ALL_COHORT_REQUEST:
        draft.cohortsLoading = true;
        return draft;
      case GET_ALL_COHORT_SUCCESS:
        draft.cohortsLoading = false;
        draft.cohorts = action.payload.cohorts;
        return draft;
      case GET_ALL_COHORT_ERROR:
        draft.cohortsLoading = false;
        return draft;

      case GET_ROLE_COHORT_BY_USER_ID_REQUEST:
        draft.roleActiveLoading = true;
        return draft;
      case GET_ROLE_COHORT_BY_USER_ID_SUCCESS:
        draft.roleActiveLoading = false;
        draft.roleActive = action.payload.roleActive;
        return draft;
      case GET_ROLE_COHORT_BY_USER_ID_ERROR:
        draft.roleActiveLoading = false;
        return draft;

      default:
        return draft;
    }
  });

export default cohortsReducer;
