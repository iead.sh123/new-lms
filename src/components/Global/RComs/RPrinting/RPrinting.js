import React from "react";
import { Row, Col, Table } from "reactstrap";
import styles from "./RPrinting.Module.scss";
import amly from "assets/img/amly_logo_t.png";
import Sections from "./Sections";
import tr from "../RTranslator";
import RubricPrinting from "./RubricPrinting";
import LessonPlanPrinting from "./LessonPlanPrinting";
import AbuseReportPrinting from "./AbuseReportPrinting";

const RPrinting = React.forwardRef((props, ref) => {
  return (
    <div
      ref={ref}
      className={styles.div_margin}
      dir={props.dir ? "ltr" : "rtl"}
    >
      <Row className={styles.RPrintHeader}>
        <Col xs={8}>
          <space className={styles.RPrintSpaceHeader}>
            <h6 className={styles.colorTitle}>
              {props?.data?.title ? tr`title` : tr`Name`} : &nbsp;
            </h6>
            <h6>
              {props?.data?.title
                ? props?.data?.title
                : props?.data?.lesson_plan?.name
                ? props?.data?.lesson_plan?.name
                : props?.data?.sender_name}
            </h6>
          </space>
          {(props?.data?.time || props?.data?.total_points) && (
            <>
              <space className={styles.RPrintSpaceHeader}>
                <h6 className={styles.colorTitle}>
                  {" "}
                  {props?.data?.time ? tr`time` : tr`total_points`} : &nbsp;
                </h6>
                {props?.data?.time && <h6>{props?.data?.time}</h6>}
                {props?.data?.total_points && (
                  <h6>{props?.data?.total_points}</h6>
                )}
              </space>
              <space className={styles.RPrintSpaceHeader}>
                <h6 className={styles.colorTitle}>
                  {tr`description`} : &nbsp;
                </h6>
                <h6>{props?.data?.description}</h6>
              </space>
            </>
          )}
          {props?.abuseId && (
            <space className={styles.RPrintSpaceHeader}>
              <h6 className={styles.colorTitle}>{tr`report`} : &nbsp;</h6>
              <h6
                dangerouslySetInnerHTML={{ __html: props?.data?.description }}
              />
            </space>
          )}
        </Col>
        <Col xs={4} className={styles.RPrintImage}>
          <img
            src={amly}
            alt="amly school image"
            style={{
              width: 120,
              height: 120,
              position: "relative",
              top: "-10px",
            }}
          />
        </Col>
      </Row>

      {/* UnitPlan */}
      {props?.data?.sections &&
        props?.data?.sections?.map((section) => (
          <Row>
            <Table bordered>
              <thead>
                <tr>
                  <th className="ListHeading">
                    <p className={styles.colorTitle}> {section?.name}</p>
                    <p className={styles.RPrintDescription}>
                      {section?.description}
                    </p>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    {section?.items?.map((item, index) => (
                      <>
                        <Sections item={item} index={index + 1} level={1} />
                      </>
                    ))}
                  </td>
                </tr>
              </tbody>
            </Table>
          </Row>
        ))}

      {/* Rubric */}
      {props?.data?.categories && (
        <RubricPrinting data={props?.data?.categories} />
      )}

      {/* Lesson Plan */}

      {props?.data?.lesson_plan && (
        <LessonPlanPrinting data={props?.data?.lesson_plan} />
      )}
      {/* Abuse Report */}
      {props?.abuseId && <AbuseReportPrinting data={props.data} />}
    </div>
  );
});

export default RPrinting;
