
import React,{useState,useEffect} from 'react';
import {getActiveNodePath,buildTreeRecursively} from './utils';
import propTypes from 'prop-types';

let level = 0;

const Tree = ({ siteMap,setActiveNode,activeNodeId ,linkPrefix,nodeClicked}) => {

   
    const [collapsedNodes,setCollapsesNodes] = useState([]);

    useEffect(()=>{
        const activeNodePath = getActiveNodePath(activeNodeId,siteMap)
        setCollapsesNodes([...activeNodePath,siteMap.id]);

    },[])


    const addCollapsedNode = (node_id)=>{
        let index = collapsedNodes.indexOf(node_id);
        if(index < 0){
            setCollapsesNodes([...collapsedNodes,node_id]);
            return;
        }
        const updatedCollapsedNodes = [...collapsedNodes];
        updatedCollapsedNodes.splice(index);

        setCollapsesNodes(updatedCollapsedNodes);
    };

    return (
        <div className="tree" style={{ '--my-color-var': '#ffff' }}>
            <ul style={{
                top: '10%',
                listStyleType:'none'
            }}>
                <li>
                    {buildTreeRecursively(siteMap,level,setActiveNode,collapsedNodes,addCollapsedNode,activeNodeId,linkPrefix,nodeClicked)}
                </li>
            </ul>
        </div>);
};



Tree.propTypes = {
    siteMap: propTypes.object.isRequired,
    setActiveNode: propTypes.func.isRequired,
    activeNodeId: propTypes.string.isRequired
}

export default Tree;
