import React, { useContext, useState } from "react";
import { primaryColor, boldGreyColor, warningColor, successColor, dangerColor, lightGray } from "config/constants";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import { relativeDate } from "utils/dateUtil";
import RQPaginator from "components/Global/RComs/RQPaginator/RQPaginator";
import Hands_Give from "assets/img/new/Hands_Give.png";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import moment from "moment";
import styles from "../../ROrganization.module.scss";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const RDiscountList = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);
	const newDate = moment();
	const [page, setPage] = useState(1);

	const handleChangePage = (pageNumber) => {
		setPage(pageNumber);
	};

	if (PaymentPolicyData.discountLoading) return <Loader />;

	return (
		<RFlex className="mt-3 mb-3" styleProps={{ flexDirection: "column" }}>
			<RFlex className={styles.div__scroll + " scroll_hidden"} styleProps={{ flexDirection: "column" }}>
				{PaymentPolicyData.discountData?.data?.data?.data?.data?.length > 0 ? (
					PaymentPolicyData.discountData?.data?.data?.data?.data?.map((item) => (
						<RFlex
							key={item.id}
							className={
								PaymentPolicyData.values.discountIdSelected == item.id + item.discountType ? styles.card__list__selected : styles.card__list
							}
							onClick={(event) => {
								event.stopPropagation();
								PaymentPolicyData.tabType == "archive" ? {} : PaymentPolicyData.handleSelectDiscountId(item.id + item.discountType, item);
							}}
						>
							<RFlex styleProps={{ gap: 0, width: "100%" }}>
								<RFlex className={styles.card__discount}>
									<span className={styles.card__discount__amount}>
										<RFlex styleProps={{ gap: 3 }}>
											<RFlex styleProps={{ gap: 0 }}>
												<span>{item.value}</span>
												<span>%</span>
											</RFlex>
										</RFlex>
									</span>
								</RFlex>
								<RFlex styleProps={{ flexDirection: "column", width: "85%", padding: "7px 10px" }}>
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<RFlex styleProps={{ gap: 5, color: boldGreyColor, fontSize: "14px" }}>
											<RFlex className={styles.heighLight__hover} styleProps={{ gap: 3, fontWeight: "bold" }}>
												<span>{item.value}%</span>
												<span>{tr`off`} </span>
											</RFlex>
											<div>
												{item?.discountType == "General" ? (
													<RTextIcon
														text={tr`the_whole_organization`}
														icon={iconsFa6.paperclip}
														flexStyle={{ gap: 5, color: primaryColor }}
													/>
												) : (
													<RTextIcon
														text={item?.numberOfItems == 0 ? tr`not_assigned_yet` : `${item?.numberOfItems} ${tr`courses`}`}
														icon={iconsFa6.paperclip}
														flexStyle={{ gap: 5, color: boldGreyColor }}
													/>
												)}
											</div>
										</RFlex>
										<div style={{ color: primaryColor }}>{item?.discountType == "General" ? <i className={iconsFa6.earthAsia} /> : ""}</div>
									</RFlex>
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<RFlex className={styles.date}>
											<span style={{ color: lightGray }}>
												{tr`start_at`}&nbsp;
												{moment(item.startDate).format("L")}
											</span>
											<span style={{ color: lightGray }}>
												{tr`expired_at`}&nbsp;
												{moment(item.expireDate).format("L")}
											</span>
											<span style={{ fontSize: "12px" }}>
												{newDate.isAfter(moment(item.expireDate), "day") ? (
													<span style={{ color: dangerColor }}>{tr`expired`}</span>
												) : moment.duration(moment(item.expireDate).diff(newDate)).asHours() < 24 ? (
													<span style={{ color: warningColor }}>24 {tr`hours_left`}</span>
												) : (
													<span style={{ color: successColor }}> {relativeDate(item.expireDate)}</span>
												)}
											</span>
										</RFlex>
									</RFlex>
								</RFlex>
							</RFlex>
						</RFlex>
					))
				) : (
					<REmptyData Image={Hands_Give} line1={tr`no_discounts_yet`} />
				)}
			</RFlex>

			{/* RQPaginator */}
			{PaymentPolicyData.discountData?.data?.data?.data?.data &&
				PaymentPolicyData.discountData?.data?.data?.data?.paginate.last_page !== 1 && (
					<RQPaginator info={PaymentPolicyData.discountData} handleChangePage={handleChangePage} page={page} />
				)}
		</RFlex>
	);
};

export default RDiscountList;
