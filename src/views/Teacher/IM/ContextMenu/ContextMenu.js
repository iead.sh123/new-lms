import { RMenu } from "components/Global/RComs/RMenu";
import React from "react";
import { UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./ContextMenu.module.scss";
import downArrow from "../../../../assets/img/BlackdownArrow.svg";
const ContextMenu = ({
	class1,
	actions = [],
	style = { color: "black", margin: "4px" },
	padding = "1px",
	stopPropagation,
	//class = "fa fa-angle-down" ,
	icon,
}) => {
	// return <RMenu actions={actions}></RMenu>;
	return (
		<UncontrolledDropdown direction="right" onClick={(event) => (stopPropagation ? event.stopPropagation() : "")}>
			<DropdownToggle
				aria-haspopup={true}
				color="default"
				data-toggle="dropdown"
				nav
				style={{ padding: "0px", display: "flex", justifyContent: "center", alignItems: "center" }}
			>
				{icon ? (
					<div style={{ height: "16px", display: "flex", justifyContent: "start", alignItems: "center", cursor: "auto" }}>
						<img src={icon} style={{ cursor: "pointer" }} width={16} height={16} />
					</div>
				) : (
					<div className={styles.ActionCircle}>
						<img src={downArrow} width={9} height={12} />
					</div>
				)}
			</DropdownToggle>
			<DropdownMenu
				persist
				aria-labelledby="navbarDropdownMenuLink"
				right
				className="mb-1 start-n80"
				style={{ minWidth: "50px", left: "-160%" }}
			>
				{actions.map((ac) => (
					<DropdownItem id={`ctm_${ac.label}`} key={`ctm_${ac.label}`} onClick={ac.action} className="p-1">
						{ac.icon && <i className={`mr-2 ${ac.icon}`} aria-hidden="true" style={{ color: ac.color }}></i>}
						{tr(ac.label)}
					</DropdownItem>
				))}
			</DropdownMenu>
		</UncontrolledDropdown>
	);
};

export default ContextMenu;
