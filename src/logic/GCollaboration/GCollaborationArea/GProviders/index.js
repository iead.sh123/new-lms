import React from "react";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RProviders from "view/Collaboration/CollaborationArea/RProviders";
import Loader from "utils/Loader";

const GProviders = () => {
	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["providers"],
		queryFn: () => collaborationApi.getProviders(),
	});

	if (isLoading) return <Loader />;

	return <RProviders />;
};

export default GProviders;
