import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSingleAbuseAsync } from "store/actions/global/abuse.action";
import { useReactToPrint } from "react-to-print";
import { useParams } from "react-router-dom";
import withDirection from "hocs/withDirection";
import RPrinting from "../RComs/RPrinting/RPrinting";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";

const GAbusePrint = ({ dir }) => {
  const dispatch = useDispatch();
  const { abuseId } = useParams();

  const { singleAbuse, arrayUsers, single_Abuse_Loading } = useSelector(
    (state) => state.abuseServiceRed
  );

  useEffect(() => {
    dispatch(getSingleAbuseAsync(abuseId));
  }, []);

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div className="content">
      {single_Abuse_Loading ? (
        <Loader />
      ) : (
        <>
          <RButton
            text={tr`print`}
            onClick={handlePrint}
            color="info"
            outline
          />
          <RPrinting
            data={singleAbuse}
            ref={componentRef}
            dir={dir}
            abuseId={abuseId}
          />
        </>
      )}
    </div>
  );
};

export default withDirection(GAbusePrint);
