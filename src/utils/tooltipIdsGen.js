const idsGenerator = (function *(){
    let i = 0;
    while(true){
        yield 'tool--'+ ++i;
    }
})();

export default idsGenerator;