import React, { useState, useReducer, useEffect } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { get, destroy, post } from "config/api";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import AddUsersToRole from "./AddUsersToRole";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../GUsersAndPermissions.module.scss";
import { primaryColor } from "config/constants";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { getUsersTypesByOrganizationAsync } from "../State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const UsersToRoleLister = () => {
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	const { roleId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [openModal, setOpenModal] = useState(false);
	const [alert, setAlert] = useState(false);
	const [payloadData, setPayloadData] = useState({
		role_id: roleId,
		organization_users_ids: [],
	});
	const [addUsersToRoleLoading, setAddUsersToRoleLoading] = useState(false);

	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	useEffect(() => {
		// getUsersTypesByOrganizationAsync(dispatch);
		getUsersTypesByOrganizationAsync({
			dispatch: dispatch,
		});
	}, []);

	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = async (emptySearch) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization_user/usersRole/${roleId}?filter=${
			emptySearch ?? searchData
		}`;
		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
		}
	};

	const handleFilterOnUserType = async (usersType) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization_user/usersRole/${roleId}${
			searchData ? `filter=${searchData}&user_type_id=${usersType.name}` : `?user_type_id=${usersType.name}`
		}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
			toast.error(response.data.msg)
0		}
	};

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/organization_user/usersRole/${roleId}`;
		let response = await get(specific_url ? specific_url : url);
		if (response?.data?.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		const data = response?.data?.data?.data.map((r) => ({
			id: r?.id,

			table_name: "usersByRoleLister",

			details: [
				{
					key: tr`name`,
					value: {
						name: r.name,
						image: Services.auth_organization_management.file + r?.hash_id,
					},
					render: renderInfo,
				},
				{ key: tr`email`, value: r.email },
				{
					key: tr`user_type`,
					value: r.user_type,
					dropdown: true,
					dropdownData: usersAndPermissionsData.usersTypes,
					onClick: (usersTypeId) => {
						handleFilterOnUserType(usersTypeId);
					},
				},
			],
			actions: [
				{
					name: tr("delete"),
					icon: iconsFa6.delete,
					color: "danger",
					outline: true,
					onClick: () => {
						handleRemove(r.id);
					},
				},
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords]
	);

	const successDelete = async (prameters) => {
		const data = {
			role_id: roleId,
			organization_users_ids: [prameters.id],
		};
		const response = await post(`${Services.auth_organization_management.backend}api/role/organizationUsersFromRole`, data);

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					setAlert(null);
				} else {
					toast.error(response.data.msg)
				}
			}
		} else {
			toast.error(response.data.msg)
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleAddUsersToRole = async () => {
		setAddUsersToRoleLoading(true);
		const response = await post(`${Services.auth_organization_management.backend}api/role/addOrganizationUsersToRole`, payloadData);

		if (response) {
			if (response.data.status == 1) {
				setAddUsersToRoleLoading(false);
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					handleCloseModal();

					setPayloadData({
						role_id: roleId,
						organization_users_ids: [],
					});
				} else {
					toast.error(response.data.msg)
				}
			}
		} else {
			setAddUsersToRoleLoading(false);
			toast.error(response.data.msg)
		}
	};
	return (
		<React.Fragment>
			{alert}
			<AppModal
				show={openModal}
				parentHandleClose={handleCloseModal}
				header={
					<RFlex styleProps={{ alignItems: "center" }}>
						<span>
							{payloadData.organization_users_ids.length} {tr`add_users`}
						</span>
						{addUsersToRoleLoading ? (
							<i className="fa fa-refresh fa-spin"></i>
						) : payloadData.organization_users_ids.length > 0 ? (
							<span
								className={styles.btn_hover}
								style={{
									color: primaryColor,
									textDecoration: "underline",
									cursor: "pointer",
								}}
								onClick={() => {
									handleAddUsersToRole();
								}}
							>
								{tr`save`}
							</span>
						) : (
							""
						)}
					</RFlex>
				}
				size={"lg"}
				headerSort={<AddUsersToRole payloadData={payloadData} setPayloadData={setPayloadData} />}
			/>
			{usersAndPermissionsData.usersTypesLoading ? (
				<Loader />
			) : (
				<section className={"p-4" + styles.box}>
					<RSearchHeader
						searchLoading={searchLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleOpenModal={handleOpenModal}
						handleChangeSearch={handleChangeSearch}
						buttonName={tr`add_users`}
						addNew={true}
					/>
					<RAdvancedLister {...propsLiterals.listerProps} />
				</section>
			)}
		</React.Fragment>
	);
};

export default UsersToRoleLister;
