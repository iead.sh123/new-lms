import withDirection from "hocs/withDirection";
import React, { useState } from "react";
import { Col, Row } from "reactstrap";
import Helper from "../Helper";
import RButton from "../RButton";
import RStep from "./RStep";
import styles from "./RStepper.Module.scss";

const RStepper = ({ steps, dir }) => {
  const [selectedStep, setSelectedStep] = useState([0]);

  const _prev = () => {
    const step = getCurrentStep(steps, 0);
    if (!step.hasPrev) {
      return;
    }
    let updatedSelectedStep = [...selectedStep]
    const last_index = updatedSelectedStep.length - 1;

    if (updatedSelectedStep[last_index] === 0 || (step.currentIndex === 0 && steps[selectedStep[0] - 1]?.steps?.length)) {
      // let parentStepStepsLength = step.currentIndex > 0 ? getCurrentStep(steps,updatedSelectedStep.splice(updatedSelectedStep.length -1),0)?.steps.length : steps.length;
      if (step.currentIndex === 0 && steps[selectedStep[0] - 1]?.steps.length) {
        updatedSelectedStep[1] = steps[selectedStep[0] - 1]?.steps.length - 1;
        updatedSelectedStep[0]--;
      }
      else {
        updatedSelectedStep.pop();
      }
      setSelectedStep(updatedSelectedStep);
      return;
    }

    updatedSelectedStep[last_index]--;
    setSelectedStep(updatedSelectedStep);

  }

  const _next = () => {
    const step = getCurrentStep(steps, 0);
    let updatedSelectedStep = null;
    if (!step.hasNext) {
      return;
    }

    if (step.currentIndex === 1 && steps[selectedStep[0]]?.steps.length - 1 === selectedStep[1] && steps.length - 1 > selectedStep[0]) {

      updatedSelectedStep = [...selectedStep];
      updatedSelectedStep.pop();
      updatedSelectedStep[0]++;

      setSelectedStep(updatedSelectedStep)
      return;
    }

    if (step.currentStep?.steps?.length > 0) {
      setSelectedStep([...selectedStep, 0]);

      return;
    }

    updatedSelectedStep = [...selectedStep];


    updatedSelectedStep[selectedStep.length - 1]++;

    setSelectedStep(updatedSelectedStep);
  }

  //steps[1].steps[2].steps[3]

  const getCurrentStep = (_steps, currentIndex, limit = null) => {
    if (!_steps) {
      return null;
    }
    (!selectedStep) && (selectedStep = selectedStep)

    if (currentIndex >= selectedStep.length - 1 || (limit && currentIndex <= limit)) {
      return {
        currentStep: _steps[selectedStep[currentIndex]], currentIndex: currentIndex,
        hasNext: (_steps.length - 1 > selectedStep[currentIndex] ||
          _steps[selectedStep[currentIndex]]?.steps?.length > 0 ||
          currentIndex === 1 && steps.length - 1 > selectedStep[0])

        , hasPrev: (selectedStep[currentIndex] > 0 || selectedStep.length > 1)
      };

    }

    return getCurrentStep(_steps[selectedStep[currentIndex]]?.steps, currentIndex + 1);
  }

  return (
    <Row className={styles.row + ` content`}>
      <Col className={styles.col} md={3} xs={12}>
        <RStep steps={steps} selected={getCurrentStep(steps, 0)?.currentStep?.num}></RStep>
      </Col>
      <Col className={styles.col} md={8} xs={12}>
        <Row
          class="StepperContainer"
          style={{
            width: "100%",
            height: "100%",
            position: "relative",
            top: "65px",
          }}
        >
          {getCurrentStep(steps, 0)?.currentStep?.View()}
        </Row>
        <Row>
          <div className={styles.rowButton}>
            <Col md={6}>
              <RButton
                className={"btn-round"}
                color="info"
                outline
                text={dir ? "Previous" : "Next"}
                onClick={dir ? _prev : _next}
              ></RButton>
            </Col>
            <Col md={6}>
              <RButton
                className={"btn-round"}
                color="info"
                text={dir ? "Next" : "Previous"}
                onClick={dir ? _next : _prev}
              ></RButton>
            </Col>
          </div>
        </Row>
        {/* {selectedStep.num} */}
      </Col>
    </Row>
  );
};

export default withDirection(RStepper);


// hasNext: (_steps.length - 1 > selectedStep[currentIndex] ||
//   _steps[selectedStep[currentIndex]]?.steps?.length > 0 ||

//   (currentIndex === 1 && steps.length - 1 > selectedStep[0] ||
// currentIndex === 1 && steps[selectedStep[0]]?.steps.length - 1 === selectedStep[1] && steps.length - 1 > selectedStep[0]


//     currentIndex > 1 && getCurrentStep(steps, 0, currentIndex - 2)?.steps.length - 1 > selectedStep[currentIndex - 2]))