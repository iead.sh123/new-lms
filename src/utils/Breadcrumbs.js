import React from 'react';
import { Link } from 'react-router-dom';
import { baseUrl } from '../config/constants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';

const Breadcrumb = ({ title, parents }) => {
    return (
        <div className="container-fluid">
            <div className="row justify-content-center">
                <nav aria-label="breadcrumb" className="theme-breadcrumb">
                    <ul className="breadcrumb">
                        <li className="breadcrumb-item">
                            <Link to={baseUrl + 'teacher/dashboard'}>
                                <FontAwesomeIcon icon={faHome} style={{ color: 'black' }} />
                            </Link>
                        </li>
                        {parents && parents
                            ? parents.map((item, index) => {
                                  return (
                                      <li
                                          className="breadcrumb-item"
                                          aria-current="page"
                                          key={index}>
                                          <Link to={baseUrl + item.url}>{item.parent}</Link>
                                      </li>
                                  );
                              })
                            : ''}

                        <li className="breadcrumb-item" aria-current="page">
                            {title}
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    );
};

export default Breadcrumb;
