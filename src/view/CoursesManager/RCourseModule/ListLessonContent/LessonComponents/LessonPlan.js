import React, { useContext } from "react";
import { baseURL, genericPath } from "engine/config";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { primaryColor } from "config/constants";
import { useHistory } from "react-router-dom";
import { Row, Col } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { useParams } from "react-router-dom";
import iconsFa6 from "variables/iconsFa6";

const LessonPlan = ({ viewMode, lessonContent, content }) => {
	const history = useHistory();
	const CourseModuleData = useContext(CourseModuleContext);
	const { courseId, cohortId } = useParams();

	return (
		<Row className="text-center mt-4" style={{ color: primaryColor }}>
			<Col
				xs={12}
				sm={4}
				onClick={() => {
					history.push(`${baseURL}/${genericPath}/collaboration_area_contents/lessons/${content.content_id}/lesson_plans/1`);
				}}
			>
				<div>
					<i className="fa fa-globe fa-2x mb-3" />
					<p>{tr`pick_from_collaboration`}</p>
				</div>
			</Col>
			<Col xs={12} sm={4}>
				<div onClick={() => CourseModuleData.handleOpenLessonPlanModal(content.content_id ?? content.id, content)}>
					<i className="fa fa-list-ul fa-2x mb-3" />
					<p>{tr`pick_from_this_course`}</p>
				</div>
			</Col>
			<Col
				xs={12}
				sm={4}
				onClick={() => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/lesson/${content.content_id}/editor/lesson-plans`
					);
				}}
			>
				<div>
					<i className="fa fa-plus fa-2x mb-3" />
					<p>{tr`create_new`}</p>
				</div>
			</Col>
		</Row>
	);
};

export default LessonPlan;
