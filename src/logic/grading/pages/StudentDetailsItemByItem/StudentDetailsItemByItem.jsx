import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFormItem from "components/Global/RComs/RFormItem";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import moment from "moment";
import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RGradesTable from "view/grading/RGradesTable/RGradesTable";
import RTable from "view/grading/RTable/RTable";
import { Input } from "reactstrap";
import Helper from "components/Global/RComs/Helper";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";

const StudentDetailsItemByItem = ({ itemStudents,goPreviousItem,
	currentGroup,
	currentItem,
	goNextItem,
	hasNextItem,
	hasPreviousItem,

	setStudentItemMark,
	batchStudentItemMark
}) => {
	const history=useHistory();
	const {courseId}=useParams();
	const headers = [
		{ inForm: false, disabled: true, label: "Student",    type: "User"  , value: "student" },
		{ inForm: false, disabled: true, label: "Mark",       type: "number", value: "calculatedFinalGrade" },
		{ inForm: true , disabled: true, label: "Final Mark", type: "number", value: "calculatedFinalTeacherGrade" },
	];

	const [editedId, setEditedId] = React.useState(null);
	const [formItems, setFormItems] = React.useState({ calculatedFinalTeacherGrade: "11" });

	const handleFormItemChange = (e, name) => {

		Helper .cl(e,"t6 handleFormItemChange e" );
		Helper.cl(name,"t6 handleFormItemChange name");

		setStudentItemMark(editedId,currentItem.id,e.target.value);
		setFormItems({
			...formItems,
			[name]: headers?.find((h) => h.value === name)?.type === "number" ? +e.target.value : e?.target?.value ?? e,
		});
	};

	React.useEffect(() => {
		setFormItems(
			headers.filter((h) => h.inForm).reduce((prev, curr) => ({ ...prev, [curr.value]: itemStudents?.byId?.[editedId]?.[curr.value] }), {})
		);
	}, [editedId]);

	const handleEdit = (editedId) => setEditedId(editedId);
	const saveGrade =async (id) => 
	{

		const ff=itemStudents.students.byId[id].calculatedFinalTeacherGrade;
		Helper.cl(ff,"ff");
		const res=await batchStudentItemMark(currentItem.id.split("_")[0] , currentItem.id.split("_")[1],id,ff)
		Helper.cl(res,"res");
		if(res.data.status==1)
			setEditedId(null);
	}

	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const renderFormInfo = (attr) => 
	
	{
		
		Helper.cl(itemStudents?.students?.byId?.[editedId],"itemStudents?.students?.byId?.[editedId]")
		return(
		<Input name={attr} 
		value={itemStudents?.students?.byId?.[editedId]?.[attr]} 
		onChange={(e) =>
			{
				 Helper.cl(e,"t6 handleFormItemChange e")
				handleFormItemChange(e, attr)
			
			}} />
	)};

	const onRecordClick = () => {};

	const records = itemStudents.students?.ids?.map((sId) => ({
		table_name: "itemStudentsDetails",
		id: sId,
		details: headers?.map((header) => ({
			key: tr(header.label),
			value:
				header.type == "Date"
					? moment(itemStudents.students?.byId?.[sId]?.[header.value]).format(DATE_FORMATE)
					: header.type == "User"
					? renderInfo({
							name: itemStudents.students?.byId?.[sId]?.[header.value].full_name,
							image: Services.auth_organization_management.file + itemStudents.students?.byId?.[sId]?.[header.value].hash_id,
					  })
					: editedId === sId && header.inForm
					? renderFormInfo(header.value)
					: itemStudents.students?.byId?.[sId]?.[header.value],
			onClick: () => (header.clickable ? onRecordClick(header.value, itemStudents.students?.byId?.[sId]) : null),
			type: "component",
			// dropdown: Object.keys(sortRecords).includes(header.label),
			// dropdownData: sortRecords?.[header.label] ?? [],
		})),
		actions: (editedId==sId)?[{
			icon: iconsFa6.pen,
			name: tr`Edit`,
			onClick: () => saveGrade(sId),
		}]: [
			{
				icon: iconsFa6.pen,
				name: tr`Edit`,
				onClick: () => handleEdit(sId),
			},
			{
				icon: iconsFa6.user,
				name: tr`Mark Details`,
				onClick: () => {
					history.push(`${process.env.REACT_APP_BASE_URL}/g/course-management/course/${courseId}/cohort/:cohortId/gradebook/student-item-details/${currentItem.id}/${sId}`);
			
					
					
				},
			},
			{
				icon: iconsFa6.delete,
				name: tr`Delete`,
				onClick: () => {},
			},
		],
	}));

	/*
        records:   {
            table_name: keyPrefix,
          id: id,
    details: id === editedId ? Object.keys(formItems)?.map((item) => ({
            key: tr(item),
            value: formItems[item],
            type: headers?.find(h => h.value === item)?.type,
            disabled: headers?.find(h => h.value === item)?.disabled || disabledFieldsForEdit.includes(item),
    
            option: headers?.find(h => h.value === item)?.options,
            onChange: e => handleFormItemChange(e, item),
            name: item,
            mode: 'form'
          })
        }
        */

	return (
		<React.Fragment>
			<RFlex>
				{Helper.js(courseId,"courseId")}
		{/* {Helper.js(itemStudents,"itemStudents")}
		{currentGroup.name+">"+currentItem.name} */}


		<div>	
			<button onClick={goPreviousItem}>Previous item</button>
			<button onClick={goNextItem}>Next item</button>
		</div>

			</RFlex>
			<RTable headers={headers} Records={records} />
		</React.Fragment>
	);
};

export default StudentDetailsItemByItem;
