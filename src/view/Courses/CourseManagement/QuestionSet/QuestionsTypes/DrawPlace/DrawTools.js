import React from "react";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";

const DrawTools = ({ canvasRef, brushColor, brushSize, setBrushColor, setBrushSize }) => {
	return (
		<RFlex styleProps={{ justifyContent: "space-between" }} className="canvas_draw_tools">
			<div>
				<input type="color" className="" value={brushColor} onChange={(e) => setBrushColor(e.target.value)} />
				<input type="range" className="form-range" min="1" max="15" value={brushSize} onChange={(e) => setBrushSize(+e.target.value)} />
			</div>
			<div>
				<RButton text={tr`clear`} color="link" faicon={iconsFa6.delete} onClick={() => canvasRef.current.clear()} />
				<RButton text={tr`undo`} color="link" faicon="fas fa-undo" onClick={() => canvasRef.current.undo()} />
			</div>
		</RFlex>
	);
};

export default DrawTools;
