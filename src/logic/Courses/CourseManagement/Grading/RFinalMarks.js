import React, { useContext, useState } from 'react'
import { GradingContext } from './GGradingManagement'
import RLister from 'components/Global/RComs/RLister'
import Loader from 'utils/Loader'
import iconsFa6 from 'variables/iconsFa6'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import Helper from 'components/Global/RComs/Helper'
import { tablesTypes } from './Constants'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import tr from 'components/Global/RComs/RTranslator'
import * as colors from 'config/constants'
import { baseURL, genericPath } from "engine/config";
import styles from './grading.module.scss'
import { editableFields } from './Constants'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import RIconDiv from 'components/Global/RComs/RIconDiv/RIconDiv'
const RFinalMarks = () => {
  const gradingData = useContext(GradingContext)
  console.log('finalMarks', gradingData?.gradingState?.finalMarks)
  const finalMarks = gradingData?.gradingState?.finalMarks
  const _finalMarksRecords = !gradingData.isLoadingFinalMarks ? finalMarks?.ids?.map((recordId) => {
    const currentRecord = finalMarks?.[recordId]
    console.log("FinalMarks", finalMarks)
    return {
      id: recordId,
      table_name: tablesTypes.finalMarks,
      details: Object.keys(currentRecord)?.map((header) => {
        const studentComponent = <RProfileName
          flexStyleProps={{ cursor: 'pointer' }}
          onClick={() => {
            gradingData.history.push({
              pathname: `${baseURL}/${genericPath}/course-management/course/${gradingData.courseId}/cohort/${gradingData.cohortId}/gradebook/studentDetails/${finalMarks.studentsIds[recordId]}`,
              state: { from: "FinalMarks" }
            })
          }}
          name={currentRecord['Students'].value.name}
          img={currentRecord['Students'].value.image} />
        const currentHeader = currentRecord?.[header]
        console.log("currentHeader", currentHeader)
        const editable = editableFields.includes(header)
        let dropdownOptions = []
        let onClick = null
        dropdownOptions = header == "Students" ? [{ name: "alphabiticallyAsc", title: "Alphabetically" }]
          : [{ name: "markAsc", title: "Order Ascending" }, { name: "markDesc", title: "Order Descending" }]
        return {
          key: header,
          dropdown: header == "Students" || header == "Final Mark" ? true : null,
          dropdownData: dropdownOptions,
          onClick: (data) => {
            gradingData.setFinalMarksOrder(data.name)
          },
          value: editable ?
            <div className='position-relative'>
              <RHoverInput
                inputValue={currentHeader.value}
                handleInputDown={gradingData.handleInputSaved}
                inputWidth='100px'
                type={currentHeader.type}
                focusOnInput={true}
                inputPlaceHolder=""
                handleInputChange={gradingData.handleInputChange}
                inputIsNotValidate={!currentHeader.value && currentHeader.touched}
                item={currentRecord}
                extraInfo={{ id: recordId, type: tablesTypes.finalMarks, inputLabel: header }}
                handleOnBlur={gradingData.handleInputSaved}
                handleEditFunctionality={gradingData.handleEditFunctionality}
                saved={currentHeader.saved}
              />
              {!currentHeader.value && currentHeader.touched ?
                <i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} /> : ''}
            </div>
            : header == "Students" ? studentComponent : currentHeader.value,
          type: editable ? 'component' : header == "Students" ? 'component' : ''
        }
      }),

    }
  }) : null
  return gradingData.isLoadingFinalMarks ? <Loader /> :
    <RFlex className="flex-column" styleProps={{ gap: '0px' }}>
      <RFlex className='align-items-center'>
        <span className='p-0 m-0'>{tr("Students_Final_Marks")}</span>
        <RIconDiv
          text={finalMarks?.ids?.length}
          divStyle={{
            borderRadius: '100%', width: '19px', height: '19px',
            fontSize: '12px', cursor: 'default', margin: '0px'
          }} />
        <RButton
          text={gradingData.calculated ? tr('calculated') : tr('calculate')}
          onClick={gradingData.calculated ? "" : gradingData.calculateFinalMarks}
          faicon={iconsFa6.calculator}
          color={gradingData.calculated ? "success" : "primary"}
          disabled={!finalMarks?.ids ? true : false}
          style={{ width: 'fit-content' }}
          loading={gradingData.isCalculating}
        />
      </RFlex>

      <RLister Records={_finalMarksRecords} center={true} />
    </RFlex>

}

export default RFinalMarks