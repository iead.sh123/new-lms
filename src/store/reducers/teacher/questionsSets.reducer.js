import produce from "immer";
import {
  GET_PATTERN_REQUEST,
  GET_PATTERN_SUCCESS,
  GET_PATTERN_ERROR,
  GET_STUDENT_DETAILS_REQUEST,
  GET_STUDENT_DETAILS_SUCCESS,
  GET_STUDENT_DETAILS_ERROR,
  POST_STUDENT_DETAILS_REQUEST,
  POST_STUDENT_DETAILS_SUCCESS,
  POST_STUDENT_DETAILS_ERROR,
  UPDATE_STUDENTS_DETAIL_FIELDS,
  SET_STUDENT_ID,
  SET_QUESTION_SET_ID,
  GET_QUESTION_SETS_REQUEST,
  GET_QUESTION_SETS_SUCCESS,
  GET_QUESTION_SETS_ERROR,
  DELETE_QUESTION_SET_REQUEST,
  DELETE_QUESTION_SET_SUCCESS,
  DELETE_QUESTION_SET_ERROR,
  GET_ALL_COURSES_REQUEST,
  GET_ALL_COURSES_SUCCESS,
  GET_ALL_COURSES_ERROR,
  GET_TOPICS_BY_COURSE_ID_SUCCESS,
  GET_TOPICS_BY_COURSE_ID_ERROR,
  GET_SINGLE_COURSE_QS_REQUEST,
  GET_SINGLE_COURSE_QS_SUCCESS,
  GET_SINGLE_COURSE_QS_ERROR,
  FILL_QS_INFO,
  GET_SELECTED_PATTERN_INFO,
  GET_ALLOWED_APPS_SUCCESS,
  GET_ALLOWED_APPS_ERROR,
  EDIT_SETTINGS,
  QS_PATTERN_SELECT_SPECIFIC_GROUP,
  UPDATE_GROUP_NAME,
  QS_PATTERN_Delete_GROUP,
  CLEAR_QS_QLLOWED_APPS,
  CLEAR_QS_COURSES,
  QS_PATTERN_ADD_GROUP,
  GET_SELECTED_GROUP_ID,
  GET_SELECTED_PATTERN_ID,
  GET_SELECTED_TOPIC,
  QS_PATTERN_ADD_TOPIC_TO_GROUP,
  DELETE_TOPIC_FROM_GROUP,
  QS_DELETE_PATTERN,
  NOT_SAVE_EDIT,
  ADD_NEW_PATTERN,
  UPDATE_QS_VALUE,
  CREATE_COPY_QS_VALUE,
  UPDATE_COURSES_VALUE,
  SAVE_QS_REQUEST,
  SAVE_QS_ERROR,
  CREATE_QUESTION_SETS_COPY_REQUEST,
  CREATE_QUESTION_SETS_COPY_ERROR,
  CALC_TOTAL_GRADE_GROUP,
  UPDATE_GROUP_GRADE_VALUE,
  UPDATE_TOPICS_GRADE_VALUE,
  CHANGE_PATTERN_NAME,
  GET_SELECTED_PATTERN_TOTAL_GRADE,
  EMPTY_STORE_QS,
  UPDATE_STUDENTS_MARK_FIELDS,
} from "../../actions/teacher/teacherTypes";
const initState = {
  CourseSingleQSet: [],
  coursesQSIdes: [],
  questionSet: [],
  qsAllowedApps: [],
  question_set_id: null,
  groups: [],
  selectedGroupText: null,
  selectedPatternId: null,
  selectedGroupId: null,
  selectedTopic: null,
  allowedApps: [],
  topics: [],
  allCourses: [],
  lessonGroups: [],
  lessonGroup: {},
  sebBrowserViolation: null,
  isLoader: false,
  studentDetailsLoading: false,
  updateStudentDetailsLoading: false,
  studentDetails: [],
  studentId: null,
  questionSetId: null,
  TotalMark: 0,
};
let temp = "";
let indexNew = "";
let indexPattern = "";
let indexGroup = "";
let topic = "";
let indexTopic = "";
let ind;
let newMark;
let newTotalMark;
let newTotalMarkFixed;
const questionSetsReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_QUESTION_SET_ID:
        draft.questionSetId = action.id;
        return draft;

      case SET_STUDENT_ID:
        draft.studentId = action.id;
        return draft;

      case UPDATE_STUDENTS_DETAIL_FIELDS:
        ind = draft.studentDetails.findIndex(
          (el) => el.id == action.temp.student_id
        );
        if (ind !== -1) {
          const student_information = draft.studentDetails[ind];
          student_information.feedback = action.temp.feedback;
          student_information.teacher_mark = action.temp.teacher_mark;
          student_information.detail.feedback = action.temp.questionFeedback;
        }

        return draft;

      case UPDATE_STUDENTS_MARK_FIELDS:
        ind = draft.studentDetails.findIndex(
          (el) => el.id == action.temp.student_id
        );
        if (ind !== -1) {
          newMark = action.temp.mark - draft.studentDetails[ind].detail.mark;

          newTotalMark = draft.studentDetails[ind].total_mark + newMark;

          const student_information = draft.studentDetails[ind];
          student_information.detail.mark = action.temp.mark;
          student_information.total_mark = newTotalMark;
        }

        return draft;

      case POST_STUDENT_DETAILS_REQUEST:
        draft.updateStudentDetailsLoading = true;
        return draft;

      case POST_STUDENT_DETAILS_SUCCESS:
        draft.updateStudentDetailsLoading = false;
        return draft;

      case POST_STUDENT_DETAILS_ERROR:
        draft.updateStudentDetailsLoading = false;
        return draft;

      case GET_STUDENT_DETAILS_REQUEST:
        draft.studentDetailsLoading = true;
        return draft;

      case GET_STUDENT_DETAILS_SUCCESS:
        draft.studentDetails = action.payload.StudentDetails;
        draft.studentDetailsLoading = false;
        return draft;

      case GET_STUDENT_DETAILS_ERROR:
        draft.studentDetailsLoading = false;
        return draft;

      case GET_PATTERN_REQUEST:
        draft.isLoader = true;
        return draft;

      case GET_PATTERN_SUCCESS:
        draft.lessonGroups = action.payload.groups;
        draft.sebBrowserViolation = action.payload.msg;
        draft.isLoader = false;
        return draft;

      case GET_PATTERN_ERROR:
        draft.isLoader = false;
        return draft;

      case GET_QUESTION_SETS_REQUEST:
        return {
          ...state,
          loading: true,
          isEdit: false,
        };

      case GET_QUESTION_SETS_SUCCESS:
        draft.coursesQSIdes = action.payload.data;
        draft.has_add_permission = action.payload.has_add_permission;
        draft.has_edit_permission = action.payload.has_edit_permission;
        draft.has_delete_permission = action.payload.has_delete_permission;
        draft.questionSet = [];
        draft.groups = [];
        draft.selectedGroupId = null;
        draft.selectedPatternId = null;
        draft.loading = false;
        return draft;

      case GET_QUESTION_SETS_ERROR:
        return {
          ...state,
          error: action.payload,
        };

      case DELETE_QUESTION_SET_REQUEST:
        return {
          ...state,
          loading: true,
        };

      case DELETE_QUESTION_SET_SUCCESS:
        draft.coursesQSIdes.data.splice(action.qs_id, 1);
        draft.loading = false;
        return draft;
      case DELETE_QUESTION_SET_ERROR:
        return {
          error: action.payload,
        };

      case GET_ALL_COURSES_REQUEST:
        return {
          ...state,
          loading: true,
        };

      case GET_ALL_COURSES_SUCCESS:
        return {
          ...state,
          loading: false,
          allCourses: action.payload,
        };

      case GET_ALL_COURSES_ERROR:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };

      case CREATE_QUESTION_SETS_COPY_REQUEST:
        return {
          ...state,
        };

      case CREATE_QUESTION_SETS_COPY_ERROR:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };

      case GET_SINGLE_COURSE_QS_REQUEST:
        return {
          ...state,
          loading: true,
        };

      case GET_SINGLE_COURSE_QS_SUCCESS:
        draft.CourseSingleQSet = [action.payload.objList];
        draft.loading = false;
        return draft;

      case FILL_QS_INFO:
        draft.groups = [];
        temp = state.CourseSingleQSet[0];
        draft.questionSet = temp;
        draft.question_set_id = action.qs_id;
        draft.qsAllowedApps = temp.allowed_apps;
        return draft;

      case CALC_TOTAL_GRADE_GROUP:
        if (state.groups) {
          let arryTotalGroupGrade = 0;
          for (let i = 0; i < state.groups.topics.length; i++) {
            arryTotalGroupGrade += state.groups.topics[i].pivot.topic_grade;
          }
          draft.groups.totalgroup_grade = arryTotalGroupGrade;
        }
        return draft;

      case GET_SINGLE_COURSE_QS_ERROR:
        return {
          ...initState,
          error: action.payload,
        };

      case SAVE_QS_REQUEST:
        return {
          ...state,
          saveLoading: true,
        };

      case SAVE_QS_ERROR:
        return {
          ...state,
          error: action.payload,
          saveLoading: false,
        };

      case CREATE_COPY_QS_VALUE:
        draft.questionSet = action.questionSet;
        draft.question_set_id_copy = action.questionSet.id
          ? action.questionSet.id
          : action.questionSet.idTemp;
        //draft.isEdit = true;
        draft.groups = [];
        draft.question_set_id = null;
        draft.selectedPatternId = null;
        draft.selectedTopic = null;
        draft.selectedGroupId = null;
        return draft;

      case GET_SELECTED_PATTERN_INFO:
        return draft;

      case GET_ALLOWED_APPS_SUCCESS:
        draft.allowedApps = action.data;
        return draft;

      case GET_ALLOWED_APPS_ERROR:
        return {
          ...state,
          error: action.payload,
        };

      case EDIT_SETTINGS:
        indexNew = -1;
        for (let i = 0; i < draft.questionSet.allowed_apps.length; i++) {
          if (draft.questionSet.allowed_apps[i].id == action.app.id) {
            indexNew = i;
            break;
          }
        }
        if (indexNew == -1) {
          draft.questionSet.allowed_apps.push(action.app);
        } else {
          if (!action.checked)
            draft.questionSet.allowed_apps.splice(indexNew, 1);
          return draft;
        }
        return draft;

      case CLEAR_QS_QLLOWED_APPS:
        draft.questionSet.allowed_apps = [];
        return draft;

      case GET_TOPICS_BY_COURSE_ID_SUCCESS:
        return {
          ...state,
          topics: action.topics,
          topicPaginator: action.topicPaginator,
        };

      case GET_TOPICS_BY_COURSE_ID_ERROR:
        return {
          ...initState,
          error: action.payload,
        };

      case QS_PATTERN_SELECT_SPECIFIC_GROUP:
        draft.selectedGroupText = action.groupId;
        return draft;

      case QS_PATTERN_ADD_GROUP:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == action.patternId
            : pattern.idTemp == action.patternId
        );
        draft.questionSet.qs_patterns[indexPattern].pattern_groups.push(
          action.group
        );
        return draft;

      case UPDATE_GROUP_NAME:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        indexGroup = state.questionSet.qs_patterns[
          indexPattern
        ].pattern_groups.findIndex((group) =>
          group.id
            ? group.id == state.selectedGroupId
            : group.idTemp == state.selectedGroupId
        );
        if (action.group.type === "pattern_group_text") {
          draft.questionSet.qs_patterns[indexPattern].pattern_groups[
            indexGroup
          ].pattern_group_text = action.group.value;
          draft.groups.pattern_group_text = action.group.value;
          draft.questionSet.qs_patterns[indexPattern].pattern_groups[
            indexGroup
          ].isNew = action.group.isNew;
          draft.groups.isNew = action.group.isNew;
        } else {
          draft.questionSet.qs_patterns[indexPattern].pattern_groups[
            indexGroup
          ].skip = action.group.value;
          draft.groups.skip = action.group.value;
        }
        return draft;

      case UPDATE_GROUP_GRADE_VALUE:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        indexGroup = state.questionSet.qs_patterns[
          indexPattern
        ].pattern_groups.findIndex((group) =>
          group.id
            ? group.id == state.selectedGroupId
            : group.idTemp == state.selectedGroupId
        );

        draft.questionSet.qs_patterns[indexPattern].pattern_groups[
          indexGroup
        ].grade = action.value;
        draft.groups.grade = action.value;
        return draft;

      case UPDATE_TOPICS_GRADE_VALUE:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        indexGroup = state.questionSet.qs_patterns[
          indexPattern
        ].pattern_groups.findIndex((group) =>
          group.id
            ? group.id == state.selectedGroupId
            : group.idTemp == state.selectedGroupId
        );

        topic =
          state.questionSet.qs_patterns[indexPattern].pattern_groups[indexGroup]
            .topics;
        for (let i = 0; i < topic.length; i++) {
          draft.questionSet.qs_patterns[indexPattern].pattern_groups[
            indexGroup
          ].topics[i].pivot.topic_grade = parseInt(action.value);
          draft.groups.topics[i].pivot.topic_grade = parseInt(action.value);
        }
        return draft;

      case QS_PATTERN_Delete_GROUP:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        draft.questionSet.qs_patterns[indexPattern].pattern_groups.splice(
          action.index,
          1
        );
        if (state.selectedGroupId == action.groupId) {
          draft.groups = null;
          draft.selectedGroupId = null;
        }
        return draft;

      case GET_SELECTED_PATTERN_ID:
        draft.selectedPatternId = action.patternId;
        return draft;

      case GET_SELECTED_PATTERN_TOTAL_GRADE:
        for (let i = 0; i < state.questionSet.qs_patterns.length; i++) {
          let totalDegree = 0;
          if (state.questionSet.qs_patterns[i].pattern_groups.length != 0) {
            for (
              let g = 0;
              g < state.questionSet.qs_patterns[i].pattern_groups.length;
              g++
            ) {
              if (
                state.questionSet.qs_patterns[i].pattern_groups[g].topics
                  .length != 0
              ) {
                for (
                  let t = 0;
                  t <
                  state.questionSet.qs_patterns[i].pattern_groups[g].topics
                    .length;
                  t++
                ) {
                  totalDegree +=
                    state.questionSet.qs_patterns[i].pattern_groups[g].topics[t]
                      .pivot.topic_grade;

                  draft.questionSet.qs_patterns[i].totalGrade = totalDegree;
                }
                if (
                  state.questionSet.qs_patterns[i].pattern_groups[g].skip != 0
                ) {
                  const totalDegrees =
                    state.questionSet.qs_patterns[i].pattern_groups[g].skip *
                    state.questionSet.qs_patterns[i].pattern_groups[g].grade;
                  totalDegree -= totalDegrees;
                  draft.questionSet.qs_patterns[i].totalGrade = totalDegree;
                }
              } else {
                draft.questionSet.qs_patterns[i].totalGrade = totalDegree;
              }
            }
          } else {
            draft.questionSet.qs_patterns[i].totalGrade = totalDegree;
          }
        }

        return draft;

      case GET_SELECTED_GROUP_ID:
        action.group && action.group?.id
          ? (draft.selectedGroupId = action.group ? action.group.id : null)
          : (draft.selectedGroupId = action.group ? action.group.idTemp : null);
        draft.groups = action.group;
        return draft;

      case GET_SELECTED_TOPIC:
        draft.selectedTopic = action.topic;
        return draft;

      case QS_PATTERN_ADD_TOPIC_TO_GROUP:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        indexGroup = state.questionSet.qs_patterns[
          indexPattern
        ].pattern_groups.findIndex((group) =>
          group.id
            ? group.id == state.selectedGroupId
            : group.idTemp == state.selectedGroupId
        );
        draft.questionSet.qs_patterns[indexPattern].pattern_groups[
          indexGroup
        ].topics.push(action.topic);
        draft.groups.topics.push(action.topic);
        return draft;

      case DELETE_TOPIC_FROM_GROUP:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == state.selectedPatternId
            : pattern.idTemp == state.selectedPatternId
        );
        indexGroup = state.questionSet.qs_patterns[
          indexPattern
        ].pattern_groups.findIndex((group) =>
          group.id
            ? group.id == state.selectedGroupId
            : group.idTemp == state.selectedGroupId
        );
        indexTopic = state.questionSet.qs_patterns[indexPattern].pattern_groups[
          indexGroup
        ].topics.findIndex((topic) => topic.id == action.topicId);
        draft.questionSet.qs_patterns[indexPattern].pattern_groups[
          indexGroup
        ].topics.splice(indexTopic, 1);
        if (state.selectedTopic?.id == action.topicId) {
          draft.selectedTopic = null;
        }
        draft.groups.topics.splice(indexTopic, 1);
        return draft;

      case QS_DELETE_PATTERN:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == action.patternId
            : pattern.idTemp == action.patternId
        );
        draft.questionSet.qs_patterns.splice(indexPattern, 1);
        if (state.selectedPatternId == action.patternId) {
          draft.selectedPatternId = null;
          draft.selectedGroupId = null;
          draft.groups = null;
          draft.selectedTopic = null;
        }
        return draft;

      case NOT_SAVE_EDIT:
        draft.isEdit = true;
        return draft;

      case ADD_NEW_PATTERN:
        draft.questionSet.qs_patterns.push(action.pattern);
        return draft;

      case UPDATE_QS_VALUE:
        draft.questionSet[action.prop] = action.value;
        return draft;

      case UPDATE_COURSES_VALUE:
        draft.questionSet.courses[0].pivot[action.prop] = action.value;
        return draft;

      case CLEAR_QS_COURSES:
        draft.questionSet.courses = [
          {
            pivot: {
              course_content_id: null,
              page_number: null,
            },
          },
        ];
        return draft;

      case CHANGE_PATTERN_NAME:
        indexPattern = state.questionSet.qs_patterns.findIndex((pattern) =>
          pattern.id
            ? pattern.id == action.patternId
            : pattern.idTemp == action.patternId
        );
        draft.questionSet.qs_patterns[indexPattern].name = action.pName;
        return draft;

      case EMPTY_STORE_QS:
        draft.CourseSingleQSet = [];
        draft.coursesQSIdes = [];
        draft.questionSet = [];
        draft.qsAllowedApps = [];
        draft.question_set_id = null;
        draft.groups = [];
        draft.selectedGroupText = null;
        draft.selectedPatternId = null;
        draft.selectedGroupId = null;
        draft.selectedTopic = null;
        draft.allowedApps = [];
        draft.topics = [];
        draft.allCourses = [];
        draft.isEdit = false;
        draft.saveLoading = false;
        return draft;

      default: {
        return draft;
      }
    }
  });

export default questionSetsReducer;
