import React, { useContext, useState } from 'react'
import { GradingContext } from './GGradingManagement'
import RLister from 'components/Global/RComs/RLister'
import Loader from 'utils/Loader'
import iconsFa6 from 'variables/iconsFa6'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import Helper from 'components/Global/RComs/Helper'
import { tablesTypes } from './Constants'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import { baseURL, genericPath } from "engine/config";
import tr from 'components/Global/RComs/RTranslator'
import * as colors from 'config/constants'
import styles from './grading.module.scss'
import { editableFields } from './Constants'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import RIconDiv from 'components/Global/RComs/RIconDiv/RIconDiv'
import { Link } from 'react-router-dom'
const RAlternativesSlider = ({ itemId, courseId, cohortId, parentType, parentId }) => {
    const gradingData = useContext(GradingContext)
    console.log("Loaders", gradingData.isLoadingAlternativesSlider, gradingData.isLoadingAlternatives)
    if (gradingData.isLoadingAlternativesSlider || gradingData.isLoadingAlternatives)
        return <Loader />
    console.log('ItemsSLider States', gradingData.gradingState)
    const itemInfo = gradingData?.gradingState?.alternativesSlider?.itemInfo
    const allItemIds = gradingData?.gradingState?.itemAlternatives?.ids
    console.log('allItemsIds', allItemIds)
    const currentIndexId = allItemIds?.indexOf(itemId)
    console.log("nextprevId, currentId index", currentIndexId)
    let prevId = currentIndexId - 1 < 0 ? null : allItemIds?.[currentIndexId - 1]
    let nextId = currentIndexId + 1 < allItemIds?.length ? allItemIds?.[currentIndexId + 1] : null
    const prevType = gradingData?.gradingState?.itemAlternatives?.[prevId]?.["Type"]?.value ?? null
    const nextType = gradingData?.gradingState?.itemAlternatives?.[nextId]?.["Type"]?.value ?? null
    console.log("nextprevId,next:", nextId)
    console.log("nextprevId,prev:", prevId)
    const itemSlider = gradingData?.gradingState?.alternativesSlider
    const _itemSliderRecords = itemSlider?.ids?.map((recordId) => {
        const currentRecord = itemSlider?.[recordId]
        return {
            id: recordId,
            table_name: tablesTypes.AlternativeSlider,
            details: Object.keys(currentRecord)?.map((header) => {
                const studentComponent = <RProfileName
                    flexStyleProps={{ cursor: 'pointer' }}
                    onClick={() => {
                        gradingData.history.push({
                            pathname: `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/gradebook/studentDetails/${recordId}`,
                            state: { from: "ItemsSlider" }
                        }
                        )
                    }}
                    name={currentRecord['Students'].value.name}
                    img={currentRecord['Students'].value.image} />
                const currentHeader = currentRecord?.[header]
                const editable = editableFields.includes(header)
                let dropdownOptions = []
                let onClick = null
                dropdownOptions = header == "Students" ? [{ name: "alphabiticallyAsc", title: "Alphabetically Ascending" }, { name: "alphabiticallyDesc", title: "Alphabetically Descending" }]
                    : [{ name: "markAsc", title: "Order Ascending" }, { name: "markDesc", title: "Order Descenging" }]
                return {
                    key: header,
                    dropdown: header == "Students" || header == "Mark" ? true : null,
                    dropdownData: dropdownOptions,
                    onClick: (data) => {
                        gradingData.setItemSliderOrder(data.name)
                    },
                    value: editable ?
                        <div className='position-relative'>
                            <RHoverInput
                                inputValue={currentHeader.value}
                                handleInputDown={gradingData.handleInputSaved}
                                inputWidth='100px'
                                type={currentHeader.type}
                                focusOnInput={false}
                                inputPlaceHolder=""
                                handleInputChange={gradingData.handleInputChange}
                                inputIsNotValidate={!currentHeader.value && currentHeader.touched}
                                item={currentRecord}
                                extraInfo={{ id: recordId, type: tablesTypes.itemSlider, inputLabel: header }}
                                handleOnBlur={gradingData.handleInputSaved}
                                handleEditFunctionality={gradingData.handleEditFunctionality}
                                saved={currentHeader.saved}
                            />
                            {!currentHeader.value && currentHeader.touched ?
                                <i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} /> : ''}
                        </div>
                        : header == "Students" ? studentComponent : currentHeader.value,
                    type: editable ? 'component' : header == "Students" ? 'component' : ''
                }
            }),

        }
    })
    return (
        <RFlex id="upper content with table" className="flex-column">
            <RFlex id="go back with title and next and previous" className="flex-column" styleProps={{ gap: '15px' }}>
                <RButton
                    text="Back_To_Grading"
                    faicon={iconsFa6.arrowLeft}
                    color='link'
                    className="text-primary p-0 m-0"
                    onClick={() => { gradingData.history.goBack() }}
                    style={{ fontSize: '14px', width: 'fit-content' }} />
                <RFlex id="title with next and previous" className='justify-content-between'>
                    <p className='p-0 m-0 ' style={{ fontSize: '14px' }}>{itemInfo?.groupName ?? "No Group "}&gt; {itemInfo?.itemType} : {itemInfo?.itemName}</p>
                    <RFlex id="next and previous buttons" styleProps={{ gap: '15px' }}>
                        <RButton
                            text="Previous_Item"
                            faicon={iconsFa6.arrowLeft}
                            color='link'
                            className="text-primary p-0 m-0"
                            disabled={prevId ? false : true}
                            onClick={() => { gradingData.history.replace(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/gradebook/type/${parentType}/${parentId}/alternatives/${prevType}/${prevId}`) }}
                            style={{ fontSize: '14px', width: 'fit-content' }} />
                        <RButton
                            text="Next_Item"
                            faicon={iconsFa6.arrowRight}
                            color='link'
                            iconRight
                            className="text-primary p-0 m-0"
                            disabled={nextId ? false : true}
                            onClick={() => { gradingData.history.replace(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/gradebook/type/${parentType}/${parentId}/alternatives/${nextType}/${nextId}`) }}
                            style={{ fontSize: '14px', width: 'fit-content' }} />
                    </RFlex>
                </RFlex>
            </RFlex>
            <RLister Records={_itemSliderRecords} />
        </RFlex>
    )
}

export default RAlternativesSlider