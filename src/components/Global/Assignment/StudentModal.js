import React from "react";
import { Modal, Button } from "reactstrap";
import AssignmentFinalMark from "./AssignmentFinalMark";
import AssignmentTotalMark from "./AssignmentTotalMark";
import TeacherFeedback from "./TeacherFeedback";
import TeacherFinalMark from "./TeacherFinalMark";

const StudentModal = ({
  student,
  onHide,
  show,
  dispatch,
  mode,
  studentFeedback,
  studentsTotalMarks,
  isLastQuestion,
  teacherMultiMarks,
  multiFeedbacks,
}) => {
  return (
    <Modal
      onHide={onHide}
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {student.student_name}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <TeacherFeedback
          dispatch={dispatch}
          mode={mode}
          feedback={studentFeedback?.feedback}
          multiFeedbacks={multiFeedbacks}
          studentId={student.id}
        />
        <AssignmentFinalMark
          literalTeacherMark={studentFeedback?.literal_teacher_mark}
        />
        <AssignmentTotalMark
          TotalMark={
            studentsTotalMarks.filter((s) => s.studentId === student.id)[0]
              ?.totalMark
          }
        />
        {isLastQuestion() && (
          <TeacherFinalMark
            dispatch={dispatch}
            mode={mode}
            studentId={student.id}
            teacherMultiMarks={teacherMultiMarks}
          />
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default StudentModal;
