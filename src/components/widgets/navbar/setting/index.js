import React, { useState } from "react";
import { Collapse, NavbarBrand, Navbar, NavItem, NavLink, Nav, Container, UncontrolledDropdown } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

import styles from "../Navbar.module.scss";
const Setting = () => {
	return (
		<NavItem>
			<NavLink className="btn-magnify" href="#">
				<i className={iconsFa6.gear + " fa-lg"} />

				{/* <UncontrolledDropdown group className="uncontrolled">
										<DropdownToggle className="Dropdown-Toggle"></DropdownToggle>
										<DropdownMenu className="Dropdown-Menu">
											<DropdownItem className="Dropdown-Item"></DropdownItem>
										</DropdownMenu>
									</UncontrolledDropdown> */}
			</NavLink>
		</NavItem>
	);
};

export default Setting;
