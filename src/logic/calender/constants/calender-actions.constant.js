export const INITIALIZE = 'calender/initialize';

export const INITIALIZE_TYPES = 'event-types/initialize';

export const BLOCK_EVENT_TYPE = 'event-types/block';

export const UNBLOCK_EVENT_TYPE = 'event-types/un-block';


export const LOAD_COMMENTS = 'comments/load';

export const LOAD_NOTES = 'notes/load';


export const ADD_COMMENT = 'comments/add';

export const ADD_NOTES = 'notes/add';
