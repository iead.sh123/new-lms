import React, { useDebugValue, useEffect,useReducer, useState } from "react";
import ChatContent from "./chatContent/chatContent";
import ChatList from "./chatList/chatList";
import "./chat.css";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Container } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { trackLastSeenAsync } from "store/actions/global/track.action";

import withDirection from "hocs/withDirection";
import Swal from "utils/Alert";
import { DANGER, SUCCESS } from "utils/Alert";
import Loader from "utils/Loader";
import { selectChat } from "store/actions/IMCycle2.actions";
import { fetchMoreMessages } from "store/actions/IMCycle2.actions";
import { changePointer } from "store/actions/IMCycle2.actions";
import { sendMessage, createChat } from "store/actions/IMCycle2.actions";
import Settings from "./settings/Settings";
import { updateSettings } from "store/actions/IMCycle2.actions";
import { IMApi } from "api/IMNewCycle";
import { searchMessage } from "store/actions/IMCycle2.actions";
import { MessageTags, ICONS } from "./constants/MessageTags";
import { tagMessage } from "store/actions/IMCycle2.actions";
import { deleteMessage } from "store/actions/IMCycle2.actions";
import { setForwardedMessage } from "store/actions/IMCycle2.actions";
import { forwardMessage } from "store/actions/IMCycle2.actions";
import { unsendMessage } from "store/actions/IMCycle2.actions";
import { setRepliedOnMessageId } from "store/actions/IMCycle2.actions";
import { WARNING } from "utils/Alert";
import { useHistory } from "react-router-dom";
import { LAYOUTS, MAPPING } from "./rightSideLayouts";
import { CHAT_ACTIONS_ICONS, ChatTags } from "./constants/ChatTags";
import { tagChat } from "store/actions/IMCycle2.actions";
import { deleteChat } from "store/actions/IMCycle2.actions";
import { selectGroup } from "store/actions/IMCycle2.actions";
import { selectMultipleMessages } from "store/actions/IMCycle2.actions";
import { selectMultipleChats } from "store/actions/IMCycle2.actions";
import NewMessage from "./newMessage/NewMessage";

import RDropdown from "components/Global/RComs/RDropdown";
import RMiniUserCard from "components/Global/RComs/RMiniUserCard";
import RTextBox from "components/Global/RComs/RTextBox";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import { get } from "config/api";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import RForm from "components/Global/RComs/RForm";
import { useRef } from "react";
import CreateGroup from "./CreateGroup";
import { flexibleCompare } from "@fullcalendar/core/internal";


function IMTest(dir) {


  const dropDownTest=()=>{
const items=[
  {id:"item1",name:"item1"},
  {id:"item2",name:"item2"},
  {id:"item3",name:"item3"},

];
const [selected,setSelected]=useState("item1");
return <RDropdown
            items={items}
              itemCallback={(item)=>{setSelected(item.id);alert(item.id)}}
              selectedItemIndex={items.findIndex(s=> selected?.toLowerCase()==s?.id?.toLowerCase())}
              ></RDropdown>

}
 const miniUser=()=>{
      return <RMiniUserCard  name="rminiuserCard" image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
}

const miniUserContainerTest=()=>{
  return <div style={{
    border:"1px solid bl#668ad7ue",
    padding: "0.2rem",
    margin: "0.2rem",
    maxWidth: "50%",
    wordWrap: "break-word",
    
    display: "inline-block",
}}>

     <RMiniUserCard  name="rminiuser " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="maher daboul" image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="maram seif" image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="lubna khalil" image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="mostafa jarad" role="senior teacher " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="klistor fdgs"  role="senior teacher " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="vitorfdf"  role="senior teacher " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="rowd sdg"  role="senior teacher " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />
     <RMiniUserCard  name="clint oatu " image="http://localhost:8001/api/files/get-by-id/njnojzyrxmpaqozaegvkqldwv" onDelete={()=>{alert("deleted")}} />

     
  </div>
};

// const [open,setOpen]=useState(true);

const modal=(open,setOpen)=>{
const createGroup=()=>{};

const createGroupForm=()=>{
  const initialformItems = [
    {
      type: "text",
      label: tr`first_name`,
      name: "first_name",
      defaultValue: "user?.first_name",

    },
    {
      type: "text",
      label: tr`last_name`,
      name: "last_name",
      defaultValue: "user?.last_name",

    },
    {
      type: "email",
      label: tr`email`,
      name: "email",
      defaultValue: "user?.email1",

    },

    {
      type: "text",
      label: tr`address`,
      name: "address",

    },
    {
      type: "text",
      label: tr`phone`,
      name: "phone",

    },
    {
      type: "text",
      label: tr`nationality`,
      name: "nationality",

    },
    {
      type: "datetime-local",
      label: tr`time_zone`,
      name: "time_zone",
     
    },
  ];

  const formRef = useRef();
  const submitForm=()=>{if(formRef.current)
    {
      formRef.current.dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }))
    }
    }
  
  
  return<><RForm
  formItems={initialformItems}
  submitLabel={"Update Profile"}
  withSubmit={true}
  onSubmit={()=>{Helper.cl("rf2");alert("submit");}}
  formRef={formRef}
/>

{/* <RButton onClick={submitForm}></RButton> */}
</>
}

 return <div>
  <button onClick={()=>{setOpen(true)}}></button>
  <button onClick={()=>{setOpen(false)}}></button>
   <RModal
        isOpen={open}
        toggle={() => setOpen(!open)}
        header={<h6 className="text-capitalize">Details</h6>}
        footer={
        <div> 
         <RButton text={tr`Create Group`} onClick={submitForm()}
          color="primary"
          
        /> 
          <RButton text={tr`cancel`} onClick={() => {
         setOpen(false)
          }}
          color="white"
          style={{color:"#668ad7",background:"white",border:"0px solid"}}
          
        /> 
        </div>
      }
        body={
      <div>{createGroupForm()}</div>}
        //  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
        size="lg"
    />
    </div>
}


const fileSolution=()=>{
return <div style={{border:"red 1px solid",width:"200px"}}>file soltion</div>

}
///////////////////////////////////The Master return
return <div>
{/* {dropDownTest()}
{miniUser()}*/}
{/* {miniUserContainerTest()}  */}
{/* {UserAdder()} */}
{/* {  modal(open,setOpen) */}
{fileSolution()}
{/* <CreateGroup ></CreateGroup>  */}

</div>
}

export default withDirection(IMTest);


