import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";
import styles from "./RUnitPlan.module.scss";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUnitPlanForm = () => {
	const UnitPlanContextData = useContext(UnitPlanContext);
	const designByDirection = UnitPlanContextData.dir ? styles.text_ltr : styles.text_rtl;

	return (
		<Form className={styles.form_basic_info}>
			<Row>
				<Col>
					<RFlex>
						<h6 style={{ color: primaryColor, textTransform: "capitalize" }}>{tr`Basic information`}</h6>
					</RFlex>
				</Col>
			</Row>
			<Row>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("title")} :</label>
						<Input
							id="title"
							required
							type="text"
							onChange={(event) => UnitPlanContextData.handelUnitPlanChange("title", event.target.value)}
							value={UnitPlanContextData?.unitPlan?.title}
							placeholder={tr("title")}
						/>
						{UnitPlanContextData.unitPlanErrors && UnitPlanContextData.unitPlanErrors.title && (
							<FormText color="danger">{UnitPlanContextData.unitPlanErrors.title}</FormText>
						)}
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("tags")} :</label>
						<RTags
							defaultLabel={UnitPlanContextData?.unitPlan?.tags}
							url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
							getValueFromArrayItem={(i) => i.id}
							getLabelFromArrayItem={(i) => i.name}
							getIdFromArrayItem={(i) => i.id}
							defaultValues={UnitPlanContextData?.unitPlan?.tags}
							onChange={(data) => UnitPlanContextData.handelUnitPlanChange("tags", data)}
						/>
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("unit_number")} :</label>
						<Input
							id="unit_number"
							value={UnitPlanContextData?.unitPlan?.unit_number}
							required
							onChange={(event) => UnitPlanContextData.handelUnitPlanChange("unit_number", event.target.value)}
							type="number"
							placeholder={tr("unit_number")}
						/>
						{UnitPlanContextData.unitPlanErrors && UnitPlanContextData.unitPlanErrors.unit_number && (
							<FormText color="danger">{UnitPlanContextData.unitPlanErrors.unit_number}</FormText>
						)}
					</FormGroup>
				</Col>
				<Col sm={6} xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("time")} :</label>
						<Input
							id="time"
							required
							value={UnitPlanContextData?.unitPlan?.time}
							type="number"
							onChange={(event) => UnitPlanContextData.handelUnitPlanChange("time", event.target.value)}
							placeholder={tr("time")}
							min={0}
						/>
						{UnitPlanContextData.unitPlanErrors && UnitPlanContextData.unitPlanErrors.time && (
							<FormText color="danger">{UnitPlanContextData.unitPlanErrors.time}</FormText>
						)}
					</FormGroup>
				</Col>
				<Col xs={12}>
					<FormGroup>
						<label className={designByDirection}>{tr("description")} :</label>
						<Input
							id="description"
							required
							type="textarea"
							onChange={(event) => UnitPlanContextData.handelUnitPlanChange("description", event.target.value)}
							value={UnitPlanContextData?.unitPlan?.description}
							placeholder={tr("description")}
						/>
						{UnitPlanContextData.unitPlanErrors && UnitPlanContextData.unitPlanErrors.description && (
							<FormText color="danger">{UnitPlanContextData.unitPlanErrors.description}</FormText>
						)}
					</FormGroup>
				</Col>
			</Row>
		</Form>
	);
};

export default RUnitPlanForm;
