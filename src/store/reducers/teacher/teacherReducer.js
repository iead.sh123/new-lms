import {
  TEACHER_VIEW_EVENT_CALENDER_REQUEST,
  TEACHER_VIEW_EVENT_CALENDER_SUCCESS,
  TEACHER_VIEW_EVENT_CALENDER_ERROR,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const initState = {
  eventcalendar: [],
  periodscalendar: [],
  loadingCalendar: false,
};

const teacherReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case TEACHER_VIEW_EVENT_CALENDER_REQUEST:
        draft.loadingCalendar = true;
        return draft;

      case TEACHER_VIEW_EVENT_CALENDER_SUCCESS:
        draft.eventcalendar = action.payload.events;
        draft.periodscalendar = action.payload.period;
        draft.loadingCalendar = false;
        return draft;

      case TEACHER_VIEW_EVENT_CALENDER_ERROR:
        draft.loadingCalendar = false;
        return draft;

      default:
        return draft;
    }
  });

export default teacherReducer;
