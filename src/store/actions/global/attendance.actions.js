
import {
    CHANGE_STUDENT_ATTENDANCE,
    ADD_NEW_SETTING_TYPE
} from '../../actions/global/globalTypes'


export const editStudentAttendance=(studentIndex,attendanceType)=>(dispatch,getState)=>{
    dispatch({
        type:CHANGE_STUDENT_ATTENDANCE,
        payload:{studentIndex,attendanceType}
    })
}
export const addNewSettingType = (data)=>(dispatch,getState)=>{
    dispatch ({
        type:ADD_NEW_SETTING_TYPE,
        payload:{data}
    })
}