import { GET_TEACHER_COLLABORATION } from "store/actions/teacher/teacherTypes";
import {
  GET_MEETINGINFO,
  GET_LESSONINFO,
  EMPTY_STORE,
  GET_TEACHER_TODOLIST,
  GET_TEACHER_COURSES,
  GET_TEACHER_LIVESESSIONS,
  GET_TEACHER_EVENTS,
  MARK_AS_DONE_REQUEST,
  MARK_AS_DONE_SUCCESS,
  MARK_AS_DONE_ERROR,
  GET_TEACHER_HOME_ROOM_REQUEST,
  GET_TEACHER_HOME_ROOM_SUCCESS,
  GET_TEACHER_HOME_ROOM_ERROR,
  GET_STUDENT_HOME_ROOM_REQUEST,
  GET_STUDENT_HOME_ROOM_SUCCESS,
  GET_STUDENT_HOME_ROOM_ERROR,
  GET_ATTENDANCE_TYPES_REQUEST,
  GET_ATTENDANCE_TYPES_SUCCESS,
  GET_ATTENDANCE_TYPES_ERROR,
  GET_ATTENDANCE_VALUES_TO_STUDENTS,
  SET_ATTENDANCE_VALUES_TO_STUDENTS,
  POST_SET_ATTENDANCE_REQUEST,
  POST_SET_ATTENDANCE_SUCCESS,
  POST_SET_ATTENDANCE_ERROR,
  SEARCH_STUDENTS_ATTENDANCE_REQUEST,
  SEARCH_STUDENTS_ATTENDANCE_SUCCESS,
  SEARCH_STUDENTS_ATTENDANCE_ERROR,
  DELETE_STUDENTS_ATTENDANCE_REQUEST,
  DELETE_STUDENTS_ATTENDANCE_SUCCESS,
  DELETE_STUDENTS_ATTENDANCE_ERROR,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const InitState = {
  MeetingInfo: null,
  LessonInfo: null,
  HomeRoom: {},
  studentHomeRoom: [],
  attendanceTypes: [],
  searchStudents: [],
  homeRoomLoading: false,
  studentHomeRoomLoading: false,
  attendanceTypesLoading: false,
  setAttendance: false,
  searchStudentsLoading: false,
  deleteStudentsLoading: false,
};

let ind;

const dashboardReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_TEACHER_HOME_ROOM_REQUEST:
        draft.homeRoomLoading = true;
        return draft;

      case GET_TEACHER_HOME_ROOM_SUCCESS:
        draft.homeRoomLoading = false;
        draft.HomeRoom = action.payload.data;
        return draft;

      case GET_TEACHER_HOME_ROOM_ERROR:
        draft.homeRoomLoading = false;
        return draft;

      case GET_STUDENT_HOME_ROOM_REQUEST:
        return {
          studentHomeRoomLoading: true,
        };

      case GET_STUDENT_HOME_ROOM_SUCCESS:
        return {
          ...state,
          studentHomeRoomLoading: false,
          studentHomeRoom: action.payload.StudentHomeRoom,
        };

      case GET_STUDENT_HOME_ROOM_ERROR:
        return {
          studentHomeRoomLoading: false,
        };

      case GET_ATTENDANCE_TYPES_REQUEST:
        draft.attendanceTypesLoading = true;
        return draft;

      case GET_ATTENDANCE_TYPES_SUCCESS:
        draft.attendanceTypesLoading = false;
        draft.attendanceTypes = action.payload.AttendanceTypes;
        return draft;

      case GET_ATTENDANCE_TYPES_ERROR:
        draft.attendanceTypesLoading = false;
        return draft;

      case GET_ATTENDANCE_VALUES_TO_STUDENTS:
        draft.studentHomeRoom?.map((el) => {
          el.status = action.payload.ObjectIsDefault.abbreviation;
          el.id = action.payload.ObjectIsDefault.id;
        });
        return draft;

      case SET_ATTENDANCE_VALUES_TO_STUDENTS:
        ind = draft.studentHomeRoom.findIndex(
          (el) => el.studentGuidanceId == action.payload.studentId
        );

        if (ind !== -1) {
          draft.studentHomeRoom[ind].status = action.payload.radioValue;
        }
        return draft;

      case POST_SET_ATTENDANCE_REQUEST:
        draft.setAttendance = true;
        return draft;

      case POST_SET_ATTENDANCE_SUCCESS:
        draft.setAttendance = false;
        return draft;

      case POST_SET_ATTENDANCE_ERROR:
        draft.setAttendance = false;
        return draft;

      case SEARCH_STUDENTS_ATTENDANCE_REQUEST:
        draft.searchStudentsLoading = true;
        return draft;

      case SEARCH_STUDENTS_ATTENDANCE_SUCCESS:
        draft.searchStudentsLoading = false;
        draft.searchStudents = action.payload.StudentAttendance;
        return draft;

      case SEARCH_STUDENTS_ATTENDANCE_ERROR:
        draft.searchStudentsLoading = false;
        return draft;

      case DELETE_STUDENTS_ATTENDANCE_REQUEST:
        draft.deleteStudentsLoading = true;
        return draft;

      case DELETE_STUDENTS_ATTENDANCE_SUCCESS:
        draft.deleteStudentsLoading = false;
        return draft;

      case DELETE_STUDENTS_ATTENDANCE_ERROR:
        draft.deleteStudentsLoading = false;
        return draft;

      case GET_MEETINGINFO:
        return {
          ...state,
          MeetingInfo: action.payload,
        };

      case GET_LESSONINFO:
        return {
          ...state,
          LessonInfo: action.payload,
        };
      case EMPTY_STORE:
        return {
          ...state,
          MeetingInfo: null,
          LessonInfo: null,
        };

      case GET_TEACHER_TODOLIST:
        return {
          ...state,
          teacherToDoList: action.payload,
        };

      case GET_TEACHER_COURSES:
        return {
          ...state,
          teacherCourses: action.payload,
        };
      case GET_TEACHER_LIVESESSIONS:
        return {
          ...state,
          teacherLiveSessions: action.payload,
        };

      case GET_TEACHER_EVENTS:
        return {
          ...state,
          teacherEvents: action.payload,
        };

      case GET_TEACHER_COLLABORATION:
        return {
          ...state,
          teacherCollaborations: action.payload,
        };

      case MARK_AS_DONE_REQUEST:
        return {
          ...state,
          loaderToDO: true,
        };

      case MARK_AS_DONE_SUCCESS:
        return {
          ...state,
          loaderToDO: false,
        };

      case MARK_AS_DONE_ERROR:
        return {
          ...state,
          loaderToDO: false,
        };

      default:
        return draft;
    }
  });
export default dashboardReducer;
