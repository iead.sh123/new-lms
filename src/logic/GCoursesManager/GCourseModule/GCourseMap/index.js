import React, { useState, useRef } from "react";
import { FormGroup, Input, Progress, Collapse } from "reactstrap";
import { boldGreyColor } from "config/constants";
import { primaryColor } from "config/constants";
import { strokeColor } from "config/constants";
import LessonContent from "./LessonContent";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import iconsFa6 from "variables/iconsFa6";
import styles from "../GCourseMap.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useDispatch } from "react-redux";
import { userInteractionAsync } from "store/actions/global/learningObjects";

const GCourseMap = ({ courseModule, myProgress, loading }) => {
	const dispatch = useDispatch();
	const targetRefs = useRef([]);
	const [searchText, setSearchText] = useState("");
	const [searchResult, setSearchResult] = useState({});
	const [itemsSelected, setItemsSelected] = useState(0);
	const [openedCollapses, setOpenedCollapses] = useState([]);

	const collapsesContentToggle = (contentId) => {
		const isOpen = openedCollapses.includes(contentId);
		if (isOpen) {
			setOpenedCollapses(openedCollapses.filter((id) => id !== contentId));
		} else {
			setOpenedCollapses([...openedCollapses, contentId]);
		}
	};

	const searchTitle = (data, searchTerm, path = []) => {
		// ps: Formate => {childId:[parentsIds]}
		const results = {};

		for (const content of data) {
			const newPath = [...path, content.id];

			if (content.title && content.title.toLowerCase().includes(searchTerm.toLowerCase())) {
				results[+content.id] = newPath.filter((item) => +item !== +content.id).map(Number);
			}

			if (content.contents && content.contents.length > 0) {
				const nestedResults = searchTitle(content.contents, searchTerm, newPath);
				Object.assign(results, nestedResults);
			}
		}

		return results;
	};

	const handleSearch = () => {
		const matchingItems = searchTitle(courseModule.modules, searchText);
		setSearchResult(matchingItems);
		setItemsSelected(1);
		setOpenedCollapses(matchingItems[Object.keys(matchingItems)[0]]);
	};

	const handleScrollToSpecificResult = (contentId) => {
		const element = document.getElementById(contentId);

		if (element) {
			element.scrollIntoView({
				block: "center",
				behavior: "smooth",
				inline: "nearest",
			});
		}
	};

	const handleMoveUp = () => {
		if (itemsSelected !== 1) {
			setItemsSelected((el) => el - 1);
			setOpenedCollapses(searchResult[Object.keys(searchResult)[itemsSelected - 2]]);
			// handleScrollToSpecificResult(searchResult[itemsSelected - 2]);
		}
	};
	const handleMoveDown = () => {
		if (itemsSelected !== Object.keys(searchResult).length) {
			setItemsSelected((el) => el + 1);
			setOpenedCollapses(searchResult[Object.keys(searchResult)[itemsSelected]]);
			// handleScrollToSpecificResult(searchResult[itemsSelected]);
		}
	};
	const handleUserInteraction = (learningObjectId) => {
		dispatch(
			userInteractionAsync(
				learningObjectId,
				{ interaction_time_in_seconds: 0, interaction_type_id: "Performance" },
				courseModule.course.learning_object_id
			)
		);
	};

	return (
		<RFlex className={styles.map__container} styleProps={{ flexDirection: "column", gap: 0 }}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<span className={styles.course__name}>{courseModule?.course?.name}</span>
				<FormGroup style={{ position: "relative" }}>
					<Input
						type="text"
						placeholder={tr`jump_to`}
						value={searchText}
						onChange={(event) => {
							event.preventDefault();
							setSearchText(event.target.value);
						}}
						onKeyDown={(event) => {
							if (event.key === "Enter") {
								handleSearch();
							}
						}}
					/>
					{Object.keys(searchResult)?.length > 0 ? (
						<RFlex
							styleProps={{ position: "absolute", top: 8, right: 12, color: boldGreyColor, alignItems: "center", justifyContent: "center" }}
						>
							<span>
								{itemsSelected}/{Object.keys(searchResult).length}
							</span>
							<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
								<i
									className={iconsFa6.chevronUp + " " + styles.search__icon}
									onClick={() => {
										handleMoveUp();
									}}
								/>
								<i
									className={iconsFa6.chevronDown + " " + styles.search__icon}
									onClick={() => {
										handleMoveDown();
									}}
								/>
							</RFlex>
							<i
								style={{ color: strokeColor, cursor: "pointer" }}
								className={iconsFa6.close}
								onClick={() => {
									setSearchResult({});
									setSearchText("");
								}}
							/>
						</RFlex>
					) : (
						<i
							onClick={handleSearch}
							style={{ position: "absolute", top: 12, right: 12, cursor: "pointer", color: strokeColor }}
							className={iconsFa6.search}
						/>
					)}
				</FormGroup>
			</RFlex>
			{/* modules */}
			<RFlex className={styles.modules__container + " scroll_hidden"}>
				{courseModule?.modules?.map((module, moduleIndex) => (
					<RFlex key={module.id} styleProps={{ flexDirection: "column" }}>
						<RFlex className={styles.module}>
							{/* start module */}
							<RFlex styleProps={{ flexDirection: "column", gap: 5 }}>
								<RFlex
									styleProps={{ alignItems: "center", justifyContent: "space-between", cursor: "pointer" }}
									onClick={() => collapsesContentToggle(module.id)}
								>
									<RFlex>
										<Progress
											value={+module.progress * 100}
											striped
											color="success"
											style={{ borderRadius: "50%", height: "36px", width: "36px" }}
										/>
										<span className={styles.module__name}>{module.title}</span>
									</RFlex>
									<i
										className={openedCollapses.includes(module?.id) ? iconsFa6.chevronDown : iconsFa6.chevronRight}
										style={{ color: primaryColor }}
									/>
								</RFlex>
								<span style={{ color: boldGreyColor }}>
									{Object.entries(module?.statistics)
										.map(([key, value]) => `${value} ${key[0] + key.substring(1).toLowerCase()}`)
										.join(" | ")}
								</span>
							</RFlex>
							{/* end module */}
							<Collapse isOpen={openedCollapses.includes(module?.id)}>
								<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
									{/* start module content */}
									{module.contents.map((content) => (
										<RFlex key={content.id} styleProps={{ flexDirection: "column" }}>
											{content.type?.toLowerCase() == "lessons" ? (
												<LessonContent
													content={content}
													searchResult={searchResult}
													itemsSelected={itemsSelected}
													collapsesContentToggle={collapsesContentToggle}
													openedCollapses={openedCollapses}
													handleUserInteraction={handleUserInteraction}
												/>
											) : (
												<RFlex className={`${styles.content}`} id={content.id}>
													<AppCheckbox
														onChange={(event) => handleUserInteraction(content.learning_object_id)}
														className={styles.checkbox__design}
														checked={content.done}
													/>
													<RFlex styleProps={{ justifyContent: "space-between", alignItems: "baseline" }}>
														<i
															className={
																content?.type?.toLowerCase() == "lessons"
																	? iconsFa6.playCircle + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_polls"
																	? iconsFa6.pollVertical + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_surveys"
																	? iconsFa6.clipboardList + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_assignments"
																	? iconsFa6.pencilSquare + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_quizzes"
																	? iconsFa6.listCheck + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_exams"
																	? iconsFa6.questionCircle + " pt-1 fa-md"
																	: content?.type?.toLowerCase() == "module_project"
																	? iconsFa6.questionCircle + " pt-1 fa-md"
																	: ""
															}
														/>
														<span
															className={`${Object.keys(searchResult).map(Number).includes(+content.id) ? styles.highlighted : ""}`}
															style={{ background: Object.keys(searchResult)[itemsSelected - 1] == +content.id ? "#f58b1f" : "" }}
														>
															{content.title}
														</span>
													</RFlex>
												</RFlex>
											)}
										</RFlex>
									))}
									{/* end module content */}
								</RFlex>
							</Collapse>
						</RFlex>
					</RFlex>
				))}
			</RFlex>
		</RFlex>
	);
};

export default GCourseMap;
