import React, { useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom";
import PerfectScrollbar from "perfect-scrollbar";
import NavbarLayouts from "components/widgets/navbar";
import routes from "routes/landing";

var ps;

function LandingLayout(props) {
	const fullPages = React.useRef();
	// const [viewportWidth, setViewportWidth] = useState(window.innerWidth);
	// useEffect(() => {
	// 	function handleResize() {
	// 		// Use window.visualViewport.width to get the viewport width accounting for zoom
	// 		setViewportWidth(window.visualViewport.width);
	// 	}

	// 	window.addEventListener("resize", handleResize);

	// 	return () => {
	// 		window.removeEventListener("resize", handleResize);
	// 	};
	// }, []);

	React.useEffect(() => {
		if (navigator.platform.indexOf("Win") > -1) {
			ps = new PerfectScrollbar(fullPages.current);
		}
		return function cleanup() {
			if (navigator.platform.indexOf("Win") > -1) {
				ps.destroy();
			}
		};
	});

	const getRoutes = (routes) => {
		return routes.map((prop, key) => {
			if (prop.collapse) {
				return getRoutes(prop.views);
			}

			if (prop.layout === process.env.REACT_APP_BASE_URL + "/landing") {
				return <Route path={prop.layout + prop.path} component={prop.component} key={key} />;
			} else {
				return null;
			}
		});
	};
	return (
		<div className="wrapper">
			<div className={"main-panel-full-page"} ref={fullPages}>
				<div className="content">
					<NavbarLayouts {...props} dashboard={false} />
					<Switch>{getRoutes(routes)}</Switch>
				</div>
			</div>
		</div>
	);
}

export default LandingLayout;
