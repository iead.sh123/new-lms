import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";

import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";

const SolveUploadFile = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);
  const SelectedValue =
    QuestionSetSolveData.questionAnswers?.questionAnswers.find(
      (item) => +item.questionContextId == +question.module_context_question_id
    );
  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      <RFlex className="mb-3 mt-3">
        {question?.files.map((file) => (
          <RFlex key={file.hash_id} styleProps={{ alignItems: "center" }}>
            <RImageViewer
              mimeType={file?.mime_type}
              url={file?.hash_id}
              imageName={file?.name}
              width={"40px"}
              height={"40px"}
            />
            <span>{file.name}</span>
          </RFlex>
        ))}
      </RFlex>
      <RFlex className="mb-3 mt-4">
        <h6>{tr`upload_your_file_here`}</h6>
      </RFlex>
      <RFlex className="mb-3">
        {QuestionSetSolveData.correctOrReviewMode ? (
          <>
            {question?.answerFiles.map((file) => (
              <div key={file.hash_id} styleProps={{ alignItems: "center" }}>
                <RImageViewer
                  mimeType={file?.mime_type}
                  url={file?.hash_id}
                  imageName={file?.name}
                  width={"40px"}
                  height={"40px"}
                />
                <span>{file.name}</span>
              </div>
            ))}
          </>
        ) : (
          <RFileSuite
            parentCallback={(values) => {
              QuestionSetSolveData.handleFillDataToSolveQuestion({
                questionGroupId: questionGroup.id,
                groupContextId: questionGroup.module_context_group_id,
                questionContextId: question.module_context_question_id,
                type: question.type.name,
                key: "files",
                value: values,
                isCorrect: true,
              });
            }}
            singleFile={true}
            binary={true}
            value={
              QuestionSetSolveData.submitted == "solving"
                ? SelectedValue?.files
                : question?.files
            }
            fileType="image/* , video/* , application/* , text/*"
          />
        )}
      </RFlex>

      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveUploadFile;
