import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import {
  getSingleAbuseAsync,
  seenAbuseReportAsync,
} from "store/actions/global/abuse.action";
import { Row, Col, Form, FormGroup } from "reactstrap";
import { useHistory } from "react-router-dom";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import RSelect from "components/Global/RComs/RSelect";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";
import RButton from "../RComs/RButton";
import styles from "./Abuse.Module.scss";
import withDirection from "hocs/withDirection";

const SingleAbuse = React.forwardRef(({ dir }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { abuseId } = useParams();
  const { user } = useSelector((state) => state?.auth);
  const { singleAbuse, arrayUsers, single_Abuse_Loading } = useSelector(
    (state) => state.abuseServiceRed
  );

  useEffect(() => {
    dispatch(getSingleAbuseAsync(abuseId));
    if (user?.type !== "parent") {
      dispatch(
        seenAbuseReportAsync({
          payload: {
            abuse_reports: [abuseId],
          },
        })
      );
    }
  }, []);
  const flag = true;

  return (
    <div
      className={
        !user?.useNewTheme && user?.type == "parent" ? "container" : "content"
      }
      style={{
        paddingTop: !user?.useNewTheme && user?.type == "parent" && "18vh",
      }}
    >
      {single_Abuse_Loading ? (
        <Loader />
      ) : (
        <>
          <Row>
            <Col md={3} xs={12} className={dir ? "" : styles.button_rtl}>
              <RButton
                text={tr`back`}
                onClick={() => history.goBack()}
                color="info"
                outline
              />
            </Col>
          </Row>

          <Form
            style={{
              padding: "20px 0px",
              background: "#fff",
              boxShadow: "0px 0px 10px 4px rgba(0, 0, 0, 0.03)",
            }}
          >
            <Row style={{ display: "flex", justifyContent: "center" }}>
              <Col xs={12}>
                <FormGroup>
                  <label
                    style={{ fontWeight: "bold" }}
                    className={dir ? "" : styles.text_rtl}
                  >
                    {tr(`abused_children`)} :
                  </label>
                  <RSelect
                    closeMenuOnSelect={true}
                    placeholder={tr`select_child`}
                    defaultValue={singleAbuse?.students_info}
                    isMulti
                    isDisabled={true}
                  />
                </FormGroup>

                <FormGroup>
                  <label
                    style={{ fontWeight: "bold" }}
                    className={dir ? "" : styles.text_rtl}
                  >
                    {tr("report")} :
                  </label>
                  <CKEditor
                    editor={ClassicEditor}
                    name="description"
                    data={singleAbuse?.description}
                    disabled
                  />
                </FormGroup>
              </Col>

              <Col xs={12}>
                <label
                  style={{ fontWeight: "bold" }}
                  className={dir ? "" : styles.text_rtl}
                >
                  {tr("abusers")} :
                </label>
                <Row className={styles.abusers}>
                  <Col xs={6}>
                    <FormGroup>
                      <label className={dir ? "" : styles.text_rtl}>
                        {tr("roles")} :
                      </label>
                      <RSelect
                        closeMenuOnSelect={true}
                        placeholder={tr`select_user_types`}
                        defaultValue={arrayUsers}
                        isMulti
                        isDisabled={true}
                      />
                    </FormGroup>
                  </Col>

                  <Col xs={6}>
                    <FormGroup>
                      <label className={dir ? "" : styles.text_rtl}>
                        {tr("staff_members")} :
                      </label>
                      <RSelect
                        closeMenuOnSelect={true}
                        placeholder={tr`select_users`}
                        defaultValue={singleAbuse?.users_info}
                        isMulti
                        isDisabled={true}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </>
      )}
    </div>
  );
});

export default withDirection(SingleAbuse);
