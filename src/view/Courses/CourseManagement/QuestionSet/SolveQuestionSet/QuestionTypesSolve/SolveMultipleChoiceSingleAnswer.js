import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Services } from "engine/services";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import styles from "../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";
import { successColor, dangerColor } from "config/constants";

const SolveMultipleChoiceSingleAnswer = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);
  const SelectedValue =
    QuestionSetSolveData.questionAnswers?.questionAnswers.find(
      (item) => +item.questionContextId == +question.module_context_question_id
    );
  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      {question?.choices &&
        question?.choices.length > 0 &&
        question?.choices.map((choice, index) => (
          <div key={choice.id} className="mb-3">
            <RFlex>
              <AppRadioButton
                name={"choiceAnswer " + question?.id}
                disabled={QuestionSetSolveData.correctOrReviewMode}
                checked={
                  QuestionSetSolveData.submitted == "solving"
                    ? SelectedValue?.answer == +choice?.order
                    : +question?.answer == +choice?.order
                }
                onClick={(e) => {
                  QuestionSetSolveData.handleFillDataToSolveQuestion({
                    questionGroupId: questionGroup.id,
                    groupContextId: questionGroup.module_context_group_id,
                    questionContextId: question.module_context_question_id,
                    type: question.type.name,
                    key: "answer",
                    value: choice.order,
                    isCorrect: true,
                  });
                }}
              />
              {QuestionSetSolveData.correctOrReviewMode ? (
                <span
                  className="d-flex pt-3"
                  style={{
                    color:
                      +question?.correctAnswer == +choice.order
                        ? successColor
                        : +question?.answer == +choice.order
                        ? dangerColor
                        : "",
                  }}
                >
                  {/* wrong answers */}
                  {+question?.answer == +choice.order &&
                    +question?.answer !== +question.correctAnswer && (
                      <i className="fa-solid fa-xmark pt-1 pr-3 pl-0" />
                    )}
                  {/* correct answers */}
                  {+question?.correctAnswer == +choice.order && (
                    <i className="fa-solid fa-check pt-1 pr-3 pl-0" />
                  )}
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{choice.text}</p>
                </span>
              ) : (
                <span className="d-flex pt-2">
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{choice.text}</p>
                </span>
              )}
            </RFlex>
            {choice?.files && choice?.files.length > 0 && (
              <img
                src={Services.storage.file + choice?.files[0]?.hash_id}
                alt={choice?.id}
                className={styles.image}
              />
            )}
          </div>
        ))}
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveMultipleChoiceSingleAnswer;
