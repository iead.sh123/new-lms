import { get } from "config/api";
import { Services } from "engine/services";

export const cohortsApi = {
  getAllCohorts: (userId) =>
    get(`${Services.cohorts.backend}api/user/cohorts`, {
      headers: { "user-id": userId },
    }),
  getRoleCohortByUserId: (userId) =>
    get(`${Services.cohorts.backend}api/cohorts/user/${userId}/role`, {
      headers: { "user-id": userId },
    }),
};
