import React, { useEffect, useState, useMemo } from "react";
import Swal, { DANGER } from "utils/Alert";
import { baseURL, genericPath } from "engine/config";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { post, destroy } from "config/api";
import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import { Row, Col } from "reactstrap";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RGroupButtons from "components/Global/RComs/RGroupButtons/RGroupButtons";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { toast } from "react-toastify";

const GLessonPlanLister = () => {
	const history = useHistory();
	const { courseId, curriculumId, cohortId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [loading, setLoading] = useState(false);
	const [alert, setAlert] = useState(false);
	const [filterData, setFilterData] = useState({
		left: "all",
		right: "recently",
	});
	const [filters, setFilters] = useState({ left: [], right: [] });
	const [rightFilter, setRightFilter] = useState([]);
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handlePickFromCollaboration = () => {
		history.push(
			`${baseURL}/${genericPath}/collaboration_area_contents/${
				courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
			}/unit_plans`
		);
	};

	const handlePushToUnitPlanEditor = () => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/unit-plans`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/unit-plans`);
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const Request = {
			data: { course_id: courseId },
		};
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&withPagination=${true}`;

		const url = `${Services.unit_plan.backend}api/v1/unit-plan/get${QueryReq}`;

		let response = await post(specific_url ? specific_url : url, Request);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleSearch = async (emptySearch) => {
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&${filterData.right}=${true}&withPagination=${true}&filter=${emptySearch ?? searchData}`;
		let specific_url;
		specific_url = `${Services.unit_plan.backend}api/v1/unit-plan/get${QueryReq}`;

		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);
		}
	};

	const handleSearchOnLeftTabs = async (leftFilter) => {
		setFilterData({ ...filterData, left: leftFilter });
		let QueryReq = "";
		QueryReq = `?${leftFilter}=${true}&${filterData.right}=${true}&withPagination=${true}${searchData ? `&filter=${searchData}` : ""}`;

		let specific_url;
		specific_url = `${Services.unit_plan.backend}api/v1/unit-plan/get${QueryReq}`;

		setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setLoading(false);
			} else {
				setLoading(false);
			}
		} else {
			setLoading(false);
		}
	};

	const handleSearchOnRightTabs = async (rightFilter) => {
		setFilterData({ ...filterData, right: rightFilter });
		let QueryReq = "";
		QueryReq = `?${filterData.left}=${true}&${rightFilter}=${true}&withPagination=${true}${searchData ? `&filter=${searchData}` : ""}`;

		let specific_url;
		specific_url = `${Services.unit_plan.backend}api/v1/unit-plan/get${QueryReq}`;

		setLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setLoading(false);
			} else {
				setLoading(false);
			}
		} else {
			setLoading(false);
		}
	};

	const successDelete = async (prameters) => {
		const response = await destroy(`${Services.unit_plan.backend}api/v1/unit-plan/delete/${prameters.unitPlanId}`);

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					setAlert(null);
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemoveUnitPlan = (unitPlanId) => {
		const prameters = {
			unitPlanId,
		};
		const message = tr`Are you sure?`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	// const handelChangeStatue = (lessonId, flag) => {};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.unit_plans?.data.map((r) => ({
			id: r.id,
			table_name: "GUnitPlanLister",

			details: [
				{ key: tr`title`, value: r?.title },
				{ key: tr`description`, value: r?.description },
				{
					key: tr("saved_as_a_template"),
					value:
						r.is_template && r.published ? (
							<i className="fa-solid fa-check" style={{ color: successColor }} />
						) : (
							<i className="fa-solid fa-minus" />
						),
					html: true,
				},
				{
					key: tr("status"),
					value: r.published ? (
						<h6
							style={{ cursor: "pointer", color: successColor, fontWeight: 400 }}
							// onClick={() => {
							// 	handelChangeStatue(lsn.id, true);
							// }}
						>
							{tr("published")}
						</h6>
					) : (
						<h6
							style={{ cursor: "pointer", color: "#DD0000", fontWeight: 400 }}
							// onClick={() => {
							// 	handelChangeStatue(lsn.id, false);
							// }}
						>
							{tr("draft")}
						</h6>
					),
					html: true,
				},
			],
			actions: [
				{
					name: tr`start_from_this_template`,
					icon: "fa-solid fa-file-lines",
					onClick: () => {
						history.push(
							`${baseURL}/${genericPath}/course-management/${
								courseId ? `course/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""
							}/cohort/${cohortId}/editor/unit-plans/${r.id}?isTemplate=${r.id}`
						);
					},
					hidden: r.is_template && r.published ? false : true,
				},
				{
					name: tr`edit`,
					icon: "fa-solid fa-pencil",
					onClick: () => {
						if (courseId) {
							history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/unit-plans/${r.id}`);
						} else if (curriculumId) {
							history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/unit-plans/${r.id}`);
						}
					},
				},
				{
					name: tr`view`,
					icon: "fa-solid fa-eye",
					onClick: () => {
						if (courseId) {
							history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/unit-plans/${r.id}`);
						} else if (curriculumId) {
							history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/unit-plans/${r.id}`);
						}
					},
				},
				{
					name: tr`export_as_pdf`,
					icon: iconsFa6.pdf,
					onClick: () => {},
					disabled: true,
				},
				{
					name: tr`delete`,
					icon: "fa-solid fa-trash-can",
					color: dangerColor,
					onClick: () => {
						handleRemoveUnitPlan(r.id);
					},
				},
			],
		}));
		setFilters(response?.data?.data?.filters);
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.unit_plans,
			},
		}),
		[processedRecords]
	);

	useEffect(() => {
		if (filters && filters.right.length > 0) {
			const updatedRightFilter = filters.right.map((filter, index) => ({
				id: index,
				title: filter.title,
				color: "primary",
				outline: true,
				active: false,
				onClick: handleSearchOnRightTabs,
			}));

			setRightFilter(updatedRightFilter);
		}
	}, [filters]);

	return (
		<>
			{alert}
			<RFlex>
				<RSearchHeader
					searchLoading={searchLoading}
					searchData={searchData}
					handleSearch={handleSearch}
					setSearchData={setSearchData}
					handleChangeSearch={handleChangeSearch}
					handlePushToAnotherRouteWhenAdd={handlePushToUnitPlanEditor}
					addNew={true}
					buttonName={tr`create_new_unit_plan`}
					outline={false}
				/>
				<RButton
					text={tr`pick_from_collaboration`}
					faicon={iconsFa6.globe}
					color={"primary"}
					outline
					onClick={handlePickFromCollaboration}
				/>
			</RFlex>
			<Row>
				{filters?.left.length > 0 && (
					<Col xs={12} md={8}>
						<RFilterTabs tabs={filters.left} handleSearchOnTabs={handleSearchOnLeftTabs} />
					</Col>
				)}
				{rightFilter.length > 0 && (
					<Col xs={12} md={4} className="d-flex justify-content-end">
						<RGroupButtons buttons={rightFilter} selectedFilter={filterData.right} />
					</Col>
				)}
				<Col xs={12}>
					<RAdvancedLister {...propsLiterals.listerProps} />
				</Col>
			</Row>
		</>
	);
};

export default GLessonPlanLister;
