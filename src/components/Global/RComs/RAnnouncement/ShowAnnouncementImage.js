import React from "react";
import styles from "./RAnnouncement.Module.scss";
import { useDispatch } from "react-redux";
import { RemoveImageFromAnnouncement } from "store/actions/admin/schoolEvents";

const ShowAnnouncementImage = ({ images }) => {
  const dispatch = useDispatch();

  const removeImage = (e, id) => {
    e.preventDefault();
    dispatch(RemoveImageFromAnnouncement(id));
  };

  return (
    <div className="row  ml-4">
      {images.map((pic, i) => {
        return (
          <div key={i} className="mb-3 ml-4">
            {["png", "jpeg", "PNG", "jpg"].includes(pic.mime_type) ? (
              <img
                src={`http://192.168.1.9:8000/storage/${pic.url}`}
                // src={`${process.env.REACT_APP_RESOURCE_URL}${pic.url}`}
                width="100px"
                height="100px"
                alt="placeholder grey 100px"
              />
            ) : (
              <span className={styles.file_border}>{pic.file_name}</span>
            )}

            <span
              className={styles.imageShow_editPost + " cursor-pointer "}
              onClick={(e) => removeImage(e, pic.id)}
            >
              X
            </span>
          </div>
        );
      })}
    </div>
  );
};

export default ShowAnnouncementImage;
