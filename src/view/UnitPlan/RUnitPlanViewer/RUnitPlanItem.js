import React from "react";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";

const RUnitPlanItem = ({ item, itemIndex, sectionIndex }) => {
	const nestedIndex = `${sectionIndex}.${itemIndex}.`;

	return (
		<div key={item.id} className={styles.un__viewer__item}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<RFlex>
					<h6>
						<span style={{ color: "#F58B1F" }}>{nestedIndex}</span>
						{item?.text}
					</h6>
				</RFlex>

				{/* <div style={{ color: "red" }}>rubric title Here</div> */}
				{item?.lesson_plan?.length == 0 ? "" : <h6>{item?.lesson_plan?.name}</h6>}

				{item?.attachments?.length > 0 && (
					<RImageViewer
						mimeType={item?.attachments[0]?.mime_type}
						url={item?.attachments[0]?.hash_id}
						imageName={item?.attachments[0]?.name}
						showFullAttachment={true}
						width={"71px"}
						height={"62px"}
					/>
				)}
			</RFlex>
		</div>
	);
};

export default RUnitPlanItem;
