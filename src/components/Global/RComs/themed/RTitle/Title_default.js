import styles from "./styles/default.Module.scss";
import React from 'react';

const Title_default = ({
  text,
  dir
}) => <h4 className={styles["title-notf"]}>
    {text}
  </h4>

export default Title_default;
