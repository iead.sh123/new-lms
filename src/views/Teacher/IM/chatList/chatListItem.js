import React, { useState } from "react";
import ContextMenu from "../ContextMenu/ContextMenu";
import UserImg from "../userImg";
import { CHAT_ACTIONS_ICONS } from "../constants/ChatTags";
import { relativeDate } from "utils/dateUtil";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RColumn from "view/RComs/Containers/RColumn";
import RRow from "view/RComs/Containers/RRow";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import styles from './chatList.module.scss'
import userLogo from '../../../../assets/img/avatar.png'
import userLogo2 from '../../../../assets/img/testUserImage.svg'
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import GroupAvatar from "assets/img/new/svg/group_avatar.svg";

import { useHistory } from "react-router-dom";
export default function ChatListItem({
  chat,
  selectChat,
  loggedUserId,
  selectedChat,
  isGroupSelected,
  groupSelectionIsEnabled,
  chatActions,
  floating
}) {
  const [showContext, setShowContext] = React.useState(false);
  const history = useHistory();

  const hideContextMenu = React.useCallback(
    () => setShowContext(false),
    [showContext]
  );

  const toggleContextMenu = (e) => {
    e.preventDefault();
    setShowContext(!showContext);
  };

  React.useEffect(() => {
    window.addEventListener("click", hideContextMenu);
    return () => window.removeEventListener("click", hideContextMenu);
  }, []);

  const loggedUserIndex = chat?.entities?.users?.ids.findIndex(
    (i) => i.id == loggedUserId
  );
  //console.log(Services?.storage?.file+ chat?.image?.hash_id)
  let image=Services?.storage?.file+ chat?.image?.hash_id
  if (typeof (image) === 'string'){
  if (image?.endsWith("null") || image?.endsWith('undefined')){
    image=chat?.name ? GroupAvatar:UserAvatar
  }}
  const [isHovering,setIsHovering]=useState(false)

  // {Helper.cl(chat.image,"chat.image")}
  return  <RFlex
              id="Whole Chat"
              onMouseEnter={()=>setIsHovering(true)}
              onMouseLeave={()=>setIsHovering(false)}
              className={`${
                (!groupSelectionIsEnabled && chat.id == selectedChat) ||
                (groupSelectionIsEnabled && isGroupSelected)
                  ? "chat_List_Item activee "
                  : "chat_List_Item"}
                  ${floating?'p-0':''} 
                
              `}
              onClick={()=>{selectChat();history.push('/g/IM')}}
              styleProps={{width:'100%',justifyContent:'space-between',backgroundColor:floating?"transparent":"",cursor:floating?'pointer':""}}
   
   >
   {/* column 1 ----------------------- images*/}

<RFlex id="left part of the chat" styleProps={{gap:'10px',width:'75%'}}>

  {chat?.name ? (
              <UserImg
              index={0}
                // image={userLogo2}
                  // image={(Services?.storage?.file + i?.image).endsWith('null')?UserAvatar:Services?.storage?.file + i?.image}

                image={image}
                // online={chat?.entities?.users?.byId[i].online}
                // index={index - ((index > loggedUserIndex) ? 1 : 0)}
                name={chat.name}
                id={chat.id}
                width={38}
                // length={chat?.entities.users.ids.length}
              />
          ) : (
            chat?.entities?.users?.ids?.map((i, index) => {
              //Helper.cl(chat?.entities?.users?.byId[i].id,"chat?.entities?.users?.byId[i].id");
                // Helper.cl(loggedUserId,"loggedUserId");
         
            
              if (chat?.entities?.users?.byId[i]?.userId === loggedUserId) ;
              else
                return (
                    <UserImg
                      image={chat?.entities?.users?.byId[i]?.image?Services?.storage?.file+ chat?.entities?.users?.byId[i]?.image:null}
                      index={0}
                      // image={Services?.storage?.file+ chat?.entities?.users?.byId[i].image}
                      online={chat?.entities?.users?.byId[i].online}
                      // index={index - (index > loggedUserIndex ? 1 : 0)}
                      name={chat?.entities?.users?.byId[i].name}
                      id={i}
                      width={38}
                      length={chat?.entities?.users?.ids.length}
                    />
                );
            })
          )}

   {/* column 2 */}
   <RFlex id="name + messages" className="flex-column" styleProps={{minWidth:'25%',maxWidth:'70%',gap:'0px',cursor:floating?"pointer":'default'}}>
          <ul className="chat_List_Item_hul" style={{color:'black'}}>
            {chat.name
              ? chat.name
              : chat?.entities?.users?.ids
                  ?.filter((i) => i !== loggedUserId)
                  .map((i, index) => (
                    <li
                      className="chat_List_Item_hli"
                      key={"usr-ch-" + index }
                    >
                      {" "}
                      {`${chat?.entities?.users?.byId[i].name} ${
                        index < chat?.entities?.users?.ids?.length - 2
                          ? ","
                          : ""
                      }`}
                    </li>
                  ))}
          </ul>

          <div id="bottom messages" style={{cursor:floating?"pointer":'default'}} className="chat_List_message">
            {" "}
            {chat?.entities?.messages?.byId?.[
              chat?.entities?.messages?.ids[
                chat?.entities?.messages?.ids?.length - 1
              ]
            ]?.message?.substring(0, 20) ??
              "No Messages" +
                (chat?.entities?.messages?.byId?.[
                  chat?.entities?.messages?.ids?.[
                    chat?.entities?.messages?.ids?.length - 1
                  ]
                ]?.message?.length > 20
                  ? "....."
                  : "")}
          </div>

   </RFlex>
</RFlex>
  
   

    {/* column 3 */}
    <RFlex id="right side of the chat"
     styleProps={{gap:'5px'}}
     className="flex-column align-items-end">
        <div
          id="date_bill"
          style={{display: "flex",flexDirection: "column",justifyContent: "center",alignItems: "center",cursor:'default'}}
        >
            
              <span
                className="ChatDate"
                title={relativeDate(
                  chat?.entities?.messages?.byId?.[
                    chat?.entities?.messages?.ids[
                      chat?.entities?.messages?.ids?.length - 1
                    ]
                  ]?.created_at
                )}
              >
{chat?.entities?.messages?.byId?.[chat?.entities?.messages?.ids[chat?.entities?.messages?.ids?.length - 1]
            ] ? 
                relativeDate(
                  chat?.entities?.messages?.byId?.[
                    chat?.entities?.messages?.ids[
                      chat?.entities?.messages?.ids?.length - 1
                    ]
                  ]?.created_at
                
             ) : 
            
            (
             "-"
            )} 
            
            </span>
            
         
          
        
        </div>
    
{!floating?(
  <RFlex 
  id="hover button and unread messages" 
  className="justify-content-end align-items-center"
  onClick={(event)=>event.stopPropagation()} >
    <RFlex id="first div in row"  styleProps={{color:'#668AD7'}}>
      {chat.tags?.map?.((t,iii) => (<i key={"ch-tag-" + iii} className={CHAT_ACTIONS_ICONS[t]}></i>))}
      </RFlex> 
    <div id="second div in row" style={{ flexDirection: "row",color:'#668AD7' }}>
      {chat.unreadMessages ? (<div
    id="third div in row" class="c_bill" style={{color:'#668AD7'}}>{chat.unreadMessages}</div>) : null}
    </div>
    {isHovering && 
    <ContextMenu actions={chatActions} />}
</RFlex>
):(
  <div id="second div in row" style={{ flexDirection: "row" }}>
  {chat.unreadMessages ? (<div
id="third div in row" class="c_bill">{chat.unreadMessages}</div>) : null}
</div>  
) }

      
    </RFlex>
      </RFlex> 

  // return (
  //   <ul
  //     //onContextMenu={toggleContextMenu}
  //     id="chatlistitem"
  //     className={
  //       (!groupSelectionIsEnabled && chat.id == selectedChat) ||
  //       (groupSelectionIsEnabled && isGroupSelected)
  //         ? "chat_List_Item activee"
  //         : "chat_List_Item"
  //     }
  //     onClick={selectChat}
  //     style={
  //       showContext
  //         ? { background: "#668ad7", overflow: "visible" }
  //         : chat.unread > 0
  //         ? { background: "#efdbc1", overflow: "visible" }
  //         : {}
  //     }
  //   >
  //     <li
  //       className="chat_List_Item_li"
  //       style={{ display: "flex", direction: "row", width: "100%" }}
  //     >
  //       <div
  //         id="usr_imgs"
  //         style={{
  //           display: "flex",
  //           direction: "row",
  //           justifyContent: "flex-start",
  //           alignItems: "flex",
  //           direction: "ltr",
  //         }}
  //       >
  //         {chat?.name ? (
  //           <div key={"chatttt-" + chat.id} className="chat-list-img">
  //             <UserImg
  //               image={chat.image}
  //               // online={chat?.entities?.users?.byId[i].online}
  //               // index={index - ((index > loggedUserIndex) ? 1 : 0)}
  //               name={chat.name}
  //               id={chat.id}
  //               width={38}
  //               // length={chat?.entities.users.ids.length}
  //             />
  //           </div>
  //         ) : (
  //           chat?.entities?.users?.ids?.map((i, index) => {
  //             if (i == loggedUserId);
  //             else
  //               return (
  //                 <div
  //                   key={"usrrrr-" + index + "" + i}
  //                   className="chat-list-img"
  //                 >
  //                   <UserImg
  //                     image={chat?.entities?.users?.byId[i].image}
  //                     online={chat?.entities?.users?.byId[i].online}
  //                     index={index - (index > loggedUserIndex ? 1 : 0)}
  //                     name={chat?.entities?.users?.byId[i].name}
  //                     id={i}
  //                     width={38}
  //                     length={chat?.entities?.users?.ids.length}
  //                   />
  //                 </div>
  //               );
  //           })
  //         )}
  //       </div>

  //       <div
  //         className="chat_list_text "
  //         style={{
  //           display: "flex",
  //           flexDirection: "column",
  //           //  overflow: "hidden",
  //           justifyContent: "flex-start",
  //           direction: "ltr",
  //         }}
  //       >
  //         <ul className="chat_List_Item_hul">
  //           {chat.name
  //             ? chat.name
  //             : chat?.entities?.users?.ids
  //                 ?.filter((i) => i !== loggedUserId)
  //                 .map((i, index) => (
  //                   <li
  //                     className="chat_List_Item_hli"
  //                     key={"usr-ch-" + index + "" + i}
  //                   >
  //                     {" "}
  //                     {`${chat?.entities?.users?.byId[i].name} ${
  //                       index < chat?.entities?.users?.ids?.length - 2
  //                         ? ","
  //                         : ""
  //                     }`}
  //                   </li>
  //                 ))}
  //         </ul>
  //         <div className="chat_List_message">
  //           {" "}
  //           {chat?.entities?.messages?.byId?.[
  //             chat?.entities?.messages?.ids[
  //               chat?.entities?.messages?.ids?.length - 1
  //             ]
  //           ]?.message?.substring(0, 25) ??
  //             "This message has been deleted" +
  //               (chat?.entities?.messages?.byId?.[
  //                 chat?.entities?.messages?.ids?.[
  //                   chat?.entities?.messages?.ids?.length - 1
  //                 ]
  //               ]?.message?.length > 25
  //                 ? "....."
  //                 : "")}
  //         </div>
  //       </div>

  //       <div
  //         id="date_bill"
  //         style={{
  //           display: "flex",
  //           flexDirection: "column",
  //           justifyContent: "center",
  //           alignItems: "center",
  //         }}
  //       >
  //         <div>
  //           {" "}
  //           {chat?.entities?.messages?.byId?.[
  //             chat?.entities?.messages?.ids[
  //               chat?.entities?.messages?.ids?.length - 1
  //             ]
  //           ] ? (
  //             <span
  //               className="chat_list_message_date"
  //               title={relativeDate(
  //                 chat?.entities?.messages?.byId?.[
  //                   chat?.entities?.messages?.ids[
  //                     chat?.entities?.messages?.ids?.length - 1
  //                   ]
  //                 ]?.created_at
  //               )}
  //             >
  //               {relativeDate(
  //                 chat?.entities?.messages?.byId?.[
  //                   chat?.entities?.messages?.ids[
  //                     chat?.entities?.messages?.ids?.length - 1
  //                   ]
  //                 ]?.created_at
  //               )}
  //             </span>
  //           ) : (
  //             <></>
  //           )}
  //         </div>
  //         {chat.unreadMessages > 0 ? (
  //           <div className="unread_messages">{chat.unreadMessages}</div>
  //         ) : null}
  //         <div style={{ flexDirection: "row" }}>
  //           {chat.tags?.map?.((t) => (
  //             <i key={"ch-tag-" + t} className={CHAT_ACTIONS_ICONS[t]}></i>
  //           ))}
  //         </div>
  //       </div>
  //     </li>

  //     <ContextMenu actions={chatActions} />
  //   </ul>
  // );
}
