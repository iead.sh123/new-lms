import React, { useState } from "react";
import { PaymentElement, LinkAuthenticationElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";

const CheckoutForm = ({ clientSecret, orderId }) => {
	const history = useHistory();
	const stripe = useStripe();
	const elements = useElements();

	const [email, setEmail] = useState("");
	const [message, setMessage] = useState("");
	const [isLoading, setIsLoading] = useState(false);

	const paymentElementOptions = {
		layout: "tabs",
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		if (!stripe || !elements) {
			// Stripe.js hasn't yet loaded.
			// Make sure to disable form submission until Stripe.js has loaded.
			return;
		}

		setIsLoading(true);

		try {
			// Attempt to confirm the payment first
			const { error, paymentIntent } = await stripe.confirmPayment({
				elements,
				// confirmParams: {
				// 	// Make sure to change this to your payment completion page
				// 	return_url: returnUrl,
				// },
				redirect: "if_required",
			});

			if (error?.type === "card_error" || error?.type === "validation_error") {
				setMessage(error?.message);
			} else if (paymentIntent && paymentIntent.status === "succeeded") {
				history.push(`/cart/payment-success/${orderId}`);
			} else {
				setMessage("An unexpected error occurred.");
			}
		} catch (error) {
			setMessage("An error occurred while processing the order or payment.");
		}

		setIsLoading(false);
	};

	return (
		<form id="payment-form" onSubmit={handleSubmit}>
			<PaymentElement id="payment-element" options={paymentElementOptions} />
			<RButton type="submit" text={tr`pay`} color="primary" style={{ width: "100%" }} loading={isLoading} disabled={isLoading} />
		</form>
	);
};

export default CheckoutForm;
