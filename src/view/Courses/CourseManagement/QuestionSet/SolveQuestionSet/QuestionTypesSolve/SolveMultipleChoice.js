import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Services } from "engine/services";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import styles from "../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import { successColor, dangerColor } from "config/constants";
import Headers from "./Headers";

const SolveMultipleChoice = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);
  const SelectedValue =
    QuestionSetSolveData.questionAnswers?.questionAnswers?.find(
      (item) => +item.questionContextId == +question.module_context_question_id
    );

  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      {question?.choices &&
        question?.choices.length > 0 &&
        question?.choices.map((choice, index) => (
          <div key={choice.id}>
            <RFlex styleProps={{ alignItems: "center" }}>
              <AppCheckbox
                disabled={QuestionSetSolveData.correctOrReviewMode}
                checked={
                  QuestionSetSolveData.submitted == "solving"
                    ? SelectedValue?.answer
                        ?.split(",")
                        .map((item) => Number(item))
                        .includes(choice.order)
                    : question?.answer
                        ?.split(",")
                        .map((item) => Number(item))
                        .includes(choice.order)
                }
                name={"choiceAnswer " + question?.type?.name}
                className={
                  question?.answer
                    ?.split(",")
                    .map((item) => Number(item))
                    .includes(choice.order)
                    ? "form-check-primary"
                    : null
                }
                onClick={(e) => {
                  QuestionSetSolveData.handleFillDataToSolveQuestion({
                    questionGroupId: questionGroup.id,
                    groupContextId: questionGroup.module_context_group_id,
                    questionContextId: question.module_context_question_id,
                    type: question.type.name,
                    key: "answer",
                    value: choice.order,
                    isCorrect: true,
                  });
                }}
              />
              {QuestionSetSolveData.correctOrReviewMode ? (
                <span
                  className="d-flex pt-3"
                  style={{
                    color: question?.correctAnswer
                      ?.split(",")
                      ?.map((item) => Number(item))
                      .includes(choice.order)
                      ? successColor
                      : question?.answer
                          ?.split(",")
                          ?.filter((el) => el !== question?.correctAnswer)
                          .map((item) => Number(item))
                          .includes(choice.order)
                      ? dangerColor
                      : "",
                  }}
                >
                  {/* wrong answers */}
                  {question?.answer
                    ?.split(",")
                    ?.filter((el) => el !== question?.correctAnswer)
                    .map((item) => Number(item))
                    .includes(choice.order) && (
                    <i className="fa-solid fa-xmark pt-1 pr-3 pl-0" />
                  )}
                  {/* correct answers */}
                  {question?.correctAnswer
                    ?.split(",")
                    ?.map((item) => Number(item))
                    .includes(choice.order) && (
                    <i className="fa-solid fa-check pt-1 pr-3 pl-0" />
                  )}
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{choice.text}</p>
                </span>
              ) : (
                <span className="d-flex pt-3">
                  {String.fromCharCode(65 + index)}.
                  <p className="pl-2">{choice.text}</p>
                </span>
              )}
            </RFlex>
            {choice?.files && choice?.files.length > 0 && (
              <img
                src={Services.storage.file + choice?.files[0]?.hash_id}
                alt={choice?.id}
                className={styles.image}
              />
            )}
          </div>
        ))}
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveMultipleChoice;
