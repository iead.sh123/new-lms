import React, { useContext } from "react";
import AppModal from "components/Global/ModalCustomize/AppModal";
import { primaryColor } from "config/constants";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useParams } from "react-router-dom";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import Loader from "utils/Loader";
import RDiscussionPost from "./RDiscussionPost";
import RDiscussionPostDesign from "./RDiscussionPostDesign";

const RContentOfThePost = () => {
  const { cohortId, categoryName, sectionId, mandatoryFlag } = useParams();

  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  return (
    <AppModal
      show={DiscussionData.seeTheContentOfThePost}
      parentHandleClose={DiscussionData.handleCloseSeeTheContentOfThePost}
      size={"xl"}
      header={DiscussionData?.cPost?.user?.full_name}
      headerSort={
        <React.Fragment>
          {DiscussionData.getPostByIdLoading ? (
            <Loader />
          ) : DiscussionData.loadPreviousLoading ? (
            <Loader />
          ) : (
            <RDiscussionPostDesign
              post={DiscussionData?.cPost}
              mB={"mb-4"}
              ml={"ml-0"}
              typeContent="post"
              community={mandatoryFlag ? false : true}
              specificMandatoryPost={true}
              indRec={0}
              seeMoreComment={false}
            />
          )}
        </React.Fragment>
      }
    />
  );
};

export default RContentOfThePost;
