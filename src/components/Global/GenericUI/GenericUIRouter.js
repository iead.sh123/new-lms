import React, { useEffect, useState } from "react";

import { Route, Switch, useParams } from "react-router-dom";
import Helper from "../RComs/Helper";
import routes from "routes/genericui";

const GenericUIRouter = (props) => {
  const getRoutes = (routes) => {
    return routes.map((prop, key) => {
      return (
        <Route
          // path={"/generic-ui-lister"+prop.path}
          component={prop.component}
          path={"/admin/generic-ui-lister/schools"}
          layout="/admin"
          key={key}
        />
      );
    });
  };

  return (
    <>
      The lister is <Switch>{getRoutes(routes)}</Switch>
    </>
  );
};

export default GenericUIRouter;
