import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { get, post } from "config/api";
import { toast } from "react-toastify";
import GenericUIRelations from "components/Global/GenericUI/GenericUIManyToMany/GenericUIRelations";
import getTabsForEntity from "./GenericUtils";
import RRoutesTabsPanel from "../RComs/RRoutesTabsPanel";
import Loader from "utils/Loader";

const GenericEditor = () => {
	const { entity_specific_name, entity_specific_id } = useParams();
	const [tabs, setTabs] = useState(getTabsForEntity(entity_specific_name));
	const changeHorizontalTabs = () => {};
	const [extraTabsLoaded, setExtraTabsLoaded] = useState(false);

	const [getTitleField, setGetTitleField] = useState("");
	const [getTableTitle, setGetTableTitle] = useState("");

	useEffect(() => {
		const getData = async () => {
			const response = await get("api/generic/tabs/" + entity_specific_name);

			if (response && response.data && response.data.data) {
				const temptabs = [];
				response?.data?.data?.map((tab_data) => {
					if (tab_data.pivotTableName && tab_data.pivotTableName.length > 1) {
						let tab = {
							// masterTableName: "school_groups"
							// detailTableName: "schools"
							// pivotTableName: "schools"
							// manyToMany: false
							// title: "Schools"
							title: tab_data.title,
							extraRouteParams:
								tab_data.masterTableName +
								"/" +
								tab_data.detailTableName +
								"/" +
								tab_data.pivotTableName +
								"/" +
								(tab_data.manyToMany ? "1" : "0"),
							RouteParams: ":masterTableName/:detailTableName/:pivotTableName/:manyToMany",

							content: () => {
								return <GenericUIRelations />;
							},
						};
						temptabs.push(tab);

						//alert("pushed tabs.length now is : "+tabs.length);
					}
				});
				setTabs([...tabs, ...temptabs]);

				setExtraTabsLoaded(true);
			}
		};
		getData();
	}, []);

	const fetchGetTableData = async () => {
		await post(`/api/generic/get-table-data`, {
			payload: { TableName: entity_specific_name, id: entity_specific_id },
		})
			.then((response) => {
				setGetTableTitle(response.data.data?.table_props?.table_title);
				setGetTitleField(response.data.data?.records[response.data.data?.title_field]);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		fetchGetTableData();
	}, []);

	return (
		<>
			{/* <>{tabs.length}</> */}

			{extraTabsLoaded ? (
				<RRoutesTabsPanel
					title={`${getTitleField} (${getTableTitle}) Editor`}
					Tabs={tabs}
					SelectedIndex={entity_specific_name}
					changeHorizontalTabs={changeHorizontalTabs}
					baseUrl={`${process.env.REACT_APP_BASE_URL}/admin/manage/${entity_specific_name}/${entity_specific_id}`}
					baseRoute={`${process.env.REACT_APP_BASE_URL}/admin/manage/:entity_specific_name/:entity_specific_id`}
					Url={
						entity_specific_id
							? `${process.env.REACT_APP_BASE_URL}/admin/manage/${entity_specific_name}/${entity_specific_id}/`
							: `${process.env.REACT_APP_BASE_URL}/admin/add/${entity_specific_name}/`
					}
				></RRoutesTabsPanel>
			) : (
				Loader()
			)}
		</>
	);
};

export default GenericEditor;
