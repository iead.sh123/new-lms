import React from "react";
import Helper from "components/Global/RComs/Helper";
import RSubmit from "components/Global/RComs/RSubmit";
import { useState } from "react";
import { Col, Row } from "reactstrap";
import RTextArea from "components/Global/RComs/RTextArea";
import tr from "components/Global/RComs/RTranslator";
import { useDispatch, useSelector } from "react-redux";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { curriculaManagmentApi } from "api/seniorTeacher/CurriculaManagment";
import { HIDE_NOTES_INITIATOR } from "store/actions/global/globalTypes";
import styles from "./NewNote.Module.scss";
import { toast } from "react-toastify";

function NewNote() {
	const current_note = useSelector((state) => state.chat.current_note);
	const show = useSelector((state) => state.chat.showNotesInitiator);
	const dispatch = useDispatch();

	const [msgInput, setMesgInput] = useState("");

	const msgOnChange = (e) => {
		setMesgInput(e.target.value);
	};
	const sendMessage = async () => {
		if (msgInput == "") {
			alert("You cannot send a blank message");
			return;
		}
		//----------------------------------------------------
		const request = {
			payload: {
				message: msgInput,
				table_name: current_note.table_name,
				row_id: current_note.row_id,
				context: current_note.context,
				context_id: current_note.context_id,
			},
		};
		let response = null;

		response = await curriculaManagmentApi.initialNoteHistory(request);
		if (response.data.status == 1) {
			setMesgInput("");
			dispatch({ type: HIDE_NOTES_INITIATOR });
		} else {
			toast.error(response.data.msg);
		}
		return response;
	};

	const _handleKeyDown = (e) => {
		if (e.key === "Enter") {
		}
	};
	const display = show ? "block" : "none";
	const hideNoteInitiator = () => {
		dispatch({ type: HIDE_NOTES_INITIATOR });
	};
	return (
		<div id="NewNote" className="NewNote" style={{ display: `${display}` }}>
			<Row>{tr("add_notes_on_this_item")} : </Row>
			<span className={styles.closeCircle} onClick={hideNoteInitiator}>
				{" "}
				<i className={"nc-icon nc-simple-remove " + styles.iconeClose}></i>{" "}
			</span>
			<Row>{current_note && current_note.nameToShow ? current_note.nameToShow : null}</Row>
			<Row>{/* {tableName} ({id}) */}</Row>

			<Row>
				<Col md="12">
					<RTextArea
						value={msgInput}
						onChange={msgOnChange}
						onKeyDown={_handleKeyDown}
						placeholder={tr("new_message")}
						ClassNames="new-messege-input-area_notes textClass mb-3  "
					></RTextArea>
				</Col>
			</Row>
			<Row style={{ float: "right" }}>
				<RSubmit
					value={tr("send_message")}
					style={{ textAlign: "right" }}
					onClick={() => {
						sendMessage();
					}}
				/>
			</Row>
		</div>
	);
}

export default NewNote;
