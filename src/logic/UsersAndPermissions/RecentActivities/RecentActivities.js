import React, { useEffect, useReducer } from "react";
import { Row, Col } from "reactstrap";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { getRecentAddedRolesAsync, getRecentUserTypesAsync, getRecentActivitiesAsync } from "../State/UsersAndPermissions.action";
import RRecentActivities from "view/UsersAndPermissions/RecentActivities/RRecentActivities";
import RRecentUserTypes from "view/UsersAndPermissions/RecentActivities/RRecentUserTypes";
import RecentAddedUser from "./RecentAddedUser";
import Loader from "utils/Loader";
import RRole from "view/UsersAndPermissions/RRole";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
 import REmptyData from "components/RComponents/REmptyData";

const RecentActivities = () => {
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	useEffect(() => {
		getRecentAddedRolesAsync(dispatch);
		getRecentUserTypesAsync(dispatch);
		getRecentActivitiesAsync(dispatch);
	}, []);

	return (
		<section>
			<Row style={{ marginBottom: "55px" }} className="p-0">
				<Col>
					{usersAndPermissionsData.recentUserTypesLoading ? (
						<Loader />
					) : (
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{usersAndPermissionsData.recentUserTypes.slice(0, 4).map((userType) => (
								<RRecentUserTypes recentUserType={userType} />
							))}
						</RFlex>
					)}
				</Col>
			</Row>
			<Row style={{ marginBottom: "55px" }}>
				<Col>
					<RFlex>
						<h6>{tr`recently_added_users`}</h6>
					</RFlex>
					<RecentAddedUser />
				</Col>
			</Row>
			<Row>
				<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
					<h6 className="mb-3">{tr`recently_added_roles`}</h6>

					<RFlex styleProps={{ flexWrap: "wrap" }}>
						{usersAndPermissionsData.recentAddedRolesLoading ? (
							<Loader />
						) : usersAndPermissionsData.recentAddedRoles.length > 0 ? (
							usersAndPermissionsData.recentAddedRoles.map((role) => <RRole role={role} lister={true} />)
						) : (
							<div style={{ width: "100%" }}>
								<REmptyData removeImage={true} line1={tr`no_recently_added_roles`} />
							</div>
						)}
					</RFlex>
				</Col>
				<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
					<h6>{tr`recent_activities`}</h6>
					{usersAndPermissionsData.recentActivitiesLoading ? (
						<Loader />
					) : (
						<RRecentActivities usersAndPermissionsData={usersAndPermissionsData} />
					)}
				</Col>
			</Row>
		</section>
	);
};

export default RecentActivities;
