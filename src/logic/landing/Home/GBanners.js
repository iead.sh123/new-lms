import React, { useContext } from "react";
import { LandingPageContext } from "./GHome";
import { SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import styles from "./Home.module.scss";
import Image1 from "assets/img/new/auth.png";
import Image2 from "assets/img/new/image_2.webp";
import Image3 from "assets/img/new/image_3.webp";

const imageArray = [Image1, Image2, Image3];
const GBanners = () => {
	const LandingPageData = useContext(LandingPageContext);

	return (
		<section className={styles.banner__width}>
			<RSwiper
				slidesPerView={1}
				navigation={false}
				modules={[Autoplay]}
				autoplay={{
					delay: 2500,
					disableOnInteraction: false,
				}}
				className={styles.swiper__width}
			>
				{LandingPageData &&
					LandingPageData.landingPage &&
					LandingPageData.landingPage.headers &&
					LandingPageData.landingPage.headers.map((banner, ind) => (
						<SwiperSlide key={ind}>
							<div className={styles.image}>
								<img src={imageArray[ind]} alt={banner.Brief} />
							</div>
							<div className={styles.info}>
								<h4>{banner.Title}</h4>
								<h6>{banner.Brief}</h6>
								<p>{banner.Description}</p>
							</div>
						</SwiperSlide>
					))}
			</RSwiper>
		</section>
	);
};

export default GBanners;
