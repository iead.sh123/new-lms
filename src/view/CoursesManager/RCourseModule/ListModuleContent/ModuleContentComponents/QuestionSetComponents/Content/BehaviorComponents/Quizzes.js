import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { FormGroup, Input } from "reactstrap";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import moment from "moment";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const Quizzes = ({ content }) => {
  const CourseModuleData = useContext(CourseModuleContext);

  return (
    <div className="mt-2">
      <RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
        <AppCheckbox
          onChange={(event) =>
            CourseModuleData.handleChangeBehaviorInModuleContent({
              moduleId: CourseModuleData.specificModule.id,
              moduleContentId: content.id,
              key: "startDate",
              value: event.target.checked == true ? " " : null,
            })
          }
          label={tr`open_date`}
          checked={content?.behavior?.startDate ? true : false}
        />
        {content?.behavior?.startDate && (
          <FormGroup>
            <Input
              name="startDate"
              type="datetime-local"
              placeholder={tr`start_date`}
              value={moment(content?.behavior?.startDate).format(
                "YYYY-MM-DDTHH:mm"
              )}
              style={{ height: "30px", width: "200px" }}
              onChange={(event) => {
                const localDateTime = new Date(event.target.value);

                CourseModuleData.handleChangeBehaviorInModuleContent({
                  moduleId: CourseModuleData.specificModule.id,
                  moduleContentId: content.id,
                  key: event.target.name,
                  value: localDateTime.toISOString(),
                });
              }}
            />
          </FormGroup>
        )}
      </RFlex>
      <RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
        <AppCheckbox
          onChange={(event) =>
            CourseModuleData.handleChangeBehaviorInModuleContent({
              moduleId: CourseModuleData.specificModule.id,
              moduleContentId: content.id,
              key: "endDate",
              value: event.target.checked == true ? " " : null,
            })
          }
          label={tr`duration`}
          checked={content?.behavior?.endDate ? true : false}
        />
        {content?.behavior?.endDate && (
          <FormGroup>
            <Input
              name="endDate"
              type="number"
              placeholder={tr`duration_in_minute`}
              defaultValue={
                content?.hasBehaviour
                  ? moment(content?.behavior?.endDate).diff(
                      moment(content?.behavior?.startDate),
                      "minutes"
                    )
                  : content?.behavior?.endDate
              }
              disabled={!moment(content?.behavior?.startDate).isValid()}
              style={{ height: "30px", width: "200px" }}
              onChange={(event) => {
                const minutesToAdd = parseInt(event.target.value, 10);

                const startDateTime = moment(content?.behavior?.startDate);

                // Check if startDateTime is valid
                if (startDateTime.isValid()) {
                  const newDateTime = startDateTime.add(
                    minutesToAdd,
                    "minutes"
                  );
                  // Now, newDateTime represents the result after adding minutesToAdd to startDateTime
                  const localDateTime = new Date(newDateTime);

                  CourseModuleData.handleChangeBehaviorInModuleContent({
                    moduleId: CourseModuleData.specificModule.id,
                    moduleContentId: content.id,
                    key: event.target.name,
                    value: newDateTime.toISOString(),
                  });
                } else {
                  console.error("Invalid start date");
                }
              }}
            />
          </FormGroup>
        )}
      </RFlex>
    </div>
  );
};

export default Quizzes;
