import { produce } from "immer";

export const attribute_scheme = produce(
  {
    id: null,
    name: { valid: false, value: "" },
    value: { valid: false, value: null },
    // type: {}
  },
  (draft) => draft
);
