import { get, post } from "config/api";

export const studentHomeAPis = {
  getNotifications: () => get("std-api/notifications"),
  getViewNotifications: () => get("api/notifications/getNotifications"),
  getImportantNotifications: () => post("std-api/importantNotifications"),
  getCurrentLessons: () => get("api/events/CurrentLessons"),

  getUpcomingEvents: () => get("api/events/StudentHomePageEvents"),

  getCalendarEvents: () => get("api/events/StudentCalendarEvents"),
  setFirebaseToken: (token) => post(`api/firebase/set/${token}`),
  markNotificationAsRead: (notification_id) =>
    post(`std-api/notifications/markAsRead/${notification_id}`),
};
