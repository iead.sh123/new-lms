import React, { useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useReactToPrint } from "react-to-print";
import { useParams } from "react-router-dom";
import { getUnitPlan } from "store/actions/global/unitPlan.actions";
import withDirection from "hocs/withDirection";
import RPrinting from "../RComs/RPrinting/RPrinting";
import RButton from "components/Global/RComs/RButton";
import Loader from "../Loader";
import tr from "../RComs/RTranslator";

const Grinting = ({ dir }) => {
  const { unitId } = useParams();
  const dispatch = useDispatch();

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  const { unitPlan, loader } = useSelector((state) => state.unitPlan);

  useEffect(() => {
    // dispatch(getUnitPlan(unitId, null));
  }, []);

  return (
    <>
      {loader ? (
        <Loader />
      ) : (
        <div className="content">
          <RButton
            text={tr`print`}
            onClick={handlePrint}
            color="primary"
            outline
          />
          <RPrinting data={unitPlan} ref={componentRef} dir={dir} />
        </div>
      )}
    </>
  );
};

export default withDirection(GUnitPlanPrinting);
