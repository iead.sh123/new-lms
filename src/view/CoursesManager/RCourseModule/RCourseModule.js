import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { primaryColor } from "config/constants";
import { Services } from "engine/services";
import RAddCourseModule from "./AddCourseModuleForm/RAddCourseModule";
import RAllModuleTabs from "./ModuleTabs/RAllModuleTabs";
import RButton from "components/Global/RComs/RButton";
import DefaultImage from "assets/img/new/course-default-cover.png";
import styles from "../CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Lamp from "assets/img/new/svg/lamp.svg";
import Tips from "assets/img/new/tips.png";
import tr from "components/Global/RComs/RTranslator";
import { useWindowWidth } from "hocs/useWindowWidth";
import RModuleBySpecificTab from "./ModuleTabs/RModuleBySpecificTab";
import { useLocation } from "react-router-dom";
import iconsFa6 from "variables/iconsFa6";
import { boldGreyColor } from "config/constants";

const RCourseModule = () => {
	const screenWidth = useWindowWidth();
	const location = useLocation();

	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<React.Fragment>
			{/* image cover */}
			<section className={styles.img_section}>
				<img
					className={styles.course_image}
					src={
						CourseModuleData?.courseModule?.course?.image_id == undefined
							? DefaultImage
							: `${Services.courses_manager.file}${CourseModuleData?.courseModule?.course?.image_id}`
					}
					alt="image_cover"
				/>
				<div className={styles.categoryBreadCrumb}>
					{CourseModuleData?.categoryAncestors?.length > 0 ? (
						<>
							{tr`create new course in`} : {CourseModuleData?.categoryAncestors?.map((name) => name + " _ ")}
						</>
					) : (
						""
					)}
				</div>
			</section>

			<RFlex styleProps={{ flexDirection: screenWidth < 991 ? "column" : "row" }}>
				{/* actions on course Modules */}
				<div style={{ width: screenWidth < 991 ? "100%" : "65%" }}>
					{!CourseModuleData.studentView && CourseModuleData?.courseModule?.modules?.length > 0 && (
						<RButton
							text={tr`create_new_module`}
							faicon="fa fa-plus"
							onClick={CourseModuleData?.handleShowCreateModuleForm}
							color="primary"
							disabled={CourseModuleData.createModule}
						/>
					)}

					{/* add module form */}
					<section style={{ marginTop: "10px" }}>{CourseModuleData.createModule && <RAddCourseModule />}</section>
					<section style={{ width: CourseModuleData?.selectedButton == 0 ? "100%" : screenWidth < 991 ? "100%" : "100%" }}>
						{CourseModuleData.studentView && <RAllModuleTabs />}
					</section>
				</div>
				<div style={{ width: screenWidth < 991 ? "100%" : "35%", height: "100%" }}>
					<RFlex styleProps={{ justifyContent: "end" }}>
						{!CourseModuleData.studentView ? (
							<RButton
								text={tr`view_as_learner`}
								faicon="fa fa-graduation-cap"
								onClick={CourseModuleData.handleViewAsStudent}
								color="link"
								style={{ color: primaryColor }}
							/>
						) : ["student", "learner"].includes(CourseModuleData?.user?.type) ? (
							""
						) : (
							<RButton
								text={tr`exit_view_as_learner`}
								faicon="fa fa-graduation-cap"
								onClick={CourseModuleData.handleExitViewAsStudent}
								color="link"
								style={{ color: primaryColor }}
							/>
						)}
						<RButton
							text={tr`view_course_map`}
							faicon="fa fa-list-ul"
							onClick={CourseModuleData.handleOpenCourseMapDrawer}
							color="primary"
							outline
						/>
					</RFlex>
				</div>
			</RFlex>

			<RFlex styleProps={{ flexDirection: screenWidth < 991 ? "column" : "row" }}>
				<div style={{ width: CourseModuleData?.selectedButton == 0 ? "100%" : screenWidth < 991 ? "100%" : "65%" }}>
					{/* all modules OR no modules yet */}
					<section>
						{CourseModuleData?.courseModule?.modules?.length == 0 ? (
							<>
								{!CourseModuleData.createModule && (
									<RFlex styleProps={{ flexDirection: "column", justifyContent: "center" }}>
										<p style={{ color: boldGreyColor }}>{tr`there is no modules yet`}</p>

										{CourseModuleData.studentView ? (
											""
										) : (
											<RFlex styleProps={{ alignItems: "center" }}>
												<p className="pt-3" style={{ color: boldGreyColor }}>{tr`you can add a module by`}</p>
												<RButton
													text={tr`create_new_module`}
													faicon="fa fa-plus"
													color="primary"
													onClick={CourseModuleData?.handleShowCreateModuleForm}
												/>
												<p className="pt-3" style={{ color: boldGreyColor }}>{tr`and start building the course content`}</p>
											</RFlex>
										)}
									</RFlex>
								)}
							</>
						) : (
							<section style={{ width: CourseModuleData?.selectedButton == 0 ? "100%" : screenWidth < 991 ? "100%" : "100%" }}>
								{!CourseModuleData.studentView && <RAllModuleTabs />}
								<RModuleBySpecificTab />
							</section>
						)}
					</section>
				</div>
				{/* this part from design in course module not found in all modules */}
				{CourseModuleData?.selectedButton !== 0 && (
					<div style={{ width: screenWidth < 991 ? "100%" : "35%", height: "100%" }}>
						{CourseModuleData?.selectedButton !== 0 && (
							<section xs={12} lg={4} className={styles.container}>
								{!CourseModuleData.studentView ? (
									""
								) : ["student", "learner"].includes(CourseModuleData?.user?.type) ? (
									""
								) : (
									<RFlex className="mt-2 justify-content-end">
										<i className={iconsFa6.cogs} aria-hidden="true" />
										<p>{tr`module_advanced_settings`}</p>
									</RFlex>
								)}

								{/* <RFlex className={`justify-content-end mb-4 ${CourseModuleData.studentView ? styles.viewStudent__margin : ""}`}>
									{Object.entries(
										CourseModuleData.specificModule && CourseModuleData.studentView
											? CourseModuleData.specificModule.published_statistics
											: CourseModuleData.specificModule.statistics
									)
										.map(([key, value]) => `${value} ${key}`)
										.join(" | ")}
								</RFlex> */}
								<section className={`${styles.tips} `}>
									<div className={styles.course_div}>
										<img src={Tips} className={styles.tips_img} />
									</div>
									<div className={styles.tips_content}>
										<RFlex>
											<img src={Lamp} />

											<h6 className="pt-2"> {tr`Tips for faster work`}</h6>
										</RFlex>
										{["student", "learner"].includes(CourseModuleData?.user?.type) ? (
											<ul>
												<li>{tr`Study for 25-30 minutes, then take a 5-10 minute break.`}</li>
												<li>{tr`Don't be afraid to ask for help when you need it.`}</li>
												<li>{tr`Find a good learning environment.`}</li>
												<li>{tr`Sleep helps your brain consolidate memories and make new connections.`}</li>
											</ul>
										) : (
											<ul>
												<li>{tr`Consider outsourcing or delegating some tasks`}</li>
												<li>{tr`Be clear, concise, and comprehensive. Everything has to be there`}</li>
												<li>{tr`Provide a variety of learning activities`}</li>
												<li>{tr`Repurpose existing content`}</li>
											</ul>
										)}
									</div>
								</section>
							</section>
						)}
					</div>
				)}
			</RFlex>
		</React.Fragment>
	);
};

export default RCourseModule;
