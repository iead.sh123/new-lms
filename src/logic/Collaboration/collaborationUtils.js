import { baseURL } from "engine/config";
import { genericPath } from "engine/config";

export const redirect = (history, table_name, record_id) => {
	if (table_name == "courses") {
		history.push(`${baseURL}/${genericPath}/course/${record_id}`);
	}
	//  else if(table_name=="lesson_plans")
	//  {
	//     history.push(`${baseURL}/${genericPath}/course/${record_id}`);
	//  }
	else if (table_name == "unit_plans") {
		history.push(`${baseURL}/${genericPath}/unit_plan/${record_id}`);
	} else if (table_name == "lessons") {
		history.push(`${baseURL}/${genericPath}/course/${record_id}`);
	}
};
