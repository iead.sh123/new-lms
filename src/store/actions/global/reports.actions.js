import {
	GET_ALL_REPORTS_SUCCESS,
	GET_ALL_REPORTS_ERROR,
	GET_REPORT_By_ID_ERROR,
	SHOW_REPORT_GRID_VIEW_ERROR,
	SHOW_REPORT_GRID_VIEW_SUCCESS,
	GET_REPORT_PARAMETERS_ERROR,
	GET_REPORT_PARAMETERS_SUCCESS,
	GET_REPORT_By_ID_SUCCESS,
	SELECTED_ITEM_TO_PRINT_REPORT,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { reportApi } from "api/report";
import { toast } from "react-toastify";

export const ShowAllReports = () => {
	return (dispatch) => {
		reportApi
			.getAllReport()
			.then((response) => {
				if (response.data.status === 1) {
					var listReport = response.data;
					dispatch({
						type: GET_ALL_REPORTS_SUCCESS,
						listReport,
					});
				} else {
					dispatch({
						type: GET_ALL_REPORTS_ERROR,
						actionError: response.data.msg,
					});
				}
			})
			.catch((error) => {
				dispatch({ type: GET_ALL_REPORTS_ERROR, actionError: error.message });
			});
	};
};

export const GetReportParameters = (ReportId) => {
	return (dispatch) => {
		reportApi
			.getReportParameter(ReportId)
			.then((response) => {
				if (response.data.status === 1) {
					var reportParameter = response.data;
					dispatch({
						type: GET_REPORT_PARAMETERS_SUCCESS,
						reportParameter,
					});
				} else {
					dispatch({
						type: GET_REPORT_PARAMETERS_ERROR,
						actionError: response.data.msg,
					});
				}
			})
			.catch((error) => {
				toast.error(error.message);
			});
	};
};

export const SHowReportGridView = (ReportId, ParametersValue) => {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			reportApi
				.viewReportGridView(ReportId, ParametersValue)
				.then((response) => {
					if (response.data.status === 1) {
						var reportGirdView = response.data;
						dispatch({
							type: SHOW_REPORT_GRID_VIEW_SUCCESS,
							reportGirdView,
						});
						resolve(response);
					} else {
						dispatch({
							type: SHOW_REPORT_GRID_VIEW_ERROR,
							actionError: response.data.msg,
						});
					}
				})
				.catch((error) => {
					reject(error);
				});
		});
	};
};

export const GetReportById = (ReportId) => {
	return (dispatch) => {
		reportApi
			.viewReportById(ReportId)
			.then((response) => {
				if (response.data.status === 1) {
					var report = response.data.data.report;
					dispatch({
						type: GET_REPORT_By_ID_SUCCESS,
						report,
					});
				} else {
					dispatch({
						type: GET_REPORT_By_ID_ERROR,
						actionError: response.data.msg,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const SelectedItemToPrint = (SelectedItem) => {
	return (dispatch) => {
		dispatch({
			type: SELECTED_ITEM_TO_PRINT_REPORT,
			SelectedItem: SelectedItem,
		});
	};
};
