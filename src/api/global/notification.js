import { get, post } from "config/api";
import { backend } from "engine/config";
import { Services } from "engine/services";

export const notificationApi = {
    notificationsUnSeen: () => get(`${ Services.notficiation.backend}api/notification/unseen`),
    viewNotifications: (notification_ids) =>
    post(`${Services.notficiation.backend}api/notification/view-notifications/${notification_ids}`),
    setNotifyMe: (notify) =>
    post(`${Services.notficiation.backend}api/notification/notifyme`,{notifyme:notify}),
};

