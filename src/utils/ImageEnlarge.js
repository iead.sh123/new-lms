import React, { useState } from "react";

const ImageEnlarge = ({ imageSrc, imageStyle }) => {
  const [isOpen, setIsOpen] = useState(false);
  const handleShowDialog = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div>
      <img
        style={imageStyle}
        // className="assignment_image"
        src={imageSrc}
        onClick={handleShowDialog}
        width="30%"
        height="30%"
      />
      {isOpen && (
        <dialog className="dialog" open={isOpen} onClick={handleShowDialog}>
          <img
            style={imageStyle}
            // className="assignment_image_large"
            src={imageSrc}
            onClick={handleShowDialog}
            width="100%"
            height="100%"
          />
        </dialog>
      )}
    </div>
  );
};

export default ImageEnlarge;
