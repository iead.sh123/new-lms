import { get, post, destroy, patch, put } from "config/api";
import { Services } from "engine/services";

export const coursesManagerApi = {
	getCreators: (search) => get(`${Services.courses_manager.backend}api/creators${search ? `?search=${search}` : ""}`),

	enrollInCourse: (courseId) => post(`${Services.courses_manager.backend}api/courses/enroll/${courseId}`),

	updateQuestionSetNameFromModuleContent: (type, contentId, data) =>
		post(`${Services.courses_manager.backend}api/module-contents/types/${type}/${contentId}/update`, data),

	// getCourseCategoriesTree: (forDropDown, searchText, withoutCourses) =>
	// 	get(
	// 		`${Services.courses_manager.backend}api/categories-tree${
	// 			forDropDown
	// 				? `?forDropDown=${forDropDown}${searchText ? `&=searchText${searchText}` : ""}${
	// 						withoutCourses ? `&=withoutCourses${withoutCourses}` : ""
	// 				  }`
	// 				: ""
	// 		}${searchText ? `?=searchText${searchText}` : ""}${withoutCourses ? `&=withoutCourses${withoutCourses}` : ""}`
	// 	),

	getCourseCategoriesTree: ({ forDropDown, searchText, withoutCourses, withoutDiscounts }) => {
		let queryParams = "";

		if (forDropDown) {
			queryParams += `?forDropDown=${forDropDown}`;
			if (searchText) queryParams += `&searchText=${searchText}`;
			if (withoutCourses) queryParams += `&withoutCourses=${withoutCourses}`;
			if (withoutDiscounts) queryParams += `&withoutDiscounts=${withoutDiscounts}`;
		} else {
			if (searchText) queryParams += `?searchText=${searchText}`;
			if (withoutCourses && withoutDiscounts) {
				queryParams += searchText ? "&" : "?";
				queryParams += `withoutCourses=${withoutCourses}`;
				queryParams += `&withoutDiscounts=${withoutDiscounts}`;
			}
		}

		const url = `${Services.courses_manager.backend}api/categories-tree${queryParams}`;
		return get(url);
	},
	addCategoryToTree: (data) => post(`${Services.courses_manager.backend}api/v1/course/category/store`, data),

	deleteCategoryFromTree: (categoryID) => destroy(`${Services.courses_manager.backend}api/v1/course/category/delete/${categoryID}`),

	getCoursesByCategoryId: (data) => post(`${Services.courses_manager.backend}api/courses/search`, data),

	searchCoursesCatalogAsync: (data) => get(`${Services.courses_manager.backend}api/course-catalogue/filter${JSON.stringify(data)}`),

	cloneToCourse: (courseId) => post(`${Services.courses_manager.backend}api/courses/${courseId}/clone`),

	instantiateToCourse: (courseId) => post(`${Services.courses_manager.backend}api/courses/${courseId}/instantiate`),

	removeCourse: (courseId) => destroy(`${Services.courses_manager.backend}api/courses/${courseId}`),

	categoryAncestors: (categoryId) => get(`${Services.courses_manager.backend}api/category-ancestors/${categoryId}`),

	saveCourse: (data, client) => post(`${Services.courses_manager.backend}api/courses${client ? `?client=${client}` : ""}`, data),
	updateCourse: (data, course_id) => patch(`${Services.courses_manager.backend}api/courses/${course_id}`, data),

	courseById: (courseId) => get(`${Services.courses_manager.backend}api/courses/${courseId}`),

	levels: () => get(`${Services.courses_manager.backend}api/levels`),

	getUserType: (forDropDownList, userType) =>
		get(
			`${Services.auth_organization_management.backend}api/organization/${userType}${
				forDropDownList ? `?forDropDownList=${forDropDownList}` : ""
			}`
		),

	saveCourseOverview: (courseId, data) => post(`${Services.courses_manager.backend}api/courses/${courseId}/overview`, data),

	getCourseOverview: (courseId) => get(`${Services.courses_manager.backend}api/courses/${courseId}/overview`),

	publishCourse: (courseId) => post(`${Services.courses_manager.backend}api/courses/${courseId}/publish`),

	unPublishCourse: (courseId) => post(`${Services.courses_manager.backend}api/courses/${courseId}/un-publish`),

	allFeedbacks: (courseId, searchText) =>
		get(`${Services.courses_manager.backend}api/courses/${courseId}/feedbacks${searchText ? `?searchText=${searchText}` : ""}`),

	patchOnCourseInformation: (data, client, courseId) =>
		patch(`${Services.courses_manager.backend}api/courses/${courseId}${client ? `?client=${client}` : ""}`, data),

	getCourseModule: (courseId, viewAsStudent) =>
		get(`${Services.courses_manager.backend}api/courses/${courseId}/modules${viewAsStudent ? `?viewAsStudent=${viewAsStudent}` : ""}`),

	getModuleContent: (moduleId) => get(`${Services.courses_manager.backend}api/modules/${moduleId}`),

	createModule: (courseId, data) => post(`${Services.courses_manager.backend}api/courses/${courseId}/modules`, data),

	updateModule: (moduleId, data) => patch(`${Services.courses_manager.backend}api/modules/${moduleId}`, data),

	deleteModule: (moduleId) => destroy(`${Services.courses_manager.backend}api/modules/${moduleId}`),

	deleteModuleContent: (id) => destroy(`${Services.courses_manager.backend}api/module-contents/${id}`),

	createQuestionSetToModuleContent: (moduleId, type) =>
		post(`${Services.lesson.backend}module-context/create-and-link/modules/${moduleId}/type/${type}`),

	publishModuleContent: (moduleContentId) => post(`${Services.courses_manager.backend}api/module-contents/publish/${moduleContentId}`),

	unPublishModuleContent: (moduleContentId) => post(`${Services.courses_manager.backend}api/module-contents/unpublish/${moduleContentId}`),

	getMyCourses: (searchText, termId) =>
		get(
			`${Services.courses_manager.backend}api/my-courses${
				searchText && !termId
					? `?search=${searchText}`
					: termId && !searchText
					? `?termId=${termId}`
					: searchText && termId
					? `?search=${searchText}&termId=${termId}`
					: ""
			}`
		),

	allFiltersToCourses: () => get(`${Services.courses_manager.backend}api/courses/filter`),

	allCoursesFilter: (user, organizationId, data) =>
		post(
			`${Services.courses_manager.backend}api/courses/filter${user ? "" : organizationId ? `?organizationId=${organizationId}` : ""}`,
			data
		),

	landingPage: (user, organizationId) => {
		const url = `${Services.courses_manager.backend}api/landing-page${organizationId ? `/${organizationId}` : ""}`;
		return get(url);
	},

	removeModuleContent: (contentId) => destroy(`${Services.courses_manager.backend}api/module-contents/${contentId}`),

	//----------------------------------MyCurricula----------------------------------
	getMyCurricula: (searchText, semesterId) =>
		get(
			`${Services.auth_organization_management.backend}api/curricula/myCurricula?semester_id=${semesterId}${
				searchText ? `&search=${searchText}` : ""
			}`
		),
	//--------------------------------------------------------------------------------

	//----------------------------------Students----------------------------------
	allStudents: (courseId) => get(`${Services.courses_manager.backend}api/courses/${courseId}/lister/learners`),
	//--------------------------------------------------------------------------------

	clonedCourse: (data) => post(`${Services.courses_manager.backend}api/courses/copy/context`, data),

	organizationStudents: ({ url: url, termId: termId, gradeLevelIds: gradeLevelIds, noPagination: noPagination, orderAlpha: orderAlpha }) =>
		get(
			url
				? url
				: `${
						Services.courses_manager.backend
				  }api/users/terms/${termId}/students-with-enrolled-classes?gradeLevelIds[]=${gradeLevelIds}&noPagination=${noPagination}${
						orderAlpha ? `&orderAlpha=${orderAlpha}` : ""
				  }`
		),

	usersEnrolledInCourse: ({ courseId, userType, orderBy }) =>
		get(`${Services.courses_manager.backend}api/courses/${courseId}/users/type/${userType}${orderBy ? `?orderBy=${orderBy}` : ""}`),

	unEnrollUsersFromCourse: ({ courseId, data }) =>
		post(`${Services.courses_manager.backend}api/courses/${courseId}/users/delete-enrollment`, data),
	enrollUsersInCourse: ({ courseId, data }) =>
		put(`${Services.courses_manager.backend}api/courses/${courseId}/learners-and-teachers`, data),

	allCategories: (unCategorized) => get(`${Services.courses_manager.backend}api/all-categories${unCategorized ? `/${unCategorized}` : ""}`),

	copyBulkCourses: ({ data }) => post(`${Services.courses_manager.backend}api/courses/copy/context/bulk`, data),

	coursesByDiscountId: (discountId, sort) =>
		get(`${Services.courses_manager.backend}api/discount/${discountId}/courses${sort ? `?sort=${sort}` : ""}`),
};
