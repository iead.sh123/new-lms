import React from "react";
import { Col, Row } from "reactstrap";
import styles from "./RStepper.Module.scss";

const RStep = ({ steps, selected, level = 0 }) => {
  let numStyle = { color: "black", border: "" };
  let nameStyle = { color: "black", border: "" };

  return (
    <>
      {steps.map((step) => {
        numStyle = { color: "black", border: "" };
        nameStyle = { color: "black", border: "" };
        if (step.num == selected) {
          numStyle.color = "#fd822b";
          //   numStyle.border = "1px solid #fd822b";
          nameStyle.color = "#fd822b";
          //   nameStyle.border = "1px solid #fd822b";
        }
        return (
          <div
            className={styles.assessment}
            key={steps.num}
            style={{ position: "relative", left: `${level * 30}px` }}
          >
            <Row class="step-item" style={{ position: "relative" }}>
              {/* <Col className={styles.navAssessment} md={2}>
                {step.num}
              </Col> */}
              <Col className={styles.navAssessment} md={12}>
                <Col xs={2} style={numStyle}>
                  {step.num}
                </Col>
                <Col xs={10} style={nameStyle}>
                  {step.name}
                </Col>
              </Col>
            </Row>
            {step.steps ? (
              <RStep steps={step.steps} selected={selected} level={level + 1} />
            ) : null}
          </div>
        );
      })}
    </>
  );
};

export default RStep;
