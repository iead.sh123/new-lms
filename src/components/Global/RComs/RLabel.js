// import React, { useRef } from "react";
// import { UncontrolledTooltip } from "reactstrap";

// export const RLabel = ({
// 	value,
// 	id1 = 0,
// 	key = -1,
// 	tooltiped = true,
// 	hidden = false,
// 	lettersToShow = 5,
// 	dangerous = false,
// 	tooltipText = "",
// 	icon,
// }) => {
// 	// const generateUniqueId = (prefix) => {
// 	// 	const max = 1000000;
// 	// 	return prefix + Math.floor(Math.random() * max);
// 	// };
// 	// const id = generateUniqueId("data" + (cutCondition ? (trimmedValue.substring(0, lettersToShow) + id1).replace(/\s/g, "") : value));
// 	const cutCondition = (typeof value === "string" || value instanceof String) && value.length > lettersToShow;

// 	const trimmedValue = cutCondition ? value.replace(/[^a-zA-Z ]/g, "") : value;
// 	const max = 1000000;
// 	const id =
// 		"data" + Math.floor(Math.random() * max) + (cutCondition ? (trimmedValue.substring(0, lettersToShow) + id1).replace(/\s/g, "") : value);

// 	const tooltipId = `Tooltip-${value}-${Math.random().toString(36).substr(2, 5)}`;

// 	return (
// 		<>
// 			{tooltiped && cutCondition ? (
// 				<>
// 					<div
// 						style={{ display: "inline-block" }}
// 						hidden={hidden}
// 						key={key}
// 						className="text-center"
// 						id={tooltipId}
// 						dangerouslySetInnerHTML={
// 							dangerous
// 								? {
// 										__html: value.replace(/<[^>]*>?/gm, "").substring(0, lettersToShow) + "..",
// 								  }
// 								: null
// 						}
// 					>
// 						{icon ? <i className={`${icon} `} aria-hidden="true" /> : dangerous ? null : value.substring(0, lettersToShow) + ".."}{" "}
// 					</div>{" "}
// 					<UncontrolledTooltip delay={0} target={tooltipId}>
// 						{tooltipText && tooltipText.length > 0 ? tooltipText : value.replace(/<[^>]*>?/gm, "")}
// 					</UncontrolledTooltip>
// 				</>
// 			) : typeof value === "string" ? (
// 				<div
// 					style={{ display: "inline-block" }}
// 					hidden={hidden}
// 					key={key}
// 					className="text-center"
// 					dangerouslySetInnerHTML={
// 						dangerous
// 							? {
// 									__html: value.replace(/<[^>]*>?/gm, ""),
// 							  }
// 							: null
// 					}
// 				>
// 					{dangerous ? null : value}
// 				</div>
// 			) : typeof value === "number" ? (
// 				<div style={{ display: "inline-block" }} hidden={hidden} key={key} className="text-center">
// 					{value}
// 				</div>
// 			) : typeof value === "boolean" ? (
// 				<div style={{ display: "inline-block" }} hidden={hidden} key={key} className="text-center">
// 					{value ? "YES" : "NO"}
// 				</div>
// 			) : null}
// 		</>
// 	);
// };

import React from "react";
import { UncontrolledTooltip } from "reactstrap";
import Helper from "./Helper";
import tr from "./RTranslator";

export const RLabel = ({
	value,
	id1 = 0,
	style,
	key = -1,
	color,
	tooltiped = true,
	hidden = false,
	lettersToShow = 5,
	dangerous = false,
	tooltipText = "",
	icon,
	otherStyle,
	showDots = true
}) => {
	const cutCondition = (typeof value === "string" || value instanceof String) && value.length > lettersToShow;

	const trimmedValue = cutCondition ? value.replace(/[^a-zA-Z ]/g, "") : value;
	const max = 1000000;
	const id =
		"data" + Math.floor(Math.random() * max) + (cutCondition ? (trimmedValue.substring(0, lettersToShow) + id1).replace(/\s/g, "") : value);
	//Helper.cl(id, "tool tip id");

	// const [ErrorBoundary, error] = useErrorBoundary();

	//   if (error) {
	//     // Handle the error here or render an error UI
	//     return <div>Oops! Something went wrong.</div>;
	//   }

	//   return (
	//     <ErrorBoundary>
	//       {/* Your functional component code here */}
	//       <div>Hello, World!</div>
	//     </ErrorBoundary>
	//   );
	// };

	return (
		<>
			{/* {typeof value != "object" ? value:"novalue"} */}
			{tooltiped && cutCondition ? (
				<>
					<div
						style={{ display: "inline-block", ...otherStyle }}
						hidden={hidden}
						key={key}
						className="text-center"
						// dangerouslySetInnerHTML={{
						//   __html:
						//     value?.length < HtmlLettersToShow
						//       ? value
						//       : value.substring(0, HtmlLettersToShow) + "...",
						// }}
						id={id}
						dangerouslySetInnerHTML={
							dangerous
								? {
									__html: value.replace(/<[^>]*>?/gm, "").substring(0, lettersToShow) + (showDots ? ".." : ""),
								}
								: null
						}
					>
						{icon ? <i className={`${icon} `} aria-hidden="true" /> : dangerous ? null : value.substring(0, lettersToShow) + (showDots ? ".." : "")}{" "}
					</div>{" "}
					<UncontrolledTooltip delay={0} target={id}>
						{tooltipText && tooltipText.length > 0 ? tooltipText : value.replace(/<[^>]*>?/gm, "")}
					</UncontrolledTooltip>
				</>
			) : typeof value === "string" ? (
				<div
					style={{ display: "inline-block", ...otherStyle }}
					hidden={hidden}
					key={key}
					className="text-center"
					dangerouslySetInnerHTML={
						dangerous
							? {
								__html: value.replace(/<[^>]*>?/gm, ""),
							}
							: null
					}
				>
					{/* {value} */}
					{dangerous ? null : value}
				</div>
			) : typeof value === "number" ? (
				<div style={{ display: "inline-block" }} hidden={hidden} key={key} className="text-center">
					{value}
				</div>
			) : typeof value === "boolean" ? (
				<div style={{ display: "inline-block" }} hidden={hidden} key={key} className="text-center">
					{value ? "YES" : "NO"}
				</div>
			) : null}
		</>
	);
};
