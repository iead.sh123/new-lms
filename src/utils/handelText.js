import React from 'react';
import { baseUrl } from "../config/constants";
import { Link } from "react-router-dom";
export const HASHTAG_FORMATTER = string => {
    return   string.split(/((?:^|\s)(?:#[a-z\d-]+))/gi).filter(Boolean).map((tag, i) => {
        if (tag.includes('#')) {
            return <><Link to={baseUrl + "filter-tag/" + tag}  className="tag" key={i}
             style={{ color: 'blue',cursor: 'pointer' }}>
                 {tag}</Link>&nbsp;</>
        } else {
            return <></>
        }
    })
};

export const getFileExtension = (filename) => {
    return filename.slice(((filename.lastIndexOf(".") - 1) >>> 0) + 2);
  };


  export  const getFileNameWithoutExtension = (filename) => {
    const lastIndex = filename.lastIndexOf(".");
    if (lastIndex === -1) {
      return filename;
    } else {
      return filename.slice(0, lastIndex);
    }
  };