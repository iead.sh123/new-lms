import React from "react";
import RFormItem from "./RFormItem";
import { Table } from "reactstrap";
import withDirection from "hocs/withDirection";
import RSubmit from "./RSubmit";
import styles from "./RTableForm.Module.scss";

// , type={element: must provide the element|input}
// rows = [[{ onChange,onClick,onBlur, value, valid,disabled ...etc,element: null }, { onChange,onClick,onBlur, value, valid,disabled ...etc,element: null }]]

const RTableForm = ({
  headers,
  onSubmit,
  rows = [], //formItems
  submitLabel = "Submit",
  disabled,
  className,
  dir,
}) => {
  return (
    <div
      style={{
        maxHeight: "300px",
        overflow: "auto",
      }}
    >
      <Table className={`${styles["Table"]} ${className}`}>
        <thead style={{ backgroundColor: "#A1A1A1", fontWeight: "bold" }}>
          <tr style={{ textAlign: "center" }}>
            {headers.map((h) => (
              <th className="fixed_th" id={h}>
                {h}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((r) => {
            return (
              <tr key={r.key}>
                {r.elements.map((rl) => {
                  const { element, disabled, ...fiProps } = rl;
                  return rl.element ? (
                    <td>{rl.element} </td>
                  ) : rl.disabled ? (
                    <td className="no-padding-no-margin text-center">
                      {rl.value}
                    </td>
                  ) : (
                    <td className="no-padding-no-margin text-center">
                      <RFormItem data={fiProps} dir={dir} />
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </Table>
      {onSubmit && (
        <RSubmit
          dir={dir}
          value={submitLabel}
          disabled={disabled}
          type="button"
          onClick={onSubmit}
        />
      )}
    </div>
  );
};

export default withDirection(RTableForm);
