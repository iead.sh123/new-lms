export const repeat = {
    Daily: 'DAILY',
    Monthly: 'MONTHLY',
    Yearly: 'YEARLY',
    Weekly: 'WEEKLY',
    CUSTOM: 'CUSTOM',//period
    ONCE: 'ONCE'
}