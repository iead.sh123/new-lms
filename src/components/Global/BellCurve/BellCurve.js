import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bellCurveAsync } from "store/actions/global/bellCure.action";
import { useParams } from "react-router-dom";
import { Row, Col } from "reactstrap";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
import bellCurve from "highcharts/modules/histogram-bellcurve"; //module
import Loader from "utils/Loader";
bellCurve(Highcharts);

const BellCurve = ({ rab_id, name }) => {
  const chartComponentRef = useRef(null);

  const { rabId } = useParams();
  const dispatch = useDispatch();
  const { bellCurve, bellCurveMark, bellCurveName, bellCurveLoading } =
    useSelector((state) => state.bellCurveRed);

  useEffect(() => {
    if (!rab_id) {
      dispatch(
        bellCurveAsync({
          payload: { TableName: "rabs", id: rabId },
        })
      );
    }
  }, []);

  return (
    <Row>
      {bellCurveLoading ? (
        <Loader />
      ) : (
        <Col xs={12}>
          <HighchartsReact
            constructorType={"chart"}
            ref={chartComponentRef}
            highcharts={Highcharts}
            options={{
              title: {
                text: name ? name : "Bell curve",
              },

              xAxis: [
                {
                  title: {
                    text: "Data",
                  },
                  alignTicks: false,
                  visible: false,
                  categories: bellCurveName,
                },
                {
                  title: {
                    text: "Bell curve",
                  },
                  alignTicks: false,
                  opposite: true,
                  // visible: false,
                },
              ],

              yAxis: [
                {
                  title: { text: "Data" },
                },
                {
                  title: { text: "Bell curve" },
                  opposite: true,
                  // visible: false,
                },
              ],

              series: [
                {
                  name: "Bell Curve",
                  type: "bellcurve",
                  xAxis: 1,
                  yAxis: 1,
                  intervals: 4,
                  baseSeries: 1,
                  zIndex: -1,
                  marker: {
                    enabled: true,
                  },
                },

                {
                  name: "Data",
                  type: "scatter",
                  data: bellCurveMark,
                  accessibility: {
                    exposeAsGroupOnly: true,
                  },
                  marker: {
                    radius: 1.5,
                    // symbol: "triangle",
                  },
                },
              ],
            }}
          />
        </Col>
      )}
    </Row>
  );
};

export default BellCurve;
