FROM node:16-alpine AS react_builder
WORKDIR /app
# Copy package files and install dependencies
COPY package.json yarn.lock ./
RUN apk add --no-cache git openssh && \
    yarn install
# Copy source code and build the React app
COPY . .
RUN yarn build
 
 
# Use an official PHP runtime as a parent image with Nginx included
 
FROM php:7.4-fpm
 
# Install nginx
 
RUN apt-get update && apt-get install -y nginx
 
# Remove the default server definition
 
RUN rm /etc/nginx/sites-enabled/default
 
# Copy configuration files
 
COPY nginx.conf /etc/nginx/nginx.conf
 
COPY site.conf /etc/nginx/conf.d/site.conf
 
COPY site.conf /etc/nginx/sites-enabled/site.conf
 
# Copy your React build folder to the container
 
COPY --from=react_builder /app/build /var/www/html
 
# Expose port 80 and 443 for the web servers
 
EXPOSE 80 443
 
# Start Nginx and PHP-FPM services
 
CMD php-fpm -D && nginx -g 'daemon off;'
 