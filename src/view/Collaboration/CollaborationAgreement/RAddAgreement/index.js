import React from "react";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../../Collaboration.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import { boldGreyColor } from "config/constants";
import RSwitch from "components/RComponents/RSwitch";

const RAddAgreement = ({ formProperties, data, loading, handlers, agreementId }) => {
	const organizationOptions = ({ innerProps, label, data }) => {
		return (
			<></>
			// <RFlex styleProps={{ alignItems: "center" }} key={data.value} className="pl-3 pr-3 pt-3 cursor-pointer" {...innerProps}>
			// 	<img src={data.image} alt={data.label} width="17px" height="20px" />
			// 	<p>{label}</p>
			// </RFlex>
		);
	};
	return (
		<RFlex className={styles.padding__modal} styleProps={{ width: "100%", flexDirection: "column", gap: "15px" }}>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
				<RFlex styleProps={{ width: "50%" }}>
					<span>{tr`create_new_agreement_with`}</span>
				</RFlex>
				<RFlex styleProps={{ width: "60%", flexDirection: "column" }}>
					<RSelect
						option={formProperties.organizations}
						closeMenuOnSelect={false}
						placeholder={tr`levels`}
						onChange={(e) => {}}
						loading={formProperties.organizationsLoading}
						isDisabled={formProperties.organizationsLoading}
						customOption={organizationOptions}
					/>
				</RFlex>
			</RFlex>
			{data?.agreementsList?.map((agreement) => (
				<RFlex key={agreement.id} styleProps={{ width: "100%", justifyContent: "space-between" }}>
					<RFlex styleProps={{ width: "50%", alignItems: "end" }}>
						<AppNewCheckbox onChange={(event) => {}} checked={false} label={agreement.name} />
					</RFlex>
					<RFlex styleProps={{ width: "60%", flexDirection: "column" }}>
						<span style={{ color: boldGreyColor }}>{tr`levels`}</span>
						<RSelect
							option={data?.levels?.map((item) => {
								return { label: item.name, value: item.id };
							})}
							closeMenuOnSelect={false}
							placeholder={tr`levels`}
							onChange={(e) => {}}
							loading={formProperties.levelsLoading}
							isDisabled={formProperties.levelsLoading}
						/>
					</RFlex>
				</RFlex>
			))}
			<RFlex styleProps={{ alignItems: "center", gap: "5px" }}>
				<span style={{ fontSize: "12px" }}>{tr`active`}</span>

				<RSwitch onChange={() => {}} checked={true} width={32} height={15} onColor="#46c37e" onHandleColor="#fff" />
			</RFlex>
			<RFlex>
				<RButton
					type="submit"
					text={agreementId ? tr`save` : tr`create`}
					color="primary"
					loading={agreementId ? loading.updateAgreementLoading : loading.createAgreementLoading}
					disabled={agreementId ? loading.updateAgreementLoading : loading.createAgreementLoading}
				/>
				<RButton
					text={tr`cancel`}
					color="link"
					onClick={() => {
						handlers.handleCloseEditorAgreement();
						formProperties.resetForm();
					}}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RAddAgreement;
