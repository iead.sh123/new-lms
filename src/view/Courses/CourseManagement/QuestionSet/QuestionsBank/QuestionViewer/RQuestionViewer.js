import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { Services } from "engine/services";
import ViewMultipleChoiceSingleAnswer from "./QuestionType/ViewMultipleChoiceSingleAnswer";
import ViewMultipleChoice from "./QuestionType/ViewMultipleChoice";
import ViewFillInBlank from "./QuestionType/ViewFillInBlank";
import ViewTrueOrFalse from "./QuestionType/ViewTrueOrFalse";
import ViewUploadFile from "./QuestionType/ViewUploadFile";
import ViewOrdering from "./QuestionType/ViewOrdering";
import ViewMatch from "./QuestionType/ViewMatch";
import ViewEssay from "./QuestionType/ViewEssay";
import ViewDraw from "./QuestionType/ViewDraw";
import styles from "./QuestionViewer.module.scss";

const RQuestionViewer = () => {
  const QuestionsBankData = useContext(QuestionsBankContext);

  const questionTypeViewerComponents = {
    MULTIPLE_CHOICES_MULTIPLE_ANSWERS: ViewMultipleChoice,
    MULTIPLE_CHOICES_SINGLE_ANSWER: ViewMultipleChoiceSingleAnswer,
    ESSAY: ViewEssay,
    FILL_IN: ViewFillInBlank,
    UPLOAD_FILE: ViewUploadFile,
    TRUEFALSE: ViewTrueOrFalse,
    SEQUENCING: ViewOrdering,
    DRAW: ViewDraw,
    MATCH: ViewMatch,
  };

  return (
    <div>
      {QuestionsBankData?.questionsBankSelected?.isGroup && (
        <div>
          <h6>{QuestionsBankData?.questionsBankSelected?.title}</h6>
          {QuestionsBankData?.questionsBankSelected?.files[0]?.hash_id !==
            undefined && (
            <img
              src={
                Services.storage.file +
                QuestionsBankData?.questionsBankSelected?.files[0]?.hash_id
              }
              alt={"group_image"}
              className={styles.image}
            />
          )}
        </div>
      )}
      {QuestionsBankData?.questionsBankSelected?.questions?.map(
        (question, questionIndex) => {
          const QuestionViewerComponent =
            questionTypeViewerComponents[question?.type];

          if (QuestionViewerComponent) {
            return (
              <QuestionViewerComponent
                question={question}
                isGroup={QuestionsBankData?.questionsBankSelected?.isGroup}
              />
            );
          }
          return null;
        }
      )}
    </div>
  );
};

export default RQuestionViewer;
