import { produce, current } from "immer";
import { ADD_NEW_FILTER_ON_LISTER } from "store/actions/admin/adminType";

export const newFilterState = {
  newFilter: null,
};

const newFilterReducer = (state = newFilterState, action) => {
  return produce(state, (draft) => {
    switch (action?.type) {
      case ADD_NEW_FILTER_ON_LISTER:
        draft.newFilter = action.data;
        return draft;

      default:
        return state;
    }
  });
};
export default newFilterReducer;
