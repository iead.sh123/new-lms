import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMyCoursesAsync } from "store/actions/global/coursesManager.action";
import Loader from "utils/Loader";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col } from "reactstrap";
import RCard from "components/Global/RComs/RCards/RCard";
import { baseURL, genericPath } from "engine/config";
import { Services } from "engine/services";
import { useHistory } from "react-router-dom";
import DefaultImage from "assets/img/new/course-default-cover.png";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { SwiperSlide } from "swiper/react";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import { useMemo } from "react";
import REmptyData from "components/RComponents/REmptyData";

const GMyCourses = ({ advanced = true, setCount, perView = 3 }) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const [searchData, setSearchData] = useState("");
	const [courses, setCourses] = useState([]);

	const { myCoursesLoading, myCourses } = useSelector((state) => state.coursesManagerRed);
	const { user } = useSelector((state) => state.auth);
	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		dispatch(getMyCoursesAsync(emptySearch ?? searchData));
	};

	useEffect(() => {
		dispatch(getMyCoursesAsync());
	}, []);

	useEffect(() => {
		if (myCourses && myCourses.length >= 0) {
			buildMyCourses();
		}
	}, [myCourses]);

	const buildMyCourses = () => {
		let courses1 = [];
		if (user?.organization_type?.toLowerCase() == "school" && user?.type?.toLowerCase() == "teacher") {
			myCourses?.map((course, index) => {
				let data = {};
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;

				data.id = course.id;
				data.name = course.name;
				data.isPublished = course.is_published;
				data.isMaster = course.is_master;
				data.color = course.color;
				data.currentInfo = [
					{
						name: "students",
						count: course.students,
						icon: "fa-solid fa-user",
					},
					{
						name: "posts",
						count: course.posts,
						icon: "fa-solid fa-pen-to-square",
					},
					{
						name: "learners",
						count: course.learners_count,
						icon: "fa-solid fa-user",
					},
					{
						name: "teachers",
						count: course.teachers_count,
						icon: "fa-solid fa-user",
					},
				];
				data.users = course.teachers;

				data.actions = [];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		} else if (
			user?.organization_type?.toLowerCase() !== "school" &&
			(user?.type?.toLowerCase() == "teacher" || user?.type?.toLowerCase() == "facilitator")
		) {
			myCourses?.map((course, index) => {
				let data = {};
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;

				data.id = course.id;
				data.name = course.name;
				data.isPublished = course.is_published;
				data.isMaster = course.is_master;
				data.color = course.color;
				data.rate = course.rating;
				data.currentInfo = [
					{
						name: "students",
						count: course.students,
						icon: "fa-solid fa-user",
					},

					{
						name: "learners",
						count: course.learners_count,
						icon: "fa-solid fa-user",
					},
					{
						name: "teachers",
						count: course.teachers_count,
						icon: "fa-solid fa-user",
					},
				];
				data.users = course.teachers;

				data.actions = [];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		} else if (
			user?.organization_type?.toLowerCase() == "school" &&
			(user?.type?.toLowerCase() == "student" || user?.type?.toLowerCase() == "parent")
		) {
			myCourses?.map((course, index) => {
				let data = {};
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;

				data.id = course.id;
				data.name = course.name;
				// data.isPublished = course.is_published;
				data.isMaster = course.is_master;
				data.color = course.color;
				data.isFree = course?.extra?.isFree;
				data.comingSoon = course?.extra?.comingSoon;
				data.isOneDayCourse = course?.extra?.isOneDayCourse;
				data.isOnline = course?.extra?.isOnline;
				data.currentInfo = [
					{
						name: "students",
						count: course.students,
						icon: "fa-solid fa-user",
					},
					{
						name: "posts",
						count: course.learners_count,
						icon: "fa-solid fa-pen-to-square",
					},
				];
				data.users = course.teachers;

				data.actions = [];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		} else if (
			user?.organization_type?.toLowerCase() !== "school" &&
			(user?.type?.toLowerCase() == "student" || user?.type?.toLowerCase() == "learner")
		) {
			myCourses?.map((course, index) => {
				let data = {};
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;

				data.id = course.id;
				data.name = course.name;
				// data.isPublished = course.is_published;
				data.isMaster = course.is_master;
				data.color = course.color;
				data.progress = course.progress;
				data.rate = course.rating;
				data.isFree = course.extra.isFree;
				data.comingSoon = course.extra.comingSoon;
				data.isOneDayCourse = course.extra.isOneDayCourse;
				data.isOnline = course.extra.isOnline;
				data.users = course.teachers;

				data.actions = [];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		} else {
			myCourses?.map((course, index) => {
				let data = {};
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;

				data.id = course.id;
				data.name = course.name;
				data.isPublished = course.is_published;
				data.isMaster = course.is_master;
				data.color = course.color;
				data.progress = course.progress;
				data.rate = course.rating;
				data.isFree = course.extra.isFree;
				data.comingSoon = course.extra.comingSoon;
				data.isOneDayCourse = course.extra.isOneDayCourse;
				data.isOnline = course.extra.isOnline;
				data.users = course.teachers;

				data.actions = [];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		}

		setCourses(courses1);
	};

	useEffect(() => {
		if (setCount && typeof setCount == "function") setCount(courses && courses?.length > 0 ? courses?.length : 0);
	}, [courses]);

	const courseCards = useMemo(
		() =>
			courses.map((course) => {
				const card = (
					<RCard
						course={course}
						hiddenFlag={user?.organization_type?.toLowerCase() !== "school" && user?.type?.toLowerCase() == "teacher" ? false : true}
					/>
				);

				return <>{advanced ? card : <SwiperSlide style={{ display: "relative", width: "340px!important" }}>{card}</SwiperSlide>}</>;
			}),
		[courses]
	);

	// if(!advanced){
	//   return <>  {myCourses && myCourses.length > 0 && (
	//     <>
	//       <h6>{tr`enrolled_courses`}</h6>
	//       <h6 className="text-muted">
	//         {myCourses.length} {tr`courses`}
	//       </h6>
	//     </>
	//   )}</>
	// }
	return (
		<React.Fragment>
			{/* {<p>organization_type -- {user?.organization_type}</p>}
			{<p>type -- {user?.type}</p>} */}
			{advanced ? (
				<RFlex
					styleProps={{
						flexWrap: "wrap",
						justifyContent: "space-between",
						marginBottom: "16px",
					}}
				>
					<RFlex styleProps={{ alignItems: "center" }}>
						{myCourses && myCourses.length > 0 && (
							<>
								<h6>{tr`enrolled_courses`}</h6>
								<h6 className="text-muted">
									{myCourses.length} {tr`courses`}
								</h6>
							</>
						)}
					</RFlex>
					<RSearchHeader
						searchLoading={myCoursesLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						inputPlaceholder={tr`search`}
						addNew={false}
						inputWidth={"30%"}
					/>
				</RFlex>
			) : (
				<></>
			)}
			{myCoursesLoading ? (
				<Loader />
			) : (
				<React.Fragment>
					<RFlex styleProps={{ flexWrap: "wrap", gap: 30 }}>
						{courses && courses?.length > 0 ? (
							advanced ? (
								courseCards
							) : (
								<div style={{ width: "70vw" }}>
									<RSwiper slidesPerView={perView} navigation={true}>
										{courseCards}
									</RSwiper>
								</div>
							)
						) : (
							<Row>
								<Col>
									<REmptyData />
								</Col>
							</Row>
						)}
					</RFlex>
				</React.Fragment>
			)}
		</React.Fragment>
	);
};

export default GMyCourses;
