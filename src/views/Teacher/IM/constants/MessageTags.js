export const MessageTags = {
    Starred: 'starred',
    Important: 'important',
    Reported: 'reported',
    Urgent: 'urgent',
    Unsend: 'unSend',
    Delete: 'delete',
    Forward: 'forward',
    Reply: "reply",
}

export const ICONS = {
    [MessageTags.Starred]: 'fa fa-star',
    [MessageTags.Important]: 'fa fa-id-card',
    [MessageTags.Reported]: 'fa-solid fa-flag',
    [MessageTags.Urgent]: 'fa fa-free-code-camp',
    [MessageTags.Unsend]: 'fa-solid fa-comment-slash',
    [MessageTags.Delete]: 'fa-regular fa-trash-can',
    [MessageTags.Forward]: 'fa-solid fa-share-from-square',
    [MessageTags.Reply]: 'fa fa-reply',
}



