import { produce, current } from "immer";
import {
  ADD_DYNAMIC_NODES,
  SET_CURRENT_NODE,
  GO_BACK,
  SET_CURRENT_NODE_BY_LINK,
  SET_LAYOUT,
  SET_SITE_MAP,
  BIND_DYNAMIC_NODES,
} from "store/actions/global/globalTypes";
import { get } from "config/api";

import types from "../../../variables/userTypes";
import { backend } from "engine/config";

const dynIdsGenerator = (function* () {
  let i = 1;
  while (true) yield `dyn-${++i}`;
})();

const siteTrees = {
  [types.TEACHER]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      //Calendar
      {
        id: 1,
        link: "/calender",
        name: "Calender",
      },
      //Reports
      {
        id: 2,
        link: "/reports-ui",
        name: "Reports",
      },
      {
        id: 3,
        link: "/collaboration-center",
        name: "Collaboration Center",
      },
      //unit plan
      {
        id: 4,
        link: "/course-unitplan",
        name: "Unit Plan",
      },

      //Discussions
      {
        id: 5,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 6,
        link: "/IM",
        name: "Messages",
      },

      {
        id: 7,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 8,
        link: "/settings",
        name: "Settings",
      },
      {
        id: 9,
        link: "/assesment-plan",
        name: "Assessment Plan",
      },
      {
        id: 10,
        link: "/dashboard",
        name: "Home",
      },
      {
        id: 11,
        link: "/abuse",
        name: "Abuse",
      },
      {
        id: 12,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },

  [types.STUDENT]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [],
  },
  [types.PARENT]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [],
  },
  [types.ADMIN]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      {
        id: 1,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 2,
        link: "/IM",
        name: "Messages",
      },
      {
        id: 3,
        link: "/assessment-plans",
        name: "Assessment Plans",
      },
      {
        id: 4,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 5,
        link: "/abuse",
        name: "Abuse",
      },
      {
        id: 6,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },
  [types.PRINCIPLE]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      //Live sessions
      {
        id: 1,
        link: "/live-sessions",
        name: "LiveSessions",
      },
      //Collaboration
      {
        id: 2,
        link: "/collaboration-center",
        name: "Collaboration",
      },
      //CollaborationOps
      {
        id: 3,
        link: "/collaboration-operations",
        name: "Collaboration Operations",
      },
      //Im
      {
        id: 4,
        link: "/instant-messaging",
        name: "IM",
      },
      //Discussions
      {
        id: 5,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 6,
        link: "/dashboard",
        name: "Home",
      },
      {
        id: 7,
        link: "/assessment-plans",
        name: "Assessment Plans",
      },
      {
        id: 8,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 9,
        link: "/abuse",
        name: "Abuse",
      },
      {
        id: 10,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },
  [types.SENIORTEACHER]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      //Live sessions
      {
        id: 1,
        link: "/live-sessions",
        name: "LiveSessions",
      },
      //Collaboration
      {
        id: 2,
        link: "/collaboration-center",
        name: "Collaboration",
      },
      //CollaborationOps
      {
        id: 3,
        link: "/collaboration-operations",
        name: "Collaboration Operations",
      },
      //Im
      {
        id: 4,
        link: "/instant-messaging",
        name: "IM",
      },
      //Discussions
      {
        id: 5,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 6,
        link: "/dashboard",
        name: "Home",
      },
      {
        id: 7,
        link: "/assessment-plans",
        name: "Assessment Plans",
      },
      {
        id: 8,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 9,
        link: "/abuse",
        name: "Abuse",
      },
      {
        id: 10,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },
  [types.SCHOOL_ADVISOR]: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      //Live sessions
      {
        id: 1,
        link: "/live-sessions",
        name: "LiveSessions",
      },
      //Collaboration
      {
        id: 2,
        link: "/collaboration-center",
        name: "Collaboration",
      },
      //CollaborationOps
      {
        id: 3,
        link: "/collaboration-operations",
        name: "Collaboration Operations",
      },
      //Im
      {
        id: 4,
        link: "/instant-messaging",
        name: "IM",
      },
      //Discussions
      {
        id: 5,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 6,
        link: "/dashboard",
        name: "Home",
      },
      {
        id: 7,
        link: "/assessment-plans",
        name: "Assessment Plans",
      },
      {
        id: 8,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 9,
        link: "/abuse",
        name: "Abuse",
      },

      {
        id: 10,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },
};

const initState = {
  activeNodeId: 0,
  navStack: [0],
  navTitle: "home",
  documentationLink: "",
  siteTree: {
    id: 0,
    name: "Home",
    link: "/dashboard",
    items: [
      //Calendar
      {
        id: 1,
        link: "/calender",
        name: "Calender",
      },
      //Reports
      {
        id: 2,
        link: "/reports-ui",
        name: "Reports",
      },
      {
        id: 3,
        link: "/collaboration-center",
        name: "Collaboration Center",
      },
      //unit plan
      {
        id: 4,
        link: "/course-unitplan",
        name: "Unit Plan",
      },

      //Discussions
      {
        id: 5,
        link: "/discussions",
        name: "Discussions",
      },
      {
        id: 6,
        link: "/IM",
        name: "Messages",
      },

      {
        id: 7,
        link: "/profile",
        name: "My Profile",
      },
      {
        id: 8,
        link: "/settings",
        name: "Settings",
      },
      {
        id: 9,
        link: "/dashboard",
        name: "Home",
      },

      {
        id: 10,
        link: "/abuse",
        name: "Abuse",
      },
      {
        id: 11,
        link: "/announcements",
        name: "Announcements",
      },
    ],
  },
};

const NavigationReducer = (state = initState, action) =>
  produce(state, (draft) => {
    let temp = null;
    switch (action.type) {
      case SET_LAYOUT:
        draft.siteTree = siteTrees[action.payload];
        draft.activeNodeId = 0;
        draft.navStack = [0];
        return draft;

      case SET_SITE_MAP:
        draft.siteTree = siteTrees[action.payload];
        return draft;

      case ADD_DYNAMIC_NODES:
        draft.siteTree.items.push(
          ...action.payload.filter(
            (nd) =>
              draft.siteTree.items.findIndex((tn) => tn.name === nd.name) < 0
          )
        );
        return draft;
      case SET_CURRENT_NODE:
        draft.activeNodeId = action.payload;
        // draft.navStack.push(action.payload);
        return draft;

      case SET_CURRENT_NODE_BY_LINK:
        temp = findNodeIdByLink(current(draft.siteTree), action.payload);
        temp !== false && (draft.activeNodeId = temp);
        // draft.navStack.push(temp);
        return draft;

      case BIND_DYNAMIC_NODES:
        temp = findNodeById(draft.siteTree, draft.activeNodeId);
        if (temp !== false) {
          let it = temp.items || [];
          temp.items = [
            ...it.filter(
              (it) => action.payload.nodes.findIndex((n) => n.id === it.id) < 0
            ),
            ...action.payload.nodes,
          ];
        }
        return draft;

      case GO_BACK:
        draft.activeNodeId =
          draft.navStack.lenght > 1 ? draft.navStack.pop() : draft.navStack[0];
        return draft;
      default:
        return draft;
    }
  });

export const getSiteMap = async (dispatch, layout = types.TEACHER) => {
  layout !== types.TEACHER && dispatch({ type: SET_LAYOUT, payload: layout });
  const res = await get(`${backend}api/getSiteMap`);
  if (res) {
    let nodes = [];
    switch (layout) {
      case types.TEACHER:
        //---------
        nodes = getTeacherDynamicSiteMap(res.data.data);
        break;
      case types.SENIORTEACHER:
      case types.PRINCIPLE:
      case types.SCHOOL_ADVISOR:
        nodes = getSeniorTeacherSiteMap(res.data.data);
        break;
    }

    dispatch({ type: ADD_DYNAMIC_NODES, payload: nodes });
  }
};

const getTeacherDynamicSiteMap = (data) => {
  const nodes = [];
  data?.children?.map((c) => {
    let node = null;
    if (c.type === "courses") {
      node = {
        id: dynIdsGenerator.next().value,
        link: "/courses",
        name: "Courses",
        items: [],
      };
      c.children.map((rab) => {
        const lessonNodes = rab.children.map((lesson) => {
          return {
            id: `lesson${lesson.item.id}`,
            link:
              "/courses/" +
              rab.item.course_id +
              `/rabs/${rab.item.id}` +
              `/lessons/${lesson.item.id}/lesson`,
            name: lesson.item.subject,
            items: [
              {
                id: "lsn-details" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Lesson Details`,
                name: "details",
              },
              {
                id: "lsn-plan" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Lesson Plan`,
                name: "plan",
              },
              {
                id: "lsn-rec" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Lesson Recordings`,
                name: "recordings",
              },
              {
                id: "lsn-supp" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Lesson Supplements`,
                name: "supplements",
              },
              {
                id: "lsn-attach" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Lesson Attachments`,
                name: "attachments",
              },
              {
                id: "lsn-exa" + lesson.item.id,
                link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/lessons/${lesson.item.id}/Assignments Exams`,
                name: "assignments exams",
              },
            ],
          };
          // rabnode.items.push(lessonnode);
        });

        let rabnode = {
          id: `rab${rab.item.id}`,
          link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}`,
          name: rab.title,
          items: [
            {
              id: "rb" + rab.item.id + "lsns",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Lessons`,
              name: "lessons",
              items: lessonNodes,
            },
            {
              id: "rb" + rab.item.id + "stds",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Students`,
              name: "Students",
            },
            {
              id: "rb" + rab.item.id + "gds",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Grades`,
              name: "grades",
            },
            {
              id: "rb" + rab.item.id + "gding",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Grading Mechanism`,
              name: "grading mechanism",
            },
            {
              id: "rb" + rab.item.id + "topics",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Topics`,
              name: "topics",
            },
            {
              id: "rb" + rab.item.id + "quest",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Question Sets`,
              name: "question",
            },
            {
              id: "rb" + rab.item.id + "rubrc",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Rubrics`,
              name: "rubrics",
            },
            {
              id: "rb" + rab.item.id + "unit",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Unit plan`,
              name: "unit",
            },
            {
              id: "rb" + rab.item.id + "imp",
              link: `/courses/${rab.item.course_id}/rabs/${rab.item.id}/Import`,
              name: "import",
            },
          ],
        };

        node.items.push(rabnode);
      });
    } else if (c.type === "Homeroom") {
      node = {
        id: dynIdsGenerator.next().value,
        link: "/homeRooms",
        name: "Home Rooms",
        items: [],
      };
    } else if (c.type == "Abuse Reports") {
      node = {
        id: dynIdsGenerator.next().value,
        link: "/abuse",
        name: "Abuse Reports",
        items: [],
      };
    } else if (c.type == "Profile") {
      node = {
        id: dynIdsGenerator.next().value,
        link: "/profile",
        name: "Profile",
        items: [],
      };
    } else {
      return;
    }

    nodes.push(node);
  });
  return nodes;
};

export const setAdminSiteMap = (dispatch, routes) => {
  routes = routes.filter((rt) => rt.views);
  const nodes = routes.map((rt) => ({
    id: rt.name + dynIdsGenerator.next().value,
    isGroup: true, //doesn't have path ,just for groupin routes
    name: rt.name,
    link: "#",
    items: rt.views?.map((vw) => ({
      id: vw.name + dynIdsGenerator.next().value,
      link: vw.path,
      name: vw.name,
      items: [],
    })),
  }));
  dispatch({ type: ADD_DYNAMIC_NODES, payload: nodes });

  // routes.map()
};

const getSeniorTeacherSiteMap = (data) => {
  const nodes = [];
  data?.children?.map((c) => {
    if (c.type === "Curriculum") {
      const curriculaNode = {
        id: dynIdsGenerator.next().value,
        link: "/curricula-managment",
        name: "Curricula",
        items: [],
      };

      c.children.map((rab) => {
        let rabnode = {
          id: `rab${rab.id}`,
          link: `/curricula-managment/${rab.id}`,
          name: rab.name,
          items: [
            {
              id: "rb-sen" + rab.id + "resourses",
              link: `/curricula-managment/${rab.id}/Resources`,
              name: "resources",
            },
            {
              id: "rb-sen" + rab.id + "rubrics",
              link: `/curricula-managment/${rab.id}/Rubrics`,
              name: "rubrics",
            },
            {
              id: "rb-sen" + rab.id + "unit",
              link: `/curricula-managment/${rab.id}/Unit Plans`,
              name: "unit plans",
            },
            {
              id: "rb-sen" + rab.id + "lesson",
              link: `/curricula-managment/${rab.id}/Lesson Plans`,
              name: "lesson plans",
            },
            {
              id: "rb-sen" + rab.id + "topics",
              link: `/curricula-managment/${rab.id}/Topics`,
              name: "topics",
            },
            {
              id: "rb-sen" + rab.id + "question",
              link: `/curricula-managment/${rab.id}/Question Sets`,
              name: "question sets",
            },
            {
              id: "rb-sen" + rab.id + "courses",
              link: `/curricula-managment/${rab.id}/Courses`,
              name: "courses",
            },
          ],
        };

        curriculaNode.items.push(rabnode);
      });
      nodes.push(curriculaNode);
    }
  });
  return nodes;
};

/***************with regular expressions to match dynamic routes */

// const findNodeIdByLink = (entryNode, link) => { //optimize when long link
//   link = link.split(/(#|\?)/)[0]; //.toLowerCase();

//   let entryNodeLink = entryNode.link.replaceAll(/(:.*.\/)|(:.*$)/gi, `.*/`);
//   entryNodeLink = entryNodeLink.charAt(entryNodeLink.length - 1) === "/" ? entryNodeLink.substr(0, entryNodeLink.length - 1) : entryNodeLink;

//   entryNodeLink = new RegExp(entryNodeLink, 'gi');

//   if (/*link.toLowerCase() == entryNode.link.toLowerCase()*/ entryNodeLink.test(link)) {
//     return entryNode.id;
//   }

//   if (/*link != entryNode.link &&*/ !entryNode.items) {
//     return false;
//   }

//   if (entryNode.items.length > 0)
//     for (let i = 0; i < entryNode.items.length; i++) {
//       const node = entryNode.items[i];
//       const fin_node = findNodeIdByLink(node, link);
//       if (fin_node) return fin_node;
//     }
//   return false;
// };

const findNodeIdByLink = (entryNode, link) => {
  //optimize when long link
  link = link.split(/(#|\?)/)[0]; //.toLowerCase();

  if (link.toLowerCase() == entryNode.link.toLowerCase()) {
    return entryNode.id;
  }

  if (!entryNode.items) {
    return false;
  }

  if (entryNode.items.length > 0)
    for (let i = 0; i < entryNode.items.length; i++) {
      const node = entryNode.items[i];
      const fin_node = findNodeIdByLink(node, link);
      if (fin_node) return fin_node;
    }
  return false;
};

const findNodeById = (entryNode, id) => {
  //optimize when long link

  if (id === entryNode.id) {
    return entryNode;
  }

  if (!entryNode.items) {
    return false;
  }

  if (entryNode.items.length > 0)
    for (let i = 0; i < entryNode.items.length; i++) {
      const node = entryNode.items[i];
      const fin_node = findNodeById(node, id);
      if (fin_node) return fin_node;
    }
  return false;
};

export const getUserTypePrefix = (type) =>
  type === "teacher"
    ? "teacher"
    : type === types.SCHOOL_ADVISOR ||
      type === types.SENIORTEACHER ||
      type === types.PRINCIPLE
    ? "senior-teacher"
    : type === "employee"
    ? "admin"
    : "student";

export const bindDynamicNodes = (dispatch, nodes, node_id = null) => {
  nodes = nodes.map((nd) => ({
    ...nd,
    id: nd.name + dynIdsGenerator.next().value,
  }));
  dispatch({ type: BIND_DYNAMIC_NODES, payload: { nodes, node_id } });
};

export default NavigationReducer;
