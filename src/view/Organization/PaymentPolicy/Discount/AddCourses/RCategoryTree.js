import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../../../ROrganization.module.scss";

const RCategoryTree = ({ categoriesTree, formProperties, handlers }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			{categoriesTree
				.filter((el) => el.name.toLowerCase() !== "uncategorized")
				.map((category) => (
					<RFlex key={category.id}>
						<p
							className={`m-0 cursor-pointer ${formProperties.values.categoryId == category.id ? styles.heightLight : ""}`}
							onClick={() => handlers.handleChangeCAtegory(category.id)}
						>
							{category?.name?.length > 25 ? category.name?.substring(0, 25) : category.name}
						</p>
						<div className={styles.courses__count}>{category.courses}</div>
					</RFlex>
				))}
		</RFlex>
	);
};

export default RCategoryTree;
