import React from "react";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import tr from "../../../Global/RComs/RTranslator";
const UploadFileQuestion = ({ assignmentReadMode, student }) => {
  return assignmentReadMode && student.detail.notSolved === 0 ? (
    <div className="col-md-3" style={{ textAlign: "center" }}>
      {["png", "jpeg"].includes(student?.student_attachments[0]?.mime_type) ? (
        <ImageEnlarge
          imageSrc={resourcesBaseUrl + student?.student_attachments[0]?.url}
        />
      ) : (
        <a
          href={resourcesBaseUrl + student?.student_attachments[0]?.url}
          alt=""
          class="btn btn-primary"
        >
          {tr`Download`}
        </a>
      )}
    </div>
  ) : (
    <h6 style={{ paddingLeft: 15 }}>No File Uploaded</h6>
  );
};
export default UploadFileQuestion;
