import React, { useContext } from 'react'
import { GradingContext } from './GGradingManagement'
import RLister from 'components/Global/RComs/RLister'
import Loader from 'utils/Loader'
import iconsFa6 from 'variables/iconsFa6'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import Helper from 'components/Global/RComs/Helper'
import styles from './grading.module.scss'
import * as colors from 'config/constants'
import { baseURL, genericPath } from "engine/config";

import { editableFields, tablesTypes } from './Constants'
import RHoverSelect from './RHoverSelect'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import tr from 'components/Global/RComs/RTranslator'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
const RGradableItems = () => {
    const gradingData = useContext(GradingContext)
    console.log('gradingData.gradingState', gradingData.gradingState)
    const groupsNames = gradingData.gradingState?.gradingGroups?.groupsNames
    const gradableItems = gradingData?.gradingState?.gradableItems
    const _gradableItemsRecords = !gradingData.isLoadingGradableItems ? gradableItems?.ids?.map((recordId) => {
        const currentRecord = gradableItems?.[recordId]
        console.log("currentRecord", currentRecord)
        return {
            id: recordId,
            table_name: tablesTypes.gradableItems,
            details: Object.keys(currentRecord)?.map((header) => {
                const alternativesComponent = <RTextIcon
                    flexStyle={{ cursor: "pointer" }}
                    onClick={() => { gradingData.setClickedAlternativesItem({ id: recordId, type: currentRecord['Type'].value, name: currentRecord['Name'].value }); gradingData.setShowAlternativesModal(true) }}
                    className="text-secondary"
                    text={`${currentRecord['Alternatives'].value} Alternatives`}
                    icon='fa-solid fa-file-lines' />
                const gradedStudentComponent = <RTextIcon
                    ///${currentRecord['Graded Students'].value.numberOFStudents}
                    onClick={() => {
                        console.log("TOPRINT", recordId, currentRecord['Type'])
                        if (recordId > 0) {

                            gradingData.history.push(`${baseURL}/${genericPath}/course-management/course/${gradingData.courseId}/cohort/${gradingData.cohortId}/gradebook/items/${currentRecord['Type'].value}/${recordId}`)
                            return
                        }
                        return
                    }}
                    flexStyle={{ cursor: 'pointer' }}
                    text={`${currentRecord['Graded Students'].value.numberOFGradedStudents}`}
                    icon='fa-solid fa-user-group' />
                const currentHeader = currentRecord?.[header]
                console.log("currentHeader", currentHeader)
                const editable = editableFields.includes(header)
                return {
                    key: header, value: editable ?
                        currentHeader.type == "select" ?
                            <RHoverSelect
                                inputWidth='100px'
                                item={currentRecord}
                                extraInfo={{ id: recordId, type: tablesTypes.gradableItems, inputLabel: header }}
                                handleSelectItem={gradingData.handleSelectGroup}
                                handleEditFunctionality={gradingData.handleEditFunctionality}
                                options={[{ label: 'noGroup', value: null }].concat(groupsNames)}
                                value={currentHeader.value}
                                saved={currentHeader.saved} /> :
                            <div className='position-relative'>
                                <RHoverInput
                                    inputValue={currentHeader.value}
                                    handleInputDown={gradingData.handleInputSaved}
                                    inputWidth='100px'
                                    type={currentHeader.type}
                                    focusOnInput={currentHeader.notFocused ? false : true}
                                    inputPlaceHolder=""
                                    handleInputChange={gradingData.handleInputChange}
                                    inputIsNotValidate={!currentHeader.value && currentHeader.touched}
                                    item={currentRecord}
                                    extraInfo={{ id: recordId, type: tablesTypes.gradableItems, inputLabel: header }}
                                    handleOnBlur={gradingData.handleInputSaved}
                                    handleEditFunctionality={gradingData.handleEditFunctionality}
                                    saved={currentHeader.saved}
                                />
                                {!currentHeader.value && currentHeader.touched ?
                                    <i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} /> : ''}
                            </div>
                        : header == "Alternatives" ? alternativesComponent : header == "Graded Students" ? gradedStudentComponent : currentHeader.value,
                    type: editable ? 'component' : header == "Alternatives" ? 'component' : header == "Graded Students" ? 'component' : ''
                }
            }),
            actions: [
                { name: currentRecord['Type'].value == 'custom' ? "delete" : "", icon: currentRecord['Type'].value == 'custom' ? iconsFa6.delete : '', justIcon: true, actionIconClass: 'text-danger', onClick: () => { currentRecord['Type'].value == 'custom' ? gradingData.handleDelete(recordId, tablesTypes.gradableItems) : '' } },
            ]
        }
    }) : null
    return gradingData.isLoadingGradableItems ? <Loader /> :
        <RFlex className="flex-column" styleProps={{ gap: '0px' }}>
            <RLister Records={_gradableItemsRecords} withPagination={true} info={gradingData.gradableItems} handleChangePage={gradingData.handleGradableItemsPageChange} page={gradingData.gradableItemsPage} />
            <RButton
                onClick={gradingData.handleAddingItem}
                faicon={iconsFa6.plus}
                text={tr("Add New Item")}
                style={{ width: 'fit-content' }}
                className="p-0 m-0 text-primary"
                color="link" />
        </RFlex>
}

export default RGradableItems