import React from "react";
import { baseUrl } from "config/constants";

function InvalidBrowser(props) {
  return (
    <section className="invalidBrowser-section">
       <div className="container">
           <div className="row">
               <div className="col-md-6">
                   <div className="main-text">
                      <p>
                        {props.msg}
                      </p>
                       {props.link != null ? <a href={props.link} target="_blank">Safe Exam browser</a>:null}
                   </div>
               </div>
               <div className="col-md-6">
                   <img 
                   src={baseUrl + "img/InvalidBrowser/boy.png"}
                    alt="" width="600px;" className="ch-img" />
               </div>
           </div>
       </div>
   </section>
  );
}

export default InvalidBrowser;
