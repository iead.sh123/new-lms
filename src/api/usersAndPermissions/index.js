import { get, post } from "config/api";
import { Services } from "engine/services";

export const usersAndPermissionsApi = {
  getUsersTypes: (forDropDownList) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/organization/usersTypes${
        forDropDownList ? `?forDropDownList=${forDropDownList}` : ""
      }`
    ),

  getRolesBySpecificUser: (organizationUserId, searchData) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/role/userRoles/${organizationUserId}${
        searchData ? `?filter=${searchData}` : ""
      }`
    ),

  getPermissionsBySpecificUser: (organizationUserId, searchData) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/scope/userScopes/${organizationUserId}${
        searchData ? `?filter=${searchData}` : ""
      }`
    ),

  getOrganizationRoles: (searchData) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/role/organizationRoles${searchData ? `?filter=${searchData}` : ""}`
    ),

  createRoleToOrganization: (data) =>
    post(
      `${Services.auth_organization_management.backend}api/role/store`,
      data
    ),

  getAllRoleCategories: () =>
    get(
      `${Services.auth_organization_management.backend}api/role-category/all`
    ),

  getRoleById: (roleId) =>
    get(
      `${Services.auth_organization_management.backend}api/role/view/${roleId}`
    ),

  getPermissionsByRole: (roleId, searchData) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/scope/scopesGroupByCategory/${roleId}${
        searchData ? `${`?filter=${searchData}`}` : ""
      }`
    ),

  getCategoriesToOrganization: (forDropDownList) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/scope-category/organizationScopesCategory${
        forDropDownList ? `?forDropDownList=${forDropDownList}` : ""
      }`
    ),

  getPermissionsByCategory: (categoryId, searchData, forDropDownList) =>
    get(
      `${
        Services.auth_organization_management.backend
      }api/scope/scopesByCategory/${categoryId}${
        searchData
          ? `${`?filter=${searchData}`}`
          : forDropDownList
          ? `?forDropDownList=${forDropDownList}`
          : ""
      }`
    ),

  getPermissionById: (permissionId) =>
    get(
      `${Services.auth_organization_management.backend}api/scope/view/${permissionId}`
    ),

  getRecentUserTypes: () =>
    get(
      `${Services.auth_organization_management.backend}api/organization_user/recentlyUserTypes`
    ),

  getRecentAddedRoles: () =>
    get(
      `${Services.auth_organization_management.backend}api/role/recentlyAdded`
    ),

  getRecentActivities: () =>
    get(
      `${Services.auth_organization_management.backend}api/organization/recentlyActivites`
    ),

  getUsersByOrganization: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/organization_user/organizationUsers?forDropDownList=${forDropDownList}`
    ),

  getPermissionsByOrganization: (forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/scope/organizationScopes?forDropDownList=${forDropDownList}`
    ),

  getUsersByUserType: (userType, forDropDownList) =>
    get(
      `${Services.auth_organization_management.backend}api/organization_user/organizationUsers/${userType}?forDropDownList=${forDropDownList}`
    ),
};
