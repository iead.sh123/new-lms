import React, { useContext } from 'react'
import styles from './grading.module.scss'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import tr from 'components/Global/RComs/RTranslator'
import RHoverComponent from 'components/Global/RComs/RHoverComponent/RhoverComponent'
import { GradingContext } from './GGradingManagement'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import { tablesTypes } from './Constants'
import * as colors from 'config/constants'

const RItem = ({ item, groupId, itemId, studentId, isStudent }) => {
    const gradingData = useContext(GradingContext)
    console.log(item)
    return (
        <RFlex styleProps={{ gap: '100px' }}>
            <RFlex className='align-items-center' styleProps={{ width: '141px' }}>
                <span className='p-0 m-0' style={{ width: '100%', fontWeight: 'bold' }}>{item?.name}</span>
            </RFlex>
            <RFlex styleProps={{ gap: "70px" }}>
                <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px', width: '52px' }}>
                    <span style={{ width: "100%", fontWeight: 'bold' }}>{tr("Mark")}</span>
                    <p className='p-0 m-0 d-flex' style={{ width: '100%' }}><span className={` p-0 m-0`} style={{color: item?.mark < 50 ? colors.dangerColor : colors.successColor}}>{item?.mark}</span>/{item?.points}</p>
                </RFlex>
                <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px', width: '114px' }}>
                    <span style={{ width: '100%', fontWeight: 'bold' }} className='p-0 m-0'>{tr("Submitting_Date")}</span>
                    <span style={{ width: '100%' }} className='p-0 m-0'>{new Date().toDateString()}</span>
                </RFlex>
                <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px' }}>
                    <span p-0 m-0 style={{ fontWeight: 'bold' }}>{tr("Letter_Mark")}</span>
                    <span className='p-0 m-0'>A+</span>
                </RFlex>
                <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px' }}>
                    <span p-0 m-0 style={{ fontWeight: 'bold' }}>{tr("Teacher_Mark")}</span>
                    {!isStudent ?
                        <RHoverInput
                            inputValue={item?.teacherGrade?.value}
                            handleInputDown={gradingData.handleInputSaved}
                            inputWidth='70px'
                            type='number'
                            focusOnInput={true}
                            inputPlaceHolder=""
                            handleInputChange={gradingData.handleInputChange}
                            inputIsNotValidate={!item?.teacherGrade?.value && item?.teacherGrade?.touched}
                            item={item}
                            extraInfo={{ type: tablesTypes.studentSlider, itemId, groupId, inputLabel: "teacherGrade", studentId }}
                            handleOnBlur={gradingData.handleInputSaved}
                            handleEditFunctionality={gradingData.handleEditFunctionality}
                            saved={item?.teacherGrade?.saved}
                        />
                        : <span className='p-0 m-0'>{item?.teacherGrade?.value}</span>
                    }
                </RFlex>
            </RFlex>
        </RFlex>
    )
}

export default RItem