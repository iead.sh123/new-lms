import React, { useContext } from "react";
import { Input, FormText, FormGroup } from "reactstrap";
import { PaymentPolicyContext } from "../GPaymentPolicy";
import { primaryColor, lightGray } from "config/constants";
import { successColor } from "config/constants";
import { useFormik } from "formik";
import useFormattedDate from "utils/useFormattedDate";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import moment from "moment";
import styles from "../../Organization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

const GAddCoupon = ({ handleCloseCouponModal }) => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	const initialValues = {
		couponType: "",
		value: null,
		maxNumber: null,
		expireDate: "",
		openMaxNumber: false,
	};

	const formSchema = yup.object().shape({
		value: yup.number().required(tr`value is required`),
		expireDate: yup
			.string()
			.required(tr`end date is required`)
			.test({
				name: "end-date-validation",
				message: tr`expire date must be after or equal to today's date`,
				test: function (expireDate) {
					const currentDate = new Date();
					const selectedDate = new Date(expireDate);
					return selectedDate >= currentDate;
				},
			}),
	});

	const handleFormSubmit = (values, resetForm) => {
		!values.maxNumber && delete values.maxNumber;
		PaymentPolicyData.handleAddCoupon({ ...values, expireDate: useFormattedDate(values.expireDate).formattedDateForBackend() });
		resetForm();
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};

	const handleOpenMaxNumber = (check) => {
		if (check == false) {
			setFieldValue("openMaxNumber", check);
			setFieldValue("maxNumber", null);
		} else {
			setFieldValue("openMaxNumber", check);
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", paddingTop: "10px" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<div className={styles.icon}>
						<i className={iconsFa6.tag + " fa-lg"} style={{ color: "white", rotate: "90deg" }} />
					</div>
					<span>{tr`add_new_coupon`}</span>
				</RFlex>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<span style={{ color: successColor }}>{tr`this coupon can be used since you share it with others`}</span>
					<RFlex styleProps={{ minHeight: "32px", width: "70%", justifyContent: "space-between" }}>
						<AppNewCheckbox
							onChange={(e) => handleOpenMaxNumber(e.target.checked)}
							label={tr`how many people can get this?`}
							checked={values.openMaxNumber}
						/>
						{values.openMaxNumber && (
							<Input
								name="maxNumber"
								type="number"
								style={{ width: "75px" }}
								value={values.maxNumber}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "maxNumber")}
							/>
						)}
					</RFlex>

					<RFlex styleProps={{ gap: "15px" }}>
						<RFlex styleProps={{ gap: 20, width: "70%" }}>
							<span>{tr`type`}</span>
							<RFlex styleProps={{ flexDirection: "column", width: "100%", gap: 0 }}>
								<RFlex styleProps={{ minHeight: "32px", width: "100%", justifyContent: "space-between" }}>
									<AppRadioButton
										name={"type"}
										label={`% ${tr`percentage_discount`}`}
										checked={values.couponType == "Percent" ? true : false}
										onChange={(e) => {
											setFieldValue("couponType", "Percent");
											setFieldValue("value", null);
										}}
										label__class__name={styles.radio__label}
									/>
									{values.couponType == "Percent" && (
										<>
											<Input
												name="value"
												type="number"
												style={{ width: "75px" }}
												value={values.value}
												onBlur={handleBlur}
												onChange={handleChange}
												className={getInputClass(touched, errors, "value")}
											/>
											<span style={{ color: lightGray, fontSize: "16px" }}>%</span>
										</>
									)}
								</RFlex>
								<RFlex styleProps={{ minHeight: "32px", width: "100%", justifyContent: "space-between" }}>
									<AppRadioButton
										name={"type"}
										label={`$ ${tr`fixed_cart_discount`}`}
										checked={values.couponType == "Fixed" ? true : false}
										onChange={(e) => {
											setFieldValue("couponType", "Fixed");
											setFieldValue("value", null);
										}}
										label__class__name={styles.radio__label}
									/>
									{values.couponType == "Fixed" && (
										<>
											<Input
												name="value"
												type="number"
												style={{ width: "75px" }}
												value={values.value}
												onBlur={handleBlur}
												onChange={handleChange}
												className={getInputClass(touched, errors, "value")}
											/>
											<span style={{ color: lightGray, fontSize: "16px" }}>$</span>
										</>
									)}
								</RFlex>
							</RFlex>
						</RFlex>
					</RFlex>
					<RFlex styleProps={{ width: "70%" }}>
						<FormGroup style={{ width: "100%" }}>
							<label>{tr("coupon_expiry_date")}</label>
							<Input
								name="expireDate"
								type="date"
								value={useFormattedDate(values.expireDate).formattedDateForDisplay()}
								placeholder={tr("start_date")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "expireDate")}
							/>
							{touched.expireDate && errors.expireDate && <FormText color="danger">{errors.expireDate}</FormText>}
						</FormGroup>
					</RFlex>
				</RFlex>

				{/* actions */}
				<RFlex>
					<RButton
						type="submit"
						text={tr`create`}
						color="primary"
						loading={PaymentPolicyData.addCouponLoading}
						disabled={PaymentPolicyData.addCouponLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						style={{ color: primaryColor }}
						onClick={() => {
							resetForm();
							handleCloseCouponModal();
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default GAddCoupon;
