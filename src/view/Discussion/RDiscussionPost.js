import React, { useContext } from "react";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useHistory, useParams } from "react-router-dom";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { FormGroup, Input, Collapse } from "reactstrap";
import RDiscussionCommentDesign from "./RDiscussionCommentDesign";
import RDiscussionPostDesign from "./RDiscussionPostDesign";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import styles from "./RDiscussion.module.scss";
import { primaryColor } from "config/constants";

const RDiscussionPost = ({
  post,
  mB,
  ml = "ml-0",
  community,
  typeContent,
  specificMandatoryPost,
  indRec = 0,
  postType,
}) => {
  const history = useHistory();
  const { width } = useWindowDimensions();
  const mobile = width < 700;
  const { cohortId, categoryName, sectionId } = useParams();
  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  return (
    <div key={post?.id} className={`${mB} mr-4 ml-0`}>
      {/* ------------------------ Title before Reported posts ------------------------*/}
      {typeContent == "post" &&
        categoryName == "Reported" &&
        post?.reporting_users &&
        post?.reporting_users?.map((report) => (
          <RFlex key={report.user_id} className="mb-2">
            <h6>
              {`${report.full_name} ${tr`has reported this content due`} ${
                report.reason
              }`}
            </h6>
          </RFlex>
        ))}

      {/* ------------------------ Title before Rejected posts ------------------------*/}
      {typeContent == "post&" &&
        categoryName == "Rejected" &&
        post?.rejected_reason && (
          <RFlex className="mb-2">
            <h6>
              {` ${tr`Your content has been rejected due`} ${
                post?.rejected_reason
              }`}
            </h6>
          </RFlex>
        )}

      <RDiscussionPostDesign
        post={post}
        mB={mB}
        ml={"ml-0"}
        typeContent={typeContent}
        community={community}
        specificMandatoryPost={specificMandatoryPost}
        indRec={0}
        seeMoreComment={false}
        postType={postType}
      />
    </div>
  );
};

export default RDiscussionPost;
