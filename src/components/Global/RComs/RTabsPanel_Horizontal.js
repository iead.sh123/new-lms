import React, { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Row,
  Col,
} from "reactstrap";
import "./RTabs.css";

const RTabsPanel_Horizontal = ({
  title,
  Tabs,
  SelectedIndex=0,
  changeHorizontalTabs,
  Url,
}) => {
  const { tabTitle } = useParams();
  const [horizontalTabs, setHorizontalTabs] = useState(
    tabTitle ? tabTitle : Tabs[SelectedIndex].title
  );
  const changeTab = (title, index) => {
    setHorizontalTabs(title);
    changeHorizontalTabs(index);
  };
  const history = useHistory();
  return (
    <div className="content">
      <Row>
        <Col md="12">
          <Card>
            <CardHeader>
              <h5 className="text-center">{title}</h5>
            </CardHeader>
            <CardBody>
              <div className="nav-tabs-navigation">
                <div className="nav-tabs-wrapper">
                  <Nav id="tabs" role="tablist" tabs>
                    {Tabs.map((tab, index) => {
                      return (
                        <NavItem key={index} className="navItemClass" style={{borderBottom: "white 0px solid!important"}}>
                          <NavLink
                            aria-expanded={horizontalTabs === tab.title}
                            data-toggle="tab"
                            onMouseUp={() => {
                              history.replace(Url ? `${Url}${tab.title}` : "#");
                            }}
                            role="tab"
                            className={
                              horizontalTabs === tab.title
                                ? "active font-weight-bold horizontal-tab"
                                : "horizontal-tab"
                            }
                            onClick={() => changeTab(tab.title, index)}
                            disabled={tab.disabled}
                            style={{display:"flex",justifyContent:"center",alignItems:"center",gap: "4px"}}
                          >
                            {tab.icon&&<i className={tab.icon}/>}
                            {tab.icon&&" "}
                            {tab.title+" "}
                            {(tab.count&&(tab.count>0))? <div  style={{display: "flex",
                                        height: "20px",
                                        fontWeight: 500,
                                        color: "#ffffff",
                                        background: "#668ad7",
                                        borderRadius: "50px",
                                        fontSize: "12px",
                                        padding: "1px",  paddingLeft: "5px",
                                        paddingRight: "5px",
   

                                        }}>
                                 {tab.count}</div>:<></>}
                          </NavLink>
                        </NavItem>
                      );
                    })}
                  </Nav>
                </div>
              </div>
              <TabContent
                className="text-center"
                id="my-tab-content"
                activeTab={horizontalTabs}
              >
                {Tabs.map((tab, index) => {
                  return (
                    <TabPane key={index} tabId={tab.title} role="tabpanel">
                      {tab.content()}
                    </TabPane>
                  );
                })}
              </TabContent>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default RTabsPanel_Horizontal;
