import React from "react";
import Select from "react-select";

const RSelect = ({
	name,
	onChange,
	onBlur,
	value,
	defaultValue,
	option,
	isMulti,
	placeholder,
	closeMenuOnSelect,
	isDisabled,
	className,
	isRtl,
	isLoading,
	customOption,
	touched,
	errors,
	style,
}) => {
	return (
		<Select
			style={{ ...style }}
			className={`react-select ${className ? className : "primary"} `}
			classNamePrefix="react-select"
			name={name}
			value={value}
			defaultValue={defaultValue}
			onChange={onChange}
			onBlur={onBlur}
			isMulti={isMulti}
			options={option}
			placeholder={placeholder}
			closeMenuOnSelect={closeMenuOnSelect}
			isDisabled={isDisabled ? isDisabled : false}
			isRtl={isRtl}
			isLoading={isLoading}
			components={customOption && { Option: customOption }}
			touched={touched}
			errors={errors}
		/>
	);
};
export default RSelect;
