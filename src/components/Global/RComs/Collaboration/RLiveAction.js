import React from "react";
import "./RCollaborationCard.css";

import { Services } from "engine/services";

import RLoader from "../RLoader";
import { RLabel } from "../RLabel";
import RToggle from "components/RComponents/RToggle";

const RLiveAction = (props) => {
	const imgsrc = (path) => {
		const b = Services.storage.file;
		if (path.includes(b)) {
			return path;
		} else return b + path;
	};

	const {
		style,
		loading,
		labelStyle,
		onClick,
		disabled,
		disabledNote,
		color,
		withBorder,
		backgroundColor,
		border,
		title,
		value,
		text,
		image,
		iconStyle,
		icon,
		type,
	} = props;

	return (
		<>
			<div
				className="live_action_Container"
				onClick={type == "checkbox" || disabled ? null : onClick}
				style={{
					...style,
					backgroundColor: backgroundColor,
					paddingLeft: style?.paddingLeft ?? "7px",
					cursor: "pointer",
					color: disabled ? "red" : color,
					border: border ?? withBorder ? `1px solid ${color}` : "none",
				}}
			>
				{loading && <RLoader mini></RLoader>}
				{!loading && type == "checkbox" && (
					<RToggle
						checked={value}
						label={title}
						value={value}
						onColor="success"
						offColor="secondary"
						onChange={disabled ? null : onClick}
					/>
				)}

				{!loading && icon && <i className={icon} style={{ iconStyle }} />}
				{!loading && image && (
					<div className="card_Author_img">
						<img src={imgsrc(image)}></img>{" "}
					</div>
				)}
				{/* {text&&<>&nbsp; <span>{" "+text}</span></>} */}
				{text &&
					(disabled ? (
						<RLabel
							style={
								labelStyle ?? {
									fontWeight: "bold",
									marginTop: "8px",
									marginBottom: "0px",
									textTransform: "capitalize",
								}
							}
							lettersToShow={text.length}
							tooltiped={disabled}
							tooltipText={disabledNote}
							value={text + " "}
						></RLabel>
					) : (
						<>
							<span>{text}</span>
						</>
					))}
			</div>
		</>
	);
};
export default RLiveAction;
