import React, { useContext, useState, useEffect } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { termsManagementApi } from "api/global/termsManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RUsersContent from "view/TermsManagement/RightSide/RUsersContent";
import Loader from "utils/Loader";
import REmptyData from "components/RComponents/REmptyData";

const GHomeRoomUsers = () => {
	const TermsManagementData = useContext(TermsManagementContext);
	const [sortBy, setSortBy] = useState("alphabiticallyDesc");

	const studentData = useFetchDataRQ({
		queryKey: ["student-users", TermsManagementData?.itemObj?.dataObj?.id, sortBy],
		queryFn: () =>
			termsManagementApi.allUsersEnrolledToHomeRoom({
				homeRoomId: TermsManagementData?.itemObj?.dataObj?.id,
				userType: "student",
				orderBy: sortBy,
			}),
	});

	const teacherData = useFetchDataRQ({
		queryKey: ["teacher-users", TermsManagementData?.itemObj?.dataObj?.id, sortBy],
		queryFn: () =>
			termsManagementApi.allUsersEnrolledToHomeRoom({
				homeRoomId: TermsManagementData?.itemObj?.dataObj?.id,
				userType: "teacher",
				orderBy: sortBy,
			}),
	});

	useEffect(() => {
		if (studentData.isSuccess || studentData.isFetching) {
			TermsManagementData.setFieldValueUsers("studentsById", studentData?.data?.data?.data?.users);
		}

		if (teacherData.isSuccess || teacherData.isFetching) {
			TermsManagementData.setFieldValueUsers("teachersById", teacherData?.data?.data?.data?.users);
		}
	}, [teacherData.isSuccess, studentData.isSuccess, studentData.isFetching, teacherData.isFetching]);

	const handleSelectedAllUsersInHomeRoom = () => {
		const allTeachers = teacherData?.data?.data?.data?.users?.map((user) => ({
			user_id: user?.user_id,
		}));
		const allStudents = studentData?.data?.data?.data?.users?.map((user) => ({
			user_id: user?.user_id,
		}));

		TermsManagementData.setFieldValueUsers("teachers", allTeachers);
		TermsManagementData.setFieldValueUsers("students", allStudents);
	};

	const handleSort = (sortType) => {
		setSortBy(sortType);
	};

	if (studentData?.isLoading || teacherData?.isLoading) {
		return <Loader />;
	} else if (studentData?.isError || teacherData?.isError) {
		return <REmptyData />;
	}

	return (
		<RUsersContent
			students={studentData?.data?.data?.data?.users}
			teachers={teacherData?.data?.data?.data?.users}
			handleSort={handleSort}
			handleSelectedAllUsers={handleSelectedAllUsersInHomeRoom}
			homeRoomUsers={true}
		/>
	);
};

export default GHomeRoomUsers;
