import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React from 'react'
import RLoader from 'components/Global/RComs/RLoader'

const UserPosition = ({ position }) => {
    //console.log("sdjflsjfsdjfsk", position)
    return (
        position ? (
            <RFlex className="flex-column">
                <RFlex className="align-items-center">
                    <i className='fa-solid fa-briefcase' />
                    <span className='p-0 m-0'>Position</span>
                </RFlex>
                <p className='p-0 m-0' style={{ wordBreak: 'break-all' }}>{position}</p>
            </RFlex>
        ):
        <RLoader mini />
  )
}

export default UserPosition