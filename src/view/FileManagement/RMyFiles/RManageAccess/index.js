import React from "react";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Switch } from "ShadCnComponents/ui/switch";
import { Label } from "ShadCnComponents/ui/label";

const RManageAccess = ({ data, handlers, loading }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column", padding: "5px" }}>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
				<span>
					{tr`manage_access_for`} &nbsp;
					{data?.itemObj?.folder_name ?? data?.itemObj?.file_name_by_user}
				</span>
				<RTextIcon
					flexStyle={{ cursor: "pointer", gap: "5px" }}
					text={tr`copy_link`}
					textStyle={{ color: "#668ad7" }}
					iconStyle={{ color: "#668ad7" }}
					onClick={() => handlers.handlers.handleCopyLink()}
					icon={iconsFa6.copy}
				/>
			</RFlex>
			<RFlex className="justify-between align-items-center">
				<RProfileName name={data?.user?.name} img={data?.user?.image} />
				<span className="text-themeBoldGrey">{tr`owner`}</span>
			</RFlex>
			<span className="font-bold">{tr`shared_with`}</span>

			{data.values?.itemAccess?.map((item) => {
				return (
					<RFlex key={item.user_id} className="justify-between align-items-center ">
						<RProfileName name={item?.user_name} img={item?.image} />
						<div className="w-60">
							<RSelect
								option={data.values.permissionList}
								isMulti={true}
								value={item.permissions}
								onChange={(e) => handlers.handleSelectPermissionsToUser({ event: e, userId: item.user_id })}
								closeMenuOnSelect={false}
							/>
						</div>
					</RFlex>
				);
			})}
			<RFlex>
				<div className="flex items-center space-x-2">
					<Label htmlFor="allow-downloads">{tr`allow_downloads`}</Label>
					<Switch
						checked={data.values.itemDownloadStatus}
						onCheckedChange={(event) => {
							handlers.handleSwitchChange(event);
						}}
						className="data-[state=checked]:bg-themeSuccess"
						id={"allow-downloads"}
						disabled={loading?.downloadStatusLoading}
					/>
					{loading?.downloadStatusLoading && <i className={iconsFa6.spinner} />}
				</div>
			</RFlex>
			<RFlex className="justify-end">
				<RButton
					text={tr`save`}
					onClick={() => handlers.handleAddAccessToItem()}
					color="primary"
					loading={loading.addAccessToItemLoading}
					disabled={loading.addAccessToItemLoading}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RManageAccess;
