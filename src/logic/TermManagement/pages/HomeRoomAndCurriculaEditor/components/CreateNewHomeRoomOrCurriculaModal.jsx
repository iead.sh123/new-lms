import { faQuestionCircle, faUserGraduate, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RModal from "components/Global/RComs/RModal";
import RSelect from "components/Global/RComs/RSelect";
import RTags from "components/Global/RComs/RTags";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import { homeRoomOrCurriculaMode } from "logic/TermManagement/constants/homeroms-curricula-editor-modes.constants";
import moment from "moment";
import React from "react";
import { FormGroup, Input, Label, FormText, Row, Col } from "reactstrap"
import Swal, { SUCCESS, DANGER } from "utils/Alert";



const CreateNewHomeRoomOrCurriculaModal = ({ mode, isOpen, toggle, handleSave, handleLoadGradeLevelStudents,
    handleLoadOrganizationTeachers, teachers, students }) => {

    const [formItems, setFormItems] = React.useState({
        name: '',
        code: '',
        learners: [],
        teachers: []
    });


    const renderSelectItem = ({ innerProps, label, data }) => {
        return (
            <div {...innerProps} role="button">
                <img
                    style={{
                        borderRadius: '100%',
                        padding: '10px'
                    }}
                    className="ml-4"
                    src={Services.auth_organization_management.file + data.image}
                    // alt={data.label}
                    width="55px"
                    height="55px"
                />
                {label}
            </div>
        );
    };



    const [loading, setLoading] = React.useState(false);


    const initializeUsers = async () => {
        setLoading(true);
        await handleLoadGradeLevelStudents([]);
        await handleLoadOrganizationTeachers();
        setLoading(false);
    }


    React.useEffect(() => {

        //if users are not already loaded load them
       (!teachers?.ids?.length && !students?.ids?.length) && initializeUsers();
    }, []);


    const handleChange = (e) => setFormItems({ ...formItems, [e.target.name]: e.target.name === "rank" ? +e.target.value : e.target.value });

    const handleCreate = async () => {
        await handleSave({ ...formItems })
        toggle();

    };

    return <RModal isOpen={isOpen}
        withCloseIcon={false}
        toggle={toggle}
        header={<RFlex> <p>{mode === homeRoomOrCurriculaMode.HOMEROOM ? tr`Create New Homeroom`: tr`Create New Course`}</p></RFlex>}
        footer={
            <RFlex styleProps={{ width: '100%', alignItems: 'start' }}>
                <RButton
                    text={tr`create`}
                    color="primary"
                    onClick={handleCreate}
                // style={{alignSelf: 'end'}}
                />

                <RButton
                    text={tr`cancel`}
                    style={{ background: 'white', color: '#668AD7' }}
                    // faicon="fa fa-plus"
                    // color="link"
                    onClick={toggle}
                // style={{alignSelf: 'end'}}
                />
            </RFlex>
        }
        body={<div>
            <Row>
                <Col md="12">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Name")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="name"
                                type="text"
                                // placeholder={tr`Name`}
                                value={formItems.name}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
            </Row>
            {mode === homeRoomOrCurriculaMode && <Row>
                <Col md="12">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Code")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="code"
                                // type="date"
                                // placeholder={tr`Start Date`}
                                value={formItems.code}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
            </Row>}

            <Row>
                <Col md="6">
                    <p className="mt-4">Teachers &nbsp;<i className="fa fa-question-circle" /></p>
                    <RSelect name={tr`Teachers`}
                        onChange={(values) => setFormItems({ ...formItems, teachers: values.map(v => ({ user_id: v.value })) })}
                        option={Object.values(teachers?.byId).map(gl => ({ label: gl.name, value: gl.id, image: gl.hash_id }))}
                        isMulti
                        closeMenuOnSelect={false}
                        placeholder={tr`Teachers`}
                        customOption={renderSelectItem}
                    />
                </Col>

                <Col md="6">
                    <p className="mt-4">Students &nbsp;<i className="fa fa-question-circle" /></p>
                    <RSelect name={tr`Students`}
                        onChange={(values) => setFormItems({ ...formItems, learners: values.map(v => ({ user_id: v.value })) })}
                        option={Object.values(students?.byId).map(gl => ({ label: gl.name, value: gl.id, image: gl.hash_id }))}
                        isMulti
                        closeMenuOnSelect={false}
                        placeholder={tr`Students`}
                        customOption={renderSelectItem}
                    />
                </Col>

            </Row>
        </div >} />



}

export default CreateNewHomeRoomOrCurriculaModal;