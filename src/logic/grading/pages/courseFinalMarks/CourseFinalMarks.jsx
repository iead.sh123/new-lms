import React, { useState, useEffect } from "react";
import { Row, Col, Input } from "reactstrap";
import { useHistory } from "react-router-dom";
import { get } from "config/api";
import { useSelector, useDispatch } from "react-redux";

import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import Helper from "components/Global/RComs/Helper";
import RBackend from "./RBackend";
import { Services } from "engine/services";
import RBill from "components/Global/RComs/RBill";
import styles from "./CourseFinalMarks1.Module.scss";
import { setEditedHomeRoom } from "logic/TermManagement/state/terms-manager.actions";

const CourseFinalMarks = ({  }) => {

const {courseId}=useParams();
const [teacherMark,setTeacherMark]=useState();
const [editedId,seteditedId]=useState(null);
const [refresh,setRefresh]=useState(1);
const [tableRefresh,setTableRefresh]=useState(1);
const dispatch = useDispatch();
const history = useHistory();

const [calculated,setCalculated] = useState(false);

const [processsedRecords, setProcessedRecords] = useState([]);
// -----------------------------------------------------------------------
//   GET
// /student-gradable-items/my-details/type/{type}/{itemId}


// GET
// /student-gradable-items/student-details/type/{type}/{itemId}/student/{studentId}


// GET
// /student-gradable-items/students-details-list/type/{type}/{itemId}


// GET
// /student-gradable-items/group-details/groups/{groupId}


// GET
// /student-gradable-items/student-my-group-details/groups/{groupId}


// GET
// /student-gradable-items/student-group-details/groups/{groupId}/students/{studentId}
//-------------------------------------------------------------------------
  const { user } = useSelector((state) => state?.auth);

  const getDataFromBackend = async (specific_url) => {
    const url = Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}`;
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };


  const userCard=(i,n)=><div><img src={Services.storage.file+i} width={"30px"}/>{n}</div>
  const save=(relation_id)=>
  {
    Helper.fastPatch(   
         Services.grading.backend+`student-gradable-items/student-course-details/update-course-teacher-grades`,
      {
        "teacherGrades": [
          {
            "mark": +teacherMark,
            "id": +relation_id
          }
        ]
      },
       "saved successfully",
       "error while calculating",
       () => {setTableRefresh(tableRefresh+1);seteditedId(null)},
       () => { alert("fail")}
       
     );
   
  }

  const input=(id)=>
  {
   return <div>
   <Input 
   value={teacherMark} 
   onChange={(e)=>{
    setTeacherMark(e.target.value);
    setRecords();
    setRefresh(refresh+3)
  }
  }/></div>
  }

  const [resp,setRepsonse]=useState([]);

  useEffect(()=>{
    setRecords()
  },[resp,refresh])



  const setRecords=()=>{
 // setProcessedRecords(response?.data?.data);
    // return;
    let calculated1=false;
    Helper.cl(resp,"setDAta response");
    let processsedRecords1 = [];

    resp?.data?.data?.map((r) => {
      let record = {};
      record.allTitle = "allTitle";//r?.creation_date;
      record.title = "title";//r?.description;
      record.table_name = "table_name";//"abuse";
      if(r?.calculatedMark)   calculated1=true;
      record.details = [
        // "full_name": "l16 l16",
        // "image": null,
        // "student_id": 862,
        // "id": 862,
        // "teacherMark": "0.00",
        // "calculatedMark": "0.00",
        // "relation_id": 278,
        // "latterGrade": "A+",
        // "GPA": "4.0"
        { key: tr`student`, value: userCard(r.image,r.full_name),type : "component" },
        { key: tr`Final Mark`, value: r?.calculatedMark },
        { key: tr`teacher Mark`, value: 
      editedId==r.id?input(r.id):r?.teacherMark
      ,type : "component" 
      },
        { key: tr`Latter Grade`, value: r?.latterGrade },
        { key: tr`GPA`, value: r?.GPA },
      
      ];

      record.actions =(editedId==r.id)?[
        {
          name: tr`save`,
          icon: "fa fa-save",
          color: "info",

          onClick: () => {
            save(r.relation_id);
            setRecords()
            setRefresh(refresh+4);
          }
        },


      ]: [
        {
          name: tr`edit`,
          icon: "fa fa-pen",
          color: "info",

          onClick: () => {
            setTeacherMark(r?.teacherMark)
            seteditedId(r.id);
            setRecords()
            setRefresh(refresh+2)
          }
        },
        {
          name: tr`mark details`,
          icon: "fa fa-eye",
          color: "info",

          onClick: () => {
            alert("mark details") 
          }
        },
        {
          name: tr`delete`,
          icon: "fa fa-print",
          color: "info",

          onClick: () => {
           alert("delete")
          },
        },
      ];

      processsedRecords1.push(record);
    });
    if(calculated1)setCalculated(true);
    setProcessedRecords(processsedRecords1);

  }

  const setData = (response) => {
    setRepsonse(response);
   // setRecords();
   // setRefresh(refresh+4);
  };

const calculate=()=>{
  

  Helper.cl("t7 f1");
  const rl= Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}/re-calculate`;
  Helper.cl("t7 f2 rl");
	Helper.fastPost(
   rl,
    {},
    "calculated successfully",
    "error while calculating",
    () => {setCalculated(true);   Helper.cl("agreement delte suceess")},
    () => {                 }
    
  );

  Helper.cl("t7 f3");
}

  const flag = true;
  // return <>{Helper.js(processsedRecords)}</>;
  // return <RBackend url={Services.grading.backend+`student-gradable-items/student-course-details/courses/${courseId}`}/>

  return (
    <div
      className={
        !user?.useNewTheme && user?.type == "parent" ? "container" : "content"
      }
      style={{
        paddingTop: !user?.useNewTheme && user?.type == "parent" && "18vh",
      }}
    >
      {
      Helper.js(styles,"styles")}
      <div 
      style={{display:"flex",justifyContent:"flex-start",alignItems:"center"}} 
      className={styles.markstitle}
      >
      
      Student Final Marks {processsedRecords?.length? <RBill count={processsedRecords?.length}/>:<></>}
      
      <RButton text={<span> <i class="fa fa-calculator"></i> {calculated?"Calculated":"Calculate"} </span>}
      style={{backgroundColor:calculated?"#47c37e":"#668AD7",color:"white"}}
      onClick={(e)=>{ calculate()}}/>

    </div>

    {teacherMark}
      {Helper.cl(processsedRecords,"processsedRecords")}
      <RAdvancedLister
        getDataFromBackend={getDataFromBackend}
        setData={setData}
        refresh={tableRefresh}
        records={processsedRecords}
        getDataObject={(response) => {
          Helper.cl(response.data.data,"advancedlister response.data")
          return response.data.data}}
        characterCount={15}
        marginT={"mt-3"}
        marginB={"mb-2"}
        showListerMode="tableLister"
      ></RAdvancedLister>
    </div>
  );
};

export default CourseFinalMarks;
