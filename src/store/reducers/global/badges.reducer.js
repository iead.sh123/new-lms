import {
  GET_BADGES_REQUEST,
  GET_BADGES_SUCCESS,
  GET_BADGES_ERROR,
  REMOVE_BADGES_REQUEST,
  REMOVE_BADGES_SUCCESS,
  REMOVE_BADGES_ERROR,
  STORE_BADGES_REQUEST,
  STORE_BADGES_SUCCESS,
  STORE_BADGES_ERROR,
} from "store/actions/global/globalTypes";
import produce from "immer";

const InitState = {
  badges: [],
  badgesLoading: false,
  removeBadgesLoading: false,
  storeBadgeLoading: false,
};

let ind;
const badgesReducer = (state = InitState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_BADGES_REQUEST:
        draft.badgesLoading = true;
        return draft;
      case GET_BADGES_SUCCESS:
        draft.badgesLoading = false;
        draft.badges = action.payload.data;
        return draft;
      case GET_BADGES_ERROR:
        draft.badgesLoading = false;
        return draft;

      case REMOVE_BADGES_REQUEST:
        draft.removeBadgesLoading = true;
        return draft;
      case REMOVE_BADGES_SUCCESS:
        draft.removeBadgesLoading = false;
        ind = draft.badges.findIndex((el) => el.id == action.payload.badgeId);
        if (ind !== -1) {
          draft.badges.splice(ind, 1);
        }
        return draft;
      case REMOVE_BADGES_ERROR:
        draft.removeBadgesLoading = false;
        return draft;

      case STORE_BADGES_REQUEST:
        draft.storeBadgeLoading = true;
        return draft;
      case STORE_BADGES_SUCCESS:
        draft.storeBadgeLoading = false;
        if (draft.badges?.length > 0) {
          ind = draft.badges.findIndex((el) => el.id == action.payload.data.id);
          if (ind !== -1) {
            draft.badges[ind].id = action.payload.id;
            draft.badges[ind].course_id = action.payload.course_id;
            draft.badges[ind].hash_id = action.payload.hash_id;
            draft.badges[ind].description = action.payload.description;
          } else {
            draft.badges.push(action.payload.data);
          }
        } else {
          draft.badges.push(action.payload.data);
        }

        return draft;
      case STORE_BADGES_ERROR:
        draft.storeBadgeLoading = false;
        return draft;

      default:
        return draft;
    }
  });

export default badgesReducer;
