import React, { useContext } from 'react'
import styles from './grading.module.scss'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import tr from 'components/Global/RComs/RTranslator'
import RHoverComponent from 'components/Global/RComs/RHoverComponent/RhoverComponent'
import { GradingContext } from './GGradingManagement'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import iconsFa6 from 'variables/iconsFa6'
import RButton from 'components/Global/RComs/RButton'
const RFinalStudentDetails = () => {
    const gradingData = useContext(GradingContext)
    const courseInfo = gradingData?.gradingState?.studentSlider?.courseInfo
    console.log("courseInfo", courseInfo)
    return (
        <RFlex styleProps={{ gap: '84px' }}>
            <RFlex styleProps={{ gap: '110px' }}>
                <RFlex className='align-items-center' styleProps={{ width: '141px' }}>
                    <span className='p-0 m-0' style={{ width: '100%', fontWeight: 'bold' }}>{tr('Final_Result')}</span>
                </RFlex>
                <RFlex styleProps={{ gap: "105px" }}>
                    <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px', width: '52px' }}>
                        <span style={{ width: "100%", fontWeight: 'bold' }}>{tr("mark")}</span>
                        <span className='p-0 m-0'>{courseInfo?.calculatedGrade}</span>
                    </RFlex>
                    <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px', width: '114px' }}>
                        <span style={{ width: '100%', fontWeight: 'bold' }} className='p-0 m-0'>{tr("Submitting_Date")}</span>
                        <span style={{ width: '100%' }} className='p-0 m-0'>{new Date().toDateString()}</span>
                    </RFlex>
                    <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px' }}>
                        <span p-0 m-0 style={{ fontWeight: 'bold' }}>{tr("Letter_Mark")}</span>
                        <span className='p-0 m-0'>A+</span>
                    </RFlex>
                    <RFlex className='flex-column justify-content-between' styleProps={{ gap: '17px' }}>
                        <span p-0 m-0 style={{ fontWeight: 'bold' }}>{tr("Pass/Fail")}</span>
                        <span>Fail</span>
                    </RFlex>
                </RFlex>
            </RFlex>
            <RButton
                text={gradingData.calculated ? tr('Recalculate') : tr('Recalculate')}
                onClick={gradingData.calculateFinalMarks}
                faicon={iconsFa6.calculator}
                color={"primary"}
                style={{ width: 'fit-content', padding: '6px 12px' }}
                loading={gradingData.isCalculating}
            />
        </RFlex>
    )
}

export default RFinalStudentDetails