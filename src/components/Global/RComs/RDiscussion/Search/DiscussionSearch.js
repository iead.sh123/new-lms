import React, { useState } from "react";
import { useSelector } from "react-redux";
import {
  Form,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Input,
  Col,
} from "reactstrap";
import styles from "./DiscussionSearch.Module.scss";
import tr from "components/Global/RComs/RTranslator";

function DiscussionSearch() {
  const [searchForm, setSearchForm] = useState({
    keyword: null,
  });
  const { user } = useSelector((state) => state.auth);
  const changeValue = (e) => {
    setSearchForm({ ...searchForm, [e.target.id]: e.target.value });
  };

  const submitSearch = (e) => {
    e.preventDefault();
    if (searchForm.keyword != null && searchForm.keyword !== "") {
      user?.type == "teacher"
        ? window.location.replace(
            `${process.env.REACT_APP_BASE_URL}/teacher/search/${searchForm.keyword}`
          )
        : user?.type == "student"
        ? window.location.replace(
            `${process.env.REACT_APP_BASE_URL}/student/search/${searchForm.keyword}`
          )
        : user?.type == "parent"
        ? window.location.replace(
            `${process.env.REACT_APP_BASE_URL}/parent/search/${searchForm.keyword}`
          )
        : user?.type == "employee"
        ? window.location.replace(
            `${process.env.REACT_APP_BASE_URL}/admin/search/${searchForm.keyword}`
          )
        : window.location.replace(
            `${process.env.REACT_APP_BASE_URL}/senior-teacher/search/${searchForm.keyword}`
          );
    }
  };

  return (
    <>
      <Col md={7}>
        <Form onSubmit={submitSearch}>
          <InputGroup className="no-border">
            <Input
              id="keyword"
              onChange={changeValue}
              value={searchForm.keyword ? searchForm.keyword : ""}
              placeholder={tr`search` + "..."}
              type="text"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText>
                <i className={"nc-icon nc-zoom-split " + styles.optical_icon} />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </Form>
      </Col>
    </>
  );
}

export default DiscussionSearch;
