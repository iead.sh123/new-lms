import avatar from "assets/img/under-construction.png";
const RUnderConstruction = () => {
  return (
    <div className="float-center mt-5 pt-4 text-center">
      <img width="500" height="500" src={avatar} />
    </div>
  );
};

export default RUnderConstruction;
