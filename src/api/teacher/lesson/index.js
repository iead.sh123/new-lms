import { get, post } from "config/api";

export const lessonPageApi = {
  getBooks: (rabId) => get(`api/rabs/${rabId}/books`),

  getLessonsContent: (rabContentId) => get(`api/lesson/${rabContentId}`),

  insertBooksToLesson: (lessonId, lessonPageContent) =>
    post(`/api/lesson/${lessonId}`, lessonPageContent),

  updateLessonBook: (lessonId, temp) =>
    post(`/api/lesson/${lessonId}/book`, temp),

  synchronizeRab: (id) => get(`/api/sync-rabs/Synchronize/${id}`),
};
