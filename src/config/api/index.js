import axios from "axios";
import Helper from "components/Global/RComs/Helper";
import { newAuth, newAuthToken } from "engine/config";
import store from "store";
import { persistor } from "store/index.js";

const apiClient = axios.create({
	baseURL: "", //process.env.REACT_APP_BACKEND_URL ,
	headers: {
		Accept: "application/json",
		"Content-Type": "application/json",
		// lang: "en", //localStorage.getItem("lang"); || "en",
	},
	timeout: process.env.REACT_APP_REQUESTS_TIMEOUT,
});

function resolved(result) {}

function rejected(result) {
	console.error(result);
}

apiClient.interceptors.request.use(
	(config) => {
		if (newAuth) {
			config.headers["Authorization"] = `Bearer ${newAuthToken}`;
		} else if (store.getState().auth.token && config.url !== "api/auth/login") {
			config.headers["Authorization"] = `Bearer ${store.getState().auth.token}`;

			config.headers["current-organization"] = store.getState().auth.user?.original?.current_organization;
			config.headers["current-type"] = store.getState().auth.user?.original?.current_type;
			config.headers["lang"] = localStorage.getItem("lang");
			if (store.getState().auth.user?.user_type == "parent") {
				config.headers["student-id"] = `${store.getState().auth?.selectedStudentId}`;
			}
		}
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

apiClient.interceptors.response.use(
	(response) => response,
	(error) => {
		if (error) {
			if (error?.response) {
				Helper.cl(error?.response, "error?.response");

				Helper.cl(error, "error");
				// if (error.response.status === 401 &&  config.url.includes("api/auth/me")) {
				//   persistor.purge();
				//   history.push(process.env.REACT_APP_BASE_URL + "/auth/login");
				//   location.reload();
				// }
			}

			return Promise.reject(new Error("Netowrk fail")).then(resolved, rejected);
		}
		return Promise.reject(new Error("Netowrk fail")).then(resolved, rejected);
	}
);

const { get, post, put, delete: destroy, patch } = apiClient;
export { get, post, put, destroy, patch };

export default apiClient;
