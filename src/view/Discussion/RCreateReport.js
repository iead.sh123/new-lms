import React, { useContext } from "react";
import { Row, Col, Form, Label, FormGroup, Input, FormText } from "reactstrap";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faPaperclip } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
import RFileUploader from "components/Global/RComs/RFileUploader";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RSelect from "components/Global/RComs/RSelect";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import style from "./RDiscussion.module.scss";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import styles from "./RDiscussion.module.scss";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";

const RCreateReport = () => {
  const DiscussionData = useContext(DiscussionContext);
  return (
    <AppModal
      show={DiscussionData.reportModal}
      parentHandleClose={DiscussionData.handleCloseAddReport}
      header={tr`report`}
      size={"lg"}
      headerSort={
        <Form>
          {DiscussionData.allReportCategoriesLoading ? (
            <Loader />
          ) : (
            <Row className="mb-3">
              <Col md={10} xs={12} className="mb-2">
                <p
                  style={{ fontWeight: 600 }}
                >{tr`why would you like to report this content ?`}</p>
              </Col>

              {DiscussionData.allReportCategories.map((reportCategory) => (
                <Col md={10} xs={12} className="mb-2" key={reportCategory.id}>
                  <AppRadioButton
                    label={reportCategory.text}
                    name="report_category_id"
                    // value={reportCategory.id}
                    onChange={(event) => {
                      DiscussionData.handleChangeAddReport(
                        event.target.name,
                        reportCategory.id
                      );
                      DiscussionData.handleChangeAddReport("other", false);
                      DiscussionData.handleChangeAddReport(
                        "other_report_reason",
                        ""
                      );
                    }}
                  />
                </Col>
              ))}

              <Col md={10} xs={12} className="mb-2">
                <FormGroup>
                  <AppRadioButton
                    label={tr`other`}
                    name="report_category_id"
                    // value={reportCategory.id}
                    onChange={(event) => {
                      DiscussionData.handleChangeAddReport("other", true);
                      DiscussionData.handleChangeAddReport(
                        "report_category_id",
                        null
                      );
                    }}
                  />
                  <Input
                    name="other_report_reason"
                    required
                    type="textarea"
                    value={DiscussionData.reportingOnPost.other_report_reason}
                    disabled={
                      DiscussionData.reportingOnPost.other == true
                        ? false
                        : true
                    }
                    onChange={(event) =>
                      DiscussionData.handleChangeAddReport(
                        event.target.name,
                        event.target.value
                      )
                    }
                  />
                </FormGroup>
              </Col>
              <RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
                <RButton
                  text={tr`submit`}
                  color="primary"
                  onClick={DiscussionData.handleSaveReportingOnPost}
                  loading={DiscussionData.reportingOnPostLoading}
                  disabled={
                    DiscussionData.reportingOnPostLoading ||
                    !(
                      DiscussionData.reportingOnPost.other_report_reason ||
                      DiscussionData.reportingOnPost.report_category_id
                    )
                      ? true
                      : false
                  }
                />
              </RFlex>
            </Row>
          )}
        </Form>
      }
    />
  );
};

export default RCreateReport;
