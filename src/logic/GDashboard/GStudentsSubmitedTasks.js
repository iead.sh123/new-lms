import Helper from "components/Global/RComs/Helper";
import RLister from "components/Global/RComs/RLister";
import RTableLister from "components/Global/RComs/RTableLister";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";



const GStudentsSubmitedTasks = ({tasks}) => {

 
//   "outDatedTasks": [
//     {
//         "id": 3,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:47:58.722Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:47:58.722Z",
//         "isClosed": false,
//         "exam": {
//             "id": 7,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:47:55.698Z",
//             "updated_at": "2023-10-04T07:47:55.698Z"
//         },
//         "type": "exams"
//     },
//     {
//         "id": 2,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:45:22.296Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:45:22.296Z",
//         "isClosed": false,
//         "exam": {
//             "id": 5,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:45:19.937Z",
//             "updated_at": "2023-10-04T07:45:19.937Z"
//         },
//         "type": "exams"
//     },
//     {
//         "id": 1,
//         "randomizeQuestionOrder": false,
//         "randomizeQuestionPartsOrder": false,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-04T07:22:03.001Z",
//         "isDefault": true,
//         "updated_at": "2023-10-04T07:22:03.001Z",
//         "isClosed": false,
//         "exam": {
//             "id": 3,
//             "name": "QTEST",
//             "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
//             "questionsCount": 1,
//             "points": 2,
//             "files": null,
//             "is_published": false,
//             "created_at": "2023-10-04T07:22:00.719Z",
//             "updated_at": "2023-10-04T07:22:00.719Z"
//         },
//         "type": "exams"
//     }
// ],
// "activeTasks": [
//     {
//         "id": 2,
//         "maximumLearnersAllowed": 200,
//         "passRequired": true,
//         "autoGrade": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 1,
//         "dueDate": null,
//         "cutoffDate": null,
//         "isDefault": true,
//         "created_at": "2023-10-03T08:11:05.333Z",
//         "updated_at": "2023-10-03T08:11:05.333Z",
//         "isClosed": false,
//         "assignment": {
//             "id": 5,
//             "name": "string",
//             "description": "string",
//             "points": 10,
//             "files": null,
//             "questionsCount": 1,
//             "is_published": true,
//             "created_at": "2023-10-03T08:11:01.032Z",
//             "updated_at": "2023-10-03T08:11:01.032Z"
//         },
//         "type": "assignments"
//     },
//     {
//         "id": 1,
//         "maximumLearnersAllowed": 200,
//         "passRequired": true,
//         "autoGrade": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 1,
//         "dueDate": null,
//         "cutoffDate": null,
//         "isDefault": true,
//         "created_at": "2023-10-03T08:10:54.742Z",
//         "updated_at": "2023-10-03T08:10:54.742Z",
//         "isClosed": false,
//         "assignment": {
//             "id": 3,
//             "name": "string",
//             "description": "string",
//             "points": 10,
//             "files": null,
//             "questionsCount": 1,
//             "is_published": true,
//             "created_at": "2023-10-03T08:10:53.803Z",
//             "updated_at": "2023-10-03T08:10:53.803Z"
//         },
//         "type": "assignments"
//     },
//     {
//         "id": 3,
//         "canContinue": false,
//         "maxAttemptsAllowed": 1,
//         "passRequired": true,
//         "autoGrade": true,
//         "isDefault": true,
//         "percentagePassingGrade": 0,
//         "randomizeQuestionOrders": 0,
//         "startDate": null,
//         "endDate": null,
//         "created_at": "2023-10-03T08:16:18.010Z",
//         "updated_at": "2023-10-03T08:16:18.010Z",
//         "isClosed": false,
//         "quiz": {
//             "id": 14,
//             "name": "string",
//             "description": "string",
//             "files": null,
//             "is_published": true,
//             "questionsCount": 5,
//             "points": 50,
//             "created_at": "2023-10-03T08:16:16.243Z",
//             "updated_at": "2023-10-03T08:16:16.243Z"
//         },
//         "type": "quizzes"
//     }
// ],
// "allTasksCount": 11
  



  // const [timeLineItems,setTimeLineItems] = useState([
  //   { icon: 'fa fa-pen', title: 'First Event', description: 'This is the first event', image: null },
  //   { icon: 'fa fa-camera', title: 'Second Event', description: <div>This is the second event <i className="fas fa-clock clock-icon"></i> <span >10:00 - 11:00</span>
  //   <div> lesson number 10 <span style={{color:"orange",cursor: "pointer" }} onClick={()=>{setLesson({id:40}),setLessonModalOpen(true);}}> &#9888; Not published Yet</span></div>
  //   </div>, image: null },
  //   { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
  // ]);


  const history = useHistory();
  const viewQuestionSet=( 
     learner_id,
   
    courseId,
    module_content_id,
    moduleId,
    questionSetType,
   )=>{
   
     history.push(
       `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/10/module/${module_content_id}/learner/${learner_id}/${questionSetType}/${module_content_id}/correction`

     );
  
    //history.push(`http://localhost:3000/g/course-management/course/1563/cohort/567/module/61/learner/1106/exams/61/correction`)
  }

const _records = React.useMemo(
  () =>
  tasks?.map((rc) => ({
      title: rc?.label,
      details: [
        { key: "Student", value: rc?.learner_full_name },
        { key: "Task", value: rc?.questionset_name },
        { key: "submitted_at", value: rc?.submitted_at },
        { key: "view", type:"component", value: <div style={{color:"#668AD7"}} onClick={() => viewQuestionSet(
          
          rc?.learner_id,
          rc?.questionset_courseId,
          rc?.module_content_id,
          rc?.moduleId,
    
          rc?.questionSetType,
        
          
          )} ><i className="fa fa-eye"/></div> }
    ],
      actions: null,
    })),
  [tasks]
);
  return <div>

    {/* {Helper.js(_records,"records")}   */}
    {/* {tasks?Helper.jstree(tasks):null} */}

    {/* {Helper.js(tasks[0])} */}
   <RTableLister Records={_records} color="#668AD7" />

</div>

};

export default GStudentsSubmitedTasks;
