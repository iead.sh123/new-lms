import React, { useContext } from "react";
import { AttendanceContext } from "./GAttendanceDetailsLister";
import Loader from "utils/Loader";
import RLister from "components/Global/RComs/RLister";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RHoverComponent from "components/Global/RComs/RHoverComponent/RhoverComponent";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import styles from "./attendance.module.scss";
import { baseURL, genericPath } from "engine/config";
import { attendanceTypes } from "./constants";
import tr from "components/Global/RComs/RTranslator";
const RTodayDetails = () => {
	const attendanceContext = useContext(AttendanceContext);
	console.log("RTodayDetails is rerendered");
	if (attendanceContext.isLoadingTodayDetails) {
		return <Loader />;
	}
	console.log("RTodayDetails", attendanceContext.attendanceStates);
	const todayDetailsState = attendanceContext?.attendanceStates?.todayDetails;
	const _todayAttendance = attendanceContext?.todayDetails?.students?.map((student, studentIndex) => ({
		id: student.id,
		table_name: "GAttendanceDetailsLister",
		details: [
			{
				key: tr("Students"),
				value: (
					<RProfileName
						name={student.name}
						img={student.image}
						onClick={() =>
							attendanceContext.history.push(
								`${baseURL}/${genericPath}/course-management/course/${attendanceContext.courseId}/cohort/${attendanceContext.cohortId}/attendance/student/${student.id}`
							)
						}
						flexStyleProps={{ cursor: "pointer" }}
					/>
				),
				type: "component",
			},
			...attendanceContext?.todayDetails?.attendance?.map((item) => ({
				key: <RHoverComponent text={item?.title} hoverValue={item?.acronym} />,
				keyType: "component",
				value: (
					<div className={styles.RadioDiv}>
						<AppRadioButton
							checked={
								todayDetailsState?.[student.id]
									? todayDetailsState?.[student.id]?.attendanceType === item.id
										? true
										: false
									: student.attendance_course_id === item.id
									? true
									: false
							}
							onChange={() => {
								console.log("OnClickOnClick");
								attendanceContext.handleAttendanceTypeChange(student.id, item.id);
							}}
							className={styles.AttendanceRadioButton}
						/>
					</div>
				),
				type: "component",
			})),
		],
	}));
	return <RLister Records={_todayAttendance} center={true} activeScroll={true} />;
};

export default React.memo(RTodayDetails);
