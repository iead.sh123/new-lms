import React, { useEffect, useState } from "react";
import {
  getQuestionsToSolve,
  getQuestionsToReviewAsync,
  getLearnerSolutionToSolveAsync,
  saveLearnerSolutionAsync,
  fillDataToSolve,
  confirmTheSolveToTheQuestion,
  resetSolveQuestion,
  solvedPercentage,
  addFeedbackToQuestion,
  calcQuestionsMark,
  addTeacherFeedbackToQs,
  correctionOfQuestionsAsync,
  fillDataToSolveInOrderingQuestion,
  removeOrderingQuestion,
  getQuestionsToReviewTeacherAsync,
} from "store/actions/global/questionSet.actions";
import { useDispatch, useSelector } from "react-redux";
import Loader from "utils/Loader";
import RSolveQuestionSet from "view/Courses/CourseManagement/QuestionSet/SolveQuestionSet/RSolveQuestionSet";
import RStartSolveQuestionSet from "view/Courses/CourseManagement/QuestionSet/SolveQuestionSet/RStartSolveQuestionSet";
import { baseURL, genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";

export const QuestionSetSolveContext = React.createContext();

const GSolveQuestionSet = () => {
  const history = useHistory();
  const [openSendAnswer, setOpenSendAnswer] = useState(null);
  const [openFeedback, setOpenFeedback] = useState(false);
  const [openQuestionGroupCollapse, setOpenQuestionGroupCollapse] =
    useState(null);

  const [step, setStep] = useState(0);
  const [showTime, setShowTime] = useState(true);
  const [timeSpentInSecond, setTimeSpentInSecond] = useState({
    startDate: 0,
    endDate: 0,
  });
  const dispatch = useDispatch();
  const {
    courseId,
    cohortId,
    categoryId,
    moduleId,
    questionSetId,
    learnerId,
    questionSetType,
    submitted,
    startPage,
  } = useParams();

  const {
    questionsToSolve,
    questionAnswers,
    percentageOfSolvingQuestions,
    questionsCorrection,
    finalMarkToStudent,
    correctQuestionsCount,
    questionsToSolveLoading,
    saveLearnerSolutionLoading,
    correctionOfQuestions,
  } = useSelector((state) => state.questionSetRed);

  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    if (submitted == "review" && !learnerId) {
      //to review marks by (student)
      // questionSetId here is questionSetContextId(questionSetContextId: between module content and Qs)
      dispatch(getQuestionsToReviewAsync(questionSetType, moduleId));
    } else if (submitted == "review" && learnerId) {
      //to review marks by (teacher)
      dispatch(
        getQuestionsToReviewTeacherAsync(questionSetType, moduleId, learnerId)
      );
    } else if (submitted == "solving") {
      //to solve question by student
      dispatch(getQuestionsToSolve(questionSetType, moduleId));
    } else if (submitted == "correction") {
      //to correct question by teacher
      // questionSetId here is questionSetContextId(questionSetContextId: between module content and Qs)
      dispatch(
        getLearnerSolutionToSolveAsync(
          questionSetType,
          questionSetId,
          learnerId
        )
      );
    }
  }, []);
  const correctOrReviewMode = ["correction", "review"].includes(submitted);
  // const correctOrReviewMode = submitted == "correction";
  const teacherInCorrectionAndReviewMode =
    submitted == "correction" || (submitted == "review" && learnerId);

  const doNotContainBehaviour =
    questionsToSolve.cutoffDate == null ||
    questionsToSolve.closeDate == null ||
    questionsToSolve.endDate == null;

  const collapsesQuestionsGroupsToggle = (questionGroupId) => {
    const isOpen = "collapse" + questionGroupId;
    if (isOpen == openQuestionGroupCollapse) {
      setOpenQuestionGroupCollapse("collapse");
    } else {
      setOpenQuestionGroupCollapse("collapse" + questionGroupId);
    }
  };

  const _tree = (ind) => {
    setStep(ind);
  };
  const _next = () => {
    setStep(step + 1);
  };

  const _prev = () => {
    setStep(step - 1);
  };

  const handlePushToSolveQuestionSet = () => {
    if (categoryId) {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleId}/${questionSetType}/${questionSetId}/${submitted}`
      );
    } else {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleId}/${questionSetType}/${questionSetId}/${submitted}`
      );
    }
    setTimeSpentInSecond({
      ...timeSpentInSecond,
      startDate: new Date().getTime(),
    });
  };

  const handleRedirectToModuleAfterSubmitted = () => {
    if (categoryId) {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/modules?moduleId=${moduleId}`
      );
    } else {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules?moduleId=${moduleId}`
      );
    }
  };

  const handleBackToSubmittedStudents = () => {
    if (categoryId) {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleId}/student/${questionSetType}/${
          learnerId ? "gradedStudents" : "submittedStudents"
        }`
      );
    } else {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleId}/student/${questionSetType}/${
          learnerId ? "gradedStudents" : "submittedStudents"
        }`
      );
    }
  };

  const handleSaveLearnerSolution = () => {
    dispatch(
      saveLearnerSolutionAsync(
        questionSetType,
        moduleId,
        {
          timeSpentInSeconds: parseInt(
            (+timeSpentInSecond.endDate - +timeSpentInSecond.startDate) / 1000,
            10
          ),
          questionAnswers: questionAnswers.questionAnswers,
        },
        handleCloseSubmitAnswerModal,
        handleRedirectToModuleAfterSubmitted
      )
    );
  };

  const handleFillDataToSolveQuestion = ({
    questionGroupId,
    groupContextId,
    questionContextId,
    type,
    key,
    value,
    isCorrect,
  }) => {
    dispatch(
      fillDataToSolve(
        questionGroupId,
        groupContextId,
        questionContextId,
        type,
        key,
        value,
        isCorrect
      )
    );
  };

  const handleConfirmTheSolveToTheQuestion = (questionGroupId, questionId) => {
    dispatch(confirmTheSolveToTheQuestion(questionGroupId, questionId));
    dispatch(solvedPercentage());
  };

  const handleResetSolveQuestion = ({
    questionGroupId,
    questionId,
    requiredQuestion,
    moduleContextQuestionId,
    submissionType,
  }) => {
    dispatch(
      resetSolveQuestion(
        questionGroupId,
        questionId,
        requiredQuestion,
        moduleContextQuestionId,
        submissionType
      )
    );
    dispatch(solvedPercentage());
  };
  const handleOpenSubmitAnswerModal = () => {
    setOpenSendAnswer(true);
    setTimeSpentInSecond({
      ...timeSpentInSecond,
      endDate: new Date().getTime(),
    });
  };
  const handleCloseSubmitAnswerModal = () => {
    setOpenSendAnswer(false);
  };

  const handleOpenAndCloseFeedback = () => {
    setOpenFeedback(!openFeedback);
  };

  const handleAddFeedbackToQuestion = ({
    learnerContextQuestionId,
    key,
    value,
  }) => {
    dispatch(addFeedbackToQuestion(learnerContextQuestionId, key, value));
  };

  const handleAddTeacherFeedbackToQs = ({ key, value }) => {
    dispatch(addTeacherFeedbackToQs(key, value));
  };

  const handleCorrectionOfQuestions = () => {
    dispatch(
      correctionOfQuestionsAsync(
        questionSetType,
        questionsToSolve.id,
        questionsCorrection,
        handleBackToSubmittedStudents
      )
    );
  };

  const handleCalcQuestionsMark = () => {
    dispatch(calcQuestionsMark());
  };

  const handleFillDataToSolveInOrderingQuestion = ({
    questionGroupId,
    questionId,
    groupContextId,
    questionContextId,
    type,
    isCorrect,
    droppableOrder,
    draggableOrder,
  }) => {
    dispatch(
      fillDataToSolveInOrderingQuestion(
        questionGroupId,
        questionId,
        groupContextId,
        questionContextId,
        type,
        isCorrect,
        droppableOrder,
        draggableOrder
      )
    );
  };

  const handleRemoveOrderingQuestion = ({
    questionGroupId,
    questionId,
    groupContextId,
    questionContextId,
    type,
    isCorrect,
    draggableOrder,
  }) => {
    dispatch(
      removeOrderingQuestion(
        questionGroupId,
        questionId,
        groupContextId,
        questionContextId,
        type,
        isCorrect,
        draggableOrder
      )
    );
  };
  const [time, setTime] = useState({ hours: 0, minutes: 0, seconds: 0 });
  useEffect(() => {
    if (questionsToSolve && ["quizzes", "exams"].includes(questionSetType)) {
      const startDate = moment(questionsToSolve.startDate);
      const endDate = moment(questionsToSolve.endDate);
      const duration = moment.duration(endDate.diff(startDate));
      const hours = duration.hours();
      const minutes = duration.minutes();
      const seconds = duration.seconds();

      setTime({ hours: hours, minutes: minutes, seconds: seconds });
    }
  }, [questionsToSolve]);

  const timeDiffInMilliseconds =
    new Date(questionsToSolve?.endDate) - new Date();

  const timeDiffInMinutes = timeDiffInMilliseconds / (1000 * 60);

  function formatMinutesToHoursAndMinutes(minutes) {
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;
    let formattedTime = "";

    if (hours > 0) {
      formattedTime += `${hours} hour${hours > 1 ? "s" : ""}`;
    }

    if (remainingMinutes > 0) {
      if (formattedTime) {
        formattedTime += " ";
      }
      formattedTime += `${Math.floor(remainingMinutes)} minute${
        remainingMinutes > 1 ? "s" : ""
      }`;
    }

    return formattedTime;
  }
  const formattedTime = formatMinutesToHoursAndMinutes(timeDiffInMinutes);

  const handleShowTime = () => {
    setShowTime(!showTime);
  };
  return (
    <div>
      {questionsToSolveLoading ? (
        <Loader />
      ) : (
        <QuestionSetSolveContext.Provider
          value={{
            questionsToSolve,
            questionAnswers,
            questionSetType,
            percentageOfSolvingQuestions,
            user,
            openFeedback,
            step,
            submitted,
            correctOrReviewMode,
            finalMarkToStudent,
            questionsCorrection,
            openQuestionGroupCollapse,
            learnerId,
            teacherInCorrectionAndReviewMode,
            formattedTime,
            handleShowTime,
            showTime,
            setTime,
            time,
            doNotContainBehaviour,

            handlePushToSolveQuestionSet,
            handleFillDataToSolveQuestion,
            handleConfirmTheSolveToTheQuestion,
            handleResetSolveQuestion,
            handleOpenSubmitAnswerModal,
            handleOpenAndCloseFeedback,
            handleAddFeedbackToQuestion,
            handleCalcQuestionsMark,
            handleAddTeacherFeedbackToQs,
            handleCorrectionOfQuestions,
            handleFillDataToSolveInOrderingQuestion,
            handleRemoveOrderingQuestion,
            collapsesQuestionsGroupsToggle,
            _tree,
            _next,
            _prev,
            setStep,

            questionsToSolveLoading,
            correctionOfQuestions,
            history,
          }}
        >
          <AppModal
            show={openSendAnswer}
            parentHandleClose={handleCloseSubmitAnswerModal}
            headerSort={
              <RFlex
                styleProps={{ flexDirection: "column", gap: 30 }}
                className="text-center"
              >
                <h6 style={{ paddingTop: "15px" }}>
                  {tr`You have answered`} {correctQuestionsCount} {tr`from`}{" "}
                  {questionsToSolve?.questionGroups?.length} {tr`questions`}
                </h6>
                <h6 className="text-muted">{tr`Are you sure to submit ?`}</h6>
                <RFlex styleProps={{ justifyContent: "center" }}>
                  <RButton
                    text={tr`review_my_answer`}
                    outline
                    color="primary"
                    onClick={handleCloseSubmitAnswerModal}
                  />
                  <RButton
                    text={tr`submit`}
                    outline
                    color="warning"
                    onClick={handleSaveLearnerSolution}
                    loading={saveLearnerSolutionLoading}
                    disabled={saveLearnerSolutionLoading}
                  />
                </RFlex>
              </RFlex>
            }
          />
          {startPage ? <RStartSolveQuestionSet /> : <RSolveQuestionSet />}
        </QuestionSetSolveContext.Provider>
      )}
    </div>
  );
};

export default GSolveQuestionSet;
