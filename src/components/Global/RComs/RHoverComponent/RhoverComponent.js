import React, { useState } from 'react'
import styles from './RHoverComponent.module.scss'
const RHoverComponent = ({ text, hoverValue }) => {
    const [isHovered, setIsHovered] = useState(false)

    return (
        <div
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            style={{ position: 'relative', display: 'inline-block' }}>

            {text}
            {isHovered &&
                <div className={styles.hoverAttendance}>{hoverValue}</div>
            }
        </div>
    )
}

export default RHoverComponent