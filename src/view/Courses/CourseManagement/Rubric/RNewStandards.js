import React, { useContext } from "react";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { greyColor } from "config/constants";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { FieldArray } from "formik";
import RAllStandards from "./RAllStandards";

const RNewStandards = ({ categoryIndex }) => {
	const RubricData = useContext(RubricContext);

	return (
		<FieldArray name={`categories[${categoryIndex}].standards`}>
			{(arrayHelpers) => (
				<UncontrolledDropdown className="btn-magnify">
					<DropdownToggle style={{ paddingLeft: "0px" }} aria-haspopup={true} data-toggle="dropdown" id="navbarDropdownMenuLink" nav>
						<RFlex styleProps={{ cursor: "pointer", width: "fit-content", height: "20px", color: greyColor }}>
							<i className="fa fa-plus pt-1" />
							<p>{tr`new_standards`}</p>
						</RFlex>
					</DropdownToggle>

					<DropdownMenu className={styles.dropdown__left} persist aria-labelledby="navbarDropdownMenuLink">
						<DropdownItem
							key={"new_standards"}
							onClick={() => {
								arrayHelpers.push({
									fakeId: Math.random(),
									subject: "",
									points: null,
									description: "",
									ratings: [{ fakeId: Math.random(), title: "", score: 0 }],
								});
								RubricData.handleOpenStandardModal({
									categoryIndex: categoryIndex,
									standardIndex: RubricData.values.categories[categoryIndex]?.standards?.length,
									arrayHelpers: arrayHelpers,
								});
							}}
							style={{ left: "25px" }}
						>
							<RFlex styleProps={{ cursor: "pointer", width: "fit-content", height: "20px" }}>
								<i className="fa fa-plus pt-1" />
								<span>{tr`new_standards`}</span>
							</RFlex>
						</DropdownItem>
						<DropdownItem key={"duplicate_standard"}>
							<RFlex styleProps={{ cursor: "pointer", width: "fit-content", height: "20px" }}>
								<i className="fa-solid fa-copy pt-1" />
								<span>{tr`duplicate_standard`} :</span>
							</RFlex>
							<RAllStandards arrayHelpers={arrayHelpers} />
						</DropdownItem>
					</DropdownMenu>
				</UncontrolledDropdown>
			)}
		</FieldArray>
	);
};

export default RNewStandards;
