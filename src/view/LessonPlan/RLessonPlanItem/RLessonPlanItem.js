import React from "react";
import RLessonPlanItemForms from "./RLessonPlanItemForms";

const RLessonPlanItem = ({
  lessonPlan,
  lessonPlanErrors,
  handelLessonPlanItemChange,
  handleAddNewLessonPlanItems,
  handleRemoveLessonPlanItem,
  handleClearLessonPlanItem,
  handleOpenPickARubric,
  setLessonPlanItemId,
  handleRemoveRubricFromLessonPlanItem,
  dir,
}) => {
  return (
    <RLessonPlanItemForms
      lessonPlan={lessonPlan}
      lessonPlanErrors={lessonPlanErrors}
      handelLessonPlanItemChange={handelLessonPlanItemChange}
      handleAddNewLessonPlanItems={handleAddNewLessonPlanItems}
      handleRemoveLessonPlanItem={handleRemoveLessonPlanItem}
      handleClearLessonPlanItem={handleClearLessonPlanItem}
      handleOpenPickARubric={handleOpenPickARubric}
      setLessonPlanItemId={setLessonPlanItemId}
      handleRemoveRubricFromLessonPlanItem={
        handleRemoveRubricFromLessonPlanItem
      }
      dir={dir}
    />
  );
};

export default RLessonPlanItem;
