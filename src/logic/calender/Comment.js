import React from 'react';
import styles from './Comment.module.scss';
import moment from 'moment';

const Comment = ({ text, date, creatorName, note, scrollToView }) => {

    const commentRef = React.useRef(null);

    React.useLayoutEffect(() => {
        if (scrollToView) {
            commentRef?.current?.scrollIntoViewIfNeeded(false, {
                block: "center",
                behavior: "smooth",
                inline: "nearest",
            });
        } 
    }, [scrollToView]);

    return <div className={styles.Comment} ref={commentRef}>
        <div className={styles.Header}>

            {!note && <span style={{ fontWeight: 'bold', fontSize: 15 }}> {creatorName} </span>}
            <span>{moment(new Date(date).toString()).format(
                "MM-DD-YYYY hh:mm:ss a"
            )}</span>
        </div>
        <p style={{ fontSize: '20px' }}>
            {text}
        </p>
    </div>
}

export default Comment;