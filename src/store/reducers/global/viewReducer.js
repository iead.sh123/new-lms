import { MINIMIZE_SIDEBAR } from "store/actions/global/globalTypes";
import { SET_SECONDARY_SIDEBAR } from "store/actions/global/globalTypes";
import { SET_THIRD_SIDEBAR } from "store/actions/global/globalTypes";
import { SET_THIRD_SIDEBAR_SHOWN } from "store/actions/global/globalTypes";
import { SET_SECONDARY_SIDEBAR_SHOWN } from "store/actions/global/globalTypes";
import Helper from "components/Global/RComs/Helper";
import { SET_THIRD_SIDEBAR_DISABLED } from "store/actions/global/globalTypes";
import { SET_THIRD_SIDEBAR_ACTIVE_NAME } from "store/actions/global/globalTypes";
import { SET_SECOND_SIDEBAR_ACTIVE_NAME } from "store/actions/global/globalTypes";

//school_id , type
const initState = {
  miniSidebar: false,
  second_sidebar: { shown: false, items: [] },
  third_sidebar: { shown: false, items: [] },
};

const viewReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case MINIMIZE_SIDEBAR:
      const s = { ...state, miniSidebar: payload.newState };
      return s;
    case SET_SECONDARY_SIDEBAR:
      return {
        ...state,
        miniSidebar: true,
        second_sidebar: {
          ...state.second_sidebar,
          items: payload.sidebar,
          key: payload.key,
          value: payload.value,
        },
      };
    case SET_THIRD_SIDEBAR:
      return {
        ...state,
        miniSidebar: true,
        third_sidebar: {
          ...state.third_sidebar,
          items: payload.sidebar,
          key: payload.key,
          value: payload.value,
        },
      };
    case SET_SECONDARY_SIDEBAR_SHOWN:
      return {
        ...state,
        second_sidebar: { ...state.second_sidebar, shown: payload.show },
      };
    case SET_THIRD_SIDEBAR_SHOWN:
      return {
        ...state,
        third_sidebar: { ...state.third_sidebar, shown: payload.show },
      };

    case SET_THIRD_SIDEBAR_DISABLED:
      return {
        ...state,
        third_sidebar: { ...state.third_sidebar, disabled: payload.disabled },
      };

    case SET_SECOND_SIDEBAR_ACTIVE_NAME:
      return {
        ...state,
        second_sidebar: { ...state.second_sidebar, active: payload.active },
      };

    case SET_THIRD_SIDEBAR_ACTIVE_NAME:
      return {
        ...state,
        third_sidebar: { ...state.third_sidebar, active: payload.active },
      };

      "";

    default:
      return state;
  }
};

export default viewReducer;
