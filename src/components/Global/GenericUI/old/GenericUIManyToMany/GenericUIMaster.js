import React, {useState , useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {translate} from "../../../../utils/translate";

import {
    setSelectedMaster, search, getTableData,
    getPivotTableData
} from '../../../../store/actions/genericUIHasManyActions';
import { setTitle, setDocumentationLink } from '../../../../store/actions/navActions';

const GenericUIMaster = ({masterTable}) => {
    const master = useSelector(state => state.genericUIHasManyReducer.master);
    const masterTableTitle = useSelector(state => state.genericUIHasManyReducer.masterTableTitle);
    const detailTableTitle = useSelector(state => state.genericUIHasManyReducer.detailTableTitle);
    const selectedMaster = useSelector(state => state.genericUIHasManyReducer.selectedMaster);

    const [keyword, setKeyword] = useState(null);

    useEffect(() => {
            let title = masterTableTitle?.slice(0, -1) + ' ' + detailTableTitle;
            dispatch(setTitle(title));
    }, [masterTableTitle , detailTableTitle])

    const dispatch = useDispatch();
    const changeValue = (e) => {
        setKeyword(e.target.value);
    }

    const handleSearch = () => {
        dispatch(getTableData(masterTable, 'master', keyword));
        master.map((line) => {
            document.getElementById('tr' + line.id).className = null;
        });
    }

    const handleSelectClicked = (line) => {
        dispatch(setSelectedMaster(line));
        master.map((line) => {
            document.getElementById('tr' + line.id).className = null;
        });
        document.getElementById('tr' + line.id).className = 'bg-acteare';
    }


    return (
        <div className="card mb-4">
            <div className='row text-center'>
            <h5 style={{
                margin:'5px auto'
            }}>{masterTableTitle}</h5>
                <input
                    id="keywordMaster"
                    value={keyword ? keyword : ''}
                    className="form-control col-6"
                    type="text"
                    placeholder={translate('Search')}
                    aria-label="Search"
                    onChange={changeValue}
                />
                <button className="btn Sebtn" name="btnSearch" onClick={handleSearch}>
                <i className="fas fa-search "></i>{' '}
                </button>
            </div>
            <div className="card-header header-title">
                
            </div>
            <table id='main-table'
                   className='table  main-table'>
                <tbody>
                {master?.map((line, index) => {
                    return (
                        <tr key={line.id}
                            id={'tr' + line.id}>
                            <td scope='col'>
                                <h5>{line.name}</h5>
                            </td>
                            <td scope='col'>
                                <button id={line.id} className='btn btn-success' onClick={(e) => {
                                    handleSelectClicked(line)
                                }}>Select
                                </button>
                            </td>
                        </tr>
                    )
                })
                }
                </tbody>
            </table>
        </div>
    );
}

export default GenericUIMaster;