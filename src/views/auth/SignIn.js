import React from "react";
import RPasswordFields from "components/Global/RComs/RInputs/RPasswordFields";
import initiateToken from "./FireBaseServices";
import RButton from "components/Global/RComs/RButton";
import styles from "./auth.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import { signIn, refresh } from "store/actions/global/auth.actions";
import { primaryColor } from "config/constants";
import { useSelector, useDispatch } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { useHistory } from "react-router-dom";
import { CART_ITEMS } from "config/constants";
import { useFormik } from "formik";
import store from "store";
import { Button } from "ShadCnComponents/ui/button";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import { removeApisNotWorkingFromLocale } from "engine/config";

const SignIn = () => {
	const dispatch = useDispatch();
	const history = useHistory();

	const { loading } = useSelector((state) => state?.auth);

	const handleFormSubmit = async (values) => {
		dispatch(signIn(values, login_callback));
		typeof window !== "undefined" && localStorage.removeItem(CART_ITEMS);
	};

	const initialValues = {
		username: "",
		password: "",
		remember_me: false,
	};

	const formSchema = yup.object().shape({
		username: yup
			.string()
			.email(tr`invalid email`)
			.required(tr`email address is required`),
		password: yup
			.string()
			.matches(/^(?=.{1,})/, tr`must contain 1 character`)
			.required(tr`password is required`),
	});

	const { values, errors, touched, handleBlur, handleChange, handleSubmit } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const handlePushToSignUpPage = () => {
		history.push(baseURL + `/auth/register`);
	};

	React.useEffect(() => {
		async function fetch() {
			const access_token = store.getState().auth.token;
			const refresh_token = store.getState().auth.refresh_token;
			if (access_token) {
				await dispatch(refresh({ refresh_token: refresh_token }, login_callback));
			}
		}

		fetch();
	}, []);

	const login_callback = (data) => {
		if (data?.user && !removeApisNotWorkingFromLocale) {
			initiateToken(data.user.original?.id);
		}
		if (["admin"]?.includes(data?.user?.original?.type?.toLowerCase())) {
			history.push(`${baseURL}/${genericPath}/users-and-permissions/recent-activities`);
		} else {
			history.push(`${baseURL}/${genericPath}/my-courses`);
		}
	};

	return (
		<section className={styles.section__sign__in__layout}>
			<h6 className={styles.auth__text}>
				{tr`login_to`} &nbsp;
				<span className={styles.auth__text_organization}>Guidance</span>
			</h6>
			<Form className="w-100" onSubmit={handleSubmit}>
				<Row className={styles.auth__row}>
					<Col xs={12} className={styles.auth__input + " p-0 m-0"}>
						<label>{tr("email_address")}:</label>
						<FormGroup>
							<Input
								name="username"
								type="email"
								value={values.username}
								placeholder={tr("email_address")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={(!!touched.username && !!errors.username) || (touched.username && errors.username) ? "input__error" : ""}
							/>
							{touched.username && errors.username && <FormText color="danger">{errors.username}</FormText>}
						</FormGroup>
					</Col>
					<Col xs={12} className="p-0 m-0">
						<label>{tr("password")}:</label>
						<FormGroup>
							<RPasswordFields
								onChange={handleChange}
								onBlur={handleBlur}
								touched={touched.password}
								errors={errors.password}
								value={values.password}
								name={`password`}
							/>
						</FormGroup>
					</Col>

					<Col xs={6} className="p-0 m-0">
						<RFlex>
							<FormGroup>
								<Input
									id="remember_me" // Use id instead of name
									type="checkbox"
									value={values.remember_me}
									onBlur={handleBlur}
									onChange={handleChange}
									className="ml-0 "
								/>
							</FormGroup>
							<span className="ml-2 text-muted">{tr`remember_me`}</span>
						</RFlex>
					</Col>
					<Col xs={6} className="d-flex justify-content-end m-0 p-0">
						<a style={{ color: primaryColor }}>{tr`forgot_password`}</a>
					</Col>
				</Row>
				<RFlex styleProps={{ justifyContent: "space-around" }}></RFlex>
				<Row>
					<Col xs={12} className="p-0 m-0">
						<RButton className="w-100 " type="submit" text={tr`log_in`} color="primary" loading={loading} disabled={loading} />
					</Col>
				</Row>
				<Row>
					<Col xs={12} className="text-center">
						<span className="text-muted">
							{tr`don't_have_an_account`} ? &nbsp;
							<span style={{ color: primaryColor }} onClick={handlePushToSignUpPage} className="cursor-pointer">{tr`register`}</span>
						</span>
					</Col>
				</Row>
			</Form>
		</section>
	);
};

export default SignIn;
