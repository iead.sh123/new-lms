import {
  GET_STUDENTS_REQUEST,
  GET_STUDENTS_SUCCESS,
  GET_STUDENTS_ERROR,
} from "store/actions/teacher/teacherTypes";

const initState = {
  list: [],
  loading: false,
  error: null,
};

const studentsReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case GET_STUDENTS_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_STUDENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: [...payload],
      };

    case GET_STUDENTS_ERROR:
      return {
        ...initState,
        error: payload,
      };

    default:
      return state;
  }
};

export default studentsReducer;
