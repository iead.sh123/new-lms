import tr from 'components/Global/RComs/RTranslator';
import React from 'react';
import {UncontrolledTooltip } from 'reactstrap';

const Event = ({ event, timeText, isStart, isEnd, isMirror, isPast, isFuture, isToday, view, backgroundColor }) => {
    return <div className='d-flex justify-content-between cursor-pointer' style={{backgroundColor}}>
        <span onClick={event.extendedProps.showDetails}>
            {event.title}

        </span>
        <div>
            {+event.extendedProps.commentsCount > 0 ? <i id={"event_comments_"+event.extendedProps._id} onClick={event.extendedProps.showComments} className="fa fa-commenting mr-2" style={{ color: 'red' }} /> : null}
            {/* <UncontrolledTooltip delay={0} target={"event_comments_"+event.extendedProps._id}>
                {tr`comments`}
            </UncontrolledTooltip> */}
            {+event.extendedProps.notesCount > 0 ? <i id={"event_notes_"+event.extendedProps._id} onClick={event.extendedProps.showNotes} className="fa fa-sticky-note" style={{ color: 'green' }} /> : <></>}
            {/* <UncontrolledTooltip delay={0} target={"event_notes_"+event.extendedProps._id}>
                {tr`notes`}
            </UncontrolledTooltip> */}
        </div>
    </div>
}


export default Event;



// event - Event Object
// timeText
// isStart
// isEnd
// isMirror
// isPast
// isFuture
// isToday
// el - the element. only available in eventDidMount and eventWillUnmount
// view