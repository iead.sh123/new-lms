import ForwardMessage from "./ForwardMessage/ForwardMessage"
import GroupManagement from "./GroupManagement/GroupManagement"
import { ReportContent } from "./ReportContent"
import { ReportMessage } from "./ReportMessage"
import NewMessage from "./newMessage/NewMessage"
import SearchMultipleChats from "./searchMultipleChats/SearchMultipleChats"
import Settings from "./settings/Settings"

export const LAYOUTS = {
    NEW_MESSAGE: "NEW_MESSAGE",
    SETTINGS: "SETTINGS",
    SEARCH_MULTIPLE_CHATS: "SEARCH_MULTIPLE_CHATS",
    FORWARD_MESSAGE: "FORWARD_MESSAGE",
    GROUP_MANAGEMENT: "GROUP_MANAGEMENT",
    REPORT_CONTENT: "REPORT_CONTENT",
    REPORT_MESSAGE: "REPORT_MESSAGE",
}

export const MAPPING  = {
    [LAYOUTS.FORWARD_MESSAGE]: ForwardMessage,
    [LAYOUTS.NEW_MESSAGE]: NewMessage,
    [LAYOUTS.SETTINGS]: Settings,
    [LAYOUTS.SEARCH_MULTIPLE_CHATS]: SearchMultipleChats,
    [LAYOUTS.GROUP_MANAGEMENT]: GroupManagement,
    [LAYOUTS.REPORT_CONTENT]: ReportContent,
    [LAYOUTS.REPORT_MESSAGE]: ReportMessage




}