// import { produce } from 'immer';
// import { CHANGE_MESSAGE_STATUS } from 'store/actions/global/globalTypes';
// import { GET_CONVERSATIONS } from 'store/actions/global/globalTypes';
// import { PUSH_CONVERSATION } from 'store/actions/global/globalTypes';
// import { GET_MESSSAGES, INITIALIZE_IM, SELECT_CHAT, PUSH_MESSAGE, CHANGE_POINTER } from 'store/actions/global/globalTypes';
// import { MessageStatus } from 'store/actions/global/IM.actions';

// const initialState = {
//     selectedChatId: null,
//     unknownGroupMessages: [],
//     entities: {
//         conversations: {
//             byId: {
//                 // id: {

//                 //     lastRecievedMessages:{

//                 //     }
//                 // participants: {
//                 //     byId: {
//                 //         // {
//                 //         //     user_id: "",
//                 //         //     conversation_id: ""
//                 //         // "username": "string",
//                 //         // "photoPath": "string",
//                 //         // "lastPingDate": 0,
//                 //         // "isOnline": true
//                 //         // }
//                 //     },
//                 //     ids: []
//                 // }
//                 // }
//             },
//             ids: []
//         },
//         messages: {
//             byId: {
//                 // id: {
//                 //     media:{

//                 //     }

//                 // }
//             },
//             nextPage: null,
//             ids: []
//         },
//         users: {
//             byId: {

//             },
//             ids: []
//         },

//     }
// }

// export default (state = initialState, action) => {
//     const { payload, type } = action;
//     return produce(state, draft => {
//         switch (type) {
//             case INITIALIZE_IM:
//                 draft.entities = { ...draft.entities, ...payload }
//                 draft.selectedChatId = payload.conversations.ids[0];
//                 draft.unread=payload.unread;
//                 return draft;

//             case GET_MESSSAGES:

//                 for (let i = 0; i < payload.ids.length; i++)
//                     draft.entities.messages.byId[payload.ids[i]] = payload.byId[payload.ids[i]];

//                 draft.entities.messages.ids = payload.ids.concat(draft.entities.messages.ids);
//                 draft.entities.messages.nextPage = payload.nextPage;
//                 return draft;

//             case SELECT_CHAT:
//                 draft.selectedChatId = payload;
//                 draft.entities.messages = {
//                     byId: {},
//                     ids: [],
//                     nextPage: null
//                 }
//                 // draft.entities.messages.nextPage = draft.entities.conversations.byId[payload].lastMessages.nextPage
//                 return draft;

//             case PUSH_MESSAGE:
//                 if (!draft.entities.conversations.ids.some(id => id === payload.conversationId) && payload.conversationId) {
//                     draft.unknownGroupMessages.push(payload)
//                     return draft;
//                 }


//                 //when coversation id is not provided that means we are sending a message
//                 if(payload.conversationId){
//                 if (!draft.entities.conversations.byId[payload.conversationId].participants.ids.some(id => id === payload.senderId)) {
//                     draft.entities.conversations.byId[payload.conversationId].participants.byId[payload.senderId] = { id: payload.senderId, ...payload.sender };
//                     draft.entities.conversations.byId[payload.conversationId].participants.ids.push(payload.senderId);
//                 }

//                 if ( draft.selectedChatId !== payload.conversationId) {

//                     draft.entities.conversations.byId[payload.conversationId].numberOfUnreadMessages++;

//                     draft.entities.conversations.byId[payload.conversationId].lastMessages.ids.push(payload.id);
//                     draft.entities.conversations.byId[payload.conversationId].lastMessages.byId[payload.id] = payload;


//                     draft.entities.conversations.ids.splice(draft.entities.conversations.ids.indexOf(payload.conversationId), 1);
//                     draft.entities.conversations.ids.unshift(payload.conversationId);

//                     return draft;
//                 }
//             }

//                 //rise on top
//                 draft.entities.conversations.ids.splice(draft.entities.conversations.ids.indexOf(draft.selectedChatId), 1);
//                 draft.entities.conversations.ids.unshift(draft.selectedChatId);

//                 //update last message
//                 draft.entities.conversations.byId[draft.selectedChatId].lastMessages.ids.push(payload.id);
//                 draft.entities.conversations.byId[draft.selectedChatId].lastMessages.byId[payload.id] = payload;

//                 //push to messages entity
//                 draft.entities.messages.ids.push(payload.id);
//                 draft.entities.messages.byId[payload.id] = payload;

//                 return draft;

//             case PUSH_CONVERSATION:

//                 draft.entities.conversations.byId[payload.conversation.id] = {
//                     ...payload.conversation, lastMessages: payload.message ? {
//                         nextPage: 1,
//                         byId: { [payload.message.id]: payload.message },
//                         ids: [payload.message.id]
//                     } : {
//                         byId: draft.unknownGroupMessages.filter(msg => msg.conversationId === payload.conversation.id).reduce((curr, prev) => ({
//                             ...prev,
//                             [curr.id]: curr
//                         }), {}),
//                         ids: draft.unknownGroupMessages.filter(msg => msg.conversationId === payload.conversation.id).map(msg => msg.id),
//                         nextPage: 1
//                     }
//                 }

//                 //to prevent errors when message event is recieved before group event
//                 draft.unknownGroupMessages = draft.unknownGroupMessages.filter(msg => msg.conversationId === payload.conversation.id);

//                 draft.entities.conversations.ids.unshift(payload.conversation.id);
//                 draft.entities.conversations.byId[payload.conversation.id].participants = {
//                     byId: payload.participants.reduce((prev, curr) => ({
//                         ...prev,
//                         [curr.id]: curr
//                     }), {})
//                     , ids: payload.participants.map(pc => pc.id)
//                 }

//                 return draft;

//             case CHANGE_POINTER:
//                 draft.entities.conversations.byId[payload].numberOfUnreadMessages = 0;
//                 return draft;

//             case CHANGE_MESSAGE_STATUS:
//                 if (payload.status === MessageStatus.SUCCESS) {
//                     delete draft.entities.messages.byId[payload.id].status; // succeed messages have not a status
//                     draft.entities.messages.byId[payload.id].id = payload.newId;
//                     return draft;
//                 }

//                 draft.entities.messages.byId[payload.id].status = MessageStatus.ERROR;
//                 return draft;

//             case GET_CONVERSATIONS:
//                 for (let i = 0; i < payload.ids.length; i++)
//                     draft.entities.conversations.byId[payload.ids[i]] = payload.byId[payload.ids[i]];

//                 draft.entities.conversations.ids = payload.ids.concat(draft.entities.conversations.ids);
//                 draft.entities.conversations.nextPage = payload.nextPageId;
//                 return draft;
//             default:
//                 return draft;
//         }
//     })
// }