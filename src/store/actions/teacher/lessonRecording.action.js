import { lessonRecordingApi } from "api/teacher/lessonRecording";

import { GET_LESSON_RECORING } from "./teacherTypes";
import { toast } from "react-toastify";

export const fetchLessonRecording = (lessonId) => async (dispatch) => {
	try {
		const response = await lessonRecordingApi.getLessonRecording(lessonId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_LESSON_RECORING,
				payload: response.data.data.data,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};
