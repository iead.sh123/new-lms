import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useContext, useState } from 'react'
import RLister from 'components/Global/RComs/RLister';
import RButton from 'components/Global/RComs/RButton';
import RSelect from 'components/Global/RComs/RSelect';
import { Input } from 'reactstrap';
import tr from 'components/Global/RComs/RTranslator';
import styles from '../attendance.module.scss'
import iconsFa6 from 'variables/iconsFa6';
import { AttendanceContext } from '../GAttendanceDetailsLister';
import Loader from 'utils/Loader';
import { options, settingsTable, tablesTypes, tabs } from '../constants';
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput';
import * as colors from 'config/constants'
import RFilterTabs from 'components/Global/RComs/RFilterTabs/RFilterTabs';
import AppRadioButton from 'components/UI/AppRadioButton/AppRadioButton';

const RCourseAttendanceSettings = () => {
    const attendanceContext = useContext(AttendanceContext)
    if (attendanceContext.isLoadingAttendanceSettings) {
        return <Loader />
    }
    const firstTime = attendanceContext.firstTime
    const [courseTemplateActive, setCourseTemplateActive] = useState(tabs[0].title)
    const pickedAssignOption = attendanceContext.assignOption
    console.log("Formik State Values", attendanceContext.attendanceStates)
    console.log("Formik State Errors", attendanceContext.errors)
    console.log("Formik State touched", attendanceContext.touched)
    const myTemplateSettings = attendanceContext?.attendanceSettings?.my_template?.map((settingElement, index) => {
        const key = settingElement.id ?? settingElement.fakeId
        return {
            id: key,
            table_name: "MyTemplateTable",
            details: settingsTable.map((header) => {
                const touched = attendanceContext?.touched?.attendanceSettings?.[index]?.[header.value]
                const error = attendanceContext?.errors?.attendanceSettings?.[index]?.[header.value]
                const focused = attendanceContext?.attendanceStates?.attendanceSettings?.[index]?.focused
                const saved = attendanceContext?.attendanceStates?.attendanceSettings?.[index]?.saved?.[header.value] ?? true
                console.log(settingElement.deletable)
                return {
                    key: header.label,
                    value: settingElement?.[header.value] == 'Presented' ? <RFlex styleProps={{ paddingLeft: '10px' }}>{settingElement[header.value]}</RFlex>
                        :
                        <div className='position-relative'>
                            <RHoverInput
                                name={`attendanceSettings.${index}.${header.value}`}
                                inputValue={attendanceContext?.attendanceStates?.attendanceSettings?.[index]?.[header.value] ?? settingElement[header.value]}
                                handleInputDown={attendanceContext.handleInputDown}
                                inputWidth={header.value == "title" ? '170px' : "60px"}
                                type={header.type}
                                focusOnInput={focused || header.value == 'title'}
                                errorClassname
                                inputPlaceHolder=""
                                handleInputChange={attendanceContext.handleChange}
                                inputIsNotValidate={error && touched}
                                item={settingElement}
                                extraInfo={{ id: settingElement.id ?? settingElement.fakeId, type: tablesTypes.attendanceSettings, propretyName: header.value, index }}
                                handleOnBlur={(event, item, extraInfo) => { attendanceContext.handleInputBlur(event, item, extraInfo); attendanceContext.handleBlur(event) }}
                                handleEditFunctionality={attendanceContext.handleEditFunctionality}
                                saved={saved}
                            />
                            {error && touched &&
                                <i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} />}
                        </div>,
                    type: 'component'
                }
            }),
            actions: [
                { name: settingElement.deletable ? "delete" : "", icon: settingElement.deletable ? iconsFa6.delete : '', justIcon: true, actionIconClass: 'text-danger', onClick: () => { settingElement.deletable ? attendanceContext.handleDeleteSetting(key) : '' } },
            ]
        }
    })
    const lastTitleInput = attendanceContext?.attendanceStates?.attendanceSettings?.[attendanceContext?.attendanceStates?.attendanceSettings?.length - 1]?.['title']
    const schoolTemplateSettings = attendanceContext?.attendanceSettings?.school_template?.map((settingElement, index) => {
        const key = settingElement.id ?? settingElement.fakeId
        return {
            id: key,
            table_name: "SchoolTemplate",
            details: settingsTable.map((header) => {
                return {
                    key: header.label,
                    value: firstTime ? <RFlex styleProps={{ cursor: 'pointer' }} onClick={attendanceContext.createCourseTemplate}>{settingElement[header.value]}</RFlex>
                        : settingElement[header.value],
                    type: 'component'
                }
            }),
        }
    })
    const _settings = firstTime ? schoolTemplateSettings : courseTemplateActive == "My_Template" ? myTemplateSettings : schoolTemplateSettings
    if (attendanceContext.creatingTemplate) {
        return <Loader />
    }
    return (
        <>
            <RFlex className="flex-column pt-2 m-0">
                <span className='p-0 m-0' style={{ width: 'fit-content' }}>{tr("Attendance_Settings")}</span>
                {!firstTime &&
                    <>
                        <p className='p-0 m-0 text-warning' style={{ fontSize: '12px' }}>{tr("Changing_Settings_Will_Effect_Grading")} </p>
                        <RFilterTabs tabs={tabs} setTabActive={setCourseTemplateActive} />
                    </>
                }

                <RLister Records={_settings} />
                {(firstTime || courseTemplateActive == "My_Template") &&
                    <RButton
                        text={tr('New_Type')}
                        faicon={iconsFa6.plus}
                        color='link'
                        className={styles.addNewButton}
                        style={{ width: 'fit-content', margin: '0px', padding: '0px' }}
                        onClick={() => { firstTime ? attendanceContext.createCourseTemplate() : attendanceContext.addNewSetting() }}
                    />
                }
                <RFlex className="justify-content-start align-items-center p-0" styleProps={{ gap: "3px" }}>
                    <p style={{ margin: '0px', width: 'max-content' }}>{tr("Teacher_Can_Assign_Or_Edit_Attendance_Within")} </p>
                    <RFlex styleProps={{ gap: '0px', width: "max-content" }}>
                        <span className='p-0 m-0 text-primary font-weight-bold'> {attendanceContext?.attendanceSettings?.max_update_time} Hours </span>
                    </RFlex>
                </RFlex>

                {(courseTemplateActive == "My_Template" && !firstTime) &&
                    <RFlex className="align-items-center">
                        <p className='p-0 m-0' style={{ fontSize: '12px', color: colors.lightGray }}>{tr("Assign_This_Template_To_This_Course_And")} :</p>
                        <RFlex className="flex-column align-items-start" styleProps={{ gap: '7px' }}>
                            <RFlex className="align-items-center">
                                <div style={{ width: "200px" }}>
                                    <RSelect
                                        option={[{ label: 'AllCourses', value: '-1' }].concat(attendanceContext.settingsCourses)}
                                        closeMenuOnSelect={true}
                                        value={attendanceContext.selectedCourses}
                                        placeholder={tr("Select_Courses")}
                                        onChange={(e) => attendanceContext.handleSelectCourse(e)}
                                        required
                                        isMulti
                                    />
                                </div>
                            </RFlex>
                        </RFlex>
                        <RButton
                            text={tr('save')}
                            color='link'
                            loading={attendanceContext.isSettingAttendance}
                            disabled={attendanceContext?.selectedCourses?.length <= 0}
                            className={styles.addNewButton}
                            style={{ width: 'fit-content', margin: '0px', padding: '0px' }}
                            onClick={() => { attendanceContext.handleAddingToAnotherCourses() }}
                        />
                    </RFlex>
                }
                {courseTemplateActive == "My_Template" ? (
                    attendanceContext?.teacherCourses?.courses_have_this_template?.length > 0 &&
                    <RFlex className="align-items-start">
                        <p className='p-0 m-0' style={{ color: colors.lightGray, fontSize: '12px', width: 'fit-content' }}>{tr("This_Template_Is_Assigned_To")} : </p>
                        <p className='p-0 m-0' style={{ color: colors.lightGray, fontSize: '12px', width:'250px' }}>{attendanceContext?.teacherCourses?.courses_have_this_template?.map((course, index) => <span className='p-0 m-0'>{course.name} {index != attendanceContext?.teacherCourses?.courses_have_this_template?.length - 1 && ','} </span>)}</p>

                    </RFlex>
                ) : (
                    <p className='p-0 m-0' style={{ color: colors.lightGray, fontSize: '12px' }}>{tr("This_Template_Is_Assigned_To_All_Courses_By_Default")}</p>
                )}
            </RFlex >
        </>
    )
}

export default RCourseAttendanceSettings