import React, { useContext } from "react";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { useParams } from "react-router-dom";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RSchoolInitiationButtons = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { sIType, sITypeId, tabTitle } = useParams();
  return (
    <RFlex styleProps={{ justifyContent: "end" }}>
      <RButton
        text={tabTitle ? tr`save` : tr`create`}
        color="primary"
        onClick={() => {
          !sIType || (sIType == "education_stage" && tabTitle == "properties")
            ? SchoolInitiationData.handleSaveEducationStage()
            : sIType == "education_stage" && tabTitle !== "properties"
            ? SchoolInitiationData.handleSaveGradeLevels()
            : sIType == "grade_levels" && tabTitle == "properties"
            ? SchoolInitiationData.handleSaveGradeLevels()
            : sIType == "grade_levels" && tabTitle !== "properties"
            ? SchoolInitiationData.handleSaveCurricula()
            : sIType == "curricula" && tabTitle == "properties"
            ? SchoolInitiationData.handleSaveCurricula()
            : "";
        }}
        loading={
          !sIType || (sIType == "education_stage" && tabTitle == "properties")
            ? SchoolInitiationData.saveEducationStageLoading
            : sIType == "education_stage" && tabTitle !== "properties"
            ? SchoolInitiationData.saveGradeLevelLoading
            : sIType == "grade_levels" && tabTitle == "properties"
            ? SchoolInitiationData.saveGradeLevelLoading
            : sIType == "grade_levels" && tabTitle !== "properties"
            ? SchoolInitiationData.saveCurriculaLoading
            : sIType == "curricula" && tabTitle == "properties"
            ? SchoolInitiationData.saveCurriculaLoading
            : ""
        }
        disabled={
          !sIType || (sIType == "education_stage" && tabTitle == "properties")
            ? SchoolInitiationData.saveEducationStageLoading
              ? true
              : false
            : sIType == "education_stage" && tabTitle !== "properties"
            ? SchoolInitiationData.saveGradeLevelLoading
              ? true
              : false
            : sIType == "grade_levels" && tabTitle == "properties"
            ? SchoolInitiationData.saveGradeLevelLoading
              ? true
              : false
            : sIType == "grade_levels" && tabTitle !== "properties"
            ? SchoolInitiationData.saveCurriculaLoading
              ? true
              : false
            : sIType == "curricula" && tabTitle == "properties"
            ? SchoolInitiationData.saveCurriculaLoading
              ? true
              : false
            : SchoolInitiationData.message && !tabTitle
            ? true
            : false
        }
      />
      {!tabTitle && (
        <RButton
          text={tr`cancel`}
          color="link"
          onClick={SchoolInitiationData.handleClose}
          disabled={SchoolInitiationData.message ? true : false}
        />
      )}
    </RFlex>
  );
};

export default RSchoolInitiationButtons;
