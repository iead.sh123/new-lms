import React, { useState, useEffect } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./SchoolLister.module.scss";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "store/actions/global/schoolManagement.action";
import * as colors from "config/constants";
import { dataTypes } from "logic/SchoolManagement/constants";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RListData from "components/Global/RComs/RListData/RListData";
import { types, userTypes } from "./constants";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import GEnrollUsers2 from "./GEnrollUsersV2";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import { TOGGLE_SELECT_MODE, TOGGLE_SELECT_MODE_OFF, TOGGLE_SELECT_MODE_ON } from "store/actions/global/globalTypes";
import { } from "store/actions/global/globalTypes";
import { toast } from "react-toastify";
import GPeriodsV2 from "./GPeriodsV2";
import RListDataV2 from "components/Global/RComs/RListDataV2/RListDataV2";
import GAssessmentSchedularV2 from "./GAssessmentSchedularV2";

const GDetailsListerV2 = (
	{
		levelsProperites,
		activeLevel,
		currentType,
		periods,
		handleBlur,
		handleChange,
		errors,
		touched,
		setFieldValue,
		setFieldTouched,
		setFieldError,
		users,
		assessments
	}) => {
	const dispatch = useDispatch();
	const currentListData = null
	const label = 'Principals'
	const selectMode = currentListData?.selectMode;
	const selectedNumber = currentListData ? currentListData.ids.filter((key) => currentListData[key].selected).length : 0;
	const currentPeriods = periods?.filter((period) => period?.level_id == activeLevel?.id)
	const currentAssessments = assessments?.filter((period) => period?.level_id == activeLevel?.id)
	const principals = currentType == types.EducationStages ? users[types.Principals].filter((principale) => principale.level_id == activeLevel.id) : null
	const seniorTeachers = currentType == types.GradeLevels ? users[types.SeniorTeacher].filter((gradeLevel) => gradeLevel.level_id == activeLevel.id) : null
	const students = currentType == types.GradeLevels ? users[types.Students].filter((gradeLevel) => gradeLevel.level_id == activeLevel.id) : null
	const [enrollUsersOpen, setEnrollUsersOpen] = useState({ isOpen: false, userTypeId: null, userType: "" });
	const handleCloseModal = () => {
		setEnrollUsersOpen({ ...enrollUsersOpen, isOpen: false });
	};

	const toggleSelectModeOn = () => {
		dispatch({ type: TOGGLE_SELECT_MODE_ON, payload: { type: label } });
	};
	const toggleSelectModeOff = () => {
		dispatch({ type: TOGGLE_SELECT_MODE_OFF, payload: { type: label } });
	};
	const selectFilterActions = [
		{
			label: tr("All"),
			action: (type) => {
				dispatch(actions.selectAll(type));
				toggleSelectModeOn();
			},
			parameter1: label,
		},
		{
			label: tr("Clear_Selection"),
			action: (type) => {
				dispatch(actions.clearAllSelection(type));
				toggleSelectModeOff();
			},
			parameter1: label,
		},
	];
	const viewAsFilterActions = [
		{
			label: tr("Alphabetically"),
			action: () => {
				label == dataTypes.Principals
					? dispatch(actions.getPrincipalForEducation(activeLevel.id, "a"))
					: dispatch(actions.getStudentsForGrade(activeLevel.id, "a"));
			},
		},
		{
			label: tr("Recent_Added"),
			action: () => {
				label == dataTypes.Principals
					? dispatch(actions.getPrincipalForEducation(activeLevel.id, "r"))
					: dispatch(actions.getStudentsForGrade(activeLevel.id, "r"));
			},
		},
	];

	const filterBy = (
		<RFlex
			id="filter By"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + colors.primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr("Sort_By")}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const handleDeleteButton = () => {
		if (label == dataTypes.Principals) {
			dispatch(actions.removePrinipalsFromStage());
		} else if (label == dataTypes.Students) {
			dispatch(actions.removeStudentsFromGrade());
		}
	};
	const handleSelectUser = (type, entity) => {
		if (!entity.selected) {
			dispatch(actions.selectUser(type, entity.id));
		} else {
			dispatch(actions.unSelectUser(type, entity.id));
		}
	};
	const handleSendNotification = async () => {
		let success = false;
		if (label == dataTypes.Principals) {
			success = await dispatch(actions.sendNotificationToPrincipals());
			if (success) toast.success(tr`Notification is Successfully Sent`);
		} else if (label == dataTypes.Students) {
			success = dispatch(actions.sendNotificationToStudents());
			if (success) toast.success(tr`Notification is Successfully Sent`);
		}
	};
	console.log("currentTypecurrentType", currentType)
	return (
		<>
			{currentType != types.School ? (
				<>
					<RFlex>
						<p className="p-0 m-0 font-weight-bold">{activeLevel?.name}</p>
						<AppModal
							show={enrollUsersOpen.isOpen}
							size={"md"}
							header={true}
							headerSort={<GEnrollUsers2
								closeModal={handleCloseModal}
								userType={enrollUsersOpen.userType}
								userTypeId={enrollUsersOpen.userTypeId}
								levelId={activeLevel.id} />}
							parentHandleClose={handleCloseModal}
						/>
					</RFlex>
					<RFlex id="Buttons" className="justify-content-between align-items-baseline" styleProps={{ width: "100%" }}>
						<RFlex id="active select" className="align-items-center" styleProps={{ gap: "15px" }}>
							<RFlex id="select with icon" className="align-items-baseline" styleProps={{ gap: "8px" }}>
								{selectMode && <span className="p-0 m-0 text-success">{selectedNumber}</span>}
								<span
									onClick={() => (currentListData?.ids?.length <= 0 ? null : toggleSelectModeOn())}
									className="p-0 m-0"
									style={{
										fontSize: "12px",
										cursor: currentListData?.ids?.length <= 0 ? "default" : "pointer",
										color: currentListData?.ids?.length <= 0 ? colors.lightGray : colors.successColor,
									}}
								>
									{selectMode ? "Selected" : "Select"}
								</span>
								{/* <RButton
                                    disabled={currentListData?.ids?.length <= 0}
                                    text={selectMode ? "Selected" : "Select"}
                                    onClick={toggleSelectModeOn}
                                    color="link"
                                    className="text-success"
                                    style={{ margin: '0px', padding: '5px 10px' }}
                                /> */}
								{currentListData?.ids?.length <= 0 ? null : (
									<RDropdownIcon
										actions={selectFilterActions}
										menuClassname={styles.SelectDropDown}
										iconContainerStyle={{ border: "1px solid" + colors.successColor, borderRadius: "100%" }}
									/>
								)}
							</RFlex>
							{selectMode && (
								<>
									<RButton
										disabled={selectedNumber <= 0 ? true : false}
										text="Send Notification"
										faicon={iconsFa6.bell}
										onClick={handleSendNotification}
										color="primary"
										outline
										style={{ margin: "0px", padding: "5px 10px" }}
									/>
									<RButton
										faicon={iconsFa6.delete}
										iconStyle={{ color: colors.dangerColor }}
										text={"Remove"}
										onClick={() => {
											handleDeleteButton();
										}}
										color="link"
										disabled={selectedNumber <= 0 ? true : false}
										style={{ margin: "0px", padding: "5px 10px" }}
										className="text-danger"
									/>
								</>
							)}
						</RFlex>
						<RDropdownIcon actions={viewAsFilterActions} component={filterBy} />
					</RFlex>
					{currentType == types.EducationStages &&
						<RListDataV2
							onClick={handleSelectUser}
							inSelectMode={selectMode}
							userType={tr("Principals")}
							userTypeId={userTypes[types.Principals]}
							data={principals}
							setEnrollUsersOpen={setEnrollUsersOpen}
						/>
					}
					{currentType == types.GradeLevels &&
						<>
							<RListDataV2
								onClick={handleSelectUser}
								inSelectMode={selectMode}
								userType={tr("Senior_Teachers")}
								userTypeId={userTypes[types.SeniorTeacher]}
								data={seniorTeachers}
								setEnrollUsersOpen={setEnrollUsersOpen}
							/>
							<RListDataV2
								onClick={handleSelectUser}
								inSelectMode={selectMode}
								userType={tr("Students")}
								userTypeId={userTypes[types.Students]}
								data={students}
								setEnrollUsersOpen={setEnrollUsersOpen}
							/>
						</>
					}

					<GPeriodsV2
						currentPeriods={currentPeriods}
						allPeriods={periods}
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						setFieldValue={setFieldValue}
						levelId={activeLevel.id}
						setFieldError={setFieldError}
						setFieldTouched={setFieldTouched}
					/>
					<GAssessmentSchedularV2
						currentAssessments={currentAssessments}
						allAssessments={assessments}
						errors={errors}
						touched={touched}
						handleChange={handleChange}
						handleBlur={handleBlur}
						setFieldValue={setFieldValue}
						levelId={activeLevel.id}
						setFieldTouched={setFieldTouched} />
				</>
			) : (
				""
			)}
		</>
	);
};

export default GDetailsListerV2;
