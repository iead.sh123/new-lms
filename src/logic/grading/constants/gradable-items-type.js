export const GradableItemTypes = {
    Assignment:  "assignments",
    Exam:  "exams",
    Quiz:  "quizzes",
    Poll:  "polls",
    Survey:  "surveys",
    Project:  "projects",
    Post:  "post",
    Attendance:  "attendance",
    Custom : "custom",
}