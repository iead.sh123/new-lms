import React from "react";
import RLoader from "components/Global/RComs/RLoader";
import styles from "../../cart.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { CART_ITEMS } from "config/constants";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { paymentApi } from "api/global/payment";
import CartItems from "../../CartItems";
import CartPrices from "../../CartPrices";

const OrderSummary = () => {
	const cartItems =
		typeof window !== "undefined"
			? localStorage.getItem(CART_ITEMS)?.length > 0
				? localStorage.getItem(CART_ITEMS).split(",").map(Number)
				: []
			: [];

	const finalCartData = useFetchDataRQ({ queryKey: ["final-cart", cartItems], queryFn: () => paymentApi.finalCart(cartItems) });

	if (finalCartData.isLoading) {
		return <RLoader />;
	}

	return (
		<RFlex styleProps={{ gap: 20, flexDirection: "column", width: "75%" }}>
			<RFlex styleProps={{ flexDirection: "column" }}>
				<span className={styles.payment_title}>{tr`order_summary`}</span>
				<div className={styles.divider} />
			</RFlex>
			{finalCartData?.data?.data?.data?.data?.length > 0 && (
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex
						className={styles.aside__content + " scroll_hidden"}
						styleProps={{
							flexDirection: "column",
							height: `calc(100vh - ${!!finalCartData?.data?.data?.data?.data?.length ? "250px - 3.25rem" : "0px"})`,
						}}
					>
						{finalCartData?.data?.data?.data?.data?.map((item, index, arr) => (
							<CartItems key={item.id} item={item} viewMode={true} />
						))}
					</RFlex>
					<div className={styles.divider} />
					<CartPrices prices={finalCartData?.data?.data?.data?.prices} />
				</RFlex>
			)}
		</RFlex>
	);
};

export default OrderSummary;
