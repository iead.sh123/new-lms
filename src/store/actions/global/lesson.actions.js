import { lessonApi } from "api/global/lesson";
import {
	POST_ADD_LESSON_REQUEST,
	POST_ADD_LESSON_SUCCESS,
	POST_ADD_LESSON_ERROR,
	CHANGE_STATUS_LESSON_REQUEST,
	CHANGE_STATUS_LESSON_SUCCESS,
	CHANGE_STATUS_LESSON_ERROR,
	GET_SECTION_LESSONS_REQUEST,
	GET_SECTION_LESSONS_SUCCESS,
	GET_SECTION_LESSONS_ERROR,
	GET_SECTION_STUDENTS_REQUEST,
	GET_SECTION_STUDENTS_SUCCESS,
	GET_SECTION_STUDENTS_ERROR,
	REMOVE_LESSON_LESSON_REQUEST,
	REMOVE_LESSON_LESSON_SUCCESS,
	REMOVE_LESSON_LESSON_ERROR,
	GET_LESSON_BY_ID_REQUEST,
	GET_LESSON_BY_ID_SUCCESS,
	GET_LESSON_BY_ID_ERROR,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_REQUEST,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_SUCCESS,
	CREATE_OR_EDIT_MODULE_CONTENT_LESSON_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const fetchSectionLessons = (sectionId) => async (dispatch) => {
	dispatch({ type: GET_SECTION_LESSONS_REQUEST });

	try {
		const response = await lessonApi.getSectionLessons(sectionId);

		if (response.data.status === 1) {
			const sectionLessons = response.data.data;

			dispatch({
				type: GET_SECTION_LESSONS_SUCCESS,
				payload: { sectionLessons },
			});
		} else {
			dispatch({
				type: GET_SECTION_LESSONS_ERROR,
				payload: response.data.message,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_SECTION_LESSONS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const postAddLessonAsync = (sectionId, data, handleClose, handleEmptyState) => async (dispatch) => {
	dispatch({ type: POST_ADD_LESSON_REQUEST });
	try {
		const response = await lessonApi.addLesson(sectionId, data);

		if (response.data.status === 1) {
			dispatch({
				type: POST_ADD_LESSON_SUCCESS,
			});

			handleClose();
			handleEmptyState();
			dispatch(fetchSectionLessons(sectionId));
		} else {
			dispatch({ type: POST_ADD_LESSON_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: POST_ADD_LESSON_ERROR,
		});
		toast.error(error?.message);
	}
};

export const removeLessonAsync = (lessonId, sectionId, hideAlertDeleteLesson) => async (dispatch) => {
	dispatch({ type: REMOVE_LESSON_LESSON_REQUEST });

	try {
		const response = await lessonApi.removeLesson(lessonId);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_LESSON_LESSON_SUCCESS,
			});

			hideAlertDeleteLesson();
			dispatch(fetchSectionLessons(sectionId));
		} else {
			dispatch({ type: REMOVE_LESSON_LESSON_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_LESSON_LESSON_ERROR,
		});
		toast.error(error?.message);
	}
};

export const changeStatusLessonAsync = (lessonId, sectionId, hideAlert) => async (dispatch) => {
	dispatch({ type: CHANGE_STATUS_LESSON_REQUEST });

	try {
		const response = await lessonApi.changeStatusLesson(lessonId);

		if (response.data.status === 1) {
			dispatch({
				type: CHANGE_STATUS_LESSON_SUCCESS,
			});

			hideAlert();
			dispatch(fetchSectionLessons(sectionId));
		} else {
			dispatch({ type: CHANGE_STATUS_LESSON_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CHANGE_STATUS_LESSON_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchSectionStudents = (sectionId) => async (dispatch) => {
	dispatch({ type: GET_SECTION_STUDENTS_REQUEST });

	try {
		const response = await lessonApi.getSectionStudents(sectionId);

		if (response.data.status === 1) {
			const sectionStudents = response.data.data.data;

			dispatch({
				type: GET_SECTION_STUDENTS_SUCCESS,
				payload: { sectionStudents },
			});
		} else {
			dispatch({
				type: GET_SECTION_STUDENTS_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_SECTION_STUDENTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const lessonByIdAsync = (lessonId, setModuleContentLesson) => async (dispatch) => {
	dispatch({ type: GET_LESSON_BY_ID_REQUEST });

	try {
		const response = await lessonApi.lessonById(lessonId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_LESSON_BY_ID_SUCCESS,
				payload: { data },
			});
			if (setModuleContentLesson) {
				setModuleContentLesson(data);
			}
		} else {
			dispatch({
				type: GET_LESSON_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_LESSON_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const createOrEditModuleContentLessonAsync =
	(moduleId, lessonId, data, type, handleCloseLessonModal, lessonIdFlag, publish) => async (dispatch) => {
		dispatch({ type: CREATE_OR_EDIT_MODULE_CONTENT_LESSON_REQUEST });
		try {
			const response = lessonIdFlag
				? await lessonApi.editLessonToModuleContent(lessonId, data, publish)
				: await lessonApi.createLessonToModuleContent(moduleId, data, publish);

			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: CREATE_OR_EDIT_MODULE_CONTENT_LESSON_SUCCESS,
					payload: { data, moduleId, type },
				});
				handleCloseLessonModal();
			} else {
				dispatch({ type: CREATE_OR_EDIT_MODULE_CONTENT_LESSON_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: CREATE_OR_EDIT_MODULE_CONTENT_LESSON_ERROR });
			toast.error(error?.message);
		}
	};
