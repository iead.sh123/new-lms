import { Col, Row } from 'reactstrap';
import styles from "./styles/theme1.Module.scss";
import tr from '../../RTranslator';
import React from 'react';

const GradeTheme1 = ({
    color,
    course_name,
    details,
    dir
}) =>
    <Col lg={4} md={6} sm={12} dir={dir ? "ltr" : "rtl"}>
        <div className={`card ${styles["NightMode"]}`}>
            <div
                className={styles.cardIndex}
                style={{
                    background: color,
                }}
            >
                <span>{course_name}</span>
            </div>
            <div className="card-body">
                <Row>
                    {details.length != 0 ? (
                        details?.map(detail => (
                            <React.Fragment key={detail.rab_id + detail.name}>
                                <Col xs={6}>{detail.name}:</Col>
                                <Col xs={6}>{detail.mark}</Col>
                                <hr />
                            </React.Fragment>
                        ))
                    ) : (
                        <div
                            style={{
                                top: "50%",
                                left: "30%",
                                position: "absolute",
                            }}
                        >
                            <h3
                                style={{
                                    color: color,
                                }}
                            >{tr`no_grade_yet`}</h3>
                        </div>
                    )}
                </Row>
            </div>
        </div>
    </Col>
export default GradeTheme1;