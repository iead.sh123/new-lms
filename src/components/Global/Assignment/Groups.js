import React, { memo } from "react";
import { useSelector } from "react-redux";
import { Badge, Row } from "reactstrap";

function toOrder(num) {
	if (num == 1) return "1st";
	if (num == 2) return "2nd";
	if (num == 3) return "3rd";
	if (num > 3) return num + "th";
}
function Counter(num, singular) {
	if (num == 1) return num + " " + singular;
	else return num + " " + singular + "s";
}
function Groups({ group, lessonGroups, currentActiveGroup }) {
	const userType = useSelector((state) => state?.auth?.user?.type);
	return (
		group.pattern_group_text && (
			<Row>
				<div
					className="col-sm-10 mt-1"
					style={userType != "student" && userType != "learner" ? { marginLeft: "-2rem" } : { marginLeft: "-1.5rem" }}
				>
					<p className="gu-assignment-group-name gu-assignment_group_text">
						{/*   <i className='fa fa-chevron-circle-left '></i>  */}
						{group.pattern_group_text || ""}
					</p>
					{/* <input value={group.pattern_group_text} type="text" className="form-control" id="inputEmail3" disabled /> */}
				</div>

				<div
					className="col-sm-2"
					style={userType != "student" && userType != "learner" ? { marginLeft: "-0.5rem" } : { marginLeft: "1rem" }}
				>
					<p className="gu-assignment_group_questionCounter">
						{toOrder(currentActiveGroup + 1)} / {Counter(lessonGroups.inst_pattern_groups.length, " Group")}
					</p>
				</div>
			</Row>
		)
	);
}

export default memo(Groups);
