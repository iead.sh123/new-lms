import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { FormGroup, Input } from "reactstrap";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";

const Assignments = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<div className="mt-2">
			<RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
				<AppCheckbox
					onChange={(event) =>
						CourseModuleData.handleChangeBehaviorInModuleContent({
							moduleId: CourseModuleData.specificModule.id,
							moduleContentId: content.id,
							key: "dueDate",
							value: event.target.checked == true ? " " : null,
						})
					}
					label={tr`due_date`}
					checked={content?.behavior?.dueDate ? true : false}
				/>
				{content?.behavior?.dueDate && (
					<FormGroup>
						<Input
							name="dueDate"
							type="datetime-local"
							placeholder={tr`due_date`}
							value={moment(content?.behavior?.dueDate).format("YYYY-MM-DDTHH:mm")}
							style={{ height: "30px", width: "200px" }}
							onChange={(event) => {
								const localDateTime = new Date(event.target.value);

								CourseModuleData.handleChangeBehaviorInModuleContent({
									moduleId: CourseModuleData.specificModule.id,
									moduleContentId: content.id,
									key: event.target.name,
									value: localDateTime.toISOString(),
								});
							}}
						/>
					</FormGroup>
				)}
			</RFlex>
			<RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
				<AppCheckbox
					onChange={(event) =>
						CourseModuleData.handleChangeBehaviorInModuleContent({
							moduleId: CourseModuleData.specificModule.id,
							moduleContentId: content.id,
							key: "cutoffDate",
							value: event.target.checked == true ? " " : null,
						})
					}
					label={tr`cut_off_date`}
					checked={content?.behavior?.cutoffDate ? true : false}
				/>
				{content?.behavior?.cutoffDate && (
					<FormGroup>
						<Input
							name="cutoffDate"
							type="datetime-local"
							placeholder={tr`cut_off_date`}
							value={moment(content?.behavior?.cutoffDate).format("YYYY-MM-DDTHH:mm")}
							style={{ height: "30px", width: "200px" }}
							onChange={(event) => {
								const localDateTime = new Date(event.target.value);
								CourseModuleData.handleChangeBehaviorInModuleContent({
									moduleId: CourseModuleData.specificModule.id,
									moduleContentId: content.id,
									key: event.target.name,
									value: localDateTime.toISOString(),
								});
							}}
						/>
					</FormGroup>
				)}
			</RFlex>
		</div>
	);
};

export default Assignments;
