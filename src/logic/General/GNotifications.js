import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { Services } from "engine/services";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { viewNotificationsAsync } from "store/actions/global/notification.action";
import { setNotifyMe } from "store/actions/global/notification.action";
import RNotificationGroup from "view/Notifications/RnotificationGroup";
import { genericPath } from "engine/config";

const GNotifications = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const notifyMe = useSelector((state) => state?.NotificationsRed?.notifyMe);
  const setNotify = (n) => {
    dispatch(setNotifyMe(n));
  };
  const [nots, setNots] = useState([]);
  useEffect(() => {
    const getAll = async () => {
      const allNOts = await get(
        `${Services.notficiation.backend}api/notification/all`
      );
      if (allNOts.data?.data?.data) {
        setNots(allNOts.data.data.data);
      }
    };
    getAll();
  }, []);

  const handleRemoveNotification = (notificationId) => {
    dispatch(viewNotificationsAsync(notificationId));
  };
  const moveToDetails = (notificationId) => {
    history.push(`/${genericPath}/notification/` + notificationId);
  };

  return (
    <RNotificationGroup
      onMarkAsSeen={() => {}}
      onStopNotificationChange={(e) => {
        setNotify(e ? 0 : 1);
      }}
      allNotifications={nots?.length??0}
      notifyMe={notifyMe}
      group={{
        icon: "star",
        title: "Notifications",
        description: "Notifications description",
        notifications: nots,
      }}
      handleOnClick={moveToDetails}
      handleOnSeenClick={handleRemoveNotification}
    />
  );
};
export default GNotifications;
