import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import RButtonIcon from "./RButtonIcon";
import RRow from "view/RComs/Containers/RRow";
import RFlex from "./RFlex/RFlex";

const RModal = ({
  isOpen,
  toggle,
  header,
  footer,
  body,
  size = "lg",
  className,
  withCloseIcon=true,
  footerStyle
}) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle} size={size} className={className}  backdrop="static">
      <ModalHeader style={{fontFamily: 'Cairo',fontStyle: "normal",fontWeight: "400",fontSize: "16px",lineHeight: "30px",textAlign:"left"}}>
        {/* <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
          
        >
          <span aria-hidden="true">×</span>
        </button> */}

      

<RFlex styleProps={{width:"100%"}}>
        <div style={{flexGrow:"100",alignSelf:"flex-start"}}>{header}</div>
        
       {withCloseIcon && <RButtonIcon onClick={toggle} style={ {border: "red 0px solid",
                  //position: "relative",
                  width: "100%",
                  backgroundColor: "inherit",
                  alignSelf:"flex-end"
                }  }

          ButtonAdditionalStyle={{border: "0px", background: "inherit",marginLeft:"0px"}}
          color={"red"}
          backgroundColor={"white"}
          text={"delete"}
          padding={"1px"}
          faicon={"close"}>

       </RButtonIcon>}
          </RFlex>
      </ModalHeader>
      <ModalBody>{body}</ModalBody>
      <ModalFooter style={footerStyle}>{footer}</ModalFooter>
    </Modal>
  );
};
export default RModal;
