import RTabsPanel from "components/Global/RComs/RTabsPanel";
import RTabsPanel_Horizontal from "components/Global/RComs/RTabsPanel_Horizontal";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import { genericPath } from "engine/config";
import { baseURL } from "engine/config";
import React, { useCallback, useEffect, useState } from "react";
import { GCollaborationSettingContents } from "./GCollaborationSettingContents";
import { GCollaborationAgreementsClients } from "./GCollaborationAgreementsClients";
import { GCollaborationAgreementsProviders } from "./GCollaborationAgreementsProviders";
import tr from "components/Global/RComs/RTranslator";
import RFormModal from "components/Global/RComs/RFormModal";

import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import { useReducer } from "react";
import RSubmit from "components/Global/RComs/RSubmit";
import produce from "immer";
import store from "store";
import { put } from "config/api";
import { post } from "config/api";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { SUCCESS } from "utils/Alert";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";

const initialState = {
	organizations: [],
	contents: [],

	levels: [],
	form: [],
	agreement: { provider: null, details: [] },
};

const reducer = (state, action) => {
	switch (action.type) {
		case "SET_ORGANIZATIONS":
			return { ...state, organizations: action.payload };
		case "SET_CONTENTS":
			return { ...state, contents: action.payload };
		case "SET_LEVELS":
			return { ...state, levels: action.payload };

		case "RESET_DETAILS":
			const new_state6 = produce(state, (temp_state) => {
				temp_state.agreement.details = [];
			});
			return new_state6;
		case "ADD_DETAIL":
			const new_state2 = produce(state, (temp_state) => {
				temp_state.agreement.details.push({
					content_id: action.payload.content_id,
					level_id: action.payload?.level_id ?? temp_state.levels[0].id,
				});
			});
			//Helper.cl(new_state2,"ADD_DETAIL new_state")
			return new_state2;

		case "REMOVE_DETAIL":
			const new_state3 = produce(state, (temp_state) => {
				const det = temp_state.agreement.details.filter((e) => e.content_id != action.payload.content_id);
			});
			//Helper.cl(new_state3,"REMOVE_DETAIL new_state")
			return new_state3;

			//---------------------
			if (e.target.checked) dispatch({ type: "", payload: { content_id: i } });
			else dispatch({ type: "", payload: { content_id: i } });

		case "ADD_CONTENT":
			const new_state9 = produce(state, (temp_state) => {
				const com = temp_state.contents.filter((c) => c.id == action.payload.content_id);
				//Helper.cl(com,"com");
				if (com) {
					if (!temp_state.agreement.contents) temp_state.agreement.contents = [];
					temp_state.agreement.contents.push({ ...com[0], levels: [] });
				}
				//  else
				//  {Helper.cl("com null");}
			});
			//Helper.cl(new_state9,"ADD_CONTENT new_state")
			return new_state9;

		case "REMOVE_CONTENT":
			const new_state10 = produce(state, (temp_state) => {
				//// Helper.cl(state,"rm content temp_state")
				//Helper.cl(action.payload.content_id,"rm content action.payload.content_id")
				temp_state.agreement.contents = temp_state.agreement.contents.filter((e) => {
					// Helper.cl(e.id,"rm content e.id")
					return e.id != action.payload.content_id;
				});
			});
			Helper.cl(new_state10, "REMOVE_CONTENT new_state");
			return new_state10;
		//-----------------

		case "SET_LEVEL": //type:"SET_LEVEL"
			//Helper.cl(state,"SET_LEVEL before state")
			//Helper.cl(action.payload,"SET_LEVEL payload")
			const new_state5 = produce(state, (temp_state) => {
				const s = temp_state.agreement.details.filter((e) => e.id == action.payload.content_id);

				if (s && s.length) s[0].level_id = action.payload.level_id;
			});
			//Helper.cl(new_state5,"SET_LEVEL new_state")
			return new_state5;
		//dispatch({ type: "SET_LEVELS", payload: {levels:selectedLevels,content_id:i}});
		case "SET_AGREEMENT_LEVELS": //type:"SET_LEVEL"
			const new_state8 = produce(state, (temp_state) => {
				const s = temp_state.agreement.contents.filter((e) => e.id == action.payload.content_id);
				if (s && s.length) {
					s[0].levels = action.payload.levels;
				}
			});
			//Helper.cl(new_state8,"al SET_AGREEMENT_LEVELS new_state")
			return new_state8;
		case "SET_PROVIDER":
			const new_state4 = produce(state, (temp_state) => {
				if (!temp_state.agreement) temp_state.agreement = {};
				temp_state.agreement.provider = action.payload.provider;
			});
			//Helper.cl(new_state4,"SET_PROVIDER new_state")
			return new_state4;

		case "SET_CONSUMER":
			const new_state11 = produce(state, (temp_state) => {
				if (!temp_state.agreement) temp_state.agreement = {};
				temp_state.agreement.destination = action.payload.consumer;
			});
			//Helper.cl(new_state4,"SET_PROVIDER new_state")
			return new_state11;

		case "SET_FORM":
			return { ...state, form: action.payload };
		case "SET_AGREEMENT_CONTENTS":
			// aggregateContents}
			//Helper.cl(state,"SET_AGREEMENT_CONTENTS state")
			//Helper.cl(action.payload,"SET_AGREEMENT_CONTENTS action.payload")
			const new_state7 = produce(state, (temp_state) => {
				temp_state.agreement.contents = action.payload;
			});
			//Helper.cl(new_state7,"SET_AGREEMENT_CONTENTS new_state")
			return new_state7;

		case "CHANGE_CONTENT_TYPE":
			// new_value:e,id:i

			const new_state = produce(state, (temp_state) => {
				const slectedItems = temp_state.form.filter((item) => {
					const parts = item.name.split("_");
					const id = parts[0];

					return id == action.payload.id && item.type === "select";
				});
				if (slectedItems && slectedItems.length) {
					const slectedItem = slectedItems[0];
					slectedItem.isDisabled = !action.payload.new_value;
				}
			});
			//Helper.cl(new_state,"changeContentType new_state")
			return new_state;
		case "CHANGE_LEVEL":
			// new_value:e,id:i
			//Helper.cl(state.form,"changeContentType form")
			const new_state1 = produce(state, (temp_state) => {
				const slectedItems = temp_state.form.filter((item) => {
					const parts = item.name.split("_");
					const id = parts[0];
					return id == action.payload.id && item.type === "select";
				});
				if (slectedItems && slectedItems.length) {
					const slectedItem = slectedItems[0];
					slectedItem.value = action.payload.new_value;
				}
			});
			//Helper.cl(new_state1,"changeContentType new_state")
			return new_state;
		default:
			return state;
	}
};
export const AgreementEditor = ({ agreement_id, openAdd, setOpenAdd, updateClients }) => {
	//  const[organizations,setOrganizations]= useState([]);
	//  const[contents,setContents]= useState([])
	//  const[levels,setLevels]= useState([])
	const [state, dispatch] = useReducer(reducer, initialState);
	const { organizations, contents, levels, form, agreement } = state;

	const [aggregatedDetails, setAggregatedDetails] = useState([]);
	const aggregate = (details) => {
		const aggregateContents = [];
		const det1 = details.map((d) => {
			const lid = d.collaboration_level.id;
			const cid = d.content_type.id;
			const prev = aggregateContents.filter((a) => a.id == cid);
			if (prev && prev.length) {
				if (!prev[0].levels) prev[0].levels = [];
				const prevlev = prev[0].levels.filter((ll) => ll.id == lid);
				if (!(prevlev && prevlev.length)) {
					prev[0].levels.push(d.collaboration_level);
					//Helper.cl("added ------------------------");
				} else {
					//Helper.cl("not added 000000000000000");
					//Helper.cl(d.collaboration_level,"atest collaboration_level");
					//Helper.cl(prevlev,"atest prevlev");
				}
			} else {
				const o = d.content_type;
				o.levels = [];
				o.levels.push(d.collaboration_level);
				//Helper.cl("first added -------------------");
				aggregateContents.push(o);
			}
		});
		//Helper.cl(aggregateContents,"aggregateContents");
		dispatch({ type: "SET_AGREEMENT_CONTENTS", payload: aggregateContents });
	};

	useEffect(() => {
		const getFromBackend = async () => {
			//{{collaboration_url}}/api/organization/all
			//{{collaboration_url}}/api/organization/2/contents
			//{{collaboration_url}}/api/organization/2/levels
			const url = Services.collaboration.backend + `api/collaboration-agreement/` + agreement_id;
			let response1 = await get(url);
			if (response1 && response1.data && response1.data.status == 1) {
				const agreement1 = response1.data.data.agreement;
				dispatch({ type: "SET_PROVIDER", payload: { provider: { id: agreement1.source_org_id, name: agreement1.source_org_name } } });
				dispatch({
					type: "SET_CONSUMER",
					payload: { consumer: { id: agreement1.destination_org_id, name: agreement1.destination_org_name } },
				});
				aggregate(agreement1?.details);
				//-----------------
				return;
			}
		};
		if (!agreement_id) {
			dispatch({ type: "SET_PROVIDER", payload: { provider: null } });
			dispatch({ type: "SET_CONSUMER", payload: { consumer: null } });

			dispatch({ type: "SET_AGREEMENT_CONTENTS", payload: [] });
			return;
		} else getFromBackend();
	}, [agreement_id]);

	const [agreementChanged, setAgreementChanged] = useState(true);
	const [form1, setForm1] = useState(); //agreement.;

	useEffect(() => {
		const getFromBackend = async () => {
			//{{collaboration_url}}/api/organization/all
			//{{collaboration_url}}/api/organization/2/contents
			//{{collaboration_url}}/api/organization/2/levels
			const url = Services.collaboration.backend + `api/organization/all`;

			let response1 = await get(url);
			if (response1 && response1.data && response1.data.status == 1) {
				dispatch({
					type: "SET_ORGANIZATIONS",
					payload: response1.data.data.data,
				});
			}
		};
		getFromBackend();
	}, []);

	// useEffect(() => {
	//   dispatch({
	//     type: "SET_FORM",
	//     payload: [
	//       {
	//         col:12,
	//         beginning:true,
	//         type: "select",
	//         label: tr`with`,
	//         name: "with",
	//         option: organizations?.map((e) => {
	//           return { label: e.name, value: e.id };
	//         }),
	//         onChange: changeDestinationOrganization,
	//       },
	//     ],
	//   });
	// }, [organizations]);

	const changeLevel = (e, i) => {
		//dispatch({ type: "CHANGE_LEVEL", payload: {new_value:e.target.checked,id:i}});
		dispatch({ type: "SET_LEVELS", payload: { level_id: e.value, content_id: i } });
	};
	const changeLevels = (e, i) => {
		const selectedLevels = e.filter((ll) => ll.checked);
		dispatch({
			type: "SET_AGREEMENT_LEVELS",
			payload: {
				levels: selectedLevels.map((l) => {
					return { ...l, id: l.value };
				}),
				content_id: i,
			},
		});
	};
	//useCallback(()=>changeContentType(c.id),form),
	const changeContentType = (e, i) => {
		//Helper.cl(e.target.checked,"before dispatch e.target.checked")
		// if(e.target.checked)
		//   dispatch({ type: "ADD_DETAIL", payload: {content_id:i}});
		// else
		//   dispatch({ type: "REMOVE_DETAIL", payload: {content_id:i}});

		if (e.target.checked) dispatch({ type: "ADD_CONTENT", payload: { content_id: i } });
		else dispatch({ type: "REMOVE_CONTENT", payload: { content_id: i } });
	};

	const getcontentsandlevels = async (id) => {
		const url = Services.collaboration.backend + `api/organization/${id}/contents`;
		let response1 = await get(url);
		const url2 = Services.collaboration.backend + `api/organization/${id}/levels`;
		let response2 = await get(url2);

		let lvls = [];
		let cntnts = [];
		if (response1 && response1.data && response1.data.status == 1) {
			cntnts = response1.data.data.data;
			dispatch({ type: "SET_CONTENTS", payload: cntnts });
			// dispatch({ type: "SET_AGREEMENT_CONTENTS",payload:cntnts});
		}
		if (response2 && response2.data && response2.data.status == 1) {
			lvls = response2.data.data.data;
			dispatch({ type: "SET_LEVELS", payload: lvls });
		}

		//dispatch({ type: "RESET_DETAILS"});
		// if (lvls && cntnts) {
		//  // let newForm = [];
		//   cntnts.map((c) => {
		//    dispatch({ type: "ADD_DETAIL", payload: {content_id:c.id,content_name:c.name,level_id:lvls[0].id} });
		//   });

		// }
		Helper.cl("as1");
	};

	const changeDestinationOrganization = async (e) => {
		//   //--------------------------
		//   //changeDestinationOrganization
		//   //Helper.cl(e,"changeDestinationOrganization e")
		dispatch({ type: "SET_CONSUMER", payload: { consumer: { id: e.value, name: e.label } } });
		//   dispatch({ type: "RESET_DETAILS"});

		//   getcontentsandlevels(e.value)
	};

	//--------------------------------------------------------------code for client initialization
	// const changeDestinationOrganization = async (e) => {
	//   //--------------------------
	//   //changeDestinationOrganization
	//   //Helper.cl(e,"changeDestinationOrganization e")
	//   dispatch({ type: "SET_PROVIDER", payload: {provider:{id:e.value,name:e.label}} });
	//   dispatch({ type: "RESET_DETAILS"});

	//   getcontentsandlevels(e.value)
	// };
	//   useEffect(()=>{

	//     Helper.cl("constructForm");

	//     getcontentsandlevels(agreement?.provider?.id)
	// },[agreement?.provider])

	//-----------------------------------------------------------

	const initializeFormMeAsTheProvider = async () => {
		//--------------------------

		dispatch({ type: "SET_PROVIDER", payload: { provider: { id: store.getState().auth.user.organization_id } } });
		dispatch({ type: "RESET_DETAILS" });

		getcontentsandlevels(store.getState().auth.user.current_organization);
	};

	useEffect(() => {
		initializeFormMeAsTheProvider();
	}, []);
	const addSubmit = async (values) => {
		//Helper.cl(values,"addSubmit values");
		//Helper.cl(agreement,"addSubmit agreement");

		const source_org_id = store.getState().auth?.user?.organization_id; //useSelector(s=>s.auth.user.organization_id);//

		const destination_org_id = agreement_id ? agreement.destination.id : agreement.destination.id;

		//[0].value,
		//Helper.cl(values.target[0],"values.target");
		//Helper.cl(destination_org_id,"destination_org_id");
		let details = [];
		//destination_org_id=values[0].value;
		//const name =
		//for(let i=1;i<values.length;i++)
		for (let i = 0; i < agreement?.contents?.length; i++) {
			const c = agreement?.contents[i];
			if (c.levels && c.levels.length) {
				c.levels.map((l) => {
					details.push({ collaboration_level_id: l.value ?? l.id, content_type_id: c.id });
				});
			} else {
				toast.error(tr`you must select 1 level at least for checked contents`);
				return;
			}
			//------------

			//}
		}
		//return;
		//----------------------------------------------
		//source_org_id
		//destination_org_id
		//-------
		//collaboration_level_id
		//content_type_id
		//----------------------------------------------
		// const name = values[0].value;
		// const desc = values[1].value;
		//------------------------------------

		const url =
			Services.collaboration.backend + (agreement_id ? `api/collaboration-agreement/` + agreement_id : `api/collaboration-agreement`);
		const payload = {
			source_org_id: source_org_id,
			destination_org_id: destination_org_id,
			id: agreement_id ?? null,
			details: details,
		}; //{"name" : name,"description" :desc,"order" :records.length+1
		//  };
		//Helper.cl(payload,"playload");
		let response1;
		if (agreement_id) response1 = await put(url, payload);
		else response1 = await post(url, payload);

		if (response1 && response1.data && response1.data.status == 1) {
			updateClients();
			setOpenAdd(false);
		} else {
			toast.error(tr`Something went wrong while writing the agreement`);
		}
		//-------------------------------
	};

	const constructForm = (ag, orgs, lvls, contents) => {
		//Helper.cl("constructForm2");
		//Helper.cl(lvls,"constructForm2 lvls");
		Helper.cl(ag, "constructForm2 ag");
		let form2 = [];
		const def = orgs.find((o) => o.id == ag?.destination?.id);
		//Helper.cl(def,"def");
		const destControl = {
			col: 12,
			beginning: true,
			type: "select",
			label: tr`with`,
			name: "with",
			defaultValue: def ? { label: def.name, value: def.id } : null,
			option: orgs?.map((e) => {
				return { label: e.name, value: e.id };
			}),
			onChange: changeDestinationOrganization,
		};

		form2.push(destControl);
		//Helper.cl(contents,"contents");
		contents?.map((c) => {
			//------------------search for existence in details
			const matches = ag?.contents?.filter((d) => d.id == c.id);
			// Helper.cl(matches,c.id+"matches");//.length
			// Helper.cl(ag.contents,c.id+"ag.details");//.length
			//
			form2.push({
				beginning: true,
				col: 5,
				type: "checkbox",
				checked: matches && matches.length,
				label: c.name,
				name: c.id + "_" + c.name,
				onChange: (e) => {
					changeContentType(e, c.id);
				},
			});
			// form2.push({
			//   col:5,
			//   type: "select",
			//   label: tr`level` ,
			//   name: c.id+"_"+"level" ,
			//   option: lvls?.map((e) => {
			//     return { label: e.name, value: e.id };
			//   }),
			//   isDisabled:!(matches&&matches.length),
			//   onChange:(e)=>{ changeLevel(e,c.id)},
			// });
			form2.push({
				col: 5,
				type: "multicheckbox_dropdown",
				label: tr`level`,
				name: c.id + "_" + "level",
				options: lvls?.map((e) => {
					// Helper.cl(e.id,"al level");
					// if(matches?.[0]?.levels)
					//   Helper.cl(matches?.[0]?.levels,"al matches?.[0]?.levels");
					return { label: e.name, value: e.id, checked: matches?.[0]?.levels?.filter((l) => l.id == e.id).length };
				}),
				isDisabled: !(matches && matches.length),
				onChange: (e) => {
					changeLevels(e, c.id);
				},
			});
			// RMulticheckboxDropdown
			return;
		});
		//   agreement.details.map((d)=>{

		//Helper.cl(form2,"form2")
		return form2;

		//   });
	};
	useEffect(() => {
		//Helper.cl("constructForm");

		setForm1(constructForm(agreement, organizations, levels, contents));
	}, [agreement, agreement?.details, organizations, levels, contents]);

	const resetForm = () => {
		dispatch({
			type: "SET_FORM",
			payload: [
				{
					col: 12,
					beginning: true,
					type: "select",
					label: tr`with`,
					name: "with",
					option: organizations?.map((e) => {
						return { label: e.name, value: e.id };
					}),
					onChange: changeDestinationOrganization,
				},
			],
		});
	};
	// return <div></div>
	return (
		<div>
			{/* //{Helper.js(organizations,"organizations")}  */}
			{/* {Helper.js(agreement,"agreement")}  */}
			<RFormModal
				title={(agreement_id ? "Edit" : "Add") + " Collaboration Agreement"}
				open={openAdd}
				setOpen={setOpenAdd}
				actionButtonText={agreement_id ? "Save" : "Create"}
				initialformItems={form1}
				onFormSubmit={(e) => {
					addSubmit(e.target);
				}}
			/>
			 
		</div>
	);
};
