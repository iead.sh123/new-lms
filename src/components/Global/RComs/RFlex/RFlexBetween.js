const RFlexBetween = ({ children, ...props }) => (
  <section
    style={{
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      width: "100%",
    }}
    {...props}
  >
    {children}
  </section>
);

export default RFlexBetween;
