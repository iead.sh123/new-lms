import { lessonAssignmentsExamsApi } from "api/teacher/lessonAssignmentsExams";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import {
	GET_QUESTION_SET_STUDENT_AND_PATTERNS,
	GET_QUESTION_SET_SUGGESTED_PERIODS,
	GET_QUESTION_SET_lESSON_CLOSED_REQUEST,
	GET_QUESTION_SET_lESSON_CLOSED_SUCCESS,
	GET_QUESTION_SET_lESSON_CLOSED_ERROR,
	SHOW_REPORT_GRID_VIEW_SUCCESS,
	GET_REPORT_BY_ID_SUCCESS,
} from "./teacherTypes";
import { toast } from "react-toastify";

export const fetchStudentsAndPatterns = (rabId, questionSetId) => async (dispatch) => {
	try {
		const response = await lessonAssignmentsExamsApi.getStudentsAndPatterns(rabId, questionSetId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_QUESTION_SET_STUDENT_AND_PATTERNS,
				payload: response.data.data,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const fetchSuggestedPeriods = (rabId, lessonId) => async (dispatch) => {
	try {
		const response = await lessonAssignmentsExamsApi.getSuggestedPeriods(rabId, lessonId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_QUESTION_SET_SUGGESTED_PERIODS,
				payload: response.data.data.objList,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const InsertLink = (linkContent, history) => async () => {
	const response = await lessonAssignmentsExamsApi.insertLink(linkContent);
	if (response.data.status === 1) {
		history.goBack();
	} else {
		toast.error(response.data.msg);
	}
};

export const fetchQuestionSetClosed = (lessonId) => async (dispatch) => {
	dispatch({ type: GET_QUESTION_SET_lESSON_CLOSED_REQUEST });
	try {
		const response = await lessonAssignmentsExamsApi.getQuestionSetClosed(lessonId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_QUESTION_SET_lESSON_CLOSED_SUCCESS,
				payload: response.data.data.objList,
			});
		} else {
			toast.error(response.data.msg);

			dispatch({
				type: GET_QUESTION_SET_lESSON_CLOSED_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_QUESTION_SET_lESSON_CLOSED_ERROR,
		});
	}
};

export const SendNotification = (questionSetId, lessonId) => async () => {
	try {
		const response = await lessonAssignmentsExamsApi.sendNotificationAssignmentExamsClosed(questionSetId, lessonId);
		if (response.data.status === 1) {
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const UpdateQuestionSet = (lessonId, questionSetId) => async () => {
	try {
		const response = await lessonAssignmentsExamsApi.updateQuestionSetFromUnlinkToLink(lessonId, questionSetId);
		if (response.data.status === 1) {
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const GetReportById = (reportId) => async (dispatch) => {
	try {
		const response = await lessonAssignmentsExamsApi.getReportById(reportId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_REPORT_BY_ID_SUCCESS,
				payload: response.data.data.report,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const ShowReportGridView = (reportId, questionSetId, rabId) => async (dispatch) => {
	try {
		const response = await lessonAssignmentsExamsApi.showReportGridView(reportId, questionSetId, rabId);
		if (response.data.status === 1) {
			dispatch({
				type: SHOW_REPORT_GRID_VIEW_SUCCESS,
				payload: response.data,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};
