import React, { useEffect, useRef, useState } from "react";
import { shuffleArray, getRandomColor } from "../../../../../utils/helper";
import { TYPES } from "../../assignmentReducer";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import { translate } from "../../../../../utils/translate";

const MatchQuestion = ({
  answers,
  choices,
  question,
  groupId,
  dispatch,
  assignmentReadMode,
  mode,
  student,
}) => {
  const [selectedChoices, setSelectedChoices] = useState([]);
  const [choicesColor, setChoicesColor] = useState([]);
  const [disableLeftColumn, setDisableLeftColumn] = useState(false);

  const [disableRightColumn, setDisableRightColumn] = useState(true);
  const choicesColorRef = useRef([]);
  const choicesLeftRef = useRef([]);
  const choicesRightRef = useRef([]);

  useEffect(() => {
    choices.map((choice, index) => {
      if (index % 2 === 0) {
        choicesLeftRef.current.push(choice);
      } else {
        choicesRightRef.current.push(choice);
      }
    });
    if (assignmentReadMode && mode !== "multi") {
      if (question.notSolved === 1) {
        setSelectedChoices([]);
        return;
      }
      setSelectedChoices(question.student_choices.map((choice) => choice.id));
      question.student_choices.map((choice, index) => {
        if (index % 2 === 0) {
          choicesColorRef.current.push({
            id: choice.id,
            color: getRandomColor(),
          });
        } else {
          choicesColorRef.current.push({
            id: choice.id,
            color: choicesColorRef.current[index - 1].color || "",
          });
        }
      });

      return;
    }
    if (mode === "multi") {
      if (student.notSolved === 1 || student.notSolved === null) {
        setSelectedChoices([]);
        return;
      }
      setSelectedChoices(student.student_choices.map((choice) => choice.id));
      student.student_choices.map((choice, index) => {
        if (index % 2 === 0) {
          choicesColorRef.current.push({
            id: choice.id,
            color: getRandomColor(),
          });
        } else {
          choicesColorRef.current.push({
            id: choice.id,
            color: choicesColorRef.current[index - 1].color || "",
          });
        }
      });

      return;
    }
    if (answers && answers.length > 0) {
      let answer = answers.filter(
        (answer) => answer.QuestionId === question.id
      )[0];
      if (answer !== undefined) {
        setSelectedChoices([...answer.Choices]);
        setChoicesColor([...answer.ChoicesColor]);
      }
    }

    // choicesRightRef.current = shuffleArray(choicesRightRef.current);
  }, []);

  useEffect(() => {
    if (assignmentReadMode) return;
    dispatch({
      type: TYPES.SAVE_ANSWER,
      payload: {
        QuestionId: question.id,
        GroupId: groupId,
        Choices: selectedChoices,
        ChoicesColor: choicesColor,
        FreeAnswer: null,
      },
    });

    if (selectedChoices.length === 0) {
      setDisableRightColumn(true);
      setDisableLeftColumn(false);
      return;
    }
    if (selectedChoices.length % 2 === 0) {
      setDisableRightColumn(true);
      setDisableLeftColumn(false);
      return;
    }
    if (selectedChoices.length % 2 !== 0) {
      setDisableRightColumn(false);
      setDisableLeftColumn(true);
      return;
    }
  }, [selectedChoices]);

  const leftColumnAnswersHandler = (e) => {
    const id = +e.target.id;
    const isChoiceExist = selectedChoices.findIndex((choice) => choice === id);

    if (isChoiceExist !== -1) {
      setSelectedChoices([
        ...selectedChoices.slice(0, isChoiceExist),
        ...selectedChoices.slice(isChoiceExist + 2),
      ]);
      const isColorExist = choicesColor.findIndex((choice) => choice.id === id);
      if (isChoiceExist !== -1) {
        setChoicesColor([
          ...choicesColor.slice(0, isColorExist),
          ...choicesColor.slice(isColorExist + 2),
        ]);
      }

      setDisableRightColumn(true);
      setDisableLeftColumn(false);
      return;
    }
    setChoicesColor([...choicesColor, { id: id, color: getRandomColor() }]);
    setSelectedChoices([...selectedChoices, id]);

    setDisableRightColumn(false);
    setDisableLeftColumn(true);
  };
  const rightColumnAnswersHandler = (e) => {
    const id = +e.target.id;
    const isChoiceExist = selectedChoices.findIndex((choice) => choice === id);

    if (isChoiceExist !== -1) {
      setSelectedChoices([
        ...selectedChoices.slice(0, isChoiceExist - 1),
        ...selectedChoices.slice(isChoiceExist + 1),
      ]);
      const isColorExist = choicesColor.findIndex((choice) => choice.id === id);
      if (isChoiceExist !== -1) {
        setChoicesColor([
          ...choicesColor.slice(0, isColorExist - 1),
          ...choicesColor.slice(isColorExist + 1),
        ]);
      }

      setDisableRightColumn(false);
      setDisableLeftColumn(true);
      return;
    }
    setChoicesColor([
      ...choicesColor,
      { id: id, color: choicesColor.pop().color },
    ]);
    setSelectedChoices([...selectedChoices, id]);

    setDisableRightColumn(!disableRightColumn);
    setDisableLeftColumn(!disableLeftColumn);
  };

  const chooseBackgroundColor = (choiceId) => {
    let chosenColor;
    if (assignmentReadMode) {
      chosenColor = choicesColorRef.current.filter(
        (choiceColor) => choiceColor.id === choiceId
      )[0];
    } else {
      chosenColor = choicesColor.filter(
        (choiceColor) => choiceColor.id === choiceId
      )[0];
    }
    return chosenColor ? chosenColor.color : "";
  };
  return (
    <div className="col-md-12">
      <div className="row">
        <div className="col-md-6">
          {choicesLeftRef.current.map((leftChoice) => {
            return (
              <div key={leftChoice.id} className="form-check mb-3">
                <label
                  className="form-check-label assignment_match_label"
                  style={{
                    backgroundColor: chooseBackgroundColor(leftChoice.id),
                  }}
                >
                  <input
                    className="form-check-input assignment_checkbox"
                    id={leftChoice.id}
                    disabled={assignmentReadMode ? true : disableLeftColumn}
                    checked={selectedChoices.includes(leftChoice.id)}
                    onChange={(e) => leftColumnAnswersHandler(e, leftChoice.id)}
                    type="checkbox"
                    readOnly={assignmentReadMode}
                  />
                  {leftChoice.choice && leftChoice.choice.includes("<math") ? (
                    <MathJax math={leftChoice.choice} />
                  ) : (
                    <div
                      dangerouslySetInnerHTML={{ __html: leftChoice.choice }}
                    />
                  )}
                </label>

                {leftChoice.attachments && (
                  <div className="col-sm-10">
                    {["png", "jpeg"].includes(
                      leftChoice.attachments.mime_type
                    ) ? (
                      <ImageEnlarge
                        imageStyle={{
                          outline: `${chooseBackgroundColor(
                            leftChoice.id
                          )} solid 10px`,
                        }}
                        imageSrc={resourcesBaseUrl + leftChoice.attachments.url}
                      />
                    ) : (
                      <a
                        href={resourcesBaseUrl + leftChoice.attachments.url}
                        alt=""
                        class="btn btn-primary"
                      >
                        {translate("Download")}
                      </a>
                    )}
                  </div>
                )}
              </div>
            );
          })}
        </div>

        <div className="col-md-6">
          {choicesRightRef.current.map((rightChoice) => {
            return (
              <div key={rightChoice.id} className="form-check mb-3">
                <label
                  className="form-check-label assignment_match_label"
                  style={{
                    backgroundColor: chooseBackgroundColor(rightChoice.id),
                  }}
                >
                  <input
                    className="form-check-input assignment_checkbox"
                    id={rightChoice.id}
                    disabled={assignmentReadMode ? true : disableRightColumn}
                    checked={selectedChoices.includes(rightChoice.id)}
                    onChange={(e) =>
                      rightColumnAnswersHandler(e, rightChoice.id)
                    }
                    type="checkbox"
                  />
                  {rightChoice.choice &&
                  rightChoice.choice.includes("<math") ? (
                    <MathJax math={rightChoice.choice} />
                  ) : (
                    <div
                      dangerouslySetInnerHTML={{ __html: rightChoice.choice }}
                    />
                  )}
                </label>

                {rightChoice.attachments && (
                  <div className="col-sm-10">
                    {["png", "jpeg"].includes(
                      rightChoice.attachments.mime_type
                    ) ? (
                      <ImageEnlarge
                        imageStyle={{
                          outline: `${chooseBackgroundColor(
                            rightChoice.id
                          )} solid 10px`,
                        }}
                        imageSrc={
                          resourcesBaseUrl + rightChoice.attachments.url
                        }
                      />
                    ) : (
                      <a
                        href={resourcesBaseUrl + rightChoice.attachments.url}
                        alt=""
                        class="btn btn-primary"
                      >
                        {translate("Download")}
                      </a>
                    )}
                  </div>
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default MatchQuestion;
