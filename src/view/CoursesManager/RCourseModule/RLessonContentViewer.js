import React from "react";
import { Row, Col } from "reactstrap";
import { linkColor } from "config/constants";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";
import LiveSession from "./ListLessonContent/LessonComponents/LiveSession/LiveSession";
import LessonPlan from "./ListLessonContent/LessonComponents/LessonPlan";
import styles from "../CoursesManager.module.scss";
import Essay from "./ListLessonContent/LessonComponents/Essay";
import Link from "./ListLessonContent/LessonComponents/Link";
import File from "./ListLessonContent/LessonComponents/File";
import tr from "components/Global/RComs/RTranslator";
import { RLabel } from "components/Global/RComs/RLabel";

const RLessonContentViewer = ({ lessonContent }) => {
	return (
		<Col xs={12} key={lessonContent.id} className={`${styles.lesson_content_viewer}`}>
			<Row>
				<Col xs={9}>
					{lessonContent?.type?.toLowerCase() == "text" ? (
						<i className="fa fa-file-text fa-lg pt-1" aria-hidden="true" />
					) : lessonContent?.type?.toLowerCase() == "link" ? (
						<i className="fa fa-link fa-lg pt-1" aria-hidden="true" style={{ color: linkColor }} />
					) : lessonContent?.type?.toLowerCase() == "attachment" ? (
						<>
							<RImageViewer
								mimeType={lessonContent?.mime_type}
								url={lessonContent?.attachment?.url}
								imageName={lessonContent?.attachment?.file_name}
								width={"22px"}
								height={"22px"}
							/>
						</>
					) : lessonContent?.type?.toLowerCase() == "live_session" ? (
						<i className="fa fa-headphones fa-lg pt-1" aria-hidden="true" />
					) : lessonContent?.type?.toLowerCase() == "lesson_plan" ? (
						<>
							<RLabel value={tr("Not visible for students")} icon="fa fa-eye-slash fa-lg pt-1" lettersToShow={15} />
						</>
					) : (
						""
					)}

					<>
						{lessonContent.type == "link" ? (
							<a
								className="ml-2"
								href={`${lessonContent?.link?.link_url}`}
								target="_blank"
								style={{ textDecoration: "underline", color: linkColor }}
							>
								{tr(lessonContent.title)}
							</a>
						) : (
							<span
								className="ml-2"
								style={{
									cursor: lessonContent.type == "attachment" ? "default" : "",
								}}
							>
								{" "}
								{tr(lessonContent.title)}
							</span>
						)}
					</>
				</Col>

				<Col xs={12}>
					{lessonContent?.type == "text" && <Essay viewMode={true} lessonContent={lessonContent} content={null} />}
					{lessonContent?.type == "link" && <Link viewMode={true} lessonContent={lessonContent} content={null} />}
					{lessonContent?.type == "attachment" && <File viewMode={true} lessonContent={lessonContent} content={null} />}
					{lessonContent?.type == "live_session" && (
						<LiveSession viewMode={true} lessonContent={lessonContent} content={null} studentView={true} />
					)}
					{lessonContent?.type == "lesson_plan" && <LessonPlan viewMode={true} lessonContent={lessonContent} content={null} />}
				</Col>
			</Row>
		</Col>
	);
};

export default RLessonContentViewer;
