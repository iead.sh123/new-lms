import React, { useState, useRef } from "react";
import { primaryColor, dangerColor } from "config/constants";
import { Form, FormGroup, Input } from "reactstrap";
import { Collapse } from "reactstrap";
import GAddCategory from "logic/GCoursesManager/GAddCategory";
import styles from "./CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import iconsFa6 from "variables/iconsFa6";

const RCoursesTree = ({
	parentId,
	courseCategories,
	level,
	handleAddCategoryToTree,
	handleRemoveCategoryFromTree,
	addEditCategoryToTreeLoading,
	formGroup,
	handleChange,
	editable = true,
	courseOnClick,
	categoryOnClick,
}) => {
	const history = useHistory();
	const { categoryId } = useParams();
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [isHovered, setIsHovered] = useState(null);
	const [isEdited, setIsEdited] = useState(null);
	const [isAdded, setIsAdded] = useState(null);
	const inputRef = useRef(null);

	const collapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const actionsOnHover = (id) => {
		if (!editable) return;
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const editCategory = (id) => {
		const isEdit = "edit" + id;
		if (isEdit == isEdited) {
			setIsEdited("edit");
		} else {
			setIsEdited("edit" + id);
		}
	};

	const addCategory = (id) => {
		const isAdd = "add" + id;
		if (isAdd == isAdded) {
			setIsAdded("add");
		} else {
			setIsAdded("add" + id);
		}
	};

	const handleFocusOnInput = () => {
		// Focus the input field when the button is clicked

		if (inputRef.current !== null) {
			inputRef.current && inputRef.current.focus();
		}
	};

	return (
		<React.Fragment>
			{courseCategories &&
				courseCategories.map((courseCategory) => {
					return (
						<>
							{courseCategory.id ? (
								<div
									key={courseCategory.id}
									onMouseEnter={() => actionsOnHover(courseCategory.id + level)}
									onMouseLeave={() => actionsOnHover(courseCategory.id + level)}
								>
									<RFlex className={level == 1 ? `mb-2` : styles.category_name_border_vertical}>
										{/* Start another collapse style after level 1 */}
										{level !== 1 && (
											<i
												onClick={() => collapsesToggle(courseCategory.id + level)}
												className={`${
													openedCollapses === "collapse" + (courseCategory.id + level) ? iconsFa6.minusSolid : iconsFa6.plusSquare
												} ${styles.category_another_level_icon}`}
												aria-hidden="true"
												style={{ color: primaryColor }}
											/>
										)}
										{/* End another collapse style after level 1 */}
										{/* Start Edit Actions */}
										{isEdited == "edit" + (courseCategory.id + level) ? (
											<RFlex>
												{editable && (
													<Form>
														<FormGroup>
															<Input
																ref={inputRef}
																name="name"
																required
																type="text"
																defaultValue={courseCategory.name}
																onChange={(event) => handleChange(event.target.name, event.target.value)}
																onBlur={() => handleChange("parent_id", parentId)}
																placeholder={tr("category_name")}
															/>
														</FormGroup>
													</Form>
												)}

												{formGroup?.name == "" ? (
													<i className={`${styles.close_icon} ${iconsFa6.close}`} aria-hidden="true" onClick={() => setIsEdited(null)} />
												) : (
													<div>
														{editable && addEditCategoryToTreeLoading ? (
															<i aria-hidden="true" className={`${styles.close_icon} ${iconsFa6.spinner}`} />
														) : (
															editable && (
																<RFlex
																	styleProps={{
																		flexDirection: "column",
																		alignItems: "center",
																	}}
																>
																	<i className={`${styles.edit_icon} fa fa-check`} aria-hidden="true" onClick={handleAddCategoryToTree} />
																	<i className={`${styles.edit_icon} fa fa-close`} aria-hidden="true" onClick={() => setIsEdited(null)} />
																</RFlex>
															)
														)}
													</div>
												)}
											</RFlex>
										) : (
											<p
												className="m-0 cursor-pointer"
												onClick={() => {
													if (categoryOnClick) categoryOnClick(courseCategory.id);
													else history.push(`${baseURL}/${genericPath}/courses-manager/category/${courseCategory.id}`);
												}}
												style={{
													color: courseCategory.id == categoryId ? primaryColor : "black",
													fontWeight: courseCategory.id == categoryId ? "bold" : "normal",
												}}
											>
												{courseCategory?.name?.length > 25 ? courseCategory.name?.substring(0, 25) : courseCategory.name}
											</p>
										)}
										{/* End Edit Actions */}
										{/* Start Collapse Icon first Level */}
										{level == 1 && !isEdited && (
											<React.Fragment>
												<i
													onClick={() => collapsesToggle(courseCategory.id + level)}
													className={`${
														openedCollapses === "collapse" + (courseCategory.id + level)
															? "fa-solid fa-chevron-down"
															: "fa-solid fa-chevron-right"
													} ${styles.category_icon}`}
													aria-hidden="true"
													style={{
														color: courseCategory.id == categoryId ? primaryColor : "black",
														// fontWeight:
														//   courseCategory.id == categoryId ? "bold" : "normal",
													}}
												></i>
											</React.Fragment>
										)}
										{/* End Collapse Icon first Level */}
										{/* Start Actions On Hover */}
										{isHovered === "hover" + (courseCategory.id + level) && !isEdited && (
											<>
												<i
													className={`fa fa-edit ${styles.category_icon}`}
													aria-hidden="true"
													style={{ color: primaryColor }}
													onClick={() => {
														editCategory(courseCategory.id + level);
														handleChange("id", courseCategory.id);
														handleFocusOnInput();
													}}
												/>
												<i
													className={`fa fa-plus ${styles.category_icon}`}
													aria-hidden="true"
													style={{ color: primaryColor }}
													onClick={() => {
														addCategory(courseCategory.id + level);
														openedCollapses === "collapse" + (courseCategory.id + level)
															? null
															: collapsesToggle(courseCategory.id + level);
													}}
												/>

												<i
													className={iconsFa6.delete + ` ${styles.category_icon}`}
													aria-hidden="true"
													style={{ color: dangerColor }}
													onClick={() => handleRemoveCategoryFromTree(courseCategory.id, courseCategory.can_delete)}
												/>
											</>
										)}
										{/* End Actions On Hover */}
									</RFlex>

									<Collapse isOpen={openedCollapses === "collapse" + (courseCategory.id + level)} className={level == 1 ? "pl-2" : ""}>
										{courseCategory.children && courseCategory.children?.length > 0 && (
											<RCoursesTree
												parentId={courseCategory.id}
												courseCategories={courseCategory.children}
												level={level + 1}
												handleAddCategoryToTree={handleAddCategoryToTree}
												handleRemoveCategoryFromTree={handleRemoveCategoryFromTree}
												addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
												formGroup={formGroup}
												handleChange={handleChange}
												editable={editable}
												courseOnClick={courseOnClick}
												categoryOnClick={categoryOnClick}
											/>
										)}

										{/* Start Courses */}
										<div className="mb-2 ">
											{courseCategory?.courses &&
												courseCategory?.courses.map((course) => (
													<RFlex styleProps={{ flexWrap: "wrap", width: "100%" }}>
														<p
															key={course.id}
															className={`${styles.category_name_border_vertical} pl-4 m-0`}
															style={{
																width: "100%",
																overflowWrap: "break-word",
																padding: "5px",
															}}
															onClick={() => {
																if (courseOnClick) courseOnClick(course.id);
															}}
														>
															{course.name}
														</p>
													</RFlex>
												))}
										</div>
										{/* End Courses */}

										{courseCategory.children &&
											courseCategory.courses &&
											courseCategory.children?.length == 0 &&
											courseCategory.courses?.length == 0 && (
												<React.Fragment>
													<p className={`text-muted m-0 ${styles.category_empty}`}>💡 {tr`this category is empty`}</p>
													<p className={`text-muted  ${styles.category_empty}`}>{tr`you can create sub category or new course`}</p>
												</React.Fragment>
											)}

										{isAdded == "add" + (courseCategory.id + level) && level !== 0 && (
											<GAddCategory
												handleCloseAddCategory={() => setIsAdded(false)}
												handleAddCategoryToTree={handleAddCategoryToTree}
												parentId={courseCategory.id}
												addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
												handleChange={handleChange}
											/>
										)}
									</Collapse>
								</div>
							) : (
								<></>
							)}
						</>
					);
				})}
		</React.Fragment>
	);
};

export default RCoursesTree;
