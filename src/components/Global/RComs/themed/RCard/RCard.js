import React from 'react';
import withTheme from 'hocs/withTheme';

const RCard = theme => {

    let themedComponent = React.lazy(()=>
        (async ()=>{
            try{
                return await import(`./RCard_${theme}`);
            }
            catch(err){
                return await import(`./RCard_default`);;
            }
        })()
    )
    return themedComponent;
}



export default withTheme(RCard);