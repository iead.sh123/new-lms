
import {
    CHANGE_STUDENT_ATTENDANCE,
    ADD_NEW_SETTING_TYPE
} from '../../actions/global/globalTypes'

const initState = {
    students: [],
    settings: []
}



const attendanceReducer = (state = initState, { type, payload }) => {
    const newState = { ...state };
    switch (type) {
        case CHANGE_STUDENT_ATTENDANCE:
            const index = payload.studentIndex
            const attendanceType = payload.attendanceType

            const newStudents = newState.students
            if (!newStudents[index]) {
                newStudents[index] = {};
            }

            Object.keys(newStudents[index]).forEach((otherType) => {
                newStudents[index][otherType] = false;
            });

            newStudents[index][attendanceType] = true;
            newState.students = newStudents
            return newState;

        case ADD_NEW_SETTING_TYPE:
            return {...state,settigns:state.settings.push({details:payload.data})}

        default:
            return state
    }
}


export default attendanceReducer