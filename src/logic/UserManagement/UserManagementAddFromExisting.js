import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { get } from "config/api";
import { useParams } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import AppModal from "components/Global/ModalCustomize/AppModal";
import UserManagementAdd from "./UserManagementAdd";
import styles from "./UserManagement.module.scss";
import { post } from "config/api";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { primaryColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const UserManagementAddFromExisting = ({ handleCloseAddFromExisting, getData, setData, payloadData, setPayloadData }) => {
	const { organizationUserId, tabTitle } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [addExtraPropsToUser, setAddExtraPropsToUser] = useState(false);
	const [userId, setUserId] = useState(null);

	const handleOpenAddExtraPropsToUser = (data) => {
		setAddExtraPropsToUser(true);
		setUserId(data.id);
	};
	const handleCloseAddExtraPropsToUser = () => setAddExtraPropsToUser(false);

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/user/store/exists/${tabTitle}`;
		let response = await get(specific_url ? specific_url : url);

		if (response && response.data && response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.users?.data?.map((rc) => ({
			id: rc.id,
			tableName: "UserManagementAddFromExisting",
			details: [{ key: tr`name`, value: rc.name }],
			actions: [
				{
					name: "add",
					icon: payloadData.some((key) => key.user_id == rc.id) ? iconsFa6.delete : "fa fa-plus",
					outline: true,
					color: payloadData.some((key) => key.user_id == rc.id) ? "danger" : "primary",
					onClick: () => {
						if (payloadData.some((key) => key.user_id == rc.id)) {
							setUserId(data.id);
							handleAddExtraProps();
						} else {
							handleOpenAddExtraPropsToUser(rc);
						}
					},
				},
			],
		}));
		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.users,
			},
		}),
		[processedRecords, payloadData]
	);

	const handleAddExtraProps = (rec) => {
		setPayloadData((prevState) => {
			// Check if the object with the same user_id already exists in the payloadData array
			const exists = prevState.some((user) => user.user_id === userId);

			if (exists) {
				// Remove the object from the payloadData array
				const updatedUserIds = prevState.filter((user) => user.user_id !== userId);

				// Return the updated state
				return updatedUserIds;
			} else {
				handleCloseAddExtraPropsToUser();
				// Add the new object to the payloadData array
				const updatedUserIds = [...prevState, { ...rec, user_id: userId }];

				// Return the updated state
				return updatedUserIds;
			}
		});
	};

	return (
		<React.Fragment>
			<AppModal
				show={addExtraPropsToUser}
				parentHandleClose={handleCloseAddExtraPropsToUser}
				header={tr`add_extra_props`}
				size={"lg"}
				headerSort={
					<UserManagementAdd
						handleCloseModal={handleCloseAddExtraPropsToUser}
						getDataFromBackend={getDataFromBackend}
						setTableData={setTableData}
						addExtraPropsToUser={true}
						fetchDataAfterAdded={true}
						handleAddExtraProps={handleAddExtraProps}
					/>
				}
			/>
			<RAdvancedLister {...propsLiterals.listerProps} />
		</React.Fragment>
	);
};

export default UserManagementAddFromExisting;
