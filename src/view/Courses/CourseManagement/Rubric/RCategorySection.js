import React, { useContext } from "react";
import { Input, FormGroup, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { primaryColor } from "config/constants";
import { dangerColor } from "config/constants";
import { getIn } from "formik";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const RCategorySection = ({ arrayHelpers, category, categoryIndex, isOpen, CategoryId }) => {
	const RubricData = useContext(RubricContext);

	return (
		<section className={styles.r__collapse}>
			<RFlex styleProps={{ alignItems: "center" }}>
				{RubricData.selectedCategories.includes(CategoryId) ? (
					<FormGroup className="mb-0">
						<label className="pr-4 pl-0">{tr("title")}: </label>

						<Input
							type="text"
							name={`categories.${categoryIndex}.${"subject"}`}
							value={RubricData.values.categories[categoryIndex]["subject"]}
							onChange={RubricData.handleChange}
							style={{
								width: "fit-content",
								display: "inline",
								padding: "0px 5px",
							}}
							className={
								(!!RubricData.touched.categories?.[categoryIndex]?.["subject"] &&
									!!RubricData.errors.categories?.[categoryIndex]?.["subject"]) ||
								(RubricData.touched.categories?.[categoryIndex]?.["subject"] && RubricData.errors.categories?.[categoryIndex]?.["subject"])
									? "input__error"
									: ""
							}
						/>
						{/* {getIn(RubricData.errors, `categories[categoryIndex]?.["subject"]`) && getIn(RubricData.touched, `categories[categoryIndex]?.["subject"]`)}
						{RubricData.touched.categories?.[categoryIndex]?.["subject"] && RubricData.errors.categories?.[categoryIndex]?.["subject"] && (
							<FormText color="danger">{RubricData.errors.categories?.[categoryIndex]?.["subject"]}</FormText>
						)} */}
					</FormGroup>
				) : (
					<RFlex styleProps={{ gap: 0, flexDirection: "column" }}>
						<span color="danger">
							{RubricData.values.categories[categoryIndex]["subject"]
								? RubricData.values.categories[categoryIndex]["subject"]
								: tr`title (for example: skills of reading)`}
						</span>

						<span>
							{getIn(RubricData.errors, `categories[categoryIndex]?.["subject"]`) &&
								getIn(RubricData.touched, `categories[categoryIndex]?.["subject"]`)}
							{RubricData.touched.categories?.[categoryIndex]?.["subject"] &&
								RubricData.errors.categories?.[categoryIndex]?.["subject"] && (
									<FormText color="danger">{RubricData.errors.categories?.[categoryIndex]?.["subject"]}</FormText>
								)}
						</span>
					</RFlex>
				)}

				{RubricData.isHovered == "hover" + CategoryId && !RubricData.selectedCategories.includes(CategoryId) ? (
					<div
						className={styles.r__icon}
						onClick={(event) => {
							event.stopPropagation();
							RubricData.handleSelectedCategories(CategoryId);
						}}
					>
						<i className={iconsFa6.pen}></i>
					</div>
				) : RubricData.selectedCategories.includes(CategoryId) ? (
					<div
						className={styles.r__icon}
						onClick={(event) => {
							event.stopPropagation();
							RubricData.handleRemoveCategoryFromSelectedCategory(CategoryId);
						}}
					>
						<i className={iconsFa6.solidCheck}></i>
					</div>
				) : (
					""
				)}
			</RFlex>
			<RFlex>
				{RubricData.isHovered == "hover" + CategoryId ? (
					<i
						className={iconsFa6.delete + " cursor-pointer"}
						style={{ color: dangerColor }}
						onClick={() => arrayHelpers.remove(categoryIndex)}
					/>
				) : (
					""
				)}
				<i
					className={`fa ${isOpen ? iconsFa6.chevronDown : iconsFa6.chevronRight} cursor-pointer`}
					style={{ color: primaryColor }}
					onClick={() => {
						RubricData.collapsesCategoryToggle(CategoryId);
					}}
				/>
			</RFlex>
		</section>
	);
};

export default RCategorySection;
