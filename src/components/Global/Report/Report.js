import { reportAPI } from "api/global/report";
import RForm from "components/Global/RComs/RForm";
import React from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import tr from "../RComs/RTranslator";
import { Button } from "reactstrap";
import { SUCCESS } from "utils/Alert";
import Loader from "../Loader";
import { toast } from "react-toastify";

const Report = ({}) => {
	const [reportText, setReportText] = React.useState("");
	const [reportCategory, setReportCategory] = React.useState(null);
	const [categories, setCategories] = React.useState([]);
	const [loading, setLoading] = React.useState(false);
	const history = useHistory();

	const { tableName, resourceId } = useParams();

	React.useEffect(() => {
		categories?.length && setReportCategory(categories[categories.length - 1]);
	}, [categories]);

	const handleAddReport = async (event) => {
		try {
			event.preventDefault();
			setLoading(true);
			const payload = {
				payload: {
					table_name: tableName,
					row_id: +resourceId,
					content_report_category_id: reportCategory.value,
					report_text: reportText,
				},
			};
			const response = await reportAPI.addReport(payload);
			if (!response.data.status) throw new Error(response.data.msg);
			history.goBack();
		} catch (err) {
			toast.error(err?.message);
		} finally {
			setLoading(false);
		}
	};

	const getReportCategories = async () => {
		try {
			setLoading(true);
			const response = await reportAPI.reportCategories();
			if (!response.data.status) throw new Error(response.data.msg);
			const cats = response.data?.data?.data;
			setCategories(
				cats.map((gd) => ({
					label: gd.type,
					value: gd.id,
				}))
			);
		} catch (err) {
			history.goBack();
			toast.error(err?.message);
		} finally {
			setLoading(false);
		}
	};

	React.useEffect(() => {
		getReportCategories();
	}, []);

	const formProps = React.useMemo(
		() => ({
			formItems: [
				{
					label: tr("category"),
					type: "select",
					name: "category",
					md: "12",
					//   required: true,
					option: categories,
					onChange: (item) => setReportCategory(item),
					value: [reportCategory],
					id: "categories",
				},
				{
					label: tr("report"),
					type: "textarea",
					name: "report",
					md: "12",
					//   required: true
					onChange: (e) => setReportText(e.target.value),
					value: reportText,
					id: "report",
				},
			],
			onSubmit: handleAddReport,
			withSubmit: loading ? false : true,
		}),
		[reportText, categories, reportCategory, loading]
	);

	return (
		<div className="content">
			<RForm {...formProps} />
			{loading ? <Loader /> : <Button onClick={() => history.goBack()}>Cancel</Button>}
		</div>
	);
};

export default Report;
