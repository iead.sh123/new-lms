import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	fileZip,
	powerPointIcon,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
	pdfIcon,
	zipIcon,
	defaultImage,
} from "config/mimeTypes";
import { Services } from "engine/services";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";

const getSourceForType = ({ fileObj, mimeType, fileManagement }) => {
	if (imageTypes.includes(mimeType)) {
		return fileManagement ? fileObj?.base64 : fileObj.url ? Services.storage.file + fileObj.url : defaultImage;
	} else if (fileTypes.includes(mimeType)) {
		return fileIcon;
	} else if (fileWord.includes(mimeType)) {
		return wordIcon;
	} else if (fileExcel.includes(mimeType)) {
		return excelIcon;
	} else if (filePowerPoint.includes(mimeType)) {
		return powerPointIcon;
	} else if (filePdf.includes(mimeType)) {
		return pdfIcon;
	} else if (fileVideo.includes(mimeType)) {
		return videoIcon;
	} else if (fileAudio.includes(mimeType)) {
		return audioIcon;
	} else if (fileZip.includes(mimeType)) {
		return zipIcon;
	} else {
		return fileObj.url ? Services.storage.file + fileObj.url : UserAvatar;
	}
};

export default getSourceForType;
