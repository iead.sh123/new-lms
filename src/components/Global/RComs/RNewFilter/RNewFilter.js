import React, { useState } from "react";
import { Nav, NavItem, NavLink, ButtonGroup, Button } from "reactstrap";
import { primaryColor } from "config/constants";
import classnames from "classnames";
import RButton from "../RButton";
import styles from "./RNewFilter.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { ADD_NEW_FILTER_ON_LISTER } from "store/actions/admin/adminType";

const RNewFilter = ({ getData, staticFilter, newFilterdispatch }) => {
  const [activeTab, setActiveTab] = useState(staticFilter[0].Title);

  const handleFilter = (filter) => {
    newFilterdispatch({
      type: ADD_NEW_FILTER_ON_LISTER,
      data: filter,
    });
  };

  return (
    <RFlex
      styleProps={{ justifyContent: "space-between", alignItems: "center" }}
    >
      {staticFilter && staticFilter.length > 0 && (
        <div className={`nav-tabs-navigation ${styles.nav_top}`}>
          <div className="nav-tabs-wrapper">
            <Nav tab>
              {staticFilter.map((tab, ind) => (
                <NavItem style={{ paddingBottom: "6px" }}>
                  <NavLink
                    className={classnames({
                      active: activeTab == tab.Title,
                    })}
                    onClick={() => {
                      setActiveTab(tab.Title);
                      handleFilter(`${tab.Title}=true`);
                    }}
                    style={{
                      color: activeTab === tab.Title ? primaryColor : "#333",
                      borderBottom:
                        activeTab === tab.Title && `1px solid ${primaryColor}`,
                      padding: "0px 20px",
                    }}
                  >
                    <RFlex>
                      {tr(tab.Title)}
                      {tab.Count && (
                        <div className={styles.count}>
                          <span style={{ fontSize: "12px" }}>{tab.Count}</span>
                        </div>
                      )}
                    </RFlex>
                  </NavLink>
                </NavItem>
              ))}
            </Nav>
          </div>
        </div>
      )}

      {/* <ButtonGroup>
        <RButton
          color="primary"
          outline
          //   onClick={() => setRSelected(1)}
          active={true}
          text="Recent"
          className={styles.border_radius}
        />
        <RButton
          color="primary"
          outline
          //   onClick={() => setRSelected(1)}
          active={false}
          text="Alphabetically"
          className={styles.border_radius}
        />
      </ButtonGroup> */}
    </RFlex>
  );
};

export default RNewFilter;
