import RButton from "components/Global/RComs/RButton";
import RModal from "components/Global/RComs/RModal";
import RSelect from "components/Global/RComs/RSelect";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Container } from "reactstrap";

const GFilterEvents = ({
  isOpen,
  toggleModal,
  eventTypes,
  handleSetFilter,
  value,
}) => {
  return (
    <RModal
      isOpen={isOpen}
      toggle={toggleModal}
      header={<h6>Filter</h6>}
      body={
        <Container>
          <RSelect
            option={Object.values(eventTypes.byId).map((t) => ({
              label: t.name,
              value: t.id,
            }))}
            placeholder={"Select Type"}
            isMulti
            value={value}
            //value={el?.field}
            onChange={(val) => handleSetFilter(val)}
          />
          <div className="d-flex justify-content-between">
            <RButton
              onClick={() => handleSetFilter([])}
              color="danger"
              text={tr`clear`}
              outline
            ></RButton>

            <RButton
              onClick={toggleModal}
              color="primary"
              text={tr`apply`}
            ></RButton>
          </div>
        </Container>
      }
      //  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
      size="lg"
    />
  );
};

export default GFilterEvents;
