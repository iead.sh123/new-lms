import React from 'react';
import withTheme from 'hocs/withTheme';
import withDirection from 'hocs/withDirection';

const RNotification = theme => {

    let themedComponent = React.lazy(() =>
        (async () => {
            try {
                return await import(`./Notification_${theme}`);
            }
            catch (err) {
                return await import(`./Notification_default`);;
            }
        })()
    )
    return themedComponent;
}



export default withDirection(withTheme(RNotification));