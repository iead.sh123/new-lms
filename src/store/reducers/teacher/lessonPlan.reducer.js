import {
	GET_LESSON_LESSONPLAN_SUCCESS,
	GET_LESSON_LESSONPLAN_REQUEST,
	GET_LESSON_LESSONPLAN_ERROR,
	GET_LESSON_PLAN_PRINTING_REQUEST,
	GET_LESSON_PLAN_PRINTING_SUCCESS,
	GET_LESSON_PLAN_PRINTING_ERROR,
	ADD_LESSON_LESSONPLAN_REQUEST,
	ADD_LESSON_LESSONPLAN_SUCCESS,
	ADD_LESSON_LESSONPLAN_ERROR,
	ADD_DATA_TO_LESSON_PLAN_ITEMS,
	ADD_NEW_LESSON_PLAN_ITEMS,
	ADD_VALIDATION_TO_LESSON_PLAN,
	SET_LESSON_PLAN_VALUE,
	REMOVE_LESSON_PLAN_ITEM,
	CLEAR_LESSON_PLAN_ITEM,
	GET_VALIDATION_LESSON_PLAN,
	PICK_A_RUBRIC_TO_LESSON_PLAN_ITEM,
	DELETE_RUBRIC_FROM_LESSON_PLAN_ITEM,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_REQUEST,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_SUCCESS,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_ERROR,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_REQUEST,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_SUCCESS,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_ERROR,
	EXPORT_LESSON_PLAN_AS_PDF_REQUEST,
	EXPORT_LESSON_PLAN_AS_PDF_SUCCESS,
	EXPORT_LESSON_PLAN_AS_PDF_ERROR,
} from "../../actions/teacher/teacherTypes";
import produce from "immer";

const InitState = {
	lessonPlan: null,
	lessonPlanErrors: {},
	tags: [],
	chooseItems: [],
	lessonPlanBase64: null,
	isDone: false,
	lessonPlanLoading: false,
	lessonPlanPrintLoading: false,
	saveLessonPlanLoading: false,
	importLessonPlanToLessonContentLoading: false,
	test: null,
	exportLessonPlanAsPdfLoading: false,
};

let ind;
let lessonPlanItemInd;
const lessonPlanReducer = (state = InitState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case GET_VALIDATION_LESSON_PLAN:
				draft.lessonPlanErrors = action.payload.initialValidationData;
				return draft;

			case GET_LESSON_LESSONPLAN_REQUEST:
				draft.isDone = false;
				draft.lessonPlanLoading = true;
				return draft;
			case GET_LESSON_LESSONPLAN_SUCCESS:
				draft.lessonPlanLoading = false;
				draft.lessonPlan = action.lessonPlan;
				draft.isEdit = action.lessonPlan !== null ? true : false;
				return draft;
			case GET_LESSON_LESSONPLAN_ERROR:
				draft.lessonPlanLoading = false;
				return draft;

			case GET_LESSON_PLAN_PRINTING_REQUEST:
				draft.lessonPlanPrintLoading = true;
				return draft;
			case GET_LESSON_PLAN_PRINTING_SUCCESS:
				draft.lessonPlanPrintLoading = false;
				draft.lessonPlanBase64 = action.payload.urlBase64;
				return draft;
			case GET_LESSON_PLAN_PRINTING_ERROR:
				draft.lessonPlanPrintLoading = false;
				return draft;

			case ADD_LESSON_LESSONPLAN_REQUEST:
				draft.saveLessonPlanLoading = true;
				return draft;
			case ADD_LESSON_LESSONPLAN_SUCCESS:
				draft.saveLessonPlanLoading = false;
				return draft;
			case ADD_LESSON_LESSONPLAN_ERROR:
				draft.saveLessonPlanLoading = false;
				return draft;

			case ADD_DATA_TO_LESSON_PLAN_ITEMS:
				ind = draft.lessonPlan.lesson_plan_items.findIndex((lessonPlanItem) =>
					lessonPlanItem?.id ? lessonPlanItem?.id == action.payload.id : lessonPlanItem?.idTemp == action.payload.id
				);
				if (ind !== -1) {
					draft.lessonPlan.lesson_plan_items[ind][action.payload.name] = action.payload.value;
				}
				return draft;

			case ADD_NEW_LESSON_PLAN_ITEMS:
				if (!action.payload.indexAdd) {
					const data = {
						idTemp: Math.random(10000),
						subject: "",
						time: "",
						description: "",
						attachments: [],
						tags: [],
						rubric: null,
						rubric_id: null,
					};
					draft.test = draft?.lessonPlanErrors;
					draft?.lessonPlan?.lesson_plan_items.unshift(data);
					draft?.lessonPlanErrors?.lesson_plan_items.unshift(data);
				} else {
					const data = {
						idTemp: Math.random(10000),
						subject: "",
						time: "",
						description: "",
						attachments: [],
						tags: [],
						rubric_id: null,
					};
					draft?.lessonPlan?.lesson_plan_items.splice(action.payload.indexAdd, 0, data);
					draft?.lessonPlanErrors?.lesson_plan_items.splice(action.payload.indexAdd, 0, data);
				}

				return draft;

			case SET_LESSON_PLAN_VALUE:
				draft.lessonPlan[action.key] = action.value;
				draft.isEdit = true;
				return draft;

			case ADD_VALIDATION_TO_LESSON_PLAN:
				if (action.payload.name == undefined) {
					draft.lessonPlanErrors = action.payload.message;
				} else if (action.payload.id) {
					ind = draft.lessonPlanErrors.lesson_plan_items.findIndex((lessonPlanItemError) =>
						lessonPlanItemError?.id ? lessonPlanItemError?.id == action.payload.id : lessonPlanItemError?.idTemp == action.payload.id
					);
					if (ind !== -1) {
						draft.lessonPlanErrors.lesson_plan_items[ind][action.payload.name] = action.payload.message;
					}
				} else {
					draft.lessonPlanErrors[action.payload.name] = action.payload.message;
				}
				return draft;

			case REMOVE_LESSON_PLAN_ITEM:
				ind = draft.lessonPlan.lesson_plan_items.findIndex((lessonPlanItem) =>
					lessonPlanItem?.id ? lessonPlanItem?.id == action.payload.id : lessonPlanItem?.idTemp == action.payload.id
				);

				lessonPlanItemInd = draft.lessonPlanErrors.lesson_plan_items.findIndex((lessonPlanItemError) =>
					lessonPlanItemError?.id ? lessonPlanItemError?.id == action.payload.id : lessonPlanItemError?.idTemp == action.payload.id
				);
				if (ind !== -1) {
					draft.lessonPlan.lesson_plan_items.splice(ind, 1);
				}
				if (lessonPlanItemInd !== -1) {
					draft.lessonPlanErrors.lesson_plan_items.splice(lessonPlanItemInd, 1);
				}

				return draft;

			case CLEAR_LESSON_PLAN_ITEM:
				ind = draft.lessonPlan.lesson_plan_items.findIndex((lessonPlanItem) =>
					lessonPlanItem?.id ? lessonPlanItem?.id == action.payload.id : lessonPlanItem?.idTemp == action.payload.id
				);
				if (ind !== -1) {
					const LPItem = draft.lessonPlan.lesson_plan_items[ind];
					LPItem.subject = "";
					LPItem.time = "";
					LPItem.description = "";
					LPItem.attachments = [];
					LPItem.tags = [];
					LPItem.rubric = {};
					LPItem.rubric_id = null;
				}
				return draft;

			case PICK_A_RUBRIC_TO_LESSON_PLAN_ITEM:
				ind = draft.lessonPlan.lesson_plan_items.findIndex((lessonPlanItem) =>
					lessonPlanItem?.id
						? lessonPlanItem?.id == action.payload.lessonPlanItemId
						: lessonPlanItem?.idTemp == action.payload.lessonPlanItemId
				);
				if (ind !== -1) {
					draft.lessonPlan.lesson_plan_items[ind].rubric = action.payload.data;
					draft.lessonPlan.lesson_plan_items[ind].rubric_id = action.payload.data.id;
				}
				return draft;

			case DELETE_RUBRIC_FROM_LESSON_PLAN_ITEM:
				ind = draft.lessonPlan.lesson_plan_items.findIndex((lessonPlanItem) =>
					lessonPlanItem?.id
						? lessonPlanItem?.id == action.payload.lessonPlanItemId
						: lessonPlanItem?.idTemp == action.payload.lessonPlanItemId
				);

				if (ind !== -1) {
					draft.lessonPlan.lesson_plan_items[ind].rubric = {};
					draft.lessonPlan.lesson_plan_items[ind].rubric_id = null;
				}
				return draft;

			case IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_REQUEST:
				draft.importLessonPlanToLessonContentLoading = true;
				return draft;
			case IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_SUCCESS:
				draft.importLessonPlanToLessonContentLoading = false;
				return draft;
			case IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_ERROR:
				draft.importLessonPlanToLessonContentLoading = false;
				return draft;

			case GET_LESSON_PLAN_BY_LESSON_PLAN_ID_REQUEST:
				draft.lessonPlanLoading = true;
				return draft;
			case GET_LESSON_PLAN_BY_LESSON_PLAN_ID_SUCCESS:
				draft.lessonPlanLoading = false;
				draft.lessonPlan = action.payload.data;
				return draft;
			case GET_LESSON_PLAN_BY_LESSON_PLAN_ID_ERROR:
				draft.lessonPlanLoading = false;
				return draft;

			case EXPORT_LESSON_PLAN_AS_PDF_REQUEST:
				draft.exportLessonPlanAsPdfLoading = true;
				return draft;
			case EXPORT_LESSON_PLAN_AS_PDF_SUCCESS:
				draft.exportLessonPlanAsPdfLoading = false;
				return draft;
			case EXPORT_LESSON_PLAN_AS_PDF_ERROR:
				draft.exportLessonPlanAsPdfLoading = false;
				return draft;

			default:
				return draft;
		}
	});
export default lessonPlanReducer;
