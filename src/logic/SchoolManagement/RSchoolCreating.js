import React from 'react'
import { SchoolContext } from './GSchoolCreating'
import RFileSuite from 'components/Global/RComs/RFile/RFileSuite';
import styles from './SchoolCreating.module.scss'
import { Input, FormGroup, FormText } from "reactstrap";
import { Services } from 'engine/services';
import { ErrorMessage } from 'formik';
const RSchoolCreating = ({ isModal = false }) => {
    const SchoolData = React.useContext(SchoolContext)
    const addressInputRef = React.useRef(null)
    React.useEffect(() => {
        if (addressInputRef) {
            addressInputRef.current.focus()
        }
        return () => { }
    }, [])
    console.log(SchoolData)
    return (
        <div className={`p-0 m-0 d-flex flex-column ${isModal ? "align-items-start" : "align-items-center"}`} style={{ gap: '10px' }}>
            <div>
                <label style={{ margin: '0px' }}>School Logo</label>
                <div>
                    <RFileSuite
                        parentCallback={(e) => { console.log("upload id", e); SchoolData?.setUploadedImage(e[0]) }}
                        roundWidth={{ width: "63px", height: '63px', type: "school" }}
                        singleFile={true}
                        fileType={["image/*"]}
                        removeButton={false}
                        value={[{ hash_id: SchoolData?.info?.image }]}
                        uploadName="upload"
                        showReplace={false}
                        showDelete={true}
                        showFileList={false}
                        showFileAdd={true}
                        setSpecificAttachment={() => { }}
                        theme="round"
                        binary={true}
                    ></RFileSuite>

                </div>
            </div>

            <div className={styles.InputsContainer}>
                <FormGroup >
                    <label className={styles.Label} htmlFor="address">School Address</label>
                    <Input
                        name="address"
                        type="text"
                        innerRef={addressInputRef}
                        placeholder='Address'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        onBlur={SchoolData.handleBlur}
                        value={SchoolData?.values?.address}
                        className={
                            (SchoolData?.touched.address && SchoolData?.errors?.address)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.address && SchoolData?.errors?.address && <FormText color="danger">{SchoolData?.errors?.address}</FormText>}
                </FormGroup>



            </div>

            <div className={styles.InputsContainer}>
                <FormGroup>
                    <label className={styles.Label} htmlFor="email">Contact Information</label>
                    <Input
                        name="email"
                        type="text"
                        placeholder='Contact Information'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        value={SchoolData?.values?.email}
                        className={
                            (!!SchoolData?.touched?.email && !!SchoolData?.errors?.email) || (SchoolData?.touched?.email && SchoolData?.errors?.email)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.email && SchoolData?.errors?.email && <FormText color="danger">{SchoolData?.errors?.email}</FormText>}

                </FormGroup>

            </div>

            <div className={styles.InputsContainer}>
                <FormGroup>
                    <label className={styles.Label} htmlFor="url">Facebook Page</label>
                    <Input
                        name="url"
                        type="text"
                        placeholder='Facebook Page'
                        style={{ width: '470px', height: '40px', border: '1px solid #DDDDDD', outline: 'none' }}
                        onChange={SchoolData?.handleChange}
                        value={SchoolData?.values?.url}
                        className={
                            (!!SchoolData?.touched?.url && !!SchoolData?.errors?.url) || (SchoolData?.touched?.url && SchoolData?.errors?.url)
                                ? "input__error"
                                : ""
                        }
                    />
                    {SchoolData?.touched?.url && SchoolData?.errors?.url && <FormText color="danger">{SchoolData?.errors?.url}</FormText>}
                </ FormGroup>
            </div>
        </div>
    )
}

export default RSchoolCreating