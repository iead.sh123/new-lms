import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { certificates } from "api/global/certificates";
import {
	GET_CERTIFICATES_FOR_SECTION_REQUEST,
	GET_CERTIFICATES_FOR_SECTION_SUCCESS,
	GET_CERTIFICATES_FOR_SECTION_ERROR,
} from "./globalTypes";
import { toast } from "react-toastify";

export const getCertificateForSection = (parent_type, parent_id) => async (dispatch) => {
	dispatch({ type: GET_CERTIFICATES_FOR_SECTION_REQUEST });
	try {
		const response = await certificates.getCertificatesForSection(parent_type, parent_id);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_CERTIFICATES_FOR_SECTION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_CERTIFICATES_FOR_SECTION_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_CERTIFICATES_FOR_SECTION_ERROR });
		toast.error(error?.message);
	}
};
