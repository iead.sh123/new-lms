import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import React from 'react'
import * as colors from 'config/constants'
import AppNewCheckbox from 'components/Global/RComs/AppNewCheckbox/AppNewCheckbox'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RLister from 'components/Global/RComs/RLister'
import { RLabel } from 'components/Global/RComs/RLabel'
const RReviewUserData = (
    {
        users,
        selectedUsers = [],
    }) => {

    const records = selectedUsers?.map((user) => {
        return {
            details: users?.headers?.map((header) => {

                const userValueComponent =
                    <RProfileName name={user.full_name} img={user.image}
                        onClick={() => { }}
                        flexStyleProps={{ cursor: "pointer" }}
                    />

                const activeComponent = <span className='p-0 m-0' style={{ color: user.is_active ? colors.successColor : colors.dangerColor }}>
                    {user.is_active ? tr("active") : tr("inactive")}
                </span>
                const userTypesComponent = <p className='p-0 m-0'>
                    {user.user_types.map((userType, index) => {
                        return (
                            <>
                                <RLabel
                                    value={`${userType.type}}`}
                                    lettersToShow={userType.type.length}
                                    showDots={false}
                                    tooltipText={userType.is_active ? "active" : "inactive"}
                                    otherStyle={{ color: userType.is_active ? "" : colors.lightGray }}
                                /> {index != user.user_types.length - 1 ? "," : ""}
                            </>
                        )
                    })}
                </p>
                const addressComponent = <RLabel
                    value={user.address}
                    lettersToShow={33}
                    tooltipText={user.address} />
                return {
                    key: header.label,
                    value: header.value == "user_types" ? userTypesComponent :
                        header.value == 'is_active' ? activeComponent :
                            header.value == 'address' ? addressComponent :
                                !user[header.value] ? "-" : user[header.value],
                    type: (header.value == "user_types" || header.value == "is_active" || header.value == 'address') ? "component" : "",
                    color: (header.value != 'full_name' && header.value != "user_types" && header.value != "is_active") ? colors.lightGray : ""
                }
            }),
            // actions: [
            //   { name: "", justIcon: true, icon: iconsFa6.delete, actionIconClass: 'text-danger' }
            // ]
        }
    })
    return (
        <RFlex styleProps={{ marginBottom: "100px" }}>
            {selectedUsers.length > 0 ?
                <RLister disableXScroll={true} Records={records} center={false} />
                :
                <RFlex className="flex-column">
                    <span style={{ fontWeight: 'bold' }}>{tr("Members")}</span>
                    <span>{tr("No_members_yet")}</span>
                </RFlex>
            }
        </RFlex>
    )
}

export default RReviewUserData