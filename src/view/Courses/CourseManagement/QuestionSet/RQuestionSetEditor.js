import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { Collapse, Form, Row, Col } from "reactstrap";
import { primaryColor } from "config/constants";
import MultipleChoiceSingleAnswer from "./QuestionsTypes/MultipleChoiceSingleAnswer";
import RQuestionSetInfo from "./RQuestionSetInfo";
import MultipleChoice from "./QuestionsTypes/MultipleChoice";
import RQuestionTypes from "./RQuestionTypes";
import QuestionsGroup from "./QuestionsGroup";
import RHideCollapse from "./RHideCollapse";
import FillInBlank from "./QuestionsTypes/FillInBlank";
import TrueOrFalse from "./QuestionsTypes/TrueOrFalse";
import UploadFile from "./QuestionsTypes/UploadFile";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import Ordering from "./QuestionsTypes/Ordering";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "./QuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Match from "./QuestionsTypes/Match";
import Essay from "./QuestionsTypes/Essay";
import Draw from "./QuestionsTypes/Draw";
import tr from "components/Global/RComs/RTranslator";

const RQuestionSetEditor = () => {
	const QuestionSetData = useContext(QuestionSetContext);

	const questionTypeComponents = {
		ESSAY: Essay,
		FILL_IN: FillInBlank,
		UPLOAD_FILE: UploadFile,
		MULTIPLE_CHOICES_MULTIPLE_ANSWERS: MultipleChoice,
		MULTIPLE_CHOICES_SINGLE_ANSWER: MultipleChoiceSingleAnswer,
		TRUEFALSE: TrueOrFalse,
		SEQUENCING: Ordering,
		DRAW: Draw,
		MATCH: Match,
	};

	return (
		<React.Fragment>
			<Form>
				{QuestionSetData.moduleId && (
					<RFlex className="pb-3">
						<RTextIcon
							icon={iconsFa6.arrowLeft}
							text={tr`back_to_creating_course`}
							flexStyle={{ color: primaryColor, cursor: "pointer" }}
							onClick={() => QuestionSetData.handleGoBack()}
						/>
					</RFlex>
				)}
				<RQuestionSetInfo />
				<RQuestionTypes questionSetIndex={null} />

				{QuestionSetData.questionSet.groups?.length > 0 &&
					QuestionSetData.questionSet.groups.map((group, ind) => {
						const isOpen = QuestionSetData.openedCollapses.includes(group?.id || group?.fakeId);

						return (
							<section>
								<section className={styles.section__group} key={group?.id ? group?.id : group?.fakeId}>
									<Collapse isOpen={isOpen}>
										<RHideCollapse
											group={group}
											groupId={group?.id ? group?.id : group?.fakeId}
											questionTitle={group?.title}
											questionPoint={group?.points}
										/>
									</Collapse>

									{!isOpen && (
										<>
											{group.isGroup ? (
												<QuestionsGroup group={group} questionGroupId={group?.id ? group?.id : group?.fakeId} questionNumber={ind + 1} />
											) : (
												group?.questions?.map((question, index) => {
													const QuestionComponent = questionTypeComponents[question.type];

													if (QuestionComponent) {
														return (
															<QuestionComponent
																questionGroupId={group?.id ? group?.id : group?.fakeId}
																question={question}
																questionNumber={ind + 1}
															/>
														);
													}

													return null;
												})
											)}
											<Row className="justify-content-end">
												<Col
													className="pl-4 d-flex cursor-pointer"
													style={{ gap: "10px" }}
													onClick={() => QuestionSetData.collapsesQuestionSetGroupToggle(group?.id ? group?.id : group?.fakeId)}
													xs={1.5}
												>
													<i className="fa-solid fa-chevron-up pt-1" style={{ color: primaryColor }} />
													<p>{tr`hide`}</p>
												</Col>
											</Row>
										</>
									)}
								</section>
								<RQuestionTypes questionSetIndex={ind + 1} />
							</section>
						);
					})}
				<RFlex>
					<RButton
						color={"primary"}
						text={tr`save`}
						onClick={QuestionSetData.handleCreateQuestionSet}
						loading={QuestionSetData.createQuestionSetLoading}
						disabled={QuestionSetData.createQuestionSetLoading}
					/>
					{/* <RButton color={"link"} text={tr`cancel`} className={styles.cancel__question__set} onClick={QuestionSetData.handleGoBack} /> */}
				</RFlex>
			</Form>
		</React.Fragment>
	);
};

export default RQuestionSetEditor;
