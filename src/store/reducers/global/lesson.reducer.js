import {
  POST_ADD_LESSON_REQUEST,
  POST_ADD_LESSON_SUCCESS,
  POST_ADD_LESSON_ERROR,
  CHANGE_STATUS_LESSON_REQUEST,
  CHANGE_STATUS_LESSON_SUCCESS,
  CHANGE_STATUS_LESSON_ERROR,
  GET_SECTION_LESSONS_REQUEST,
  GET_SECTION_LESSONS_SUCCESS,
  GET_SECTION_LESSONS_ERROR,
  GET_SECTION_STUDENTS_REQUEST,
  GET_SECTION_STUDENTS_SUCCESS,
  GET_SECTION_STUDENTS_ERROR,
  REMOVE_LESSON_LESSON_REQUEST,
  REMOVE_LESSON_LESSON_SUCCESS,
  REMOVE_LESSON_LESSON_ERROR,
  GET_LESSON_BY_ID_REQUEST,
  GET_LESSON_BY_ID_SUCCESS,
  GET_LESSON_BY_ID_ERROR,
} from "store/actions/global/globalTypes";
import produce from "immer";

const initState = {
  sectionStudents: [],
  sectionLessons: [],
  lessonById: {},

  changeStatusLessonLoading: false,
  sectionStudentsLoading: false,
  sectionLessonsLoading: false,
  removeLessonLoading: false,
  addLessonLoading: false,
  lessonByIdLoading: false,
};

const lessonReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case POST_ADD_LESSON_REQUEST:
        draft.addLessonLoading = true;
        return draft;
      case POST_ADD_LESSON_SUCCESS:
        draft.addLessonLoading = false;
        return draft;
      case POST_ADD_LESSON_ERROR:
        draft.addLessonLoading = false;
        return draft;

      case CHANGE_STATUS_LESSON_REQUEST:
        draft.changeStatusLessonLoading = true;
        return draft;
      case CHANGE_STATUS_LESSON_SUCCESS:
        draft.changeStatusLessonLoading = false;
        return draft;
      case CHANGE_STATUS_LESSON_ERROR:
        draft.changeStatusLessonLoading = false;
        return draft;

      case GET_SECTION_LESSONS_REQUEST:
        draft.sectionLessonsLoading = true;
        return draft;
      case GET_SECTION_LESSONS_SUCCESS:
        draft.sectionLessons = action.payload.sectionLessons;
        draft.sectionLessonsLoading = false;
        return draft;
      case GET_SECTION_LESSONS_ERROR:
        draft.sectionLessonsLoading = false;
        return draft;

      case GET_SECTION_STUDENTS_REQUEST:
        draft.sectionStudentsLoading = true;
        return draft;
      case GET_SECTION_STUDENTS_SUCCESS:
        draft.sectionStudents = action.payload.sectionStudents;
        draft.sectionStudentsLoading = false;
        return draft;
      case GET_SECTION_STUDENTS_ERROR:
        draft.sectionStudentsLoading = false;
        return draft;

      case REMOVE_LESSON_LESSON_REQUEST:
        draft.removeLessonLoading = true;
        return draft;
      case REMOVE_LESSON_LESSON_SUCCESS:
        draft.removeLessonLoading = false;
        return draft;
      case REMOVE_LESSON_LESSON_ERROR:
        draft.removeLessonLoading = false;
        return draft;

      case GET_LESSON_BY_ID_REQUEST:
        draft.lessonByIdLoading = true;
        return draft;
      case GET_LESSON_BY_ID_SUCCESS:
        draft.lessonByIdLoading = false;
        draft.lessonById = action.payload.data;
        return draft;
      case GET_LESSON_BY_ID_ERROR:
        draft.lessonByIdLoading = false;
        return draft;

      default:
        return draft;
    }
  });

export default lessonReducer;
