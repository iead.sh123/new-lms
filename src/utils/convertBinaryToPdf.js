import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const ConvertBinaryToPdf = ({ binaryFile, mimeType = "application/pdf", folderName = "file" }) => {
	const blob = new Blob([binaryFile], {
		type: mimeType,
	});
	const blobUrl = window.URL.createObjectURL(blob);
	const a = document.createElement("a");
	a.href = blobUrl;
	a.download = `${folderName}`;

	// Check if the browser supports creating a Blob URL
	if ("download" in a) {
		a.style.display = "none";
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	} else {
		toast.error(tr`Your browser does not support Blob URLs or download attribute.`);
	}

	// Clean up by revoking the Blob URL
	URL.revokeObjectURL(blobUrl);
};

export default ConvertBinaryToPdf;
