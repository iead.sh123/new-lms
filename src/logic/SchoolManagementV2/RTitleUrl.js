import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React from 'react'
import { useState } from 'react'
import { Input } from 'reactstrap'
const RTitleUrl = ({ linkIndex,onDelete }) => {
    const [titelInputValue, setTitleInputValue] = useState('')
    const [URLInputValue, setURLInputValue] = useState('')
    const handleTitleInputChange = (event) => {
        setTitleInputValue(event.target.value)
    }
    const handleURLInputChange = (event) => {
        setURLInputValue(event.target.value)
    }
    const deleteLink=(event,index)=>{
        onDelete(index)
        event.stopPropagation()
    }
    return (
        <RFlex
            id="title with url"
            className="align-items-center"
        >
            <RFlex
                styleProps={{ gap: '0px' }}
                className="flex-column"
                id="just title inputs or title header with title input">
                {linkIndex === 0 ?
                    (<>
                        <span className='p-0 m-0'>
                            Link Title
                        </span>
                        <Input
                            value={titelInputValue}
                            onChange={(event) => handleTitleInputChange(event)}
                            type='text'
                            placeholder='link title'
                            style={{ width: '165px' }} />
                    </>)
                    :
                    (
                        <Input
                            value={titelInputValue}
                            onChange={(event) => handleTitleInputChange(event)}
                            type='text'
                            placeholder='link title'
                            style={{ width: '165px' }} />
                    )
                }
            </RFlex>
            <RFlex
                styleProps={{ gap: '0px' }}
                className="flex-column"
                id="just url inputs or url header with url input">
                {linkIndex === 0 ?
                    (<>
                        <span className='p-0 m-0'>
                            URl
                        </span>
                        <Input
                            value={URLInputValue}
                            onChange={(event) => handleURLInputChange(event)}
                            type='url'
                            placeholder='www.google.com'
                            style={{ width: '320px' }} />
                    </>)
                    :
                    (
                        <Input
                            value={URLInputValue}
                            onChange={(event) => handleURLInputChange(event)}
                            type='url'
                            placeholder='www.google.com'
                            style={{ width: '320px' }} />
                    )
                }
            </RFlex>
            <i
                className='fa-regular fa-trash-can text-danger'
                style={{ marginTop: linkIndex === 0 ? '19px' : '0px',cursor:'pointer' }}
                onClick={(event)=>deleteLink(event,linkIndex)} />
        </RFlex>
    )
}

export default RTitleUrl