import React from "react";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RSelect from "components/Global/RComs/RSelect";
import Credit from "./Credit";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form, FormGroup, Input, Label } from "reactstrap";

const RLearningObject = ({
  learningObject,
  certificate,
  badges,
  handleAddValueToLearningObject,
  options,
  showCredit,
  setShowCredit,
  certificateSelected,
  setCertificateSelected,
}) => {
  return (
    <Form>
      <Row>
        <Col xs={12} sm={6}>
          <FormGroup>
            <Label>{tr`max_interactions_allowed`}</Label>
            <Input
              type="number"
              name="max_interactions_allowed"
              placeholder={tr`max_interactions_allowed`}
              defaultValue={learningObject.max_interactions_allowed}
              onChange={(e) => {
                handleAddValueToLearningObject(e.target.name, e.target.value);
              }}
            />
          </FormGroup>
        </Col>
        <Col xs={12} sm={6}>
          <FormGroup>
            <Label>{tr`weight`}</Label>
            <Input
              type="number"
              name="weight"
              placeholder={tr`weight`}
              defaultValue={learningObject.weight}
              onChange={(e) => {
                handleAddValueToLearningObject(e.target.name, e.target.value);
              }}
            />
          </FormGroup>
        </Col>
        <Col xs={12} sm={6}>
          <FormGroup>
            <Label>{tr`pre_requisites`}</Label>
            <RSelect
              option={options}
              closeMenuOnSelect={true}
              placeholder={tr`pre_requisites`}
              isMulti={true}
              onChange={(e) =>
                handleAddValueToLearningObject(
                  "prerequisite_ids",
                  e.map((el) => el.value)
                )
              }
              defaultValue={
                learningObject?.prerequisites &&
                learningObject?.prerequisites.map((el) => {
                  return {
                    label: el.prerequisite_learning_object.content_name,
                    value: el.prerequisite_learning_object.id,
                  };
                })
              }
            />
          </FormGroup>
        </Col>
        <Col xs={12} sm={6}>
          <FormGroup>
            <Label>{tr`next`}</Label>
            <RSelect
              option={options}
              closeMenuOnSelect={true}
              placeholder={tr`next`}
              onChange={(e) => handleAddValueToLearningObject("next", e.value)}
              defaultValue={[
                {
                  label: learningObject.content_name,
                  value: learningObject.id,
                },
              ]}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <AppCheckbox
            checked={showCredit}
            onChange={(event) => setShowCredit(event.target.checked)}
            label={tr`add_credit`}
          />
        </Col>
        <Credit
          learningObject={learningObject}
          showCredit={showCredit}
          certificate={certificate}
          badges={badges}
          handleAddValueToLearningObject={handleAddValueToLearningObject}
          certificateSelected={certificateSelected}
          setCertificateSelected={setCertificateSelected}
        />
      </Row>
    </Form>
  );
};

export default RLearningObject;
