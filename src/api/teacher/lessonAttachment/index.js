import { get, post, destroy } from "config/api";

export const lessonAttachmentApi = {
  getLessonAttachment: (lessonId) => get(`api/lesson-attachments/${lessonId}`),
  storeLessonAttachment: (rabID, lessonID, request) =>
    post(`api/lesson-attachments/store/${rabID}/${lessonID}`, request),
  deleteLessonAttachment: (attachmentId) =>
    destroy(`api/lesson-attachments/delete/${attachmentId}`),
    getBooks: (rabId) => get(`api/rabs/${rabId}/books`),
};
