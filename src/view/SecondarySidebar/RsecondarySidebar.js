import React from "react";
import tr from "components/Global/RComs/RTranslator";
import { useLocation } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { Nav, NavItem } from "reactstrap";
import styles from "./RSecondarySidebar.module.scss";

const RSecondarySidebar = (props) => {
	const { secondSideBarRoutes, bgColor, activeColor } = props;
	const location = useLocation();
	const english = localStorage.getItem("language") != "arabic";
	const float = english ? "left" : "right";

	const activeRoute = (tabName) => {
		const segments = location.pathname.split("/");
		const lastWord = segments[segments.length - 1];

		return lastWord.replace(/-/g, "") == tabName.replace(/_/g, "") ? styles.active__item : styles.not__active__item;
	};

	const path = location.pathname;

	const courseIdMatch = path.match(/\/course-management\/course\/([^/]+)/);
	const courseId = courseIdMatch ? courseIdMatch[1] : null;

	const categoryIdMatch = path.match(/\/category\/([^/]+)/);
	const categoryId = categoryIdMatch ? categoryIdMatch[1] : null;

	const cohortIdMatch = path.match(/\/cohort\/([^/]+)/);
	const cohortId = cohortIdMatch ? cohortIdMatch[1] : null;

	const lessonIdMatch = path.match(/\/lesson\/([^/]+)/);
	const lessonId = lessonIdMatch ? lessonIdMatch[1] : null;

	const curriculumIdMatch = path.match(/\/curricula\/([^/]+)/);
	const curriculumId = curriculumIdMatch ? curriculumIdMatch[1] : null;

	const createLinks = (routes) => {
		return routes.map((prop, key) => {
			if (prop.redirect) {
				return null;
			}

			if (prop.invisible) {
				return null;
			}

			const dynamicPathWithCourseId = prop.path.replace(":courseId", courseId);

			const dynamicPathWithCategoryId = dynamicPathWithCourseId.replace(":categoryId", categoryId);

			const dynamicPathWithCohortId = dynamicPathWithCategoryId.replace(":cohortId", cohortId);

			const dynamicPathWithCurriculumId = dynamicPathWithCohortId.replace(":curriculumId", curriculumId);

			const dynamicPath = dynamicPathWithCurriculumId.replace(":lessonId", lessonId);

			return (
				<li key={key}>
					<NavItem>
						<NavLink style={{ textAlign: float }} to={prop.layout + dynamicPath} className={prop.disabled ? "disabled__link" : ""}>
							{prop.icon !== undefined ? (
								<>
									<i className={prop.icon} style={{ float: float }} />
									<p style={{ textTransform: "capitalize" }}>{tr(prop.name)}</p>
								</>
							) : (
								<>
									<span className="sidebar-mini-icon">{prop.mini}</span>
									<span className={activeRoute(prop.name)}>{tr(prop.name)}</span>
								</>
							)}
						</NavLink>
					</NavItem>
				</li>
			);
		});
	};

	return (
		<div className={styles.sidebar__container + " scroll_hidden"} data-color={bgColor} data-active-color={activeColor}>
			<h6 className={styles.sidebar__title}>{tr`Course Management`}</h6>
			{secondSideBarRoutes && secondSideBarRoutes.length > 0 && <Nav style={{ display: "block" }}>{createLinks(secondSideBarRoutes)}</Nav>}
		</div>
	);
};

export default RSecondarySidebar;
