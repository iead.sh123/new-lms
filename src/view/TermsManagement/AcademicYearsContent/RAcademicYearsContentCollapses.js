import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { boldGreyColor } from "config/constants";
import { dangerColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import moment from "moment";
import styles from "../TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { successColor } from "config/constants";

const RAcademicYearsContentCollapses = ({
	data,
	type,
	iD,
	aYCollapse,
	tCollapse,
	gLCollapse,
	academicYearId = null,
	termId = null,
	gradeLevelId = null,
	homeRoomId = null,
	firstCurriculumID = null,
	allHomeRoomsToGradeLevel = [],
	mLeft = "0px",
	fWeight = "bold",
}) => {
	const TermsManagementData = useContext(TermsManagementContext);

	// const aYCollapse = TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${iD}`;
	// const tCollapse = TermsManagementData.openedTermsCollapses == "collapse" + `term_${iD}`;
	// const gLCollapse = TermsManagementData.openedGradeLevelsCollapses == "collapse" + `gradeLevel_${iD}`;
	return (
		<RFlex
			styleProps={{ flexDirection: "column", width: "100%" }}
			onClick={() =>
				TermsManagementData.handleChangeActiveNode({
					nodeName: type,
					academicYearId: academicYearId,
					termId: termId,
					gradeLevelId: gradeLevelId,
					homeRoomId: homeRoomId,
					firstCurriculumID: firstCurriculumID,
					allHomeRoomsToGradeLevel: allHomeRoomsToGradeLevel,
					data: data,
				})
			}
		>
			<RFlex
				className={styles.collapse__div}
				styleProps={{
					marginLeft: mLeft,
					fontWeight: fWeight,
					border:
						type == "academicYear"
							? TermsManagementData.highlightCollapse == `academicYear_${iD}` &&
							  TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${iD}`
								? `1px solid ${primaryColor}`
								: ""
							: type == "term"
							? TermsManagementData.highlightCollapse == `term_${iD}` &&
							  TermsManagementData.openedTermsCollapses == "collapse" + `term_${iD}`
								? `1px solid ${primaryColor}`
								: ""
							: type == "gradeLevel"
							? TermsManagementData.highlightCollapse == `gradeLevel_${iD}` &&
							  TermsManagementData.openedGradeLevelsCollapses == "collapse" + `gradeLevel_${iD}`
								? `1px solid ${primaryColor}`
								: ""
							: "",
				}}
				onClick={() =>
					type == "academicYear" || type == "term" || type == "gradeLevel"
						? TermsManagementData.handleOpenCollapse({ id: `${type}_${iD}`, type: type })
						: {}
				}
				onMouseEnter={() => {
					type == "academicYear" || type == "term" || type == "gradeLevel"
						? TermsManagementData.handleIconHovering(`${type}_${data.id}`)
						: {};
				}}
				onMouseLeave={() => {
					type == "academicYear" || type == "term" || type == "gradeLevel"
						? TermsManagementData.handleIconHovering(`${type}_${data.id}`)
						: {};
				}}
			>
				{/* <span>{aYCollapse + "_" + tCollapse + "_" + gLCollapse}</span> */}
				<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
					<RFlex styleProps={{ alignItems: "center", gap: "15px" }}>
						<span
							onClick={(event) => {
								event.stopPropagation();

								if (type == "course") {
									TermsManagementData.handlePushToModules(data.id, data.cohort_id);
								}
							}}
						>
							{data?.name ?? data?.title}
						</span>
						{/* {type == "course" && (
							<span
								style={{ color: primaryColor, cursor: "pointer" }}
								onClick={() => TermsManagementData.handlePublishCourseMutation(data.id)}
							>{tr`publish`}</span>
						)} */}
						{type == "term" && (
							<span style={{ color: data?.isFinalized ? dangerColor : data?.isActive ? successColor : primaryColor, fontWeight: 500 }}>
								{data.isActive && !data.isFinalized
									? "active"
									: !data.isActive && !data.isFinalized
									? "inactive"
									: !data.isActive && data.isFinalized
									? "finalized"
									: ""}
							</span>
						)}

						{TermsManagementData.isHovering == `${type}_${data.id}` && (type == "academicYear" || type == "term") && (
							<RFlex styleProps={{ gap: "15px" }}>
								<i
									className={iconsFa6.delete}
									style={{ color: dangerColor, fontSize: "11px" }}
									onClick={(event) => {
										event.stopPropagation();
										TermsManagementData.handelDelete(data.id, type);
									}}
								/>
								<i
									className={iconsFa6.pen}
									style={{ fontSize: "11px" }}
									onClick={(event) => {
										event.stopPropagation();
										type == "academicYear"
											? TermsManagementData.handleOpenAcademicYearModal(data)
											: TermsManagementData.handleOpenTermModal(data, academicYearId);
									}}
								/>
							</RFlex>
						)}
					</RFlex>

					<RFlex styleProps={{ alignItems: "center", gap: "15px", color: boldGreyColor, fontWeight: 300 }}>
						{(type == "academicYear" || type == "term") && (
							<RFlex>
								{moment(data?.startDate).format("l")}
								<span>-</span>
								{moment(data?.endDate).format("l")}
							</RFlex>
						)}

						{type == "course" && <RFlex>{data?.category_name}</RFlex>}
						<RFlex>
							{type == "academicYear" ? (
								<span>{data?.terms?.length > 0 ? `${data?.terms?.length} terms` : tr`no_terms`}</span>
							) : type == "term" ? (
								<span>
									{data?.courses
										? data?.courses?.length > 0
											? `${data?.courses?.length} ${tr("courses")}`
											: tr`no_courses_yet`
										: data?.gradelevels?.length > 0
										? tr`all_grade_levels`
										: tr`no_grade_levels`}
								</span>
							) : type == "gradeLevel" ? (
								<span>
									{data?.children?.homerooms?.length > 0 ? `${data?.children?.homerooms?.length} ${tr`home_rooms`}` : tr`no_home_rooms`}
								</span>
							) : (
								<span></span>
							)}
						</RFlex>
						{(type == "academicYear" || type == "term" || type == "gradeLevel") && (
							<RFlex>
								<i
									className={
										type == "academicYear"
											? TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${iD}`
												? iconsFa6.chevronDown
												: iconsFa6.chevronRight
											: type == "term"
											? TermsManagementData.openedTermsCollapses == "collapse" + `term_${iD}`
												? iconsFa6.chevronDown
												: iconsFa6.chevronRight
											: type == "gradeLevel"
											? TermsManagementData.openedGradeLevelsCollapses == "collapse" + `gradeLevel_${iD}`
												? iconsFa6.chevronDown
												: iconsFa6.chevronRight
											: ""
									}
									style={{
										fontSize: "11px",
										fontWeight: "bold",
									}}
								/>
							</RFlex>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RAcademicYearsContentCollapses;
