import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import ConvertBinaryToPdf from "utils/convertBinaryToPdf";
import tr from "components/Global/RComs/RTranslator";

//----***types options***----
// interface MutationOptions {
// 	queryFn: (...args: any[]) => Promise<any>;
// 	onSuccessFn?: (...options: any) => void;
// 	closeDialog?: () => void;
// 	multipleKeys?: boolean;
// 	invalidateKeys?: any[];
// 	displaySuccess?: boolean;
// 	onSuccessLink?: string;
// 	onSuccessLinkQueries?: any;
//  dispatch:boolean //to indicates if there as any dispatch after success
//  action : the action that need to be dispatched after success
//  downloadFile?: boolean;
// }

export const useMutateData = (options) => {
	const dispatch = useDispatch();
	const queryClient = useQueryClient();
	const history = useHistory();

	const data = useMutation(options.queryFn, {
		onSuccess(data, variables, context) {
			// Because the response from the backend adopts this method, I don't know why
			if (data?.data?.status === 0 || data?.data?.code == 500) {
				data?.data?.msg?.map((el) => toast.error(el)) ?? data?.msg?.map((el) => toast.error(el));

				options.onErrorFn && options.onErrorFn(data?.data?.msg ?? data?.msg, variables);
			} else {
				options.multipleKeys
					? options.invalidateKeys && options.invalidateKeys.map((key) => queryClient.invalidateQueries(key))
					: options.invalidateKeys && queryClient.invalidateQueries(options.invalidateKeys);

				options.displaySuccess && toast.success(data?.data?.msg ?? tr`success`);

				options.onSuccessLink &&
					history.push({
						pathname: options.onSuccessLink,
						query: options.onSuccessLinkQueries ? options.onSuccessLinkQueries : {},
					});
				options.onSuccessFn && options.onSuccessFn({ data: data?.data, variables: variables });
				options.closeDialog && options.closeDialog();
				options.downloadFile &&
					ConvertBinaryToPdf({
						binaryFile: data?.data,
						mimeType: variables?.mimeType ?? options.mimeType,
						folderName: variables?.folderName ?? options.folderName,
					});
				options.dispatch && dispatch(options.action(data));
			}
		},
		onError: (error, variables) => {
			toast.error(error?.message ?? "Error");
			options.onErrorFn && options.onErrorFn(error?.message ?? "Error", variables);
		},
	});
	return data;
};
