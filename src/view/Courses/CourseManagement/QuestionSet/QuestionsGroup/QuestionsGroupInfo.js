import React, { useEffect, useContext } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { dangerColor } from "config/constants";
import { Services } from "engine/services";
import { RLabel } from "components/Global/RComs/RLabel";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const QuestionsGroupInfo = ({ group, questionGroupId, formChecked }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	//this method don't correct ,it must be adjusted
	useEffect(() => {
		QuestionSetData.setRequiredAnswerQuestionsGroup(formChecked);
	}, []);

	return (
		<Row className="m-3">
			<Col xs={12} md={4} className="pl-0">
				<RFlex styleProps={{ gap: 0, flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("name")}</Label>
						<RLabel value={tr(" ")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<Input
							name="title"
							type="text"
							placeholder={tr`title`}
							value={group?.title}
							onChange={(event) =>
								QuestionSetData.handleChangeQuestionsGroup({
									questionGroupId: questionGroupId,
									key: event.target.name,
									value: event.target.value,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
			<Col xs={12} md={4}>
				<RFlex styleProps={{ gap: 0, flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("tags")}</Label>
						<RLabel value={tr(" ")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<RTags
							url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
							getValueFromArrayItem={(i) => i.id}
							getLabelFromArrayItem={(i) => i.name}
							getIdFromArrayItem={(i) => i.id}
							defaultValues={group?.tags}
							onChange={(data) =>
								QuestionSetData.handleChangeQuestionsGroup({
									questionGroupId: questionGroupId,
									key: "tags",
									value: data,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
			<Col xs={12} md={4} className="d-flex justify-content-end align-items-end">
				<i
					className={iconsFa6.delete}
					style={{ color: dangerColor, cursor: "pointer" }}
					onClick={() =>
						QuestionSetData.handleRemoveQuestionGroup({
							questionGroupId: questionGroupId,
							questionTypeId: null,
							isGroup: group.isGroup,
						})
					}
				/>
			</Col>
			<Col sm={8} xs={12} className="pl-0  mb-3">
				<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
					<RFlex>
						<AppCheckbox
							onChange={(event) => {
								QuestionSetData.handleRequiredAnswerQuestionsGroup();
							}}
							label={<span>{tr`required_question`}</span>}
							checked={QuestionSetData.requiredAnswerQuestionsGroup}
						/>
					</RFlex>

					{QuestionSetData.requiredAnswerQuestionsGroup && (
						<>
							<RFlex
								styleProps={{
									alignItems: "center",
								}}
							>
								<Label className="p-0 m-0">{tr("number_of_required_questions")}</Label>
								<FormGroup>
									<Input
										name="requiredQuestionsToAnswer"
										type="number"
										value={group?.requiredQuestionsToAnswer}
										min={1}
										style={{ width: "80px" }}
										onChange={(event) => {
											QuestionSetData.handleChangeQuestionsGroup({
												questionGroupId: questionGroupId,
												key: event.target.name,
												value: +event.target.value,
											});
											// QuestionSetData.handleChangeAllRequiredAnswerToFalse({
											//   questionGroupId: questionGroupId,
											//   key: "isRequired",
											//   value: false,
											// });
										}}
									/>
								</FormGroup>
							</RFlex>
							<RFlex styleProps={{ alignItems: "center" }}>
								<Label className="p-0 m-0">{tr("points_for_each_question")}</Label>
								<FormGroup>
									<Input
										name="points"
										type="number"
										value={group?.points}
										min={1}
										style={{ width: "80px" }}
										onChange={(event) => {
											QuestionSetData.handleChangeQuestionsGroup({
												questionGroupId: questionGroupId,
												key: event.target.name,
												value: event.target.value,
											});

											// QuestionSetData.handleChangeAllRequiredAnswerToFalse({
											//   questionGroupId: questionGroupId,
											//   key: "isRequired",
											//   value: false,
											// });
										}}
									/>
								</FormGroup>
							</RFlex>
						</>
					)}
				</RFlex>
			</Col>
			<Col xs={12} className="pl-0">
				<RFileSuite
					parentCallback={(values) => {
						QuestionSetData.handleChangeQuestionsGroup({
							questionGroupId: questionGroupId,
							key: "files",
							value: values,
						});
					}}
					singleFile={true}
					binary={true}
					value={group?.files}
					fileType="image/*"
				/>
			</Col>
		</Row>
	);
};

export default QuestionsGroupInfo;
