import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import { dangerColor, warningColor } from "config/constants";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RCkEditor from "components/Global/RComs/RCkEditor";
import styles from "./QuestionsTypes.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import iconsFa6 from "variables/iconsFa6";

const FillInBlank = ({ questionGroupId, question, questionNumber = 0, group }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const requiredQuestionsToQuestionGroup = group?.requiredQuestionsToAnswer && group?.points ? true : false;

	return (
		<>
			<Row className="mt-4 mb-1">
				<Col md={8} xs={12}>
					<RFlex styleProps={{ flexDirection: "column" }}>
						<RFlex>
							<i className="fa-solid fa-pen-to-square pt-1" />
							<RFlex>
								<Label className="p-0 m-0">
									{tr`question`} &nbsp;
									{questionNumber}
								</Label>
								<Label className="p-0 m-0">({tr("fill_in_blank")})</Label>
							</RFlex>
						</RFlex>
						<FormGroup>
							<RCkEditor
								data={question?.text}
								handleChange={(data) =>
									QuestionSetData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: "text",
										value: data,
									})
								}
							/>
						</FormGroup>
					</RFlex>
				</Col>
				<Col md={4} xs={12}>
					<RFlex
						styleProps={{
							alignItems: "center",
							justifyContent: "space-between",
						}}
					>
						<RFlex styleProps={{ flexDirection: "column" }}>
							<RFlex>
								<Label className="p-0 m-0">{tr("points")}</Label>
							</RFlex>
							<FormGroup>
								<Input
									name="points"
									type="number"
									placeholder={tr`points`}
									min={1}
									value={requiredQuestionsToQuestionGroup ? group.points : question?.points}
									disabled={requiredQuestionsToQuestionGroup}
									onChange={(event) => {
										QuestionSetData.handleFillDataToQuestionType({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											key: event.target.name,
											value: event.target.value,
										});
									}}
								/>
							</FormGroup>
						</RFlex>

						<RFlex styleProps={{ alignItems: "center", paddingTop: "20px" }}>
							<RFileSuite
								parentCallback={(values) => {
									QuestionSetData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: "files",
										value: values,
									});
								}}
								singleFile={true}
								binary={true}
								value={question?.files}
								theme="button_with_replace"
								fileType="image/*"
							/>
							<i
								className={iconsFa6.delete}
								style={{ color: dangerColor, cursor: "pointer" }}
								onClick={() =>
									QuestionSetData.handleRemoveQuestionGroup({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										isGroup: group?.isGroup,
									})
								}
							></i>
						</RFlex>
					</RFlex>
					<Button type="button" className={styles.instructions__button}>
						<i className="far fa-question-circle"></i> {tr`instructions`}
					</Button>
				</Col>
			</Row>
			<Row className="mt-4 mb-2">
				<Col xs={12} className="mt-2">
					<AppCheckbox
						onChange={(event) => {
							QuestionSetData.handleFillDataToQuestionType({
								questionGroupId: questionGroupId,
								questionTypeId: question?.id ? question?.id : question?.fakeId,
								key: "shuffleAnswers",
								value: event.target.checked,
							});
						}}
						label={tr`shuffle_answer`}
					/>
				</Col>
				{!requiredQuestionsToQuestionGroup && (
					<Col xs={12} className="mt-2">
						<AppCheckbox
							onChange={(event) => {
								QuestionSetData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "isRequired",
									value: event.target.checked,
								});
							}}
							label={<span>{tr`required_question`}</span>}
							checked={question?.isRequired}
						/>
					</Col>
				)}
			</Row>
		</>
	);
};

export default FillInBlank;
