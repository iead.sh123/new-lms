import React, { useState, useEffect } from "react";
import { TYPES } from "../assignmentReducer";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import MathJax from "react-mathjax-preview";

const SingleAnswerQuestion = ({
  choices,
  answers,
  question,
  groupId,
  dispatch,
  assignmentReadMode,
  student,
  mode,
}) => {
  const [correctAnswer, setCorrectAnswer] = useState();
  const [studentChoice, setStudentChoice] = useState();

  useEffect(() => {
    if (assignmentReadMode && mode !== "multi") {
      if (question.notSolved === 1) {
        setStudentChoice();
        setCorrectAnswer();
        return;
      }
      setStudentChoice(question.student_choices[0].id);
      setCorrectAnswer(
        choices.filter((choice) => choice.is_correct === 1)[0].id
      );
    }

    if (mode === "multi") {
      if (student.notSolved === 1 || student.notSolved === null) {
        setStudentChoice();
        setCorrectAnswer();
        return;
      }
      setStudentChoice(student.student_choices[0].id);
      setCorrectAnswer(
        choices.filter((choice) => choice.is_correct === 1)[0].id
      );
    }
  }, [question.id, assignmentReadMode]);

  const changeChoiceHandler = (e, choiceId) => {
    dispatch({
      type: TYPES.SAVE_ANSWER,
      payload: {
        QuestionId: question.id,
        GroupId: groupId,
        choiceId,
        Choices: [choiceId],
        FreeAnswer: null,
      },
    });
  };
  return choices.map((choice, index) => {
    return (
      <div
        key={index + choice.id}
        className="col-md-12 d-flex align-items-center justify-content-center row mt-2"
      >
        <div
          className={`${choice.attachments ? "col-md-7" : "col-md-11"} mr-auto`}
        >
          <div
            className={`form-control ${
              choice.id === correctAnswer ? "correct_answer" : ""
            }${
              choice.id === studentChoice && choice.id !== correctAnswer
                ? "wrong_answer"
                : ""
            }`}
          >
            {choice.choice && choice.choice.includes("<math") ? (
              <MathJax math={choice.choice} />
            ) : (
              <div dangerouslySetInnerHTML={{ __html: choice.choice }} />
            )}
          </div>
        </div>
        {choice.attachments && (
          <div className="col-md-3" style={{ textAlign: "center" }}>
            {["png", "jpeg"].includes(choice.attachments.mime_type) ? (
              <ImageEnlarge
                imageSrc={resourcesBaseUrl + choice.attachments.url}
              />
            ) : (
              <a
                href={resourcesBaseUrl + choice.attachments.url}
                alt=""
                className="btn btn-primary"
              >
                Download
              </a>
            )}
          </div>
        )}
        <div className="col-md-1 text-center">
          <div className="custom-control custom-checkbox custom-control-inline mx-0 pl-3">
            <div className="form-check">
              <input
                type="radio"
                value={choice.id}
                onChange={(e) => changeChoiceHandler(e, choice.id)}
                className="form-check-input assignment_checkbox"
                name="single_answer"
                checked={
                  assignmentReadMode
                    ? choice.id === studentChoice
                    : answers.some(
                        (answer) =>
                          answer.choiceId === choice.id &&
                          answer.QuestionId === question.id
                      )
                }
                disabled={assignmentReadMode}
              />
            </div>
          </div>
        </div>
      </div>
    );
  });
};

export default SingleAnswerQuestion;
