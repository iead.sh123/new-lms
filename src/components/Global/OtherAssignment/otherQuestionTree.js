import React, { useEffect, useMemo, useState } from "react";
import { TYPES } from "./otherAssignmentReducer";
import { useSelector, useDispatch } from "react-redux";
import { updateQuestionSetStudentDetails } from "store/actions/teacher/questionsSets.actions";
import REditableTree from "../RComs/REditableTree/REditableTree";

const OtherQuestionsTree = ({
  lessonGroups,
  dispatch,
  currentActiveTopic,
  currentActiveGroup,
  currentActiveQuestion,
  currentQuestion,
  studentQsQuestionId,
}) => {
  const dispatch1 = useDispatch();

  const [selectedNode, setSelectedNode] = useState("");
  const { studentDetails } = useSelector((state) => state.questionSets);

  const onSelect = (info) => {
    dispatch({
      type: TYPES.CHANGE_QUESTION_TREE,
      payload: { info, lessonGroups },
    });
    dispatch1(
      updateQuestionSetStudentDetails(studentQsQuestionId, {
        payload: { data: studentDetails },
      })
    );
    if (
      currentActiveQuestion > 0 ||
      currentActiveGroup > 0 ||
      currentActiveTopic > 0
    ) {
      dispatch({ type: TYPES.DISABLE_NEXT_BUTTON, payload: false });
    }
  };

  useEffect(() => {
    setSelectedNode(
      `${currentQuestion?.id}${currentActiveGroup}${currentActiveTopic}${currentActiveQuestion}`
    );
  }, [currentActiveTopic, currentActiveGroup, currentActiveQuestion]);

  const treeData = useMemo(() => {
    return lessonGroups?.inst_pattern_groups?.map((group, groupIndex) => {
      const topics = group.inst_topics?.map((topic, topicIndex) => {
        const questions = topic.inst_questions?.map(
          (question, questionIndex) => {
            return {
              name: questionIndex + 1,
              key: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
              id: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
              ID: `${question.id}`,
              groupId: groupIndex,
              topicId: topicIndex,
              questionId: questionIndex,
              type: "question",
              className:
                question.notSolved === 0
                  ? "assignment_tree_question-solved"
                  : "",
              link: "",
              color: question.notSolved === 0 ? "#ffeeff" : "#ffeeff",
              icon:
                question.notSolved === 0
                  ? "fa fa-check-circle-o ml-2 "
                  : "fa fa-check-circle ml-2",
            };
          }
        );
        return {
          name: topic.title,
          key: `${topic.id}${groupIndex}${topicIndex}`,
          groupId: groupIndex,
          topicId: topicIndex,
          items: questions,
          type: "topic",
          id: `${topic.id}${groupIndex}${topicIndex}`,
          ID: `${topic.id}`,
        };
      });
      return {
        name: group.pattern_group_text,
        key: `${group.id}${groupIndex}`,
        id: `${group.id}${groupIndex}`,
        ID: `${group.id}`,
        groupId: groupIndex,
        items: topics,
        type: "group",
        link: "group link",
      };
    });
  }, []);

  return (
    <div
      className="gu-assignment-questions-tree mt-1   "
      style={{ borderTopRightRadius: "15px", borderBottomRightRadius: "15px" }}
    >
      {treeData?.map((s) => (
        <REditableTree
          data={s}
          activeNodes={[selectedNode]}
          linkPrefix=""
          nodeClicked={onSelect}
        ></REditableTree>
      ))}
    </div>
  );
};

export default OtherQuestionsTree;
