import React from "react";
import styles from "./RegularCard.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import RCardSkeleton from "../RCardSkeleton/RCardSkeleton";

const RRegularCard = ({ id, header, icon, handleIconClicked, items, status, contextMenuActions, style = {} }) => {
	return (
		<div className={styles.Card} style={status ? { borderBottom: `2px solid ${status.color}`, ...style } : { ...style }}>
			<RFlex styleProps={{ justifyContent: "space-between", fontSize: "18px" }}>
				<p>{header}</p>
				{icon ? <FontAwesomeIcon role="button" icon={icon} onClick={handleIconClicked} /> : <></>}
				{contextMenuActions && contextMenuActions.length > 0 && (
					<UncontrolledDropdown direction="right">
						<DropdownToggle
							aria-haspopup={true}
							// caret
							color="default"
							data-toggle="dropdown"
							nav
							style={{
								color: "black",
								fontWeight: "bolder",
								fontSize: "large",
								// background: `black`,
								// borderRadius: "100%",
							}}
						>
							<i class="fa fa-ellipsis-v" aria-hidden="true" role="button" style={{ cursor: "pointer" }}></i>
						</DropdownToggle>
						<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
							{contextMenuActions?.map((action, index) => (
								<DropdownItem
									key={action.id}
									disabled={action.disabled ? action.disabled : false}
									onClick={() => {
										action?.onClick?.();
									}}
								>
									<RFlex
										styleProps={{
											alignItems: "center",
											justifyContent: "space-between",
											...(action.color && { color: action.color }),
										}}
									>
										<i role="button" class={action?.icon} aria-hidden="true" />
										<span>{action.name}</span>
									</RFlex>
								</DropdownItem>
							))}
						</DropdownMenu>
					</UncontrolledDropdown>
				)}
			</RFlex>

			{items && (
				<RFlex styleProps={{ flexDirection: "column", fontSize: "16px" }}>
					{items.map((item, ind) => (
						<RFlex key={"RegularCard-" + id + ind}>
							{item.icon ? <FontAwesomeIcon icon={item.icon} /> : <></>}
							<p>{item.text}</p>
						</RFlex>
					))}
				</RFlex>
			)}

			{status && <p style={{ color: status.color }}>{status.text}</p>}
		</div>
	);
};

export default RRegularCard;
