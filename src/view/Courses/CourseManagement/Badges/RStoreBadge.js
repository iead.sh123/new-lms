import React from "react";
import { useFormik } from "formik";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import * as yup from "yup";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import { useParams } from "react-router-dom";

const RStoreBadge = ({ storeBadgeLoading, badgeById, handleStoreBadge }) => {
	const { courseId, curriculumId } = useParams();
	const handleFormSubmit = async (values) => {
		let newValues = {};
		if (courseId) {
			newValues = { ...values, course_id: badgeById?.course_id || courseId };
		} else {
			newValues = {
				...values,
				curriculm_id: badgeById?.curricula_id || curriculumId,
			};
		}
		handleStoreBadge(newValues);
	};

	const initialValues = {
		id: badgeById?.id || null,
		description: badgeById?.description || "",
		attachments: badgeById?.attachments || [],
	};

	const formSchema = yup.object().shape({
		description: yup.string(tr`invalid description`).required(tr`description address is required`),
	});

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});
	return (
		<Form className="w-100" onSubmit={handleSubmit}>
			<Row>
				<Col xs={12} className={" p-0 m-0"}>
					<label>{tr("title")}:</label>
					<FormGroup>
						<Input
							name="description"
							type="text"
							value={values.description}
							placeholder={tr("title")}
							onBlur={handleBlur}
							onChange={handleChange}
							className={
								(!!touched.description && !!errors.description) || (touched.description && errors.description) ? "input__error" : ""
							}
						/>
						{touched.description && errors.description && <FormText color="danger">{errors.description}</FormText>}
					</FormGroup>
				</Col>
				<Col xs={12} className={" p-0 m-0"}>
					<FormGroup>
						<RFileSuite
							parentCallback={(values) => {
								setFieldValue("attachments", values);
							}}
							singleFile={true}
							binary={true}
							value={values?.attachments}
						/>
					</FormGroup>
				</Col>
			</Row>

			<Row className="d-flex justify-content-end mt-3">
				<Col xs={4} className=" p-0 m-0">
					<RButton
						className="w-100 "
						type="submit"
						text={tr`save`}
						color="primary"
						loading={storeBadgeLoading}
						disabled={storeBadgeLoading}
					/>
				</Col>
			</Row>
		</Form>
	);
};

export default RStoreBadge;
