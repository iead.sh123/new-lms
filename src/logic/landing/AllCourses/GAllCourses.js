import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  allFiltersToCoursesAsync,
  allCoursesAsync,
} from "store/actions/global/coursesManager.action";
import { useHistory, useLocation } from "react-router-dom";
import RCourseCategories from "view/Landing/AllCourses/RCourseCategories";
import RAllCourses from "view/Landing/AllCourses/RAllCourses";
import Loader from "utils/Loader";
import styles from "./AllCourses.module.scss";

const GAllCourses = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const categoryIdParams = queryParams.get("categoryId");
  const booleanFiltersParams = queryParams.get("booleanFilters");
  const textParams = queryParams.get("text");

  const { allCourses, allCoursesLoading } = useSelector(
    (state) => state.coursesManagerRed
  );
  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(allFiltersToCoursesAsync());
    if (categoryIdParams || textParams || booleanFiltersParams) {
      handleApplySearch(categoryIdParams ? categoryIdParams : null);
    } else {
      dispatch(allCoursesAsync(user, 1, {}));
    }
  }, []);

  const handleSearch = (categoryId) => {
    let queryParams = "";

    if (booleanFiltersParams && booleanFiltersParams.split(",").length > 0) {
      if (categoryId && textParams) {
        queryParams = `?text=${textParams}&categoryId=${categoryId}&booleanFilters=${booleanFiltersParams}`;
      } else if (!categoryId && textParams) {
        queryParams = `?text=${textParams}&booleanFilters=${booleanFiltersParams}`;
      } else if (categoryId && !textParams) {
        queryParams = `?categoryId=${categoryId}&booleanFilters=${booleanFiltersParams}`;
      } else {
        queryParams = `?booleanFilters=${booleanFiltersParams}`;
      }
    } else {
      if (categoryId && textParams) {
        queryParams = `?text=${textParams}&categoryId=${categoryId}`;
      } else if (!categoryId && textParams) {
        queryParams = `?text=${textParams}`;
      } else if (categoryId && !textParams) {
        queryParams = `?categoryId=${categoryId}`;
      } else {
        queryParams = "";
      }
    }

    history.push(`/landing/all-courses${queryParams}`);

    handleApplySearch(categoryId);
  };

  const handleApplySearch = (categoryId) => {
    let data = {};
    const booleanFilter =
      booleanFiltersParams && booleanFiltersParams.split(",");

    const newFormate =
      booleanFiltersParams &&
      booleanFilter.reduce((acc, item) => {
        acc[item] = true;
        return acc;
      }, {});

    if (categoryId) {
      data.category_id = categoryId;
    }

    if (booleanFilter?.length > 0) {
      data = { ...data, ...newFormate };
    }

    if (textParams !== "") {
      data.name = textParams;
    }

    dispatch(allCoursesAsync(user, 1, data));
  };

  return (
    <React.Fragment>
      {allCoursesLoading ? (
        <Loader />
      ) : (
        <section className={styles.container}>
          <article className={styles.categories + " scroll_hidden"}>
            <RCourseCategories
              allCourses={allCourses}
              handleSearch={handleSearch}
              categoryIdParams={categoryIdParams}
            />
          </article>
          <section className={styles.courses + " scroll_hidden"}>
            <RAllCourses allCourses={allCourses} />
          </section>
        </section>
      )}
    </React.Fragment>
  );
};

export default GAllCourses;
