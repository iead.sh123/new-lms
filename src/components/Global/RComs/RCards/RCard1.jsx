import React, { useRef } from "react";
import { Card, CardImg, Progress, Row, Col } from "reactstrap";
import { faPlusSquare, faStar } from "@fortawesome/free-regular-svg-icons";
import { baseURL, genericPath } from "engine/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router-dom";
import { Services } from "engine/services";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { primaryColor, successColor, warningColor, dangerColor } from "config/constants";
import TestImg from "assets/img/new/Chemistry.jpg";
import styles from "./RCard.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import moment from "moment";
import NotesButton from "views/Teacher/IM/Notes/NotesButton";
import RLiveAction from "../Collaboration/RLiveAction";
import Helper from "../Helper";

const RCard1 = ({
	link,
	color,
	details,
	users,
	notes,
	id,
	image,
	categories,
	lesson_name,
	categoryName,
	rate,
	actions,
	name,
	icon,
	add,
	title,
	progress,
	...others
}) => {
	const history = useHistory();

	const cardRef = useRef(null);

	// Assuming you have Moment.js library included in your project

	// Define the two dates

	// Output the result

	const changeHeightCard = () => {
		if (cardRef.current) {
			const heightCard = cardRef.current.clientHeight;
		}
	};

	const column1 = details?.slice(0, Math.ceil(details.length / 2));
	const column2 = details?.slice(Math.ceil(details.length / 2));
	return (
		<RFlex styleProps={{ justifyContent: "space-between" }}>
			<Card className={styles.r_card} style={{ borderBottom: `4px solid ${color}` }} key={id}>
				<CardImg className={styles.card_img} src={image ?? TestImg} alt={image} onClick={() => link && history.push(link)} />

				<RFlex
					className={styles.action_buttons}
					styleProps={{
						width: "100%",
						justifyContent: "space-between",
					}}
				>
					<RFlex>
						{categories ? (
							categories?.map((e) => (
								<div className={styles.sections} style={{ background: `${color}` }}>
									{e}
								</div>
							))
						) : categoryName ? (
							<div className={styles.sections} style={{ background: `${color}` }}>
								{categoryName}
							</div>
						) : (
							""
						)}
					</RFlex>

					{/* <RFlex>
            {rate ? (
              <div
                className={styles.section_stars}
                style={{ background: `${color}` }}
              >
                <FontAwesomeIcon icon={faStar} />
                {rate}
              </div>
            ) : (
              ""
            )}

            {actions && actions.length > 0 && (
              <UncontrolledDropdown direction="end">
                <DropdownToggle
                  aria-haspopup={true}
                  // caret
                  color="default"
                  data-toggle="dropdown"
                  nav
                  style={{
                    background: `${color}`,
                    borderRadius: "100%",
                    zIndex: "99999999",
                  }}
                >
                  <i
                    class="fa fa-ellipsis-v"
                    aria-hidden="true"
                    style={{ cursor: "pointer", color: "#fff" }}
                  ></i>
                </DropdownToggle>
                <DropdownMenu
                  persist
                  aria-labelledby="navbarDropdownMenuLink"
                  right
                >
                  {actions?.map((action, index) => (
                    <DropdownItem
                      key={action.id}
                      disabled={action.disabled ? action.disabled : false}
                      onClick={() => {
                        action.onClick();
                      }}
                    >
                      <RFlex
                        styleProps={{
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <i
                          style={{ color: action.color ?? "#000" }}
                          className={action?.icon}
                          aria-hidden="true"
                        />
                        <span style={{ color: action.color ?? "#000" }}>
                          {action.name}
                        </span>
                      </RFlex>
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </UncontrolledDropdown>
            )}
          </RFlex> */}
				</RFlex>

				<section ref={cardRef}>
					<RFlex
						styleProps={{
							justifyContent: "space-between",
							padding: " 10px",
						}}
					>
						<RFlex style={{ direction: "column" }}>
							{name && (
								<>
									<span className={styles.card_name} style={{ color: `${color}` }}>
										{name}
									</span>

									<div
										// className={styles.card_name}
										style={{ color: "black" }}
									>
										{lesson_name}
									</div>
								</>
							)}
							{/* {icon && (
                <img
                  className={styles.card_icon}
                  src={icon ?? TestIcon}
                  alt={icon}
                />
              )} */}
							{/* {isCloned && !hiddenFlag ? (
                <span
                  className="pt-1"
                  style={{ color: primaryColor, fontWeight: "bold" }}
                >{tr`cloned`}</span>
              ) : (
                ""
              )} */}
						</RFlex>
						{add && (
							<div style={{ display: "flex", alignItems: "center" }}>
								<FontAwesomeIcon icon={faPlusSquare} color={`${color}`} size="lg" id={`test`} />
							</div>
						)}
					</RFlex>

					{title && (
						<RFlex styleProps={{ padding: "0px 10px" }}>
							<p className="text-muted">{title}</p>
						</RFlex>
					)}

					{/* notes */}
					{notes?.map((n) => n)}

					{/* {currentInfo && currentInfo.length > 0 && (
            <Row>
              {currentInfo?.map((cI) => (
                <Col xs={6} className="d-flex mb-2" key={cI?.name ?? cI?.title}>
                  {cI.icon && <i className={cI.icon + " pt-1 pr-2 pl-0"} />}
                  <span>
                    {cI?.count ?? cI?.value} {tr(cI?.name ?? cI?.title)}
                  </span>
                </Col>
              ))}
            </Row>
          )} */}

					{progress && (
						<RFlex styleProps={{ padding: "0px 10px", alignItems: "center" }}>
							<Progress animated value={60} className={styles.progress_bar}>
								{progress}%
							</Progress>
							<h6>you have completed {Progress}%</h6>
						</RFlex>
					)}

					{users && users.length > 0 ? (
						<RFlex
							styleProps={{
								justifyContent: "space-between",
								alignItems: "center",
							}}
						>
							<div className={styles.imageDiv}>
								{users?.map((user, index) => {
									return (
										<div key={index}>
											<img
												src={user.image_hash_id == undefined ? TestImg : `${Services.storage.file}${user.image_hash_id}`}
												alt={user.image_hash_id}
												className={`${styles.userTypeImage} `}
												style={{
													zIndex: index,
													right: index * 20,
													position: "relative",
												}}
											/>
										</div>
									);
								})}
							</div>
							<div className="pl-4">
								{users?.map((user, index) => (
									<span key={index}>
										{user.user_name}
										{index !== users.length - 1 && ","}
									</span>
								))}
							</div>
						</RFlex>
					) : (
						<></>
					)}
				</section>

				<div>
					<Row style={{ paddingTop: "15px" }}>
						<Col md={6}>
							{column1?.map((item) => (
								<RLiveAction icon={item.icon} text={item.text} onClick={() => {}} />
							))}
						</Col>
						<Col md={6}>
							{column2?.map((item) => (
								<RLiveAction icon={item.icon} text={item.text} onClick={() => {}} />
							))}
						</Col>
					</Row>
				</div>
				{/* <RFlex styleProps={{ padding: "10px 10px" }}>
            <i
              aria-hidden="true"
              className={`fa fa-arrow-circle-o-down fa-lg ` + styles.pointer}
              onClick={() => changeHeightCard()}
            ></i>
          </RFlex> */}
			</Card>
		</RFlex>
	);
};

export default RCard1;
