import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { userLogsAsync } from "store/actions/global/track.action";

import {
  getStateMachineComponents,
  getStateMachineById,
  addValuesToStateMachine,
  addNewNode,
  removeNode,
  addStatusToNode,
  addStateMachine,
  editStateMachine,
  emptyStateMachine,
} from "store/actions/global/learningObjects";
import Loader from "utils/Loader";
import RStateMachine from "view/LearningObject/RStateMachine/RStateMachine";
import RSubmit from "components/Global/RComs/RSubmit";
import tr from "components/Global/RComs/RTranslator";

const GStateMachine = () => {
  const dispatch = useDispatch();
  const { sectionId, lessonId, lessonContentId } = useParams();

  const {
    learningObject,
    stateMachine,
    statuses,
    interactions,
    responses,
    interactionConstraintType,
    stateMachineComponentsLoading,
    test,
    addStateMachineLoading,
    editStateMachineLoading,
    stateMachineLoading,
  } = useSelector((state) => state.learningObjectRed);
  useEffect(() => {
    dispatch(
      getStateMachineComponents(
        sectionId ? "sections" : lessonId ? "lessons" : "lessons_contents"
      )
    );
  }, []);

  useEffect(() => {
    if (
      learningObject &&
      learningObject.state_machine_id &&
      learningObject.state_machine_id !== null
    ) {
      dispatch(getStateMachineById(learningObject.id));
    } else {
      dispatch(emptyStateMachine());
    }
  }, [learningObject]);

  //userLogs
  useEffect(() => {
    dispatch(
      userLogsAsync([
        {
          interface_name: "state machine",
          operation: "view",
          context: {
            id: sectionId ? sectionId : lessonId ? lessonId : lessonContentId,
            type: sectionId
              ? "sections"
              : lessonId
              ? "lessons"
              : "lesson_contents",
          },
        },
      ])
    );
  }, []);

  const handleAddValueToStateMachine = (name, value) => {
    dispatch(addValuesToStateMachine(name, value));
  };

  const handleAddNewNode = () => {
    dispatch(addNewNode());
  };

  const handleRemoveNode = (id) => {
    dispatch(removeNode(id));
  };

  const handleAddStatusToNode = (name, value, nodeId) => {
    dispatch(addStatusToNode(name, value, nodeId));
  };

  const handleAddStateMachine = () => {
    if (learningObject.state_machine_id) {
      dispatch(
        editStateMachine(stateMachine.id, stateMachine, learningObject.id)
      );
    } else {
      dispatch(addStateMachine(learningObject.id, stateMachine));
    }
  };

  return (
    <div>
      {stateMachineComponentsLoading ? (
        <Loader />
      ) : (
        <>
          {stateMachineLoading ? (
            <Loader />
          ) : (
            <>
              <RStateMachine
                stateMachine={stateMachine}
                statuses={statuses}
                interactions={interactions}
                responses={responses}
                handleAddValueToStateMachine={handleAddValueToStateMachine}
                handleAddNewNode={handleAddNewNode}
                handleRemoveNode={handleRemoveNode}
                handleAddStatusToNode={handleAddStatusToNode}
              />

              <RSubmit
                onClick={handleAddStateMachine}
                disabled={addStateMachineLoading ? true : false}
                value={tr`save`}
                loading={addStateMachineLoading}
              />
            </>
          )}
        </>
      )}
    </div>
  );
};

export default GStateMachine;
