import React, { useContext } from "react";
import { primaryColor, dangerColor } from "config/constants";
import { Table, Collapse } from "reactstrap";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { warningColor } from "config/constants";
import { FieldArray } from "formik";
import RCategorySection from "./RCategorySection";
import RNewStandards from "./RNewStandards";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCategoryCollapses = ({ arrayHelpers, category, categoryIndex }) => {
	const RubricData = useContext(RubricContext);

	const isOpen = !RubricData.openedCollapses.includes(category?.id ? category?.id : category?.fakeId);

	const CategoryId = category.id ? category.id : category.fakeId;
	return (
		<section
			key={CategoryId}
			onMouseEnter={() => RubricData.actionsOnHover(CategoryId)}
			onMouseLeave={() => RubricData.actionsOnHover(CategoryId)}
		>
			{!isOpen && (
				<RCategorySection
					arrayHelpers={arrayHelpers}
					category={category}
					categoryIndex={categoryIndex}
					isOpen={isOpen}
					CategoryId={CategoryId}
				/>
			)}

			<Collapse isOpen={isOpen}>
				<RCategorySection
					arrayHelpers={arrayHelpers}
					category={category}
					categoryIndex={categoryIndex}
					isOpen={isOpen}
					CategoryId={CategoryId}
				/>
				<Table bordered className="mb-0">
					<thead>
						<tr>
							<th style={{ background: "white", width: "320px" }}>
								{tr`standards`} ({category?.standards?.length})
							</th>
							<th style={{ background: "white", color: warningColor, textAlign: "center" }}>{tr`ratings`}</th>
						</tr>
					</thead>

					<tbody>
						{category &&
							category.standards &&
							category.standards.length > 0 &&
							category.standards.map((standard, standardIndex) => {
								return (
									<tr key={standard.id ? standard.id : standard.fakeId}>
										<td
											style={{ fontWeight: "bold", width: "320px" }}
											onMouseEnter={() => RubricData.actionsStandardOnHover(standard.id ? standard.id : standard.fakeId)}
											onMouseLeave={() => RubricData.actionsStandardOnHover(standard.id ? standard.id : standard.fakeId)}
										>
											<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
												<span style={{ color: "#818181" }}>
													{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}
												</span>
												{RubricData.isStandardHovered == "standardHover" + (standard.id ? standard.id : standard.fakeId) && (
													<FieldArray name={`categories[${categoryIndex}].standards`}>
														{(arrayHelpers) => {
															return (
																<RFlex>
																	<i
																		className={iconsFa6.delete + " cursor-pointer"}
																		style={{ color: dangerColor }}
																		onClick={() => arrayHelpers.remove(standardIndex)}
																	/>
																	<i
																		className={iconsFa6.pen + " cursor-pointer"}
																		style={{ color: primaryColor }}
																		onClick={() =>
																			RubricData.handleOpenStandardModal({
																				categoryIndex: categoryIndex,
																				standardIndex: standardIndex,
																				arrayHelpers: arrayHelpers,
																			})
																		}
																	/>
																</RFlex>
															);
														}}
													</FieldArray>
												)}
											</RFlex>
										</td>
										<td style={{ padding: "0px" }}>
											<Table className="mb-0">
												<tbody>
													<tr>
														{/* {standard?.ratings
															.sort((a, b) => a.score - b.score)
															.map((rate, ind, array) => (
																<td
																	key={rate.id ? rate.id : rate.fakeId}
																	style={{
																		color: "#818181",
																		border: "0px",
																		borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																	}}
																>
																	<div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
																		{rate.score ? <span>{rate.score}</span> : ""}
																		<span>{rate.title}</span>
																	</div>
																</td>
															))} */}

														{standard?.ratings.map((rate, ind, array) => (
															<td
																key={rate.id ? rate.id : rate.fakeId}
																style={{
																	color: "#818181",
																	border: "0px",
																	borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																}}
															>
																<div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
																	{rate.score ? <span>{rate.score}</span> : ""}
																	<span>{rate.title}</span>
																</div>
															</td>
														))}
													</tr>
												</tbody>
											</Table>
										</td>
									</tr>
								);
							})}
					</tbody>
				</Table>
			</Collapse>
			<section style={{ width: "fit-content" }}>
				<RNewStandards categoryIndex={categoryIndex} />
			</section>
		</section>
	);
};

export default RCategoryCollapses;
