import Helper from "../RComs/Helper";

export const TYPES = {
  INITIALIZE_ASSIGNMENT: "INITIALIZE_ASSIGNMENT",
  SAVE_ANSWER: "SAVE_ANSWER",
  GO_TO_NEXT_QUESTION: "GO_TO_NEXT_QUESTION",
  GO_TO_PREVIOUS_QUESTION: "GO_TO_PREVIOUS_QUESTION",
  GO_TO_NEXT_TOPIC: "GO_TO_NEXT_TOPIC",
  GO_TO_PREVIOUS_TOPIC: "GO_TO_PREVIOUS_TOPIC",
  GO_TO_NEXT_GROUP: "GO_TO_NEXT_GROUP",
  GO_TO_PREVIOUS_GROUP: "GO_TO_PREVIOUS_GROUP",
  DISABLE_NEXT_BUTTON: "DISABLE_NEXT_BUTTON",
  TOGGLE_PREVIOUS_BUTTON: "TOGGLE_PREVIOUS_BUTTON",
  SKIP_TOPIC: "SKIP_TOPIC",
  SKIP_LAST_TOPIC: "SKIP_LAST_TOPIC",
  SKIP_LAST_GROUP: "SKIP_LAST_GROUP",
  UN_SKIP_LAST_GROUP: "UN_SKIP_LAST_GROUP",

  UN_SKIP_TOPIC: "UN_SKIP_TOPIC",
  TOGGLE_SKIP_BUTTON: "TOGGLE_SKIP_BUTTON",
  TOGGLE_UN_SKIP_BUTTON: "TOGGLE_UN_SKIP_BUTTON",
  SAVE_TEACHER_FEEDBACK: "SAVE_TEACHER_FEEDBACK",
  SAVE_MULTI_TEACHER_FEEDBACK: "SAVE_MULTI_TEACHER_FEEDBACK",
  SAVE_SINGLE_QUESTION__MARK: "SAVE_SINGLE_QUESTION__MARK",
  SAVE_MULTI_QUESTION_MARK: "SAVE_MULTI_QUESTION_MARK",
  SAVE_FINAL_TEACHER_MARK: "SAVE_FINAL_TEACHER_MARK",
  SAVE_MULTI_FINAL_TEACHER_MARK: "SAVE_MULTI_FINAL_TEACHER_MARK",

  CHANGE_TOTAL_MARK: "CHANGE_TOTAL_MARK",
  CHANGE_MULTI_TOTAL_MARKS: "CHANGE_MULTI_TOTAL_MARKS",
  CHANGE_QUESTION_TREE: "CHANGE_QUESTION_TREE",
};

export const assignmentInitialState = {
  nextButtonDisabled: false,
  previousButtonDisabled: true,
  currentActiveGroup: 0,
  currentActiveTopic: 0,
  currentActiveQuestion: 0,
  RabId: null,
  StudentQsPatternId: null,
  Details: [],
  skipButtonDisabled: true,
  unSkipButtonDisabled: true,
  currentSkip: 0,
  currentGroupSkip: 0,
  skippedTopics: [],
  skippedGroups: [],
  assignmentReadMode: false,
  assignmentCorrectingMode: false,
  Feedback: "",
  multiFeedbacks: [],
  TotalMark: 0,
  TeacherMark: 0,
  teacherMultiMarks: [],
  questionsMarks: [],
  questionsMultiMarks: [],
  studentsTotalMarks: [],
};

export const assignmentReducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case TYPES.INITIALIZE_ASSIGNMENT:
      return {
        ...state,
        RabId: payload.rabId,
        StudentQsPatternId: payload.studentQsPatternId,
        currentSkip: payload.skip,
        currentGroupSkip: payload.skip,
        skipButtonDisabled: payload.currentGroupSkip === 0 ? true : false,
        assignmentReadMode: payload.isDone === 1 ? true : false,
        TotalMark: payload.totalMark,
        TeacherMark: payload.teacherMark,
        studentsTotalMarks: payload.studentsTotalMarks,
      };
    case TYPES.SAVE_ANSWER:
      if (state.Details.length === 0) {
        return { ...state, Details: [...state.Details, payload] };
      }

      const answerIndex = state.Details.findIndex(
        (answer) =>
          answer.QuestionId === payload.QuestionId &&
          answer.GroupId === payload.GroupId
      );

      if (answerIndex !== -1) {
        return {
          ...state,
          Details: [
            ...state.Details.slice(0, answerIndex),
            { ...payload },
            ...state.Details.slice(answerIndex + 1),
          ],
        };
      }
      return {
        ...state,
        Details: [...state.Details, payload],
      };

    case TYPES.GO_TO_NEXT_QUESTION:
      return {
        ...state,
        currentActiveQuestion: state.currentActiveQuestion + 1,
        previousButtonDisabled: false,
      };
    case TYPES.GO_TO_PREVIOUS_QUESTION:
      return {
        ...state,
        currentActiveQuestion: state.currentActiveQuestion - 1,
        nextButtonDisabled: false,
      };
    case TYPES.GO_TO_NEXT_TOPIC:
      return {
        ...state,
        currentActiveTopic: state.currentActiveTopic + 1,
        currentActiveQuestion: 0,
      };
    case TYPES.GO_TO_PREVIOUS_TOPIC:
      return {
        ...state,
        currentActiveTopic: state.currentActiveTopic - 1,
        currentActiveQuestion: payload,
        nextButtonDisabled: false,
      };
    case TYPES.GO_TO_NEXT_GROUP:
      return {
        ...state,
        currentActiveGroup: state.currentActiveGroup + 1,
        currentActiveQuestion: 0,
        currentActiveTopic: 0,
      };
    case TYPES.GO_TO_PREVIOUS_GROUP:
      return {
        ...state,
        currentActiveGroup: state.currentActiveGroup - 1,
        currentActiveQuestion: payload.question,
        currentActiveTopic: payload.topic,
      };
    case TYPES.DISABLE_NEXT_BUTTON: {
      return {
        ...state,
        nextButtonDisabled: true,
      };
    }
    case TYPES.TOGGLE_PREVIOUS_BUTTON: {
      return {
        ...state,
        previousButtonDisabled: payload,
      };
    }
    case TYPES.TOGGLE_SKIP_BUTTON: {
      const selectedGroup = state.skippedGroups.filter(
        (g) => g.groupId === payload.currentGroupId
      )[0];

      if (selectedGroup !== undefined) {
        return {
          ...state,
          currentSkip: selectedGroup.groupSkip,
          currentGroupSkip: payload.currentGroupSkip,
          skipButtonDisabled: selectedGroup.groupSkip > 0 ? false : true,
        };
      }
      return {
        ...state,
        currentSkip: payload.currentSkip,
        currentGroupSkip: payload.currentGroupSkip,
        skipButtonDisabled: payload.currentGroupSkip > 0 ? false : true,
      };
    }

    case TYPES.TOGGLE_UN_SKIP_BUTTON: {
      return {
        ...state,
        unSkipButtonDisabled: !state.skippedTopics.includes(
          payload.currentTopicId
        ),
        skipButtonDisabled: state.skippedTopics.includes(payload.currentTopicId)
          ? true
          : state.currentSkip === 0
          ? true
          : false,
      };
    }
    case TYPES.SKIP_TOPIC: {
      return {
        ...state,
        currentActiveTopic: state.currentActiveTopic + 1,
        currentActiveQuestion: 0,
        currentSkip: state.currentSkip - 1,
        skippedTopics: [...state.skippedTopics, payload.currentTopicId],
        skipButtonDisabled: state.currentSkip - 1 === 0 ? true : false,
        skippedGroups: [
          ...state.skippedGroups,
          { groupId: payload.currentGroupId, groupSkip: state.currentSkip - 1 },
        ],
      };
    }
    case TYPES.SKIP_LAST_TOPIC: {
      return {
        ...state,
        currentActiveTopic: 0,
        currentActiveQuestion: 0,
        currentActiveGroup: state.currentActiveGroup + 1,
        currentSkip: state.currentSkip - 1,
        skippedTopics: [...state.skippedTopics, payload.currentTopicId],
        skipButtonDisabled: state.currentSkip - 1 === 0 ? true : false,
        skippedGroups: [
          ...state.skippedGroups,
          { groupId: payload.currentGroupId, groupSkip: state.currentSkip - 1 },
        ],
      };
    }
    case TYPES.SKIP_LAST_GROUP: {
      return {
        ...state,
        // currentActiveTopic: 0,
        // currentActiveQuestion: 0,
        // currentActiveGroup: 0,
        currentSkip: state.currentSkip - 1,
        skippedTopics: [...state.skippedTopics, payload.currentTopicId],
        skipButtonDisabled: true,
        unSkipButtonDisabled: false,
        skippedGroups: [
          ...state.skippedGroups,
          { groupId: payload.currentGroupId, groupSkip: state.currentSkip - 1 },
        ],
      };
    }
    case TYPES.UN_SKIP_LAST_GROUP: {
      return {
        ...state,
        currentSkip: state.currentSkip + 1,
        skippedTopics: state.skippedTopics.filter(
          (skippedTopic) => skippedTopic !== payload.currentTopicId
        ),
        skippedGroups: state.skippedGroups.filter(
          (g) => g.groupId !== payload.currentGroupId
        ),
        unSkipButtonDisabled: true,
        skipButtonDisabled: false,
      };
    }

    case TYPES.UN_SKIP_TOPIC: {
      const selectedGroupIndex = state.skippedGroups.findIndex(
        (g) => g.groupId === payload.currentGroupId
      );

      return {
        ...state,
        currentActiveQuestion: 0,
        currentSkip: state.currentSkip + 1,
        skippedTopics: state.skippedTopics.filter(
          (skippedTopic) => skippedTopic !== payload.currentTopicId
        ),
        skippedGroups: state.skippedGroups.filter(
          (g) => g.groupId !== payload.currentGroupId
        ),
        unSkipButtonDisabled: state.currentGroupSkip !== state.currentSkip,
        skipButtonDisabled: state.currentSkip - 1 === 0 ? true : false,
      };
    }

    case TYPES.SAVE_TEACHER_FEEDBACK: {
      return {
        ...state,
        Feedback: payload,
      };
    }
    case TYPES.SAVE_MULTI_TEACHER_FEEDBACK: {
      const studentIndex = state.multiFeedbacks.findIndex(
        (s) => s.studentId === payload.studentId
      );
      if (studentIndex !== -1) {
        return {
          ...state,
          multiFeedbacks: [
            ...state.multiFeedbacks.slice(0, studentIndex),
            { ...payload },
            ...state.multiFeedbacks.slice(studentIndex + 1),
          ],
        };
      }
      return {
        ...state,
        multiFeedbacks: [...state.multiFeedbacks, payload],
      };
    }

    case TYPES.SAVE_SINGLE_QUESTION__MARK: {
      if (state.questionsMarks.length === 0) {
        return { ...state, questionsMarks: [...state.questionsMarks, payload] };
      }

      const questionIndex = state.questionsMarks.findIndex(
        (question) => question.QuestionId === payload.QuestionId
      );

      if (questionIndex !== -1) {
        return {
          ...state,
          questionsMarks: [
            ...state.questionsMarks.slice(0, questionIndex),
            { ...payload },
            ...state.questionsMarks.slice(questionIndex + 1),
          ],
        };
      }
      return {
        ...state,
        questionsMarks: [...state.questionsMarks, payload],
      };
    }
    case TYPES.SAVE_MULTI_QUESTION_MARK: {
      if (state.questionsMultiMarks.length === 0) {
        return {
          ...state,
          questionsMultiMarks: [...state.questionsMultiMarks, payload],
        };
      }

      const questionIndex = state.questionsMultiMarks.findIndex(
        (question) =>
          question.questionId === payload.questionId &&
          question.studentId === payload.studentId
      );

      if (questionIndex !== -1) {
        return {
          ...state,
          questionsMultiMarks: [
            ...state.questionsMultiMarks.slice(0, questionIndex),
            { ...payload },
            ...state.questionsMultiMarks.slice(questionIndex + 1),
          ],
        };
      }
      return {
        ...state,
        questionsMultiMarks: [...state.questionsMultiMarks, payload],
      };
    }

    case TYPES.SAVE_MULTI_FINAL_TEACHER_MARK: {
      const studentIndex = state.teacherMultiMarks.findIndex(
        (s) => s.studentId === payload.studentId
      );
      if (studentIndex !== -1) {
        return {
          ...state,
          teacherMultiMarks: [
            ...state.teacherMultiMarks.slice(0, studentIndex),
            { ...payload },
            ...state.teacherMultiMarks.slice(studentIndex + 1),
          ],
        };
      }
      return {
        ...state,
        teacherMultiMarks: [...state.teacherMultiMarks, payload],
      };
    }
    case TYPES.SAVE_FINAL_TEACHER_MARK: {
      return {
        ...state,
        TeacherMark: payload,
      };
    }
    case TYPES.CHANGE_TOTAL_MARK: {
      let questionIsCorrected = state.questionsMarks.filter(
        (q) => q.QuestionId === payload.question.id
      );

      if (questionIsCorrected[0] !== undefined) {
        let newMark =
          state.TotalMark - questionIsCorrected[0].Mark + payload.questionMark;

        return {
          ...state,
          TotalMark: newMark,
        };
      }
      if (payload.questionMark === payload.question.corrected_mark) {
        return state;
      }
      return {
        ...state,
        TotalMark:
          state.TotalMark -
          payload.question.corrected_mark +
          payload.questionMark,
      };
    }
    case TYPES.CHANGE_MULTI_TOTAL_MARKS: {
      let questionIsCorrected = state.questionsMultiMarks.filter(
        (q) =>
          q.questionId === payload.questionId &&
          q.studentId === payload.studentId
      );
      let studentTotalMarkIndex = state.studentsTotalMarks.findIndex(
        (s) => s.studentId === payload.studentId
      );
      let studentTotalMark = state.studentsTotalMarks.filter(
        (s) => s.studentId === payload.studentId
      )[0];

      //check if question is already corrected
      if (
        questionIsCorrected[0] !== undefined &&
        studentTotalMarkIndex !== -1 &&
        questionIsCorrected[0].Mark !== undefined &&
        payload.questionMark !== undefined
      ) {
        let newStudentTotalMark =
          studentTotalMark.totalMark -
          questionIsCorrected[0].Mark +
          payload.questionMark;

        return {
          ...state,
          studentsTotalMarks: [
            ...state.studentsTotalMarks.slice(0, studentTotalMarkIndex),
            { studentId: payload.studentId, totalMark: newStudentTotalMark },
            ...state.studentsTotalMarks.slice(studentTotalMarkIndex + 1),
          ],
        };
      }
      if (payload.questionMark === payload.student.corrected_mark) {
        return state;
      }
      if (payload.questionMark !== undefined) {
        let newStudentTotalMark =
          studentTotalMark.totalMark -
          payload.student.corrected_mark +
          payload.questionMark;

        return {
          ...state,
          studentsTotalMarks: [
            ...state.studentsTotalMarks.slice(0, studentTotalMarkIndex),
            { studentId: payload.studentId, totalMark: newStudentTotalMark },
            ...state.studentsTotalMarks.slice(studentTotalMarkIndex + 1),
          ],
        };
      }
      return state;
    }
    case TYPES.CHANGE_QUESTION_TREE: {
      if (!(payload && payload.type)) {
        return state;
      }
      if (payload.type === "group") {
        return {
          ...state,
          currentActiveGroup: payload.groupId,
          currentActiveTopic: 0,
          currentActiveQuestion: 0,
        };
      }
      if (payload.type === "topic") {
        return {
          ...state,
          currentActiveGroup: payload.groupId,
          currentActiveTopic: payload.topicId,
          currentActiveQuestion: 0,
        };
      }
      if (payload.type === "question") {
        return {
          ...state,
          currentActiveGroup: payload.groupId,
          currentActiveTopic: payload.topicId,
          currentActiveQuestion: payload.questionId,
        };
      }
    }
    default:
      return state;
  }
};
