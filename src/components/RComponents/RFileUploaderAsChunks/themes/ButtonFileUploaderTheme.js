import React from "react";
import styles from "../RFileUploaderAsChunks.module.scss";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

const ButtonFileUploaderTheme = ({ getRootProps, getInputProps, style, buttonText, buttonIcon, buttonColor, sizeFile }) => {
	return (
		<section>
			<div {...getRootProps({ ...style, display: "none" })} className="mb-2">
				<input {...getInputProps()} />
				<div className={styles.flex__column}>
					<RButton text={tr`${buttonText}`} faicon={buttonIcon} color={buttonColor} className="m-0 p-0 w-fit" />
				</div>
			</div>
		</section>
	);
};

export default ButtonFileUploaderTheme;
