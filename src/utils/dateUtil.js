import Helper from "components/Global/RComs/Helper";
import moment from "moment";

// Version 4.1
const refomatDate = (dateObject, fromFormat, toFormat) => { };

export const relativeDate = (date) => {
  const now = moment();
  const then = moment(date);

  // if (!then) return date;

  if (!then.isValid()) {
    return "-";
  }

  if (
    now.diff(then, "days") <
    7 /*|| (now.date() === then.date() && now.diff(then, "years") === 0) */ ||
    (now.date() === then.date() &&
      now.month() === then.month() &&
      now.diff(then, "years") > 0)
  ) {
    return then.fromNow();
  }
  return then.format("YYYY/MM/DD HH:mm") === "Invalid date"
    ? date
    : then.format("YYYY/MM/DD HH:mm");
};

export const getRelativeDate = (inputDate1) => {

  const parts = inputDate1?.split(".");

  const inputDate = parts?.[0] ?? inputDate1;



  const currentDate = moment();
  const providedDate = moment(inputDate);


  const duration =

    moment.duration(
      (currentDate > providedDate) ? currentDate.diff(providedDate) :
        providedDate.diff(currentDate))
    ;



  const years = duration.years();
  const months = duration.months();
  const days = duration.days();
  const hours = duration.hours();
  const minutes = duration.minutes();
  const seconds = duration.seconds();
  //return inputDate+ ":"+years +","+ months +","+ days +","+ hours +","+ minutes +","+ seconds ;

  let relativeDateString = "";

  let postfix = (currentDate > providedDate) ? "ago" : "left!";
  if (years > 0) {
    relativeDateString += years + (years === 1 ? " year " : " years ");
  }
  else if (years < 0) {
    relativeDateString += -1 * years + (years === -1 ? " year " : " years ");
    postfix = "left !";
  }
  else if (months > 0) {
    relativeDateString += months + (months === 1 ? " month " : " months ");
  }
  else if (months < 0) {
    relativeDateString += -1 * months + (months === -1 ? " month " : " months ");
    postfix = "left !";
  }
  else if (days > 0) {
    relativeDateString += days + (days === 1 ? " day " : " days ");
  }
  else if (days < 0) {
    relativeDateString += -1 * days + (days === -1 ? " day " : " days ");
    postfix = "left !";
  }

  else if (hours > 0) {
    relativeDateString += hours + (hours === 1 ? " hour " : " hours ");
  }
  else if (hours < 0) {
    relativeDateString += -1 * hours + (hours === -1 ? " hour " : " hours ");
    postfix = "left !";
  }
  else if (minutes > 0) {
    relativeDateString += minutes + (minutes === 1 ? " minute " : " minutes ");
  }
  else if (minutes < 0) {
    relativeDateString += -1 * minutes + (minutes === -1 ? " minute " : " minutes ");
    postfix = "left !";
  }
  else if (seconds > 0) {
    relativeDateString += seconds + (seconds === 1 ? " second " : " seconds ");
  }
  else if (seconds < 0) {
    relativeDateString += -1 * seconds + (seconds === -1 ? " second " : " seconds ");
    postfix = "left !";
  }
  return relativeDateString + postfix;
};

export const convertTimeToDate = (time) => {
  const currentDate = moment().format('YYYY-MM-DD');// Get Dummy date to concat the time to it
  const dateTimeString = moment(currentDate.concat(`T${time}`)).format(); //concat time and adding a timezone so we can convert to UTC
  const resultMoment = moment(dateTimeString).utc().format(); // convert to UTC
  return resultMoment
}
// export default pSBC;
