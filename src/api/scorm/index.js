import { get, post, patch } from "config/api";
import { Services } from "engine/services";
export const scormLMSAPI = {
    getScormEntry: (scormId/*, studentId*/) => get(`${Services.scorm.backend}api/scorm-aggregates/${scormId}`),// null, //assume this give me course entry point by course id and student history associated with this course
    /*get(`api/courses/course/${courseId}/student/${studentId}`)*/ //modify
    // getScormEntryForStudent: (scormId) => get(`/api/scorm-trackings/${scormId}`),
    commit: (courseId, studentId, payload) => post("", payload), //assumes this commits student tracking
    getCMIData: (courseId, userId) => get(""), //assumes this give me student progress
    // allScorms: (lessonId) => get(`api/lessons/${lessonId}/scorms`),
    uploadScormArchive: (lessonId, file) => post(`/api/lessons/${lessonId}/scorms`, file),
    exportLessonContentAsScorm: (lessonId) => post(`/api/scorms/export-lesson/${lessonId}`)

}