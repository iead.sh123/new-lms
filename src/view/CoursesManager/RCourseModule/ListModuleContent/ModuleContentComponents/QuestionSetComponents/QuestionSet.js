import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

const QuestionSet = ({ type }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<Row className="text-center mt-4" style={{ color: primaryColor, width: "100%" }}>
			<Col xs={12} sm={4}>
				<div onClick={() => CourseModuleData.handlePickFromCollaboration(type?.toLowerCase())}>
					<i className="fa fa-globe fa-2x mb-3" />
					<p>{tr`pick_from_collaboration`}</p>
				</div>
			</Col>
			<Col xs={12} sm={4}>
				<div onClick={() => CourseModuleData.handleOpenPickFromCourseModal(type?.toLowerCase())}>
					<i className="fa fa-list-ul fa-2x mb-3" />
					<p>{tr`pick_from_this_course`}</p>
				</div>
			</Col>
			<Col xs={12} sm={4}>
				<div onClick={() => CourseModuleData.handleCreateQuestionSet(type?.toLowerCase())}>
					<i className="fa fa-plus fa-2x mb-3" />
					<p>{tr`create_new`}</p>
				</div>
			</Col>
		</Row>
	);
};

export default QuestionSet;
