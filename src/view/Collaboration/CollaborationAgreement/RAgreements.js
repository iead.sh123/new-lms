import React from "react";
import CollaborationAgreementCard from "./CollaborationAgreementCard";
import styles from "../Collaboration.module.scss";
import RTitle from "components/RComponents/RTitle";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RAgreements = ({ data, handlers }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RTitle
				text={tr`my_clients`}
				count={0}
				actionHandler={{ handleClick: handlers.handleOpenEditorAgreement, name: tr`add_agreement` }}
			/>
			<RFlex className={styles.container__rectangle__card} styleProps={{ flexWrap: "wrap" }}>
				{data?.content?.map((item) => (
					<CollaborationAgreementCard key={item.id} item={item} handlers={handlers} />
				))}
			</RFlex>
		</RFlex>
	);
};

export default RAgreements;
