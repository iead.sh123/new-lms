import React, { useState } from "react";
import { NavItem, NavLink } from "reactstrap";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { paymentApi } from "api/global/payment";
import CartContent from "./CartContent";
import iconsFa6 from "variables/iconsFa6";
import Drawer from "react-modern-drawer";
import styles from "../Navbar.module.scss";
import "react-modern-drawer/dist/index.css";

const CartDrawer = () => {
	const cartItems = useFetchDataRQ({ queryKey: ["cart-items"], queryFn: () => paymentApi.fetchCartItems() });

	const [isOpen, setIsOpen] = useState(false);

	const toggleDrawer = () => {
		setIsOpen((prevState) => !prevState);
	};

	return (
		<NavItem>
			<NavLink style={{ position: "relative" }} className="btn-magnify" href="#" onClick={toggleDrawer}>
				<i className={iconsFa6.cart + " fa-lg"} />
				{cartItems?.data?.data?.data?.length > 0 && (
					<span className={styles.dots}>
						{cartItems?.data?.data?.data?.length > 99
							? "+" + `${cartItems?.data?.data?.data?.length}`
							: cartItems?.data?.data?.data?.length}
					</span>
				)}
			</NavLink>

			<Drawer open={isOpen} onClose={toggleDrawer} direction="right" className={styles.drawer_cart}>
				<CartContent toggleDrawer={toggleDrawer} />
			</Drawer>
		</NavItem>
	);
};

export default CartDrawer;
