import React, { useState, useCallback } from "react";
import { resourcesBaseUrl } from "config/constants";
import QuestionMark from "./QuestionMark";
import DrawQuestion from "./QuestionType/DrawQuestion";
import FreeTextQuestion from "./QuestionType/FreeTextQuestion";
import MatchDragQuestion from "./QuestionType/MatchQuestion/MatchQuestionDrag";
import MatchQuestion from "./QuestionType/MatchQuestion/MatchQuestion";
import MultiAnswerQuestion from "./QuestionType/MultiAnswerQuestion";
import SingleAnswerQuestion from "./QuestionType/SingleAnswerQuestion";
import UploadFileQuestion from "./QuestionType/UploadFileQuestion";
import MathJax from "react-mathjax-preview";
import ImageEnlarge from "utils/ImageEnlarge";
import { translate } from "../../../utils/translate";
import StudentModal from "./StudentModal";

export const QUESTION_TYPE = {
  SINGLE_ANSWER_MC: "single_answer_mc",
  MULTIPLE_ANSWER_MC: "multiple_answer_mc",
  FREE_TEXT: "free_text",
  MATCH: "match",
  DRAW: "draw",
  UPLOAD_FILE: "upload_file",
};

const QuestionsMultiCorrect = ({
  question,
  assignmentState,
  groupId,
  dispatch,
  assignmentReadMode,
  mode,
  questionsMultiMarks,
  currentActiveQuestion,
  topicLength,
  lessonGroups,
  studentsTotalMarks,
  isLastQuestion,
  teacherMultiMarks,
  multiFeedbacks,
}) => {
  const [studentModalShow, setStudentModalShow] = useState(false);
  const [currentStudent, setCurrentStudent] = useState({});

  const questionProps = {
    answers: assignmentState.Details,
    choices: question.inst_choices,
    groupId: groupId,
    dispatch: dispatch,
    question: question,
    assignmentReadMode: assignmentReadMode,
    mode: mode,
  };

  const getStudentFeedback = useCallback(() => {
    if (currentStudent.id in lessonGroups.feedbacks)
      return lessonGroups.feedbacks[currentStudent.id];
  }, [currentStudent]);

  const questionTypeSwitch = (question, student) => {
    switch (question.question_type) {
      case QUESTION_TYPE.SINGLE_ANSWER_MC:
        return <SingleAnswerQuestion {...questionProps} student={student} />;
      case QUESTION_TYPE.MULTIPLE_ANSWER_MC:
        return <MultiAnswerQuestion {...questionProps} student={student} />;
      case QUESTION_TYPE.FREE_TEXT:
        return <FreeTextQuestion {...questionProps} student={student} />;
      case QUESTION_TYPE.MATCH:
        return assignmentReadMode || mode === "teacher" || mode === "multi" ? (
          <MatchQuestion {...questionProps} student={student} />
        ) : (
          <MatchDragQuestion {...questionProps} student={student} />
        );
      case QUESTION_TYPE.DRAW:
        return <DrawQuestion {...questionProps} student={student} />;
      case QUESTION_TYPE.UPLOAD_FILE:
        return <UploadFileQuestion {...questionProps} student={student} />;
      default:
        return <p>None</p>;
    }
  };
  return (
    <div className="row mt-2 d-flex align-items-center justify-content-center assignment_question_container">
      <div className="col-md-10 pt-2">
        <div className="form-group row">
          <label className="col-sm-2  assignment_question_label">
            {translate("Question")}
          </label>
          <div className="col-sm-8">
            {question.question_text &&
            question.question_text.includes("<math") ? (
              <MathJax math={question.question_text} />
            ) : (
              <p
                className="assignment_question_text"
                dangerouslySetInnerHTML={{ __html: question.question_text }}
              />
            )}
          </div>

          <div className="col-sm-2">
            <p className="QS-p assignment_questionCounter">
              {currentActiveQuestion + 1} / {topicLength}
            </p>
          </div>
          {question.attachments && (
            <div className="col-sm-10" style={{ textAlign: "center" }}>
              {["png", "jpeg"].includes(question.attachments.mime_type) ? (
                <ImageEnlarge
                  imageSrc={resourcesBaseUrl + question.attachments.url}
                />
              ) : (
                <a
                  url={resourcesBaseUrl + question.attachments.url}
                  alt="Download"
                  class="btn btn-primary"
                >
                  Download
                </a>
              )}
            </div>
          )}
        </div>
      </div>
      <div className="col-md-10">
        <div className="form-group row">
          <label className="col-md-6 col-form-label">
            {translate("Question mark")}
          </label>
          <p className="QS-p"> {question.mark.toFixed(2)}</p>
        </div>
      </div>
      <div className="col-md-10">
        <div className="form-group row">
          <label className="col-md-12 col-form-label assignment_question_label">
            {translate("Answers")}
          </label>
          {React.Children.toArray(
            Object.entries(question.students).map((student) => (
              <div className="assignment_multi_answer col-md-12">
                <div className="row">
                  <p className="assignment_question_label"> Student Name:</p>
                  <p
                    onClick={() => {
                      setCurrentStudent({ ...student[1], id: student[0] });
                      setStudentModalShow(true);
                    }}
                    className="assignment_student_name ml-3"
                  >
                    {student[1].student_name}{" "}
                  </p>
                </div>
                {questionTypeSwitch(question, student[1])}
                <QuestionMark
                  question={question}
                  student={student}
                  mode={mode}
                  dispatch={dispatch}
                  questionsMultiMarks={questionsMultiMarks}
                />
              </div>
            ))
          )}
        </div>
      </div>
      <StudentModal
        show={studentModalShow}
        onHide={() => setStudentModalShow(false)}
        student={currentStudent}
        mode={mode}
        dispatch={dispatch}
        studentFeedback={getStudentFeedback(currentStudent.id)}
        studentsTotalMarks={studentsTotalMarks}
        isLastQuestion={isLastQuestion}
        teacherMultiMarks={teacherMultiMarks}
        multiFeedbacks={multiFeedbacks}
      />
    </div>
  );
};

export default QuestionsMultiCorrect;
