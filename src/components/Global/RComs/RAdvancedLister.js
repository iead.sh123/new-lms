import React, { useState, useEffect } from "react";
import GenericFilter from "logic/GenericUi/GenericFilter";
import withDirection from "hocs/withDirection";
import RPaginator from "components/Global/RComs/RPaginator";
import RLister from "components/Global/RComs/RLister";
import RFilter from "components/Global/RComs/RFilter";
import RSubmit from "./RSubmit";
import Loader from "utils/Loader";
import tr from "./RTranslator";
import RNewFilter from "./RNewFilter/RNewFilter";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const RAdvancedLister = ({
	getDataFromBackend,
	setData,
	filterItems,
	records,
	getDataObject,
	actionButtons,
	title,
	actionButtonsPick,
	submitValue,
	getCurrentPage,
	getLastPage,
	getFirstPageUrl,
	getLastPageUrl,
	getNextPageUrl,
	getPrevPageUrl,
	getTotal,
	table_props,
	showListerMode,
	localnewfilter,
	filterdispatch,
	actionFormData,
	handelSaveFormData,
	table_name,
	dir1,
	dir,
	hideTableHeader = false,
	firstCellImageProperty,
	align = "center",
	staticFilter,
	localFilter,
	newFilterdispatch,
	SpecialCard,
	perLine,
	swiper,
	refresh,
	overflow,
	overflowX,
	cardsJustifyContents,
	usingPaginator = true,
	noBorders,
}) => {
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState(null);
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [loader, setLoader] = useState(null);

	const [mode, setMode] = useState(true);

	const changeModeAdvancedLister = () => {
		setMode(!mode);
	};

	const mounted = React.useRef(false);

	const getData = async (specific_url) => {
		setLoader(true);
		const response = await getDataFromBackend(specific_url);

		if (response) {
			mounted.current && handleResponse(response);
		} else {
			setLoader(false);
		}
	};

	const direction = dir1 ?? dir ?? "flex-start";
	//we used table_name in dependence to match a collaboration page
	useEffect(() => {
		mounted.current = true;
		getData();
		return () => {
			mounted.current = false;
		};
	}, [table_name, refresh]);

	const handleResponse = (response) => {
		if (response?.data?.status === 1) {
			setLoader(false);
			const dataObject = getDataObject(response);

			if (dataObject) {
				setCurrentPage(getCurrentPage && typeof getCurrentPage == "function" ? getCurrentPage(dataObject) : dataObject.current_page);
				setLastPage(getLastPage && typeof getLastPage == "function" ? getLastPage(dataObject) : dataObject.last_page);
				setFirstPageUrl(getFirstPageUrl && typeof getFirstPageUrl == "function" ? getFirstPageUrl(dataObject) : dataObject.first_page_url);
				setLastPageUrl(getLastPageUrl && typeof getLastPageUrl == "function" ? getLastPageUrl(dataObject) : dataObject.last_page_url);
				setNextPageUrl(getNextPageUrl && typeof getNextPageUrl == "function" ? getNextPageUrl(dataObject) : dataObject.next_page_url);
				setPrevPageUrl(getPrevPageUrl && typeof getPrevPageUrl == "function" ? getPrevPageUrl(dataObject) : dataObject.prev_page_url);
				setTotal(getTotal && typeof getTotal == "function" ? getTotal(dataObject) : dataObject.total);
			}
			setData(response);
		} else {
			if (response?.data?.msg) {
				setLoader(false);
				toast.error(response.data.msg);
			}
		}
	};

	return (
		<div className={"content "} style={{ width: "100%" }}>
			{
				<>
					{actionButtons && actionButtons?.length > 0 ? (
						<div
							className="row"
							style={{
								display: "flex",
								justifyContent: direction ? direction : "flex-end",
							}}
						>
							{actionButtons.map((a) => (
								<RSubmit
									key={Math.random()}
									value={a.text}
									icon={a.icon}
									className={a.className}
									onClick={a.onClick}
									disabled={a.disabled ? a.disabled : false}
									type="button"
									dir={direction}
									style={{ borderRadius: "2px", borderColor: "#668ad7" }}
								/>
							))}
						</div>
					) : null}

					{staticFilter && staticFilter.length > 0 && (
						<RNewFilter getData={getData} staticFilter={staticFilter} localFilter={localFilter} newFilterdispatch={newFilterdispatch} />
					)}

					{loader ? (
						Loader()
					) : (
						<>
							{filterItems && filterItems?.length > 0 ? (
								<RFilter
									onFilterClicked={() => {
										getData();
									}}
									title={title}
									filterItems={filterItems}
									submitValue={submitValue}
								/>
							) : null}
							{table_props && table_props?.length > 0 ? (
								<GenericFilter
									table_props={table_props}
									getData={getData}
									localnewfilter={localnewfilter}
									filterdispatch={filterdispatch}
									// filterField={filterField}
									// setFilterField={setFilterField}
									// filterValue={filterValue}
									// setFilterValue={setFilterValue}
								/>
							) : null}

							{records?.length == 0 ? (
								<div
									style={{
										display: "flex",
										justifyContent: "center",
										textAlign: "center",
										width: "100%",
									}}
								>
									<REmptyData line1={tr`no_data_to_show`} />
								</div>
							) : (
								<>
									<RLister
										Records={records}
										showListerMode={showListerMode}
										modeRAdvancedLister={mode}
										triggerRAdvancedLister={changeModeAdvancedLister}
										triggerRAdvancedListerStatus={true}
										actionFormData={actionFormData}
										handelSaveFormData={handelSaveFormData}
										hideTableHeader={hideTableHeader}
										firstCellImageProperty={firstCellImageProperty}
										align={align}
										SpecialCard={SpecialCard}
										perLine={perLine}
										swiper={swiper}
										overflow={overflow}
										overflowX={overflowX}
										cardsJustifyContents={cardsJustifyContents}
										noBorders={noBorders}
									/>
									{total && usingPaginator && (
										<RPaginator
											firstPageUrl={firstPageUrl}
											lastPageUrl={lastPageUrl}
											currentPage={currentPage}
											lastPage={lastPage}
											prevPageUrl={prevPageUrl}
											nextPageUrl={nextPageUrl}
											getData={getData}
										></RPaginator>
									)}
								</>
							)}
						</>
					)}

					<div className="row">
						{actionButtonsPick && actionButtonsPick?.length > 0
							? actionButtonsPick.map((a) => (
									<RSubmit key={Math.random()} value={a.text} icon={a.icon} className={a.className} onClick={a.onClick} dir={direction} />
							  ))
							: null}
					</div>
				</>
			}
		</div>
	);
};

export default withDirection(RAdvancedLister);
