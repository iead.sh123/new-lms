import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMonthlyTraitAsync } from "store/actions/global/quote.action";
import Loader, { LIST, ELLIPSIS } from "utils/Loader";
import RQuote from "../RComs/RText/RQuote";

const GMonthlyTrait =  ({basicStyle=false,minHeight}) => {
  const dispatch = useDispatch();
  const { monthlyTrait, monthlyTrait_Loading } = useSelector(
    (state) => state.quoteServiceRed
  );

  useEffect(() => {
    dispatch(getMonthlyTraitAsync());
  }, []);
  return (
    <>
      {monthlyTrait_Loading
        ? Loader(ELLIPSIS)
        : monthlyTrait !== null && (
            <RQuote
              color={monthlyTrait.color}
              image={monthlyTrait.image}
              text={monthlyTrait.text}
              author={monthlyTrait.author ? monthlyTrait.author : ""}
              basicStyle={basicStyle}
              minHeight={"100%"}
            />
          )}
    </>
  );
};

export default GMonthlyTrait;
