import React from "react";
import { Input } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RSelect from "components/Global/RComs/RSelect";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
const RUserTypeSettings = ({
	values,
	errors,
	touched,
	handleBlur,
	handleChange,
	review,
	showSettings = true,
	handleSelectLevel,
	savedSettingsValues,
	handleAddingAnotherLevel,
	levels,
	handleSetDefault,
	view,
}) => {
	console.log("errorserrors", errors);
	console.log("touchedtouched", touched);
	return (
		<RFlex className="flex-column">
			<RFlex className="flex-column">
				{review && !savedSettingsValues.userTypeName && showSettings && (
					<span className="p-0 m-0" style={{ fontWeight: "bold", width: "fit-content" }}>
						{tr("settings")}
					</span>
				)}
				<RFlex className="flex-column">
					<RFlex className="align-items-center" styleProps={{ width: "100%" }}>
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("User_Type_Title")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.userTypeName ?? "-"}
							</span>
						) : (
							<Input
								name="userTypeName"
								placeholder={tr("user_type_title")}
								className={`${errors.userTypeName && touched.userTypeName ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.userTypeName}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
					<RFlex className="align-items-center">
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Description")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.description ?? "-"}
							</span>
						) : (
							<Input
								name="description"
								className={`${errors.description && touched.description ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
			{!review && !view && (
				<RFlex className="flex-column" styleProps={{ gap: "0px" }}>
					{levels.map((level) => (
						<RFlex className="align-items-center">
							<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
								{tr("Level")}
							</span>
							<div style={{ width: "591px" }}>
								<RSelect
									option={[]}
									closeMenuOnSelect={true}
									value={null}
									placeholder={tr("choose_level")}
									onChange={(e) => handleSelectLevel(e)}
								/>
							</div>
						</RFlex>
					))}
					<RButton
						text={tr("Add_another_level")}
						faicon={iconsFa6.plus}
						color="link"
						className="m-0 text-primary"
						style={{ width: "fit-content", height: "fit-content" }}
						onClick={() => handleAddingAnotherLevel()}
					/>
				</RFlex>
			)}
			{!review && !view && (
				<AppNewCheckbox
					label={tr("Set_as_default_type_for_manual_registering")}
					onChange={() => {
						handleSetDefault();
					}}
					paragraphStyle={{ marginBottom: "70px" }}
				/>
			)}
		</RFlex>
	);
};

export default RUserTypeSettings;
