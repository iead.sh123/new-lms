import { get, post, destroy } from "config/api";

export const adminTermInitiationApi = {
  postWeeklySchedule: (data) =>
    post(`emp-api/weekly-schedule-edit/store-grade-ws`, data),

  deleteWeeklyScheduleElement: (elementId) =>
    destroy(`emp-api/weekly-schedule-edit/${elementId}`),

  getWeeklyScheduleGrade: (gradeId) =>
    get(`emp-api/weekly-schedule-edit/get-grade-ws/${gradeId}`),

  getWeeklyScheduleElements: (gradeId) =>
    get(`emp-api/weekly-schedule-edit/element-type/${gradeId}`),

  getWeeklySchedule: () =>
    get(`emp-api/weekly-schedule-edit/get-WeeklySchedule`),

  getWeeklyScheduleDays: () => get(`emp-api/weekly-schedule-edit/get-days`),

  getWeeklySchedulePeriod: () =>
    get(`emp-api/weekly-schedule-edit/get-periods`),

  getWeeklyScheduleGrades: () => get(`emp-api/weekly-schedule-edit/get-grades`),

  getWeeklyScheduleCourse: (gradeId) =>
    post(`emp-api/weekly-schedule-edit/get-grade-courses`, gradeId),

  getWeeklyScheduleClass: (courseId) =>
    post(`emp-api/weekly-schedule-edit/get-course-rabs`, courseId),

  addWeeklySchedule: (data) => post(`emp-api/weekly-schedule-edit/store`, data),

  deleteWeeklySchedule: (WeeklyScheduleId) =>
    destroy(`emp-api/weekly-schedule-edit/${WeeklyScheduleId}`),

  getMoveTermMarks: () => get(`emp-api/term-finalize/get-move-marks`),

  doMoveTermMarks: () => get(`emp-api/term-finalize/do-move-marks`),

  studentAssess: () => get(`emp-api/term-finalize/assess`),

  passStudent: () => get(`emp-api/term-finalize/pass-students`),

  DonePassStudent: (request) =>
    post(`emp-api/term-finalize/pass-students`, request),

  // termGetFlag: () => get(`emp-api/term-finalize/flags`),
  termGetFlag: () => post(`emp-api/term-finalize/finalize`),

  lockClass: (gradeId) =>
    get(`emp-api/flag/post-Grade-Pass-Students-Flag/${gradeId}`),

  getWeeklyScheduleViewer: (gradeId) =>
    get(`emp-api/weekly-schedule-edit/get-grade-ws/${gradeId}`),

  getWeeklyScheduleValidate: () => get(`emp-api/weekly-schedule-edit/validate`),

  getAllStudentClasses: () => post(`emp-api/term/branched-school-classes`),

  getStudentWithBranches: (courseId) =>
    get(`emp-api/term/students-branched/${courseId}`),

  getTermInitiate: () => get(`/emp-api/term/initiate`),

  saveTermInitiation: (payload) => post(`/emp-api/term/initiate`, payload),

  getCreateCourses: () => get(`/emp-api/term/create-Rabs`),

  saveCreateCourses: (payload) => post(`/emp-api/term/create-Rabs`, payload),

  getAssignTeachers: () => get(`emp-api/term/assign-teachers`),

  saveAssignTeachers: (payload) =>
    post(`emp-api/term/assign-teachers`, payload),

  getAssignStudentsToRabs: () => get(`emp-api/term/assign-students`),

  saveAssignStudentsToRabs: (payload) =>
    post(`emp-api/term/assign-students-rabs`, payload),

  closeGrade: (grade_id) =>
    get(
      `/emp-api/flag/post-Grade-CreateCourseFlag-AssignTeacherFlag-AssignStudentFlag/${grade_id}`
    ),

  getActivateTerm: () => get(`/emp-api/term/activate-term`),

  postAssignStudentToBranches: (data) =>
    post(`emp-api/term/assign-students-BranchedSchoolClassess`, data),

  postActivateTerm: () => post(`/emp-api/term/activate-term`),

  getGradesBranchesStudents: () =>
    get(
      `/emp-api/flag/get-GradesBranches-Students-AssignStudentsToBranchesFlag`
    ),

  getAssignStudentsToBranchesFlag: (gradeId) =>
    get(`/emp-api/flag/post-Grade-AssignStudentsToBranchesFlag/${gradeId}`),

  getGradesWeeklySchedule: () =>
    get(`/emp-api/flag/get-Grades-WeeklyScheduleFlag`),

  getWeeklyScheduleFlag: (gradeId) =>
    get(`/emp-api/flag/post-Grade-Create-WeeklySchedule-Flag/${gradeId}`),
};
