import React, { useState, useMemo } from "react";
import { pickALessonPlanToUnitPlanSection } from "store/actions/global/unitPlan.actions";
import { versionServiceRubric } from "engine/config";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import { Row, Col } from "reactstrap";
import { post } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import styles from "./LessonPlan.module.scss";
import { toast } from "react-toastify";

const GPickLessonPlanLister = ({
	handleAddItems,
	handleClosePickALessonPlan,
	openPickALessonPlan,
	sectionId,
	itemId,
	typeToPick,
	lessonContentId,
	loading,
}) => {
	const dispatch = useDispatch();
	const { sectionId: sId, lessonId: lId, courseId: cId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [lessonPlanToLessonContent, setLessonPlanToLessonContent] = useState({
		id: null,
		destination_table: "",
		destination_id: "",
	});

	const emptyState = () => {
		setLessonPlanToLessonContent({
			id: null,
			destination_table: "",
			destination_id: "",
		});
	};

	const getDataFromBackend = async (specific_url) => {
		const Request = {
			payload: { course_id: cId },
		};
		let QueryReq = "";

		if (typeToPick == "lessonContent") {
			QueryReq = `?published=${true}&withPagination=${true}`;
		} else if (typeToPick == "item") {
			QueryReq = `?published=${true}&withPagination=${true}`;
		}

		const url = `${Services.lesson_plan.backend}api/${versionServiceRubric}/lesson-plan/get${QueryReq}`;

		let response = await post(specific_url ? specific_url : url, Request);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.LessonPlans?.data.map((r) => ({
			id: r.id,
			Titles: [r.title],
			table_name: "pickALessonPlanLister",

			details: [{ key: tr`name`, value: r?.name }],
			actions: [
				{
					name: tr("pick"),
					icon: "fa fa-check",
					color: typeToPick == "lessonContent" ? (r.id == lessonPlanToLessonContent.id ? "success" : "primary") : "primary",
					outline: typeToPick == "lessonContent" ? (r.id == lessonPlanToLessonContent.id ? false : true) : false,

					onClick: () => {
						if (typeToPick == "item") {
							// handleAddItems(sectionId, itemId, "lesson_plan", r, "parent", true);
							handleAddItems(sectionId, itemId, "lesson_plan_id", r.id, "parent", true);
						} else if (typeToPick == "course") {
							dispatch(pickALessonPlanToUnitPlanSection(r, cId));
						} else if (typeToPick == "lessonContent") {
							setLessonPlanToLessonContent({
								id: r?.id,
								destination_table: "lessons",
								destination_id: lessonContentId,
							});
						} else {
							return;
						}
					},
				},
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.LessonPlans,
			},
		}),
		[processedRecords, lessonPlanToLessonContent]
	);

	return (
		<AppModal
			show={openPickALessonPlan}
			parentHandleClose={handleClosePickALessonPlan}
			header={
				<>
					<span className="mr-2">{tr`pick_a_lesson_plan`} </span>
					{typeToPick == "lessonContent" &&
						(loading ? (
							<i className="fa fa-refresh fa-spin" />
						) : (
							<RButton
								className={styles.save__button}
								text={tr`save`}
								color="link"
								onClick={() => handleAddItems(lessonPlanToLessonContent, emptyState)}
								disabled={!lessonPlanToLessonContent.id}
							/>
						))}
				</>
			}
			size={"lg"}
			headerSort={
				<Row>
					<Col xs={12} className="d-flex justify-content-end"></Col>
					<Col xs={12}>
						<RAdvancedLister {...propsLiterals.listerProps} />
					</Col>
				</Row>
			}
		/>
	);
};

export default GPickLessonPlanLister;
