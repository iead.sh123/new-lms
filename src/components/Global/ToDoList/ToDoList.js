import {
  Button,
  Card,
  CardBody,
  CardFooter,
  FormGroup,
  Input,
  Label,
  Table,
  UncontrolledTooltip,
} from "reactstrap";
import styles from "./ToDoList.Module.scss";
import Loader, { ELLIPSIS } from "utils/Loader";
import { useHistory } from "react-router-dom";
import tr from "../RComs/RTranslator";

const ToDoList = ({
  lists,
  parentChecked,
  parentMakeAsDone,
  noAction = false,
  type,
  loader,
  chekedTask,
}) => {
  const history = useHistory();
  return loader?.loaderToDO ? (
       Loader(ELLIPSIS)
  ) : (
    <div style={{height:"400px"}}>
      <Button
        disabled={chekedTask?.length != 0 ? false : true}
        hidden={noAction}
        onClick={parentMakeAsDone}
        className={
          "btn-round btn-icon btn-icon-mini btn-neutral float-left ml-3 " +
          styles.m_t
        }
        color="info"
        id={type == 1 ? "tooltipUnDo" : "tooltipDone"}
        title=""
        type="button"
      >
        {type == 1 ? (
          <i className="nc-icon nc-simple-remove" />
        ) : (
          <i className="nc-icon nc-ruler-pencil" />
        )}
      </Button>

      <UncontrolledTooltip
        delay={0}
        target={type == 1 ? "tooltipUnDo" : "tooltipDone"}
      >
        {type == 1 ? tr`mark_as_undo` : tr`mark_as_done`}
      </UncontrolledTooltip>

      <div
        className={
          styles.table_responsive + " table-full-width table-responsive"
        }

        style={{minHeight:"200px",maxHeight:"400px",height:"250px"}}
      >
        {" "}
        <Card className="card-tasks mr-2">
          <CardBody>
            <Table>
              <tbody>
                {lists?.data?.map((list, index) => {
                  return (
                    <tr
                      className="cursor-pointer"
                      key={index}
                      hidden={list.done == type || type == 2 ? false : true}
                    >
                      <td>
                        {!noAction ? (
                          <FormGroup check>
                            <Label check>
                              <Input
                                type="checkbox"
                                onChange={() => {
                                  parentChecked(list.id);
                                }}
                              />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        ) : list.done ? (
                          <i
                            className="fa fa-check-circle-o text-success"
                            aria-hidden="true"
                          ></i>
                        ) : null}
                      </td>
                      <td
                        className="text-left float-left cursor-pointer "
                        onClick={() => {
                          if (list.to_do_item_type_id == 1) {
                            history.push(
                              `${process.env.REACT_APP_BASE_URL}/teacher/courses/${list.parameters[0].value}/rabs/${list.parameters[1].value}/lessons/${list.parameters[2].value}/Lesson%20Details`
                            );
                          } else {
                            history.push(
                              `${process.env.REACT_APP_BASE_URL}/teacher/courses/${list.parameters[0].value}/${list.parameters[1].value}/lessons/${list.parameters[2].value}/mark/${list.parameters[3].value}`
                            );
                          }
                        }}
                      >
                        {list?.subject}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
      <Card
        className=" mr-3 mb-1 "
        style={{ marginTop: "1px", height: "10px" }}
      >
        <CardFooter></CardFooter>
      </Card>
    </div>
  );
};
export default ToDoList;
