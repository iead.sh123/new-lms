import React from "react";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import styles from "../../ROrganization.module.scss";
import { primaryColor, dangerColor, lightGray, boldGreyColor } from "config/constants";

const RGeneralDiscountDetails = ({ discountDetails }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<AppNewCheckbox label={tr`the whole organization`} paragraphStyle={{ color: boldGreyColor }} checked={true} disabled={true} />
			<label style={{ color: lightGray }}>{tr("once you choose this you can’t change it")}</label>
			<RFlex>
				<span style={{ color: boldGreyColor }}>{tr`how_does_this_discount_work`}?</span>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<AppRadioButton
						name={"applyType"}
						label={tr`replace_with_other_discounts`}
						checked={discountDetails?.applyType == "OnlyGeneral" ? true : false}
						label__class__name={styles.radio__label}
					/>
					<AppRadioButton
						name={"applyType"}
						label={tr`apply_both`}
						checked={discountDetails?.applyType == "Both" ? true : false}
						label__class__name={styles.radio__label}
					/>
					<AppRadioButton
						name={"applyType"}
						label={tr`apply_the_bigger_amount`}
						checked={discountDetails?.applyType == "MaxValue" ? true : false}
						label__class__name={styles.radio__label}
					/>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RGeneralDiscountDetails;
