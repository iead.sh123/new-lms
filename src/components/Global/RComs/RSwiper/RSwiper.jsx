import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Autoplay, Scrollbar, A11y } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

const RSwiper = ({ slidesPerView, spaceBetween, navigation, children, autoplay, modules, style }) => {
	return (
		<Swiper
			className="swiper_container"
			modules={modules ? modules : [(Pagination, Navigation)]}
			spaceBetween={spaceBetween ?? 50}
			slidesPerView={slidesPerView ?? 2}
			navigation={navigation ?? true}
			pagination={{ clickable: true }}
			autoplay={autoplay}
			style={style}
		>
			{children}
		</Swiper>
	);
};

export default RSwiper;
