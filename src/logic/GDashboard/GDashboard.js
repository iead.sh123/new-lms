import Helper from "components/Global/RComs/Helper";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import GAdminDashboard from "./GAdminDashboard";
import GTeacherDashboard from "./GTeacherDashboard";
import GStudentDashboard from "./GStudentDashboard";
import { Services } from 'engine/services';
import { GSeniorTeacherDashboard } from "./GSeniorTeacherDashboard";
import GFacilitatorDashboard from "./GFacilitatorDashboard";
import GLearnerDashboard from "./GLearnerDashboard";
import GCourseAdminDashboard from "./GCourseAdminDashboard";
import GCurriculaDirectorDashboard from "./GCurriculaDirectorDashboard";
import GRCAdminDashboard from "./GRCAdminDashboard";
import GParentDashboard from "./GParentDashboard";

const GDashboard = () => {
 const { user } = useSelector((state) => state?.auth);
// return  <GSeniorTeacherDashboard/>
// return <>Dashboard</>
return <>


{(user?.organization_user?.user_type__organization?.organization_type_id==116)?
((user?.current_type?.toLowerCase()=="facilitator")?<GCourseAdminDashboard userType="fa"/>:
((user?.current_type?.toLowerCase()=="course administrator")||(user?.current_type?.toLowerCase()=="course_administrator"))?<GCourseAdminDashboard userType="ca"/>:
((user?.current_type?.toLowerCase()=="curriculum_director")||(user?.current_type?.trim().toLowerCase()=="curriculum director"))?<GCourseAdminDashboard userType="cd"/>:
(user?.current_type?.toLowerCase()=="student"||user?.current_type?.toLowerCase()=="learner")?<GLearnerDashboard/>:
user?.current_type?.toLowerCase()=="admin"?<GCourseAdminDashboard userType="ad"/>:
<>This type ""{user?.current_type}"" doesn't have its own dashboard yet in north point organization type {user.organization_user?.user_type__organization?.organization_type_id}

{user?.current_type?.trim().toLowerCase()}
</>

):


(user?.current_type?.toLowerCase()=="admin")?<GAdminDashboard/>:
user?.current_type?.toLowerCase()=="teacher"?<GTeacherDashboard/>:
user?.current_type?.toLowerCase()=="student"?<GStudentDashboard/>:
user?.current_type?.toLowerCase()=="parent"?<GParentDashboard/>:
user?.current_type?.toLowerCase()=="senior_teacher"?<GSeniorTeacherDashboard/>:
user?.current_type?.toLowerCase()=="school_advisor"?<GSeniorTeacherDashboard/>:
<>This type {user?.current_type} doesn't have its own dashboard yet</>
}
</>
};

export default GDashboard;
