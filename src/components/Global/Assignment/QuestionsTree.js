import React, { useEffect, useMemo, useState } from "react";
import Helper from "../RComs/Helper";
//import Tree from 'antd/es/Tree';
//import { DownOutlined } from '@ant-design/icons';
import { TYPES } from "./assignmentReducer";
import ReusableTree from "./ReusableTree";
//import 'antd/dist/antd.css';
import { Button } from "reactstrap";
import Tree from "../RComs/RTree/RTree";
import REditableTree from "../RComs/REditableTree/REditableTree";
import "./assignment.css";
const QuestionsTree = ({
  lessonGroups,
  dispatch,
  currentActiveTopic,
  currentActiveGroup,
  currentActiveQuestion,
  currentQuestion,
  mode,
  saveAssignmentDataHandler,
  assignmentReadMode,
  totalMark,
  rabName,
}) => {
  const [selectedNode, setSelectedNode] = useState("");
  const [selectedNode1, setSelectedNode1] = useState("");
  const onSelect = (info) => {
    dispatch({ type: TYPES.CHANGE_QUESTION_TREE, payload: info });
  };

  useEffect(() => {
    setSelectedNode(
      `${currentQuestion?.id}${currentActiveGroup}${currentActiveTopic}${currentActiveQuestion}`
    );
    setSelectedNode1(
      `${currentQuestion?.id}-${currentActiveGroup}-${currentActiveTopic}-${currentActiveQuestion}`
    );
  }, [currentActiveTopic, currentActiveGroup, currentActiveQuestion]);

  const treeData = useMemo(() => {
    return lessonGroups?.inst_pattern_groups?.map((group, groupIndex) => {
      const topics = group.inst_topics?.map((topic, topicIndex) => {
        const questions = topic.inst_questions?.map(
          (question, questionIndex) => {
            return {
              name: questionIndex + 1,
              key: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
              id: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
              groupId: groupIndex,
              topicId: topicIndex,
              questionId: questionIndex,
              type: "question",
              className:
                question.notSolved === 0
                  ? "assignment_tree_question-solved"
                  : "",
              link: "",
              color: question.notSolved === 0 ? "#ffeeff" : "#ffeeff",
              icon:
                question.notSolved === 0
                  ? "fa fa-check-circle-o ml-2 "
                  : "fa fa-check-circle ml-2",
            };
          }
        );
        return {
          name: topic.title,
          key: `${topic.id}${groupIndex}${topicIndex}`,
          groupId: groupIndex,
          topicId: topicIndex,
          items: questions,
          type: "topic",
          id: `${topic.id}${groupIndex}${topicIndex}`,
        };
      });
      return {
        name: group.pattern_group_text,
        key: `${group.id}${groupIndex}`,
        id: `${group.id}${groupIndex}`,
        groupId: groupIndex,
        items: topics,
        type: "group",
        link: "group link",
      };
    });
  }, []);

  const newtreeData = treeData;

  const qtreeStyle = {
    // backgroundImage: "linear-gradient(to bottom, #64605A, #252525)",
    borderTopRightRadius: "15px",
    borderBottomRightRadius: "15px ",
  };
  if (mode == "teacher") {
    //   qtreeStyle.backgroundColor="gray";
    // qtreeStyle.backgroundImage = "linear-gradient(to bottom,gray, gray)";
  } else {
  }
  return (
    <div className="gu-assignment-questions-tree mt-1   " style={qtreeStyle}>
      {mode == "student" && (
        <div class="gu-assignment-items mt-3">
          {rabName ? (
            <div class="gu-assignment-item">
              {" "}
              Course: <span class="gu-assignment-course-name">
                {" "}
                {rabName}
              </span>{" "}
            </div>
          ) : null}
          {/* <div class="gu-assignment-item"> Time: <span class="gu-assignment-time-mark"> 02:00 h</span> </div> */}
          <div class="gu-assignment-item">
            {" "}
            Total Mark:{" "}
            <span class="gu-assignment-time-marks">
              {" "}
              {totalMark ? totalMark : "0"}
            </span>{" "}
          </div>
        </div>
      )}

      {newtreeData?.map((s) => (
        <REditableTree
          /* movable */
          data={s}
          setActiveNode={() => {
            alert("setActiveNode");
          }}
          activeNodes={[selectedNode]}
          linkPrefix=""
          nodeClicked={onSelect}
        ></REditableTree>
      ))}

      {mode == "student" && !assignmentReadMode && (
        <Button
          onClick={saveAssignmentDataHandler}
          className="gu-assignment-submite-btn"
        >
          Submit
        </Button>
      )}
    </div>
    // <Tree
    //     className="assignment_tree"
    //     showLine
    //     switcherIcon={<DownOutlined />}
    //     defaultExpandAll={true}
    //     onSelect={onSelect}
    //     treeData={treeData}
    //     selectedKeys={[selectedNode]}
    // />
  );
};

export default QuestionsTree;
//
