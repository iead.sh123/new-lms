import { get, post } from "config/api";

export const parentApi = {
  getAllChildren: () => get("ptr-api/children"),
  getChildrenUpComingEvents: (childId) =>
    get(`api/events/StudentUpComingEvents/${childId}`),
  getNotifications: (childId) =>
    get(`api/notifications/getNotifications/${childId}`),
  getChildrenGradingBook: (termId, childId) =>
    get(`api/studentrabs/studentGradingBook/${termId}/${childId}`),

  getCalendarEvents: (user_id) =>
    get(`api/events/StudentCalendarEvents/${user_id}`),

  postHandleWithCare: (data) => post(`ptr-api/handel-with-care`, data),

  markNotificationAsSeen: (notificationId) =>
    post(`api/notifications/markNotificationAsSeen/${notificationId}`),
};
