import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Collapse } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

const RSingleRole = ({ role, isOpen, isSelected, handleSelectRole, handleCollapseClicked, review, canEdit }) => {
	return (
		<RFlex className="flex-column align-items-start" styleProps={{ gap: "5px" }}>
			<RFlex className="justify-content-between align-items-center" styleProps={{ gap: "1px", width: "80%" }}>
				<RFlex className="align-items-center" styleProps={{ gap: "2px" }}>
					{!review && (
						<AppNewCheckbox idsOrderGenerator={role.id} checked={isSelected} onChange={() => (canEdit ? handleSelectRole(role) : "")} />
					)}
					<RFlex
						className="align-items-center"
						styleProps={{ gap: "3px", cursor: "pointer" }}
						onClick={() => handleCollapseClicked(role.id)}
					>
						<span className="p-0 m-0">{role.name}</span>
						<i className={`${isOpen ? iconsFa6.chevronDown : iconsFa6.chevronRight} fa-sm`} />
					</RFlex>
				</RFlex>
				<RFlex className="align-items-center justify-content-start">
					<span className="text-primary">
						{role.permissions.length} {tr("permission")}
					</span>
					<p className="p-0 m-0 " style={{ width: "400px", maxWidth: "400px" }}>
						{role.description}
					</p>
				</RFlex>
			</RFlex>
			<Collapse isOpen={isOpen}>
				<RFlex styleProps={{ paddingLeft: "15px" }} className="flex-column">
					{role.permissions.map((permission, index) => {
						return (
							<span className="p-0 m-0" style={{ fontSize: "11px" }}>
								{permission.name}
							</span>
						);
					})}
				</RFlex>
			</Collapse>
		</RFlex>
	);
};

export default RSingleRole;
