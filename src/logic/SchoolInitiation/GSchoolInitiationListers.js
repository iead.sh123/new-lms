import React, { useState, useContext } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { fillNameSchoolInitiation } from "store/actions/global/schoolInitiation.actions";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { get, destroy } from "config/api";
import { useDispatch } from "react-redux";
import { Services } from "engine/services";
import RSchoolInitiationStepper from "view/SchoolInitiation/RSchoolInitiationStepper";
import GSchoolInitiationHeader from "./GSchoolInitiationHeader";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const GSchoolInitiationListers = () => {
	const dispatch = useDispatch();
	const SchoolInitiationData = useContext(SchoolInitiationContext);
	const history = useHistory();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [alert, setAlert] = useState(false);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);

	const { sIType, sITypeId, semesterId, tabTitle } = useParams();

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const getDataFromBackend = async (specific_url) => {
		let url;

		if (sIType == "education_stage") {
			url = `${Services.auth_organization_management.backend}api/grade-level/getByEducationStage/${sITypeId}`;
		} else if (sIType == "grade_levels") {
			url = `${Services.auth_organization_management.backend}api/curricula/${semesterId}/${sITypeId}`;
		} else {
			url = `${Services.auth_organization_management.backend}api/education-stage/getByOrganization`;
		}

		let response = await get(specific_url ? specific_url : url);

		if (response.data.status == 1) {
			dispatch(
				fillNameSchoolInitiation(
					sIType == "education_stage"
						? response.data.data?.education_stage
						: sIType == "grade_levels"
						? response.data.data?.grade_level
						: null
				)
			);
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.data?.map((r) => ({
			id: r?.id,
			table_name: "schoolInitiationEducationStageLister",
			details:
				sIType == "education_stage"
					? [
							{
								key: tr`order`,
								value: r?.grade_level_order,
							},
							{
								key: tr`title`,
								value: r?.title,
							},
							{
								key: tr`code`,
								value: r?.code,
							},
					  ]
					: sIType == "grade_levels"
					? [
							{
								key: tr`name`,
								value: r?.name,
							},
					  ]
					: [
							{
								key: tr`order`,
								value: r?.order,
							},
							{
								key: tr`name`,
								value: r?.name,
							},
					  ],

			actions: [
				{
					name: tr`manage`,
					icon: "fa fa-edit",
					onClick: () => {
						if (sIType == "education_stage") {
							history.push(`${baseURL}/${genericPath}/school-initiation/${"grade_levels"}/${r.id}/${"properties"}`);
						} else if (sIType == "grade_levels") {
							history.push(`${baseURL}/${genericPath}/school-initiation/${"curricula"}/${r.id}/${"properties"}`);
						} else {
							history.push(`${baseURL}/${genericPath}/school-initiation/${"education_stage"}/${r.id}/${"properties"}`);
						}
					},
				},
				{
					name: tr`log`,
					icon: "fa fa-history",
					onClick: () => {},
				},
				{
					name: tr`remove`,
					icon: iconsFa6.delete,
					color: "info",

					onClick: () => {
						handleRemove(r.id);
					},
				},
			],
		}));

		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response?.data?.data?.data,
				table_name: SchoolInitiationData.showAll || semesterId,
			},
		}),
		[processedRecords, SchoolInitiationData.showAll, semesterId]
	);

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const successDelete = async (prameters) => {
		const response = await destroy(
			sIType == "education_stage"
				? `${Services.auth_organization_management.backend}api/grade-level/delete/${prameters.id}`
				: sIType == "grade_levels"
				? `${Services.auth_organization_management.backend}api/curricula/delete/${prameters.id}`
				: `${Services.auth_organization_management.backend}api/education-stage/delete/${prameters.id}`
		);

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);

					hideAlert();
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleSearch = async (emptySearch) => {
		let specific_url;

		if (sIType == "education_stage") {
			specific_url = `${Services.auth_organization_management.backend}api/grade-level/getByEducationStage/${sITypeId}?filter=${
				emptySearch ?? searchData
			}`;
		} else if (sIType == "grade_levels") {
			specific_url = `${Services.auth_organization_management.backend}api/curricula/${semesterId}/${sITypeId}?filter=${
				emptySearch ?? searchData
			}`;
		} else {
			specific_url = `${Services.auth_organization_management.backend}api/education-stage/getByOrganization?filter=${
				emptySearch ?? searchData
			}`;
		}
		setSearchLoading(true);

		const response = await getDataFromBackend(specific_url);

		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);

			toast.error(response.data.msg);
		}
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	return (
		<>
			{alert}
			<RSchoolInitiationStepper />
			<GSchoolInitiationHeader
				handleChangeSearch={handleChangeSearch}
				handleSearch={handleSearch}
				searchLoading={searchLoading}
				searchData={searchData}
				setSearchData={setSearchData}
			/>

			<RAdvancedLister {...propsLiterals.listerProps} />
		</>
	);
};

export default GSchoolInitiationListers;
