/* eslint-disable no-console */
import axios from "axios";
//import { store } from '../store/config';
//import { authLogout } from '../store/actions/authActions';

const version = "";
const API_URL = "/"; //(process.env.NODE_ENV === 'test') ? process.env.BASE_URL || (`http://localhost:${process.env.PORT}/api/${version}/`) : `/api/${version}`;

if (localStorage.getItem("token")) {
  axios.defaults.baseURL = API_URL;
  axios.defaults.headers.common.Accept = "application/json";
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("token");
  axios.defaults.headers.common["current-type"] =
    localStorage.getItem("current-type");
  axios.defaults.headers.common["current-school"] =
    localStorage.getItem("current-school");
  axios.defaults.headers.common["lang"] = localStorage.getItem("lang");
  axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

  axios.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response.status === 401) {
        //   store.dispatch(authLogout());
      }
      return Promise.reject(error);
    }
  );
}
export default axios;
