import { lessonPageApi } from "api/teacher/lesson";
import { toast } from "react-toastify";
import {
	GET_LESSONS_CONTENT_REQUEST,
	GET_LESSONS_CONTENT,
	GET_LESSONS_CONTENT_ERROR,
	GET_LESSON_CONTENT,
	UPDATE_LESSONPAGEBOOK_REQUEST,
	UPDATE_LESSONPAGEBOOK_SUCCESS,
	UPDATE_LESSONPAGEBOOK_ERROR,
	SYNCHRONIZE_RAB_REQUEST,
	SYNCHRONIZE_RAB_SUCCESS,
	SYNCHRONIZE_RAB_ERROR,
} from "./teacherTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";

export const synchronizeRabAsync = (id) => async (dispatch) => {
	dispatch({ type: SYNCHRONIZE_RAB_REQUEST });

	try {
		const response = await lessonPageApi.synchronizeRab(id);

		if (response.data.status === 1) {
			dispatch({
				type: SYNCHRONIZE_RAB_SUCCESS,
			});
		} else {
			dispatch({
				type: SYNCHRONIZE_RAB_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: SYNCHRONIZE_RAB_ERROR,
		});
	}
};

export const fetchLessonsContent = (rabContentId) => async (dispatch) => {
	dispatch({ type: GET_LESSONS_CONTENT_REQUEST });

	try {
		const response = await lessonPageApi.getLessonsContent(rabContentId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_LESSONS_CONTENT,
				payload: response.data.data.models.rab_contents,
			});
		} else {
			dispatch({
				type: GET_LESSONS_CONTENT_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_LESSONS_CONTENT_ERROR,
		});
	}
};

export const fetchLessonContent = (rabContentId) => async (dispatch) => {
	try {
		const response = await lessonPageApi.getLessonsContent(rabContentId);
		if (response.data.status === 1) {
			dispatch({
				type: GET_LESSON_CONTENT,
				payload: response.data.data.models,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const InsertBookToLesson = (lessonId, lessonPageContent) => async (dispatch) => {
	try {
		const response = await lessonPageApi.insertBooksToLesson(lessonId, lessonPageContent);
		if (response.data.status === 1) {
			const response2 = await lessonPageApi.getLessonsContent(lessonId);

			dispatch({
				type: GET_LESSONS_CONTENT,
				payload: response2.data.data.models.rab_contents,
			});
		} else {
			toast.error(response.data.msg);
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const updateLessonPageBook = (lessonId, temp) => async (dispatch) => {
	// dispatch({ type: UPDATE_LESSONPAGEBOOK_REQUEST });
	try {
		const response = await lessonPageApi.updateLessonBook(lessonId, temp);
		if (response.data.status === 1) {
			dispatch({
				type: UPDATE_LESSONPAGEBOOK_SUCCESS,
				payload: response.data.data?.rab_content_id,
			});
		} else {
			toast.error(response.data.msg);
		}
	} catch (error) {
		toast.error(error?.message);
	}
};
