import React from "react";
import RForm from "components/Global/RComs/RForm";
import tr from "components/Global/RComs/RTranslator";
import { Container } from "reactstrap";
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from "reactstrap";
import { getInput } from "../utils";

const Stage3 = ({
  handleSubmit,
  containerTitle = "",
  scheme,
  handleUpdateProps,
  containerActions,
  relationItems,
}) => {
  //relation_id

  const formProps = {
    formItems: scheme.map((sc) => ({
      label: tr(sc.Title),
      type: getInput(sc).type,
      md: "12",
      name: tr(sc.Title),
      value: relationItems[sc.Title],
      valid: true,
      // required: true,
      option: getInput(sc).options,
      onChange: (event) =>
        handleUpdateProps(
          sc.Field,
          getInput(sc).type === "checkbox"
            ? event.target.checked
            : getInput(sc).type === "file"
            ? event.target.files
            : getInput(sc).type === "select"
            ? event
            : event.target.value
        ),
    })),
    withSubmit: true,
    onSubmit: (e) => {
      e.preventDefault();
      handleSubmit();
    },
  };

  return (
    <Modal size="lg" style={{ maxWidth: "700px", width: "100%" }} isOpen={true}>
      <ModalHeader>{containerTitle}</ModalHeader>
      <ModalBody>
        <Container fluid>
          <RForm {...formProps} />
        </Container>
      </ModalBody>
      <ModalFooter>
        {containerActions.map((ac) => (
          <Button color={ac.btnColor} onClick={ac.handler}>
            {ac.btnCaption}
          </Button>
        ))}
      </ModalFooter>
    </Modal>
  );
};

export default Stage3;
