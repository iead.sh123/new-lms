import React from "react";
import tr from "components/Global/RComs/RTranslator";
import { UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu } from "reactstrap";
import { primaryColor } from "config/constants";

// add styling for menu and item
const RDropdownIcon = ({
	actions = [],
	iconContainerStyle,
	iconClassname = "fa-solid fa-angle-down fa-sm text-success",
	iconStyle,
	toggleStyle,
	dropdownStyle,
	menuClassname,
	menuStyle,
	itemStyle,
	stopPropagation,
	direction = "end",
	component = null,
	dropdownClassname = "",
	makeDividerAsIndexes,
}) => {
	const defaultIconContainerStyle = {
		height: "16px",
		width: "16px",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		cursor: "auto",
	};
	return (
		<UncontrolledDropdown
			direction={direction}
			onClick={(event) => (stopPropagation ? event.stopPropagation() : "")}
			style={{ ...dropdownStyle }}
			className={dropdownClassname}
		>
			<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" style={{ padding: "0px", ...toggleStyle }} nav>
				{!component ? (
					<div style={{ ...defaultIconContainerStyle, ...iconContainerStyle }}>
						<i className={iconClassname} style={{ cursor: "pointer", ...iconStyle }} />
					</div>
				) : (
					component
				)}
			</DropdownToggle>

			<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right className={menuClassname} style={{ ...menuStyle }}>
				{actions.map((ac, index) => {
					return (
						<React.Fragment key={`ctm_${ac.label}`}>
							{makeDividerAsIndexes && makeDividerAsIndexes?.length > 0 && makeDividerAsIndexes.includes(index) && <DropdownItem divider />}
							<DropdownItem
								id={`ctm_${ac.label}`}
								onClick={() => ac.action(ac.parameter1)}
								style={{ ...itemStyle, color: ac.name ? (ac.name == ac.value ? primaryColor : "") : "" }}
							>
								{ac.icon && <i className={`mr-2 ${ac.icon}`} aria-hidden="true" style={{ color: ac.color }}></i>}
								{tr(ac.label)}
							</DropdownItem>
						</React.Fragment>
					);
				})}
			</DropdownMenu>
		</UncontrolledDropdown>
	);
};

export default RDropdownIcon;
