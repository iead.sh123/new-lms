import React, { useState } from "react";
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap";
import tr from "../RTranslator";
import styles from "./RAnnouncement.Module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import { useHistory, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  DeleteAnnouncementAsync,
  PublishAnnouncementAsync,
} from "store/actions/admin/schoolEvents";
import ReactBSAlert from "react-bootstrap-sweetalert";

const AnnouncementSetting = ({ announcement }) => {
  const dispatch = useDispatch();
  const { schoolEventID, iDHomeRoom, announcementID } = useParams();
  const history = useHistory();

  const [dropdownOpen, setOpen] = useState(false);
  const [alert, setAlert] = useState(false);

  const toggle = () => setOpen(!dropdownOpen);

  const { user } = useSelector((state) => state?.auth);

  const { publishAnnouncementLoading } = useSelector(
    (state) => state.SchoolEventsRed
  );

  //Hide Alert
  const hideAlert = () => {
    setAlert(null);
  };

  //Show Alert
  const showAlerts = (child) => {
    setAlert(child);
  };

  //Success Delete
  const successDelete = (e, id) => {
    e.preventDefault();
    dispatch(DeleteAnnouncementAsync(id));
    hideAlert();
  };

  //Handle Delete Announcement
  const handelDeleteAnnouncement = (e, id) => {
    showAlerts(
      <ReactBSAlert
        warning
        title={tr("are_you_sure_to_delete_it")}
        onConfirm={() => successDelete(e, id)}
        onCancel={() => hideAlert()}
        confirmBtnBsStyle="info"
        cancelBtnBsStyle="danger"
        confirmBtnText={tr("yes_delete_it")}
        cancelBtnText={tr("cancel")}
        showCancel
        btnSize=""
      ></ReactBSAlert>
    );
  };

  const handlePublishAnnouncement = (e) => {
    e.preventDefault();

    dispatch(
      PublishAnnouncementAsync({
        payload: { announcement_ids: [announcement.id] },
      })
    );
  };

  return (
    <>
      {alert}
      <Row>
        <Col>
          <Button
            color="info"
            onClick={(e) => handlePublishAnnouncement(e, iDHomeRoom)}
            disabled={publishAnnouncementLoading ? true : false}
          >
            {publishAnnouncementLoading ? (
              <>
                {tr`publish`} <i className="fa fa-refresh fa-spin"></i>
              </>
            ) : (
              tr("publish")
            )}
          </Button>
        </Col>
        <Col>
          <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle className={" btn btn-link"} id="dropdown-basic">
              <FontAwesomeIcon
                className={styles.icon_color}
                icon={faEllipsisH}
              />{" "}
            </DropdownToggle>
            <DropdownMenu
              aria-haspopup={true}
              caret
              color="default"
              data-toggle="dropdown"
              className={styles.dropdown_edit}
            >
              <DropdownItem>
                <span
                  name="Edit"
                  onClick={() =>
                    user?.type == "teacher"
                      ? history.push(
                          `${process.env.REACT_APP_BASE_URL}/teacher/home-room/${iDHomeRoom}/announcement/${announcement.id}`
                        )
                      : user?.type == "employee" && !schoolEventID
                      ? history.push(
                          `${process.env.REACT_APP_BASE_URL}/admin/announcement/${announcement.id}`
                        )
                      : history.push(
                          `${process.env.REACT_APP_BASE_URL}/admin/school-event/${schoolEventID}/announcement/${announcement.id}`
                        )
                  }
                >
                  <i
                    className="fa fa-pencil fa-lg"
                    style={{ color: "#46C37E" }}
                  ></i>
                  &nbsp; <span>{tr`edit`}</span>
                </span>
              </DropdownItem>

              <DropdownItem>
                <span
                  onClick={(e) => {
                    handelDeleteAnnouncement(e, announcement.id);
                  }}
                >
                  <i
                    className="fa fa-trash fa-lg"
                    style={{ color: "#ef8157" }}
                  ></i>
                  &nbsp; <span>{tr`delete`}</span>
                </span>
              </DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>
        </Col>
      </Row>
    </>
  );
};

export default AnnouncementSetting;
