import React from "react";
import RManageAccess from "view/FileManagement/RMyFiles/RManageAccess";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { fileManagementApi } from "api/global/fileManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useSelector } from "react-redux";
import { useFormik } from "formik";

const GManageAccess = ({ data, parentInvalidateKey, handlers }) => {
	const { user } = useSelector((state) => state.auth);

	const initialValues = { itemDownloadStatus: data.item?.download_available, itemAccess: [], permissionList: [] };

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	const permissionListData = useFetchDataRQ({
		queryKey: ["permissionList"],
		queryFn: () => fileManagementApi.permissionList(),
		onSuccessFn: ({ data }) => {
			setFieldValue(
				"permissionList",
				data?.data?.map((per) => ({
					label: per.permission_type,
					value: per.id,
				}))
			);
		},
	});

	const folderAccessData = useFetchDataRQ({
		queryKey: ["folderAccess_manageAccess", data.folderIds[0]],
		queryFn: () => fileManagementApi.folderAccess(data.folderIds[0]),
		enableCondition: data.folderIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			const formattedData = data?.data?.map((user) => {
				const permissions = user.permissions.map((permission) => ({
					label: permission.permission_type,
					value: permission.id,
				}));
				return { ...user, permissions };
			});
			setFieldValue("itemAccess", formattedData);
		},
	});

	const fileAccessData = useFetchDataRQ({
		queryKey: ["fileAccess_manageAccess", data.fileIds[0]],
		queryFn: () => fileManagementApi.fileAccess(data.fileIds[0]),
		enableCondition: data.fileIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			const formattedData = data?.data?.map((user) => {
				const permissions = user.permissions.map((permission) => ({
					label: permission.permission_type,
					value: permission.id,
				}));
				return { ...user, permissions };
			});
			setFieldValue("itemAccess", formattedData);
		},
	});

	const addAccessToFolderMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.addAccessToFolder({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			handlers.handleCloseManageAccess();
		},
	});

	const addAccessToFileMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.addAccessToFile({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: () => {
			handlers.handleCloseManageAccess();
		},
	});

	const downloadFolderAvailableMutate = useMutateData({
		queryFn: ({ folderId, data }) => fileManagementApi.downloadFolderAvailable({ folderId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemDownloadStatus", variables?.data.download_available);
		},
	});

	const downloadFileAvailableMutate = useMutateData({
		queryFn: ({ fileId, data }) => fileManagementApi.downloadFileAvailable({ fileId, data }),
		invalidateKeys: parentInvalidateKey,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue("itemDownloadStatus", variables?.data.download_available);
		},
	});

	const handleSelectPermissionsToUser = ({ event, userId }) => {
		const ind = values.itemAccess?.findIndex((userItem) => userItem.user_id == userId);
		if (ind !== -1) {
			setFieldValue(`itemAccess[${ind}].permissions`, event);
		}
	};

	const handleAddAccessToItem = () => {
		if (data.fileIds.length > 0) {
			addAccessToFileMutate.mutate({
				fileId: data.fileIds[0],
				data: {
					data: values.itemAccess.map((item) => ({
						user: item.user_id,
						permissions: item.permissions.map((el) => el.id ?? el.value),
					})),
				},
			});
		} else {
			addAccessToFolderMutate.mutate({
				folderId: data.folderIds[0],
				data: {
					data: values.itemAccess.map((item) => ({
						user: item.user_id,
						permissions: item.permissions.map((el) => el.id ?? el.value),
					})),
				},
			});
		}
	};

	const handleSwitchChange = (event) => {
		if (data.fileIds.length > 0) {
			downloadFileAvailableMutate.mutate({ fileId: data.fileIds[0], data: { download_available: event } });
		} else {
			downloadFolderAvailableMutate.mutate({ folderId: data.folderIds[0], data: { download_available: event } });
		}
	};

	if (
		permissionListData.isLoading ||
		(folderAccessData.isLoading && folderAccessData.fetchStatus !== "idle") ||
		(fileAccessData.isLoading && fileAccessData.fetchStatus !== "idle")
	)
		return <Loader />;

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RManageAccess
				data={{ itemObj: data.item, user, values }}
				handlers={{ handlers, handleSelectPermissionsToUser, handleAddAccessToItem, handleSwitchChange }}
				loading={{
					downloadStatusLoading: downloadFolderAvailableMutate.isLoading || downloadFileAvailableMutate.isLoading,
					addAccessToItemLoading: addAccessToFolderMutate.isLoading || addAccessToFolderMutate.isLoading,
				}}
			/>
		</RFlex>
	);
};

export default GManageAccess;
