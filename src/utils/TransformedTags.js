import React from "react";
import { RANDOM_COLOR } from "config/constants";
import RTagsViewer from "components/Global/RComs/Collaboration/RTagsViewer";

export function TransformedTags({ tags }) {
  const TAGS =
    tags &&
    tags.length > 0 &&
    tags.map((tag) => {
      const randomColorIndex = Math.floor(Math.random() * RANDOM_COLOR.length);
      const randomColor = RANDOM_COLOR[randomColorIndex];

      return {
        size: "25px",
        tag_id: tag.value ? tag.value : tag.tag_id,
        name: tag.label ? tag.label : tag.name,
        backgroundColor: randomColor.backgroundColor,
        textColor: randomColor.textColor,
      };
    });

  return <RTagsViewer tags={TAGS} />;
}
