import React from "react";
import checkIcon from "assets/img/check.svg";
import "./AppNewCheckbox.css";

const AppNewCheckbox = (props) => {
	const checkStyle = { "--check-icon": `url(${checkIcon})` };

	const { label, paragraphStyle, flexClassName, flexStyle, idsOrderGenerator, ...otherProps } = props;

	return (
		<div className="custom-checkbox">
			<input type="checkbox" id={`checkbox ${idsOrderGenerator}`} {...otherProps} />
			<label className="square" htmlFor={`checkbox ${idsOrderGenerator}`}>
				<div style={checkStyle} className="icon-inside-label"></div>
			</label>

			<label htmlFor={`checkbox ${idsOrderGenerator}`} style={{ ...paragraphStyle, margin: "0px", padding: "0px", cursor: "pointer" }}>
				{label}
			</label>
		</div>
	);
};

export default AppNewCheckbox;
