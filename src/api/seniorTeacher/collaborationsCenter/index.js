import { get, post, destroy } from "config/api";

export const collaborationsCenterApi = {
  getRecordsNomination: (url) => get(url),
  reviewApprovedRecords: (url) => get(url),
  collaborationHomeData: (url) => get(url),
  approvalNotificationsPool: (tableName) =>
    get(
      `tch-api/collaboration-center/approval-comments-notifications-for-pool/${tableName}`
    ),
  markAsRead: (request) =>
    post(`tch-api/collaboration-center/mark-as-read`, request),
  saveApproveRecords: (request) =>
    post(`api/collaboration/approve-records`, request),
  saveNominate: (request) => post(`api/course-resources/nominate`, request),
  updateReviewedRecords: (request) =>
    post(`api/collaboration/update-reviewed-records`, request),
  getCollaborationOperations: (url) => get(url),
  saveCollaborationOperations: (request) =>
    post(`api/collaboration-operations/store`, request),
  deleteCollaborationOperations: (id) =>
    destroy(`api/collaboration-operations/${id}`),
};
