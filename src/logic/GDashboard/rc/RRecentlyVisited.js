import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RRecentlyVisited.css'
function RRecentlyVisited({day,month,course,coursColor,progress,nextCourse,nextIcon}){
  
  
    return(
        <div style={{margin:'1rem'}}>
    
        <Row className="main_Row" > 
           <Col  xs lg="2" >
              <Row className="date"> <Col>{day}  </Col></Row>
              <Row className="date"> <Col>{month} </Col></Row>
           </Col>
           <Col   lg="9" >
              <Row className="course"  style={{color:coursColor}}> <Col>{course}  </Col></Row>
              <Row> 
                <Col lg="4" className="progress_" >
                   <div className="progress_par" >
                     <div className="progress_par_active" style={{width:progress}}></div>
                   </div>
                   
                </Col>
                <Col   lg="8" >
                 <span className="next_"> Next: </span>  <span className="next_course"><i className={nextIcon}> </i> {nextCourse}</span>
                </Col>
              </Row>
           </Col>
           <Col lg="1" className="right_icon">
            <i className="fa fa-angle-right"></i>
           </Col>
          </Row>
         
        </div>
    )
}
export default RRecentlyVisited;