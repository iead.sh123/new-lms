import React from "react";
import RDropzone2 from "./RDropzone2";

function RFileAdd(props) {
  return (
    <div>
      <RDropzone2 {...props} />
    </div>
  );
}

export default RFileAdd;
