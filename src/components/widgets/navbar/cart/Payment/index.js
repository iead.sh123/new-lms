import React, { useState } from "react";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useWindowWidth } from "hocs/useWindowWidth";
import { paymentApi } from "api/global/payment";
import { CART_ITEMS } from "config/constants";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import OrderSummary from "./OrderSummary";
import PaymentMethods from "./PaymentMethods";

const Payment = () => {
	const screenWidth = useWindowWidth();

	const [clientSecret, setClientSecret] = useState("");
	const [orderId, setOrderId] = useState("");
	let cartItems = [];

	if (typeof window !== "undefined") {
		cartItems = localStorage.getItem(CART_ITEMS)?.length > 0 ? localStorage.getItem(CART_ITEMS)?.split(",").map(Number) : [];
	}

	const createPaymentIntent = useFetchDataRQ({
		queryKey: ["create-payment-intent"],
		queryFn: () => paymentApi.createPaymentIntent({ cartItems: cartItems }),
		onSuccessFn: (data) => {
			setClientSecret(data?.data?.data?.clientSecret);
			setOrderId(data?.data?.data?.orderID);
		},
	});

	// Loading
	if (createPaymentIntent.isLoading) {
		<Loader />;
	}

	return (
		<RFlex styleProps={{ gap: "2%" }}>
			<RFlex styleProps={{ width: screenWidth < 991 ? "100%" : "51%", padding: "30px 0px", justifyContent: "center" }}>
				<PaymentMethods clientSecret={clientSecret} orderId={orderId} />
			</RFlex>
			<RFlex
				styleProps={{
					width: screenWidth < 991 ? "100%" : "47%",
					height: "100vh",
					padding: "30px 0px",
					background: "#F3F3F3",
					justifyContent: "center",
					border: "1px solid #D9D9D9",
				}}
			>
				<OrderSummary />
			</RFlex>
		</RFlex>
	);
};

export default Payment;
