import React, { useState } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { get, destroy, post } from "config/api";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import UserManagementAdd from "logic/UserManagement/UserManagementAdd";
import RHeaderPrincipal from "view/SchoolInitiation/EducationStage/RHeaderPrincipal";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import GAddFromUsers from "./GAddFromUsers";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "components/Global/RComs/RTranslator";
import { fillNameSchoolInitiation } from "store/actions/global/schoolInitiation.actions";
import { useDispatch } from "react-redux";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const GUsersLister = () => {
	const dispatch = useDispatch();
	const { sIType, sITypeId, tabTitle } = useParams();
	const [addFromUserLoading, setAddFromUserLoading] = useState(false);
	const [addFromUsersModal, setAddFromUsersModal] = useState(false);
	const [processedRecords, setProcessedRecords] = useState([]);
	const [userModal, setUserModal] = useState(false);
	const [searchLoading, setSearchLoading] = useState(false);
	const [searchData, setSearchData] = useState("");
	const [alert, setAlert] = useState(false);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleOpenAddUser = () => setUserModal(true);
	const handleCloseAddUser = () => {
		setUserModal(false);
	};

	const handleOpenAddFromUser = () => setAddFromUsersModal(true);
	const handleCloseAddFromUser = () => setAddFromUsersModal(false);

	const handleAddFromUser = async (userId) => {
		setAddFromUserLoading(true);
		let response;

		if (tabTitle == "principal") {
			response = await post(`${Services.auth_organization_management.backend}api/principal/storeExistesUser`, {
				user_id: userId,
				education_stage_id: sITypeId,
			});
		} else if (tabTitle == "senior_teacher") {
			response = await post(`${Services.auth_organization_management.backend}api/senior-teacher/storeExistesUser`, {
				user_id: userId,
				curricula_id: sITypeId,
			});
		}

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					setAddFromUserLoading(false);
					handleCloseAddFromUser();
				} else {
					setAddFromUserLoading(false);

					toast.error(response.data.msg);
				}
			} else {
				setAddFromUserLoading(false);

				toast.error(response.data.msg);
			}
		} else {
			setAddFromUserLoading(false);

			toast.error(response.data.msg);
		}
	};

	const handleRemove = (organizationUserId) => {
		const prameters = {
			organizationUserId,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const successDelete = async (prameters) => {
		let response;
		let data = {
			user_type: tabTitle === "principal" ? "Principal" : tabTitle,
			organization_user_id: prameters.organizationUserId,
			name: sIType,
			value: sITypeId,
		};
		response = await post(`${Services.auth_organization_management.backend}api/organization/detachUserType`, data);
		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					hideAlert();
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleSearch = async (emptySearch) => {
		let specific_url;

		if (tabTitle == "principal") {
			specific_url = `${Services.auth_organization_management.backend}api/principal/getByEducationStage/${sITypeId}?filter=${
				emptySearch ?? searchData
			}`;
		} else if (tabTitle == "student") {
			specific_url = `${Services.auth_organization_management.backend}api/grade-level/getStudentsList/${sITypeId}?filter=${
				emptySearch ?? searchData
			}`;
		} else if (tabTitle == "senior_teacher") {
			specific_url = `${Services.auth_organization_management.backend}api/senior-teacher/getSeniorTeachersByCourse/${sITypeId}?filter=${
				emptySearch ?? searchData
			}`;
		}

		setSearchLoading(true);

		const response = await getDataFromBackend(specific_url);

		if (response) {
			if (response.data.status == 1) {
				setTableData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);

			toast.error(response.data.msg);
		}
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const getDataFromBackend = async (specific_url) => {
		let url;

		if (tabTitle == "principal") {
			url = `${Services.auth_organization_management.backend}api/principal/getByEducationStage/${sITypeId}`;
		} else if (tabTitle == "student") {
			url = `${Services.auth_organization_management.backend}api/grade-level/getStudentsList/${sITypeId}`;
		} else if (tabTitle == "senior_teacher") {
			url = `${Services.auth_organization_management.backend}api/senior-teacher/getSeniorTeachersByCourse/${sITypeId}`;
		}

		let response = await get(specific_url ? specific_url : url);

		if (response.data.status == 1) {
			dispatch(
				fillNameSchoolInitiation(
					sIType == "education_stage"
						? response.data.data?.education_stage
						: sIType == "grade_levels"
						? response.data.data?.grade_level
						: null
				)
			);
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};
	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.data.map((r) => ({
			id: r?.id,
			table_name: "educationStagePrincipalsLister",
			details: [
				{
					key: tr`name`,
					value: {
						name: r.name,
						image: Services.auth_organization_management.file + r?.profile?.hash_id,
					},
					render: renderInfo,
				},
				{
					key: tr`email`,
					value: r?.email,
				},
				{
					key: tr`status`,
					value: r?.status ? tr`active` : tr`inactive`,
					color: r?.status ? "green" : "red",
				},
			],

			actions: [
				{
					name: tr`remove`,
					icon: iconsFa6.delete,
					color: "danger",
					outline: true,

					onClick: () => {
						handleRemove(r.organization_user_id);
					},
				},
			],
		}));

		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response?.data?.data?.data,
				table_name: sITypeId,
			},
		}),
		[processedRecords, sITypeId]
	);

	return (
		<>
			{alert}
			<AppModal
				show={userModal}
				parentHandleClose={handleCloseAddUser}
				header={
					tabTitle == "principal"
						? tr`add_principal`
						: tabTitle == "student"
						? tr`student`
						: tabTitle == "senior_teacher"
						? "senior_teacher"
						: ""
				}
				size={"lg"}
				headerSort={
					<UserManagementAdd
						handleCloseModal={handleCloseAddUser}
						getDataFromBackend={getDataFromBackend}
						setTableData={setTableData}
						fetchDataAfterAdded={true}
						curriculumId={sIType == "curricula" && sITypeId}
					/>
				}
			/>
			<AppModal
				show={addFromUsersModal}
				parentHandleClose={handleCloseAddFromUser}
				header={tr`add_from_users`}
				size={"lg"}
				headerSort={<GAddFromUsers handleAddFromUser={handleAddFromUser} addFromUserLoading={addFromUserLoading} />}
			/>
			<RHeaderPrincipal
				handleOpenAddUser={handleOpenAddUser}
				handleOpenAddFromUser={handleOpenAddFromUser}
				handleChangeSearch={handleChangeSearch}
				handleSearch={handleSearch}
				searchLoading={searchLoading}
				searchData={searchData}
				setSearchData={setSearchData}
			/>
			<RAdvancedLister {...propsLiterals.listerProps} />
		</>
	);
};

export default GUsersLister;
