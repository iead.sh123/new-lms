import React from "react";
import RGradesTable from "view/grading/RGradesTable/RGradesTable";
import { initialState, reducer } from "./state/grading.reducer";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { addGradingGroups, assignGradableItemToGroup, createCustomGradableItem, deleteGradingGroup, loadCourseGradableItems, loadGradingGroups, updateGraddableItems, updateGradingGroup, deleteGraddableItem, submitAlternatives, NAVIGATE_STEPPER, loadGradableItemDetailsForAllStudents 
,moveToNextItem,moveToPreviousItem

} from "./state/grading.actions";
import { entities } from "./constants/entities.constants";
import { pageMapping } from "./pages";
import { pages } from "./constants/pages.constant";
import Loader from "utils/Loader";
import RNavigationHeader from "components/Global/RComs/RNavigationHeader/RNavigationHeader";
import tr from "components/Global/RComs/RTranslator";
import Helper from "components/Global/RComs/Helper";
import { gradingApi } from "api/grading";

const Grading = () => {
	//refs
	const mounted = React.useRef(false);

	//state
	const [data, dispatch] = React.useReducer(reducer, initialState);
	const [loadingEntities, setLoadingEntities] = React.useState([]);

	const { courseId } = useParams();

	const handleLoadCourseGradingGroups = React.useCallback(async () => {
		setLoadingEntities([...loadingEntities, entities.gradingGroups]);
		await loadGradingGroups(courseId, dispatch, mounted);
		setLoadingEntities(loadingEntities.filter((en) => en !== entities.gradingGroups));
	}, [courseId]);

	const handleLoadCourseGraddableItems = React.useCallback(async () => {
		setLoadingEntities([...loadingEntities, entities.gradableItems]);
		await loadCourseGradableItems(courseId, dispatch, mounted);
		setLoadingEntities(loadingEntities.filter((en) => en !== entities.gradableItems));
	}, [courseId]);

	const handleAddGroup = async (data) => {
		setLoadingEntities([...loadingEntities, entities.gradingGroups]);
		await addGradingGroups(courseId, { ...data, extraPoints: +data.extraPoints }, dispatch, mounted);
		setLoadingEntities(loadingEntities.filter((en) => en !== entities.gradingGroups));
	};

	const handleDeleteGradingGroup = async (id) => {
		// setLoadingEntities([...loadingEntities, entities.gradingGroups]);
		await deleteGradingGroup(id, dispatch, mounted);

		// setLoadingEntities(loadingEntities.filter(en => en !== entities.gradingGroups));
	};

	const handleUpdateGradingGroup = async (id, data) => {
		//  setLoadingEntities([...loadingEntities, entities.gradingGroups]);
		await updateGradingGroup(id, data, dispatch, mounted);
		//  setLoadingEntities(loadingEntities.filter(en => en !== entities.gradingGroups));
	};

	const handleAssignGradableItemToGroup = async (gradableItemType, id, group) => {
		await assignGradableItemToGroup(gradableItemType, id, group, dispatch, mounted);
		await handleLoadCourseGraddableItems();
	};

	const handleCreateCustomGradableItem = async (payload) => {
		await createCustomGradableItem(
			{ ...payload, groupName: undefined },
			courseId,
			data.entities.gradingGroups.byId?.[payload?.groupName?.value],
			dispatch,
			mounted
		);
		await handleLoadCourseGraddableItems();
	};

	const handleUpdateGradableItems = async (id, data) => {
		await updateGraddableItems(id, { ...data, group: data?.groupName?.value, groupName: undefined }, dispatch, mounted);
		await handleLoadCourseGraddableItems();
	};

	const handleDeleteGradableItems = async (id) => {
		await deleteGraddableItem(id, dispatch, mounted);
		await handleLoadCourseGraddableItems();
	};

	const handleSubmitAlternatives = async (itemCompositeId, alternativesPayload) => {
		await submitAlternatives(itemCompositeId, alternativesPayload, dispatch, mounted);
		await handleLoadCourseGraddableItems();
	};

	React.useEffect(() => {
		mounted.current = true;

		return () => {
			mounted.current = false;
		};
	}, []);

	const Page = pageMapping[data.selectedPage];


  const handleNavigateStepper = (page) => {
    dispatch({ type: NAVIGATE_STEPPER, payload: page });
  }
//--------------------------------------------------------------- Student Details editor
  const handleLoadGradableItemStudentsDetails = (type , itemId)=>{
    loadGradableItemDetailsForAllStudents(type , itemId , dispatch , mounted);
    dispatch({ type: "SET_CURRENT_ITEM", payload:{item_type:type,item_id:itemId} });
    dispatch({ type: NAVIGATE_STEPPER, payload: pages.STUDENT_DETAILS });
  }
  const goNextItem = (direction=1)=>{
   
    Helper.cl(data.currentGroup,"currentGroup");
    Helper.cl(data.currentItem,"currentItem");

    let selected=-1;
    Object.values(data.entities.gradableItems.byId).map((s,i)=>
{      if(s.id==data.currentItem.id && s.type==data.currentItem.type)
        selected=i;
        }
    )
    if(Object.values(data.entities.gradableItems.byId)[selected+1])
        {
          //alert(selected);
          handleLoadGradableItemStudentsDetails(Object.values(data.entities.gradableItems.byId)[selected+1].type,Object.values(data.entities.gradableItems.byId)[selected+1].id.split('_')[1]);
        }



   // loadGradableItemDetailsForAllStudents(type , itemId , dispatch , mounted);
   // dispatch({ type: NAVIGATE_STEPPER, payload: pages.STUDENT_DETAILS });
  }
const goPreviousItem = ()=>{
   
  Helper.cl(data.currentGroup,"currentGroup");
  Helper.cl(data.currentItem,"currentItem");

  let selected=-1;
  Object.values(data.entities.gradableItems.byId).map((s,i)=>
{      if(s.id==data.currentItem.id && s.type==data.currentItem.type)
      selected=i;
      }
  )
  if(Object.values(data.entities.gradableItems.byId)[selected-1])
      {
        //alert(selected);
        handleLoadGradableItemStudentsDetails(Object.values(data.entities.gradableItems.byId)[selected-1].type,Object.values(data.entities.gradableItems.byId)[selected-1].id.split('_')[1]);
      }



 // loadGradableItemDetailsForAllStudents(type , itemId , dispatch , mounted);
 // dispatch({ type: NAVIGATE_STEPPER, payload: pages.STUDENT_DETAILS });
}

const setStudentItemMark=(student_id,item_id,new_grade)=>
{
  dispatch({ type: "SET_STUDENT_ITEM_MARK", payload:{student_id:student_id,item_id:item_id,new_grade:new_grade} });
}

const batchStudentItemMark=async(item_type , item_id,student_id,new_grade)=>
{
 return await gradingApi.batchStudentItemGrade(item_type , item_id,student_id,new_grade)
}
//--------------------------------------------------------------
  const pageProps = {
    [pages.GROUP_AND_ITEMS]: {
      handleLoadCourseGraddableItems,
      handleLoadCourseGradingGroups,
      handleDeleteGradingGroup,
      gradingGroups: data.entities.gradingGroups,
      courseGradableItems: data.entities.gradableItems,
      handleAddGroup,
      handleUpdateGradingGroup,
      handleUpdateGradableItems,
      handleAssignGradableItemToGroup,
      handleCreateCustomGradableItem,
      handleDeleteGradableItems,
      handleSubmitAlternatives,
      handleLoadGradableItemStudentsDetails
    } , 
    [pages.STUDENT_DETAILS]:{
      itemStudents: data.GradableItemStudentsDetails,
      goPreviousItem,
      currentGroup:data.currentGroup,
      currentItem:data.currentItem,
      goNextItem,
      hasNextItem:null,
      hasPreviousItem:null,
      setStudentItemMark,
      batchStudentItemMark
    }
  }
  return <React.Fragment>
    Grading.jsx flag1 selected page = {data.selectedPage}
    {data.selectedPage!="STUDENT_DETAILS"?  <RNavigationHeader
      steps={[{ id: pages.GROUP_AND_ITEMS, onClick: () => handleNavigateStepper(pages.GROUP_AND_ITEMS), text: tr`Mark Details` }, { id: pages.COURSE_FINAL_MARKS, onClick: () => handleNavigateStepper(pages.COURSE_FINAL_MARKS), text: tr`Final Marks` }]}
      selectedStep={data.selectedPage}
      mini />:
      <div onClick={()=>{handleNavigateStepper(pages.GROUP_AND_ITEMS)}}> <span id="backButton" style={{cursor: "pointer"}}>
      <i class="fas fa-arrow-left"></i> Back
    </span>
    </div>
      
      }
Grading.jsx flag2 {Helper.js(data.selectedPage,"selectedpage")}
    <React.Suspense fallback={<Loader />}>
      <Page {...pageProps[data.selectedPage]} loading={loadingEntities} />
    </React.Suspense>

  </React.Fragment>
}
export default Grading;
