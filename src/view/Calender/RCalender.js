import React from 'react';
import './cutsom.scss'
import FullCalendar from '@fullcalendar/react'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import timegridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction'
import RCalendarHeader from './RCalendarHeader';

const RCalender = ({ advanced = true, eventDataTransform, renderEvent, events, customButtons = {}, config = { initialView: "dayGridMonth", plugins: [dayGridPlugin, timegridPlugin, listPlugin, interactionPlugin] },handlePrint,toggleFilterModal }) => {

    const [workDays, setWorkDays] = React.useState(false);
    const [title, setTitle] = React.useState('CurrentDate')
    const [currentDay, setCurrentDay] = React.useState(0)
    const [currentFilter, setCurrentFilter] = React.useState("Month")
    const calendarRef = React.useRef(null)

    React.useEffect(() => {
        calendarRef.current.getApi().changeView('dayGridMonth');
        setTitle(calendarRef.current.getApi().view.title);
        setCurrentDay(getDayNumber())
        return () => { }
    }, [])


    const toggleWorkDays = () => {
        setWorkDays(!workDays)
    }
    const today = () => {
        calendarRef.current.getApi().today()
        updateTitle()

    }
    const next = () => {
        calendarRef.current.getApi().next()
        updateTitle()

    }
    const previous = () => {
        calendarRef.current.getApi().prev()
        updateTitle()
    }
    const updateTitle = () => {
        const currentTitle = calendarRef.current.getApi().view.title;
        setTitle(currentTitle);
    };

    const getDayNumber = () => {
        const currentDate = calendarRef.current.getApi().getDate();
        const dayNumber = new Intl.DateTimeFormat('en', { day: 'numeric' }).format(currentDate);
        return dayNumber;
    };
    const changeView = (viewName) => {
        calendarRef.current.getApi().changeView(viewName);
        updateTitle()
    };

    const handleViewChange = (viewType) => {
        switch (viewType) {
            case 'Day':
                changeView('timeGridDay');
                setCurrentFilter('Day')
                break;
            case 'Week':
                changeView('timeGridWeek');
                setCurrentFilter("Week")

                break;
            case 'Month':
                changeView('dayGridMonth');
                setCurrentFilter("Month")

                break;
            default:
                console.error('Invalid viewType:', viewType);
                break;
        }
    };


    return <>
        <RCalendarHeader
            advanced={advanced}
            next={next}
            previous={previous}
            title={title}
            currentDay={currentDay}
            toggleWorkDays={toggleWorkDays}
            today={today}
            currentFilter={currentFilter}
            handleViewChange={handleViewChange}
            handlePrint={handlePrint}
            toggleFilterModal={toggleFilterModal} />
        <FullCalendar
            ref={calendarRef}
            plugins={config.plugins}
            initialView={config.initialView}
            weekends={!workDays}
            stickyHeaderDates
            stickyFooterScrollbar
            events={events}
            eventContent={renderEvent}
            eventDataTransform={eventDataTransform}
            customButtons={advanced ? {
                ...customButtons,
                workDays: {
                    text: workDays ? 'All Days' : 'Work Days',
                    click: () => setWorkDays(!workDays),
                },
            } : {}}
            views={advanced ? {
                dayGridMonth: {
                    buttonText: 'Month',

                },
                timeGridWeek: {
                    buttonText: 'Week',

                },
                timeGridDay: {
                    buttonText: 'Day',
                    //   slotDuration: '00:30:00',
                    //   eventTimeFormat: { hour: 'numeric', minute: '2-digit', meridiem: 'short' }
                },
                listWeek: {
                    buttonText: 'List Week',

                },
                listMonth: {
                    buttonText: 'List Month',

                },
                listYear: {
                    buttonText: 'List Year',

                }
            } : {}}

            headerToolbar={advanced ? {
                start: 'prev,next today workDays',
                center: 'title ' + Object.keys(customButtons).join(' '),
                end: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek,listMonth,listYear',
                // right: 
            } : {}}
        />
    </>

}



export default RCalender;



// plugins={[ dayGridPlugin, timeGridPlugin ]}
// initialView="dayGridMonth" // set the default view to monthly


