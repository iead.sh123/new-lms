import React, { useState } from "react";
import ReactPlayer from "react-player/lazy";

export const RVideoPlayer = ({
  url,
  width = "640px",
  height = "360px",
  id = 0,
}) => {
  const [sub, setSub] = useState([
    {
      kind: "subtitles",
      src: process.env.REACT_APP_BACKEND_URL + "api/files/subtitle/" + id,
      srcLang: "English",
      default: true,
      color: "orange",
    },
  ]);

  return (
    <ReactPlayer
      config={{
        file: {
          attributes: {
            controlsList: "nodownload",
            crossOrigin: "true",
          },
          // tracks: sub,
        },
      }}
      url={url}
      controls={true}
      loop
      autoplay
      playsinline
      width={width}
      height={height}
    />
  );
};
