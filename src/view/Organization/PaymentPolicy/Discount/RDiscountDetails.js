import React from "react";
import RGeneralDiscountDetails from "./RGeneralDiscountDetails";
import RNormalDiscountDetails from "./RNormalDiscountDetails";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor, dangerColor, lightGray, boldGreyColor } from "config/constants";

const RDiscountDetails = ({ discountDetails, generalDiscountData, coursesByDiscountId, handlers, states, loading }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column", width: "100%", gap: 20 }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<span>
					{`${discountDetails.value}%`} &nbsp;
					{tr`discount_is_assigned_to`} :
				</span>
				<span
					style={{ color: dangerColor, cursor: "pointer" }}
					onClick={() => handlers.handleDeactivateDiscount()}
				>{tr`deactivate_discount`}</span>
			</RFlex>

			{/* //--------------when i click on general discount------------------- */}
			{discountDetails?.discountType == "General" && <RGeneralDiscountDetails discountDetails={discountDetails} />}

			{/* //--------------if i have discount on organization and i click on normal discount------------------- */}
			{generalDiscountData?.value && discountDetails?.discountType !== "General" && (
				<RFlex styleProps={{ flexDirection: "column" }}>
					<span style={{ color: boldGreyColor }}>{tr`organization-level discount is already active`}</span>
					<span>
						<span style={{ color: lightGray }}>{tr`only one organization-level discount can be applied, you can deactivate it in`}</span>
						&nbsp;
						<span style={{ color: primaryColor }}>{generalDiscountData?.value}%</span>
					</span>
				</RFlex>
			)}

			{discountDetails?.discountType !== "General" && (
				<RNormalDiscountDetails
					discountDetails={discountDetails}
					coursesByDiscountId={coursesByDiscountId}
					handlers={handlers}
					states={states}
					loading={loading}
				/>
			)}

			{/* //--------------------------------- */}
		</RFlex>
	);
};

export default RDiscountDetails;
