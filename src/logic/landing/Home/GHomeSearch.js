import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Form, FormGroup, Input } from "reactstrap";
import styles from "../AllCourses/AllCourses.module.scss";
import tr from "components/Global/RComs/RTranslator";

const GHomeSearch = () => {
  const history = useHistory();
  const [searchData, setSearchData] = useState("");

  const handleChangeSearch = (text) => {
    setSearchData(text);
  };

  const handleSearch = () => {
    history.push(`/landing/all-courses?text=${searchData}`);
  };
  return (
    <Form className={styles.form__input}>
      <FormGroup>
        <Input
          type="text"
          placeholder={tr`search`}
          className={styles.search__input}
          value={searchData}
          onChange={(event) => {
            handleChangeSearch(event.target.value);
          }}
        />
        <i
          aria-hidden="true"
          className={
            false
              ? `fa fa-refresh fa-spin ${styles.search__input__icon}`
              : `fa fa-search ${styles.search__input__icon}`
          }
          onClick={() => searchData !== "" && handleSearch({ reset: false })}
        />
      </FormGroup>
    </Form>
  );
};

export default GHomeSearch;
