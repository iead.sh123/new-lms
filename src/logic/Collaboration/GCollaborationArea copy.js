import React, { useState } from "react";
import { GCollaborationAreaConsumedContents } from "./GCollaborationAreaConsumedContents";
import { GCollaborationAreaProviders } from "./GCollaborationAreaProviders";
import { GCollaborationAreaClients } from "./GCollaborationAreaClients";
import { GConsumedContents } from "./GConsumedContents";
import { useEffect } from "react";
import { Services } from "engine/services";
import RRecentActivities from "components/Global/RComs/Collaboration/RRecentActivities";
import RTagsViewer from "components/Global/RComs/Collaboration/RTagsViewer";
import RColumn from "view/RComs/Containers/RColumn";
import Helper from "components/Global/RComs/Helper";
import RTitle from "components/Global/RComs/Collaboration/RTitle";

export const GCollaborationArea = () => {
	const [activities, setActivities] = useState([]);
	const [transformedActivities, settransformedActivities] = useState([]);
	const [tags, setTags] = useState([]);

	const colors = [
		{ textColor: "#000000", backgroundColor: "#CCCCCC" }, // Light gray
		{ textColor: "#333333", backgroundColor: "#EAEAEA" }, // Lighter gray
		{ textColor: "#333333", backgroundColor: "#F3E5F5" }, // Lavender
		{ textColor: "#000000", backgroundColor: "#F8BBD0" }, // Light pink
		{ textColor: "#000000", backgroundColor: "#FFCC80" }, // Light orange
		{ textColor: "#000000", backgroundColor: "#FFF9C4" }, // Light yellow
		{ textColor: "#000000", backgroundColor: "#B2DFDB" }, // Light green
		{ textColor: "#000000", backgroundColor: "#B3E5FC" }, // Light blue
		{ textColor: "#000000", backgroundColor: "#D1C4E9" }, // Light purple
		{ textColor: "#000000", backgroundColor: "#FFD180" }, // Light amber
	];

	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.collaboration.backend + "api/collaboration-area/most-used-tags/destination-part",
				"fail to get",
				(response) => {
					setTags(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	const transformedTags = tags.map((tag) => {
		const randomColorIndex = Math.floor(Math.random() * colors.length);
		const randomColor = colors[randomColorIndex];

		return {
			size: "25px",
			tag_id: tag.tag_id,
			name: tag.name,
			iconColor: randomColor.textColor,
			backgroundColor: randomColor.backgroundColor,
		};
	});

	//---------------------------------------------------------------------
	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.collaboration.backend + "api/collaboration-area/last-activities",
				"fail to get",
				(response) => {
					setActivities(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	useEffect(() => {
		let transformedActivities1 = [];

		Object.entries(activities).map((ac) => {
			const [acName, acActivities] = ac;
			let targetKey = "";
			let targetActivities = [];
			if (acName == "today_activities") targetKey = "Today";
			if (acName == "yesterday_Activities") targetKey = "Yesterday";
			if (acName == "before_yesterday_activities") targetKey = "Before";

			Object.entries(acActivities).map((activity) => {
				const [acKey, acValues] = activity;

				if (acKey == "clients_added") {
					acValues.map((v) => {
						targetActivities.push(
							<div>
								New Client <a href={`organization/${v.id}`}>{v.name}</a>
							</div>
						);
					});
				}

				if (acKey == "providers_added") {
					acValues.map((v) => {
						targetActivities.push(
							<div>
								New Provider <a href={`organization/${v.id}`}>{v.name}</a>
							</div>
						);
					});
				}

				if (acKey == "all_consumed_contents_by_my_clients") {
					acValues.map((v) => {
						targetActivities.push(<div>{v}</div>);
					});
				}

				if (acKey == "all_consumed_contents_by_my_users") {
					acValues.map((v) => {
						targetActivities.push(<div>{v}</div>);
					});
				}
			});
			transformedActivities1 = { ...transformedActivities1, [targetKey]: targetActivities };
		});
		settransformedActivities(transformedActivities1);
	}, [activities]);

	return (
		<div>
			<RTitle text="Providers" />
			<GCollaborationAreaProviders />

			<RTitle text="Clients" />

			<GCollaborationAreaClients />
			<RTitle text="consumed contents" />

			<GCollaborationAreaConsumedContents />

			<div style={{ display: "flex" }}>
				<RColumn style={{ width: "50%", padding: "20px" }}>
					{transformedTags && transformedTags.length ? (
						<>
							<RTitle text="Most Used Tags" />
							<RTagsViewer tags={transformedTags} />
						</>
					) : (
						<></>
					)}

					<div>
						<div>
							<RTitle text="Recent Activities" />
							<RRecentActivities activities={transformedActivities} />
						</div>
					</div>
				</RColumn>
				<div style={{ width: "50%", padding: "20px" }}>
					<RTitle text="Consumed Contents" />

					<GConsumedContents />
				</div>
			</div>
		</div>
	);
};
