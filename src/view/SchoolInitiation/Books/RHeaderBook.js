import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../RSchoolInitiation.module.scss";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import { Form, FormGroup, Input } from "reactstrap";
import useWindowDimensions from "components/Global/useWindowDimensions";

const RHeaderBook = ({
  handleOpenAddBook,
  handleChangeSearch,
  handleSearch,
  searchLoading,
  searchData,
  setSearchData,
  seeAddingABook,
}) => {
  const { width } = useWindowDimensions();
  const mobile = width < 1000;
  return (
    <RFlex
      styleProps={{
        display: "flex",
        width: "100%",
        alignItems: "center",
        gap: 30,
        flexDirection: mobile ? "column" : "row",
      }}
      className="mb-4"
    >
      {seeAddingABook && (
        <RButton
          color="primary"
          text={tr`add_a_book`}
          faicon="fa fa-plus"
          onClick={handleOpenAddBook}
        />
      )}
      <Form className={styles.form_input}>
        <FormGroup>
          <Input
            type="text"
            placeholder={tr`search`}
            className={styles.search_input}
            value={searchData}
            onChange={(event) => {
              handleChangeSearch(event.target.value);
            }}
          />
          <i
            aria-hidden="true"
            className={
              searchLoading
                ? `fa fa-refresh fa-spin ${styles.search_input_icon}`
                : `fa fa-search ${styles.search_input_icon}`
            }
            onClick={() => searchData !== "" && handleSearch()}
          />
          <i
            aria-hidden="true"
            className={`fa fa-close ${styles.clear_input_icon}`}
            onClick={() => {
              if (searchData !== "") {
                setSearchData("");
                handleSearch("");
              }
            }}
          />
        </FormGroup>
      </Form>
    </RFlex>
  );
};

export default RHeaderBook;
