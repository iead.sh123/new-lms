import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { primaryColor } from "config/constants";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RQuestionSetNavigation = () => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);
  const GroupNextStep =
    QuestionSetSolveData?.questionsToSolve?.questionGroups[
      QuestionSetSolveData.step + 1
    ];
  const GroupPreviousStep =
    QuestionSetSolveData?.questionsToSolve?.questionGroups[
      QuestionSetSolveData.step - 1
    ];

  const IsGroup =
    QuestionSetSolveData?.questionsToSolve?.questionGroups[
      QuestionSetSolveData.step
    ]?.isGroup;

  const QuestionGroup =
    QuestionSetSolveData?.questionsToSolve?.questionGroups[
      QuestionSetSolveData.step
    ];

  const requiredQuestion = IsGroup
    ? !QuestionGroup?.requiredQuestionsToAnswer
      ? QuestionGroup?.questions.every((question) => question.isRequired)
      : +QuestionGroup?.requiredQuestionsToAnswer ==
        +QuestionGroup?.questions.filter((question) => question.isRequired)
          .length
    : QuestionGroup?.questions[0]?.isRequired;

  return (
    <RFlex
      styleProps={{
        justifyContent: "space-between",
        width: "100%",
        paddingTop: "20px",
        alignItems: "center",
      }}
      className={"mb-4 mt-4"}
    >
      <span>
        {QuestionSetSolveData.step !== 0 && (
          <span
            onClick={() => {
              QuestionSetSolveData._prev();
              (!requiredQuestion ||
                QuestionSetSolveData.submitted !== "solving") &&
                QuestionSetSolveData.openQuestionGroupCollapse !==
                  `collapse${GroupPreviousStep.id}` &&
                QuestionSetSolveData.collapsesQuestionsGroupsToggle(
                  GroupPreviousStep.id
                );
            }}
            className="cursor-pointer"
          >
            <span style={{ color: primaryColor, paddingRight: 10 }}>
              <i className="fa-solid fa-chevron-left"></i>
            </span>
            {tr`previous_question`}
          </span>
        )}
      </span>
      <span>
        {QuestionSetSolveData.step !==
        QuestionSetSolveData?.questionsToSolve?.questionGroups?.length - 1 ? (
          <span
            onClick={() => {
              QuestionSetSolveData.submitted == "solving" && requiredQuestion
                ? {}
                : QuestionSetSolveData._next();
              (!requiredQuestion ||
                QuestionSetSolveData.submitted !== "solving") &&
                QuestionSetSolveData.openQuestionGroupCollapse !==
                  `collapse${GroupNextStep.id}` &&
                QuestionSetSolveData.collapsesQuestionsGroupsToggle(
                  GroupNextStep.id
                );
            }}
            style={{
              cursor:
                QuestionSetSolveData.submitted == "solving" && requiredQuestion
                  ? "no-drop"
                  : "pointer",
            }}
          >
            {tr`next_question`}

            <span style={{ color: primaryColor, paddingLeft: 10 }}>
              <i className="fa-solid fa-chevron-right "></i>
            </span>
          </span>
        ) : QuestionSetSolveData.submitted == "solving" ? (
          <RButton
            text={tr`submit_answers`}
            color="primary"
            outline
            faicon="fas fa-power-off"
            onClick={() => QuestionSetSolveData.handleOpenSubmitAnswerModal()}
          />
        ) : QuestionSetSolveData.teacherInCorrectionAndReviewMode ? (
          <RButton
            text={tr`next`}
            color="warning"
            outline
            faicon="fa-solid fa-arrow-right"
            iconRight={true}
            onClick={() =>
              QuestionSetSolveData.setStep(
                QuestionSetSolveData?.questionsToSolve?.questionGroups?.length
              )
            }
          />
        ) : (
          ""
        )}
      </span>
    </RFlex>
  );
};

export default RQuestionSetNavigation;
