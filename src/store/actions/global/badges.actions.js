import { badgesApi } from "api/global/badges";
import {
	GET_BADGES_REQUEST,
	GET_BADGES_SUCCESS,
	GET_BADGES_ERROR,
	REMOVE_BADGES_REQUEST,
	REMOVE_BADGES_SUCCESS,
	REMOVE_BADGES_ERROR,
	STORE_BADGES_REQUEST,
	STORE_BADGES_SUCCESS,
	STORE_BADGES_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const getBadgesAsync = (courseId, curriculumId, filter) => async (dispatch) => {
	dispatch({ type: GET_BADGES_REQUEST });
	try {
		const response = await badgesApi.getBadges(courseId, curriculumId, filter);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_BADGES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_BADGES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_BADGES_ERROR });
		toast.error(error?.message);
	}
};

export const removeBadgeAsync = (badgeId, handleCloseModal) => async (dispatch) => {
	dispatch({ type: REMOVE_BADGES_REQUEST });
	try {
		const response = await badgesApi.removeBadge(badgeId);
		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_BADGES_SUCCESS,
				payload: { badgeId },
			});
			handleCloseModal && handleCloseModal();
		} else {
			dispatch({ type: REMOVE_BADGES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: REMOVE_BADGES_ERROR });
		toast.error(error?.message);
	}
};

export const storeBadgeAsync = (data, handleCloseStoreModal) => async (dispatch) => {
	dispatch({ type: STORE_BADGES_REQUEST });
	try {
		const response = await badgesApi.storeBadge(data);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: STORE_BADGES_SUCCESS,
				payload: { data },
			});
			handleCloseStoreModal && handleCloseStoreModal();
		} else {
			dispatch({ type: STORE_BADGES_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: STORE_BADGES_ERROR });
		toast.error(error?.message);
	}
};
