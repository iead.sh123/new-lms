import Pusher from "pusher-js";

let pusher = null;

const initializePusher = () => {
  pusher = new Pusher(process.env.REACT_APP_PUSHER_KEY, { cluster: "mt1" });
  Pusher.logToConsole = false;
};

export const subscribeToPusher = (events) => {
  !pusher && initializePusher();
  for (let chanelName in events) {
    const channel = pusher.subscribe(
      process.env.REACT_APP_PUSHER_SLOT + chanelName
    );

    for (let eventName in events[chanelName]) {
      channel.bind(eventName, events[chanelName][eventName]);
    }
  }
};
