import React, { memo } from "react";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import { translate } from "../../../utils/translate";
import MathJax from "react-mathjax-preview";
import { useSelector } from "react-redux";
function toOrder(num) {
  if (num == 1) return "1st";
  if (num == 2) return "2nd";
  if (num == 3) return "3rd";
  if (num > 3) return num + "th";
}
function Counter(num, singular) {
  if (num == 1) return num + " " + singular;
  else return num + " " + singular + "s";
}
function Topics({ topic, currentActiveTopic, groupLength }) {
  return (
    <>
      <div className=" row gu-assignment_label mt-3 ">Topic</div>
      <div className="form-group row topic-area mt-2 ">
        <div className="col-sm-10 p-0">
          {topic?.topic_text && topic?.topic_text.includes("<math") ? (
            <MathJax math={topic?.topic_text} />
          ) : (
            <div
              className="gu-assignment_topic_text pt-1"
              /* className="assignment_topic_text " */
              dangerouslySetInnerHTML={{ __html: topic?.topic_text }}
            />
          )}
        </div>
        <div className="col-sm-2">
          <p
            className="gu-assignment_group_questionCounter
            gu-assignment_group_question_Counter "
          >
            {toOrder(currentActiveTopic + 1)} / {Counter(groupLength, "Topic")}
          </p>
        </div>

        {topic?.attachments && (
          <div className="col-sm-10" style={{ textAlign: "center" }}>
            {["png", "jpeg"].includes(topic?.attachments?.mime_type) ? (
              <ImageEnlarge
                imageSrc={resourcesBaseUrl + topic?.attachments?.url}
              />
            ) : (
              <a
                class="btn btn-primary"
                href={resourcesBaseUrl + topic?.attachments?.url}
                alt="download"
              >
                {translate("Download")}
              </a>
            )}
          </div>
        )}
      </div>
    </>
  );
}

export default memo(Topics);
