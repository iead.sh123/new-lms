import react, { useState } from "react"
import RRLiveSessions from "./RRLiveSessions";
import Helper from "components/Global/RComs/Helper";
import { useDispatch } from "react-redux";
import { joinMeetingToGetAttendLinkAsync } from "store/actions/global/liveSession.actions";

function GLiveSessions({liveSessions,userType}){
    const dispatch=useDispatch();
//     const liveSessions1=[
//     {time:"11:30",AmPm:"Am",cours:'course name',lessonTitle:"the best way to connect people" ,leftTime:"5 Min"},
//     {time:"2:30",AmPm:"pm",cours:'course name',lessonTitle:"the best way to connect people" ,},
//     {time:"4:30",AmPm:"pm",cours:'course name',lessonTitle:"the best way to connect people" ,},

//    ]
  
    return(
        <div style={{width:'100%',marginLeft:'1rem'}}>
{/* {Helper.js(liveSessions,"live Sessions")} */}
            {liveSessions?.map(i=>
            <>
            {/* {Helper.js(i,"i")} */}
            <RRLiveSessions
            onClickAction={()=>{dispatch(joinMeetingToGetAttendLinkAsync(i?.meetingId,userType))}}
            meetingId={i?.meetingId}
            userType={userType}
            time={i.time} AmPm={i.AmPm} course={i.cours} lessonTitle={i.lessonTitle}
            leftTime={i.leftTime }/></>
            )}
         
        </div>
    )
}
export default GLiveSessions;