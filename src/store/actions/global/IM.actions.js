import { IMApi } from "api/IM2";

import {
  INITIALIZE_IM,
  GET_MESSSAGES,
  SELECT_CHAT,
  PUSH_MESSAGE,
  CHANGE_POINTER,
  PUSH_CONVERSATION,
  CHANGE_STATUS,
  CHANGE_MESSAGE_STATUS,
  GET_CONVERSATIONS,
} from "./globalTypes";

export const MessageStatus = {
  ERROR: "ERROR",
  PENDING: "PENDING",
  SUCCESS: "SUCCESS",
};

const idsGenerator = (function* () {
  let i = 1;
  while (true) yield i++;
})();

export const initializeConversations = () => async (dispatch) => {
  // const users_response = await IMApi.getAllUsers();
  // if (!users_response.data?.status)
  //     throw new Error(users_response.data?.msg);

  const converstions_response = await IMApi.getAllConversations();
  if (!converstions_response.data?.status)
  return ;  
  //throw new Error(converstions_response.data?.msg);

  // const participants = converstions_response.data?.reduce((prev, curr) => [...prev, ...curr.participants.map(pc=>({...pc,conversationId: curr.conversationId}))], []);

  dispatch({
    type: INITIALIZE_IM,
    payload: {
      // users: {

      //     byId: users_response.data?.users?.reduce((prev, curr) => ({
      //         ...prev,
      //         [curr.id]: curr
      //     }), {}) ?? {},

      //     ids: users_response.data?.users.map(usr => usr.id) ?? []
      // },
      conversations: {
        byId:
          converstions_response.data?.data?.data?.reduce(
            (prev, curr) => ({
              ...prev,
              [curr.conversationId]: {
                id: curr.conversationId,
                lastMessages: {
                  byId:
                    curr.lastReceivedMessages /*?.messages*/
                      ?.reduce(
                        (prev, curr) => ({
                          ...prev,
                          [curr.id]: curr,
                        }),
                        {}
                      ) ?? {},
                  ids:
                    curr.lastReceivedMessages /*?.messages*/
                      ?.map((pc) => pc.id) ?? [],
                  nextPage: curr.lastReceivedMessages.pageId,
                },
                ...curr.conversationMetadata,
                participants: {
                  byId:
                    curr?.participants?.reduce(
                      (prev, curr) => ({
                        ...prev,
                        [curr.id]: curr,
                      }),
                      {}
                    ) ?? {},
                  ids: curr?.participants?.map((pc) => pc.id) ?? [],
                },
              },
            }),
            {}
          ) ?? {},
        ids:
          converstions_response.data?.data?.data?.map(
            (conv) => conv.conversationId
          ) ?? [],
        nextPage: converstions_response.data?.data?.nextPageId,
      },
    },
  });
};

export const fetchMoreMessages =
  (id, nextPage = null) =>
  async (dispatch) => {
    const response = await IMApi.fetchMoreMessages(id, nextPage);
    if (!response.data?.status) throw new Error(response.data?.msg);

    dispatch({
      type: GET_MESSSAGES,
      payload: {
        byId:
          response.data?.data?.messages?.reduce(
            (prev, curr) => ({
              ...prev,
              [curr.id]: { ...curr, conversationId: id },
            }),
            {}
          ) ?? {},
        ids: response.data?.data?.messages?.map((msg) => msg.id) ?? [],
        nextPage: response.data?.data?.id,
      },
    });
  };

export const fetchMoreConversations = (nextPage) => async (dispatch) => {
  const response = await IMApi.getAllConversations(nextPage);
    if (!response.data?.status) 
        return;
        //throw new Error(response.data?.msg);

  dispatch({
    type: GET_CONVERSATIONS,
    payload: {
      byId:
        response.data?.data?.data?.reduce(
          (prev, curr) => ({
            ...prev,
            [curr.conversationId]: {
              id: curr.conversationId,
              lastMessages: {
                byId:
                  curr.lastReceivedMessages /*?.messages*/
                    ?.reduce(
                      (prev, curr) => ({
                        ...prev,
                        [curr.id]: curr,
                      }),
                      {}
                    ) ?? {},
                ids:
                  curr.lastReceivedMessages /*?.messages*/
                    ?.map((pc) => pc.id) ?? [],
                nextPage: curr.lastReceivedMessages.pageId,
              },
              ...curr.conversationMetadata,
              participants: {
                byId:
                  curr?.participants?.reduce(
                    (prev, curr) => ({
                      ...prev,
                      [curr.id]: curr,
                    }),
                    {}
                  ) ?? {},
                ids: curr?.participants?.map((pc) => pc.id) ?? [],
              },
            },
          }),
          {}
        ) ?? {},
      ids: response.data?.data?.data?.map((conv) => conv.conversationId) ?? [],
      nextPage: response.data?.data?.nextPageId,
    },
  });
};

export const sendMessage = (chatId, message) => async (dispatch) => {
  const tempId = "msg" + idsGenerator.next().value;
  dispatch({
    type: PUSH_MESSAGE,
    payload: { ...message, id: tempId, status: MessageStatus.PENDING },
  });

  try {
    const response = await IMApi.sendToChat(chatId, {
      message: message.content,
      attachment: message.attachments[0],
      sendDate: message.sendDate,
    });
    if (!response.data?.status) throw new Error(response.data?.msg);

    dispatch({
      type: CHANGE_MESSAGE_STATUS,
      payload: {
        ...message,
        id: tempId,
        newId: response.data?.data,
        status: MessageStatus.SUCCESS,
      },
    });
  } catch (err) {
    dispatch({
      type: CHANGE_MESSAGE_STATUS,
      payload: {
        ...message,
        id: tempId,
        newId: null,
        status: MessageStatus.ERROR,
      },
    });
    throw err;
  }
};

export const recieveMessage = (data) => (dispatch) => {
  dispatch({ type: PUSH_MESSAGE, payload: data });
};

export const recieveConversation = (data) => (dispatch) => {
  dispatch({
    type: PUSH_CONVERSATION,
    payload: {
      conversation: {
        id: data.conversationId ?? data.groupId,
        name: data.name,
        numberOfUnreadMessages: 0,
        photoPath: "",
      },
      participants: data.user2
        ? [data.user2, data.user1]
        : [] /* data.participants.map(id=>({id: id,username: "",}))*/,
    },
  });
};

export const createChat =
  (recievers, message, currentUser, groupName) => async (dispatch) => {
    groupName = groupName ? { name: groupName } : {};

    const response = await IMApi.createConversation({
      users_id: recievers.map((rc) => rc.id),
      message: message.content,
      ...groupName,
    });

    if (!response.data?.status) throw new Error(response.data?.msg);

    dispatch({
      type: PUSH_CONVERSATION,
      payload: {
        conversation: {
          id: response.data?.data?.ConversationId,
          name: groupName.name,
          numberOfUnreadMessages: 0,
          photoPath: "",
        },
        message: { id: response.data?.data?.MessageId, ...message },
        participants: [
          ...recievers,
          { id: currentUser.id, username: currentUser.userName },
        ],
      },
    });
  };

export const changePointer = (chatId) => async (dispatch) => {
  const response = await IMApi.changePointer(chatId);
  if (!response.data.status) throw new Error(response.data.msg);
  dispatch({ type: CHANGE_POINTER, payload: chatId });
};

export const selectChat = (id) => ({
  type: SELECT_CHAT,
  payload: id,
});
