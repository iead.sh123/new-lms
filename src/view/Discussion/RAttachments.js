import React, { useState } from "react";
import { Services } from "engine/services";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RAttachments = ({ attachments, specificMandatoryPost }) => {
  const [selectedImageIndex, setSelectedImageIndex] = useState(null);

  const handleImageClick = (index) => {
    setSelectedImageIndex(index);
    // Open the modal here or perform any other action you need
  };

  const getImageStyle = (index) => {
    if (attachments.length === 1) {
      return { width: "100%" };
    } else {
      const distance = 100 / (attachments.length - 1);
      const marginLeft = index === 0 ? 0 : `${distance}%`;
      return { marginLeft };
    }
  };
  return (
    <div
      style={{ display: specificMandatoryPost ? "block" : "flex" }}
      className="mt-4"
    >
      {attachments.slice(0, 3).map((attachment, index) => (
        <div key={attachment.hash_id} style={{ position: "relative" }}>
          <img
            src={`${Services.discussion.file + attachment?.hash_id}`}
            style={{
              border: "1px solid #333",
              filter: index === 1 ? "blur(5px)" : "none",
              width: "300px",
              //   ...getImageStyle(index),
            }}
            onClick={() => index === 1 && handleImageClick(index)}
          />
          {index === 1 && (
            <div
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
                color: "#fff",
                padding: "5px",
                borderRadius: "50%",
              }}
            >
              +2
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default RAttachments;
