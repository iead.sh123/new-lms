import React from 'react';
import RModal from "components/Global/RComs/RModal";
import tr from "components/Global/RComs/RTranslator";
import { FormGroup, Input, Label, FormText, Row, Col } from "reactstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import RButton from 'components/Global/RComs/RButton';
import RFlex from 'components/Global/RComs/RFlex/RFlex';

const CreateOtherModal = ({ edited, handleSave, toggle, isOpen }) => {

    const [formItems, setFormItems] = React.useState({
        name: ''
    });


    React.useEffect(() => {
        if (edited)
            setFormItems({ name: edited.name });
    }, [edited?.id]);


    const handleChange = (e) => setFormItems({ ...formItems, [e.target.name]: e.target.value });

    const handleCreate = async () => {
        await handleSave({ ...formItems })
        toggle();

    };

    return <RModal isOpen={isOpen}
        withCloseIcon={false}
        toggle={toggle}
        header={<p>{tr`Create New`}</p>}
        footer={
            <RFlex styleProps={{ width: '100%', justifyContent: 'end' }}>
                <RButton
                    text={tr`Save`}
                    color="primary"
                    onClick={handleCreate}
                // style={{alignSelf: 'end'}}
                />

                <RButton
                    text={tr`Cancel`}
                    style={{ background: 'white', color: '#668AD7' }}
                    // faicon="fa fa-plus"
                    // color="link"
                    onClick={toggle}
                // style={{alignSelf: 'end'}}
                />
            </RFlex>
        }
        body={<div>

            <Input
                name="name"
                type="text"
                // placeholder={tr`Name`}
                value={formItems.name}
                onChange={handleChange}
            />
        </div >} />

}



export default CreateOtherModal;