import withDirection from "hocs/withDirection";
import React from "react";
import { Button } from "reactstrap";

function RBack({ onBack, className, dir }) {
	return (
		<Button className={className ? className : "R-Navigator-btn button_text"} onClick={onBack}>
			<i
				className={dir ? `nc-icon nc-minimal-left R-Navigator-icon` : `nc-icon nc-minimal-right R-Navigator-icon`}
				style={{ top: "50%", left: "50%" }}
			></i>
		</Button>
	);
}
export default withDirection(RBack);
