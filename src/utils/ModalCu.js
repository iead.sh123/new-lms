import React, { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal';


function ModalCu({ children, show, header,footer, parentHandleClose, widthModal }) {


    const handleClose = () => {
        parentHandleClose(false)
    }
    return (
        <Modal
            show={show}
            onHide={handleClose}
            contentClassName="modal-content-cu"
            style={{ '--my-width-var': widthModal }}
        >
            <Modal.Header closeButton>
                <span style={{ textAlign: 'center' }}>{header}</span>
            </Modal.Header>
            <Modal.Body contentClassName="modal-content-cu">
                <div className="modal-content back-ground" >
                    <div className="modal-body-cu">
                        <span>
                            {children}
                        </span>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                {footer}
            </Modal.Footer>
        </Modal>

    );
}
export default ModalCu;
