import {SET_CURRENT_NODE} from '../global/globalTypes';

const setCurrentNode = payload=>({
    type: SET_CURRENT_NODE,
    payload: payload,
});

export default setCurrentNode;