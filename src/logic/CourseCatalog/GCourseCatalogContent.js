import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { Row, Col } from "reactstrap";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";

import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import GCourseAdd from "./GCourseAdd";
import Helper from "components/Global/RComs/Helper";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import GCoursesCatalogFilter from "./GCoursesCatalogFilter";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";

import GCourseCards from "./GCourseCards";
import GCoursetable from "./GCoursetable";
import { publishAndUnPublishCourseAsync } from "store/actions/global/coursesManager.action";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { removeCourseAsync } from "store/actions/global/coursesManager.action";
import RLiveAction from "components/Global/RComs/Collaboration/RLiveAction";
import produce from "immer";
import RToggle from "components/RComponents/RToggle";

const GCourseCatalogContent = ({ history, reloadCategories, courseSilentLoading, setCourseSilentLoading }) => {
	const dispatch = useDispatch();

	const [searchData, setSearchData] = useState("");
	const [courseType, setCourseType] = useState([]);
	const [tabTitle, setTabTitle] = useState("");
	const [tableView, setTableView] = useState(true);
	const [editedId, setEditedId] = useState(-1);

	const [alert1, setAlert] = useState(false);
	const { categoryId } = useParams();
	const { coursesByCategory, coursesStatistics, coursesByCategoryLoading } = useSelector((state) => state.coursesManagerRed);
	const [filterCredits, setFilterCredits] = useState(0);

	const [filterStatus, setFilterStatus] = useState(null);
	const [filtering, setFiltering] = useState(false);
	const [courses, setCourses] = useState([]);
	const [openAdd, setOpenAdd] = useState(false);

	useEffect(() => {
		if (!openAdd) {
			setCourseId(null);
			setCourseName(null);
			setCourseCode(null);
			setCourseCredits(null);
			setCourseCategory(null);
			setCourseImage(null);
			setCourseDescription(null);
		}
	}, [openAdd]);
	//--------------------------course add edit
	const [courseId, setCourseId] = useState(null);
	const [courseName, setCourseName] = useState("");
	const [courseCode, setCourseCode] = useState("");
	const [courseCredits, setCourseCredits] = useState(0);
	const [courseCategory, setCourseCategory] = useState(null);
	const [courseImage, setCourseImage] = useState();
	const [courseDescription, setCourseDescription] = useState();

	const [modalMode, setModalMode] = useState();
	//-------------------------------
	const filter = () => {
		setFiltering(true);
		setCourseSilentLoading(false);
		searchCatalog({
			min_credits: filterCredits ? +filterCredits : null,
			max_credits: filterCredits ? +filterCredits : null,
			name: searchData,
			category_id: categoryId,
			status: filterStatus,
			noPagination: true,
		});
	};

	const filterreset = () => {
		setFiltering(true);
		setCourseSilentLoading(false);
		searchCatalog({
			min_credits: null,
			max_credits: null,
			name: searchData,
			category_id: categoryId,
			status: "all",
			noPagination: true,
		});
	};
	useEffect(() => {
		setCourseSilentLoading(false);
		setSearchData("");
		setFiltering(false);
		setFilterCredits(null);
		setFilterStatus("all");
	}, [categoryId]);
	//-----------------------------------------------------Coures Actions
	const setloading = (id) => {
		setCourses((prevCourses) =>
			produce(prevCourses, (draft) => {
				const courseToUpdate = draft.find((course) => course.id === id);

				if (courseToUpdate) {
					courseToUpdate.loading = true;
				}
			})
		);
	};

	const publishAndUnPublishCourse = (id, catid, ispublished) => {
		setCourseSilentLoading(true);
		dispatch(publishAndUnPublishCourseAsync(id, catid, ispublished));
		reload();
	};

	const editCourseInformation = (cid) => {
		setCourseSilentLoading(true);
		//alert(JSON.stringify(course?.image_id))

		const course = courses.filter((c) => c.id == cid)?.[0];
		//alert(course?.name)
		setCourseId(course?.id);
		setCourseName(course?.name);
		setCourseCode(course?.code);
		setCourseCredits(course?.credits);
		setCourseCategory(course?.category_id);
		setCourseDescription(course?.overview?.description);
		setCourseImage(course?.image_id);
		setModalMode("edit");
		setOpenAdd(true);

		//setEditedId(cid);
	};

	const editCourseInformation1 = (cid) => {
		//setEditedId(cid);
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const successDelete = async (prameters) => {
		setCourseSilentLoading(true);
		dispatch(removeCourseAsync(prameters.courseId, hideAlert, prameters?.isPublished, categoryId));
		reload();
	};

	const handleRemoveCourse = (courseId) => {
		const prameters = {
			courseId,
		};
		const message = (
			<div>
				<h6>{tr`Are you sure? `}</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
			</div>
		);
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	//-----------------------------------------------------
	const { courseManagerTree } = useSelector((state) => state.coursesManagerRed);
	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const searchCatalog = (data) => {
		setCourseSilentLoading(true);

		Helper.fastGet(
			`${Services.courses_manager.backend}api/course-catalogue/filter?query=${JSON.stringify(data)}`,
			//	"http://localhost:3000/courses.json",
			"error getting",
			(response) => {
				if (response && response.data && response.data.data)
					setCourses(
						response.data.data.map((c) => ({
							...c,
							code: c.code && c.code.length ? c.code : "-",
							category_name: c.category_name && c.category_name.length ? c.category_name : "No program",
						}))
					);
				else setCourses([]);
			}
		);
	};

	const searchRadioButtonArray = [
		// {
		// 	id: 1,
		// 	name: tr`Search this progam`,
		// 	categoryId: categoryId ? true : false,
		// 	style:{fontSize: '12px'},
		// 	selectedStyle:{color:"#668AD7"}
		// },
		// {
		// 	id: 2,
		// 	name: tr`Search in all programs`,
		// 	categoryId: categoryId ? false : true,
		// 	style:{fontSize: '12px'},
		// 	selectedStyle:{color:"#668AD7"}
		// },
	];

	useEffect(() => {
		if (categoryId) {
			//dispatch(
			//	getCoursesByCategoryIdAsync(
			searchCatalog({
				category_id: categoryId,
				noPagination: 1,
			});
			//);
		} else {
			//dispatch(
			// getCoursesByCategoryIdAsync({
			// 	category_id: null,
			// 	noPagination: 1,
			// })
			searchCatalog({
				category_id: categoryId,
				noPagination: 1,
			});
			//);
		}
	}, [categoryId]);

	const handleSearchOnRadioButton = (id) => {
		if (id == 2) {
			history.push(`${baseURL}/${genericPath}/courses-catalog`);
		}
	};

	const reload = () => {
		handleSearch();
	};
	const handleSearch = () => {
		// const payload = {
		// 	category_id: +categoryId,
		// 	name: emptySearch ?? searchData,
		// 	noPagination: 1,
		// 	course_types: courseType,
		// 	isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
		// };
		// tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;

		// dispatch(getCoursesByCategoryIdAsync(payload));

		if (!filtering)
			searchCatalog({
				name: searchData,
				category_id: categoryId ?? null,
				noPagination: true,
			});

		if (filtering) {
			filter();
		}
	};
	const newCourseCallback = () => {
		setCourseId(null);
		setCourseCategory(categoryId);
		setModalMode("add");
		setOpenAdd(true);
	};
	// const handleSearch1OnCourseType = (data) => {
	// 	setCourseType(data);
	// 	const payload = {
	// 		category_id: categoryId ? categoryId : null,
	// 		noPagination: 1,
	// 		course_types: data,
	// 		isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
	// 		name: searchData,
	// 	};
	// 	tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;

	// 	dispatch(getCoursesByCategoryIdAsync(payload));
	// };

	// const handleSea2rchOnTabs = (tabTitle) => {
	// 	setTabTitle(tabTitle);
	// 	const payload = {
	// 		category_id: categoryId ? categoryId : null,
	// 		noPagination: 1,
	// 		course_types: courseType,
	// 		isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
	// 		name: searchData,
	// 	};
	// 	tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;
	// 	dispatch(getCoursesByCategoryIdAsync(payload));
	// };

	return (
		<section>
			<React.Fragment>
				<React.Fragment>
					{alert1}
					<Row id="catalog_search_row" style={{ display: "flex", alignItems: "center" }}>
						<Col md={9} sm={12} className="m-0 p-0">
							<RSearchHeader
								searchLoading={coursesByCategoryLoading}
								searchData={searchData}
								handleSearch={handleSearch}
								setSearchData={setSearchData}
								handleChangeSearch={handleChangeSearch}
								inputPlaceholder={tr`search`}
								addNew={false}
								searchRadioButton={searchRadioButtonArray}
								handleSearchOnRadioButton={handleSearchOnRadioButton}
								searchInputStyle={{
									//maxWidth:"320px",
									padding: "6px 16px",
									fontSize: "12px",
								}}
								catalog={true}
							/>
						</Col>
						{/* <Col md={3} sm={12} className="m-0 p-0">
							<RSelect
								name="course_type"
								option={[
									{ label: "Master Courses", value: "isMaster" },
									{ label: "Instance Courses", value: "isInstance" },
									{ label: "Private Courses", value: "isPrivate" },
								]}
								onChange={(e) => handleSearchOnCourseType(e.map((el) => el.value))}
								isMulti={true}
							/>
						</Col> */}

						{/* <Col md={2} sm={12}>
							 <RFilterTabs
								tabs={coursesStatistics && coursesStatistics.length > 0 && coursesStatistics}
								handleSearchOnTabs={handleSearchOnTabs}
								firstSelectedTab="All"
							/>
						</Col> */}

						<Col md={3} sm={12} style={{ display: "flex", height: "30px", justifyContent: "end" }}>
							{/*	<RButton
								text={tr`create_new_course`}
								faicon="fa fa-plus"
								onClick={() =>
									history.push({
										pathname: `${baseURL}/${genericPath}/courses-manager/editor${
											categoryId ? `/category/${categoryId}` : ""
										}/course-information`,
										state: { emptyStore: true },
									})
								}
								color="primary"
							/>*/}
							<RLiveAction
								style={{
									borderRadius: "2px",
									border: "1px solid #668AD7)",
									fontWeight: "500",
									lineHeight: "normal",
									textTransform: "capitalize",
									paddingLeft: "10px",
									paddingRight: "10px",
									paddingTop: "5px",
									paddingBottom: "5px",
									marginLeft: "5px",
									marginRight: "5px",
								}}
								labelStyle={{
									paddingLeft: "10px",
									paddingRight: "10px",
									paddingTop: "5px",
									paddingBottom: "5px",
									marginLeft: "5px",
									marginRight: "5px",
								}}
								color="#668AD7"
								border={true}
								text={"Show All Courses"}
								onClick={() => {
									history.push(`${baseURL}/${genericPath}/courses-catalog`);
								}}
							/>

							<RLiveAction
								style={{
									borderRadius: "2px",
									border: "1px solid #668AD7)",
									fontWeight: "500",

									lineHeight: "normal",
									textTransform: "capitalize",
									paddingLeft: "10px",
									paddingRight: "10px",
									paddingTop: "5px",
									paddingBottom: "5px",
								}}
								labelStyle={{
									paddingLeft: "10px",
									paddingRight: "10px",
									paddingTop: "5px",
									paddingBottom: "5px",
								}}
								color="#668AD7"
								border={true}
								text={"New Course"}
								onClick={newCourseCallback}
							/>

							<GCourseAdd
								openAdd={openAdd}
								setOpenAdd={setOpenAdd}
								courseId={courseId}
								courseName={courseName}
								setCourseName={setCourseName}
								courseCode={courseCode}
								setCourseCode={setCourseCode}
								courseCredits={courseCredits}
								setCourseCredits={setCourseCredits}
								courseCategory={courseCategory}
								setCourseCategory={setCourseCategory}
								courseDescription={courseDescription}
								setCourseDescription={setCourseDescription}
								courseImage={courseImage}
								setCourseImage={setCourseImage}
								categories={courseManagerTree}
								mode={modalMode}
								successCallback={() => {
									reload();
									setOpenAdd(false);
									reloadCategories?.();
									Helper.cl(courseCategory, "course category set after relaod");
									categoryId && history.push(`${baseURL}/${genericPath}/courses-catalog/category/${courseCategory}`);
								}}
							/>
						</Col>
					</Row>
					{courses && courses.length ? (
						<Row>
							<RFlex style={{ display: "flex", height: "40px", justifyContent: "space-between", alignItems: "flex-start", width: "100%" }}>
								<GCoursesCatalogFilter
									filterCredits={filterCredits}
									setFilterCredits={setFilterCredits}
									filterStatus={filterStatus}
									setFilterStatus={setFilterStatus}
									filter={filter}
									filterreset={filterreset}
								/>
								<div style={{ height: "24px" }}>
									<RToggle
										value={tableView}
										onColor={"success"}
										offColor={"gray"}
										style={{ height: "18px" }}
										onText={<i className="fa fa-list"></i>}
										offText={<i className="fa fa-table"></i>}
										onChange={setTableView}
									/>
								</div>
							</RFlex>
						</Row>
					) : (
						<></>
					)}
					<Row>{/* <RButton text={"Edit"} onClick={editCourseInformation1(1243)}/> */}</Row>
					{coursesByCategoryLoading && !courseSilentLoading ? (
						<Loader />
					) : (
						<React.Fragment>
							{tableView ? (
								<div>
									<GCoursetable
										courses={courses}
										showCategory={!categoryId}
										publishAndUnPublishCourseAsync={publishAndUnPublishCourse}
										editCourseInformation={editCourseInformation}
										handleRemoveCourse={handleRemoveCourse}
										newCourseCallback={newCourseCallback}
										setloading={setloading}
									/>
								</div>
							) : (
								// <GCourseCard
								// 	payloadData={{
								// 	category_id: categoryId ? categoryId : null,
								// 	noPagination: 1,
								// 	course_types: courseType,
								// 	isPublished: tabTitle == "published" ? true : tabTitle == "draft" ? false : null,
								//     name: searchData
								// 	}}
								// 	/>
								<GCourseCards
									courses={courses}
									publishAndUnPublishCourseAsync={publishAndUnPublishCourse}
									editCourseInformation={editCourseInformation}
									handleRemoveCourse={handleRemoveCourse}
									newCourseCallback={newCourseCallback}
								/>
							)}
						</React.Fragment>
					)}
				</React.Fragment>
			</React.Fragment>
		</section>
	);
};

export default GCourseCatalogContent;
