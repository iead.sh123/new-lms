
import RAdvancedLister from "components/Global/RComs/RAdvancedLister"; 
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React,{useState} from 'react';
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import RRow from "view/RComs/Containers/RRow";
import RColumn from "view/RComs/Containers/RColumn";
  //   import { useState } from 'react';
 import { useEffect } from 'react';
import RTagsViewer from "components/Global/RComs/Collaboration/RTagsViewer";

import RBieChart from "components/Global/RComs/RBieChart";
import RRecentActivities from "components/Global/RComs/Collaboration/RRecentActivities";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { GCollaborationAreaProviders } from "./GCollaborationAreaProviders";
// import Helper from 'components/Global/RComs/Helper';
// import { Services } from 'engine/services';
export const GCollaborationAreaConsumedContents=({setNoData=(x)=>{},setCount=()=>{}})=>{
  const history = useHistory();


const [consumedContentsData,setConsumedContentsData]=useState([]);
const [consumedContentsRecords,setConsumedContentsRecords]=useState([]);


const getConsumedContentsFromBackend1 = async (specific_url) => {
   
  const url = Services.collaboration.backend+`api/collaboration-area/consumed-contents`;
  let response1 = await get(specific_url ? specific_url : url);
  
  if (response1) {
   //Helper.cl(response1,"co cl response1")
    return response1;
  }
};
const setConsumedContentsData1=(response)=>{setConsumedContentsData(response?.data?.data)};

useEffect(()=>{

  if(!(consumedContentsData&&consumedContentsData.length>0)) if(typeof setNoData === 'function' && setNoData !== null) setNoData(true);
  if(typeof setCount === 'function' && setCount !== null) setCount(consumedContentsData?.length);
  setConsumedContentsRecords(
    consumedContentsData?.map((r, index) => {
      return {
        specialProps:

        // "sharable_content_name": "Lesson Plans",
        // "name": "lessonplan_42",
        // "source_organization": {
        //     "name": "HazemProvider",
        //     "image": null
        // }

         {  title : { text: r.name},
          subtitle : { text: r.sharable_content_name},
          Author:{image:r.source_organization.image, text:r.source_organization.name},
           //UpLeftIcon:"All:86", 
           //Actions: [{title: r.active?"Activate":"Deactivate", icon: r.active?"fa fa-power-off":"fa fa-power-off", color:r.active?"green":"red" , 
                //     onClick:()=>{activate(r.id,!r.active)},
                   //  withBorder:false}
                 //  ],
                     bottomBorderColor:"",
                     Description :r.updated_at
                     },
           // active:1
           // approved_by_destination:0
           // created_at:"2023-07-20T06:27:28.000000Z"
           // destination_org_id:8
           // id:12
           // source_org_id:1


         
   }
 }
 )
 )
},[consumedContentsData])



     return <div>

  
         <RAdvancedLister
       hideTableHeader={false}
       colorEveryOtherRow={false}
       //firstCellImageProperty={"image"}
       getDataFromBackend={getConsumedContentsFromBackend1}
       setData={setConsumedContentsData1}
       records={consumedContentsRecords}
       getDataObject={(response) => response.data?.data?.data}
       characterCount={15}
       marginT={"mt-3"}
       marginB={"mb-2"}
       align={"left"}
       SpecialCard={RCollaborationCard}
       showListerMode={"cardLister"}
       swiper
       perLine={4}
/>

</div> 
}