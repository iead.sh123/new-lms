import SweetAlert from "react-bootstrap-sweetalert";
import tr from "./RTranslator";

export const deleteSweetAlert = (
	showAlerts,
	hideAlert,
	successDelete,
	prameters,
	message = tr`are_you_sure_to_delete_it`,
	confirm = tr`yes_delete_it`
) => {
	showAlerts(
		<SweetAlert
			title={
				<div
					style={{
						fontSize: "15px",
						fontWeight: "600",
						textTransform: "capitalize",
					}}
				>
					{message}
				</div>
			}
			onConfirm={() => { successDelete(prameters); hideAlert() }}
			onCancel={() => hideAlert()}
			confirmBtnBsStyle="danger"
			confirmBtnText={confirm}
			cancelBtnText={tr`cancel`}
			showCancel
			confirmBtnStyle={{
				color: "#FFF",
				fontSize: "12px",
				fontWeight: "400",
				textTransform: "capitalize",
				display: "flex",
				alignItems: "center",
				justifyContent: "center",
			}}
			cancelBtnStyle={{
				color: "black",
				fontSize: "12px",
				fontWeight: "400",
				textTransform: "capitalize",
				display: "flex",
				alignItems: "center",
				justifyContent: "center",
			}}
		/>
	);
	return;
};

export const nodeleteSweetAlert = (showAlerts, message, hideAlert) => {
	showAlerts(
		<SweetAlert
			title={
				<div
					style={{
						fontSize: "15px",
						fontStyle: "normal",
						fontWeight: "600",
						lineHeight: "normal",
						textTransform: "capitalize",
					}}
				>
					{message ? message : tr`are_you_sure_to_delete_it`}
				</div>
			}
			onCancel={() => hideAlert()}
			onConfirm={() => hideAlert()}
			confirmBtnStyle={{
				color: "#FFF",
				fontFamily: "Cairo",
				fontSize: "12px",
				fontStyle: "normal",
				fontWeight: "400",
				lineHeight: "normal",
				textTransform: "capitalize",
				display: "flex",
				padding: "5px 25px",
			}}
		></SweetAlert>
	);
	return;
};
