import {
  ADD_NEW_FILTER,
  DELETE_NEW_FILTER,
  ADD_VALUES_TO_THE_FILTER,
  ADD_NAME_TO_THE_FILTER,
  RESET_FILTERS,
} from "store/actions/admin/adminType";

export const addNewFilter = (data) => {
  return (dispatch) => {
    dispatch({
      type: ADD_NEW_FILTER,
      data,
    });
  };
};

export const deleteNewFilter = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_NEW_FILTER,
      id,
    });
  };
};

export const addValuesToTheFilter = (inputValue, id) => {
  return (dispatch) => {
    dispatch({
      type: ADD_VALUES_TO_THE_FILTER,
      inputValue,
      id,
    });
  };
};

export const addNameToTheFilter = (nameFilter, id) => {
  return (dispatch) => {
    dispatch({
      type: ADD_NAME_TO_THE_FILTER,
      nameFilter,
      id,
    });
  };
};

export const resetFilters = () => (dispatch) =>
  dispatch({ type: RESET_FILTERS, payload: null });
