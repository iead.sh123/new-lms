import { get } from "config/api";

export const quoteApi = {
  getQuote: () => get(`api/quote/getActive`),
  getMonthlyTrait: () => get(`api/monthly_trait/getActive`),
};
