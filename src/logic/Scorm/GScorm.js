import React from "react";
import RFileUploaderAsChunks from "components/RComponents/RFileUploaderAsChunks";
import { Services } from "engine/services";

const GScorm = ({ moduleId, handlePushScormToModule }) => {
	return (
		<RFileUploaderAsChunks
			parentCallbackToFillData={handlePushScormToModule}
			typeFile={[".zip"]}
			contextId={moduleId}
			contextName="modules"
			apiURL={`${Services.scorm.backend}api/scorm-aggregates`}
		/>
	);
};

export default GScorm;
