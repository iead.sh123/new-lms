import React, { useState } from "react";
import { Row, Col, FormGroup, Input, Label, Collapse } from "reactstrap";
import styles from "../CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useSelector } from "react-redux";
import { primaryColor } from "config/constants";
import { dangerColor } from "config/constants";
import hands from "assets/img/new/hands.png";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RButton from "components/Global/RComs/RButton";
import RCardUser from "components/Global/RComs/RCardUser/RCardUser";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RCardUserFeedback from "components/Global/RComs/RCardUserFeedback/RCardUserFeedback";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import RFeedBack from "./RFeedBack";
import { boldGreyColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import fairLevel from "assets/img/new/svg/fair.svg";
import expertLevel from "assets/img/new/svg/expert.svg";
import intermediateLevel from "assets/img/new/svg/intermediate.svg";
import beginnerLevel from "assets/img/new/svg/beginner.svg";

const RCourseOverView = ({
	handelSetItemsToCourseOverView,
	handelChangeItemsInCourseOverView,
	handelDeleteItemsInCourseOverView,
	handleSaveCourseOverView,
}) => {
	const { courseById, allFeedback, saveCourseOverViewLoading, allFeedbackLoading, courseStudentMode } = useSelector(
		(state) => state.coursesManagerRed
	);

	const [seeAllLanguages, setSeeAllLanguages] = useState(false);

	const [showLearnersFeedback, setShowLearnersFeedback] = useState(false);
	const [showAllLearnersFeedback, setShowAllLearnersFeedback] = useState(false);
	const [openedCollapses, setOpenedCollapses] = useState(null);

	const toggleQuestionCollapse = (id) => {
		const isOpenSection = "collapse" + id;
		if (isOpenSection == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const handleSeeAllLanguages = () => {
		setSeeAllLanguages(!seeAllLanguages);
	};

	const handleShowAllLearnersFeedback = () => {
		setShowAllLearnersFeedback(true);
	};
	const handleCloseAllLearnersFeedback = () => {
		setShowAllLearnersFeedback(false);
	};

	return (
		<React.Fragment>
			<AppModal
				size="xl"
				show={showAllLearnersFeedback}
				parentHandleClose={handleCloseAllLearnersFeedback}
				header={
					<Row>
						<Col xs={12} sm={12}>
							{allFeedback?.feedbacks?.length} {tr`Rate`}
						</Col>
					</Row>
				}
				headerSort={<RFeedBack feedbacks={allFeedback} allFeedbackLoading={allFeedbackLoading} />}
			/>

			<Row>
				<Col xs={12} md={8} className="pl-0">
					<Row className="mb-1">
						<Col xs={8} className="pl-0">
							<RFlex>
								<h6 className="pt-2">{courseById?.name}</h6>
								{courseById?.category?.category_name && (
									<div className={styles.category_name} style={{ background: courseById?.color ?? primaryColor }}>
										{courseById?.category?.category_name}
									</div>
								)}
							</RFlex>
						</Col>
						{courseById?.category?.start_date && (
							<Col xs={4} className="d-flex justify-content-end">
								<p className="pt-2">
									{tr`start_on`}: {courseById?.category?.start_date}
								</p>
							</Col>
						)}
					</Row>
					{courseById?.language?.length > 0 && (
						<Row className="mb-4 ">
							<Col xs={12} className="pl-0">
								<RFlex>
									<i className={`${iconsFa6.globe} pt-1`} style={{ color: "#F34400" }} />
									{(seeAllLanguages ? courseById?.language : courseById?.language?.slice(0, 2))?.map((language) => (
										<span key={language.value}>{language?.label}</span>
									))}
									{courseById?.language?.length > 2 && (
										<span onClick={handleSeeAllLanguages} className={`${styles.text_decoration} cursor-pointer`}>
											<span className="pr-2">{!seeAllLanguages && courseById?.language?.length}</span>
											{seeAllLanguages ? tr`Less Languages` : tr`More Languages`}
										</span>
									)}
								</RFlex>
							</Col>
						</Row>
					)}
					<Row className="mb-3">
						<Col xs={12} className="pl-0">
							{courseStudentMode ? (
								<p>{courseById?.overview?.description}</p>
							) : (
								<FormGroup>
									<Label style={{ color: boldGreyColor }}>{tr`description`}</Label>
									<Input
										name="description"
										type="textarea"
										required
										placeholder={tr("describe this course")}
										onChange={(e) =>
											handelChangeItemsInCourseOverView({
												name: e.target.name,
												value: e.target.value,
												key: null,
											})
										}
										value={courseById?.overview?.description}
									/>
								</FormGroup>
							)}
						</Col>
					</Row>

					{/* what you will learn */}
					<Row className="mb-3">
						<Col xs={12} className="pl-0">
							<h6>{tr`what you will learn`}</h6>
						</Col>
						{!courseStudentMode && (
							<Col xs={12} className="pl-0">
								<RFlex>
									<div
										style={{ color: primaryColor }}
										className="d-flex cursor-pointer"
										onClick={(e) =>
											handelSetItemsToCourseOverView({
												key: "what_you_will_learn",
											})
										}
									>
										<i className="fa fa-plus pr-2 pl-0 pt-1" />
										<p>{tr`add_new`}</p>
									</div>
									<p
										className="text-muted"
										style={{ fontSize: "13px" }}
									>{tr`for example : you will learn calculating fractions and complex numbers`}</p>
								</RFlex>
							</Col>
						)}

						{courseById?.overview && courseById?.overview?.what_you_will_learn && (
							<Col xs={12} className="pl-0">
								{courseById?.overview &&
									courseById?.overview?.what_you_will_learn.map((el) =>
										courseStudentMode ? (
											<li key={el.id ?? el.fakeId}>{el.item}</li>
										) : (
											<RFlex
												styleProps={{
													alignItems: "center",
													color: dangerColor,
													marginBottom: "10px",
												}}
												key={el.id ?? el.fakeId}
											>
												<Input
													name="item"
													type="text"
													required
													onChange={(e) =>
														handelChangeItemsInCourseOverView({
															name: e.target.name,
															value: e.target.value,
															id: el?.id ?? el?.fakeId,
															key: "what_you_will_learn",
														})
													}
													defaultValue={el.item}
												/>
												<i
													className={iconsFa6.delete + " cursor-pointer"}
													onClick={() =>
														handelDeleteItemsInCourseOverView({
															id: el.id ?? el.fakeId,
															key: "what_you_will_learn",
														})
													}
												/>
											</RFlex>
										)
									)}
							</Col>
						)}
					</Row>
					{/* Who this course is for */}
					<Row className={`${styles.who_this_course} mb-3`}>
						<Col xs={12}>
							<h6>{tr`Who this course is for`}</h6>

							{!courseStudentMode && (
								<RFlex>
									<div
										style={{ color: primaryColor }}
										className="d-flex cursor-pointer"
										onClick={() =>
											handelSetItemsToCourseOverView({
												key: "who_is_this_course_for",
											})
										}
									>
										<i className="fa fa-plus pr-2 pl-0 pt-1" />
										<p>{tr`add_new`}</p>
									</div>
									<p className="text-muted" style={{ fontSize: "13px" }}>{tr`for example : this course is for who trying to learn math`}</p>
								</RFlex>
							)}
							{courseById?.overview &&
								courseById?.overview?.who_is_this_course_for &&
								courseById?.overview?.who_is_this_course_for?.map((el) =>
									courseStudentMode ? (
										<li key={el.id ?? el.fakeId}>{el.item}</li>
									) : (
										<RFlex
											styleProps={{
												alignItems: "flex-start",
												color: dangerColor,
												marginBottom: "10px",
											}}
											key={el.id ?? el.fakeId}
										>
											<Input
												name="item"
												type="text"
												required
												onChange={(e) =>
													handelChangeItemsInCourseOverView({
														name: e.target.name,
														value: e.target.value,
														id: el?.id ?? el?.fakeId,
														key: "who_is_this_course_for",
													})
												}
												value={el.item}
											/>
											<i
												className={iconsFa6.delete + " cursor-pointer"}
												onClick={() =>
													handelDeleteItemsInCourseOverView({
														id: el.id ?? el.fakeId,
														key: "who_is_this_course_for",
													})
												}
											/>
										</RFlex>
									)
								)}
						</Col>
					</Row>
				</Col>

				<Col xs={12} md={4}>
					<Row>
						<Col xs={12} className={`${styles.right_col_design} mb-4`}>
							<RFlex styleProps={{ justifyContent: "space-evenly", alignItems: "center" }}>
								{courseById?.levels?.length > 0 && (
									<RFlex>
										<img
											src={
												courseById?.levels[0]?.name.toLowerCase() == "intermediate"
													? intermediateLevel
													: courseById?.levels[0]?.name.toLowerCase() == "fair"
													? fairLevel
													: courseById?.levels[0]?.name.toLowerCase() == "expert"
													? expertLevel
													: beginnerLevel
											}
											alt={courseById?.levels[0]?.name}
											width={"14px"}
											height={"14px"}
											style={{ position: "relative", top: "3px", color: primaryColor }}
										/>
										<p className="m-0">{courseById?.levels[0]?.name}</p>
									</RFlex>
								)}
								{courseById?.numberOfHours && (
									<RFlex>
										<i className={iconsFa6.clock + " pt-1"} />
										<p className="m-0"> {+courseById?.numberOfHours}</p>
									</RFlex>
								)}
								<RFlex>
									<i className={iconsFa6.globe + " pt-1"} />
									<p className="m-0">{courseById?.isOnline == 1 ? `${tr`online`}` : `${tr`offline`}`}</p>
								</RFlex>
							</RFlex>
						</Col>

						<Col xs={12} className={`${styles.right_col_design} ${styles.overflow_scroll_schedule} mb-4`}>
							<Row>
								<Col xs={12} className="pl-0">
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<p className="font-weight-bold">{tr`course_schedule`}</p>
										{!courseStudentMode && (
											<div
												style={{ color: primaryColor }}
												className="d-flex cursor-pointer"
												onClick={(e) =>
													handelSetItemsToCourseOverView({
														key: "schedule",
													})
												}
											>
												<i className="fa fa-plus pr-2 pt-1" />
												<p>{tr`add_new`}</p>
											</div>
										)}
									</RFlex>
								</Col>
								{courseById?.overview && courseById?.overview?.schedule && (
									<Col xs={12} className="pl-0">
										{courseById?.overview?.schedule.map((el) =>
											courseStudentMode ? (
												<p key={el.id ?? el.fakeId}>{el.value}</p>
											) : (
												<RFlex
													styleProps={{
														alignItems: "center",
														color: dangerColor,
														marginBottom: "10px",
													}}
													key={el.id ?? el.fakeId}
												>
													<Input
														name="value"
														type="text"
														required
														onChange={(e) =>
															handelChangeItemsInCourseOverView({
																name: e.target.name,
																value: e.target.value,
																id: el?.id ?? el?.fakeId,
																key: "schedule",
															})
														}
														defaultValue={el.value}
													/>
													<i
														className={iconsFa6.delete + " cursor-pointer"}
														onClick={() =>
															handelDeleteItemsInCourseOverView({
																id: el.id ?? el.fakeId,
																key: "schedule",
															})
														}
													/>
												</RFlex>
											)
										)}
									</Col>
								)}
							</Row>
						</Col>

						<Col xs={12} className={`${styles.right_col_design} ${styles.overflow_scroll_req} mb-4`}>
							<Row>
								<Col xs={12} className="pl-0">
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<p className="font-weight-bold">{tr`requirements`}</p>
										{!courseStudentMode && (
											<div
												style={{ color: primaryColor }}
												className="d-flex cursor-pointer"
												onClick={(e) =>
													handelSetItemsToCourseOverView({
														key: "requirements",
													})
												}
											>
												<i className="fa fa-plus pr-2 pt-1" />
												<p>{tr`add_new`}</p>
											</div>
										)}
									</RFlex>
								</Col>

								{courseById?.overview && courseById?.overview?.requirements && (
									<Col xs={12} className="pl-0">
										{courseById?.overview?.requirements.map((el) =>
											courseStudentMode ? (
												<p key={el.id ?? el.fakeId}>{el.item}</p>
											) : (
												<RFlex
													styleProps={{
														alignItems: "center",
														color: dangerColor,
														marginBottom: "10px",
													}}
													key={el.id ?? el.fakeId}
												>
													<Input
														name="item"
														type="text"
														required
														onChange={(e) =>
															handelChangeItemsInCourseOverView({
																name: e.target.name,
																value: e.target.value,
																id: el?.id ?? el?.fakeId,
																key: "requirements",
															})
														}
														defaultValue={el.item}
													/>
													<i
														className={iconsFa6.delete + " cursor-pointer"}
														onClick={() =>
															handelDeleteItemsInCourseOverView({
																id: el.id ?? el.fakeId,
																key: "requirements",
															})
														}
													/>
												</RFlex>
											)
										)}
									</Col>
								)}
							</Row>
						</Col>

						<Col Col xs={12} className={`${styles.right_col_design} ${styles.overflow_scroll_prev} mb-4`}>
							<Row>
								<Col xs={12} className="pl-0">
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<p className="font-weight-bold">{tr`previous_knowledge`}</p>
										{!courseStudentMode && (
											<div
												style={{ color: primaryColor }}
												className="d-flex cursor-pointer"
												onClick={(e) =>
													handelSetItemsToCourseOverView({
														key: "previous_knowledge",
													})
												}
											>
												<i className="fa fa-plus pr-2 pt-1" />
												<p>{tr`add_new`}</p>
											</div>
										)}
									</RFlex>
								</Col>
								{courseById?.overview && courseById?.overview?.previous_knowledge && (
									<Col xs={12} className="pl-0">
										{courseById?.overview?.previous_knowledge.map((el) =>
											courseStudentMode ? (
												<p key={el.id ?? el.fakeId}>{el.item}</p>
											) : (
												<RFlex
													styleProps={{
														alignItems: "center",
														color: dangerColor,
														marginBottom: "10px",
													}}
													key={el.id ?? el.fakeId}
												>
													<Input
														name="item"
														type="text"
														required
														onChange={(e) =>
															handelChangeItemsInCourseOverView({
																name: e.target.name,
																value: e.target.value,
																id: el?.id ?? el?.fakeId,
																key: "previous_knowledge",
															})
														}
														defaultValue={el.item}
													/>
													<i
														className={iconsFa6.delete + " cursor-pointer"}
														onClick={() =>
															handelDeleteItemsInCourseOverView({
																id: el.id ?? el.fakeId,
																key: "previous_knowledge",
															})
														}
													/>
												</RFlex>
											)
										)}
									</Col>
								)}
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
			{/* Facilitator */}
			<Row className="mb-4" id="facilitator">
				<Col xs={12} className={`d-flex pl-0 ${courseById?.teachers?.length == 0 ? "justify-content-start" : "justify-content-end"} `}>
					{courseById?.teachers?.length == 0 && <span style={{ color: dangerColor }} className="pt-3">{tr`no added facilitators `}</span>}
				</Col>
				<RFlex styleProps={{ flexWrap: "wrap", justifyContent: "flex-start" }}>
					{courseById?.teachers && courseById?.teachers?.map((user) => <RCardUser key={user.user_id} user={user} />)}
				</RFlex>
			</Row>
			{/* Feedbacks */}
			{courseStudentMode && courseById?.feedbacks?.length == 0 ? (
				""
			) : (
				<Row className="mb-3">
					<Col xs={10} className="pl-0">
						<RFlex styleProps={{ alignItems: "center" }}>
							<span className="font-weight-bold">{tr`what students said about the course`}</span>
							<RFlex>
								<i className="fa fa-star " style={{ color: "rgb(255, 188, 11)", fontSize: "20px" }} />
								<span style={{ color: "rgb(255, 188, 11)" }}>{courseById?.rating}</span>
								{courseById?.feedbacks?.length > 0 ? (
									<AppCheckbox onClick={() => setShowLearnersFeedback(!showLearnersFeedback)} label={tr`Show learners’ feedback`} />
								) : (
									<span style={{ color: dangerColor }}>{tr`no feedback yet`}</span>
								)}
							</RFlex>
						</RFlex>
					</Col>

					<Col xs={2} className="d-flex justify-content-end pl-0">
						{courseById?.feedbacks?.length > 0 && (
							<p style={{ color: primaryColor }} className={styles.see_all} onClick={handleShowAllLearnersFeedback}>{tr`see_all`}</p>
						)}
					</Col>
					{showLearnersFeedback &&
						courseById.feedbacks.map((feedback) => (
							<Col xs={12} key={feedback.id} className="pl-0">
								<RCardUserFeedback feedback={feedback} />
							</Col>
						))}
				</Row>
			)}
			{/* Recommendations */}
			{/* <Row className="mb-3">
				<Col xs={12}>
					<RFlex>
						<h6>{tr`course_content`}</h6>
						<AppCheckbox
						onClick={() => setShowCourseContent(!showCourseContent)}
						/>
						<h6>{tr`show_course_summery`}</h6>
						{showCourseContent && (
							<>
								<span>
								{courseById?.statistics?.modules} <span>{tr`modules`}</span>
								</span>
								<span>
								{courseById?.statistics?.lessons} <span>{tr`lessons`}</span>
								</span>
								<span>
								{courseById?.statistics?.links} <span>{tr`links`}</span>
								</span>
								<span>
								{courseById?.statistics?.files} <span>{tr`files`}</span>
								</span>
							</>
						)}
					</RFlex>
				</Col>
			</Row> */}
			{/* FAQ */}
			<Row className="mb-3">
				<Col xs={12} sm={8} className="pl-0">
					<Row>
						<Col xs={12} className="pl-0">
							<h6>{tr`FAQ`}</h6>
						</Col>
						{!courseStudentMode && (
							<Col xs={12} className="pl-0">
								<RFlex>
									<div
										style={{ color: primaryColor }}
										className="d-flex cursor-pointer"
										onClick={(e) =>
											handelSetItemsToCourseOverView({
												key: "faq",
											})
										}
									>
										<i className="fa fa-plus pr-2 pl-0 pt-1" />
										<p>{tr`add_new_question`}</p>
									</div>
								</RFlex>
							</Col>
						)}

						{courseById?.overview && courseById?.overview?.faq && (
							<Col xs={11} className="pl-0">
								{courseById?.overview?.faq.map((el, index) =>
									courseStudentMode ? (
										<Row key={index} className={styles.faq_collapse}>
											<Col
												xs={12}
												className={styles.question_collapse}
												onClick={() => toggleQuestionCollapse(el?.id ? el?.id : el?.fakeId)}
											>
												<Row>
													<Col className="pl-0" xs={10}>
														{el.question}
													</Col>
													<Col className="pl-0 d-flex justify-content-end" xs={2}>
														<i
															style={{ color: primaryColor }}
															className={
																openedCollapses === "collapse" + (el?.id ? el?.id : el?.fakeId)
																	? iconsFa6.chevronRight
																	: iconsFa6.chevronDown
															}
														/>
													</Col>
												</Row>
											</Col>
											<Collapse isOpen={openedCollapses === "collapse" + (el?.id ? el?.id : el?.fakeId)}>
												<Col className={styles.answer_collapse}>{el.answer}</Col>
											</Collapse>
										</Row>
									) : (
										<Row className="mb-4" key={index}>
											<Col xs={11} className="pl-0">
												<Input
													className="mb-2"
													name="question"
													type="text"
													required
													onChange={(e) =>
														handelChangeItemsInCourseOverView({
															name: e.target.name,
															value: e.target.value,
															id: el?.id ? el?.id : el?.fakeId,
															key: "faq",
														})
													}
													value={el.question}
													placeholder={`${index + 1}. ${tr`question`}`}
												/>

												<Input
													name="answer"
													type="text"
													required
													onChange={(e) =>
														handelChangeItemsInCourseOverView({
															name: e.target.name,
															value: e.target.value,
															id: el?.id ? el?.id : el?.fakeId,
															key: "faq",
														})
													}
													value={el.answer}
													placeholder={tr`answer`}
												/>
											</Col>
											<Col className="d-flex align-items-center" xs={1}>
												<i
													className={iconsFa6.delete + " cursor-pointer"}
													onClick={() =>
														handelDeleteItemsInCourseOverView({
															id: el.id ?? el.fakeId,
															key: "faq",
														})
													}
													style={{ color: dangerColor }}
												/>
											</Col>
										</Row>
									)
								)}
							</Col>
						)}
					</Row>
				</Col>
				<Col xs={12} sm={4}>
					<img src={hands} alt="hands" width={310} height={360} />
				</Col>
			</Row>
			<Row className="mb-3">
				<Col xs={12} className="pl-0">
					<h6>{tr`course_policy`}</h6>
				</Col>
				<Col xs={6} className="pl-0">
					{courseStudentMode ? (
						<p
							style={{ fontWeight: 400 }}
							dangerouslySetInnerHTML={{
								__html: courseById?.overview?.policy,
							}}
						/>
					) : (
						<CKEditor
							editor={ClassicEditor}
							data={courseById?.overview?.policy}
							onChange={(event, editor) => {
								const data = editor.getData();
								handelChangeItemsInCourseOverView({
									name: "policy",
									value: data,
									key: null,
								});
							}}
						/>
					)}
				</Col>
			</Row>
			{!courseStudentMode && (
				<Row>
					<Col xs={4} className="pl-0">
						<RButton
							text={tr`save`}
							color="primary"
							loading={saveCourseOverViewLoading}
							disabled={saveCourseOverViewLoading}
							onClick={handleSaveCourseOverView}
						/>
					</Col>
				</Row>
			)}
		</React.Fragment>
	);
};

export default RCourseOverView;
