import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../RSchoolInitiation.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import RButton from "components/Global/RComs/RButton";
import { Form, FormGroup, Input } from "reactstrap";
import useWindowDimensions from "components/Global/useWindowDimensions";
import { useParams } from "react-router-dom";

const RHeaderPrincipal = ({
  handleOpenAddUser,
  handleOpenAddFromUser,
  handleChangeSearch,
  handleSearch,
  searchLoading,
  searchData,
  setSearchData,
}) => {
  const { width } = useWindowDimensions();
  const mobile = width < 1000;
  const { sIType, sITypeId, tabTitle } = useParams();

  return (
    <RFlex
      styleProps={{
        display: "flex",
        width: "100%",
        alignItems: "center",
        gap: 30,
        flexDirection: mobile ? "column" : "row",
      }}
      className="mb-4"
    >
      <RButton
        color="primary"
        text={
          tabTitle == "principal"
            ? tr`create_a_principal`
            : tabTitle == "student"
            ? tr`create_a_student`
            : tabTitle == "senior_teacher"
            ? tr`create_a_senior_teacher`
            : ""
        }
        faicon="fa fa-plus"
        onClick={handleOpenAddUser}
      />
      <Form className={styles.form_input}>
        <FormGroup>
          <Input
            type="text"
            placeholder={tr`search`}
            className={styles.search_input}
            value={searchData}
            onChange={(event) => {
              handleChangeSearch(event.target.value);
            }}
          />
          <i
            aria-hidden="true"
            className={
              searchLoading
                ? `fa fa-refresh fa-spin ${styles.search_input_icon}`
                : `fa fa-search ${styles.search_input_icon}`
            }
            onClick={() => searchData !== "" && handleSearch()}
          />
          <i
            aria-hidden="true"
            className={`fa fa-close ${styles.clear_input_icon}`}
            onClick={() => {
              if (searchData !== "") {
                setSearchData("");
                handleSearch("");
              }
            }}
          />
        </FormGroup>
      </Form>
      {tabTitle !== "student" && (
        <p
          style={{
            color: primaryColor,
            textDecoration: "underline",
            cursor: "pointer",
            position: "relative",
            top: "6px",
          }}
          onClick={handleOpenAddFromUser}
        >{tr`add_from_users`}</p>
      )}
    </RFlex>
  );
};

export default RHeaderPrincipal;
