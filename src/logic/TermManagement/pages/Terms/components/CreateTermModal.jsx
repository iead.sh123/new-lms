import { faQuestionCircle, faUserGraduate, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RModal from "components/Global/RComs/RModal";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import { FormGroup, Input, Label, FormText, Row, Col } from "reactstrap"
import Swal, { SUCCESS, DANGER } from "utils/Alert";



const CreateTermModal = ({ isOpen, toggle, handleSave, edited }) => {

    const [formItems, setFormItems] = React.useState({
        name: '',
        startDate: '',
        endDate: '',
        code: '',
        rank: 0
    });


    React.useEffect(() => {
        if (edited)
            setFormItems({ ...edited, startDate: moment(edited.startDate).format('yyyy-MM-DD'), endDate: moment(edited.endDate).format('yyyy-MM-DD') });
    }, [edited?.id]);


    const handleChange = (e) => setFormItems({ ...formItems, [e.target.name]: e.target.name === "rank" ? +e.target.value : e.target.value });

    const handleCreate = async () => {
        await handleSave({ ...formItems })
        toggle();

    };

    return <RModal isOpen={isOpen}
        withCloseIcon={false}
        toggle={toggle}
        header={<RFlex><FontAwesomeIcon icon={faUserPlus} /> <p>{tr`Create Term`}</p></RFlex>}
        footer={
            <RFlex styleProps={{ width: '100%', alignItems: 'start' }}>
                <RButton
                    text={edited?.id ? tr`update` : tr`create`}
                    color="primary"
                    onClick={handleCreate}
                // style={{alignSelf: 'end'}}
                />

                <RButton
                    text={tr`cancel`}
                    style={{ background: 'white', color: '#668AD7' }}
                    // faicon="fa fa-plus"
                    // color="link"
                    onClick={toggle}
                // style={{alignSelf: 'end'}}
                />
            </RFlex>
        }
        body={<div>
            <Row>
                <Col md="12">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Name")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="name"
                                type="text"
                                // placeholder={tr`Name`}
                                value={formItems.name}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
            </Row>
            <Row>
                <Col md="6">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Start Date")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="startDate"
                                type="date"
                                // placeholder={tr`Start Date`}
                                value={formItems.startDate}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>

                <Col md="6">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("End Date")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="endDate"
                                type="date"
                                // placeholder={tr`End Date`}
                                value={formItems.endDate}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
            </Row>

            <Row>
                <Col md="6">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Code")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="code"
                                type="text"
                                // placeholder={tr`Code`}
                                value={formItems.code}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
                <Col md="6">
                    <RFlex styleProps={{ flexDirection: 'column' }}>
                        <div>
                            <label>{tr("Order")}</label>
                            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted ml-4 mt-2" />
                        </div>
                        <FormGroup>
                            <Input
                                name="rank"
                                type="number"
                                // placeholder={tr`Code`}
                                value={formItems.rank}
                                onChange={handleChange}
                            />
                        </FormGroup>
                    </RFlex>
                </Col>
            </Row>
        </div >} />
}

export default CreateTermModal;