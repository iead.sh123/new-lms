import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useEffect, useRef, useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import * as colors from "config/constants";
import { useHistory, useParams } from "react-router-dom";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import { Terms, usersTabs } from "../constants";
import styles from "./userStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { Form, useFormik } from "formik";
import * as Yup from "yup";
import RButton from "components/Global/RComs/RButton";
import RUserBasicInfo from "./RUserBasicInfo";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { usersManagementApi } from "api/UsersManagement";
import RLoader from "components/Global/RComs/RLoader";
import { useMutateData } from "hocs/useMutateData";
import RRoles from "../Shared/RRoles";
import RPermissons from "../Shared/RPermissons";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
const GCreateViewUser = () => {
	//---------------------------------Global-----------------------------------------------------
	const { CUDToQueryKey, operations } = useCUDToQueryKey();

	const [activeTab, setActiveTab] = useState(usersTabs[0].title);
	const [currentUserTypes, setCurrentUserTypes] = useState([
		{ id: -1, title: tr("on_organization_Level"), active: true, roles: [], permissions: [] },
	]);
	const currentOrganization = { title: "TestOrg", id: 1 };
	const [pickedUserType, setPickedUserType] = useState({
		id: -1,
		title: tr("on_organization_Level"),
		active: true,
		roles: [],
		permissions: [],
	});
	const handleChangePickedUserType = (pickedUserType) => {
		setPickedUserType(pickedUserType);
		const sortedRoles = getSortedRoles(roles, pickedUserType);
		CUDToQueryKey({
			operation: operations.REPLACE,
			queryKey: [Terms.Roles, backRoleSearchTerm],
			newData: sortedRoles,
			insertionDepth: "data.data",
		});
		// const sortedPermissions = getSortedPermissions(permissions, pickedUserType);
		// CUDToQueryKey({
		// 	operation: operations.REPLACE,
		// 	queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		// 	newData: sortedPermissions,
		// 	insertionDepth: "data.data",
		// });
	};
	//---------------------------------User Basic Info--------------------------------------------------
	const [userTypesCount, setUserTypesCount] = useState([{ id: 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	const [accountActiveStatus, setAccountActiveStatus] = useState(true);
	const [uploadId, setUploadId] = useState(null);
	const [hashId, setHashId] = useState(null);
	const [currentUserId, setCurrentUserId] = useState();
	const [pickedOrganization, setPickedOrganization] = useState(null);
	const [gender, setGender] = useState("male");
	const initialValues = {
		username: "",
		firstname: "",
		lastname: "",
		userid: "",
		email: "",
		password: "",
		address: "",
		phone: "",
		emergency: "",
	};
	const phoneRegex = /^[+]?[(]?[0-9]{1,4}[)]?[-\s.]?[0-9]{1,3}[-\s.]?[0-9]{3,4}[-\s.]?[0-9]{3,4}$/im;
	const validationSchema = Yup.object({
		username: Yup.string().required(tr("User_Name_is_required")),
		firstname: Yup.string().required(tr("First_Name_is_required")),
		lastname: Yup.string().required(tr("Last_Name_is_required")),
		email: Yup.string().email(tr("email_is_invalid")).required(tr("email_is_required")),
		password: Yup.string().min(8, tr("Password_must_be_at_least_8_charachters")).required(tr("Password_is_required")),
		phone: Yup.string().matches(phoneRegex, "Invalide Phone Number"),
		emergency: Yup.string().matches(phoneRegex, "Invalide Phone Number"),
	});
	const { values, touched, errors, setFieldValue, setFieldTouched, handleSubmit, setFieldError, handleChange, handleBlur } = useFormik({
		initialValues,
		validationSchema,
		validateOnMount: true,
		onSubmit: (values) => getSaveAction(),
	});
	const handleAddingUserType = () => {
		const maxId = userTypesCount.reduce((max, type) => (max > type.id ? max : type.id), 0);
		setUserTypesCount([...userTypesCount, { id: maxId + 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	};
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(),
	});
	const handleSelectOption = (option, stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		if (!option.value) {
			userTypesCountCopy[typeIndex].pickedOption = [];
			userTypesCountCopy[typeIndex].disabledStatus = true;
			userTypesCountCopy[typeIndex].activeStatus = false;
		} else {
			userTypesCountCopy[typeIndex].pickedOption = option;
			userTypesCountCopy[typeIndex].disabledStatus = false;
			userTypesCountCopy[typeIndex].activeStatus = true;
		}
		setUserTypesCount(userTypesCountCopy);
	};
	const handleSwitchChange = (stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		userTypesCountCopy[typeIndex].activeStatus = !userTypesCountCopy[typeIndex].activeStatus;
		setUserTypesCount(userTypesCountCopy);
	};
	const handleAccountActiveChanged = () => {
		setAccountActiveStatus(!accountActiveStatus);
	};
	const generateEmailFromUsername = () => {
		if (errors.username) {
			setFieldTouched("username", true);
			return;
		}
		const domain = "@OrganizationName.org";
		const email = values.username.trim().replace(/\s/g, ".") + domain;
		setFieldValue("email", email);
	};
	const suggestPassword = () => {
		const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		let password = "";
		const length = 8; // Length of the password

		for (let i = 0; i < length; i++) {
			const randomIndex = Math.floor(Math.random() * characters.length);
			password += characters.charAt(randomIndex);
		}
		setFieldValue("password", password);
	};
	const createUserAccountMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.createUserAccount(payload),
		onSuccessFn: ({ data }) => {
			setHashId(data?.data?.user?.image);
			setCurrentUserId(data?.data?.user?.id);
			const types = data?.data?.user?.organization_users
				.filter((userType, index) => userType.type)
				.map((userType, index) => ({
					id: userType.type.type.id,
					title: userType.type.type.name,
					active: false,
					roles: userType.roles,
					permissions: userType.permissions,
				}));
			types.forEach((type, index) => {
				setSelectedRoles({ ...selectedRoles, [type.id]: type.roles });
				setSelectedPermissions({ ...selectedPermissions, [type.id]: type.permissions });
			});
			setCurrentUserTypes([...currentUserTypes, ...types]);
			setActiveTab(Terms.Roles);
			scrollToTop();
		},
	});

	console.log("currentUserTypes", currentUserTypes);
	const updateUserAccountMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.updateUserAccount(currentUserId, payload),
		onSuccessFn: ({ data }) => {
			setHashId(data?.data?.image);
			const types = data?.data?.organization_users
				.filter((userType, index) => userType.type)
				.map((userType, index) => ({
					id: userType.type.type.id,
					title: userType.type.type.name,
					roles: userType.roles,
					permissions: userType.permissions,
				}));
			types.forEach((type, index) => {
				setSelectedRoles({ ...selectedRoles, [type.id]: type.roles });
				setSelectedPermissions({ ...selectedPermissions, [type.id]: type.permissions });
			});
			setCurrentUserTypes([...currentUserTypes, ...types]);
			setActiveTab(Terms.Roles);
			scrollToTop();
		},
	});
	console.log("uploadId", uploadId);
	const handleCreatingNewUserAccount = () => {
		console.log("Running");
		if (Object.keys(errors).length > 0) {
			return;
		}
		const types = userTypesCount.map((type, index) => ({ id: type?.pickedOption?.value, active: type?.activeStatus }));
		const finalTypes = types.filter((type) => type.id);
		const payload = {
			name: values.username,
			image: uploadId.url ? [uploadId] : undefined,
			first_name: values.firstname,
			last_name: values.lastname,
			gender: gender.charAt(0),
			email: values.email,
			password: values.password,
			c_password: values.password,
			// organization_group_id: 0,
			organization_id: currentOrganization.id,
			details: {
				address: values.address,
				emergency_number: values.emergency,
				phone_number: values.phone,
			},
			is_active: accountActiveStatus,
			identifier: values.userid,
			types: finalTypes.length > 0 ? finalTypes : undefined,
			// tags: [
			// 	{
			// 		id: 0,
			// 		name: "string",
			// 		color_id: 0,
			// 	},
			// ,
		};
		currentUserId ? updateUserAccountMutation.mutate({ payload }) : createUserAccountMutation.mutate({ payload });
	};
	const handleSelectOrganization = (e) => {
		setPickedOrganization(e);
	};
	const topRef = useRef(null);

	console.log("errors2", errors);
	//--------------------------------Roles------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState({ "-1": [] });
	const [savedRoles, setSavedRoles] = useState({ "-1": [] });
	console.log("selectedRoles", selectedRoles);
	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			console.log("dasd");
		},
	});

	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles[pickedUserType.id].some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = { ...selectedRoles, [pickedUserType.id]: selectedRoles[pickedUserType.id].filter((p1) => p1.id != role.id) };
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = { ...selectedRoles, [pickedUserType.id]: [...selectedRoles[pickedUserType.id], role] };
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const removeRolesFromUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.assignRolesToUsers(typeId < 0 ? undefined : typeId, payload),
		onSuccessFn: () => {
			scrollToTop();
		},
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
	});
	const assignRolesToUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.assignRolesToUsers(typeId < 0 ? undefined : typeId, payload),
		onSuccessFn: () => {
			scrollToTop();
		},
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
	});

	const getRemovedRolesForEachUserType = () => {
		const allSavedRoles = currentUserTypes.reduce((result, userType) => {
			result[userType.id] = userType.roles;
			return result;
		}, {});
		const idsInEachUserTypeSelectedRoles = Object.keys(selectedRoles).reduce((result, key) => {
			result[key] = new Set(selectedRoles[key].map((role) => role.id));
			return result;
		}, {});
		const removedRolesInEachUserType = Object.keys(allSavedRoles).reduce((result, key) => {
			result[key] = allSavedRoles[key].filter((role, index) => !idsInEachUserTypeSelectedRoles[key].includes(role.id));
			return result;
		}, {});
		const removedRolesNamesInEachUserType = Object.keys(removedRolesInEachUserType).reduce((result, key) => {
			result[key] = removedRolesInEachUserType.map((role, index) => role.name);
			return result;
		}, {});
		return removedRolesNamesInEachUserType;
	};
	const getSortedRoles = (roles, pickedOption) => {
		const rolesCopy = JSON.parse(JSON.stringify(roles));
		const savedUserTypeRolesIds = new Set(savedRoles[pickedOption.id]?.map((r) => r.id));
		const rolesArray = rolesCopy?.data?.data;
		rolesArray?.sort((r1, r2) => {
			const r1IsSelected = savedUserTypeRolesIds.has(r1.id);
			const r2IsSelected = savedUserTypeRolesIds.has(r2.id);
			if (r1IsSelected && !r2IsSelected) {
				return -1;
			}
			if (!r1IsSelected && r2IsSelected) {
				return 1;
			}
			return 0;
		});
		return rolesArray;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState({ "-1": [] });
	const [savedPermissions, setSavedPermissions] = useState({ "-1": [] });
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		keepPreviousData: true,
		onSuccessFn: (data) => {
			console.log("Success", data);
		},
	});

	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissonsToUsers(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: () => {
			history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const removePermissionsFromUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromUsers(payload),
		onSuccessFn: () => {
			history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};
	const getSortedPermissions = (permissions, userTypePermissions) => {
		const permissionsCopy = JSON.parse(JSON.stringify(permissions));
		const savedPermissionsIds = new Set(userTypePermissions?.permissions?.map((p) => p.id));
		const permissionsArray = permissionsCopy?.data?.data;
		permissionsArray?.sort((p1, p2) => {
			const p1IsSelected = savedPermissionsIds.has(p1.id);
			const p2IsSelected = savedPermissionsIds.has(p2.id);
			if (p1IsSelected && !p2IsSelected) {
				return -1;
			}
			if (!p1IsSelected && p1IsSelected) {
				return 1;
			}
			return 0;
		});
		return permissionsArray;
	};
	//----------------------------------------Global-------------------------------------------
	const history = useHistory();
	const getNextTab = () => {
		switch (activeTab) {
			case Terms.BasicInfo:
				return tr(`assign_${Terms.Roles}`);
			case Terms.Roles:
				return tr(`assign_${Terms.Permissions}`);
		}
	};
	const getNextAction = () => {
		switch (activeTab) {
			case Terms.BasicInfo:
				setActiveTab(Terms.Roles);
				break;
			case Terms.Roles:
				setActiveTab(Terms.Permissions);
				break;
		}
	};
	const scrollToTop = () => {
		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
		}
	};
	const getSaveAction = () => {
		let payload = {};
		console.log("Running");
		switch (activeTab) {
			case Terms.BasicInfo:
				handleCreatingNewUserAccount(payload);
				// setActiveTab(Terms.Roles);
				break;
			case Terms.Roles:
				const allUserTypeRemovedRoles = getRemovedRolesForEachUserType();
				Object.keys(allUserTypeRemovedRoles).forEach((key, index) => {
					const removedRolesNames = allUserTypeRemovedRoles[key];
					if (removedRolesNames.length > 0) {
						payload = {
							role_names: removedRolesNames,
							user_ids: [currentUserId],
						};
						removeRolesFromUserInSpecificUserTypeMutation.mutate({ payload });
					}
					if (selectedRoles[key].length > 0) {
						const roleNames = selectedRoles[key].map((role) => role.name);
						payload = {
							role_names: roleNames,
							user_ids: [currentUserId],
						};
						assignRolesToUserInSpecificUserTypeMutation.mutate({ typeId: key, payload });
					}
					setSavedRoles({ ...savedRoles, [key]: selectedRoles[key] });
				});
				setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						user_ids: [currentUserId],
					};
					removePermissionsFromUsersMutation.mutate({ payload });
				}
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						user_ids: [currentUserId],
					};
					assignPermissionsToUsersMutation.mutate({ payload });
				}
				setSavedPermissions(selectedPermissions);

				break;
		}
	};

	return (
		<RFlex className="flex-column" styleProps={{ position: "relative" }}>
			<RFlex className="align-items-center">
				<div
					ref={topRef}
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/users`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				{tr("Create_new_Account")}
			</RFlex>
			<RTabs tabs={usersTabs} setTabActive={setActiveTab} activeTab={activeTab} />
			{activeTab == Terms.BasicInfo &&
				(isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RUserBasicInfo
						values={values}
						touched={touched}
						errors={errors}
						handleChange={handleChange}
						handleBlur={handleBlur}
						userTypesCount={userTypesCount}
						handleAddingUserType={handleAddingUserType}
						userTypes={userTypes?.data?.data}
						handleSelectOption={handleSelectOption}
						handleSwitchChange={handleSwitchChange}
						handleAccountActiveChanged={handleAccountActiveChanged}
						accountActiveStatus={accountActiveStatus}
						gender={gender}
						setGender={setGender}
						generateEmailFromUsername={generateEmailFromUsername}
						suggestPassword={suggestPassword}
						setUploadId={setUploadId}
						handleSelectOrganization={handleSelectOrganization}
						currentOrganization={currentOrganization}
						pickedOrganization={pickedOrganization}
						hashId={hashId}
						scrollToTop={scrollToTop}
						currentUserTypes={currentUserTypes}
					/>
				))}
			{activeTab == Terms.Roles &&
				(isLoadingRoles ? (
					<RLoader />
				) : (
					<RRoles
						roles={getSortedRoles(roles, pickedUserType.roles)}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles[pickedUserType.id]}
						inUser
						currentUserTypes={currentUserTypes}
						pickedUserType={pickedUserType}
						handleChangePickedUserType={handleChangePickedUserType}
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={permissions?.data?.data}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions[pickedUserType.id]}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
						inUser
						currentUserTypes={currentUserTypes}
						pickedUserType={pickedUserType}
						handleChangePickedUserType={handleChangePickedUserType}
					/>
				))}
			<div className={styles.roolFooter}>
				<RFlex className="align-items-center" styleProps={{ gap: "0px" }}>
					<RButton
						text={activeTab != Terms.Permissions ? tr("Save_and_Next") : tr("Create_account_and_finish")}
						color="primary"
						className="m-0"
						loading={
							createUserAccountMutation.isLoading ||
							updateUserAccountMutation.isLoading ||
							assignRolesToUserInSpecificUserTypeMutation.isLoading ||
							removeRolesFromUserInSpecificUserTypeMutation.isLoading ||
							assignPermissionsToUsersMutation.isLoading ||
							removePermissionsFromUsersMutation.isLoading
						}
						disabled={
							createUserAccountMutation.isLoading ||
							updateUserAccountMutation.isLoading ||
							assignRolesToUserInSpecificUserTypeMutation.isLoading ||
							removeRolesFromUserInSpecificUserTypeMutation.isLoading ||
							assignPermissionsToUsersMutation.isLoading ||
							removePermissionsFromUsersMutation.isLoading ||
							(!currentUserId && activeTab != Terms.BasicInfo)
						}
						onClick={handleSubmit}
					/>
					{activeTab != Terms.Permissions && (
						<RButton
							text={getNextTab()}
							color="link"
							className="m-0"
							faicon={activeTab == Terms.Permissions ? iconsFa6.chevronLeft : iconsFa6.chevronRight}
							iconRight={activeTab == Terms.Permissions ? false : true}
							onClick={getNextAction}
						/>
					)}
				</RFlex>
			</div>
		</RFlex>
	);
};

export default GCreateViewUser;
