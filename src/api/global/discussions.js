import { get, post, put, destroy } from "config/api";
import { Services } from "engine/services";

export const discussionsApi = {
  getHomePagePosts: (pageNumber, cohortId) =>
    get(
      `${Services.discussion.backend}api/post/home-page${
        cohortId
          ? `${`/${cohortId}?page=${pageNumber}`}`
          : `?page=${pageNumber}`
      }`
    ),

  createPost: (data) => post(`${Services.discussion.backend}api/post`, data),

  getPostById: (postId) =>
    get(`${Services.discussion.backend}api/post/${postId}`),

  getCommentById: (commentId) =>
    get(`${Services.discussion.backend}api/comment/${commentId}`),

  updatePost: (data, postId) =>
    put(`${Services.discussion.backend}api/post/${postId}`, data),

  removePost: (postId) =>
    destroy(`${Services.discussion.backend}api/post/${postId}`),

  searchPost: (text, pageNumber, cohortId) =>
    get(
      `${Services.discussion.backend}api/search/posts/${text}${
        cohortId
          ? `${`/${cohortId}?page=${pageNumber}`}`
          : `?page=${pageNumber}`
      }`
    ),

  getAllCategories: (cohortId) =>
    get(
      `${Services.discussion.backend}api/post/categories${
        cohortId ? `/${cohortId}` : ""
      }`
    ),

  chooseSpecificCategory: (categoryId, pageNumber, cohortId) =>
    get(
      `${Services.discussion.backend}api/post/category/${categoryId}${
        cohortId
          ? `${`/${cohortId}?page=${pageNumber}`}`
          : `?page=${pageNumber}`
      }`
    ),

  acceptContent: (contentId) =>
    get(`${Services.discussion.backend}api/content/approve/${contentId}`),

  rejectContent: (contentId, reason) =>
    get(
      `${Services.discussion.backend}api/content/reject/${contentId}?reason=${reason}`
    ),

  ignoreReportedContent: (contentId) =>
    get(
      `${Services.discussion.backend}api/reported-content/ignore/${contentId}`
    ),

  deleteReportedContent: (contentId) =>
    destroy(
      `${Services.discussion.backend}api/reported-content/delete/${contentId}`
    ),

  getContentByContentId: (contentId) =>
    get(`${Services.discussion.backend}api/content/${contentId}`),

  editContent: (contentId, data) =>
    put(`${Services.discussion.backend}api/content/update/${contentId}`, data),

  deleteRejectedContent: (contentId) =>
    destroy(`${Services.discussion.backend}api/content/delete/${contentId}`),

  getAllReportCategories: () =>
    get(`${Services.discussion.backend}api/report-categories`),

  reportOnPost: (contentId, data) =>
    post(`${Services.discussion.backend}api/content/report/${contentId}`, data),

  getPostsCreatedBySpecificUser: (userId, pageNumber, cohortId) =>
    get(
      `${Services.discussion.backend}api/post/user/${userId}${
        cohortId
          ? `${`/${cohortId}?page=${pageNumber}`}`
          : `?page=${pageNumber}`
      }`
    ),

  addLikeOnPostOrComment: (data) =>
    post(`${Services.discussion.backend}api/like`, data),

  loadAllPeople: (contentId, pageNumber) =>
    get(
      `${Services.discussion.backend}api/like/load-likes/${contentId}?page=${pageNumber}`
    ),

  getAllMandatoryPosts: (contentId, pageNumber) =>
    get(
      `${Services.discussion.backend}api/post/mandatory/${contentId}?page=${pageNumber}`
    ),

  addComment: (data) => post(`${Services.discussion.backend}api/comment`, data),

  updateComment: (commentId, data) =>
    put(`${Services.discussion.backend}api/comment/${commentId}`, data),

  removeComment: (commentId) =>
    destroy(`${Services.discussion.backend}api/comment/${commentId}`),

  loadPreviousComments: (contentId, nextId) =>
    get(
      `${Services.discussion.backend}api/comment/content/${contentId}/previous/${nextId}`
    ),
};
