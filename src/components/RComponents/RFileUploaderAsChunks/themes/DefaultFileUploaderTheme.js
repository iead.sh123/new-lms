import React from "react";
import styles from "../RFileUploaderAsChunks.module.scss";
import RFilesList from "../RFilesList";
import tr from "components/Global/RComs/RTranslator";

const DefaultFileUploaderTheme = ({ getRootProps, getInputProps, style, files, progress, removeFile, sizeFile, uploadFileIcon }) => {
	return (
		<section>
			<div {...getRootProps({ style })} className="mb-2">
				<input {...getInputProps()} />
				<div className={styles.flex__column}>
					<img src={uploadFileIcon} width="42px" height="42px" alt="uploadFileIcon" />
					<div className={styles.flex}>
						<span className={styles.upload__file__text}>{tr`Browse files`}</span>
						<span className={styles.drag__file__text}>{tr`or Drag your file to start uploading`}</span>
					</div>
					<span className={styles.max__size__text}>
						{tr`max uploading size is`} {sizeFile / 1000} GB
					</span>
				</div>
			</div>
			<RFilesList files={files} progress={progress} removeFile={removeFile} />
		</section>
	);
};

export default DefaultFileUploaderTheme;
