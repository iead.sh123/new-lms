import React from "react";

const RImage = ({ src, placeholder, width, height, style }) => {
	return <img src={src} alt={placeholder ?? "image"} width={(width ?? 100) + "px"} height={(height ?? 100) + "px"} style={style} />;
};

export default RImage;
