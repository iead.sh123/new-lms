import React, { createRef } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";

import { Container } from "reactstrap";
import Loader from "utils/Loader";
import store from "store";
import { useParams } from "react-router-dom";
import { scormLMSAPI } from "api/scorm";
import "utils/SCORM_adapter";
import RScorm from "view/scorm/RScorm";
import { Services } from "engine/services";
import { appDomain } from "engine/config";

const GScormViewable = ({ scormId }) => {
	const [scormEntryPoint, setScormEntryPoint] = React.useState("");
	const [activeSCO, setActiveSCO] = React.useState(0);
	const [activeScorm, setActiveScorm] = React.useState(null);
	const [loading, setLoading] = React.useState(false);
	const [scos, setScos] = React.useState([]);
	const user = useSelector((state) => state.auth.user);

	// const token = useSelector((state) => state.auth.token);

	// const { scormId } = useParams();

	// const setScormEntryPoint = (url)=>{

	//  setEntry(url); //&token=${token}`
	// }

	const iframeRef = React.useRef(null);

	useEffect(() => {
		init();
	}, [scormId]);

	const init = async () => {
		user.type === "student" ? await getScormForStudent() : await getScormEntry();
	};

	const handleNextSco = (currIndex) => {
		const nextInd = currIndex + 1;
		if (nextInd > scos.length - 1) return;

		if (scos[nextInd].block || !scos[nextInd].entry_url) return handleNextSco(nextInd);

		const api = (response.data.data.version = "scorm_2004" ? window.API_1484_11 : window.API);

		api.settings.lmsCommitUrl = `${Services.scorm.backend}api/scorm-trackings/${scos[nextInd].id}`;

		setActiveSCO(nextInd);

		setScormEntryPoint(Services.scorm.file + activeScorm.uuid + "/" + scos[nextInd].entry_url);
	};

	const handlePreviousSco = (currIndex) => {
		const prevInd = currIndex - 1;
		if (prevInd < 0) return;

		if (scos[prevInd].block || !scos[prevInd].entry_url) return handlePreviousSco(prevInd);

		const api = (response.data.data.version = "scorm_2004" ? window.API_1484_11 : window.API);

		api.settings.lmsCommitUrl = `${Services.scorm.backend}api/scorm-trackings/${scos[prevInd].id}`;

		setActiveSCO(prevInd);

		setScormEntryPoint(Services.scorm.file + activeScorm.uuid + "/" + scos[prevInd].entry_url);
	};

	const getScormForStudent = async () => {
		try {
			setLoading(true);

			const response = await scormLMSAPI.getScormEntry(scormId);

			if (!response.data.status) throw new Error(response.data.msg);

			const api = (response.data.data.version = "scorm_2004" ? window.API_1484_11 : window.API);

			api.setValue("cmi.core.student_name", `${user.first_name}, ${user.last_name}`);
			api.setValue("cmi.core.student_id", user.id);

			setScos(response.data.data.scos);

			if (response.data.data.scos.length) {
				api.currentState &&
					api?.loadFromJSON(
						response.data.data.scos.length && response.data.data.scos[0].sco_trackings.length
							? JSON.parse(response.data.data.scos[0].sco_trackings[0].details)
							: {}
					);
				//  api?.clear("Initialize");
				setActiveSCO(0);

				api.settings = {
					lmsCommitUrl: `${Services.scorm.backend}api/scorm-trackings/${response.data.data.scos[0].id}`, //false, //readOnly ? false : false /*backend commit url */, //tp override
					xhrHeaders: {
						Authorization: `Bearer ${store.getState().auth.token}`,
						"current-type": store.getState().auth.user?.type,
						"current-school": store.getState().auth.user.school_id,
					},
				};
			}

			setActiveScorm({ ...response.data.data, scos: null });

			setScormEntryPoint(Services.scorm.file + response.data.data.uuid + "/" + response.data.data.entry_url);
		} catch (err) {
			console.log(err);
		} finally {
			setLoading(false);
		}
	};

	const getScormEntry = async () => {
		try {
			setLoading(true);
			const response = await scormLMSAPI.getScormEntry(scormId);
			if (!response.data.status) throw new Error(response.data.msg);

			setScos(response.data.data.scos);
			if (scos.length) {
				setActiveSCO(0);
			}

			setActiveScorm({ ...response.data.data, scos: null });
			setScormEntryPoint(Services.scorm.file + response.data.data.uuid + "/" + response.data.data.entry_url);
		} catch (err) {
			console.log(err);
		} finally {
			setLoading(false);
		}
	};

	return (
		<Container className="content">
			{loading ? (
				<Loader />
			) : (
				<RScorm
					ref={iframeRef}
					src={scormEntryPoint /*"http://localhost:3000/course0115/index_lms.html"*/}
					hasNextSco={activeSCO < scos.length - 1}
					hasPrevSco={activeSCO !== 0}
					handleNextSco={() => handleNextSco(activeSCO)}
					handlePrevSco={() => handlePreviousSco(activeSCO)}
				/>
			)}
		</Container>
	);
};

export default GScormViewable;

// && cp .htaccess build/",
