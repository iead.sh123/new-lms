import React from "react";
import styles from "../RLessonPlan.module.scss";
import { primaryColor } from "config/constants";

const RLessonPlanAddButton = ({
  handleAddNewLessonPlanItems,
  index,
  position,
}) => {
  return (
    <div className={styles.addition_div}>
      <div className={styles.addition_line}></div>
      <div
        className={
          position == "down" ? styles.circle_parent_down : styles.circle_parent
        }
      >
        <div
          className={styles.addition_container_icon}
          style={{ background: primaryColor }}
        >
          <i
            className={`fa fa-plus  ${styles.addition_icon}`}
            onClick={() => handleAddNewLessonPlanItems(index)}
          />
        </div>
      </div>
    </div>
  );
};

export default RLessonPlanAddButton;
