import React, { useEffect, useState } from "react";
import { getLessonContentByLessonIdAsync } from "store/actions/teacher/lessonContent.action";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import RLessonViewer from "view/Courses/CourseManagement/Lesson/RLessonViewer";
import Loader from "utils/Loader";
import { getLessonPlans } from "store/actions/teacher/lessonPlan.actions";

const GLessonsViewer = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { lessonId } = useParams();

  const Tabs = [{ title: `lesson_content` }, { title: `lesson_plan` }];
  const [tabActive, setTabActive] = useState(Tabs[0]?.title);

  useEffect(() => {
    dispatch(getLessonContentByLessonIdAsync(lessonId));
    dispatch(getLessonPlans(lessonId));
  }, []);

  const { lessonContentByLessonId, lessonContentByLessonIdLoading } =
    useSelector((state) => state.lessonContentRed);

  const { lessonPlan, lessonPlanLoading } = useSelector(
    (state) => state.lessonPlan
  );

  return (
    <div>
      {lessonContentByLessonIdLoading ? (
        <Loader />
      ) : (
        <RLessonViewer
          lessonContentByLessonId={lessonContentByLessonId}
          lessonPlan={lessonPlan}
          history={history}
          Tabs={Tabs}
          tabActive={tabActive}
          setTabActive={setTabActive}
        />
      )}
    </div>
  );
};

export default GLessonsViewer;
