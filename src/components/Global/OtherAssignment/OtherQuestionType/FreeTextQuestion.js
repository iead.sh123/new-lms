import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";

const FreeTextQuestion = ({ assignmentReadMode, student }) => {
  return (
    <CKEditor
      editor={ClassicEditor}
      config={{
        toolbar: {
          items: [
            "heading",
            "MathType",
            "ChemType",
            "|",
            "bold",
            "italic",
            "link",
            "bulletedList",
            "numberedList",
            "imageUpload",
            "mediaEmbed",
            "insertTable",
            "blockQuote",
            "undo",
            "redo",
          ],
        },
      }}
      data={`${student.detail.free_answer}`}
      disabled={assignmentReadMode}
    />
  );
};

export default FreeTextQuestion;
