import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React from 'react'
import { Link } from 'react-router-dom'
import * as colors from 'config/constants'
import tr from 'components/Global/RComs/RTranslator'
const GCurriculaCourseV2 = ({ course }) => {
    return (
        <RFlex
            id="course div"
            className="align-items-center"
            styleProps={{
                gap: '0px',
                boxShadow: "0px 0px 8px 0px #E4E4E4",
                marginLeft: "60px",
                padding: "7px 20px",
            }}
        >
            <RFlex className="align-items-center" styleProps={{ minHeight: '25px' }}>
                <span style={{ fontSize: '12px' }}>{course?.name}</span>
                <p className='p-0 m-0'
                    style={{ color: colors.lightGray, fontSize: '11px', textAlign: 'center' }}
                >
                    {tr("You_Can_Enroll_Students_And_Teachers_in")} <Link style={{ textDecoration: 'underline' }} to='/g/terms-management'>
                        {tr("Term_Manager")}
                    </Link>
                </p>
            </RFlex>
        </RFlex>
    )
}

export default GCurriculaCourseV2