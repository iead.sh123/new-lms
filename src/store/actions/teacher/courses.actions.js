import {
	GET_COURSES_REQUEST,
	GET_COURSES_SUCCESS,
	GET_COURSES_ERROR,
	ADD_COURSE_REQUEST,
	ADD_COURSE_SUCCESS,
	ADD_COURSE_ERROR,
	FETCH_NEW_DATA,
} from "./teacherTypes";
import { teacherApi } from "api/teacher";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { userLogsAsync } from "../global/track.action";
import { toast } from "react-toastify";

export const getCoursesAsync = () => async (dispatch) => {
	dispatch({ type: GET_COURSES_REQUEST });

	try {
		const response = await teacherApi.getCourses();

		if (response.data.status === 1) {
			const coursesData = response.data.data;
			dispatch({
				type: GET_COURSES_SUCCESS,
				payload: coursesData,
			});
		} else {
			dispatch({ type: GET_COURSES_ERROR, payload: response.data.message });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_COURSES_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
		toast.error(error?.message);
	}
};

export const addCourseAsync = (data, handleCloseModal) => async (dispatch) => {
	dispatch({ type: ADD_COURSE_REQUEST });

	try {
		const response = await teacherApi.addCourse(data);

		if (response.data.status === 1) {
			dispatch({
				type: ADD_COURSE_SUCCESS,
			});
			handleCloseModal();
			dispatch(fetchNewData(true));
			dispatch(
				userLogsAsync([
					{
						interface_name: "Section",
						operation: "Add",
						context: data,
					},
				])
			);
		} else {
			dispatch({ type: ADD_COURSE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_COURSE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchNewData = (status) => async (dispatch) => {
	dispatch({ type: FETCH_NEW_DATA, payload: status });
};
