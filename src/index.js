import App from "App";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import store, { persistor } from "store/index.js";
import { PersistGate } from "redux-persist/integration/react";
import { createBrowserHistory } from "history";
import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.3.0";
import "./index.css";
// import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { Bounce, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const history = createBrowserHistory();
const queryClient = new QueryClient();

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<QueryClientProvider client={queryClient}>
				<BrowserRouter>
					<App />
				</BrowserRouter>
				<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
			</QueryClientProvider>
		</PersistGate>
		<ToastContainer
			position="top-center"
			autoClose={2500}
			limit={2}
			hideProgressBar={false}
			newestOnTop={false}
			closeOnClick
			rtl={false}
			pauseOnFocusLoss
			draggable
			pauseOnHover
			theme="light"
		/>
	</Provider>,

	document.getElementById("root")
);

// ReactDOM.createRoot(document.getElementById("root")).render(
// 	<React.StrictMode>
// 		<Provider store={store}>
// 			<PersistGate loading={null} persistor={persistor}>
// 				<QueryClientProvider client={queryClient}>
// 					<BrowserRouter>
// 						<App />
// 					</BrowserRouter>
// 					<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
// 				</QueryClientProvider>
// 			</PersistGate>
// 		</Provider>
// 	</React.StrictMode>
// );
