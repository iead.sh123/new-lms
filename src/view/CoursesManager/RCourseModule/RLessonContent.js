import React, { useContext } from "react";
import { Row, Col, Collapse, Input } from "reactstrap";
import { primaryColor, dangerColor } from "config/constants";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { linkColor } from "config/constants";
import { RLabel } from "components/Global/RComs/RLabel";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";
import LiveSession from "./ListLessonContent/LessonComponents/LiveSession/LiveSession";
import LessonPlan from "./ListLessonContent/LessonComponents/LessonPlan";
import styles from "../CoursesManager.module.scss";
import Essay from "./ListLessonContent/LessonComponents/Essay";
import Link from "./ListLessonContent/LessonComponents/Link";
import File from "./ListLessonContent/LessonComponents/File";
import tr from "components/Global/RComs/RTranslator";
import { useLocation } from "react-router-dom";
import iconsFa6 from "variables/iconsFa6";

const RLessonContent = ({ lessonContent, content, viewMode }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	const location = useLocation();

	const removeActionsFromLessonContent =
		["attachment", "lesson_plan"].includes(lessonContent?.type?.toLowerCase()) ||
		(["link", "text"].includes(lessonContent?.type?.toLowerCase()) && ["student", "learner"].includes(CourseModuleData.user?.type)) ||
		(location?.pathname?.includes("student") && lessonContent?.type !== "live_session") ||
		(CourseModuleData.viewAsStudent && lessonContent?.type?.toLowerCase() == "link") ||
		(viewMode && lessonContent?.type?.toLowerCase() == "link" && !lessonContent.newData);

	return (
		<div style={{ marginBottom: "12px" }}>
			<Row className="d-flex justify-content-end" key={lessonContent.id}>
				<Col
					// xs={viewMode ? 8 : 11}
					xs={11}
					className={`${styles.lesson_content}`}
					onMouseEnter={() => CourseModuleData.showActionsOnHover(lessonContent.id)}
					onMouseLeave={() => CourseModuleData.showActionsOnHover(lessonContent.id)}
					onClick={() => (removeActionsFromLessonContent ? {} : CourseModuleData.collapsesLessonContentToggle(lessonContent.id))}
				>
					<Row>
						<Col xs={9} className="d-flex align-items-center">
							{lessonContent?.type?.toLowerCase() == "text" ? (
								<i className="fa fa-file-text fa-lg pt-1" aria-hidden="true" />
							) : lessonContent?.type?.toLowerCase() == "link" ? (
								<i className="fa fa-link fa-lg pt-1" aria-hidden="true" style={{ color: linkColor }} />
							) : lessonContent?.type?.toLowerCase() == "attachment" ? (
								<RImageViewer
									mimeType={lessonContent?.mime_type}
									url={lessonContent?.attachment?.url}
									// imageName={lessonContent?.attachment?.file_name}
									width={"22px"}
									height={"22px"}
								/>
							) : lessonContent?.type?.toLowerCase() == "live_session" ? (
								<i className="fa fa-headphones fa-lg pt-1" aria-hidden="true" />
							) : lessonContent?.type?.toLowerCase() == "lesson_plan" ? (
								<>
									<RLabel value={tr("Not visible for students")} icon="fa fa-eye-slash fa-lg pt-1" lettersToShow={15} />
								</>
							) : (
								""
							)}

							{CourseModuleData.editAttachment ? (
								CourseModuleData.lessonContentId == lessonContent.id ? (
									<Input
										type="text"
										name="title"
										defaultValue={
											CourseModuleData.editAttachment ? CourseModuleData.fileNameLessonAttachmentState.name : lessonContent?.title
										}
										onChange={(e) => {
											CourseModuleData.setFileNameLessonAttachmentState({
												name: e.target.value,
											});
										}}
										style={{
											width: "fit-content",
											display: "inline",
											padding: "0px 5px",
										}}
										className="ml-2"
									/>
								) : (
									<>
										{lessonContent.type == "link" ? (
											<a
												className="ml-2"
												href={`${lessonContent?.link?.link_url}`}
												target="_blank"
												style={{ textDecoration: "underline", color: linkColor }}
											>
												{tr(lessonContent.title)}
											</a>
										) : (
											<span
												className="ml-2"
												style={{
													cursor: lessonContent.type == "attachment" ? "default" : "",
												}}
											>
												{" "}
												{tr(lessonContent.title)}
											</span>
										)}
									</>
								)
							) : (
								<>
									{lessonContent.type == "link" ? (
										<a
											className="ml-2"
											href={`${lessonContent?.link?.link_url}`}
											target="_blank"
											style={{ textDecoration: "underline", color: linkColor }}
										>
											{tr(lessonContent.title)}
										</a>
									) : (
										<span
											className="ml-2"
											style={{
												cursor: lessonContent.type == "attachment" ? "default" : "",
											}}
										>
											{" "}
											{tr(lessonContent.title)}
										</span>
									)}
								</>
							)}

							{lessonContent?.type == "attachment" ? (
								CourseModuleData.studentView ? (
									""
								) : CourseModuleData.editAttachment ? (
									CourseModuleData.lessonContentId == lessonContent.id ? (
										CourseModuleData.editLessonAttachmentLoading ? (
											<i className="fa fa-refresh fa-spin mr-2" style={{ color: primaryColor }} />
										) : (
											<i
												className="fa fa-pencil-square mr-2"
												style={{ color: primaryColor }}
												onClick={(event) => {
													event.stopPropagation();

													CourseModuleData.handleEditLessonContent(
														CourseModuleData.specificModule.id,
														content?.id,
														lessonContent.id,
														lessonContent.type
													);
												}}
											/>
										)
									) : (
										CourseModuleData.isHovered === "hover" + lessonContent.id && (
											<i
												className="fa fa-pencil mr-2"
												style={{ color: primaryColor }}
												onClick={(event) => {
													event.stopPropagation();

													CourseModuleData.setEditAttachment(true);
													CourseModuleData.setFileNameLessonAttachmentState({
														...CourseModuleData.fileNameLessonAttachmentState,
														name: lessonContent.title?.split(".")[0],
													});
													CourseModuleData.setLessonContentId(lessonContent.id);
												}}
											/>
										)
									)
								) : (
									CourseModuleData.isHovered === "hover" + lessonContent.id &&
									!CourseModuleData.studentView &&
									!lessonContent.newData && (
										<i
											className="fa fa-pencil mr-2"
											style={{ color: primaryColor }}
											onClick={(event) => {
												event.stopPropagation();
												CourseModuleData.setEditAttachment(true);
												CourseModuleData.setFileNameLessonAttachmentState({
													...CourseModuleData.fileNameLessonAttachmentState,
													name: lessonContent.title?.split(".")[0],
												});
												CourseModuleData.setLessonContentId(lessonContent.id);
											}}
										/>
									)
								)
							) : (
								""
							)}
						</Col>

						<Col xs={3} className="d-flex justify-content-end p-0 pt-1">
							{CourseModuleData.isHovered === "hover" + lessonContent.id && !CourseModuleData.studentView && (
								<>
									{lessonContent?.type == "lesson_plan" && !lessonContent.newData && (
										<i
											className="fa fa-eye mr-2"
											style={{ color: primaryColor }}
											onClick={(event) => {
												event.stopPropagation();

												CourseModuleData.handlePushToLessonPlanViewer(lessonContent.id);
											}}
										/>
									)}
									<i
										className={iconsFa6.delete + " mr-2"}
										style={{ color: dangerColor }}
										onClick={(event) => {
											event.stopPropagation();

											CourseModuleData.handleDelete(
												CourseModuleData.specificModule.id,
												content?.id,
												lessonContent.id,
												lessonContent.type,
												lessonContent.newData
											);
										}}
									/>
								</>
							)}

							{removeActionsFromLessonContent ? (
								""
							) : (
								<i
									className={
										CourseModuleData.openedLessonContentCollapses === "collapse" + lessonContent.id
											? "fa fa-chevron-down cursor-pointer"
											: "fa fa-chevron-right cursor-pointer"
									}
									style={{ color: primaryColor }}
									onClick={(event) => {
										event.stopPropagation();
										if (lessonContent.type == "text") {
											CourseModuleData.setEditLessonTextState({
												...CourseModuleData.editLessonTextState,
												name: lessonContent?.title,
												text: lessonContent?.text?.text,
											});
										} else if (lessonContent.type == "link") {
											CourseModuleData.setEditLessonLinkState({
												...CourseModuleData.editLessonLinkState,
												link_url: lessonContent.link?.link_url,
												link_title: lessonContent?.link?.link_title,
											});
										}
									}}
								/>
							)}
						</Col>
					</Row>

					{lessonContent?.type?.toLowerCase() == "text" && removeActionsFromLessonContent && (
						<Essay viewMode={true} lessonContent={lessonContent} content={content} />
					)}
				</Col>
			</Row>

			<Collapse isOpen={CourseModuleData.openedLessonContentCollapses === "collapse" + lessonContent.id}>
				<Row className="d-flex justify-content-end" key={lessonContent.id}>
					{/* <Col xs={viewMode ? 8 : 11} className={`${styles.lesson_content}`}> */}
					<Col xs={11} className={`${styles.lesson_content}`}>
						{lessonContent?.type == "text" && <Essay viewMode={viewMode} lessonContent={lessonContent} content={content} />}
						{lessonContent?.type == "link" && <Link viewMode={viewMode} lessonContent={lessonContent} content={content} />}
						{lessonContent?.type == "attachment" && <File viewMode={viewMode} lessonContent={lessonContent} content={content} />}
						{lessonContent?.type == "live_session" && <LiveSession viewMode={viewMode} lessonContent={lessonContent} content={content} />}
						{lessonContent?.type == "lesson_plan" && <LessonPlan viewMode={viewMode} lessonContent={lessonContent} content={content} />}
					</Col>
				</Row>
			</Collapse>
		</div>
	);
};

export default RLessonContent;
