import { get } from "config/api";

export const lessonRecordingApi = {
  getLessonRecording: (lessonId) => get(`api/lessons/vcRecordings/${lessonId}`),
};
