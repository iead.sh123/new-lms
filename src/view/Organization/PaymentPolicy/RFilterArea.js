import React, { useContext } from "react";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import { primaryColor, boldGreyColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import Switch from "react-bootstrap-switch";
import styles from "../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RFilterArea = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	return (
		<RFlex styleProps={{ justifyContent: "space-between", minHeight: "35px" }}>
			<RFlex>
				<RFlex
					styleProps={{
						cursor: "pointer",
						alignItems: "center",
						color: PaymentPolicyData.values.showFilter ? primaryColor : boldGreyColor,
						height: "100%",
					}}
					onClick={() => PaymentPolicyData.handleShowFilter()}
				>
					<i className={iconsFa6.filter} />
					{tr`filter`}
				</RFlex>
				{PaymentPolicyData.values.showFilter && (
					<RFlex styleProps={{ height: "100%", justifyContent: "flex-start" }}>
						{PaymentPolicyData.values.itemNameValue !== "coupons" && (
							<RSelect
								className={styles.filter__select}
								closeMenuOnSelect={true}
								option={PaymentPolicyData.values.assignedToItems}
								placeholder={tr`assigned_to`}
								onChange={(item) => {
									PaymentPolicyData.setFieldValue("assignedToItemValue", item);
								}}
								value={[PaymentPolicyData.values.assignedToItemValue]}
							/>
						)}
						<RSelect
							className={styles.filter__select}
							closeMenuOnSelect={true}
							option={PaymentPolicyData.values.timeItems}
							placeholder={tr`left_time`}
							onChange={(item) => {
								PaymentPolicyData.setFieldValue("timeItemValue", item);
							}}
							value={[PaymentPolicyData.values.timeItemValue]}
						/>

						<RButton
							text={tr`apply`}
							onClick={() => PaymentPolicyData.handleSearch()}
							color="primary"
							className={styles.apply__button + " p-1"}
							loading={PaymentPolicyData.discountLoading || PaymentPolicyData.couponLoading}
							disabled={PaymentPolicyData.discountLoading || PaymentPolicyData.couponLoading}
						/>
						<RButton
							text={tr`reset`}
							onClick={() => PaymentPolicyData.handleResetFilter()}
							color="link"
							className={styles.reset__button + " p-1"}
							disabled={PaymentPolicyData.discountLoading || PaymentPolicyData.couponLoading}
						/>
					</RFlex>
				)}
			</RFlex>
			<RFlex styleProps={{ height: "100%", position: "relative", top: 5 }}>
				<Switch
					value={PaymentPolicyData.switchMode}
					offText={<i className={iconsFa6.list} />}
					onText={<i className={iconsFa6.list} />}
					onColor="success"
					offColor="success"
					onChange={() => PaymentPolicyData.handleSwitchMode()}
					size="small"
				/>
			</RFlex>
		</RFlex>
	);
};

export default RFilterArea;
