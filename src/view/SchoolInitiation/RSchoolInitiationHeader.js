import React, { useContext } from "react";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { Form, FormGroup, Input } from "reactstrap";
import { useParams } from "react-router-dom";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RButton from "components/Global/RComs/RButton";
import styles from "./RSchoolInitiation.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RSchoolInitiationHeader = ({
  handleChangeSearch,
  handleSearch,
  searchLoading,
  searchData,
  setSearchData,
}) => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  const { sIType, sITypeId } = useParams();
  const { width } = useWindowDimensions();
  const mobile = width < 1000;

  return (
    <>
      <RFlex
        styleProps={{
          display: mobile ? "block" : "flex",
          width: "100%",
          alignItems: "center",

          gap: 30,
        }}
      >
        <RButton
          color="primary"
          text={
            sIType == "education_stage"
              ? tr`create_grade_level`
              : sIType == "grade_levels"
              ? tr`create_curricula`
              : tr`create_education_stage`
          }
          faicon="fa fa-plus"
          onClick={SchoolInitiationData.handleOpen}
        />
        <Form className={styles.form_input}>
          <FormGroup>
            <Input
              type="text"
              placeholder={tr`search`}
              className={styles.search_input}
              value={searchData}
              onChange={(event) => {
                handleChangeSearch(event.target.value);
              }}
            />
            <i
              aria-hidden="true"
              className={
                searchLoading
                  ? `fa fa-refresh fa-spin ${styles.search_input_icon}`
                  : `fa fa-search ${styles.search_input_icon}`
              }
              onClick={() => searchData !== "" && handleSearch()}
            />
            <i
              aria-hidden="true"
              className={`fa fa-close ${styles.clear_input_icon}`}
              onClick={() => {
                if (searchData !== "") {
                  setSearchData("");
                  handleSearch("");
                }
              }}
            />
          </FormGroup>
        </Form>
      </RFlex>
    </>
  );
};

export default RSchoolInitiationHeader;
