import React, { useContext, Fragment } from "react";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { primaryColor } from "config/constants";
import { SwiperSlide } from "swiper/react";
import RDiscussionPost from "./RDiscussionPost";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RDiscussion = () => {
  const history = useHistory();
  const {
    sectionId,
    cohortId,
    searchQuery,
    categoryName,
    userId,
    mandatoryFlag,
  } = useParams();
  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  return (
    <Fragment>
      {" "}
      {/* ------------------------ Mandatory posts ------------------------*/}
      {!cohortId || mandatoryFlag || searchQuery ? (
        ""
      ) : (
        <Fragment>
          <RFlex
            styleProps={{ width: "100%", justifyContent: "space-between" }}
          >
            <h6>{tr`Mandatory`}</h6>
            <h6
              className="mr-4 ml-0 text-capitalize"
              style={{ color: primaryColor }}
              onClick={() =>
                history.push(
                  `${baseURL}/${genericPath}/section/${sectionId}/mandatory-posts/${"mandatory"}/${cohortId}`
                )
              }
            >{tr`See All Posts`}</h6>
          </RFlex>
          <RSwiper slidesPerView={1} navigation={false}>
            {DiscussionData?.mandatoryPosts &&
              DiscussionData?.mandatoryPosts.map((post) => {
                return (
                  <SwiperSlide key={post?.id}>
                    <RDiscussionPost
                      post={post}
                      mB={"mb-0"}
                      typeContent="post"
                      community={false}
                      specificMandatoryPost={true}
                      postType="mandatory"
                    />
                  </SwiperSlide>
                );
              })}
          </RSwiper>
        </Fragment>
      )}
      {/* ------------------------ Most reacted Post ------------------------*/}
      {!(categoryName || userId || searchQuery || mandatoryFlag) && (
        <>
          <h6 className="mt-4">{tr`Most reacted on`}</h6>
          {DiscussionData?.mostReactedPost !== null && (
            <RDiscussionPost
              post={DiscussionData?.mostReactedPost}
              mB={"mb-4"}
              community={true}
              typeContent="post"
              postType="mostReacted"
            />
          )}
        </>
      )}
      {/* ------------------------ Title before all posts ------------------------*/}
      {!userId && (
        <RFlex
          styleProps={{
            justifyContent: searchQuery ? "space-between" : "start",
          }}
        >
          {!(categoryName || searchQuery || mandatoryFlag) && (
            <i className="fa fa-globe" aria-hidden="true" />
          )}
          <h6 className="text-muted text-capitalize">
            {categoryName
              ? categoryName
              : searchQuery
              ? tr`search_results`
              : mandatoryFlag
              ? tr`mandatory`
              : tr`all`}
          </h6>
          {searchQuery && (
            <h6
              className="text-capitalize cursor-pointer"
              style={{ color: primaryColor }}
              onClick={() =>
                history.push(`${baseURL}/${genericPath}/community`)
              }
            >{tr`back_to_community`}</h6>
          )}
        </RFlex>
      )}
      {/* ------------------------ Title before Waiting posts ------------------------*/}
      {categoryName == "Waiting" && (
        <RFlex>
          <h6 className="mt-3">{tr`Need admin’s review to publish`}</h6>
        </RFlex>
      )}
      {/*------------------------ All posts ------------------------*/}
      {DiscussionData?.posts.map((post) => {
        return (
          <RDiscussionPost
            post={post}
            mB={"mb-4"}
            typeContent="post"
            community={mandatoryFlag ? false : true}
            postType="all"
          />
        );
      })}
    </Fragment>
  );
};

export default RDiscussion;
