import { Input } from "reactstrap";
import styles from "./AppInput.Module.scss";

const AppInput = (props) => {
  return <Input {...props} className={styles["app-input"]}/>;
};

export default AppInput;
