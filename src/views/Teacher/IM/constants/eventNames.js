export const eventTypes = {
    NEW_MESSAGE: "MessageArrived",
    NEW_CHAT: "NewChatArrived",
    MESSAGE_UNSENT: "MessageDeleted",
    CHANGE_POINTER: "MessageRead",
    TUMBNAIL_UPDATED: "thumb_file_created"
}

export const GENERIC_EVENT_NAME = "generic_channel";