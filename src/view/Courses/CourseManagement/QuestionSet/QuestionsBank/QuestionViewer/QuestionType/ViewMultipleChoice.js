import React from "react";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import styles from "../QuestionViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, FormGroup, Label } from "reactstrap";
import { Services } from "engine/services";

const ViewMultipleChoice = ({ question, isGroup }) => {
  return (
    <Row className="mt-4 d-flex align-items-center">
      {!isGroup && (
        <Col xs={12} className="p-0">
          <RFlex styleProps={{ flexDirection: "column" }}>
            <RFlex>
              <i className="fas fa-tasks pt-1" />
              <Label className="p-0 m-0">{tr`multiple_choice`}</Label>
            </RFlex>
          </RFlex>
        </Col>
      )}
      <Col xs={12} className="p-0 pt-3">
        {isGroup ? (
          <ul>
            <li>
              <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
            </li>
          </ul>
        ) : (
          <h6 dangerouslySetInnerHTML={{ __html: question?.text }} />
        )}
        {question?.files[0]?.hash_id !== undefined && (
          <img
            src={Services.storage.file + question?.files[0]?.hash_id}
            alt={question?.id}
            className={styles.image}
          />
        )}
      </Col>
      <Col xs={12} className="p-0 pt-3">
        {question.choices.map((choice, index) => (
          <div key={choice.id}>
            <FormGroup className="d-flex align-items-center">
              <AppCheckbox
                name={question?.id}
                label={String.fromCharCode(65 + index) + " . " + choice.text}
                checked={question.correctAnswer
                  ?.split(",")
                  ?.map(Number)
                  .includes(+choice.order)}
              />
              {choice?.files[0]?.hash_id !== undefined && (
                <img
                  src={Services.storage.file + choice?.files[0]?.hash_id}
                  alt={choice?.id}
                  className={styles.image}
                />
              )}
            </FormGroup>
          </div>
        ))}
      </Col>
    </Row>
  );
};

export default ViewMultipleChoice;
