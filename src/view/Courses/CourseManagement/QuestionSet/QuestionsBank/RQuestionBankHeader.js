import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { primaryColor } from "config/constants";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import styles from "../QuestionSet.module.scss";

const RQuestionBankHeader = () => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<RFlex
			styleProps={{
				justifyContent: "space-between",
				flexWrap: "wrap",
				alignItems: "center",
			}}
		>
			<RFlex style={{ color: primaryColor, cursor: "pointer" }} onClick={() => QuestionsBankData.handlePushToQsEditor()}>
				<i class="fa-solid fa-arrow-left pt-1 pr-1"></i>
				<span>
					{tr`back_to_creating_`}
					{QuestionsBankData.questionSetType}
				</span>
			</RFlex>
			<UncontrolledDropdown direction="right">
				<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" id="navbarDropdownMenuLink" nav>
					<RButton text={tr`create_new_question`} faicon={"fa-solid fa-chevron-down"} color="primary" outline iconRight={true} />
				</DropdownToggle>
				<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" left>
					{QuestionsBankData.questionTypesEnum.map((qTE) => (
						<DropdownItem
							key={qTE.name}
							className={styles.question__set__types}
							onClick={() => QuestionsBankData.handleOpenQuestionEditor(qTE)}
						>
							<RFlex>
								<i className={qTE.icon} />
								<span>{tr(qTE.label)}</span>
							</RFlex>
						</DropdownItem>
					))}
				</DropdownMenu>
			</UncontrolledDropdown>
		</RFlex>
	);
};

export default RQuestionBankHeader;
