import "scorm-again";

(() => {
	delete window.API;
	delete window.API_1484_11;

	window.API = new Scorm12API();

	window.API_1484_11 = new Scorm2004API();
})();
