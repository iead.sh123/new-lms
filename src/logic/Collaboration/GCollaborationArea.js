
import RAdvancedLister from "components/Global/RComs/RAdvancedLister"; 
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React,{useState} from 'react';
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import RRow from "view/RComs/Containers/RRow";
import RColumn from "view/RComs/Containers/RColumn";
  //   import { useState } from 'react';
 import { useEffect } from 'react';
import RTagsViewer from "components/Global/RComs/Collaboration/RTagsViewer";

import RBieChart from "components/Global/RComs/RBieChart";
import RRecentActivities from "components/Global/RComs/Collaboration/RRecentActivities";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { GCollaborationAreaProviders } from "./GCollaborationAreaProviders";

import { GCollaborationAreaConsumedContents } from "./GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "./GCollaborationAreaClients";
import { GConsumedContents } from "./GConsumedContents";
// import Helper from 'components/Global/RComs/Helper';
// import { Services } from 'engine/services';
export const GCollaborationArea=()=>{
  const history = useHistory();
  const [records, setRecords] = useState([]);
  const [records1, setRecords1] = useState([]);
  const getDataFromBackend = async (specific_url) => {
   
     const url = Services.collaboration.backend+`api/collaboration-area/providers`;
     //Helper.cl(url,"co url")
     let response1 = await get(specific_url ? specific_url : url);
 
     if (response1) {
      //Helper.cl(response1,"co cl response1")
       return response1;
     }
   };

   const getDataFromBackend1 = async (specific_url) => {
   
    const url = Services.collaboration.backend+`api/collaboration-area/destination`;
    //Helper.cl(url,"co url")
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
     //Helper.cl(response1,"co cl response1")
      return response1;
    }
  };




 const collaborate=(content_id,organization_id)=>{
  //Helper.cl(content_id,"content_id");
  //Helper.cl(organization_id,"organization_id");
 }
  const setData = (response) =>
  {
    //Helper.cl(response,"col cli response");
   //return ;
    setRecords(
       response?.data?.data?.map((r, index) => {
        //Helper.cl(r,"col cli r");
 
         return {
           specialProps:
            {
                onClick1:()=>{
                  history.push(`${baseURL}/${genericPath}/collaboration_area_contents/${r.agreement_id}`);
                  //alert(r.id);
                  },
              //'name' ,'parent_id','image'
                title : { text: r.sharable_contents_counts+"Content types"},
              //subtitle : { text: 'Unit Plan'},
               Author:{image:r.image, text:r.name},
              // onClick:()=>{},
              // UpLeftIcon:"fa fa-angle-right", 
              //UpLeftIcon:"All:86", 
              //approveStatus:{icon:r.approved_by_destination?"fa fa-power-off":"fa fa-power-off", title:r.approved_by_destination?"approved_by_destination":"not approved yet",color:r.approved_by_destination?"green":"blue"},
              // bottomBorderColor:"",
              // Author:{text:r.source_organization.name,image: r.source_organization.image},
            },
             
                      

                      
              // active:1
              // approved_by_destination:0
              // created_at:"2023-07-20T06:27:28.000000Z"
              // destination_org_id:8
              // id:12
              // source_org_id:1


            
      }}));
  };

  const setData1 = (response) =>
  {
    //Helper.cl(response,"col cli response");
   //return ;
    setRecords1(
       response?.data?.data?.map((r, index) => {
        //Helper.cl(r,"col cli r");
  
         return {
           specialProps:
            {  title : { text: r.sharable_contents_counts+"Content types"},
            // subtitle : { text: 'Unit Plan'},
             Author:{image:r.image, text:r.name},
              //UpLeftIcon:"All:86", 
              //Actions: [{title: r.active?"Activate":"Deactivate", icon: r.active?"fa fa-power-off":"fa fa-power-off", color:r.active?"green":"red" , 
                   //     onClick:()=>{activate(r.id,!r.active)},
                      //  withBorder:false}
                    //  ],
  
                        bottomBorderColor:"",
                   
                        Description :r.updated_at
                        },
              // active:1
              // approved_by_destination:0
              // created_at:"2023-07-20T06:27:28.000000Z"
              // destination_org_id:8
              // id:12
              // source_org_id:1
  
  
            
      }
    }
    )
    )
  };

//--------------------------------------------------------------------------
// getDataFromBackend={getConsumedContentsFromBackend1}
// setData={setConsumedContentsData}
// records={consumedContentsRecords}

const [consumedContentsData,setConsumedContentsData]=useState([]);
const [consumedContentsRecords,setConsumedContentsRecords]=useState([]);


const getConsumedContentsFromBackend1 = async (specific_url) => {
   
  const url = Services.collaboration.backend+`api/collaboration-area/consumed-contents`;
  let response1 = await get(specific_url ? specific_url : url);

  if (response1) {
   //Helper.cl(response1,"co cl response1")
    return response1;
  }
};
const setConsumedContentsData1=(response)=>{setConsumedContentsData(response?.data?.data)};

useEffect(()=>{
  setConsumedContentsRecords(
    consumedContentsData?.map((r, index) => {
      return {
        specialProps:

        // "sharable_content_name": "Lesson Plans",
        // "name": "lessonplan_42",
        // "source_organization": {
        //     "name": "HazemProvider",
        //     "image": null
        // }

         {  title : { text: r.name},
          subtitle : { text: r.sharable_content_name},
          Author:{image:r.source_organization.image, text:r.source_organization.name},
           //UpLeftIcon:"All:86", 
           //Actions: [{title: r.active?"Activate":"Deactivate", icon: r.active?"fa fa-power-off":"fa fa-power-off", color:r.active?"green":"red" , 
                //     onClick:()=>{activate(r.id,!r.active)},
                   //  withBorder:false}
                 //  ],
                     bottomBorderColor:"",
                     Description :r.updated_at
                     },
           // active:1
           // approved_by_destination:0
           // created_at:"2023-07-20T06:27:28.000000Z"
           // destination_org_id:8
           // id:12
           // source_org_id:1


         
   }
 }
 )
 )
},[consumedContentsData])



//-----------------------------------------------------



  const colors = [
    { textColor: '#000000', backgroundColor: '#CCCCCC' }, // Light gray
    { textColor: '#333333', backgroundColor: '#EAEAEA' }, // Lighter gray
    { textColor: '#333333', backgroundColor: '#F3E5F5' }, // Lavender
    { textColor: '#000000', backgroundColor: '#F8BBD0' }, // Light pink
    { textColor: '#000000', backgroundColor: '#FFCC80' }, // Light orange
    { textColor: '#000000', backgroundColor: '#FFF9C4' }, // Light yellow
    { textColor: '#000000', backgroundColor: '#B2DFDB' }, // Light green
    { textColor: '#000000', backgroundColor: '#B3E5FC' }, // Light blue
    { textColor: '#000000', backgroundColor: '#D1C4E9' }, // Light purple
    { textColor: '#000000', backgroundColor: '#FFD180' }, // Light amber
  ];

  const [tags,setTags]=useState([]);
  useEffect (()=>{
    const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/most-used-tags/destination-part","fail to get",(response)=>{setTags(response.data?.data)},()=>{})}
    get_it();
    },[])
    
        const transformedTags = tags.map((tag) => {
  
      const randomColorIndex = Math.floor(Math.random() * colors.length);
      const randomColor = colors[randomColorIndex];
    
         return {
          size:"25px",
      tag_id: tag.tag_id,
      name: tag.name,
      iconColor:randomColor.textColor, // Assuming backgroundColor represents iconColor
      backgroundColor: randomColor.backgroundColor,
    }});

//---------------------------------------------------------------------
    const [activities,setActivities]=useState([]);
    useEffect (()=>{
      const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/last-activities","fail to get",(response)=>{setActivities(response.data?.data)},()=>{})}
      get_it();
      },[])
      
  
      const [transformedActivities,settransformedActivities]=useState([]);
     
     
      useEffect(()=>{
        
        let transformedActivities1=[]
        Helper.cl(activities,"rc activities")

        Object.entries(activities).map((ac) => {
          Helper.cl(ac,"rc ac");
          const [acName,acActivities]=ac;
        let targetKey="";
        let targetActivities=[];
        if(acName=="today_activities") targetKey="Today"
        if(acName=="yesterday_Activities") targetKey="Yesterday"
        if(acName=="before_yesterday_activities") targetKey="Before"
        Helper.cl(targetKey,"rc targetKey"); 
     
        Object.entries(acActivities).map((activity) => {
        
          Helper.cl(activity,"rc activity");
          
          const [acKey,acValues]=activity;

          Helper.cl(acKey,"acKey")
          Helper.cl(acValues,"acValues")
          
   
          if(acKey=="clients_added")
          {
            Helper.cl("match clients_added");
            acValues.map(v=>{Helper.cl("clients_added"); targetActivities.push(<div>New Client <a href={`organization/${v.id}`}>{v.name}</a></div>)});
          }

        if(acKey=="providers_added")
        {
          Helper.cl("match providers_added");
          acValues.map(v=>{Helper.cl("in providers_added"); targetActivities.push(<div>New Provider <a href={`organization/${v.id}`}>{v.name}</a></div>)});
         } 

         if(acKey=="all_consumed_contents_by_my_clients")
        {
          Helper.cl("match all_consumed_contents_by_my_clients");
          Helper.cl("match all_consumed_contents_by_my_clients");
          acValues.map(v=>{Helper.cl("clients_added"); targetActivities.push(<div>{v}</div>)});
          }

      if(acKey=="all_consumed_contents_by_my_users")
      {
        
        Helper.cl("match all_consumed_contents_by_my_users");
        acValues.map(v=>{Helper.cl("clients_added"); targetActivities.push(<div>{v}</div>)});
        }
      });
      Helper.cl(targetActivities,"rc targetActivities");
      transformedActivities1={...transformedActivities1,[targetKey]:targetActivities} ;
      });
      Helper.cl(transformedActivities1,"ra transformedActivities1");
      settransformedActivities(transformedActivities1) ;
    },[activities])
      
//---------------------------------------------------------------------------


//---------------------------------------------------------------------
const [providerConsumedContents,setProviderConsumedContents]=useState([]);
useEffect (()=>{
  const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/most-consumed-contents/provider-side/","fail to get",(response)=>{setProviderConsumedContents(response.data?.data)},()=>{})}
  get_it();
  },[])

  const [transformedProviderConsumedContents,settransformedProviderConsumedContents]=useState([]);
 
 
  useEffect(()=>{
    
    settransformedProviderConsumedContents(providerConsumedContents.map(t=>({text:t.sharable_content_name,count:t.count})));

    Helper.cl(providerConsumedContents,"providerConsumedContents")
},[providerConsumedContents])
  
//---------------------------------------------------------------------------
//---------------------------------------------------------------------
const [consumerConsumedContents,setconsumerConsumedContents]=useState([]);
useEffect (()=>{
  const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/most-consumed-contents/client-side/","fail to get",(response)=>{setconsumerConsumedContents(response.data?.data)},()=>{})}
  get_it();
  },[])
  

  const [transformedconsumerConsumedContents,settransformedconsumerConsumedContents]=useState([]);
 
 
  useEffect(()=>{
    settransformedconsumerConsumedContents(consumerConsumedContents.map(t=>({text:t.sharable_content_name,count:t.count})));

    Helper.cl(consumerConsumedContents,"consumerConsumedContents")
},[consumerConsumedContents])
  
//---------------------------------------------------------------------------

    const activities1= {Today : ["line1","line2"] ,
    yesterday : ["line","line2"],
    beforeYeterday:["line1","line2"],
   
    Today1 : ["line1","line2"] ,
    yesterday1 : ["line","line2"],
    beforeYeterday1:["line1","line2"],
    Today11: ["line1","line2"] ,
    yesterday11 : ["line","line2"],
    beforeYeterday11:["line1","line2"],
    Today2 : ["line1","line2"] ,
    yesterday22 : ["line","line2"],
    beforeYeterday22:["line1","line2"],
    Today22 : ["line1","line2"] ,
    yesterday2 : ["line","line2"],
    beforeYeterday2:["line1","line2"],
   }
    const data = [
      { text: 'Courses', count: 20 },
      { text: 'Books', count: 15 },
      { text: 'Videos', count: 10 },
      // Add more data objects as needed
    ];


     return <div>
        
      <RTitle text="Providers"/>
      <GCollaborationAreaProviders/>
{/* 

      <RAdvancedLister
       hideTableHeader={false}
       colorEveryOtherRow={false}
       //firstCellImageProperty={"image"}
       getDataFromBackend={getDataFromBackend}
       setData={setData}
       records={records}
       getDataObject={(response) => response.data?.data?.data}
       characterCount={15}
       marginT={"mt-3"}
       marginB={"mb-2"}
       align={"left"}
       SpecialCard={RCollaborationCard}
       showListerMode={"cardLister"}
       swiper
       perLine={4}
/> */}
   <RTitle text="Clients"/>
         {/* <RAdvancedLister
       hideTableHeader={false}
       colorEveryOtherRow={false}
       //firstCellImageProperty={"image"}
       getDataFromBackend={getDataFromBackend1}
       setData={setData1}
       records={records1}
       getDataObject={(response) => response.data?.data?.data}
       characterCount={15}
       marginT={"mt-3"}
       marginB={"mb-2"}
       align={"left"}
       SpecialCard={RCollaborationCard}
       showListerMode={"cardLister"}
       swiper
       perLine={4}
/> */}

<GCollaborationAreaClients/>
  <RTitle text="consumed contents"/>

  <GCollaborationAreaConsumedContents/>
         {/* <RAdvancedLister
       hideTableHeader={false}
       colorEveryOtherRow={false}
       //firstCellImageProperty={"image"}
       getDataFromBackend={getConsumedContentsFromBackend1}
       setData={setConsumedContentsData1}
       records={consumedContentsRecords}
       getDataObject={(response) => response.data?.data?.data}
       characterCount={15}
       marginT={"mt-3"}
       marginB={"mb-2"}
       align={"left"}
       SpecialCard={RCollaborationCard}
       showListerMode={"cardLister"}
       swiper
       perLine={4}
/> */}
<div style={{display:"flex"}}>
<RColumn style={{width:"50%",padding:"20px"}}>
 
{transformedTags&&transformedTags.length?<>
<RTitle text="Most Used Tags"/>
<RTagsViewer tags={transformedTags} />
</>:<></>}

  <div>

       <div >
        <RTitle text="Recent Activities"/>
          <RRecentActivities activities={transformedActivities}/>
       
        </div>
    </div>
</RColumn>
<div style={{width:"50%",padding:"20px"}}>
  

<RTitle text="Consumed Contents"/>
 

 <GConsumedContents/>
      {/* <RBieChart data={transformedconsumerConsumedContents} /> */}
    {/* <RBieChart data={transformedProviderConsumedContents} /> */}


</div>
</div>
</div> 
}