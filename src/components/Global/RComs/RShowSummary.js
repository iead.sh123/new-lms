import { Alert } from "reactstrap";

const RShowSummary = ({
  title,
  details,
  show,
  colseDetails,
  borderColor,
  borderWidth,
  styles = {},
}) => {
  return (
    <>
      <Alert
        isOpen={show}
        style={{
          boxShadow: "0px 1px 10px",
          backgroundColor: " #f4f3ef",
          ...styles,
        }}
        color="red"
        toggle={colseDetails}
        fade={true}
        className="pt-4 mt-1 mr-1 pr-2 pl-2 ml-1"
      >
        {title ? (
          <div className={"title-Details  mt-1 mb-2 badge bg-secondary "}>
            <span> {title}</span>
          </div>
        ) : null}

        {details()}
        <div className="d-flex justify-content-end"></div>
      </Alert>
    </>
  );
};
export default RShowSummary;
