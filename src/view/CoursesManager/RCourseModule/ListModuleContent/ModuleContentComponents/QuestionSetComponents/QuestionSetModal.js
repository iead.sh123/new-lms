import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { allQuestionSetForCourseAsync, pickQuestionSetToModuleAsync } from "store/actions/global/questionSet.actions";
import { useParams, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const QuestionSetModal = ({ questionSetType, handleClosePickFromCourseModal }) => {
	const { courseId } = useParams();
	const location = useLocation();
	const dispatch = useDispatch();

	const queryParams = new URLSearchParams(location.search);
	const moduleIdParams = queryParams.get("moduleId");

	const [searchData, setSearchData] = useState("");

	const { allQuestionSetForCourse, allQuestionSetForCoursePagination, allQuestionSetForCourseLoading } = useSelector(
		(state) => state.questionSetRed
	);

	useEffect(() => {
		dispatch(allQuestionSetForCourseAsync(null, courseId, `?type=${questionSetType}&filter=${"ALL"}`));
	}, []);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		dispatch(
			allQuestionSetForCourseAsync(
				null,
				courseId,
				`?type=${questionSetType}&filter=${"ALL"}${emptySearch == undefined ? `&searchText=${searchData}` : ""}`
			)
		);
	};

	const _records = allQuestionSetForCourse?.map((rc) => ({
		id: rc.id,
		details: [{ key: tr`title`, value: rc?.title }],
		actions: [
			{
				name: "check",
				icon: iconsFa6.plus,
				color: "primary",
				outline: true,
				onClick: () => dispatch(pickQuestionSetToModuleAsync(moduleIdParams, questionSetType, rc.id, handleClosePickFromCourseModal)),
			},
		],
	}));

	return (
		<div>
			{allQuestionSetForCourseLoading ? (
				<Loader />
			) : (
				<>
					<RSearchHeader
						searchLoading={allQuestionSetForCourseLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						inputWidth={"50%"}
					/>
					{allQuestionSetForCourse?.length > 0 ? (
						<>
							<RLister Records={_records} />
							{allQuestionSetForCoursePagination.total > 0 ? (
								<NewPaginator
									firstPageUrl={allQuestionSetForCoursePagination.firstPageUrl}
									lastPageUrl={allQuestionSetForCoursePagination.lastPageUrl}
									currentPage={allQuestionSetForCoursePagination.currentPage}
									lastPage={allQuestionSetForCoursePagination.lastPage}
									prevPageUrl={allQuestionSetForCoursePagination.prevPageUrl}
									nextPageUrl={allQuestionSetForCoursePagination.nextPageUrl}
									total={allQuestionSetForCoursePagination.total}
									// data={`type=${questionSetType}&filter=${"ALL"}`}
									getAction={allQuestionSetForCourseAsync}
								/>
							) : (
								""
							)}
						</>
					) : (
						<REmptyData />
					)}
				</>
			)}
		</div>
	);
};

export default QuestionSetModal;
