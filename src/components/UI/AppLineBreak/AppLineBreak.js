import styles from "./AppLineBreak.Module.scss";

const AppLineBreak = () => {
  return <hr className={styles["app-line-break"]} />;
};

export default AppLineBreak;
