import Helper from "components/Global/RComs/Helper";
import { produce } from "immer";
import { SET_NOTIFYME_SUCCESS } from "store/actions/global/globalTypes";
import {
  RECEIVE_NEW_NOTIFICATIONS,
  Add_PUSHED_CHAT,
  MARK_CHATS_AS_READ,
  SET_USER_CHATS,
  REMOVE_NOTIFICATION,
  GET_ANNOUNCEMENT_FROM_LOCAL_STORAGE,
  NOTIFICATION_UNSEEN_REQUEST,
  NOTIFICATION_UNSEEN_SUCCESS,
  NOTIFICATION_UNSEEN_ERROR,
  VIEW_NOTIFICATIONS_REQUEST,
  VIEW_NOTIFICATIONS_SUCCESS,
  VIEW_NOTIFICATIONS_ERROR,
  TRACKABLE_RECORD_ASYNC,
} from "store/actions/global/globalTypes";

const initialState = {
  chats: 0,
  notificationsTotalRecord: 0,
  notifications: [],
  postNotifications: [],
  announcementNotifications: [],
  notificationsLoading: false,
};

let ind;
let indPost;
let indAnnouncement;
const NotificationsReducer = (state = initialState, action) => {
  const { payload, type } = action;
  return produce(state, (draft) => {
    switch (type) {
      case NOTIFICATION_UNSEEN_REQUEST:
        draft.notificationsLoading = true;
        return draft;
      case NOTIFICATION_UNSEEN_SUCCESS:
        draft.notificationsLoading = false;
        draft.notifications = action.payload.notificationUnseen;
        draft.notificationsTotalRecord =
          action.payload.notificationUnseenTotalRecord;
        return draft;
      case NOTIFICATION_UNSEEN_ERROR:
        draft.notificationsLoading = false;
        return draft;

      case VIEW_NOTIFICATIONS_SUCCESS:
        // draft.notifications = action.payload.new_data;
        draft.notificationsTotalRecord -= 1; // action.payload.total_record;
        const g = draft.notifications.find(
          (g) => g.id == payload.notification_id
        );
        g.seen = true;

        // ind = draft.notifications.findIndex(
        //   (el) => el.event_id == action.payload.notification_id
        // );
        // indPost = draft.postNotifications.findIndex(
        //   (el) => el.event_id == action.payload.notification_id
        // );
        // indAnnouncement = draft.announcementNotifications.findIndex(
        //   (el) => el.event_id == action.payload.notification_id
        // );

        // if (ind !== -1) {
        //   draft.notifications.splice(ind, 1);
        // }
        // if (indPost !== -1) {
        //   draft.postNotifications.splice(indPost, 1);
        // }
        // if (indAnnouncement !== -1) {
        //   draft.announcementNotifications.splice(indAnnouncement, 1);
        // }
        return draft;
        "SET_NOTIFY_ME"
      case RECEIVE_NEW_NOTIFICATIONS:
        const ig = action.payload;
          Helper.cl(action.payload.shown,"action.payload.shown");
        if (action.payload.shown == false||action.payload.shown == "false") {
          return;
        }
        if (
          action.payload?.event == "" ||
          action.payload == "" ||
          action.payload == ""
        ) {
        }
        ///-----------------------
        const selectedGroup = draft.notifications.find(
          (g) =>
            g.table_name == ig.table_name &&
            g.row_id == ig.row_id &&
            !g.seen
        );
          Helper.cl(draft?.notifications?.length,"draft.notifications");
          Helper.cl(selectedGroup?.id,"selectedGroup");
        if (selectedGroup) {
          draft.notifications
            .find((g) => selectedGroup.id == g.id)
            .notifications.push(ig.notifications[0]);
          const oldNOts =selectedGroup.notifications;// draft.notifications.find((g) => selectedGroup.id == g.id).notifications;
         // draft.notifications.find((g) => selectedGroup.id == g.id).otherNots=oldNOts.length;
         // draft.notifications.find((g) => selectedGroup.id == g.id).title + ",and" + (oldNOts.length-1) +" others";
          // draft.notifications.find(
          //   (g) => selectedGroup.id == g.id
          // ).description += "and" + oldNOts.length +" other notifications";
          selectedGroup.otherNots=oldNOts.length;
          //selectedGroup.description += "and" + oldNOts.length +" other notifications";

        } else {
          draft.notifications.unshift(action.payload);
          draft.notificationsTotalRecord = draft.notificationsTotalRecord + 1;
        }
        //------------------------
        // if (action?.payload?.payload?.type == "posts") {
        //   draft.postNotifications.unshift(action.payload);
        // } else if (action?.payload?.payload?.type == "announcements") {
        //   draft.announcementNotifications.unshift(action.payload);
        // }
        return draft;
      case SET_NOTIFYME_SUCCESS:
        draft.notifyMe = action.payload.notifyMe;
        return draft;
      case SET_NOTIFYME_SUCCESS:
        draft.notifyMe = action.payload.notifyMe;
        return draft;

      case TRACKABLE_RECORD_ASYNC:
        draft.notifications = action.payload.notifications;
        draft.notificationsTotalRecord = action.payload.countNotifications;
        return draft;

      case SET_USER_CHATS:
        draft.chats = action.chats.reduce(
          (prev, curr) => (curr.pivot.seen ? prev : prev + 1),
          0
        );
        return draft;

      case MARK_CHATS_AS_READ:
        draft.chats = 0;
        return draft;

      case Add_PUSHED_CHAT:
        draft.chats++;
        return draft;

      default:
        return draft;
    }
  });
};

export default NotificationsReducer;
