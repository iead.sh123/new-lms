import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Row, Col, Form, FormGroup, Button } from "reactstrap";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import { fetchAllChildrens } from "store/actions/parent/childrensData.actions";
import {
  getUserTypesAsync,
  getUserAsync,
  addAbuseAsync,
} from "store/actions/global/abuse.action";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import RSelect from "components/Global/RComs/RSelect";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";
import RButton from "../RComs/RButton";
import { setTitle } from "store/actions/student/navActions";
import styles from "./Abuse.Module.scss";
import withDirection from "hocs/withDirection";

const AddAbuse = ({ dir }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [optionsValueChild, setOptionsValueChild] = useState([]);
  const [optionsValueUserTypes, setOptionsValueUserTypes] = useState([]);
  const [optionsValueUsers, setOptionsValueUsers] = useState([]);

  const { user } = useSelector((state) => state?.auth);
  const {
    add_Abuse_Loading,
    userTypes,
    users,
    user_Types_Loading,
    users_Loading,
  } = useSelector((state) => state.abuseServiceRed);
  const { allChildrens } = useSelector((state) => state.childrensDataRed);

  const [formGroup, setFormGroup] = useState({
    description: "",
    students: [],
    users: [],
  });

  useEffect(() => {
    dispatch(fetchAllChildrens());
    dispatch(getUserTypesAsync());
  }, []);

  const handleDescriptionChange = (data) => {
    setFormGroup({
      ...formGroup,
      description: data,
    });
  };

  const getOptionsChild = () => {
    const arrayDataChild = [];
    allChildrens &&
      allChildrens.map((child) => {
        arrayDataChild.push({
          value: child.id,
          label: `${child.user.first_name} ${child.user.last_name}`,
        });
      });
    setOptionsValueChild(arrayDataChild);
  };

  const getOptionsUserTypes = () => {
    const arrayDataUserTypes = [];
    userTypes &&
      userTypes.map((child) => {
        arrayDataUserTypes.push({
          value: Math.random(),
          label: child,
        });
      });
    setOptionsValueUserTypes(arrayDataUserTypes);
  };

  const getOptionsUsers = () => {
    const arrayDataUsers = [];
    users &&
      users.map((user) => {
        arrayDataUsers.push({
          value: user.id,
          label: user.name,
        });
      });
    setOptionsValueUsers(arrayDataUsers);
  };

  // Set Select Child And UserTypes
  useEffect(() => {
    getOptionsChild();
    getOptionsUserTypes();
  }, [allChildrens, userTypes]);

  // Set Select Classes
  useEffect(() => {
    if (users) {
      getOptionsUsers();
    }
  }, [users]);

  //Handle Select Child
  const handleSelectChild = (e) => {
    const ArraySelected = e;
    let ArrayStudent = [];
    for (let i = 0; i < ArraySelected.length; i++) {
      ArrayStudent.push(ArraySelected[i].value);
    }

    setFormGroup({
      ...formGroup,
      students: ArrayStudent,
    });
  };

  //Handle Select User Types
  const handleSelectUserTypes = (e) => {
    const ArraySelected = e;
    let ArrayUserTypes = [];
    for (let i = 0; i < ArraySelected.length; i++) {
      ArrayUserTypes.push(ArraySelected[i].label);
    }

    dispatch(getUserAsync({ payload: { users_types: ArrayUserTypes } }));
  };

  //Handle Select Users
  const handleSelectUsers = (e) => {
    const ArraySelected = e;
    let ArrayUserTypes = [];
    for (let i = 0; i < ArraySelected.length; i++) {
      ArrayUserTypes.push(ArraySelected[i].value);
    }
    setFormGroup({
      ...formGroup,
      users: ArrayUserTypes,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addAbuseAsync(formGroup, history));
  };

  useEffect(() => {
    dispatch(setTitle(tr("addd_abuse_report")));
  }, []);

  const flag = true;

  return (
    <div
      className={
        !user?.useNewTheme && user?.type == "parent" ? "container" : "content"
      }
      style={{
        paddingTop: !user?.useNewTheme && user?.type == "parent" && "18vh",
      }}
    >
      <Row>
        <Col xs={12} className={dir ? "" : styles.button_rtl}>
          <RButton
            text={tr`back`}
            onClick={() =>
              user?.type == "parent"
                ? history.push(`${process.env.REACT_APP_BASE_URL}/parent/abuse`)
                : user?.type == "teacher"
                ? history.push(
                    `${process.env.REACT_APP_BASE_URL}/teacher/abuse`
                  )
                : user?.type == "employee"
                ? history.push(`${process.env.REACT_APP_BASE_URL}/admin/abuse`)
                : user?.type === "school_advisor" ||
                  user?.type === "seniorteacher" ||
                  user?.type === "principal"
                ? history.push(
                    `${process.env.REACT_APP_BASE_URL}/senior-teacher/abuse`
                  )
                : ""
            }
            color="info"
            outline
          />
        </Col>
      </Row>

      {user_Types_Loading ? (
        <Loader />
      ) : (
        <Form
          onSubmit={(e) => handleSubmit(e)}
          style={{
            padding: "20px 10px",
            background: "#fff",
            boxShadow: "0px 0px 10px 4px rgba(0, 0, 0, 0.03)",
          }}
        >
          <Row style={{ display: "flex", justifyContent: "center" }}>
            <Col xs={12}>
              <FormGroup>
                <label
                  style={{ fontWeight: "bold" }}
                  className={dir ? "" : styles.text_rtl}
                >
                  {tr("abused_children")} :
                </label>
                <RSelect
                  option={optionsValueChild}
                  closeMenuOnSelect={true}
                  placeholder={tr`select_child`}
                  onChange={(e) => handleSelectChild(e)}
                  isMulti
                  required
                />
              </FormGroup>

              <FormGroup>
                <label
                  style={{ fontWeight: "bold" }}
                  className={dir ? "" : styles.text_rtl}
                >
                  {tr("report")} :
                </label>
                <CKEditor
                  id="editor"
                  editor={ClassicEditor}
                  name="description"
                  //   data={groupForms.description}
                  onChange={(event, editor) => {
                    const data = editor.getData();
                    handleDescriptionChange(data);
                  }}
                  required
                />
              </FormGroup>
            </Col>

            <Col xs={12}>
              <label
                style={{ fontWeight: "bold" }}
                className={dir ? "" : styles.text_rtl}
              >
                {tr("abusers")} :
              </label>
              <Row className={styles.abusers}>
                <Col xs={6}>
                  <FormGroup>
                    <label className={dir ? "" : styles.text_rtl}>
                      {tr("roles")} :
                    </label>
                    <RSelect
                      option={optionsValueUserTypes}
                      closeMenuOnSelect={true}
                      placeholder={tr`select_role`}
                      onChange={(e) => handleSelectUserTypes(e)}
                      isMulti
                      required
                    />
                  </FormGroup>
                </Col>
                {users_Loading ? (
                  // Loader(ELLIPSIS)
                  <h6 style={{ margin: "auto" }}> {tr`loading`}....</h6>
                ) : (
                  <Col xs={6}>
                    <FormGroup>
                      <label className={dir ? "" : styles.text_rtl}>
                        {tr("staff_members")} :
                      </label>
                      <RSelect
                        option={optionsValueUsers}
                        closeMenuOnSelect={true}
                        placeholder={tr`select_staff_members`}
                        onChange={(e) => handleSelectUsers(e)}
                        isMulti
                      />
                    </FormGroup>
                  </Col>
                )}
              </Row>
            </Col>

            <Col xs={12} style={{ display: "flex", justifyContent: "center" }}>
              <Button
                block
                className="btn-round mb-3 "
                color="warning"
                disabled={add_Abuse_Loading ? true : false}
                style={{ width: 400 }}
              >
                {add_Abuse_Loading ? (
                  <>
                    {add_Abuse_Loading && tr`send`}{" "}
                    <i className="fa fa-refresh fa-spin"></i>
                  </>
                ) : (
                  tr("send")
                )}
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </div>
  );
};

export default withDirection(AddAbuse);
