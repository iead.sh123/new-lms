import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import React from "react";

const CorrectOrReviewMatch = ({ question, ind }) => {
  const studentAnswers =
    question.answer !== ""
      ? question.answer
          .split(",")
          .map((pair) => pair.split(":")[1])
          .map(Number)
      : [];

  const correctAnswer = question.correctAnswer
    ? question.correctAnswer
        .split(",")
        .map((pair) => pair.split(":")[0])
        .map(Number)
    : [];

  const updatedStudentAnswers = studentAnswers.filter(
    (id) => !correctAnswer.includes(id)
  );

  return (
    <div>
      {updatedStudentAnswers && updatedStudentAnswers.length > 0 && (
        <div style={{ color: dangerColor }}>
          <i className="fa-solid fa-xmark pt-1 pr-2" />
          {question.pairs &&
            question.pairs.filter((item) =>
              updatedStudentAnswers.includes(item.order)
            )[ind]?.text}
        </div>
      )}
      {correctAnswer && correctAnswer.length > 0 && (
        <div style={{ color: successColor }}>
          <i className="fa-solid fa-check pt-1 pr-2" />
          {question.pairs &&
            question.pairs.filter((item) => correctAnswer.includes(item.order))[
              ind
            ]?.text}
        </div>
      )}
    </div>
  );
};

export default CorrectOrReviewMatch;
