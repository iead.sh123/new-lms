import React, { useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import RTabsPanel_Vertical from "components/Global/RComs/RTabsPanel_Vertical";
import UserManagementAdd from "logic/UserManagement/UserManagementAdd";
import GRolesByUser from "../Roles/GRolesByUser";

import UsersToRoleLister from "../Users/UsersToRoleLister";
import GPermissionsToUser from "../Permissions/GPermissions";
import GRolesByPermission from "../Roles/GRolesByPermission";
import UsersToPermissionLister from "../Users/UsersToPermissionLister";
import GPermissionsToRole from "../Permissions/GPermissionsToRole";
import GPermissionForm from "../Permissions/GPermissionForm";
import GAddRole from "../Roles/GAddRole";

const ManagementTabs = () => {
  const {
    roleId,
    permissionId,
    userAndPermissionType,
    organizationUserId,
    tabTitle,
    categoryId,
  } = useParams();

  const UserTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/users-and-permissions/user-type/${organizationUserId}/${tabTitle}/properties`,
      content: () => {
        return <UserManagementAdd />;
      },
    },
    {
      name: "roles",
      title: "Roles",
      url: `${baseURL}/${genericPath}/users-and-permissions/user-type/${organizationUserId}/${tabTitle}/roles`,
      content: () => {
        return <GRolesByUser />;
      },
    },
    {
      name: "permissions",
      title: "Permissions",
      url: `${baseURL}/${genericPath}/users-and-permissions/user-type/${organizationUserId}/${tabTitle}/permissions`,
      content: () => {
        return <GPermissionsToUser />;
      },
    },
  ];

  const RoleTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/users-and-permissions/role/${roleId}/properties`,
      content: () => {
        return <GAddRole />;
      },
    },
    {
      name: "users",
      title: "Users",
      url: `${baseURL}/${genericPath}/users-and-permissions/role/${roleId}/users`,
      content: () => {
        return <UsersToRoleLister />;
      },
    },
    {
      name: "permissions",
      title: "Permissions",
      url: `${baseURL}/${genericPath}/users-and-permissions/role/${roleId}/permissions`,
      content: () => {
        return <GPermissionsToRole />;
      },
    },
  ];

  const PermissionTabs = [
    {
      name: "properties",
      title: "Properties",
      url: `${baseURL}/${genericPath}/users-and-permissions/permission/${permissionId}/category/${categoryId}/properties`,
      content: () => {
        return <GPermissionForm />;
      },
    },
    {
      name: "users",
      title: "Users",
      url: `${baseURL}/${genericPath}/users-and-permissions/permission/${permissionId}/category/${categoryId}/users`,
      content: () => {
        return <UsersToPermissionLister />;
      },
    },
    {
      name: "roles",
      title: "Roles",
      url: `${baseURL}/${genericPath}/users-and-permissions/permission/${permissionId}/category/${categoryId}/roles`,
      content: () => {
        return <GRolesByPermission />;
      },
    },
  ];

  return (
    <RTabsPanel_Vertical
      Tabs={
        userAndPermissionType == "user-type"
          ? UserTabs
          : userAndPermissionType == "role"
          ? RoleTabs
          : userAndPermissionType == "permission"
          ? PermissionTabs
          : []
      }
    />
  );
};

export default ManagementTabs;
