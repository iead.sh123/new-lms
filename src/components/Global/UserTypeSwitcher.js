import React from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { SWITCH_TYPE } from "store/actions/global/globalTypes";
import { useHistory } from "react-router-dom";
import Helper from "./RComs/Helper";

const UserTypeSwitcher = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const selected_school = useSelector(
    (state) => state?.auth?.user?.current_organization
  );

  const schools = useSelector((state) => state?.auth?.schoolsAndTypes);
  const currentType = useSelector((state) => state?.auth?.user?.current_type);

  const swicthSchool = (type, school_id) => {
    dispatch({ type: SWITCH_TYPE, payload: { type, school_id } });
    // setTimeout(
    //   () =>
    //     (window.location.href = `${process.env.REACT_APP_BASE_URL}/${
    //       type === "teacher"
    //         ? "teacher"
    //         : type === "school_advisor" ||
    //           type === "seniorteacher" ||
    //           type === "principal"
    //         ? "senior-teacher"
    //         : type === "employee"
    //         ? "admin"
    //         : "student"
    //     }/dashboard`),
    //   500
    // );

    // var  interval =  setInterval(
    //  () =>
    //   {

    //   if(selected_school==school_id && type==currentType)
    //{
    const ur = `${process.env.REACT_APP_BASE_URL}/auth/loader`;

    history.push(ur); // }
    //clearInterval(interval);
    //${    type === "teacher"
    //      ? "teacher"
    //      : type === "school_advisor" ||
    //        type === "seniorteacher" ||
    //        type === "principal"
    //      ? "senior-teacher"
    //      : type === "employee"
    //      ? "admin"
    //    : "student"
    //  }/dashboard`}; ;
  };
  //}
  //   ,500
  // );

  //};

  return (
    <div>
      {/* selected school {selected_school}
      schools {JSON.stringify(schools)} */}
      <UncontrolledDropdown
        // className="btn-magnify mr-4"
        // className="mr-4"
        /* className="btn-rotate" */ nav
      >
        <DropdownToggle
          aria-haspopup={true}
          caret
          color="default"
          data-toggle="dropdown"
          id="navbarDropdownMenuLink"
          // style={{padding: '5px', marginRight: 15}}
          nav
        >
          <i className="nc-icon nc-single-02" />
        </DropdownToggle>
        <DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
          {schools?.map((elem) => {
            return (
              <DropdownItem
                key={elem.id}
                onClick={() => swicthSchool(elem.type, elem.organization.id)}
                disabled={
                  selected_school == elem.organization.id &&
                  elem?.type?.toLowerCase() == currentType?.toLowerCase()
                }
                style={
                  selected_school == elem.organization.id &&
                  elem?.type?.toLowerCase() == currentType?.toLowerCase()
                    ? { backgroundColor: "gray", Color: "gray" }
                    : { backgroundColor: "lightgray", Color: "black" }
                }
              >
                <i>
                  {selected_school == elem.organization.id &&
                  elem?.type?.toLowerCase() == currentType?.toLowerCase() ? (
                    <span class="dot"></span>
                  ) : (
                    <></>
                  )}
                  {elem.organization_name} as {elem.type}
                  {/* {elem.organization.id } |cur  {selected_school} {currentType}                   {!((selected_school == elem.organization.id )&&                (elem?.type?.toLowerCase()==currentType?.toLowerCase()))?" active":" not active"}
                   */}{" "}
                </i>
              </DropdownItem>
            );
          })}
        </DropdownMenu>
      </UncontrolledDropdown>
    </div>
  );
};

export default UserTypeSwitcher;
