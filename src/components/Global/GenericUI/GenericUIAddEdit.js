import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { post } from "config/api";
import ReactBSAlert from "react-bootstrap-sweetalert";
import RAddEdit from "../RComs/RAddEdit";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GenericUIAddEdit = (props) => {
	const [hiddenColumns, setHiddenColumns] = useState(["id", "password"]);
	const { entity_specific_name, entity_specific_id } = useParams();
	let record_id = entity_specific_id;

	let table_name = "";
	if (props && props.prop_entity_specific_name) {
		table_name = props.prop_entity_specific_name;
		record_id = false;
	} else table_name = entity_specific_name;

	const history = useHistory();

	const [initialformItems, setInitialFormItems] = useState([]);
	const [tableProps, setTableProps] = useState({});
	const [alerts, setAlerts] = useState(false);
	const [record, setRecord] = useState(null);
	const [loading, setLoading] = useState(false);

	const TypeEnum = {
		text: "text",
		number: "number",
		time: "time",
		checkbox: "checkbox",
		select: "select",
		color: "color",
		file: "file",
	};

	const showAlerts = (child) => {
		setAlerts(child);
	};

	//handle options to set in select
	const getOptionsFromData = (data) => {
		const output = [];
		data.map((d) => {
			if (d.ID) output.push({ value: d.ID, label: d.name });
		});

		return output;
	};

	//handleChange type:text
	const handleTextChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:number
	const handleValueChanged = (e, column) => {
		let temp = record;
		if (e.target && e.target.value) {
			temp[column] = e.target.value;
			setRecord(temp);
		}
	};

	//handleChange type:select
	const handleSelectValueChanged = (e, column) => {
		let temp = record;
		temp[column] = e.value;
		setRecord(temp);
	};

	//handleChange type:checkbox
	const handleChecked = (e, column) => {
		let temp = record;
		temp[column] = e.target.checked;
		setRecord({ ...temp });
	};

	//handleChange type:file
	//use this way because the RDropzone Component return callback (contain data)
	const handleFileChanged = (childData, column) => {
		if (childData) {
			let listDataAdded = [];
			childData.map((item) => {
				listDataAdded.push({
					url: item.url,
					file_name: item.file_name,
					type: item.type,
				});
			});

			let temp = record;
			temp[column] = listDataAdded;
			setRecord({ ...temp });
		}
	};

	//routing to function to set data
	const getFormItemOnChange = (fieldtype, c) => {
		if (fieldtype == TypeEnum.text) {
			return (e) => handleTextChanged(e, c);
		} else if (fieldtype == TypeEnum.select) {
			return (e) => handleSelectValueChanged(e, c);
		} else if (fieldtype == TypeEnum.number) {
			return (e) => handleValueChanged(e, c);
		} else if (fieldtype == TypeEnum.checkbox) {
			return (e) => handleChecked(e, c);
		} else {
			return (e) => handleTextChanged(e, c);
		}
	};

	//to set type in object
	const getFormItemType = (field) => {
		let type = TypeEnum.text;
		if (field.IsDropDownList) {
			type = TypeEnum.select;
		} else if (field.Title?.toLowerCase().includes("color")) {
			type = TypeEnum.color;
		} else if (field?.Type == "file") {
			type = TypeEnum.file;
		} else {
			const fieldType = field?.Type;
			if (fieldType.includes("varchar") || fieldType.includes("text")) {
				type = TypeEnum.text;
			}
			if (fieldType.includes("bigint") || fieldType.includes("int") || fieldType.includes("smallint")) {
				type = TypeEnum.number;
			}
			if (fieldType.includes("time")) {
				type = TypeEnum.time;
			}
			if (fieldType.includes("tinyint")) {
				type = TypeEnum.checkbox;
			}
		}
		return type;
	};

	//call api to set record ...
	useEffect(() => {
		const getData = async () => {
			setLoading(true);
			let Request = record_id
				? {
						payload: {
							TableName: table_name,
							id: record_id,
						},
				  }
				: {
						payload: { TableName: table_name },
				  };

			let response1 = await post("/api/generic/get-table-data", Request);
			//setHasAddPermission(response1.data?.data?.has_add_permission);
			if (response1 && response1.data.status && response1.data && response1.data.data) {
				setLoading(false);
				let data = response1.data.data;
				let init = {};
				if (props.preSetValues) {
					props.preSetValues.map((psv) => {
						init[psv.field] = psv.value;
					});
				}

				setRecord(record_id ? data.records : init);
				setTableProps(data.table_props);
				if (data.hidden_fields && data.hidden_fields.length > 0) setHiddenColumns(data.hidden_fields);
			}
		};
		getData();
	}, [table_name]);

	//initialFormItems
	useEffect(() => {
		let form_data = [];

		tableProps?.data?.map((tp, key) => {
			if (hiddenColumns.findIndex((t) => t == tp.Field) !== -1) return null;
			let t = getFormItemType(tp);
			let formitem = {};
			let selectOptions = getOptionsFromData(tp.data);
			let ss = selectOptions.filter((i) => i.value == record[tp.Field]);
			let selected = record ? ss[0] : null;

			if (t == TypeEnum.select) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					required: false,
					isMulti: false,
					option: selectOptions,
					onChange: getFormItemOnChange(t, tp.Field),
					// defaultValue: tp.description
					value: selected,
					defaultValue: selected,
				};
			} else if (t == TypeEnum.checkbox) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					onChange: getFormItemOnChange(t, tp.Field),
					checked: record ? record[tp.Field] : null,
				};
			} else if (t == TypeEnum.file) {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					handleData: (file) => handleFileChanged(file, tp.Field),
					fileSize: 100,
				};
			} else {
				formitem = {
					id: key,
					type: t,
					name: tp.Field,
					label: tp.Title,
					required: false,
					onChange: getFormItemOnChange(t, tp.Field),
					// defaultValue: tp.description
					defaultValue: record ? record[tp.Field] : null,
				};
			}
			form_data.push(formitem);
		});
		setInitialFormItems(form_data);
	}, [record, tableProps]);

	//action
	const onAddEditSubmit = async (e) => {
		e.preventDefault();

		const payload = {
			payload: {
				TableName: table_name,
				record: record,
			},
		};

		const res = await post(`${process.env.REACT_APP_BACKEND_URL}api/generic/store`, payload);

		if (res && res.data && res.data.status == 1) {
			showAlerts(
				<ReactBSAlert
					success
					title={!record_id ? tr`Added Successfully` : "Edited Successfully"}
					onConfirm={() => {
						history.goBack();
					}}
				></ReactBSAlert>
			);
		} else {
			toast.error(res.data.msg);
		}

		return false;
	};
	return (
		<>
			{loading ? (
				<Loader />
			) : (
				<>
					{alerts}

					<RAddEdit
						initialFormItems={initialformItems}
						onSubmit={(e) => {
							onAddEditSubmit(e);
							if (props.onSubmit) props.onSubmit();
						}}
						oldObject={!(props && props.preSetValues) && record_id && initialformItems}
						allowEdit={record?.has_edit_permission}
						preSetValues={props && props.preSetValues}
					/>
				</>
			)}
		</>
	);
};

export default GenericUIAddEdit;

// const hideAlert = () => {
//   setAlerts(null);
// };

// const renderColumn = (column) => {
//   if (hiddenColumns.findIndex((t) => t == column.Field) !== -1) return null;
//   else {
//     if (column.IsDropDownList) {
//       return (
//         <div className="row alineCenter">
//           <div className="col-md-4 flex-endAline">
//             <label>{column.Title + " : "}</label>
//           </div>
//           <div className="col-md-8 flex-startAline">
//             <select
//               className="form-control"
//               id={column.Field}
//               value={record ? record[column.Field] : null}
//               onChange={(e) => {
//                 handleTextChanged(e, column);
//               }}
//             >
//               {column.data.map((item) => {
//                 return (
//                   <option key={item.ID} value={item.ID}>
//                     {item.name}
//                   </option>
//                 );
//               })}
//             </select>
//           </div>
//         </div>
//       );
//     }
//     switch (column.Type) {
//       case "varchar(255)":
//       case "varchar(2000)":
//       case "varchar(191)":
//       case "varchar(5)":
//       case "text":
//         if (column.Field == "color") {
//           return (
//             <div className="row alineCenter">
//               <div className="col-md-4 flex-endAline">
//                 <label className="col-6">{column.Title + " : "}</label>
//               </div>
//               <div className="col-md-8 flex-startAline">
//                 <input
//                   id={column.Field}
//                   key={column.Field}
//                   className="from-control"
//                   type="color"
//                   value={record ? record[column.Field] : null}
//                   onChange={(e) => {
//                     handleTextChanged(e, column);
//                   }}
//                 />
//               </div>
//             </div>
//           );
//         } else {
//           return (
//             <div className="row alineCenter">
//               <div className="col-md-4 flex-endAline">
//                 <label className="col-6">{column.Title + " : "}</label>
//               </div>
//               <div className="col-md-8 flex-startAline">
//                 <input
//                   id={column.Field}
//                   key={column.Field}
//                   className="form-control"
//                   type="text"
//                   value={record ? record[column.Field] : null}
//                   onChange={(e) => {
//                     handleTextChanged(e, column);
//                   }}
//                 />
//               </div>
//             </div>
//           );
//         }
//         break;
//       case "bigint(20) unsigned":
//       case "int(11)":
//       case "smallint(6)":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="number"
//                 value={record ? record[column.Field] : null}
//                 onChange={(e) => {
//                   handleTextChanged(e, column);
//                 }}
//                 min={0}
//               />
//             </div>
//           </div>
//         );
//         break;
//       case "time":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="time"
//                 value={record ? record[column.Field] : null}
//                 onChange={(e) => {
//                   handleTextChanged(e, column);
//                 }}
//               />
//             </div>
//           </div>
//         );
//         break;
//       case "tinyint(1)":
//       case "tinyint(4)":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="checkbox"
//                 checked={record ? record[column.Field] : false}
//                 onChange={(e) => {
//                   handleChecked(e, column);
//                 }}
//               />
//             </div>
//           </div>
//         );
//     }
//   }
// };

// const getFormItem = (column) => {
//   if (hiddenColumns.findIndex((t) => t == column.Field) !== -1) return null;
//   else {
//     if (column.IsDropDownList) {
//       return (
//         <div className="row alineCenter">
//           <div className="col-md-4 flex-endAline">
//             <label>{column.Title + " : "}</label>
//           </div>
//           <div className="col-md-8 flex-startAline">
//             <select
//               className="form-control"
//               id={column.Field}
//               value={record ? record[column.Field] : null}
//               onChange={(e) => {
//                 handleTextChanged(e, column);
//               }}
//             >
//               {column.data.map((item) => {
//                 return (
//                   <option key={item.ID} value={item.ID}>
//                     {item.name}
//                   </option>
//                 );
//               })}
//             </select>
//           </div>
//         </div>
//       );
//     }
//     switch (column.Type) {
//       case "varchar(255)":
//       case "varchar(2000)":
//       case "varchar(191)":
//       case "varchar(5)":
//       case "text":
//         if (column.Field == "color") {
//           return (
//             <div className="row alineCenter">
//               <div className="col-md-4 flex-endAline">
//                 <label className="col-6">{column.Title + " : "}</label>
//               </div>
//               <div className="col-md-8 flex-startAline">
//                 <input
//                   id={column.Field}
//                   key={column.Field}
//                   className="from-control"
//                   type="color"
//                   value={record ? record[column.Field] : null}
//                   onChange={(e) => {
//                     handleTextChanged(e, column);
//                   }}
//                 />
//               </div>
//             </div>
//           );
//         } else {
//           return (
//             <div className="row alineCenter">
//               <div className="col-md-4 flex-endAline">
//                 <label className="col-6">{column.Title + " : "}</label>
//               </div>
//               <div className="col-md-8 flex-startAline">
//                 <input
//                   id={column.Field}
//                   key={column.Field}
//                   className="form-control"
//                   type="text"
//                   value={record ? record[column.Field] : null}
//                   onChange={(e) => {
//                     handleTextChanged(e, column);
//                   }}
//                 />
//               </div>
//             </div>
//           );
//         }
//         break;
//       case "bigint(20) unsigned":
//       case "int(11)":
//       case "smallint(6)":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="number"
//                 value={record ? record[column.Field] : null}
//                 onChange={(e) => {
//                   handleTextChanged(e, column);
//                 }}
//                 min={0}
//               />
//             </div>
//           </div>
//         );
//         break;
//       case "time":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="time"
//                 value={record ? record[column.Field] : null}
//                 onChange={(e) => {
//                   handleTextChanged(e, column);
//                 }}
//               />
//             </div>
//           </div>
//         );
//         break;
//       case "tinyint(1)":
//       case "tinyint(4)":
//         return (
//           <div className="row alineCenter">
//             <div className="col-md-4 flex-endAline">
//               <label className="col-6">{column.Title + " : "}</label>
//             </div>
//             <div className="col-md-8 flex-startAline">
//               <input
//                 id={column.Field}
//                 key={column.Field}
//                 className="form-control"
//                 type="checkbox"
//                 checked={record ? record[column.Field] : false}
//                 onChange={(e) => {
//                   handleChecked(e, column);
//                 }}
//               />
//             </div>
//           </div>
//         );
//     }
//   }
// };

// const handleSave = () => {
//   let request = {
//     payload: {
//       TableName: table_name,
//       record: record,
//     },
//   };
//   axios
//     .post(process.env.REACT_APP_BACKEND_URL + "api/generic/store", request)
//     .then((response) => {
//       if (response.data.status == 1) {
//         getData(null);
//         setAddMode(false);
//       } else {
//       }
//     })
//     .catch((error) => {
//     });
// };
// const createRecord = () => {
//   let temp = {};
//   for (let i = 0; i < table_props.length; i++) {
//     temp[table_props[i].Field] = null;
//   }
//   return temp;
// };
