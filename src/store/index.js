import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

// import { createLogger } from 'redux-logger'
import rootReducer from "./reducers";

const persistConfig = {
	key: "root",
	storage,
	whitelist: ["auth"], //, "lessonPlan"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Middleware and store enhancers
const enhancers = [applyMiddleware(thunk)];
window.__REDUX_DEVTOOLS_EXTENSION__ && enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
// const store = createStore(rootReducer, compose(...enhancers));

let store = createStore(persistedReducer, compose(...enhancers));

export let persistor = persistStore(store);

export default store;
