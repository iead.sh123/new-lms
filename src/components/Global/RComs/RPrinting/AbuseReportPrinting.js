import React from "react";
import { Row, Col } from "reactstrap";
import styles from "./RPrinting.Module.scss";
import tr from "../RTranslator";

const AbuseReportPrinting = ({ data }) => {
  return (
    <Row style={{ marginTop: 20 }}>
      <Col xs={12}>
        <space>
          <h6 className={styles.colorTitle}>{tr`abused_children`} : &nbsp;</h6>
          <ul>
            {data?.students_info?.map((el) => (
              <li>{el.label}</li>
            ))}
          </ul>
        </space>
      </Col>
      <Col xs={12}>
        <space>
          <h6 className={styles.colorTitle}>{tr`abuses`} : &nbsp;</h6>
          <ul>
            {data?.users_info?.map((el) => (
              <li>{el.label}</li>
            ))}
          </ul>
        </space>
      </Col>
    </Row>
  );
};

export default AbuseReportPrinting;
