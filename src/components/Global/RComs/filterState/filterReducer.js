import { produce, current } from "immer";
import {
  ADD_NEW_FILTER,
  DELETE_NEW_FILTER,
  ADD_VALUES_TO_THE_FILTER,
  ADD_NAME_TO_THE_FILTER,
  RESET_FILTERS,
} from "store/actions/admin/adminType";
import Helper from "../Helper";

export const filterState = {
  newFilter: [],
};

let ind;
const filterReducer = (state = filterState, action) => {
  return produce(state, (draft) => {
    switch (action?.type) {
      case ADD_NEW_FILTER:
        draft.newFilter.push(action.data);
        return draft;

      case DELETE_NEW_FILTER:
        ind = draft.newFilter.findIndex((el) => el.idTemp == action.id);

        if (ind !== -1) draft.newFilter.splice(ind, 1);
        return draft;

      case ADD_VALUES_TO_THE_FILTER:
        ind = draft.newFilter.findIndex((el) => el.idTemp == action.id);
        if (ind !== -1) {
          const items = draft.newFilter[ind];
          items.value = action.inputValue.value;
        }
        return draft;

      case ADD_NAME_TO_THE_FILTER:
        ind = draft.newFilter.findIndex((el) => el.idTemp == action.id);
        if (ind !== -1) {
          const items = draft.newFilter[ind];
          items.field = action.nameFilter.field;
          items.type = action.nameFilter.type;
          items.index = action.nameFilter.index;
          items.operation = action.nameFilter.operation;
        }
        return draft;

      case RESET_FILTERS:
        draft.newFilter = [];
        return draft;

      default:
        return state;
    }
  });
};
export default filterReducer;
