import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLister from "components/Global/RComs/RLister";
import React from "react";
import { permissionsHeader } from "../constants";
import styles from "./sharedStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
const RReviewPermissions = ({ savedPermissions }) => {
	const records = savedPermissions.map((permission, index) => {
		return {
			details: permissionsHeader.map((header) => {
				const privilegeValueComponent = permission.privileged == 1 ? <div className={styles.priviligeStyle}>{tr("Privilege")}</div> : "";
				return {
					key: header.key,
					value: header.name == "privilege" ? privilegeValueComponent : permission[header.value],
					type: header.name == "privilege" ? "component" : "",
				};
			}),
		};
	});
	return (
		<RFlex className="flex-column">
			{records.length > 0 ? (
				<RLister Records={records} activeScroll={true} />
			) : (
				<RFlex className="flex-column">
					<span style={{ fontWeight: "bold" }}>{tr("Permisssions")}</span>
					<span>{tr("No_permessions_yet")}</span>
				</RFlex>
			)}
		</RFlex>
	);
};

export default RReviewPermissions;
