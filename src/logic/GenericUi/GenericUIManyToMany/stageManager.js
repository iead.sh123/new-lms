export const STAGE1 = "STAGE1";
export const STAGE2 = "STAGE2";
export const STAGE3 = "STAGE3";
import Stage1 from "./Stages/Stage1";
import Stage2 from "./Stages/Stage2";
import Stage3 from "./Stages/Stage3";

const stageLiterals = {
  [STAGE1]: Stage1,
  [STAGE2]: Stage2,
  [STAGE3]: Stage3,
};

export const getStageComponent = (stage) => {
  return stageLiterals[stage];
};
