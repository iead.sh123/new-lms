import RLister from 'components/Global/RComs/RLister'
import React, { useState } from 'react'
import tr from 'components/Global/RComs/RTranslator'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import { scaleTypes } from './constants/index'
import { SET_SCALE_INPUT_SAVED, SET_SCLAE_INPUT_TOUCHED } from 'store/actions/global/globalTypes'
import { CHANGE_SCALE_INPUT_VALUE } from 'store/actions/global/globalTypes'
import Loader from 'utils/Loader'
const GGradeScale = () => {
  const dispatch = useDispatch()
  const [enterPressed, setEnterPressed] = useState(false)
  const { gradeScale } = useSelector((state) => state.schoolManagementRed)

  const handleEditFunctionality = (event, scaleDetail, { type }) => {
    setEnterPressed(false)
    dispatch({ type: SET_SCALE_INPUT_SAVED, payload: { id: scaleDetail.id, type } })
  }

  const gradeScaleisValidate = (scaleDetail) => {
    if (!scaleDetail.letter || !scaleDetail.min || !scaleDetail.max || !scaleDetail.GPA) {
      return false
    }
    return true
  }
  const handleInputDown = async (event, scaleDetail, { type }) => {
    if (event.key == "Enter") {
      setEnterPressed(true)
      let success = false
      dispatch({ type: SET_SCLAE_INPUT_TOUCHED, payload: { id: scaleDetail.id, type } })
      if (scaleDetail[type] == "") {
        return
      }
      // if (gradeScaleisValidate(scaleDetail))
      success = await dispatch(actions.editScaleValue({ id: scaleDetail.id, letter: scaleDetail.letter, min: scaleDetail.min, max: scaleDetail.max, GPA: scaleDetail.GPA }))
      if (success) {
        dispatch({ type: SET_SCALE_INPUT_SAVED, payload: { id: scaleDetail.id, type } })

      }
    }
  }
  const handleOnBlur = async (event, scaleDetail, { type }) => {
    if (enterPressed) {
      return
    }
    let success = false
    dispatch({ type: SET_SCLAE_INPUT_TOUCHED, payload: { id: scaleDetail.id, type } })
    if (scaleDetail[type] == "") {
      return
    }

    success = await dispatch(actions.editScaleValue({ id: scaleDetail.id, letter: scaleDetail.letter, min: scaleDetail.min, max: scaleDetail.max, GPA: scaleDetail.GPA }))
    if (success) {
      dispatch({ type: SET_SCALE_INPUT_SAVED, payload: { id: scaleDetail.id, type } })

    }
  }
  const handleInputChange = (event, scaleDetail, { type }) => {
    dispatch({ type: CHANGE_SCALE_INPUT_VALUE, payload: { id: scaleDetail.id, type, value: event.target.value } })
  }



  const _records = !gradeScale.loading ? gradeScale.ids?.map((key) => {
    const scaleDetail = gradeScale[key]
    const minMaxComponent = (
      <RFlex styleProps={{ gap: '3px' }} className="align-items-center">
        <RHoverInput
          inputValue={scaleDetail.min}
          handleInputDown={handleInputDown}
          inputWidth='60px'
          type='number'
          inputPlaceHolder=""
          textCenter={true}
          handleInputChange={handleInputChange}
          inputIsNotValidate={!scaleDetail['min'] && scaleDetail['touched']?.min}
          item={scaleDetail}
          handleOnBlur={handleOnBlur}
          handleEditFunctionality={handleEditFunctionality}
          extraInfo={{ type: scaleTypes.Min }}
          saved={scaleDetail?.saved?.['min']}
        />
        <span className='p-0 m-0 font-weight-bold'>-</span>
        <RHoverInput
          inputValue={scaleDetail.max}
          handleInputDown={handleInputDown}
          inputWidth='60px'
          type='number'
          inputPlaceHolder=""
          textCenter={true}
          handleInputChange={handleInputChange}
          inputIsNotValidate={!scaleDetail['max'] && scaleDetail['touched']?.max}
          item={scaleDetail}
          handleOnBlur={handleOnBlur}
          handleEditFunctionality={handleEditFunctionality}
          extraInfo={{ type: scaleTypes.Max }}
          saved={scaleDetail?.saved?.['max']}
        />
      </RFlex>
    )
    return {
      details: [
        {
          key: tr`letter_grade`,
          value: <RHoverInput
            inputValue={scaleDetail.letter}
            handleInputDown={handleInputDown}
            inputWidth='60px'
            inputPlaceHolder=""
            textCenter={true}
            handleInputChange={handleInputChange}
            inputIsNotValidate={!scaleDetail['letter'] && scaleDetail['touched']?.letter}
            item={scaleDetail}
            handleOnBlur={handleOnBlur}
            handleEditFunctionality={handleEditFunctionality}
            extraInfo={{ type: scaleTypes.Letter }}
            saved={scaleDetail?.saved?.['letter']}
          /> ?? "-",
          type: 'component'
        },
        { key: tr`percent_grade`, value: minMaxComponent, type: 'component' },
        {
          key: tr`4.0_scale`,
          value: < RHoverInput
            inputValue={scaleDetail.GPA}
            item={scaleDetail}
            type='number'
            inputPlaceHolder=""
            textCenter={true}
            handleInputChange={handleInputChange}
            handleInputDown={handleInputDown}
            handleOnBlur={handleOnBlur}
            inputIsNotValidate={!scaleDetail['GPA'] && scaleDetail['touched']?.GPA}
            handleEditFunctionality={handleEditFunctionality}
            extraInfo={{ type: scaleTypes.GPA }}
            inputWidth='60px'
            saved={scaleDetail.saved?.['GPA']}
          /> ?? "-",
          type: 'component'
        },
      ],
    }
  }) : ""
  return (
    gradeScale.loading ? <Loader /> :
      <RFlex className='flex-column' styleProps={{ gap: '2px', width: '100%' }}>
        <span className='font-weight-bold'>{tr("grade_scale")}</span>
        <RLister center={true} Records={_records} activeScroll={true} />
      </RFlex>
  )
}

export default GGradeScale