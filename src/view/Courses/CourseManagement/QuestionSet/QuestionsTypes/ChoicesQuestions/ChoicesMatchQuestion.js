import React, { useContext } from "react";
import { Row, FormGroup, Input, Col } from "reactstrap";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { dangerColor } from "config/constants";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const ChoicesMatchQuestion = ({ questionGroupId, question, questionType, choices, questionBank }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const QuestionsBankData = useContext(QuestionsBankContext);

	const QS = questionBank ? QuestionsBankData : QuestionSetData;
	return (
		<>
			<Row>
				<Col xs={12} sm={6}>
					{choices.map((choice, index) =>
						choice.order % 2 !== 0 ? (
							// <FormGroup className="d-flex align-items-center justify-content-between" style={{ height: "100px" }}>
							<FormGroup className="d-flex align-items-center justify-content-between">
								{/* <i className="fa-solid fa-grip-vertical pl-0 pr-2" /> */}
								{String.fromCharCode(65 + (index + 1 - 1) / 2)}
								<Input
									style={{ margin: "0px 20px" }}
									name="text"
									type="text"
									placeholder={tr`text`}
									value={choice?.text}
									onChange={(event) => {
										QS.handleFillDataInChoicesAnswers({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											choiceId: choice?.id ? choice?.id : choice?.fakeId,
											key: event.target.name,
											value: event.target.value,
											questionType: questionType,
										});
									}}
								/>
								<RFileSuite
									parentCallback={(values) => {
										QS.handleFillDataInChoicesAnswers({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											choiceId: choice?.id ? choice?.id : choice?.fakeId,
											key: "files",
											value: values,
											questionType: questionType,
										});
									}}
									singleFile={true}
									binary={true}
									value={choice?.files}
									theme="button_with_replace"
									fileType="image/*"
								/>
							</FormGroup>
						) : null
					)}
				</Col>

				<Col xs={12} sm={6}>
					{choices.map((choice) =>
						choice.order % 2 == 0 ? (
							// <FormGroup className="d-flex align-items-center justify-content-between" style={{ height: "100px" }}>
							<FormGroup className="d-flex align-items-center justify-content-between">
								<Input
									style={{ margin: "0px 20px" }}
									name="text"
									type="text"
									placeholder={tr`text`}
									value={choice?.text}
									onChange={(event) => {
										QS.handleFillDataInChoicesAnswers({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											choiceId: choice?.id ? choice?.id : choice?.fakeId,
											key: event.target.name,
											value: event.target.value,
											questionType: questionType,
										});
									}}
								/>
								<RFlex
									styleProps={{
										alignItems: "center",
										justifyContent: "end",
									}}
								>
									<RFileSuite
										parentCallback={(values) => {
											QS.handleFillDataInChoicesAnswers({
												questionGroupId: questionGroupId,
												questionTypeId: question?.id ? question?.id : question?.fakeId,
												choiceId: choice?.id ? choice?.id : choice?.fakeId,
												key: "files",
												value: values,
												questionType: questionType,
											});
										}}
										singleFile={true}
										binary={true}
										value={choice?.files}
										theme="button_with_replace"
										fileType="image/*"
									/>
									<i
										className={iconsFa6.delete}
										style={{ color: dangerColor, cursor: "pointer" }}
										onClick={() =>
											QS.handleRemoveChoiceFromQuestion({
												questionGroupId: questionGroupId,
												questionTypeId: question?.id ? question?.id : question?.fakeId,
												choiceId: choice?.id ? choice?.id : choice?.fakeId,
												questionType: questionType,
												choiceOrder: choice.order,
											})
										}
									/>
								</RFlex>
							</FormGroup>
						) : null
					)}
				</Col>
			</Row>
		</>
	);
};

export default ChoicesMatchQuestion;
