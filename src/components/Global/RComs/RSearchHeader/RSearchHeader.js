import React from "react";
import useWindowDimensions from "components/Global/useWindowDimensions";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "./RSearchHeader.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Form, FormGroup, Input } from "reactstrap";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";

const RSearchHeader = ({
	searchLoading,
	searchData,
	handleSearch,
	setSearchData,
	handleOpenModal,
	handleChangeSearch,
	buttonName,
	inputPlaceholder,
	addNew,
	searchRadioButton = [],
	handleSearchOnRadioButton,
	handlePushToAnotherRouteWhenAdd,
	inputWidth,
	outline,
	disabledOnInput,
	searchInputStyle,
	catalog = false,
	alignItems,
	widthInput,
	margin = null
}) => {
	const { width } = useWindowDimensions();
	const mobile = width < 1000;
	return (
		<RFlex
			styleProps={{
				width: inputWidth ? (mobile ? "100%" : inputWidth) : "100%",
				alignItems: alignItems ? alignItems : "baseline",
				flexColumn: mobile ? "column" : "row",
				gap: 30,
			}}
		>
			{addNew && (
				<RButton
					color="primary"
					text={buttonName}
					faicon={iconsFa6.plus}
					onClick={() => {
						handlePushToAnotherRouteWhenAdd ? handlePushToAnotherRouteWhenAdd() : handleOpenModal();
					}}
					outline={outline == false ? outline : true}
				/>
			)}
			<Form
				// style={{ width: widthInput ? widthInput : "300px" }}
				className={styles.form_input}
				onSubmit={(event) => {
					event.preventDefault();
				}}
			>
				<FormGroup style={{ width: widthInput ? widthInput : "300px", margin: margin }}>
					<Input
						type="text"
						placeholder={inputPlaceholder ?? tr`search`}
						// className={styles.search_input}
						style={searchInputStyle}
						value={searchData}
						onChange={(event) => {
							event.preventDefault();
							handleChangeSearch(event.target.value);
						}}
						onKeyDown={(event) => {
							if (event.key === "Enter" && handleSearch) {
								handleSearch();
							}
						}}
						disabled={disabledOnInput ? disabledOnInput : false}
					/>

					<i
						aria-hidden="true"
						className={searchLoading ? `${iconsFa6.spinner + " " + styles.search_input_icon} ` : `fa fa-search ${styles.search_input_icon}`}
						onClick={() => handleSearch()}
					/>

					{searchData == "" ? (
						""
					) : (
						<i
							aria-hidden="true"
							className={`fa fa-close ${styles.clear_input_icon}`}
							onClick={() => {
								if (searchData !== "") {
									setSearchData && setSearchData("");
									handleSearch("");
								}
							}}
						/>
					)}
				</FormGroup>
			</Form>
			{/* <div style={{ flexGrow: "true" }}>
				{searchRadioButton && searchRadioButton?.length == 1 ? (
					<>
						{searchRadioButton &&
							searchRadioButton.map((radioButton) => (
								<RButton
									name="checkbox22"
									title={radioButton.name}
									style={radioButton.style}
									selectedStyle={radioButton.selectedStyle}
									checked={radioButton.categoryId ? true : false}
									// disabled={radioButton.id == 1 ? true : false}
									onClick={() => handleSearchOnRadioButton(radioButton.id)}
								/>
							))}
					</>
				) : (
					searchRadioButton &&
					searchRadioButton.map((radioButton) => (
						<AppRadioButton
							name="radio"
							label={radioButton.name}
							style={radioButton.style}
							selectedStyle={radioButton.selectedStyle}
							checked={radioButton.categoryId ? true : false}
							// disabled={radioButton.id == 1 ? true : false}
							onClick={() => handleSearchOnRadioButton(radioButton.id)}
						/>
					))
				)}
			</div> */}
		</RFlex>
	);
};

export default RSearchHeader;
