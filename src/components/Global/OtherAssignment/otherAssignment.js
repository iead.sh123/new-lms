import React from "react";
import { otherUseAssignment } from "./otherUseAssignment";
import Loader, { ELLIPSIS } from "utils/Loader";
import { Row, Col, Card, CardBody, Button } from "reactstrap";
import OtherQuestionsTree from "./otherQuestionTree";
import OtherAssignmentQuestionNavigation from "./otherAssignmentQuestionNavigation";
import Groups from "../Assignment/Groups";
import Topics from "../Assignment/Topics";
import OtherQuestion from "./otherQuestion";

const OtherAssignment = ({ mode, rabId }) => {
  const other_assignment = otherUseAssignment(mode, rabId);

  const {
    lessonGroups,
    isLoader,
    sebBrowserViolation,
    dispatch,
    otherAssignmentState,
  } = other_assignment;

  const {
    nextButtonDisabled,
    previousButtonDisabled,
    currentActiveGroup,
    currentActiveTopic,
    currentActiveQuestion,
    StudentQsQuestionId,
    StudentQsPatternId,
    TotalMark,
    // assignmentReadMode,
    questionsMarks,
  } = otherAssignmentState;

  const containerStyle = {
    paddingRight: "15px",
    paddingLeft: "0px",
  };

  if (mode == "teacher") containerStyle.paddingTop = "66px";

  //used for show submit or close button when click to last question
  const isLastQuestion = () => {
    return (
      currentActiveGroup === lessonGroups?.inst_pattern_groups.length - 1 &&
      currentActiveTopic ===
        lessonGroups?.inst_pattern_groups[
          lessonGroups?.inst_pattern_groups.length - 1
        ].inst_topics.length -
          1 &&
      currentActiveQuestion ===
        lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
          currentActiveTopic
        ].inst_questions.length -
          1
    );
  };

  const saveAssignmentDataHandler = () => {};

  const saveTeacherMarksHandler = () => {};

  return isLoader ? (
    <>{Loader(ELLIPSIS)}</>
  ) : (
    <Row>
      <Col lg={3} sm={5}>
        <OtherQuestionsTree
          mode={mode}
          lessonGroups={lessonGroups}
          // assignmentReadMode={assignmentReadMode}
          dispatch={dispatch}
          currentActiveTopic={currentActiveTopic}
          currentActiveGroup={currentActiveGroup}
          currentActiveQuestion={currentActiveQuestion}
          currentQuestion={
            lessonGroups &&
            lessonGroups?.inst_pattern_groups &&
            lessonGroups?.inst_pattern_groups[currentActiveGroup]?.inst_topics[
              currentActiveTopic
            ]?.inst_questions[currentActiveQuestion]
          }
          studentQsQuestionId={StudentQsQuestionId}
        />
      </Col>
      <Col className=" p-1" lg={9} sm={7}>
        <Card>
          <CardBody>
            <Row>
              <Col
                xs={12}
                style={{ borderBottom: "solid 1px rgba(99, 95, 89, 0.4)" }}
              >
                {/* {currentActiveGroup} - {currentActiveTopic} -{" "}
                {currentActiveQuestion} - {StudentQsPatternId} -{" "}
                {StudentQsQuestionId} */}
                {lessonGroups && lessonGroups?.inst_pattern_groups && (
                  <Col xs={12} style={{ zIndex: 2 }}>
                    <Groups
                      group={
                        lessonGroups?.inst_pattern_groups[currentActiveGroup]
                      }
                      currentActiveGroup={currentActiveGroup}
                      lessonGroups={lessonGroups}
                    />
                  </Col>
                )}
                {lessonGroups && lessonGroups?.inst_pattern_groups && (
                  <Col xs={12} style={{ zIndex: 2 }}>
                    <Topics
                      topic={
                        lessonGroups?.inst_pattern_groups[currentActiveGroup]
                          .inst_topics[currentActiveTopic]
                      }
                      currentActiveTopic={currentActiveTopic}
                      groupLength={
                        lessonGroups?.inst_pattern_groups[currentActiveGroup]
                          .inst_topics.length
                      }
                    />
                  </Col>
                )}
              </Col>
            </Row>
            <OtherAssignmentQuestionNavigation
              nextButtonDisabled={nextButtonDisabled}
              previousButtonDisabled={previousButtonDisabled}
              mode={mode}
              // assignmentReadMode={assignmentReadMode}
              currentActiveGroup={currentActiveGroup}
              currentActiveQuestion={currentActiveQuestion}
              currentActiveTopic={currentActiveTopic}
              lessonGroups={lessonGroups}
              dispatch={dispatch}
              saveAssignmentDataHandler={saveAssignmentDataHandler}
              saveTeacherMarksHandler={saveTeacherMarksHandler}
              isLastQuestion={isLastQuestion}
              studentQsQuestionId={StudentQsQuestionId}
            />
            <OtherQuestion
              assignmentState={otherAssignmentState}
              assignmentReadMode={true}
              groupId={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups[currentActiveGroup].id
              }
              dispatch={dispatch}
              questionsMarks={questionsMarks}
              currentActiveQuestion={currentActiveQuestion}
              mode={mode}
              question={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups[currentActiveGroup]
                  ?.inst_topics[currentActiveTopic]?.inst_questions[
                  currentActiveQuestion
                ]
              }
              questionLength={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups[currentActiveGroup]
                  ?.inst_topics[currentActiveTopic]?.inst_questions?.length
              }
              topiccLength={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups[currentActiveGroup]
                  ?.inst_topics.length
              }
              topicLength={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups[currentActiveGroup]
                  ?.inst_topics[currentActiveTopic]?.inst_questions.length
              }
              patternLength={
                lessonGroups &&
                lessonGroups?.inst_pattern_groups &&
                lessonGroups?.inst_pattern_groups.length
              }
              rabId={rabId}
              studentQsPatternId={StudentQsPatternId}
              studentQsQuestionId={StudentQsQuestionId}
              TotalMark={TotalMark}
              currentActiveTopic={currentActiveTopic}
              currentActiveGroup={currentActiveGroup}
            />
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default OtherAssignment;
// AssignmentBody;
