import React, { useRef, useState, useEffect } from "react";
import CanvasDraw from "react-canvas-draw";
import useWindowDimensions from "components/Global/useWindowDimensions";
import DrawTools from "./DrawTools";
import lzString from "lz-string";
import amly from "assets/img/amly_logo_t.png";
import { Services } from "engine/services";

const DrawPlace = ({
  QuestionSetSolveData,
  questionGroupId,
  groupContextId,
  questionContextId,
  type,
  hashId,
  readMode,
}) => {
  const { width } = useWindowDimensions();

  const mdScreen = width < 750 && width > 600;
  const isSmallScreen = width < 600;

  const canvasRef = useRef();
  const [savedCanvas, setSavedCanvas] = useState("");
  const [brushColor, setBrushColor] = useState("");
  const [brushSize, setBrushSize] = useState(6);

  return (
    <React.Fragment>
      {readMode && (
        <DrawTools
          canvasRef={canvasRef}
          brushColor={brushColor}
          brushSize={brushSize}
          setBrushColor={setBrushColor}
          setBrushSize={setBrushSize}
        />
      )}
      <div
        onClick={() =>
          QuestionSetSolveData.handleFillDataToSolveQuestion({
            questionGroupId: questionGroupId,
            groupContextId: groupContextId,
            questionContextId: questionContextId,
            type: type,
            key: "answer",
            value: lzString.compressToUTF16(
              canvasRef && canvasRef.current && canvasRef.current.getSaveData()
            ),
            isCorrect: true,
          })
        }
      >
        <CanvasDraw
          lang={"ar"}
          ref={(canvasDraw) => (canvasRef.current = canvasDraw)}
          canvasWidth={mdScreen ? 500 : isSmallScreen ? 300 : width - 400}
          canvasHeight={mdScreen ? 500 : isSmallScreen ? 300 : width - 400}
          brushColor={brushColor}
          saveData={savedCanvas}
          brushRadius={brushSize}
          // enablePanAndZoom
          imgSrc={hashId !== undefined ? Services.storage.file + hashId : null}
          disabled={!readMode}
        />
      </div>
    </React.Fragment>
  );
};

export default DrawPlace;
