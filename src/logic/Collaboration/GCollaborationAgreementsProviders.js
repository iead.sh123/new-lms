import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React, { useEffect, useState } from "react";
import moment from "moment";
import { AgreementViewer } from "./AgreementViewer";
import RRow from "view/RComs/Containers/RRow";
import RLiveAction from "components/Global/RComs/Collaboration/RLiveAction";
import tr from "components/Global/RComs/RTranslator";
import produce from "immer";
export const GCollaborationAgreementsProviders = ({setProvidersCount}) => {
  const [records, setRecords] = useState([]);
  const [data1,setData1]=useState([]);
  const [refreshRecords,setRefreshRecords]=useState(true);
  const getDataFromBackend = async (specific_url) => {
    const url =
      Services.collaboration.backend + `api/collaboration-agreement/providers`;
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };
  const [agreementIdToView,setAgreementIdToView]=useState();
  const view = (agreement_id) => {
    setAgreementIdToView(agreement_id);
  }; 
  const approveAgreement = (id) => {

    Helper.fastPost(Services.collaboration.backend+`api/collaboration-agreement/approve/${id}`,{},"Approved successfully","error while approving",
    ()=>{   
      
      setData1(
      produce(records,
        (tempRecords=>{
          const sr=tempRecords.filter(r=>r.id==id);
          if(sr&&sr.length)sr[0].approved_by_destination=true;
        }
        )
        )
      )
    
      setRefreshRecords(!refreshRecords);
    }


    
    ,()=>{Helper.cl("fail")})
  
  };
  const deleteAgreement = (id) => {

    Helper.fastPost(Services.collaboration.backend+`api/collaboration-agreement/remove/${id}`,{},"Deleted successfully","error while deleting",
    ()=>{
      setData1(
        produce(records,
          (tempRecords=>{
            const sr=tempRecords.filter(r=>r.id==id);
            if(sr&&sr.length)sr[0].approved_by_destination=true;
          }
          )
          )
        )
        setRefreshRecords(!refreshRecords);
    }


    
    
    ,()=>{Helper.cl("fail")})
  
  };
  const setData = (response) => {
    setData1(response?.data?.data);
    }

    useEffect(() => {
      setRecords(
       data1.map((r, index) => {
          return {
            specialProps: {
              title: { text: r.details?.length + "Content types" },
              // subtitle : { text: 'Unit Plan'},
              // Author:{image:'tea.png', text: 'Canvas'},
              // onClick:()=>{},
              // UpLeftIcon:"fa fa-angle-right",
              UpLeftIcon: "All:86",
              Actions: [
                {
                  title: "View",
                  icon: "	fa fa-eye",
                  color: "gray",
                  onClick:()=>{view(r.id);},
                  withBorder: true,
                },
                {
                  title: "Approve",
                  icon: "fa fa-check",
                  color: "green",
                  onClick: ()=>{approveAgreement(r.id)},
                  withBorder: true,
                  disabled:r.approved_by_destination,
                  disabledNote:"It's aleady approved"
                },
                // {
                //   title: "Approve",
                //   icon: "fa fa-check",
                //   color: "green",
                //   onClick: "",
                //   withBorder: true,
                // },
                // {
                //   title: "Reject",
                //   icon: "fa fa-close",
                //   color: "red",
                //   onClick: "",
                //   withBorder: true,
                // },
              ],
              approveStatus: {
                title: (!r.approved_by_destination)?"not approved yet": r.active?"Activated":"Deactivated" ,
                icon: (!r.approved_by_destination)?"fa fa-history":r.active  ? "fa fa-power-off" : "fa fa-power-off",
                color: r.active||(r.approved_by_destination)  ? "green" : "red",
              },
            
             
              // approveStatus: {
              //   icon: r.approved_by_destination
              //     ? "fa fa-power-off"
              //     : "fa fa-history",
              //   title: r.approved_by_destination
              //     ? "approved by destination"
              //     : "not approved yet",
              //   color: r.approved_by_destination ? "green" : "blue",
              // },
              bottomBorderColor: "",
              Author: {
                text: r?.source_organization?.name,
                image: r?.source_organization?.image,
              },
               Description:"date:"+moment(r.updated_at).format("YYYY-MM-DD HH:mm A") ,
              menuActions: [
                // {
                //   title: "View",
                //   icon: "	fa fa-eye",
                //   color: "gray",
                //   onClick: "",
                //   withBorder: true,
                // },
                {
                  title: "Approve",
                  icon: "fa fa-check",
                  color: "green",
                  onClick: ()=>{approveAgreement(r.id)},
                  withBorder: true,
                  disabled:r.approved_by_destination,
                  disabledNote:"It's aleady approved"
                },
                {
                  title: "Delete",
                  icon: "fa fa-remove",
                  color: "red",
                  onClick:  ()=>{deleteAgreement(r.id)},
                  withBorder: true,
                },
              ],
            },
  
            // active:1
            // approved_by_destination:0
            // created_at:"2023-07-20T06:27:28.000000Z"
            // destination_org_id:8
            // id:12
            // source_org_id:1
          };
        })
      );
  
      setProvidersCount(data1.length)
    },[data1]);
    //return ;
   
  return (<div style={{height:"50vh"}}>
    {(agreementIdToView&&agreementIdToView>0)?
    <><RRow>
    <RLiveAction 
      title=""
      icon="fas fa-arrow-left"
      color="black"
      text=""
      // color="red"
      loading={false}
      type="button"
      onClick={()=>{setAgreementIdToView(null)}}
      
    />
  </RRow>
    <AgreementViewer agreement_id={agreementIdToView}/></>:
    <RAdvancedLister
      hideTableHeader={false}
      colorEveryOtherRow={false}
      //firstCellImageProperty={"image"}
      getDataFromBackend={getDataFromBackend}
      setData={setData}
      records={records}
      getDataObject={(response) => response.data?.data?.data}
      characterCount={15}
      marginT={"mt-3"}
      marginB={"mb-2"}
      align={"left"}
      SpecialCard={RCollaborationCard}
      showListerMode={"cardLister"}
      perLine={3}
      refresh={refreshRecords}
    />}
    </div>
  );
};
