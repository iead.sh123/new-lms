import React, { useContext, useState } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { Services } from "engine/services";
import SaveAnswersButtons from "../SaveAnswersButtons";
import Feedbacks from "../feedbacks";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import CorrectOrReviewMatch from "./CorrectOrReviewMatch";

import Headers from "../Headers";

const SolveMatch = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  // Maintain a list of used answer options
  const [usedOptions, setUsedOptions] = useState([]);
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  const answersOption = ({ innerProps, label, data }) => {
    return (
      <div
        className="pl-1 pt-3 d-flex align-items-center cursor-pointer"
        {...innerProps}
      >
        {data.image_hash_id == null ? (
          ""
        ) : (
          <img
            className={styles.answer__image}
            src={Services.storage.file + data.image_hash_id}
            alt={data.label}
            width="55px"
            height="55px"
          />
        )}
        <p>{label}</p>
      </div>
    );
  };

  const selectedAnswers =
    QuestionSetSolveData?.questionAnswers?.questionAnswers.find(
      (el) => el.questionContextId == question.module_context_question_id
    );

  // const correctAnswer = selectedAnswers?.answer
  //   ? selectedAnswers?.answer.split(",").map((pair) => {
  //       return {
  //         questionOrder: pair.split(":")[0],
  //         answerOrder: pair.split(":")[1],
  //       };
  //     })
  //   : [];
  // const correctAnswer = selectedAnswers?.answer
  //   ? selectedAnswers?.answer.split(",").map((pair) => +pair.split(":")[1])
  //   : [];

  const correctAnswer = selectedAnswers?.answer
    ? selectedAnswers?.answer.split(",").reduce((acc, pair) => {
        const [key, value] = pair.split(":").map(Number);
        acc[key] = { questionOrder: key, answerOrder: value };

        return acc;
      }, {})
    : null;

  // const correctAnswer = selectedAnswers?.answer
  //   ? selectedAnswers?.answer.split(",").map((pair) => {
  //       return {
  //         questionOrder: pair.split(":")[0],
  //         answerOrder: pair.split(":")[1],
  //       };
  //     })
  //   : [];

  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />
      <div className="mb-3 mt-3">
        {question.pairs
          .filter((item) => item.isRight == false)
          .map((el, ind) => (
            <RFlex
              styleProps={{
                justifyContent: "space-between",
                marginBottom: "20px",
                alignItems: "center",
                flexWrap: "wrap",
              }}
            >
              <div key={el.id} style={{ display: "grid" }}>
                <span>{el.text} ?</span>
                {el?.files && el?.files.length > 0 && (
                  <img
                    src={Services.storage.file + el?.files[0]?.hash_id}
                    alt={el?.id}
                    className={styles.image}
                  />
                )}
              </div>

              {/* .filter((ell) => {
                        return correctAnswer == null
                          ? {}
                          : !correctAnswer[el?.order]?.answerOrder ==
                              ell?.order &&
                              correctAnswer[el?.order]?.questionOrder ==
                                el.order;
                      }) */}
              {QuestionSetSolveData.correctOrReviewMode ? (
                <CorrectOrReviewMatch question={question} ind={ind} />
              ) : (
                <div style={{ width: "300px" }}>
                  <RSelect
                    name={el.id}
                    // value={{}}
                    key={el.id}
                    option={question.pairs
                      .filter((item) => item.isRight == true)

                      .map((ell) => {
                        return {
                          value: ell.id,
                          label: ell.text,
                          image_hash_id:
                            ell.files.length > 0 ? ell.files[0].hash_id : null,
                          questionOrder: el.order,
                          answerOrder: ell.order,
                        };
                      })}
                    closeMenuOnSelect={true}
                    placeholder={tr`select_answer`}
                    onChange={(e) => {
                      setUsedOptions((prevUsedOptions) => [
                        ...prevUsedOptions,
                        e.value,
                      ]);
                      QuestionSetSolveData.handleFillDataToSolveQuestion({
                        questionGroupId: questionGroup.id,
                        groupContextId: questionGroup.module_context_group_id,
                        questionContextId: question.module_context_question_id,
                        type: question.type.name,
                        key: "answer",
                        value: `${e.questionOrder}:${e.answerOrder}`,
                        isCorrect: true,
                      });
                    }}
                    customOption={answersOption}
                  />
                </div>
              )}
            </RFlex>
          ))}
      </div>
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveMatch;
