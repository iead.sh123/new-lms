import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardHeader,
  CardBody,
} from "reactstrap";
import GLearningObject from "./GLearningObject";
import GStateMachine from "./GStateMachine";
import classnames from "classnames";
import tr from "components/Global/RComs/RTranslator";

const LearningObject = () => {
  const [activeTab, setActiveTab] = useState("learningObject");

  return (
    <Card>
      <CardHeader>
        <h5 className={"text-center"}>{tr`learning_object`}</h5>
      </CardHeader>
      <CardBody>
        <div className="nav-tabs-navigation">
          <div className="nav-tabs-wrapper">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: activeTab === "learningObject",
                  })}
                  onClick={() => {
                    setActiveTab("learningObject");
                  }}
                >
                  {tr`learning_object`}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: activeTab === "stateMachine",
                  })}
                  onClick={() => {
                    setActiveTab("stateMachine");
                  }}
                >
                  {tr`state_machine`}
                </NavLink>
              </NavItem>
            </Nav>
          </div>
        </div>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="learningObject">
            <GLearningObject />
          </TabPane>
          <TabPane tabId="stateMachine">
            <GStateMachine />
          </TabPane>
        </TabContent>
      </CardBody>
    </Card>
  );
};

export default LearningObject;
