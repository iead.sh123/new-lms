import React, { useEffect, useState } from "react";

import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import firebaseConfig from "./firebase-config";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { post } from "config/api";
import { destroy } from "config/api";

const initiateToken = (uid) => {

  // Initialize Firebase
  const firebaseApp = initializeApp(firebaseConfig);
  const messaging = getMessaging(firebaseApp);
  getToken(messaging, {
    vapidKey:
      "BIuWWRuP_RcwT_dyR1ZOGo7eH0dl8sOicQa10EoBwm9hyhpNJ5n97DqFepk3hgZl7G7AsZtfd7-2GQryHZWdJMM",
  })
    .then(async (currentToken) => {
      if (currentToken) {
        // Send the token to your server and update the UI if necessary
        // ...
        const data = {
          token: currentToken,
        
          device_type: "web",
        };

        //-------------------------------------------------------------------

        try {
          const response = await post(
            Services.notficiation.backend + "api/token/register-token",
            data
          );

          if (response.data.status === 1) {
            // Helper.cl("token registered successfuly");
          } else {
            // Helper.cl("register token error");
          }
        } catch (error) {
          // Helper.cl(error, "register token exception error");
        }
      }
      //------------------------------------------------------------------------
      //setToken(currentToken); //save token to store
      else {
        // Show permission request UI
        // console.log(
        //   "No registration token available. Request permission to generate one."
        // );
        // ...
      }
    })
    .catch((err) => {
      // console.log("An error occurred while retrieving token. ", err);
      // ...
    });

  onMessage(messaging, (payload) => {
    // Helper.cl(payload, "nl2 events 1");
    // console.log("events on on Message. ", payload);
    // console.log("Message received. ", payload);
    // ...
  });

  async function requestNotificationsPermissions() {
    // console.log("bc4:Requesting notifications permission...");
    const permission = await Notification.requestPermission();

    if (permission === "granted") {
      // console.log("bc4:Notification permission granted.");
      // Notification permission granted.
      await saveMessagingDeviceToken();
    } else {
      // console.log("bc4:Unable to get permission to notify.");
    }
  }

  const handleMessage = (message, background) => {
    //  Helper.cl(events,"events in the onMessage");
    // Helper.cl(message, "nl3 onMessage message");

    // Helper.cl(message.data,"message.data");+

    // const datajson=JSON.parse(message?.data?.payload);
    //  Helper.cl(datajson,"nl3 onMessage datajson");
    // // datajson?.map(d=>{
    // //   Helper.cl(d,"datajson memeber");
    // // })
    //   console.log(
    //     'fb1 New foreground notification from Firebase Messaging! in Firebase Services',
    //    JSON.parse(message.data.payload)
    //   );
    //   console.log(
    //     '--------'
    //   );
    // if(background){
    //      const notificationTitle = payload.notification.title;
    //      const notificationOptions = {
    //        body: payload.notification.body,
    //        icon: "/logo192.png",
    //    };
    //  self.registration.showNotification(
    //      notificationTitle,
    //      notificationOptions
    //    );

    //}
    //-----------
    const fn = message?.data?.notifications;
    const jfn = JSON.parse(fn);

    const newpayload = JSON.parse(jfn[0].payload);

    if (newpayload) {
      const se = events.find((e) => e.name == "new_notification");
      if (se) {
        Helper.cl(se, "nl3 selected event is ");

        const newData = message.data;
        newData.notifications = [];
        const newNotification = jfn[0];
        newNotification.payload = newpayload;
        newData.notifications.push(newNotification);

        se.callback(newData);
      } else {
        Helper.cl("No callback for this name 1");
      }

      const se1 = events.find((e) => e.name == "generic_channel");
      if (se1) {
        Helper.cl(se1, "nl3 selected event is ");

        const newData1 = message.data;
        // newData.notifications = [];
        // const newNotification = jfn[0];
        // newNotification.payload = newpayload;
        // newData.notifications.push(newNotification);
        se1.callback(newData1);
      } else {
        Helper.cl("No callback for this name 2");
      }
    }
  };
  async function saveMessagingDeviceToken() {
    try {
      const currentToken = await getToken(getMessaging());
      if (currentToken) {
        // console.log("bc4:Got FCM device token:", currentToken);
        // Saving the Device Token to Cloud Firestore.
        // const tokenRef = doc(getFirestore(), 'fcmTokens', currentToken);
        //  await setDoc(tokenRef, { uid: getAuth().currentUser.uid });

        // This will fire when a message is received while the app is in the foreground.
        // When the app is in the background, firebase-messaging-sw.js will receive the message instead.
        const channel4Broadcast = new BroadcastChannel("channel4");
        channel4Broadcast.onmessage = (event) => {
          Helper.cl(
            event.data,
            "bc4:got stuff from background message : event.data.data"
          );
          handleMessage(event.data, true);
        };
        onMessage(getMessaging(), (message) => {
          Helper.cl("bc4:onMessage(getMessaging()");
          handleMessage(message);
        });
      } else {
        // Need to request permissions to show notifications.
        requestNotificationsPermissions();
      }
    } catch (error) {
      console.error("Unable to get messaging token.", error);
    }
  }
  Helper.cl("fb1 f1");
  saveMessagingDeviceToken();
  Helper.cl("fb1 f2");
};
export const removeToken = async () => {
  // Initialize Firebase

  
  Helper.cl("removeToken1");
  const firebaseApp = initializeApp(firebaseConfig);
  const messaging = getMessaging(firebaseApp);

  Helper.cl("removeToken1");
  getToken(messaging, {vapidKey:"BIuWWRuP_RcwT_dyR1ZOGo7eH0dl8sOicQa10EoBwm9hyhpNJ5n97DqFepk3hgZl7G7AsZtfd7-2GQryHZWdJMM",
  })
    .then(async (currentToken) => {
      Helper.cl("removeToken2");
      if (currentToken) {
        Helper.cl("removeToken3");
        const data = {token: currentToken};
        try {
          const response = await destroy(
            Services.notficiation.backend + "api/token/remove-token",
            data
          );

          if (response.data.status === 1) {
            // Helper.cl("token registered successfuly");
          } else {
            // Helper.cl("register token error");
          }
        } catch (error) {
          // Helper.cl(error, "register token exception error");
        }
      }
      //------------------------------------------------------------------------
      //setToken(currentToken); //save token to store
      else {
        // Show permission request UI
        // console.log(
        //   "No registration token available. Request permission to generate one."
        // );
        // ...
      }
    })
    .catch((err) => {
      Helper.cl("removeToken15 err",err);
      // console.log("An error occurred while retrieving token. ", err);
      // ...
    });


};

const events = [];
export const subscribeToReceiver = (new_events) => {
  Helper.cl(new_events, "str:subscribeToReceiver new events");
  Object.entries(new_events).forEach(([r1, r2]) => {
    Helper.cl(r1, "str:one new r1");
    Helper.cl(r2, "str:one new r2");
    Object.entries(r2).forEach(([key, value]) => {
      Helper.cl(key, "str:push eventkey");
      Helper.cl( "str:push callback");
      events.push({ name: key, callback: value });
    });
  });
  //Helper.cl(events,"events subscribeToReceiver events ")
};

export default initiateToken;
