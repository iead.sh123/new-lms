import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { warningColor } from "config/constants";
import { Input } from "reactstrap";
import { Services } from "engine/services";
import styles from "../../SolveQuestionSet.module.scss";

const Headers = ({ questionNumber, question }) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <>
      <RFlex styleProps={{ justifyContent: "space-between" }} className="mb-4">
        <span>
          {tr`question`} {questionNumber}
          {question?.isRequired &&
          QuestionSetSolveData.submitted == "solving" ? (
            <span
              style={{ color: warningColor, paddingLeft: 10 }}
            >{tr`You have to answer this question to proceed`}</span>
          ) : (
            ""
          )}
        </span>
        {QuestionSetSolveData.teacherInCorrectionAndReviewMode ? (
          <span>
            {tr`points_from`}&nbsp;
            {question?.points}
          </span>
        ) : QuestionSetSolveData.submitted == "review" ? (
          <span>
            <span style={{ color: warningColor, fontWeight: "bold" }}>
              {question?.teacherMark}
            </span>
            /{question?.points}&nbsp;
            {tr`points`}
          </span>
        ) : (
          <span>
            {question?.points} {tr`points`}
          </span>
        )}
      </RFlex>
      <RFlex
        className="mb-3"
        styleProps={{ justifyContent: "space-between", alignItems: "center" }}
      >
        <h6 dangerouslySetInnerHTML={{ __html: `${question?.text} ?` }} />

        {QuestionSetSolveData.teacherInCorrectionAndReviewMode && (
          <Input
            name="mark"
            type="number"
            min={0}
            value={
              QuestionSetSolveData?.questionsCorrection?.questionsMarks[
                question?.learner_context_question_id
              ]?.mark
            }
            max={question?.points}
            style={{ width: "85px" }}
            onChange={(event) => {
              QuestionSetSolveData.handleAddFeedbackToQuestion({
                learnerContextQuestionId: question.learner_context_question_id,
                key: event.target.name,
                value: event.target.value,
              });
              QuestionSetSolveData.handleCalcQuestionsMark();
            }}
          />
        )}
      </RFlex>
      {question?.files && question?.files.length > 0 && (
        <RFlex>
          <img
            src={Services.storage.file + question?.files[0]?.hash_id}
            alt={question?.id}
            className={styles.full__image}
          />
        </RFlex>
      )}
    </>
  );
};

export default Headers;
