import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { get } from "config/api";
import { useParams } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const AddUsersToRole = ({ payloadData, setPayloadData }) => {
	const { organizationUserId, tabTitle, roleId } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);

	const handleAddUsersToRole = (record) => {
		setPayloadData((prevState) => {
			// Check if the id already exists in the organization_users_ids array
			const exists = prevState.organization_users_ids.includes(record.organization_user_id);

			if (exists) {
				// Remove the id from the organization_users_ids array
				const updatedRolesIds = prevState.organization_users_ids.filter((roleId) => roleId !== record.organization_user_id);

				// Return the updated state
				return {
					...prevState,
					organization_users_ids: updatedRolesIds,
				};
			} else {
				// Add the id to the organization_users_ids array
				const updatedRolesIds = [...prevState.organization_users_ids, record.organization_user_id];

				// Return the updated state
				return {
					...prevState,
					organization_users_ids: updatedRolesIds,
				};
			}
		});
	};

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/organization_user/usersForAddToRole/${roleId}`;
		let response = await get(specific_url ? specific_url : url);

		if (response && response.data && response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.map((rc) => ({
			id: rc.id,
			tableName: "AddUsersToRoleLister",
			details: [
				{ key: tr`name`, value: rc.name },
				{ key: tr`email`, value: rc.email },
			],
			actions: [
				{
					name: "add",
					icon: payloadData.organization_users_ids.includes(rc.organization_user_id) ? iconsFa6.delete : "fa fa-plus",
					outline: true,
					color: payloadData.organization_users_ids.includes(rc.organization_user_id) ? "danger" : "primary",
					onClick: () => {
						handleAddUsersToRole(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords, payloadData]
	);
	return <RAdvancedLister {...propsLiterals.listerProps} />;
};

export default AddUsersToRole;
