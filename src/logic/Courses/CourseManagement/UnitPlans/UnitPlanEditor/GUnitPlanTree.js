import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Collapse, Form, FormGroup } from "reactstrap";
import { Input } from "reactstrap";
import RUnitplanItems from "view/UnitPlan/RUnitPlanTree/RUnitplanItems";
import Drawer from "react-modern-drawer";
import styles from "../UnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import "react-modern-drawer/dist/index.css";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";

const GUnitPlanTree = () => {
	const history = useHistory();
	const { courseId, cohortId, unitPlanId } = useParams();
	const [selectedItems, setSelectedItems] = useState([]);
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [searchText, setSearchText] = useState("");
	const [isOpen, setIsOpen] = useState(false);

	const toggleDrawer = () => {
		setIsOpen((prevState) => !prevState);
	};

	const toggleSectionCollapse = (id) => {
		const isOpenSection = "collapse" + id;
		if (isOpenSection == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};
	const { unitPlan } = useSelector((state) => state.unitPlan);

	const handleJumpToItemInEditor = (UnitPlanItem) => {
		if (unitPlanId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/unit-plans/${unitPlanId}?unitPlanItem=${UnitPlanItem}`
			);
		} else {
		}
	};

	// Recursive function to search for items by name and collect them along with their parents
	const searchItems = (sections, name, path = []) => {
		const results = [];

		const searchInSection = (section, currentPath) => {
			for (const item of section.items) {
				if (item.text.includes(name)) {
					results.push({
						item: item.id ? item.id : item.fakeId,
						path: [...currentPath, section.id ? section.id : section.fakeId],
					});
				}

				if (item.items && item.items.length > 0) {
					searchInSection(item, [...currentPath, section.id ? section.id : section.fakeId]);
				}
			}
		};

		for (const section of sections) {
			searchInSection(section, path);
		}

		return results;
	};
	// Handle the search and selection of items
	const handleSearch = () => {
		const matchingItems = searchItems(unitPlan?.uplsections, searchText);
		// Open collapse to first result (just added to section collapse)
		if (matchingItems && matchingItems.length > 0) {
			setOpenedCollapses("collapse" + matchingItems[0]?.path[0]);
		}
		// Flatten the matchingItems array to extract item and path values
		const flattenedData = matchingItems.reduce((accumulator, currentItem) => {
			// Push item and path into the accumulator array
			accumulator.push(currentItem.item, ...currentItem.path);
			return accumulator;
		}, []);
		// Create a Set from the flattenedData to remove duplicates
		const uniqueData = [...new Set(flattenedData)];
		setSelectedItems(uniqueData);
	};

	return (
		<React.Fragment>
			<div className={styles.tree_box} onClick={toggleDrawer}>
				<div className={styles.watermark}></div>
			</div>
			<Drawer open={isOpen} onClose={toggleDrawer} direction="right" className={styles.drawer_tree}>
				{unitPlan?.uplsections?.length > 0 ? (
					<>
						<Form className={styles.form_input}>
							<FormGroup>
								<Input
									type="text"
									placeholder={tr`jump-to`}
									className={styles.input__tree}
									onChange={(event) => {
										setSearchText(event.target.value);
									}}
									value={searchText}
								/>
								<i
									aria-hidden="true"
									className={`fa fa-search ${styles.search_input_icon}`}
									onClick={() => searchText !== "" && handleSearch()}
								/>
							</FormGroup>
						</Form>

						{unitPlan &&
							unitPlan?.uplsections &&
							unitPlan?.uplsections.length > 0 &&
							unitPlan?.uplsections.map((section) => {
								return (
									<div key={section?.id ? section?.id : section?.fakeId}>
										<div
											className={styles.tree__section}
											onClick={() => {
												toggleSectionCollapse(section?.id ? section?.id : section?.fakeId);
												handleJumpToItemInEditor(section?.id ? section?.id : section?.fakeId);
											}}
										>
											<i
												className={`${
													openedCollapses === "collapse" + (section?.id ? section?.id : section?.fakeId)
														? "fa-solid fa-chevron-down pt-1"
														: "fa-solid fa-chevron-right pt-1"
												} ${styles.do__not__matched__item}
                      `}
											/>

											<i
												className={`${
													openedCollapses === "collapse" + (section?.id ? section?.id : section?.fakeId)
														? "fa-solid fa-folder-open pt-1"
														: "fa-solid fa-folder pt-1"
												} ${
													selectedItems.includes(section.id ? section.id : section.fakeId)
														? styles.matched__item
														: styles.do__not__matched__item
												}
                      `}
											/>

											<p
												className={` ${
													selectedItems.includes(section.id ? section.id : section.fakeId)
														? styles.matched__item
														: styles.do__not__matched__item
												}`}
											>
												{section.name}
											</p>
										</div>
										<Collapse isOpen={openedCollapses === "collapse" + (section?.id ? section?.id : section?.fakeId)}>
											<RUnitplanItems
												items={section.items}
												searchText={searchText}
												handleJumpToItemInEditor={handleJumpToItemInEditor}
												selectedItems={selectedItems}
											/>
										</Collapse>
									</div>
								);
							})}
						<RFlex onClick={toggleDrawer} className={styles.hide_drawer + " m-3"}>
							<p>{tr`hide`}</p>
							<i className="fa fa-arrow-right" style={{ position: "relative", top: "5px" }} />
						</RFlex>
					</>
				) : (
					<div className={styles.no__data}>
						<h6>{tr`no_data`}</h6>
					</div>
				)}
			</Drawer>
		</React.Fragment>
	);
};

export default GUnitPlanTree;
