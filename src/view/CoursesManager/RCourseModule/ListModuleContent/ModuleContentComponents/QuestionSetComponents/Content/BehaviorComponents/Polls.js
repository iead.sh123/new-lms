import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { FormGroup, Input } from "reactstrap";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";

const Polls = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<div className="mt-2">
			<RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
				<AppCheckbox
					onChange={(event) =>
						CourseModuleData.handleChangeBehaviorInModuleContent({
							moduleId: CourseModuleData.specificModule.id,
							moduleContentId: content.id,
							key: "displayDate",
							value: event.target.checked == true ? " " : null,
						})
					}
					label={tr`display_date`}
					checked={content?.behavior?.displayDate ? true : false}
				/>
				{content?.behavior?.displayDate && (
					<FormGroup>
						<Input
							name="displayDate"
							type="datetime-local"
							placeholder={tr`display_date`}
							value={moment(content?.behavior?.displayDate).format("YYYY-MM-DDTHH:mm")}
							style={{ height: "30px", width: "200px" }}
							onChange={(event) => {
								const localDateTime = new Date(event.target.value);

								CourseModuleData.handleChangeBehaviorInModuleContent({
									moduleId: CourseModuleData.specificModule.id,
									moduleContentId: content.id,
									key: event.target.name,
									value: localDateTime.toISOString(),
								});
							}}
						/>
					</FormGroup>
				)}
			</RFlex>
			<RFlex styleProps={{ width: "35%", justifyContent: "space-between" }}>
				<AppCheckbox
					onChange={(event) =>
						CourseModuleData.handleChangeBehaviorInModuleContent({
							moduleId: CourseModuleData.specificModule.id,
							moduleContentId: content.id,
							key: "closeDate",
							value: event.target.checked == true ? " " : null,
						})
					}
					label={tr`closing_date`}
					checked={content?.behavior?.closeDate ? true : false}
				/>
				{content?.behavior?.closeDate && (
					<FormGroup>
						<Input
							name="closeDate"
							type="datetime-local"
							placeholder={tr`close_date`}
							value={moment(content?.behavior?.closeDate).format("YYYY-MM-DDTHH:mm")}
							style={{ height: "30px", width: "200px" }}
							onChange={(event) => {
								const localDateTime = new Date(event.target.value);

								CourseModuleData.handleChangeBehaviorInModuleContent({
									moduleId: CourseModuleData.specificModule.id,
									moduleContentId: content.id,
									key: event.target.name,
									value: localDateTime.toISOString(),
								});
							}}
						/>
					</FormGroup>
				)}
			</RFlex>
		</div>
	);
};

export default Polls;
