import { get, post } from "config/api";
import { Services } from "engine/services";

export const studentCoursesAPis = {
  getCourses: (termId) => get(`api/rabs/std${termId ? "/" + termId : ""}`),
  historySchoolClasses: () => get("api/rabs/history-school-classes"),

  getCourseCategories: (courseId) =>
    get(`api/courses/${courseId}/categoriestree`),
  getPlaceHolderContent: (qId, rabId) =>
    post(`std-api/question-set/place-holder/${qId}/${rabId}`),
  fetchLessonAssignments: (lessonId) =>
    get(`std-api/lesson/${lessonId}/question-sets`),
  fetchLessonVcRecordings: (lessonId) =>
    get(`api/lessons/vcRecordings/${lessonId}`),
  fetchLessonVcSupplementRecordings: (lessonId) =>
    get(`api/lessons/vcSupplementRecordings/${lessonId}`),
  fetchLessonAttachments: (lessonId) =>
    get(`api/lesson-attachments/${lessonId}`),

  fetchCourseContentsByCourseId: (courseId) =>
    get(`${Services.lesson.backend}api/courses/${courseId}/books`),

  fetchRabBook: (rabId) => get(`api/rabs/${rabId}/books`),
  getStudentCourseContent: (rabId) => get(`api/lessons/${rabId}`),
};
