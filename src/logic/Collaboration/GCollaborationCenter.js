import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSharedContentCard from "components/Global/RComs/Collaboration/RSharedContentCard";
import RCoursesTree from "view/CoursesManager/RCoursesTree";
import RCourseCard from "components/Global/RComs/Collaboration/RCourseCard";
import Select from "react-select";
import RTags from "components/Global/RComs/RTags";
import tr from "components/Global/RComs/RTranslator";
import { post } from "config/api";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import RCategoryCard from "components/Global/RComs/Collaboration/RCategoryCard";
import store from "store";
import RMulticheckboxDropdown from "components/Global/RComs/RMulticheckboxDropdown";
import RColumn from "view/RComs/Containers/RColumn";
import RRow from "view/RComs/Containers/RRow";
import produce from "immer";
import Swal from "utils/Alert";
import { redirect } from "./collaborationUtils";
import { DANGER } from "utils/Alert";
import { GCollaborationCenterDashboard } from "./GCollaborationCenterDashboard";
import { useSelector } from "react-redux";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RLessonPlanViewer from "view/Courses/CourseManagement/LessonPlan/RLessonPlanViewer";
import GCollaborationViewer from "./GCollaborationViewer";
import { toast } from "react-toastify";
//backend urls collaborate/{type_id}
// approve-records/{type_id}
// review-records/{type_id}

export const GCollaborationCenter = () => {
	const [nominatedCount, setNominiatedCount] = useState(0);
	const [approvedCount, setApprovedCount] = useState(0);
	const [reviewCount, setReviewCount] = useState(0);
	const [openModalToView, setOpenModalToView] = useState(false);
	const [dataToView, setDataToView] = useState({});

	//---------------------------------------center
	const [state, setState] = useState("nominate");

	//const uuser=  useSelector (s=>s.auth?.user) ;//store.getState().auth.user.current_organization;
	const org_id = useSelector((s) => s.auth?.user?.organization_id); //store.getState().auth.user.current_organization;

	const handleOpenModal = (data) => {
		setOpenModalToView(true);
		setDataToView(data);
	};
	const handleCloseModal = () => {
		setOpenModalToView(false);
		setDataToView({});
	};

	//--------------------------------------------
	const params = useParams();

	const agreement_id = params.agreement_id;
	const context_name = params.context_name;
	const context_id = params.context_id;
	const content_type = params.content_type;

	const [currentContentData, setCurrentContentData] = useState({});
	const [currentContent, setCurrentContent] = useState(0);
	//----------------------------------------------------------------Work Area

	const [itemsData1, setItemsData1] = useState([]);
	const [refreshRecords, setRefreshRecords] = useState(false);
	const [itemRecords, setItemRecords] = useState([]);

	useEffect(() => {
		setRefreshRecords(!refreshRecords);
	}, [currentContent, state]);

	const getItemsFromBackend = () => async (specific_url) => {
		// const url = Services.collaboration.backend+`api/collaboration-area/agreement/${params.agreement_id}/sharable-contents/`;
		if (!currentContent) {
			Helper.cl("getItemsFromBackend:must Select a content");
			return;
		}
		const subpath =
			state == "nominate"
				? "records-to-collaborate/" + currentContent
				: state == "review"
				? "records-to-review/" + currentContent
				: state == "approve"
				? "records-to-approve/" + currentContent
				: null;

		if (!subpath) {
			Helper.cl("Error at getItemsFromBackend");
			return;
		}

		const url = Services.collaboration.backend + `api/collaboration-center/` + subpath;
		let response1 = await get(url);
		if (response1) {
			Helper.cl("getItemsFromBackend resposne1");
			return response1;
		}
	};
	const setItemsData = (response) => {
		Helper.cl("sh1 f2")
			? "records-to-collaborate/" + currentContent
			: state == ""
			? "records-to-review/" + currentContent
			: state == ""
			? "records-to-approve/" + currentContent
			: null;

		const dd =
			state == "nominate" && response?.data?.data?.records
				? response?.data?.data?.records
				: state == "approve" && response?.data?.data?.records_to_approve
				? response?.data?.data?.records_to_approve
				: state == "review" && response?.data?.data?.records_to_review
				? response?.data?.data?.records_to_review
				: [];
		const dd1 = dd.map((d) => ({ ...d, levels: d.collaboration_levels ?? d.levels }));
		Helper.cl(dd1, "dd");
		setItemsData1(dd1);
	};

	const viewRecord = (record_id) => {
		//redirect somewhere
		// redirect(history, currentContentData.table_name, record_id);

		//redirect somewhere
		redirect(history, currentContentData.table_name, record_id);
		// ______________________________
		// courses;
		// lesson_plans;
		// unit_plans;
		// lessons;
		// if (currentContentData.table_name == "lesson_plans") {
		// }
	};
	const nominateRecord = (contnet_id, record_id) => {
		const firsthit = itemsData1?.filter((i) => i.id == record_id)?.[0];
		if (!(firsthit && firsthit && firsthit.levels)) {
			toast.error(tr("must select levels"));

			return;
		}
		Helper.cl(firsthit.levels, "firsthit.levels");
		const payload = {
			records: [
				{
					organization_id: org_id,
					id: record_id,
					refused: 0,
					nominated: 1,
					approved: 0,
					collaboration_levels: firsthit?.levels?.map((s) => s.id),
				},
			],
		};

		Helper.fastPost(
			Services.collaboration.backend + `api/collaboration-center/collaborate/${contnet_id}`,
			payload,
			"nominited successfully",
			"Error while nomination",

			() => {
				setItemsData1(itemsData1.filter((r) => r.id != record_id));
				setContentTypesRefresh(!contentTypesRefresh);
			},
			() => {
				Helper.cl("fail");
			}
		);
	};

	const approveRecord = (contnet_id, record_id) => {
		const firsthit = itemsData1.filter((i) => i.id == record_id)?.[0];
		Helper.cl(firsthit, "firsthit");
		if (!(firsthit && firsthit && firsthit.levels)) {
			toast.error(tr("must select levels"));

			return;
		}
		const payload = {
			records_to_approve: [
				{
					id: record_id,
					nominated: 0,
					refused: 0,
					approved: 1,
					// "candidate_for_collaboration": 1,
					organization_id: org_id,
					collaboration_levels: firsthit.levels.map((s) => ({ id: s.id })),
				},
			],
		};

		Helper.fastPost(
			Services.collaboration.backend + `api/collaboration-center/approve-records/${contnet_id}`,
			payload,
			"approved successfully",
			"Error while approving",
			() => {
				setItemsData1(itemsData1.filter((r) => r.id != record_id));
				Helper.cl("success"),
					() => {
						Helper.cl("fail");
					};
			}
		);
	};
	const reviewRecord = (contnet_id, record_id) => {
		const firsthit = itemsData1.filter((i) => i.id == record_id)?.[0];
		if (!(firsthit && firsthit && firsthit.levels)) {
			toast.error(tr("must select levels"));

			return;
		}

		const payload = {
			records_to_review: [
				{
					id: record_id,
					nominated: 0,
					refused: 0,
					approved: 1,
					candidate_for_collaboration: 1,
					collaboration_levels: firsthit.levels.map((s) => ({ id: s.id })),
				},
			],
		};
		Helper.fastPost(
			Services.collaboration.backend + `api/collaboration-center/review-records/${contnet_id}`,
			payload,
			"Reviewed successfully",
			"Error while reviewing",
			() => {
				Helper.cl("success"),
					() => {
						Helper.cl("fail");
					};
			}
		);
	};

	const refuseRecord = (contnet_id, record_id) => {
		const firsthit = itemsData1.filter((i) => i.id == record_id)?.[0];
		if (!firsthit) {
			toast.error(tr("must select levels"));
			return;
		}
		const payload = {
			records_to_review: [
				{
					id: record_id,
					nominated: 0,
					refused: 1,
					approved: 0,
					// "candidate_for_collaboration": 1,
					organization_id: org_id,
					collaboration_levels: firsthit.levels.map((s) => s.id),
				},
			],
		};

		Helper.fastPost(
			Services.collaboration.backend + `api/collaboration-center/refuse-records/${contnet_id}`,
			payload,
			"refused successfully",
			"Error while refusing",
			() => {
				setItemsData1(itemsData1.filter((r) => r.id != record_id));
				Helper.cl("success"),
					() => {
						Helper.cl("fail");
					};
			}
		);
	};

	const setRecordLevels = (contnet_id, record_id, new_value) => {
		//redirect somewhere
		Helper.cl(new_value, "setRecordLevels new_value");
		const selectedLevels = new_value.filter((ll) => ll.checked).map((l) => ({ ...l, id: l.value }));
		const itemsData2 = produce(itemsData1, (temp_data) => {
			const firsthit = temp_data.filter((i) => i.id == record_id)?.[0];
			firsthit.levels = selectedLevels;
		});
		setItemsData1(itemsData2);
		//r.levels.filter(ll=>ll.id==l.id).length>0
		// Helper.cl(itemsData2,"setRecordLevels itemsData2")
		// Helper.cl(itemsData1,"setRecordLevels setItemsData1")
	};

	useEffect(() => {
		Helper.cl(itemsData1, "itemsData1 at the useEffect");

		setItemRecords(
			itemsData1?.map((r, index) => {
				//const actions=getActions(r.id,r.sharable_status,r.sharable_status?.pivot,r.sharable_status?.pivot?.need_approval)
				Helper.cl(r.tags, "ttt r.tags");
				return {
					id: r.id,
					specialProps: {
						title: { text: r.name },
						//UpLeftText:"All:"+r.count,
						//Actions: actions,
						//subtitle :{ text:(r.count??0)+" all,"+(r.count??0)+" recent"},
						//bottomBorderColor:r.sharable_status?"gray":"gray",
						//  onClick1:()=>{clickContent(r.id)},
						tags: r.tags
							? JSON.parse(r.tags)
							: []?.slice(0, 2)?.map((t, i) => {
									return { ...t, backgroundColor: i == 1 ? "#FEF1EC" : "#D9E5FE", iconColor: i == 1 ? "#F34400" : "#0051FE" };
							  }),

						// "organization_name": "ogranization7",
						// "organization_image": "nxopxjdmlknaddyaewgvyqrzd"
						//creator_name

						Author: { image: r.creator_image, text: "creator:" + r.creator_name },
						Actions: [
							{
								onClick: () => {
									if (currentContentData.table_name == "lesson_plans" || currentContentData.table_name == "unit_plans") {
										handleOpenModal(r);
									} else {
										viewRecord(r.id);
									}
								},
								title: "view",
								icon: "fa fa-eye",
								color: "black",
							},

							state == "nominate"
								? {
										onClick: () => {
											nominateRecord(currentContent, r.id);
										},
										title: "nominate",
										icon: "fa fa-check",
										color: "green",
								  }
								: state == "approve"
								? {
										onClick: () => {
											approveRecord(currentContent, r.id);
										},
										title: "approve",
										icon: "fa fa-check",
										color: "green",
								  }
								: state == "review"
								? {
										onClick: () => {
											reviewRecord(currentContent, r.id);
										},
										title: "review",
										icon: "fa fa-check",
										color: "green",
								  }
								: null,

							state == "nominate"
								? {
										onClick: () => {
											refuseRecord(currentContent, r.id);
										},
										title: "Remove",
										icon: "fa fa-remove",
										color: "red",
								  }
								: state == "approve"
								? {
										onClick: () => {
											refuseRecord(currentContent, r.id);
										},
										title: "Refuse",
										icon: "fa fa-remove",
										color: "red",
								  }
								: state == "review"
								? {
										onClick: () => {
											refuseRecord(currentContent, r.id);
										},
										title: "Refuse",
										icon: "fa fa-remove",
										color: "red",
								  }
								: null,
						],

						additionalComponent: (
							<RMulticheckboxDropdown
								options={levels.map((l) => {
									return {
										label: l.name,
										value: l.id,

										checked:
											state == "approve" || state == "review"
												? r?.levels?.filter((ll) => {
														Helper.cl(ll, "filtering levels");
														return (ll.value ?? ll.id) == l.id;
												  }).length > 0
												: state == "nominate"
												? r?.levels?.filter((ll) => {
														Helper.cl(ll, "filtering levels");
														return (ll.value ?? ll.id) == l.id;
												  }).length > 0
												: null,
									};
								})}
								onChange={(e) => {
									setRecordLevels(currentContent, r.id, e);
								}}
								// disabled={state!="nominate"}
							/>
						), //hazem decide how to make the checked false and true from the item itself
						//Author

						//                 approved: 0
						//                 approved_at: "2023-07-20 12:53:13"
						// approved_by: 1
						// candidate_for_collaboration: 1
						// created_at: "2023-07-20 12:53:13"
						// creator_id: 1
						// id: 391
						// name: "lesson_plan_7"
						// nominated: 1
						// organization_id: 1
						// refused: 0
						// refused_at: "2023-07-20 12:53:13"
						// refused_by: 1
						// set_as_candidate_at: "2023-07-20 12:53:13"
						// set_as_candidate_by: 1
						// updated_at: "2023-07-20 12:53:13"
					},
				};
			})
		);

		if (state == "nominate") setNominiatedCount(itemsData1.length);
		else if (state == "approve") setApprovedCount(itemsData1.length);
		else if (state == "review") setReviewCount(itemsData1.length);
	}, [itemsData1, levels, state]);

	//-------------------------------------------------------------------

	//----------------------------------------------------------Courses--Work Area

	const [category_id, setCategoryId] = useState([]);
	const [coursesContentsData, setCoursesContentsData] = useState([]);
	const [refreshCourses, setRefreshCourses] = useState(false);
	const [coursesRecords, setCoursesRecords] = useState([]);
	const viewCourse = async (id) => {
		if (currentContentData.table_name == "courses") {
			history.push(`${baseURL}/${genericPath}/course/${id}`);
		}
		// const url = Services.collaboration.backend+`api/collaboration-area/sharable_content/4/record/${id}`;
		// let response1 = await get(url);
		// if (response1) {
		//     Helper.cl(response1,"course view response")
		//   return response1;
		// }
	};
	const history = useHistory();

	const handleImport = async (id) => {
		const payload = { record_ids: [id], context_name: context_name, context_id: context_id };
		const url = Services.collaboration.backend + `api/collaboration-area/sharable_content/${currentContent}/import`;
		let response1 = await post(url, payload);
		if (response1) {
			history.goBack();
			// return response1;
		}
	};

	// const clickContent = async (id) => {
	// 	Helper.cl(id, "clickConet id");
	// 	if (agreement_id) {
	// 		// view
	// 		Helper.cl(id, "clickContent id");
	// 		Helper.cl(currentContentData, "clickContent currentContentData");
	// 		if (currentContentData.table_name == "lesson_plans") {
	// 			history.push(`${baseURL}/${genericPath}/course/${id}/lesson-plan`);
	// 		}
	// 		if (currentContentData.table_name == "courses") {
	// 			history.push(`${baseURL}/${genericPath}/course/${id}/lesson-plan`);
	// 		}
	// 	} ///-----------------------------------import
	// 	else {
	// 		//hazem import
	// 		const payload = { record_ids: [id], context_name: context_name, context_id: context_id };
	// 		const url = Services.collaboration.backend + `api/collaboration-area/sharable_content/${currentContent}/import`;
	// 		let response1 = await post(url, payload);
	// 		if (response1) {
	// 			Helper.cl("sh1 f1");
	// 			return response1;
	// 		}
	// 	}

	// 	// const url = Services.collaboration.backend+`api/sharable_content/4/record/${id}`;
	// 	// let response1 = await get(url);
	// 	// if (response1) {
	// 	//     Helper.cl(response1,"course view response")
	// 	//   return response1;
	// 	// }
	// };

	useEffect(() => {
		Helper.cl(coursesContentsData, "coursesContentsData");
		setCoursesRecords(
			coursesContentsData.map((r, index) => {
				Helper.cl(r, "setCoursesRecords contents r");
				return {
					id: r.id,
					specialProps: {
						title: { text: r.name },
						//UpLeftText:"All:"+r.count,
						//Actions: actions,
						//subtitle :{ text:(r.count??0)+" all,"+(r.count??0)+" recent"},
						//bottomBorderColor:r.sharable_status?"gray":"gray",
						onClick1: () => {
							viewCourse(r.id);
						},
						tags: r.tags,
						//Author

						bgImage: Services.storage.file + r.image,
						actions: [
							{
								onClick: () => {
									viewCourse(r.id);
								},
								title: agreement_id ? "view" : "import",
								text: agreement_id ? "view" : "import",
								icon: "fa fa-eye",
							},
						],
					},

					name: "course2",
				};
			})
		);
	}, [coursesContentsData]);

	useEffect(() => {
		setRefreshCourses(!refreshCourses);
	}, [category_id]);

	//----------------------------------------------------------------
	// const [treeData,setTreeData]=useState([]);
	// useEffect(() =>{
	//   // alert(currentContent,"current contnet changedd")
	//     const getTree= async()=>{
	//         const url=Services.collaboration.backend+`api/collaboration-area/categories-tree/agreement/${params.agreement_id}/sharable-content/${currentContent}`;
	//         const res=await get(url)
	//         if(res&&res.data?.status&&res.data?.data)
	//         {

	//             setTreeData(res.data?.data)
	//         }
	//         else
	//         {
	//             Helper.cl(url,"tree1 url");
	//             Helper.cl(res,"tree1 res");
	//         }
	//   }

	//   Helper.cl(currentContent,"tree1 currentContent")
	//   if(currentContent==4)
	//   {
	//     Helper.cl("tree1 1")
	//     getTree();

	//   }
	//   else
	//   {setRefreshRecords(!refreshRecords);}

	// },[currentContent])
	// const [treeData,setTreeData]=useState([]);
	useEffect(() => {
		setContentTypes(
			produce(contentTypes, (tempContentTypes) => {
				tempContentTypes.map((c) => {
					c.focused = false;
				});
				const ss = tempContentTypes.filter((c) => currentContent == c.id);
				if (ss && ss.length) ss[0].focused = true;
			})
		);
	}, [currentContent]);

	const [contentTypes, setContentTypes] = useState([]);
	const [contentTypesRecords, setContentTypesRecords] = useState([]);

	const getContentTypesFromBackend = async (specific_url) => {
		const url = Services.collaboration.backend + `api/collaboration-center/organization-contents/` + state;
		Helper.cl(url, "co url");
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			Helper.cl(response1, "counts1 contents");
			return response1;
			//response.data?.data?.data?.counts
		}
	};

	const [contentTypesRefresh, setContentTypesRefresh] = useState(false);
	useEffect(() => {
		setContentTypesRefresh(!contentTypesRefresh);
	}, [state]);
	const [levels, setLevels] = useState([]);

	useEffect(() => {
		const getLevels = async (specific_url) => {
			const url = Services.collaboration.backend + `api/organization/${org_id}/levels`;
			Helper.cl(url, "co url");
			let response2 = await get(specific_url ? specific_url : url);

			if (response2 && response2.data && response2.data.status == 1) {
				const lvls = response2.data.data.data;
				setLevels(lvls);
			}
		};
		getLevels();
	}, []);
	useEffect(() => {
		setContentTypesRecords(
			contentTypes.map((r, index) => {
				// const count=(r.count??r.records_count??0)
				return {
					id: r.id,
					specialProps: {
						title: { text: r.name },
						subtitle: { text: r?.counts?.all + " all | " + r?.counts?.recent + " recent" },
						bottomBorderColor: r.sharable_status ? "gray" : "gray",
						onClick1: () => {
							setCurrentContentData(r);
							setCurrentContent(r.id);
						},
						borderLess: true,
						focused: r.focused,
						padding: "1px",
						margin: "1px",
					},
				};
			})
		);
		!currentContent && setCurrentContent(contentTypes?.[0]?.id);
	}, [contentTypes, nominatedCount, approvedCount, reviewCount]);

	const setContentTypes1 = (response) => {
		if (response?.data?.data?.data) setContentTypes(response?.data?.data?.data);
	};

	//--------------------------------------------------Provider select
	const [providers, setProviders] = useState([]);
	const [tags, setTags] = useState([]);
	useEffect(() => {
		const getProviders = async (specific_url) => {
			const url = Services.collaboration.backend + `api/collaboration-area/providers`;
			Helper.cl(url, "co url");
			let response1 = await get(specific_url ? specific_url : url);

			if (response1) {
				const provs = response1?.data?.data?.map((r, index) => {
					return {
						value: r.id,
						label: (
							<RFlex>
								<img height="20px" src={Services.storage.file + r.image}></img>
								<span>{r.name}</span>{" "}
							</RFlex>
						),
					};
				});
				setProviders(provs);
			}
		};
		getProviders();
	}, []);
	const [selectedOptions, setSelectedOptions] = useState([]);

	const handleChange = (selectedOptions) => {
		setSelectedOptions(selectedOptions);
	};

	const [searchPayload, setSearchPayload] = useState([]);
	const search = async () => {
		const tagids = tags.map((t) => t.value);
		const provids = providers.map((p) => p.value);

		const url = Services.collaboration.backend + `api/collaboration-area/sharable-contents`;
		Helper.cl(url, "co url");
		const s = { tag_ids: tagids, destination_side: provids };
		setSearchPayload(s);
		let response1 = await post(url, s);

		if (response1) {
			Helper.cl(response1, "search Response");

			if (response1?.data?.data) setContentTypes(response1?.data?.data?.sharable_contents);
		}
	};
	//----------------------------------------------------------------
	// const categoryCards11=[
	//   {title:"Collaboration Dashboard",text:"Collaboration Collaboration",icon:"",backGroundColor:"gray",cornerColor:"blue",iconColor:"gray",onClick:()=>{setState("dashboard")}},
	//   {title:"Nominate",text:"Nominate Nominate",icon:"fa fa-paperclip",          backGroundColor:"red",cornerColor:"yellow",iconColor:"red",onClick:()=>{setState("nominate")}},
	//   {title:"Approve",text:"Approve Approve Approve",icon:"fa fa-check-square",  backGroundColor:"blue" ,cornerColor:"green",iconColor:"blue",onClick:()=>{setState("approve")}},
	//   {title:"Review",text:"Review Review Review",icon:"fa fa-search",            backGroundColor:"",cornerColor:"green",iconColor:"",onClick:()=>{setState("review")}},
	//            ]
	const categoryCards = [
		{
			title: "Collaboration Dashboard",
			text: "All And Recent Collaborated Content",
			icon: "fa fa-pie-chart",
			backGroundColor: "",
			cornerColor: "rgb(234,229,251)",
			iconColor: "rgb(178,152,223)",
			onClick: () => {
				setState("Collaboration Dashboard");
			},
		},
		{
			title: "Nominate",
			text: "Select Content To Approve Them",
			icon: "fa fa-hand-pointer",
			backGroundColor: "",
			cornerColor: "rgb(231,249,227)",
			iconColor: "rgb(148,227,120)",
			onClick: () => {
				setState("nominate");
			},
		},
		{
			title: "Approve",
			text: "Approve Nominated Content To Review",
			icon: "fa fa-check",
			backGroundColor: "",
			cornerColor: "rgb(226,245,252)",
			iconColor: "rgb(166,224,236)",
			onClick: () => {
				setState("approve");
			},
		},
		{
			title: "Review",
			text: "Review Content And Share whith Others",
			icon: "fa fa-search",
			backGroundColor: "",
			cornerColor: "rgb(255,239,2116)",
			iconColor: "rgb(241,210,156)",
			onClick: () => {
				setState("review");
			},
		},
	];
	const [options, setOptions] = useState([
		{ label: "l1", value: "1", checked: true },
		{ label: "l2", value: "2", checked: true },
		{ label: "l3", value: "3", checked: false },
		{ label: "l4", value: "4", checked: false },
	]);

	const selectedOptions1 = options.filter((option) => option.checked);

	const handleDropdownChange = (updatedOptions) => {
		setOptions(updatedOptions);
	};

	return (
		<RColumn>
			<AppModal
				size="xl"
				show={openModalToView}
				parentHandleClose={handleCloseModal}
				headerSort={<GCollaborationViewer tableName={currentContentData.table_name} id={dataToView.id} handleImport={handleImport} />}
			/>

			<RFlex>
				{/* states= {state} */}
				<div className="cards">
					{categoryCards.map((i) => (
						<div className="card_1">
							<RCategoryCard
								// height='125px'
								title={i?.title}
								text={i?.text}
								icon={i?.icon}
								focused={state.toLowerCase() == i?.title.toLowerCase()}
								backGroundColor={i.backGroundColor}
								cornerColor={i.cornerColor}
								iconColor={i.iconColor}
								onClick={i?.onClick}
							/>
						</div>
					))}
				</div>
				{/* <RMulticheckboxDropdown
            options={options}

            onChange={handleDropdownChange}
          /> */}
			</RFlex>
			{state == "Collaboration Dashboard" ? (
				<GCollaborationCenterDashboard />
			) : (
				<RFlex extraStyles={{ alignItems: "flex-start", width: "100%" }}>
					<RFlex style={{ flex: 1, width: "150px" }}>
						shared contents
						{/* {Helper.js(levels,"levels")} */}
						<RAdvancedLister
							hideTableHeader={false}
							colorEveryOtherRow={false}
							//firstCellImageProperty={"image"}
							getDataFromBackend={getContentTypesFromBackend}
							setData={setContentTypes1}
							records={contentTypesRecords}
							getDataObject={(response) => response.data?.data?.data}
							characterCount={15}
							marginT={"mt-3"}
							marginB={"mb-2"}
							align={"left"}
							SpecialCard={RCollaborationCard}
							showListerMode={"cardLister"}
							perLine={1}
							refresh={contentTypesRefresh}
						/>
					</RFlex>

					<RFlex style={{ flex: 10 }}>
						<RAdvancedLister
							hideTableHeader={false}
							colorEveryOtherRow={false}
							//firstCellImageProperty={"image"}
							getDataFromBackend={getItemsFromBackend(currentContent)}
							setData={setItemsData}
							records={itemRecords}
							getDataObject={(response) => response.data?.data?.data}
							characterCount={15}
							refresh={refreshRecords}
							marginT={"mt-3"}
							marginB={"mb-2"}
							align={"left"}
							SpecialCard={RCollaborationCard}
							showListerMode={"cardLister"}
							perLine={3}
							overflow={"visible"}
							overflowX={"visible"}
						/>
					</RFlex>
				</RFlex>
			)}
		</RColumn>
	);
};
