import React from "react";
import { Button } from "reactstrap";
import { translate } from "../../../../utils/translate";

const DrawTools = ({
  canvasRef,
  setBrushColor,
  brushColor,
  brushSize,
  setBrushSize,
  saveCanvas,
}) => {
  return (
    <div className="canvas_draw_tools">
      <div>
        <Button type="button" variant="success" onClick={() => saveCanvas()}>
          {translate("Save")}
        </Button>
        <Button
          type="button"
          variant="danger"
          onClick={() => {
            canvasRef.current.clear();
          }}
        >
          {translate("Clear")}
        </Button>
        <Button
          type="button"
          variant="info"
          onClick={() => {
            canvasRef.current.undo();
          }}
        >
          {translate("Undo")}
        </Button>
      </div>
      <div className="canvas_draw_color">
        <input
          type="color"
          className=""
          value={brushColor}
          onChange={(e) => setBrushColor(e.target.value)}
        />

        <input
          type="range"
          className="form-range"
          min="1"
          max="15"
          value={brushSize}
          onChange={(e) => setBrushSize(+e.target.value)}
        />
      </div>
    </div>
  );
};

export default DrawTools;
