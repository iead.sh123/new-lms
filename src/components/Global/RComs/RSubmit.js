import { Button } from "reactstrap";
import tr from "./RTranslator";

function RSubmit({
  className,
  onClick,
  value,
  icon,
  disabled,
  id,
  type,
  dir,
  style,
  loading,
}) {
  return (
    <Button
      id={id ? id : null}
      disabled={disabled ? disabled : false}
      className={"btn-round button_text " + (className ? className : "")}
      value={value}
      onClick={onClick}
      color="primary"
      outline
      type={type ? type : "submit"}
      style={style}
    >
      {loading ? (
        <>
          {value ? value : tr`submit`}{" "}
          <i className="c:\Users\User\AppData\Local\Programs\Microsoft VS Code\resources\app\out\vs\code\electron-sandbox\workbench\workbench.htmlfa fa-refresh fa-spin"></i>
        </>
      ) : icon ? (
        <div style={{ textAlign: "center" }}>
          <i className={icon} style={{ fontWeight: "bold" }}></i>
          {value}
        </div>
      ) : value ? (
        value
      ) : (
        tr`submit`
      )}
    </Button>
  );
}
export default RSubmit;
