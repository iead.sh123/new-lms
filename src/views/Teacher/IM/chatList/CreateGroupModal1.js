import React, { useRef, useState } from "react";
import ContextMenu from "../ContextMenu/ContextMenu";
import UserImg from "../userImg";
import { CHAT_ACTIONS_ICONS } from "../constants/ChatTags";
import { relativeDate } from "utils/dateUtil";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RColumn from "view/RComs/Containers/RColumn";
import RRow from "view/RComs/Containers/RRow";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import RForm from "components/Global/RComs/RForm";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import UserSelector from "./UserSelector";
import IMNewCycleReducer from "store/reducers/global/IMNewCycle.reducer";
import { IMApi } from "api/IMNewCycle";
import Swal, { SUCCESS, WARNING, DANGER } from "utils/Alert";
import RRoundImageEdit from "components/Global/RComs/RRoundImageEdit";
import { Label } from "reactstrap";
import RLoader from "components/Global/RComs/RLoader";
import { toast } from "react-toastify";
export default function CreateGroupModal1({ open, setOpen, group = true, loggedUserId }) {
	const [image, setImage] = useState();

	const handleUploadImage = (newImage) => {
		//Helper.cl(newImage, "new Image");

		setImage(newImage);
		// let imageAdded = [];
		// childData.map((item) => {
		//   imageAdded.push({
		//     url: item.url,
		//     file_name: item.file_name,
		//     type: item.type,
		//   });
		// });
		//
	};
	const messageControl = {
		type: "text",
		label: tr`Message`,
		name: "Message",
	};
	const fileControl = {
		type: "file",
		label: tr`Group Image`,
		name: "group_image",
		binary: true,
		uploadName: "groupImage",
		setSpecificAttachment: () => {},
		typeFile: "image/jpeg, image/* , image/png ,image/jpg",
		singleFile: true,
		handleData: handleUploadImage,
	};
	const initialformItems = [
		{
			type: "text",
			label: tr`Group Name`,
			name: "groupName",
		},
		{
			type: "text",
			label: tr`Description`,
			name: "Description",
		},
		// fileControl,
		// {
		//   type: "datetime-local",
		//   label: tr`time_zone`,
		//   name: "time_zone",

		// },
	];

	const chatItems = [];
	const [users, setUsers] = useState([]);
	const formRef = useRef();

	const [creating, setCreating] = useState(false);
	const submitForm = () => {
		setCreating(true);
		//Helper.cl("submitForm");
		if (formRef.current) {
			formRef.current.dispatchEvent(new Event("submit", { cancelable: true, bubbles: true }));
		}
	};

	const onCreateGroupSubmit = async (e) => {
		e.preventDefault();
		setCreating(true);
		// Helper.cl(,"submitForm e.target[0].value");
		// Helper.cl(,"submitForm e.target[1].value");
		// Helper.cl(e.target[2],"submitForm e.target[2]");

		//  Helper.cl(users,"Users");

		const payload = group
			? {
					name: e.target[0].value,
					message: e.target[1].value,
					user_ids: users.map((u) => u.id),
					image: [image],
					user_id: loggedUserId,
			  }
			: {
					user_id: loggedUserId,
					//message: e.target[0].value,
					user_ids: users.map((u) => u.id),
			  };

		//get({{chats_url}}/api/chats/initialNewChat)

		const s = await IMApi.createConversation(payload);
		//Helper.cl(s, "s");
		if (s.data.status == 1) {
			setOpen(false);
		} else {
			toast.error(s.data.msg);
		}
		setCreating(false);
	};

	return (
		<>
			{/* {open?"open true":"open false"} */}
			<RModal
				isOpen={open}
				toggle={() => setOpen(false)}
				header={<h6 className="text-capitalize">{group ? "Create Group" : "New Chat"}</h6>}
				footer={
					<div>
						{/* {creating?<RLoader/>:<></>} */}
						<RButton
							text={creating ? "Creating..." : group ? tr`Create Group` : tr`Create Chat`}
							disabled={creating}
							onClick={submitForm}
							color="primary"
						/>
						<RButton
							text={tr`cancel`}
							onClick={() => {
								setOpen(false);
							}}
							color="white"
							style={{
								color: "#668ad7",
								background: "white",
								border: "0px solid",
							}}
						/>
					</div>
				}
				body={
					<div id="crateGroupForm">
						<Label
							style={{
								fontWeight: "bold",
								marginTop: "8px",
								marginBottom: "0px",
								textTransform: "capitalize",
							}}
						>
							Select Users
						</Label>

						<UserSelector setUsers={setUsers} single={!group}></UserSelector>
						<RForm
							formItems={group ? initialformItems : chatItems}
							// submitLabel={"Update Profile"}
							withSubmit={false}
							onSubmit={onCreateGroupSubmit}
							formRef={formRef}
						/>
						{group && <RRoundImageEdit images={[image]} setImages={(e) => setImage(e[0])} />}
					</div>
				}
				//  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
				size="lg"
			/>
		</>
	);
}
