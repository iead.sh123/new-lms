import { faHandMiddleFinger } from "@fortawesome/free-solid-svg-icons";
import React from "react";
const RGrid = ({
  children,
  rows,
  rowe,
  cols,
  cole,
  overflowX,
  height,
  additionalStyle,
  additionalClasses,
}) => {
  return (
    <div
      style={Object.assign({}, additionalStyle, {
        display: "inline-block",
        gridColumnStart: cols,
        gridColumnEnd: cole,
        gridRowStart: rows,
        gridRowEnd: rowe,
        // backgroundColor: "red",
        // borderRadius: "20px",
        overflowX: "hidden",
        height: height,
      })}
      className={additionalClasses}
    >
      {children}
    </div>
  );
};
export default RGrid;
