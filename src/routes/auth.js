import SignIn from "views/auth/SignIn";
import SignUp from "views/auth/SignUp";

const routes = [
	{
		path: "/login",
		name: "Login",
		mini: "L",
		component: SignIn,
		layout: process.env.REACT_APP_BASE_URL + "/auth",
	},
	{
		path: "/loader",
		name: "Loader",
		mini: "CP",
		component: SignIn,
		layout: process.env.REACT_APP_BASE_URL + "/auth",
	},
	{
		path: "/register",
		name: "Register",
		mini: "L",
		component: SignUp,
		layout: process.env.REACT_APP_BASE_URL + "/auth",
	},
];
export default routes;
