import { liveSessionApi } from "api/global/liveSession";
import {
	START_LIVE_SESSION_REQUEST,
	START_LIVE_SESSION_SUCCESS,
	START_LIVE_SESSION_ERROR,
	END_LIVE_SESSION_REQUEST,
	END_LIVE_SESSION_SUCCESS,
	END_LIVE_SESSION_ERROR,
	JOIN_MEETING_TO_GET_ATTEND_LINK_REQUEST,
	JOIN_MEETING_TO_GET_ATTEND_LINK_SUCCESS,
	JOIN_MEETING_TO_GET_ATTEND_LINK_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const startLiveSessionAsync = (meetingId, startCounter, handleCloseModal, userType) => async (dispatch) => {
	dispatch({ type: START_LIVE_SESSION_REQUEST });

	try {
		const response = await liveSessionApi.startMeeting(meetingId);
		if (response.data.status === 1) {
			dispatch({
				type: START_LIVE_SESSION_SUCCESS,
			});
			handleCloseModal && handleCloseModal();
			startCounter && startCounter();
			dispatch(joinMeetingToGetAttendLinkAsync(meetingId, userType));
		} else {
			dispatch({
				type: START_LIVE_SESSION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: START_LIVE_SESSION_ERROR,
		});
		toast.error(error?.message);
	}
};

export const endLiveSessionAsync = (meetingId, resetCounter, handleOpenModal) => async (dispatch) => {
	dispatch({ type: END_LIVE_SESSION_REQUEST });

	try {
		const response = await liveSessionApi.endMeeting(meetingId);
		if (response.data.status === 1) {
			dispatch({
				type: END_LIVE_SESSION_SUCCESS,
			});
			resetCounter && resetCounter();
			handleOpenModal && handleOpenModal();
		} else {
			dispatch({
				type: END_LIVE_SESSION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: END_LIVE_SESSION_ERROR,
		});
		toast.error(error?.message);
	}
};

export const joinMeetingToGetAttendLinkAsync = (meetingId, userType) => async (dispatch) => {
	dispatch({ type: JOIN_MEETING_TO_GET_ATTEND_LINK_REQUEST });

	try {
		const response = await liveSessionApi.joinMeetingToGetAttendLink(meetingId, userType);
		if (response.data.status === 1) {
			dispatch({
				type: JOIN_MEETING_TO_GET_ATTEND_LINK_SUCCESS,
			});
			window.open(response.data.data, "_blank");
		} else {
			dispatch({
				type: JOIN_MEETING_TO_GET_ATTEND_LINK_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: JOIN_MEETING_TO_GET_ATTEND_LINK_ERROR,
		});
		toast.error(error?.message);
	}
};
