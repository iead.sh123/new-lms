import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { successColor } from "config/constants";
import { Row, Col } from "reactstrap";
import Cool_Kids_Bust from "assets/img/new/Cool_Kids_Bust.png";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const LiveSessionModal = ({ handleCloseLiveSessionModal }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<Row>
			<Col md={12} lg={7} className="d-flex justify-content-between flex-column">
				<p>
					{tr`great`} , {CourseModuleData?.user?.name} !
				</p>
				<RFlex styleProps={{ color: successColor }}>
					<i className="mt-1 fa fa-check-circle" />
					<p>{tr`your first live session was created successfully!!`}</p>
				</RFlex>
				<p>{tr`you can see today’s live sessions in your dashboard`}</p>
				<p>{tr`or manage this life session`}</p>
				<RFlex>
					<RButton
						onClick={() =>
							CourseModuleData.handleViewLessonContent({
								LessonId: null,
								MeetingId: null,
							})
						}
						text={tr`manage_this_session`}
						color="primary"
					/>
					<RButton onClick={handleCloseLiveSessionModal} text={tr`got_it`} color="primary" outline />
				</RFlex>
			</Col>
			<Col md={12} lg={5}>
				<img src={Cool_Kids_Bust} width={"298px"} height={"272px"} />
			</Col>
		</Row>
	);
};

export default LiveSessionModal;
