import React from "react";
import Switch from "react-bootstrap-switch";


// eslint-disable-next-line react/prop-types
const RreactstrapSwitch = ({
  defaultValue,
  offText,
  onText,
  onColor,
  offColor,
  onChange})=>{
  return <Switch
    defaultValue={defaultValue}
    offText={offText}
    onText={onText} 
    onColor={onColor}
    offColor={offColor}
    onChange={onChange}
  />;
  
};
export default RreactstrapSwitch;
