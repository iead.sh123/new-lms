import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect, RouteProps } from "react-router-dom";

const TeacherRoute = (props) => {
  const { component: Component, ...rest } = props;
  const { isAuthenticated, user } = useSelector((state) => state?.auth);

  const render = (props) => {
    if (!isAuthenticated) {
      return <Redirect to={process.env.REACT_APP_BASE_URL} />;
    }
    if (user?.type !== "teacher") {
      return <Redirect to={process.env.REACT_APP_BASE_URL} />;
    }

    return <Component {...props} />;
  };

  return <Route {...rest} render={render} exact />;
};

export default TeacherRoute;
