import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { usePaginate } from "components/Global/RComs/RPaginatorLocal";
import { useParams } from "react-router";
import RPaginatorLocal from "components/Global/RComs/RPaginatorLocal";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { allStudentsAsync } from "store/actions/global/coursesManager.action";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import REmptyData from "components/RComponents/REmptyData";

function GStudents() {
	const { courseId } = useParams();
	const dispatch = useDispatch();
	const [searchData, setSearchData] = useState("");
	const [allRecords, setAllRecords] = useState([]);
	const [records, setRecords] = useState([]);

	const { students, studentsLoading } = useSelector((state) => state.coursesManagerRed);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const _records = React.useMemo(() => {
		if (!students || !students.records || !students.headers) {
			return [];
		}

		const transformedData = students.records.map((record) => ({
			table_name: "students",
			id: record.user_id,
			details: students.headers.map((header) => ({
				key: tr(header.label),
				value: record[header.value],
				type: header.value.toLowerCase() == "image" ? "image" : null,
			})),
		}));
		// return transformedData;
		setAllRecords(transformedData);
		setRecords(transformedData);
	}, [students]);

	const handleSearch = (emptySearch) => {
		const filteredData = allRecords.filter((record) => {
			// Convert the "Full Name" value to lowercase for case-insensitive search
			const fullName = record.details.find((detail) => detail.key === "Full Name");
			return fullName && fullName.value.toLowerCase().includes(emptySearch !== undefined ? emptySearch : searchData.toLowerCase());
		});
		setRecords(filteredData);
	};

	useEffect(() => {
		dispatch(allStudentsAsync(courseId));
	}, []);

	const { currentPage, totalPages, handleNext, handlePrevious, handleLast, handleFirst, pItems } = usePaginate(10, records);

	return (
		<>
			<div className="content">
				{studentsLoading ? (
					<Loader />
				) : (
					<>
						<RSearchHeader
							searchLoading={false}
							searchData={searchData}
							handleSearch={handleSearch}
							setSearchData={setSearchData}
							handleChangeSearch={handleChangeSearch}
							inputPlaceholder={tr`search`}
							addNew={false}
							inputWidth={"30%"}
						/>
						{records.length > 0 ? (
							<>
								<RLister Records={pItems} ListerClass={""} ItemClass={""} check={0} />
								<RPaginatorLocal
									total={totalPages}
									currentPage={currentPage}
									handleNext={handleNext}
									handlePrevious={handlePrevious}
									handleLast={handleLast}
									handleFirst={handleFirst}
								/>
							</>
						) : (
							<REmptyData />
						)}
					</>
				)}
			</div>
		</>
	);
}

export default GStudents;
