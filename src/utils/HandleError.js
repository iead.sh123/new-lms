import { toast } from "react-toastify";
import tr from "components/Global/RComs/RTranslator";

export const handleError = (msg) => {
	switch (msg) {
		case "Request failed with status code 500":
			toast.error(tr`Request failed  500, Verify the data entered , thank you`);
			break;

		default:
			toast.error(msg);
			break;
	}
};
