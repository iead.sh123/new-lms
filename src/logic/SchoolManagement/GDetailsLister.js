import React, { useState, useEffect } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./SchoolLister.module.scss";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "store/actions/global/schoolManagement.action";
import RIconDiv from "components/Global/RComs/RIconDiv/RIconDiv";
import * as colors from "config/constants";
import { dataTypes } from "logic/SchoolManagement/constants";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RListData from "components/Global/RComs/RListData/RListData";
import { types } from "./constants";
import RButtonIcon from "components/Global/RComs/RButtonIcon";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import GEnrollUsers from "./GEnrollUsers";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GCoursesSchool from "./GCoursesSchool";
import AssessmentSchedular from "./AssessmentSchedular";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { TOGGLE_SELECT_MODE, TOGGLE_SELECT_MODE_OFF, TOGGLE_SELECT_MODE_ON } from "store/actions/global/globalTypes";
import {} from "store/actions/global/globalTypes";
import Loader from "utils/Loader";
import { toast } from "react-toastify";
const GDetailsLister = () => {
	const dispatch = useDispatch();
	const { openStageId, openGradLevelId, stages, gradeLevels } = useSelector((state) => state?.schoolManagementRed);
	const { principals, students } = useSelector((state) => state?.schoolManagementRed);
	const currentGradeLevel = gradeLevels[openGradLevelId];
	const currentStage = stages[openStageId];
	const label = currentStage && !currentGradeLevel ? dataTypes.Principals : currentStage && currentGradeLevel ? dataTypes.Students : null;
	const currentId = currentStage && !currentGradeLevel ? openStageId : currentStage && currentGradeLevel ? openGradLevelId : null;
	const { assessments } = useSelector((state) => state?.schoolManagementRed);
	const currentType =
		currentStage && !currentGradeLevel ? types.Stage : currentStage && currentGradeLevel ? types.GradeLevel : types.School;
	const currentListData = currentType == types.School ? null : currentType == types.Stage ? principals : students;
	const selectMode = currentListData?.selectMode;
	const selectedNumber = currentListData ? currentListData.ids.filter((key) => currentListData[key].selected).length : 0;

	const toggleSelectModeOn = () => {
		dispatch({ type: TOGGLE_SELECT_MODE_ON, payload: { type: label } });
	};
	const toggleSelectModeOff = () => {
		dispatch({ type: TOGGLE_SELECT_MODE_OFF, payload: { type: label } });
	};
	const selectFilterActions = [
		{
			label: tr("All"),
			action: (type) => {
				dispatch(actions.selectAll(type));
				toggleSelectModeOn();
			},
			parameter1: label,
		},
		{
			label: tr("Clear_Selection"),
			action: (type) => {
				dispatch(actions.clearAllSelection(type));
				toggleSelectModeOff();
			},
			parameter1: label,
		},
	];
	const viewAsFilterActions = [
		{
			label: tr("Alphabetically"),
			action: () => {
				label == dataTypes.Principals
					? dispatch(actions.getPrincipalForEducation(currentId, "a"))
					: dispatch(actions.getStudentsForGrade(currentId, "a"));
			},
		},
		{
			label: tr("Recent_Added"),
			action: () => {
				label == dataTypes.Principals
					? dispatch(actions.getPrincipalForEducation(currentId, "r"))
					: dispatch(actions.getStudentsForGrade(currentId, "r"));
			},
		},
	];
	const [enrollUsersOpen, setEnrollUsersOpen] = useState(false);
	const handleCloseModal = () => {
		setEnrollUsersOpen(false);
	};
	const filterBy = (
		<RFlex
			id="filter By"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + colors.primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr("Sort_By")}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const handleDeleteButton = () => {
		if (label == dataTypes.Principals) {
			dispatch(actions.removePrinipalsFromStage());
		} else if (label == dataTypes.Students) {
			dispatch(actions.removeStudentsFromGrade());
		}
	};
	const handleSelectUser = (type, entity) => {
		if (!entity.selected) {
			dispatch(actions.selectUser(type, entity.id));
		} else {
			dispatch(actions.unSelectUser(type, entity.id));
		}
	};
	const handleSendNotification = async () => {
		let success = false;
		if (label == dataTypes.Principals) {
			success = await dispatch(actions.sendNotificationToPrincipals());
			if (success) toast.success(tr`success`);
		} else if (label == dataTypes.Students) {
			success = dispatch(actions.sendNotificationToStudents());
			if (success) toast.success(tr`success`);
		}
	};
	return (
		<>
			{/* <button onClick={() => dispatch(actions.getPrincipalForEducation(currentStage.id))}>Hello</button> */}
			{currentType != types.School ? (
				<>
					<RFlex>
						<p className="p-0 m-0 font-weight-bold">{currentGradeLevel ? currentGradeLevel?.title : currentStage?.name}</p>
						<AppModal
							show={enrollUsersOpen}
							size={"md"}
							header={true}
							headerSort={<GEnrollUsers closeModal={handleCloseModal} type={label} levelId={currentId} />}
							parentHandleClose={handleCloseModal}
						/>
					</RFlex>
					<RFlex id="Buttons" className="justify-content-between align-items-baseline" styleProps={{ width: "100%" }}>
						<RFlex id="active select" className="align-items-center" styleProps={{ gap: "15px" }}>
							<RFlex id="select with icon" className="align-items-baseline" styleProps={{ gap: "8px" }}>
								{selectMode && <span className="p-0 m-0 text-success">{selectedNumber}</span>}
								<span
									onClick={() => (currentListData?.ids?.length <= 0 ? null : toggleSelectModeOn())}
									className="p-0 m-0"
									style={{
										fontSize: "12px",
										cursor: currentListData?.ids?.length <= 0 ? "default" : "pointer",
										color: currentListData?.ids?.length <= 0 ? colors.lightGray : colors.successColor,
									}}
								>
									{selectMode ? "Selected" : "Select"}
								</span>
								{/* <RButton
                                    disabled={currentListData?.ids?.length <= 0}
                                    text={selectMode ? "Selected" : "Select"}
                                    onClick={toggleSelectModeOn}
                                    color="link"
                                    className="text-success"
                                    style={{ margin: '0px', padding: '5px 10px' }}
                                /> */}
								{currentListData?.ids?.length <= 0 ? null : (
									<RDropdownIcon
										actions={selectFilterActions}
										menuClassname={styles.SelectDropDown}
										iconContainerStyle={{ border: "1px solid" + colors.successColor, borderRadius: "100%" }}
									/>
								)}
							</RFlex>
							{selectMode && (
								<>
									<RButton
										disabled={selectedNumber <= 0 ? true : false}
										text="Send Notification"
										faicon={iconsFa6.bell}
										onClick={handleSendNotification}
										color="primary"
										outline
										style={{ margin: "0px", padding: "5px 10px" }}
									/>
									<RButton
										faicon={iconsFa6.delete}
										iconStyle={{ color: colors.dangerColor }}
										text={"Remove"}
										onClick={() => {
											handleDeleteButton();
										}}
										color="link"
										disabled={selectedNumber <= 0 ? true : false}
										style={{ margin: "0px", padding: "5px 10px" }}
										className="text-danger"
									/>
								</>
							)}
						</RFlex>
						<RDropdownIcon actions={viewAsFilterActions} component={filterBy} />
					</RFlex>
					{currentListData.loading ? (
						<Loader />
					) : currentListData.ids.length > 0 ? (
						<RListData
							onClick={handleSelectUser}
							inSelectMode={selectMode}
							label={label}
							levelType={currentType}
							data={currentListData}
							setEnrollUsersOpen={setEnrollUsersOpen}
							dataLength={Object.keys(currentListData).length}
						/>
					) : (
						<>
							<RFlex className="flex-column">
								<span className="p-0 m-0 font-weight-bold">{label}</span>

								<p className="p-0 m-0" style={{ cursor: "pointer" }} onClick={() => setEnrollUsersOpen(true)}>
									No {label} Yet &nbsp;
									<span className="text-primary p-0 m-0"> {tr(`Enroll Or Create New`)} Account</span>
								</p>
							</RFlex>
						</>
					)}
					{currentType == types.GradeLevel && <AssessmentSchedular assessments={assessments} />}
				</>
			) : (
				""
			)}
		</>
	);
};

export default GDetailsLister;
