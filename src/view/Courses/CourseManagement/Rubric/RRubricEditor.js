import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/CourseManagement/Rubrics/GRubricEditor";
import { primaryColor } from "config/constants";
import { FieldArray } from "formik";
import RCategoryCollapses from "./RCategoryCollapses";
import RRubricHeader from "./RRubricHeader";
import RFormBasic from "./RFormBasic";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RRubricEditor = () => {
	const RubricData = useContext(RubricContext);

	return (
		<RFlex className={styles.rubric__editor__container}>
			<RFlex className={styles.rubric__editor__preview__container}>
				<RRubricHeader />
				<RFormBasic />
			</RFlex>

			<FieldArray name="categories">
				{(arrayHelpers) => (
					<>
						<RFlex
							styleProps={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
							onClick={() => arrayHelpers.push({ fakeId: Math.random(), subject: "", standards: [] })}
						>
							<i className="fa fa-plus pt-1" />
							<p>{tr`create_new_category`}</p>
						</RFlex>
						<RFlex className={styles.rubric__editor__categories}>
							{RubricData.values.categories &&
								RubricData.values.categories.length > 0 &&
								RubricData.values.categories.map((category, index) => (
									<RCategoryCollapses
										key={category.id ? category.id : category.fakeId}
										arrayHelpers={arrayHelpers}
										category={category}
										categoryIndex={index}
									/>
								))}
						</RFlex>
					</>
				)}
			</FieldArray>
		</RFlex>
	);
};

export default RRubricEditor;
