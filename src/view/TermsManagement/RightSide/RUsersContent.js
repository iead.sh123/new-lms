import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { primaryColor, successColor } from "config/constants";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import { dangerColor } from "config/constants";
import { boldGreyColor } from "config/constants";
import styles from "../TermsManagement.module.scss";

const RUsersContent = ({ students, teachers, handleSort, handleSelectedAllUsers, homeRoomUsers }) => {
	const TermsManagementData = useContext(TermsManagementContext);
	const displayedStudents = TermsManagementData.showAllStudentsInHomeRoom ? students : students?.slice(0, 12);
	const displayedTeachers = TermsManagementData.showAllTeachersInHomeRoom ? teachers : teachers?.slice(0, 12);

	const openedActions = TermsManagementData.showActions == TermsManagementData.itemObj?.dataObj?.id;

	const disabledActions =
		TermsManagementData.valuesUsers.teachers && TermsManagementData.valuesUsers.students
			? TermsManagementData.valuesUsers.students.concat(TermsManagementData.valuesUsers.teachers)?.length == 0
			: true;

	const viewAsButton = (
		<RFlex
			id="view as"
			styleProps={{
				height: "33px",
				justifyContent: "center",
				alignItems: "center",
				padding: "0px 10px",
				border: "1px solid" + primaryColor,
				borderRadius: "2px",
				cursor: "pointer",
			}}
		>
			<span className="p-0 m-0 text-primary" style={{ fontSize: "12px" }}>
				{tr`sort_by`}
			</span>
			<RFlex className="flex-column" styleProps={{ gap: "10px" }}>
				<i className="fa-solid fa-angle-up fa-sm text-primary" />
				<i className="fa-solid fa-angle-down fa-sm text-primary" />
			</RFlex>
		</RFlex>
	);

	const viewAsFilterActions = [
		{ label: tr`alphabetically`, action: () => handleSort("alphabiticallyDesc") },
		{ label: tr`recently_added`, action: () => handleSort("addedDesc") },
	];

	const selectFilterActions = [
		{
			label: tr`all`,
			action: () => {
				handleSelectedAllUsers();
				TermsManagementData.handleShowActions(TermsManagementData.itemObj?.dataObj?.id);
			},
		},
		{
			label: tr`clear_selection`,
			action: () => {
				TermsManagementData.handleHideActions();
			},
		},
	];

	return (
		<RFlex styleProps={{ flexDirection: "column", gap: 20 }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<h6>{TermsManagementData.itemObj?.dataObj?.name}</h6>
				{homeRoomUsers && (
					<RFlex styleProps={{ alignItems: "center" }}>
						<span
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() =>
								TermsManagementData.handleOpenHomeRoomModal({
									data: true,
								})
							}
						>{tr`edit`}</span>
						<i
							className={iconsFa6.delete}
							style={{ color: dangerColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handelDelete(TermsManagementData.itemObj?.dataObj?.id, "homeRoom")}
						/>
					</RFlex>
				)}
			</RFlex>

			<RFlex styleProps={{ width: "100%", justifyContent: "space-between", alignItems: "center", flexWrap: "wrap" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<RFlex className="align-items-baseline" styleProps={{ gap: "8px" }}>
						{+TermsManagementData?.valuesUsers?.teachers?.length + TermsManagementData?.valuesUsers?.students?.length > 0 && (
							<span className="p-0 m-0 text-success">
								{+TermsManagementData?.valuesUsers?.teachers?.length + TermsManagementData?.valuesUsers?.students?.length}
							</span>
						)}
						<span
							onClick={() => TermsManagementData.handleShowActions(TermsManagementData.itemObj?.dataObj?.id)}
							className="p-0 m-0 text-success"
							style={{ fontSize: "12px", cursor: "pointer" }}
						>
							{tr`select`}
						</span>
						<RDropdownIcon
							actions={selectFilterActions}
							iconContainerStyle={{ border: "1px solid" + successColor, borderRadius: "100%" }}
						/>
					</RFlex>

					{openedActions && (
						<div>
							<RButton
								text={tr`send_notification`}
								faicon={iconsFa6.bell}
								color="primary"
								outline
								style={{ margin: "0px", padding: "5px 10px" }}
								onClick={() =>
									TermsManagementData.handleSendNotificationsToUsersSelectedInHomeRoom({ type: homeRoomUsers ? "homeRoom" : "course" })
								}
								loading={TermsManagementData.sendNotificationsToUsersSelectedInHomeRoomLoading}
								disabled={disabledActions || TermsManagementData.sendNotificationsToUsersSelectedInHomeRoomLoading}
							/>
							<RButton
								text={tr`remove`}
								faicon={iconsFa6.delete}
								color="link"
								style={{ color: dangerColor }}
								onClick={() => TermsManagementData.handleUnEnrollUsersFromHomeRoom({ type: homeRoomUsers ? "homeRoom" : "course" })}
								loading={
									homeRoomUsers ? TermsManagementData.unEnrollUsersFromHomeRoomLoading : TermsManagementData.unEnrollUsersFromCourseLoading
								}
								disabled={
									homeRoomUsers
										? TermsManagementData.unEnrollUsersFromHomeRoomLoading || disabledActions
										: TermsManagementData.unEnrollUsersFromCourseLoading || disabledActions
								}
							/>
							{homeRoomUsers && (
								<span
									style={{ color: boldGreyColor, cursor: "pointer" }}
									onClick={() => TermsManagementData.handleOpenMoveUsersFromHomeRoomToAnother()}
								>{tr`change_home_room`}</span>
							)}
						</div>
					)}
				</RFlex>

				<RDropdownIcon actions={viewAsFilterActions} component={viewAsButton} />
			</RFlex>

			<RFlex styleProps={{ flexDirection: "column", flexWrap: "wrap" }}>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex styleProps={{ justifyContent: "space-between" }}>
						<RFlex>
							<span style={{ fontWeight: "bold" }}>{tr`students`}</span>
							<div className={styles.users__count}>{students?.length}</div>
						</RFlex>
						<span
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handleOpenUsersToEnrollModal("students")}
						>{tr`enroll_students`}</span>
					</RFlex>
					{students?.length == 0 && (
						<RFlex>
							<span style={{ color: boldGreyColor }}>{tr`no_students_yet`}</span>
							<span
								style={{ color: primaryColor, cursor: "pointer" }}
								onClick={() => TermsManagementData.handleOpenUsersToEnrollModal("students")}
							>{tr`enroll_students_or_create_new_profile`}</span>
						</RFlex>
					)}
				</RFlex>

				<RFlex styleProps={{ flexWrap: "wrap" }}>
					{displayedStudents?.map((student, index) => (
						<RFlex
							key={index}
							styleProps={{ alignItems: "center", width: "31%", cursor: openedActions ? "pointer" : "default" }}
							onClick={() => (openedActions ? TermsManagementData.handleSelectedHomeRoomEnrolledUsers("students", student.user_id) : {})}
						>
							<RProfileName key={student.id} name={student.full_name} img={student.hash_id ?? student.image} />
							{TermsManagementData.valuesUsers["students"]?.some((item) => +item.user_id == +student.user_id) ? (
								<i className={iconsFa6.check} style={{ color: successColor }} />
							) : (
								""
							)}
						</RFlex>
					))}
				</RFlex>
				{students?.length > 12 && (
					<RFlex>
						<span
							style={{ width: "fit-content", color: primaryColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handleShowAllStudentsInHomeRoom()}
						>
							{!TermsManagementData.showAllStudentsInHomeRoom ? tr`hide_students` : tr`show_all_students`}
						</span>
					</RFlex>
				)}
			</RFlex>

			<RFlex styleProps={{ flexDirection: "column", flexWrap: "wrap" }}>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex styleProps={{ justifyContent: "space-between" }}>
						<RFlex>
							<span style={{ fontWeight: "bold" }}>{tr`teachers`}</span>
							<div className={styles.users__count}>{teachers?.length}</div>
						</RFlex>
						<span
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handleOpenUsersToEnrollModal("teachers")}
						>{tr`enroll_teachers`}</span>
					</RFlex>
					{teachers?.length == 0 && (
						<RFlex>
							<span style={{ color: boldGreyColor }}>{tr`no_teachers_yet`}</span>
							<span
								style={{ color: primaryColor, cursor: "pointer" }}
								onClick={() => TermsManagementData.handleOpenUsersToEnrollModal("teachers")}
							>{tr`enroll_teachers_or_create_new_profile`}</span>
						</RFlex>
					)}
				</RFlex>

				<RFlex styleProps={{ flexWrap: "wrap" }}>
					{displayedTeachers?.map((teacher, index) => (
						<RFlex
							key={index}
							styleProps={{ alignItems: "center", width: "31%", cursor: openedActions ? "pointer" : "default" }}
							onClick={() => (openedActions ? TermsManagementData.handleSelectedHomeRoomEnrolledUsers("teachers", teacher.user_id) : {})}
						>
							<RProfileName key={teacher.id} name={teacher.full_name} img={teacher.hash_id ?? teacher.image} />
							{TermsManagementData.valuesUsers["teachers"].some((item) => +item.user_id == +teacher.user_id) ? (
								<i className={iconsFa6.check} style={{ color: successColor }} />
							) : (
								""
							)}
						</RFlex>
					))}
				</RFlex>

				{teachers?.length > 12 && (
					<RFlex>
						<span
							style={{ width: "fit-content", color: primaryColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handleShowAllTeachersInHomeRoom()}
						>
							{!TermsManagementData.showAllTeachersInHomeRoom ? tr`hide_teachers` : tr`show_all_teachers`}
						</span>
					</RFlex>
				)}
			</RFlex>
		</RFlex>
	);
};

export default RUsersContent;
