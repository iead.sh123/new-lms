import React, { useState, useEffect } from "react";
import Swal, { WARNING } from "utils/Alert";
import { useParams, useLocation, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { exportRubricAsPdfAsync } from "store/actions/global/rubric.actions";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { Form, Formik } from "formik";
import { rubricApi } from "api/global/rubric";
import RRubricEditor from "view/Courses/CourseManagement/Rubric/RRubricEditor";
import GRubricViewer from "./GRubricViewer";
import RStandard from "view/Courses/CourseManagement/Rubric/RStandard";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

export const RubricContext = React.createContext();

const GRubricEditor = () => {
	const { cohortId, curriculumId, courseId, rubricId } = useParams();
	const location = useLocation();
	const dispatch = useDispatch();
	const history = useHistory();

	const [openedCollapses, setOpenedCollapses] = useState([]);
	const [standardModal, setStandardModal] = useState(false);
	const [previewMode, setPreviewMode] = useState(false);
	const [isViewer, setIsViewer] = useState(false);
	const [categoryData, setCategoryData] = useState({ categoryIndex: null, standardIndex: null, arrayHelpers: null });
	const [isHovered, setIsHovered] = useState(null);
	const [isStandardHovered, setIsStandardHovered] = useState(null);
	const [selectedCategories, setSelectedCategories] = useState([]);

	const { exportRubricAsPdfLoading } = useSelector((state) => state.rubricRed);

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["rubricById", rubricId],
		queryFn: () => rubricApi.getRubricById(rubricId),
		enableCondition: rubricId != null,
	});

	const rubricMutation = useMutateData({
		queryFn: (date) => rubricApi.add(date),
		invalidateKeys: ["rubric"],
		displaySuccess: true,
		onSuccessLink: `${baseURL}/${genericPath}/course-management/${
			courseId ? `course/${courseId}` : `curricula/${curriculumId}`
		}/cohort/${cohortId}/rubrics`,
	});

	useEffect(() => {
		if (location.pathname.includes("viewer")) {
			setPreviewMode(true);
			setIsViewer(true);
		}
	}, [location]);

	const handleOpenStandardModal = ({ categoryIndex, standardIndex, arrayHelpers }) => {
		setStandardModal(true);
		setCategoryData({ categoryIndex: categoryIndex, standardIndex: standardIndex, arrayHelpers: arrayHelpers });
	};

	const handleCloseStandardModal = () => {
		setStandardModal(false);
		setCategoryData({ categoryIndex: null, standardIndex: null, arrayHelpers: null });
	};

	const collapsesCategoryToggle = (categoryId) => {
		const isOpen = openedCollapses.includes(categoryId);
		if (isOpen) {
			setOpenedCollapses(openedCollapses.filter((id) => id !== categoryId));
		} else {
			setOpenedCollapses([...openedCollapses, categoryId]);
		}
	};

	const actionsOnHover = (id) => {
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const actionsStandardOnHover = (id) => {
		const isOpen = "standardHover" + id;
		if (isOpen == isStandardHovered) {
			setIsStandardHovered("standardHover");
		} else {
			setIsStandardHovered("standardHover" + id);
		}
	};

	const handleSelectedCategories = (categoryId) => {
		setSelectedCategories((items) => [...items, categoryId]);
	};

	const handleRemoveCategoryFromSelectedCategory = (categoryId) => {
		setSelectedCategories(selectedCategories.filter((id) => id !== categoryId));
	};

	const handleExportRubricAsPdf = () => {
		dispatch(exportRubricAsPdfAsync(rubricId));
	};

	const handleBackToRubricEditor = () => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/rubric/${rubricId}`);
		} else if (curriculumId) {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/rubric/${rubricId}`);
		}
	};

	const handleChangePointedValue = (value, setFieldValue, values) => {
		Swal.input({
			title: tr``,
			message: value
				? tr`Are you sure you want to make this rubric pointed ? You need to insert all the points for this rubric to proceed.`
				: tr`Are you sure you want to make this rubric non-pointed? This action will remove all the points from this rubric.`,
			type: WARNING,
			onConfirm: () => changePointedValue(value, setFieldValue, values),
			onCancel: () => null,
			// confirmBtnText: "",
			// cancelBtnText: "",
		});
	};

	const changePointedValue = (value, setFieldValue, values) => {
		setFieldValue("pointed", value);

		if (value == false) {
			if (Array.isArray(values.categories)) {
				values.categories.forEach((category) => {
					if (Array.isArray(category.standards)) {
						category.standards.forEach((standard) => {
							delete standard.points;
							if (Array.isArray(standard.ratings)) {
								standard.ratings.forEach((rating) => {
									delete rating.score;
								});
							}
						});
					}
				});
			}
		}
	};

	const initialValues = {
		course_id: courseId,
		id: (data && data.data && data.data.data && data.data.data?.id) || null,
		title: (data && data.data && data.data.data && data.data.data?.title) || "",
		description: (data && data.data && data.data.data && data.data.data?.description) || "",
		is_published: (data && data.data && data.data.data && data.data.data?.is_published) || false,
		pointed: (data && data.data && data.data.data && data.data.data?.pointed) || true,
		total_points: (data && data.data && data.data.data && data.data.data?.total_points) || null,
		tags: (data && data.data && data.data.data && data.data.data?.tags) || [],
		categories: (data && data.data && data.data.data && data.data.data?.categories) || [],
		openDescriptionCollapse: (data && data.data && data.data.data && data.data.data?.description?.length > 0 ? true : false) || false,
	};

	const validationSchema = yup.object().shape({
		title: yup.string().required(tr`title is required`),
		categories: yup.array().of(
			yup.object().shape({
				subject: yup.string().required(tr`title is required`),
				standards: yup.array().of(
					yup.object().shape({
						subject: yup.string().required(tr`subject is required`),
						points: yup.number().when("pointed", {
							is: (pointed) => pointed == true,
							then: () => yup.number().required(tr`points is required`),
							otherwise: () => yup.number().nullable(),
						}),

						ratings: yup.array().of(
							yup.object().shape({
								title: yup.string().required(tr`title is required`),
								score: yup.number().when("pointed", {
									is: (pointed) => pointed == true,
									then: () => yup.number().required(tr`score is required`),
									otherwise: () => yup.number().nullable(),
								}),
							})
						),
					})
				),
			})
		),
	});

	const submitHandler = (values) => {
		rubricMutation.mutate({ data: values });
	};

	if (isLoading && rubricId != null) return <Loader />;

	return (
		<Formik
			validateOnChange={true}
			enableReinitialize={true}
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={submitHandler}
		>
			{({ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue, initialValues }) => {
				return (
					<RubricContext.Provider
						value={{
							values,
							touched,
							errors,
							isHovered,
							isStandardHovered,
							selectedCategories,
							previewMode,
							exportRubricAsPdfLoading,
							isViewer,

							handleChange,
							handleBlur,
							handleOpenStandardModal,
							handleSelectedCategories,
							handleRemoveCategoryFromSelectedCategory,
							handleExportRubricAsPdf,
							handleBackToRubricEditor,
							handleChangePointedValue,

							setFieldValue,
							setPreviewMode,
							actionsOnHover,
							actionsStandardOnHover,

							collapsesCategoryToggle,
							openedCollapses,
						}}
					>
						{previewMode ? (
							<GRubricViewer />
						) : (
							<Form onSubmit={handleSubmit}>
								<AppModal
									show={standardModal}
									parentHandleClose={handleCloseStandardModal}
									headerSort={<RStandard categoryData={categoryData} handleCloseStandardModal={handleCloseStandardModal} />}
								/>

								<RRubricEditor />

								<RButton
									type="submit"
									text={tr`save`}
									color="primary"
									loading={rubricMutation.isLoading}
									disabled={rubricMutation.isLoading}
								/>
							</Form>
						)}
					</RubricContext.Provider>
				);
			}}
		</Formik>
	);
};

export default GRubricEditor;
