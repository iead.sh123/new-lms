import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import SaveAnswersButtons from "./SaveAnswersButtons";
import RCkEditor from "components/Global/RComs/RCkEditor";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";
import styles from "../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const SolveEssay = ({ key, questionGroup, question, questionNumber, readMode = false, correctionMode = false }) => {
	const QuestionSetSolveData = useContext(QuestionSetSolveContext);
	const SelectedValue = QuestionSetSolveData.questionAnswers?.questionAnswers.find(
		(item) => +item.questionContextId == +question.module_context_question_id
	);
	return (
		<div key={key}>
			<Headers questionNumber={questionNumber} question={question} />
			<RFlex className="mb-3 mt-3">
				{readMode ? (
					<div className={styles.essay__read__mode + " text-muted"} dangerouslySetInnerHTML={{ __html: question?.answer }} />
				) : (
					<RCkEditor
						data={QuestionSetSolveData.submitted == "solving" ? SelectedValue?.answer : question?.answer}
						disabled={readMode}
						handleChange={(data) => {
							QuestionSetSolveData.handleFillDataToSolveQuestion({
								questionGroupId: questionGroup.id,
								groupContextId: questionGroup.module_context_group_id,
								questionContextId: question.module_context_question_id,
								type: question.type.name,
								key: "answer",
								value: data,
								isCorrect: true,
							});
						}}
					/>
				)}
			</RFlex>
			{!QuestionSetSolveData.correctOrReviewMode && <SaveAnswersButtons questionGroup={questionGroup} question={question} />}{" "}
			{correctionMode && <Feedbacks question={question} />}
		</div>
	);
};

export default SolveEssay;
