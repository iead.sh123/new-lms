// import RFlex from "components/Global/RComs/RFlex/RFlex";
// import { FormGroup, Label, Input } from "reactstrap";

// const AppCheckbox = (props) => {
// 	return (
// 		<FormGroup className="p-0 m-0" check>
// 			<Label check style={{fontSize:props.fontSize}}>
// 				<RFlex  >
// 					<Input type="checkbox" {...props} />
// 					<span className={"form-check-sign"} />
// 					<p className="mb-0">{props.label}</p>
// 				</RFlex>
// 			</Label>
// 		</FormGroup>
// 	);
// };

// export default AppCheckbox;

import RFlex from "components/Global/RComs/RFlex/RFlex";
import { FormGroup, Label, Input } from "reactstrap";

const AppCheckbox = (props) => {
	return (
		<FormGroup className={`p-0 m-0 ${props.formGroupClassname}`} check>
			<Label check>
				<RFlex>
					<Input type="checkbox" style={{ backgroundColor: "blue" }} {...props} />
					<span className={"form-check-sign"} style={{ marginTop: "0px" }} />
					{/* <p style={{ marginTop: "0px", marginBottom: "0px", ...props.paragraphStyle }}>{props.label}</p> */}
					<p style={{ marginTop: "10px", marginBottom: "0px" }}>{props.label}</p>
				</RFlex>
			</Label>
		</FormGroup>
	);
};

export default AppCheckbox;
