import { useEffect, useReducer } from "react";
import { useSelector } from "react-redux";

import {
  otherAssignmentInitialState,
  otherAssignmentReducer,
  TYPES,
} from "./otherAssignmentReducer";

export const otherUseAssignment = (mode, rabId) => {
  const { lessonGroups, isLoader, sebBrowserViolation } = useSelector(
    (state) => state.questionSets
  );

  const [otherAssignmentState, dispatch] = useReducer(
    otherAssignmentReducer,
    otherAssignmentInitialState
  );

  const { currentActiveGroup, currentActiveTopic, currentActiveQuestion } =
    otherAssignmentState;

  useEffect(() => {
    if (lessonGroups && lessonGroups?.inst_pattern_groups) {
      dispatch({
        type: TYPES.INITIALIZE_ASSIGNMENT,
        payload: {
          studentQsQuestionId: lessonGroups,
          studentQsPatternId: lessonGroups?.id,
        },
      });
    }
  }, [lessonGroups]);

  useEffect(() => {
    if (
      currentActiveQuestion > 0 ||
      currentActiveGroup > 0 ||
      currentActiveTopic > 0
    ) {
      dispatch({ type: TYPES.TOGGLE_PREVIOUS_BUTTON, payload: false });
    }
  }, [currentActiveQuestion, currentActiveTopic]);

  return {
    lessonGroups,
    isLoader,
    sebBrowserViolation,
    dispatch,
    otherAssignmentState,
    rabId,
  };
};
