import React, { useState } from "react";
import { get, destroy, post } from "config/api";
import tr from "components/Global/RComs/RTranslator";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

const GAddFromUsers = ({ handleAddFromUser, addFromUserLoading }) => {
	const { sIType, sITypeId, tabTitle } = useParams();

	const [processedRecords, setProcessedRecords] = useState([]);

	const getDataFromBackend = async (specific_url) => {
		let url;

		if (tabTitle == "principal") {
			url = `${Services.auth_organization_management.backend}api/principal/allUsersForAddAsPrincipal?education_stage_id=${sITypeId}`;
		} else if (tabTitle == "senior_teacher") {
			url = `${Services.auth_organization_management.backend}api/senior-teacher/allUsersForAddAsSeniorTeacher?curricula_id=${sITypeId}`;
		}

		let response = await get(
			specific_url
				? tabTitle == "principal"
					? `${specific_url}&education_stage_id=${sITypeId}`
					: tabTitle == "senior_teacher"
					? `${specific_url}&curricula_id=${sITypeId}`
					: url
				: url
		);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data.map((r) => ({
			id: r?.id,
			table_name: "addFromUserLister",
			details: [
				{
					key: tr`name`,
					value: r?.name,
				},
			],

			actions: [
				{
					name: tr`addUser`,
					icon: "fa fa-plus",
					color: "primary",
					outline: true,
					onClick: () => {
						handleAddFromUser(r.id);
					},
					disabled: addFromUserLoading,
					loading: addFromUserLoading,
				},
			],
		}));

		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response?.data?.data,
			},
		}),
		[processedRecords]
	);
	return <RAdvancedLister {...propsLiterals.listerProps} />;
};

export default GAddFromUsers;
