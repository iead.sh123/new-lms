import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { primaryColor } from "config/constants";
import { Row, Col, Input } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { dangerColor } from "config/constants";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

const Link = ({ viewMode, lessonContent, content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<>
			{viewMode ? (
				""
			) : (
				<>
					{lessonContent.newData && (
						<RFlex xs={12} style={{ color: primaryColor, cursor: "pointer" }} className="mb-3">
							<i className="fa fa-plus pr-2 pt-4" />
							<span onClick={() => CourseModuleData.handleAddLinksToLessonModule(content, lessonContent)}>{tr`add_new`}</span>
						</RFlex>
					)}
					{(lessonContent.links || [lessonContent.link]).map((link, index) => (
						<Row key={link?.id ? link?.id : link?.fakeId} className="mb-3 mt-3">
							<Col xs={12} sm={5}>
								<Input
									type="text"
									name="link_title"
									onChange={(event) => {
										lessonContent.newData
											? CourseModuleData.handleAddContentToLessonContent(
													content,
													lessonContent,
													event.target.name,
													event.target.value,
													"link",
													index
											  )
											: CourseModuleData.setEditLessonLinkState({
													...CourseModuleData.editLessonLinkState,
													link_title: event.target.value,
											  });
									}}
									placeholder={tr`link_title`}
									defaultValue={link?.link_title}
								/>
							</Col>
							<Col xs={12} sm={5}>
								<Input
									type="text"
									name="link_url"
									onChange={(event) => {
										lessonContent.newData
											? CourseModuleData.handleAddContentToLessonContent(
													content,
													lessonContent,
													event.target.name,
													event.target.value,
													"link",
													index
											  )
											: CourseModuleData.setEditLessonLinkState({
													...CourseModuleData.editLessonLinkState,
													link_url: event.target.value,
											  });
									}}
									placeholder={tr`link_url`}
									defaultValue={link?.link_url}
								/>
							</Col>

							{lessonContent.newData && (
								<Col xs={12} sm={2}>
									<i
										className={iconsFa6.delete + " pt-2"}
										style={{ color: dangerColor, cursor: "pointer" }}
										onClick={(event) => {
											CourseModuleData.handleRemoveLinkFromLessonContent(content, lessonContent, link?.fakeId);
										}}
									/>
								</Col>
							)}
						</Row>
					))}
					<Row>
						<Col xs={12} className="d-flex justify-content-end">
							{/* <RButton
								onClick={() =>
									lessonContent.newData
										? CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, true, "link")
										: CourseModuleData.handleEditLessonContent(
												CourseModuleData.specificModule.id,
												content.id,
												lessonContent.id,
												lessonContent.type
										  )
								}
								color="primary"
								text={tr`save_and_publish`}
								outline
								loading={CourseModuleData.lessonLinkLoading}
								disabled={CourseModuleData.lessonLinkLoading || lessonContent?.links?.length == 0}
							/> */}
							<RButton
								onClick={() =>
									lessonContent.newData
										? CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, false, "link")
										: CourseModuleData.handleEditLessonContent(
												CourseModuleData.specificModule.id,
												content.id,
												lessonContent.id,
												lessonContent.type
										  )
								}
								color="success"
								text={tr`save`}
								loading={CourseModuleData.lessonLinkLoading}
								disabled={CourseModuleData.lessonLinkLoading || lessonContent?.links?.length == 0}
							/>
						</Col>
					</Row>
				</>
			)}
		</>
	);
};

export default Link;
