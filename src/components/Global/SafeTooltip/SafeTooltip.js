import React from "react";
import { UncontrolledTooltip } from "reactstrap";

class SafeTooltip extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: "" };
  }

  componentDidCatch(error) {
    this.setState({ error: `${error.name}: ${error.message}` });
  }

  render() {
    const { error } = this.state;
    if (error) {
      return <></>;
    } else {
      return (
        <UncontrolledTooltip {...this.props}>
          {" "}
          {this.props.children}{" "}
        </UncontrolledTooltip>
      );
    }
  }
}

export default SafeTooltip;
