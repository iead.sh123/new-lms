import React, { useContext, useState } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { termsManagementApi } from "api/global/termsManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GCoursesFromSchoolCatalog = () => {
	const TermsManagementData = useContext(TermsManagementContext);
	const [search, setSearch] = useState("");

	const initialValues = {
		name: "",
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	const coursesFromSchoolCatalogData = useFetchDataRQ({
		queryKey: ["coursesFromSchoolCatalog", search],
		queryFn: () =>
			termsManagementApi.getCoursesFromSchoolCatalog({
				termId: null,
				gradeLevelId: TermsManagementData.itemObj.gradeLevelId,
				searchText: search,
			}),
	});

	const _records = React.useMemo(
		() =>
			coursesFromSchoolCatalogData?.data?.data?.data?.data?.map((course) => ({
				details: [
					{ key: tr`course_name`, value: course.name },
					{ key: tr`course_code`, value: course?.code ?? "-" },
					{ key: tr`credit`, value: course.credits },
				],
				actions: [
					{
						name: " ",
						icon: iconsFa6.plus,
						color: "primary",
						outline: true,
						loading: TermsManagementData.makeCourseClonedLoading,
						onClick: () =>
							TermsManagementData.handleMakeCourseCloned({
								courseId: course.id,
								termId: TermsManagementData?.itemObj?.termId,
								curriculumId: course.curriculum_id,
							}),
					},
				],
			})),
		[coursesFromSchoolCatalogData?.data?.data?.data?.data]
	);

	//Actions
	const handleSearch = (emptyData) => {
		// setFieldValue("name", emptyData !== undefined ? "" : values.name);
		setSearch(emptyData !== undefined ? "" : values.name);
	};

	if (coursesFromSchoolCatalogData.isError) {
		return <REmptyData />;
	}

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<RSearchHeader
					searchLoading={coursesFromSchoolCatalogData.isLoading}
					searchData={values.name}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("name", value)}
					handleChangeSearch={(value) => setFieldValue("name", value)}
				/>
			</RFlex>
			{coursesFromSchoolCatalogData.isLoading ? <Loader /> : <RLister Records={_records} />}
		</RFlex>
	);
};

export default GCoursesFromSchoolCatalog;
