import { get, post, patch } from "config/api";
import { Services } from "engine/services";

export const learningObjects = {
	getLearningObjectById: (id, type) => get(`${Services.scorm.backend}api/learning-object/${type}/${id}`),

	getLearningObjectAvailable: (type) => get(`${Services.scorm.backend}api/learning-objects/available/${type}`),

	editLessonObject: (id, data) => patch(`${Services.scorm.backend}api/learning-objects/${id}`, data),

	getStateMachineById: (learningObjectId) => get(`${Services.scorm.backend}api/learning-objects/${learningObjectId}/state-machine`),

	getStateMachineComponents: (type) =>
		get(
			`${
				type == "sections"
					? Services.courses_manager.backend
					: type === "lessons"
					? Services.lesson.backend
					: Services.lesson_content.backend
			}api/learningobjects/statemachine/components`
		),

	addStateMachine: (learningObjectId, data) =>
		post(`${Services.scorm.backend}api/learning-objects/${learningObjectId}/state-machine`, data),

	editStateMachine: (stateMachineId, data) => patch(`${Services.scorm.backend}api/state-machines/${stateMachineId}`, data),

	getLearningObjectMyProgress: (learningObjectId) => get(`${Services.scorm.backend}api/learning-objects/${learningObjectId}/my-progress`),

	userInteraction: (objectId, data) => post(`${Services.scorm.backend}api/learning-objects/${objectId}/interactions`, data),
};
