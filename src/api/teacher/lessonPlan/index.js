import { get, post, destroy } from "config/api";

import { Services } from "engine/services";

export const lessonPlanApi = {
  getLessonPlan: (lessonId) =>
    get(
      `${Services.lesson_plan.backend}api/v1/lesson-plan/getLessonPlanByLessonId/${lessonId}`
    ),

  storeLessonLessonPlan: (data) =>
    post(`${Services.lesson_plan.backend}api/v1/lesson-plan/store`, data),

  getLessonPlanPrinting: (lessonPlanId) =>
    get(`${Services.lesson_plan.print}api/v1/print/${lessonPlanId}`),

  importLessonPlan: (data) =>
    post(`${Services.lesson_plan.backend}api/v1/lesson-plan/import`, data),

  lessonPlanByLessonPlanId: (lessonPlanId) =>
    get(
      `${Services.lesson_plan.backend}api/v1/lesson-plan/getLessonPlan/${lessonPlanId}`
    ),

  exportLessonPlanAsPdf: (lessonPlanId) =>
    get(`${Services.lesson_plan.backend}api/v1/print/${lessonPlanId}`, {
      responseType: "blob",
    }),
};
