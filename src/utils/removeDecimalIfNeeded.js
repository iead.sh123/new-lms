function removeDecimalIfNeeded(number) {
	let strNumber = number.toString();

	if (strNumber.includes(".") && strNumber.split(".")[1] === "00") {
		strNumber = strNumber.split(".")[0];
	}

	return strNumber;
}
export default removeDecimalIfNeeded;
