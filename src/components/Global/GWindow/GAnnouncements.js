import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  FetchAnnouncementsAsync,
  RemoveAnnouncementAsync,
  setFlag,
} from "store/actions/admin/schoolEvents";
import { setTitle } from "store/actions/student/navActions";
import InfiniteScroll from "react-infinite-scroll-component";
import RAnnouncement from "../RComs/RAnnouncement/RAnnouncement";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";
import { trackLastSeenAsync } from "store/actions/global/track.action";

const GAnnouncements = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state?.auth);

  const {
    announcements,
    loadMorePage,
    announcementsLoading,
    flagAnnouncement,
  } = useSelector((state) => state.SchoolEventsRed);

  useEffect(() => {
    dispatch(setFlag(false));
    dispatch(setTitle(tr("Announcements")));
    dispatch(RemoveAnnouncementAsync());
    dispatch(FetchAnnouncementsAsync(1));
  }, []);

  const fetchData = () => {
    loadMorePage !== null && dispatch(FetchAnnouncementsAsync(loadMorePage));
  };

  useEffect(() => {
    dispatch(
      trackLastSeenAsync({ payload: { trackable_type: "announcements" } })
    );
  }, [announcements]);

  return (
    <div className="content">
      <div
        id="scrollableDiv2"
        className={
          (user?.type == "parent" || user?.type == "student") &&
          !user?.useNewTheme
            ? "container"
            : "content"
        }
        style={{
          paddingTop:
            (user?.type == "parent" || user?.type == "student") &&
            !user?.useNewTheme
              ? "25vh"
              : "",
          height:
            user?.type === "school_advisor" ||
            user?.type === "seniorteacher" ||
            user?.type === "principal" ||
            user?.type === "employee" ||
            user?.type === "teacher"
              ? "80vh"
              : "",

          overflow:
            user?.type === "school_advisor" ||
            user?.type === "seniorteacher" ||
            user?.type === "principal" ||
            user?.type === "employee" ||
            user?.type === "teacher"
              ? "auto"
              : "",
        }}
      >
        {user?.type === "student" || user?.type === "parent" ? (
          <InfiniteScroll
            dataLength={announcements?.length}
            next={fetchData}
            hasMore={loadMorePage !== null ? true : false}
            loader={<Loader />}
            style={{ overflow: "hidden" }}
            // endMessage={
            //   <h4 style={{ textAlign: "center" }}>{tr`no_data_to_show`}</h4>
            // }
          >
            <RAnnouncement Announcements={announcements} />
          </InfiniteScroll>
        ) : (
          <InfiniteScroll
            dataLength={announcements?.length}
            next={fetchData}
            hasMore={loadMorePage !== null ? true : false}
            loader={<Loader />}
            style={{ overflow: "hidden" }}
            // endMessage={
            //   <h4 style={{ textAlign: "center" }}>{tr`no_data_to_show`}</h4>
            // }
            scrollableTarget="scrollableDiv2"
          >
            <RAnnouncement
              Announcements={announcements}
              flag={flagAnnouncement}
            />
          </InfiniteScroll>
        )}
      </div>
    </div>
  );
};

export default GAnnouncements;
