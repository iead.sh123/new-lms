import React, { useState } from "react";
import { Row, Col, Form, FormGroup, Input, Label } from "reactstrap";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";

const RUnitPlanAddItem = ({ handleAddItemsToSection, handleCloseAddItem, openAddItem, sectionId, itemId }) => {
	const [text, setText] = useState("");

	return (
		<AppModal
			show={openAddItem}
			parentHandleClose={handleCloseAddItem}
			header={tr`create_new_item`}
			size={"lg"}
			headerSort={
				<Form>
					<Row className="mb-3">
						<Col lg={12} xs={12} className="pl-0 ">
							<Label className="text-muted">{tr`Item name`}</Label>
							<FormGroup>
								<Input
									id="text"
									required
									type="text"
									//   value={text}
									onChange={(event) => setText(event.target.value)}
								/>
							</FormGroup>
						</Col>
					</Row>
				</Form>
			}
			footer={
				<>
					<RButton
						text={tr`create`}
						onClick={() => {
							handleAddItemsToSection(sectionId, itemId, "text", text, null, false);
							handleCloseAddItem();
						}}
						color="primary"
					/>
					<RButton text={tr`cancel`} onClick={handleCloseAddItem} color="primary" outline type="button" />
				</>
			}
		/>
	);
};

export default RUnitPlanAddItem;
