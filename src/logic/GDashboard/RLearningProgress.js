import React, { useEffect, useState } from "react";
import RTimeLine from "./RTimeLine";
import RBieChart from "components/Global/RComs/RBieChart";



const RLearningProgress = ({}) => {


 const transformedconsumerConsumedContents=[
    {text:"h",count:1},
    {text:"g",count:2},
    {text:"k",count:3},
    {text:"l",count:4},  
]

 

  return <>LearningProgress
   <RBieChart data={transformedconsumerConsumedContents} color={"#007bff"} label={"progress"}/>
  </>
};

export default RLearningProgress;
