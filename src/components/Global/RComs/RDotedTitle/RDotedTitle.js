import React from "react";
import RFlex from "../RFlex/RFlex";
import styles from "./RDotedTitle.module.scss";
import tr from "../RTranslator";

const RDotedTitle = ({ title, icon = "fa fa-info" }) => {
	return (
		<RFlex>
			<i className={`${icon} ${styles.icon}`} />
			<h6 className={styles.title}>{title}</h6>
			<div className={styles.border}></div>
		</RFlex>
	);
};

export default RDotedTitle;
