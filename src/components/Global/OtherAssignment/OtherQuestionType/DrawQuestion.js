import React, { useEffect, useRef, useState } from "react";
import DrawingPanel from "react-canvas-draw";

import lzString from "lz-string";

const DrawQuestion = ({ assignmentReadMode, student }) => {
  return (
    <DrawingPanel
      lang={"ar"}
      canvasWidth={600}
      canvasHeight={600}
      brushColor={"black"}
      saveData={
        student?.detail?.notSolved === 1
          ? ""
          : lzString.decompressFromUTF16(student?.detail?.draw)
      }
      brushRadius={6}
      disabled={assignmentReadMode}
    />
  );
};

export default DrawQuestion;
