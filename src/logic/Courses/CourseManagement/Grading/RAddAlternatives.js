import React, { useContext, useState } from 'react'
import { GradingContext } from './GGradingManagement'
import Loader from 'utils/Loader'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { baseURL, genericPath } from 'engine/config'
import RLister from 'components/Global/RComs/RLister'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import iconsFa6 from 'variables/iconsFa6'
import tr from 'components/Global/RComs/RTranslator'
import { tablesTypes } from './Constants'
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader'
import * as colors from 'config/constants'
const RAddAlternatives = ({ item }) => {
  // const gradingData = useContext(GradingContext)
  // if (gradingData.isLoadingAlternatives)
  //   return <Loader />
  const gradingData = useContext(GradingContext)

  const [searchData, setSearchData] = useState("");
  const [idsToHide, setIdsToHide] = useState([])
  const handleSearch = (clearData) => {
    let data = searchData
    if (clearData == "") {
      data = clearData
    }
    gradingData.setAlternativeSearchTerm(data)
  };
  const wantedColumns = ['Type', 'Name', 'Points']
  console.log("RAddAlternatives", item)
  const gradableItems = gradingData?.gradingState?.filteredAlternatives
  console.log('RAddAlternatives', gradableItems)

  const _alternativesgradableItemsRecords =
    gradableItems?.ids?.map((recordId) => {
      const currentRecord = gradableItems?.[recordId]
      console.log("currentRecord", currentRecord)
      return {
        id: recordId,
        table_name: tablesTypes.Alternatives,
        details: Object.keys(currentRecord)?.filter((header) => wantedColumns.includes(header))?.map((header) => {
          const currentHeader = currentRecord?.[header]
          return {
            key: header, value: currentHeader.value,
          }
        }),
        actions: [
          {
            name: " ",
            icon: iconsFa6.plus,
            color: "primary",
            outline: true,
            onClick: () => { gradingData.handleAddingAlternativeToItem(item.id, item.type, recordId, currentRecord['Type'].value); setIdsToHide([...idsToHide, recordId]) },
          },
        ]
      }
    })
  return (
    <RFlex className="flex-column" styleProps={{ gap: '15px' }}>
      <RFlex className="flex-column">
        <p className='p-0 m-0' style={{ fontSize: '16px' }}>{tr("Alternatives_To")}: {item?.name}</p>
        <RFlex styleProps={{ gap: "15px" }} className='align-items-baseline'>
          <RSearchHeader
            // searchLoading={courses.loading}
            searchData={searchData}
            handleSearch={handleSearch}
            setSearchData={setSearchData}
            handleChangeSearch={(value) => setSearchData(value)}
            inputWidth="320px"
          />
          {/* <RButton
            text={tr("Save")}
            // disabled={selectedUsers <= 0}
            color="link"
            className="text-primary m-0 p-0"
            style={{ width: "fit-content", color: colors.primaryColor, textDecoration: "underline", cursor: "pointer" }}
          // onClick={handleSaveUsers}
          /> */}
        </RFlex>
      </RFlex>
      <RLister Records={_alternativesgradableItemsRecords} withPagination={true} info={gradingData.alternativesData} handleChangePage={gradingData.handleAlternativesPageChange} page={gradingData.alternativesPage} />
    </RFlex>
  )
}

export default RAddAlternatives