import React from "react";
import styles from "./RLessonPlanViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { TransformedTags } from "utils/TransformedTags";

const RLessonPlanMainData = ({ lessonPlan, width }) => {
	return (
		<section className={styles.lp__viewer__main__data} style={{ width: width ? width : "" }}>
			<section className={styles.lp__viewer__main__data__container}>
				<RFlex>
					<span className="font-weight-bold"> {tr`title`}</span>: {lessonPlan?.name}
				</RFlex>
				<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
					{lessonPlan?.unit_nb && (
						<span>
							<span className="font-weight-bold">{tr`unit_number`}</span>: {lessonPlan?.unit_nb}
						</span>
					)}
					{lessonPlan?.lesson_nb && (
						<span>
							<span className="font-weight-bold">{tr`lesson_number`}</span>: {lessonPlan?.lesson_nb}
						</span>
					)}
				</RFlex>
				{lessonPlan?.tags?.length > 0 && (
					<RFlex styleProps={{ alignItems: "center", width: "100%" }}>
						<span className="font-weight-bold">{tr`tags`}</span>:
						<TransformedTags tags={lessonPlan?.tags} />
					</RFlex>
				)}
			</section>
		</section>
	);
};

export default RLessonPlanMainData;
