import { IMApi } from "api/IMNewCycle";
import RResourceViewer from "components/Global/RComs/RResourceViewer/RResourceViewer";
import React from "react";
import styles from "./GroupManagement.module.scss";
import RFile from "components/Global/RComs/RResourceViewer/RFile";
import Loader from "utils/Loader";
import { Services } from "engine/services";
import REmptyData from "components/RComponents/REmptyData";

const ChatGallery = ({ chatId, handleGoBack }) => {
	const [resources, setResources] = React.useState([]);
	const [nextPage, setNextPage] = React.useState(null);
	const [loadingMore, setLoadingMore] = React.useState(false);
	const [showViewer, setShowViewer] = React.useState(false);

	const getResources = async (nextP) => {
		setLoadingMore(true);

		try {
			const response = await IMApi.chatGallery(chatId, nextP);
			if (!response.data.status) throw new Error(response.data.msg);
			setResources([...resources, ...response.data?.data?.data?.data]);
			setNextPage(+response.data?.data?.data?.next_page_url?.split("page=")?.[1]?.split("&")?.[0]);
			setLoadingMore(false);
		} catch (err) {
			setLoadingMore(false);
		}
	};

	const loadMore = async (e) => {
		if (e?.target.scrollHeight - e?.target.scrollTop !== e?.target.clientHeight || loadingMore || !nextPage) return;

		setLoadingMore(true);
		await getResources(nextPage);
		setLoadingMore(false);
	};

	React.useEffect(() => {
		getResources();
	}, []);

	return (
		<>
			{resources.length == 0 ? (
				<div
					style={{
						display: "flex",
						justifyContent: "center",
						alignItems: "center",
						height: "60vh",
					}}
				>
					<REmptyData />
				</div>
			) : (
				<div onScroll={loadMore} style={{ overflowY: "scroll", height: "70vh" }}>
					{loadingMore ? (
						<Loader />
					) : (
						<div className={styles["Gallery"]}>
							{/* {"resources="+resources.length} */}
							{resources.map((rc, ind) => {
								const url1 = Services.storage.file + (rc?.thumbUrl ?? rc?.hash_id);

								const image_extenstions = ["jpg", "jpeg", "png", "bmp"];
								var ext = url1.substr(url1.lastIndexOf(".") + 1);

								const image = image_extenstions.includes(ext);
								return (
									<div onClick={() => setShowViewer("" + ind)} key={"chat-gallery-" + rc.id} className={styles["GalleryImage"]}>
										{/* {image ? (
                      <div
                        style={{
                          width: "100px",
                          height: "100px",
                          background: `url(${url1})`,
                          backgroundSize: "cover",
                          backgroundRepeat: "unset",
                        }}
                      ></div>
                    ) : ( */}
										<RFile src={Services.storage.file + rc?.hash_id} mimeType={rc?.mime_type} />
										{/* )} */}
									</div>
								);
							})}

							{showViewer && (
								<RResourceViewer
									resources={resources}
									sourceUrl={Services.storage.file}
									onHideCallBack={() => setShowViewer(null)}
									selectedIndex={+showViewer}
									modalMode
								/>
							)}
						</div>
					)}
				</div>
			)}
		</>
	);
};

export default ChatGallery;
