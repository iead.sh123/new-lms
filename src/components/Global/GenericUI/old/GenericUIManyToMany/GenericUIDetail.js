import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Modal from "react-bootstrap/Modal";
import { translate } from "../../../../utils/translate";

import { toast } from "react-toastify";
import {
  getTableData,
  storeOneToMany,
  storeManyToMany,
} from "../../../../store/actions/genericUIHasManyActions";

const GenericUIDetail = ({ masterTable, detailTable, pivotTable }) => {
  const detail = useSelector((state) => state.genericUIHasManyReducer.detail);
  const props = useSelector((state) => state.genericUIHasManyReducer.props);
  const detailTableTitle = useSelector(
    (state) => state.genericUIHasManyReducer.detailTableTitle
  );
  const pivot = useSelector((state) => state.genericUIHasManyReducer.pivot);
  const selectedMaster = useSelector(
    (state) => state.genericUIHasManyReducer.selectedMaster
  );

  const [detailId, setDetailId] = useState(null);

  const [keyword, setKeyword] = useState(null);

  const [show, setShow] = useState(false);

  const [itemProps, setItemProps] = useState({});

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const modalChangeChecked = (e, field) => {
    let temp = itemProps;
    temp[field.Field] = e.target.checked;
    setItemProps(temp);
  };

  const modalChangeDDL = (e, field) => {
    let temp = itemProps;
    temp[field.Field] = e.target.value;
    setItemProps(temp);
  };

  const handleSave = () => {
    let request = {
      tableName: pivotTable,
      masterTable: masterTable,
      masterId: selectedMaster.id,
      detailTable: detailTable,
      detailId: detailId,
      item: itemProps,
    };
    dispatch(storeManyToMany(request));
    setShow(false);
  };

  const dispatch = useDispatch();
  const changeValue = (e) => {
    setKeyword(e.target.value);
  };

  const handleSearch = () => {
    dispatch(getTableData(detailTable, "detail", keyword));
  };

  const handleAddClicked = (line) => {
    if (!selectedMaster) {
      toast.error(translate("Select A Master First"));
    } else {
      let found = false;
      if (pivotTable == detailTable) {
        pivot.map((item) => {
          if (item.id == line.id) {
            found = true;
            return;
          }
        });
      } else {
        pivot.map((item) => {
          if (item.name == line.name) {
            found = true;
            return;
          } else if (item.name.includes("Securable")) {
            let startIndex = item.name.indexOf("Securable");
            if (startIndex != -1) {
              if (item.name.substring(startIndex).includes(line.name)) {
                found = true;
                return;
              }
            }
          }
        });
      }

      if (found) {
        toast.error(translate("Added Already !"));
      } else {
        if (pivotTable == detailTable) {
          let request = {
            tableName: pivotTable,
            id: line.id,
            masterTable: masterTable,
            value: selectedMaster.id,
          };
          dispatch(storeOneToMany(request));
        } else {
          if (props.length == 0) {
            let request = {
              tableName: pivotTable,
              masterTable: masterTable,
              masterId: selectedMaster.id,
              detailTable: detailTable,
              detailId: line.id,
              item: null,
            };
            dispatch(storeManyToMany(request));
          } else {
            setDetailId(line.id);
            let temp = itemProps;
            props.map((item) => {
              if (item.Type == "tinyint(1)" || item.Type == "tinyint(4)") {
                temp[item.Field] = false;
              } else {
                temp[item.Field] = null;
              }
            });
            handleShow();
          }
        }
      }
    }
  };

  return (
    <div className="card mb-4">
      <div className="row text-center">
        <h5
          style={{
            margin: "5px auto",
          }}
        >
          {detailTableTitle}
        </h5>
        <input
          id="keywordDetail"
          value={keyword ? keyword : ""}
          className="form-control col-6"
          type="text"
          placeholder={translate("Search")}
          aria-label="Search"
          onChange={changeValue}
        />
        <button className="btn Sebtn" name="btnSearch" onClick={handleSearch}>
          <i className="fas fa-search"></i>{" "}
        </button>
      </div>
      <div className="card-header header-title"></div>
      <table id="main-table" className="table table-bordered main-table">
        <tbody>
          {detail?.map((line, index) => {
            return (
              <tr key={line.id}>
                <td scope="col">
                  <h5>{line.name}</h5>
                </td>
                <td scope="col">
                  <button
                    id={line.id}
                    className="btn btn-danger"
                    onClick={(e) => {
                      handleAddClicked(line);
                    }}
                  >
                    +
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Modal show={show} onHide={handleClose} keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>Add</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props?.map((field) => {
            return (
              <div className="row">
                {
                  {
                    "tinyint(4)": (
                      <div className="row alineCenter">
                        <div className="col-md-4 flex-endAline">
                          <label htmlFor={field.Field}>{field.Title}</label>
                        </div>
                        <div className="col-md-8 flex-startAline">
                          <input
                            name={field.Field}
                            id={field.Field}
                            type="checkbox"
                            className="form-control-checkbox"
                            onChange={(e) => modalChangeChecked(e, field)}
                          />
                        </div>
                      </div>
                    ),
                    "tinyint(1)": (
                      <div className="row alineCenter">
                        <div className="col-md-4 flex-endAline">
                          <label htmlFor={field.Field}>{field.Title}</label>
                        </div>
                        <div className="col-md-8 flex-startAline">
                          <input
                            name={field.Field}
                            id={field.Field}
                            type="checkbox"
                            className="form-control-checkbox"
                            onChange={(e) => modalChangeChecked(e, field)}
                          />
                        </div>
                      </div>
                    ),
                    "bigint(20) unsigned": (
                      <div className="row alineCenter">
                        <div className="col-md-4 flex-endAline">
                          <label htmlFor={field.Field}>{field.Title}</label>
                        </div>
                        <div className="col-md-8 flex-startAline">
                          <select
                            id={"ddl_" + field.Field}
                            className="form-control"
                            value={itemProps[field.Field]}
                            onChange={(e) => modalChangeDDL(e, field)}
                          >
                            {field.data?.length > 0
                              ? field.data?.map((dataRow) => {
                                  return (
                                    <option
                                      key={dataRow.ID}
                                      value={dataRow.ID}
                                      className="form-control"
                                      name={Object.entries(dataRow)[1]}
                                    >
                                      {Object.entries(dataRow)[1][1]}
                                    </option>
                                  );
                                })
                              : null}
                          </select>
                        </div>
                      </div>
                    ),
                  }[field.Type]
                }
              </div>
            );
          })}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-secondary" onClick={handleClose}>
            Close
          </button>
          <button className="btn btn-primary" onClick={handleSave}>
            Save
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default GenericUIDetail;
