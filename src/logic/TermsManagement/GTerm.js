import React, { useContext } from "react";
import { FormGroup, Input, FormText } from "reactstrap";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { useFormik } from "formik";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";
import moment from "moment";
import styles from "./TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

const GTerm = () => {
	const TermsManagementData = useContext(TermsManagementContext);
	const initialValues = {
		name: TermsManagementData.itemObj?.dataObj?.name || "",
		startDate: TermsManagementData.itemObj?.dataObj?.startDate || "",
		endDate: TermsManagementData.itemObj?.dataObj?.endDate || "",
	};

	const formSchema = yup.object().shape({
		name: yup.string().required(tr`name is required`),
		// startDate: yup
		// 	.string()
		// 	.required("Start date is required")
		// 	.test({
		// 		name: "start-date-validation",
		// 		message: "Start date must be before or equal to end date and should not be earlier than today's date",
		// 		test: function (startDate) {
		// 			const currentDate = new Date();
		// 			const selectedDate = new Date(startDate);
		// 			return selectedDate >= currentDate;
		// 		},
		// 	}),
		// endDate: yup
		// 	.string()
		// 	.required("End date is required")
		// 	.test({
		// 		name: "end-date-validation",
		// 		message: "End date must be after or equal to start date and should not be earlier than today's date",
		// 		test: function (endDate) {
		// 			const currentDate = new Date();
		// 			const selectedDate = new Date(endDate);
		// 			return selectedDate >= currentDate;
		// 		},
		// 	}),
		startDate: yup
			.string()
			.required("Start date is required")
			.test({
				name: "start-date-validation",
				message: "Start date should not be after end date or after today's date",
				test: function (startDate) {
					const endDate = this.parent.endDate;
					const currentDate = new Date();

					if (!startDate) return true;

					const selectedStartDate = new Date(startDate);
					const selectedEndDate = new Date(endDate);

					return selectedStartDate <= selectedEndDate && selectedStartDate >= currentDate;
				},
			}),
		endDate: yup
			.string()
			.required("End date is required")
			.test({
				name: "end-date-validation",
				message: "End date should not be before start date or after today's date",
				test: function (endDate) {
					const startDate = this.parent.startDate;
					const currentDate = new Date();

					if (!endDate) return true;

					const selectedStartDate = new Date(startDate);
					const selectedEndDate = new Date(endDate);

					return selectedEndDate >= selectedStartDate && selectedEndDate >= currentDate;
				},
			}),
	});

	const handleFormSubmit = (values) => {
		if (TermsManagementData.itemObj?.dataObj?.id) {
			TermsManagementData.handleEditTerm(values, TermsManagementData.itemObj?.dataObj?.id);
		} else {
			TermsManagementData.handleAddTerm(values, TermsManagementData.itemObj?.academicYearId);
		}
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};
	return (
		<form onSubmit={handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", gap: "15px" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<div className={styles.icon}>
						<i className={iconsFa6.userPlus + " fa-lg"} style={{ color: "white" }} />
					</div>
					<span>{TermsManagementData.itemObj?.dataObj?.id ? tr`edit_term` : tr`create_new_term`}</span>
				</RFlex>
				<RFlex styleProps={{ width: "100%" }}>
					<FormGroup style={{ width: "100%" }}>
						<label>{tr("term")}</label>
						<Input
							name="name"
							type="text"
							value={values.name}
							placeholder={tr("term")}
							onBlur={handleBlur}
							onChange={handleChange}
							className={getInputClass(touched, errors, "name")}
						/>
						{touched.name && errors.name && <FormText color="danger">{errors.name}</FormText>}
					</FormGroup>
				</RFlex>
				<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("start_date")}</label>
						<Input
							name="startDate"
							type="date"
							value={moment(values.startDate).format("YYYY-MM-DD")}
							placeholder={tr("start_date")}
							onBlur={handleBlur}
							onChange={handleChange}
							className={getInputClass(touched, errors, "startDate")}
						/>
						{touched.startDate && errors.startDate && <FormText color="danger">{errors.startDate}</FormText>}
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("end_date")}</label>
						<Input
							name="endDate"
							type="date"
							value={moment(values.endDate).format("YYYY-MM-DD")}
							placeholder={tr("end_date")}
							onBlur={handleBlur}
							onChange={handleChange}
							className={getInputClass(touched, errors, "endDate")}
						/>
						{touched.endDate && errors.endDate && <FormText color="danger">{errors.endDate}</FormText>}
					</FormGroup>
				</RFlex>
				<RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
					<RButton
						type="submit"
						text={TermsManagementData.itemObj?.dataObj?.id ? tr`save` : tr`create`}
						color="primary"
						loading={TermsManagementData.itemObj?.dataObj?.id ? TermsManagementData.editTermLoading : TermsManagementData.addTermLoading}
						disabled={TermsManagementData.itemObj?.dataObj?.id ? TermsManagementData.editTermLoading : TermsManagementData.addTermLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						onClick={() => {
							TermsManagementData.handleCloseTermModal();
							resetForm();
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default GTerm;
