import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../RSchoolInitiation.module.scss";
import RSchoolInitiationButtons from "./RSchoolInitiationButtons";

const RGradeLevelForm = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { tabTitle } = useParams();

  return (
    <Form className={tabTitle == "properties" ? styles.add_form : ""}>
      <Row>
        <Col xs={6}>
          <RFlex>
            <label>{tr("title")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="title"
              type="text"
              placeholder={tr`title`}
              value={SchoolInitiationData.addGradeLevels.title}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>

        {/* <Col xs={6}>
          <RFlex>
            <label>{tr("name")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={SchoolInitiationData.appGradeLevels}
              closeMenuOnSelect={true}
              placeholder={tr`name`}
              value={SchoolInitiationData.addGradeLevels.app_grade_level}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  "app_grade_level",
                  event
                );
              }}
              isLoading={SchoolInitiationData.appGradeLevelsLoading}
            />
          </FormGroup>
        </Col> */}
        <Col xs={6}>
          <RFlex>
            <label>{tr("code")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="code"
              type="string"
              placeholder={tr`code`}
              value={SchoolInitiationData.addGradeLevels.code}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>

        <Col xs={12}>
          <RFlex>
            <label>{tr("order")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="grade_level_order"
              type="number"
              placeholder={tr`order`}
              value={SchoolInitiationData.addGradeLevels.order}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>

        <Col xs={6}>
          <RFlex>
            <label>{tr("education_stage")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={SchoolInitiationData.educationStage}
              closeMenuOnSelect={true}
              placeholder={tr`education_stage`}
              value={SchoolInitiationData.addGradeLevels.education_stage}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  "education_stage",
                  event
                );
              }}
              isLoading={SchoolInitiationData.educationStageLoading}
            />
          </FormGroup>
        </Col>

        <Col xs={6}>
          <RFlex>
            <label>{tr("grade_level_scale")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={SchoolInitiationData.gradeScales}
              closeMenuOnSelect={true}
              placeholder={tr`grade_level_scale`}
              value={SchoolInitiationData.addGradeLevels.grade_scale}
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  "grade_scale",
                  event
                );
              }}
              isLoading={SchoolInitiationData.gradeScalesLoading}
            />
          </FormGroup>
        </Col>

        <Col xs={6}>
          <RFlex>
            <label>{tr("assessment_plan")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={[]}
              closeMenuOnSelect={true}
              placeholder={tr`assessment_plan`}
              defaultValue={
                SchoolInitiationData.addGradeLevels.assessment_plan_id
              }
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  "assessment_plan_id",
                  event.value
                );
              }}
              isLoading={false}
            />
          </FormGroup>
        </Col>

        <Col xs={6}>
          <RFlex>
            <label>{tr("report_card_template")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={[]}
              closeMenuOnSelect={true}
              placeholder={tr`report_card_template`}
              defaultValue={
                SchoolInitiationData.addGradeLevels.report_card_template_id
              }
              onChange={(event) => {
                SchoolInitiationData.handleChangeGradeLevels(
                  "report_card_template_id",
                  event.value
                );
              }}
              isLoading={false}
            />
          </FormGroup>
        </Col>
      </Row>
      {tabTitle && <RSchoolInitiationButtons />}
    </Form>
  );
};

export default RGradeLevelForm;
