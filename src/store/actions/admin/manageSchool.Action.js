import {
	GET_GRADES_LEVELS_REQUEST,
	GET_GRADES_LEVELS,
	GET_GRADES_LEVELS_ERROR,
	GET_USER_BY_ID_REQUEST,
	GET_USER_BY_ID,
	GET_USER_BY_ID_ERROR,
	GET_USERS_FROM_USER_REQUEST,
	GET_USERS_FROM_USER_SUCCESS,
	GET_USERS_FROM_USER_ERROR,
	CHECK_USER,
	SAVE_USERS_FROM_USER_REQUEST,
	SAVE_USERS_FROM_USER_SUCCESS,
	SAVE_USERS_FROM_USER_ERROR,
	GET_ATTENDANCE_REPORTS_REQUEST,
	GET_ATTENDANCE_REPORTS_SUCCESS,
	GET_ATTENDANCE_REPORTS_ERROR,
	EMPTY_ATTENDANCE_REPORT,
	GET_SYNCHRONIZE_ACTION_REQUEST,
	GET_SYNCHRONIZE_ACTION_SUCCESS,
	GET_SYNCHRONIZE_ACTION_ERROR,
	SELECT_SCHOOL_CLASS_SOURCE,
	SELECT_COURSES_SOURCE,
	SELECT_SECTIONS_SOURCE,
	DELETE_SOURCE,
	DELETE_DESTINATION,
	SELECT_SCHOOL_CLASS_DESTINATION,
	SELECT_COURSES_DESTINATION,
	SELECT_SECTIONS_DESTINATION,
	DELETE_SECTION_DESTINATION,
	SELECT_VALUES_SECTION_DESTINATION,
	SELECTED_IDS,
	SAVE_SYNCHRONIZE_RAB_REQUEST,
	SAVE_SYNCHRONIZE_RAB_SUCCESS,
	SAVE_SYNCHRONIZE_RAB_ERROR,
	FETCH_ACCEPTED_USER_TYPES_REQUEST,
	FETCH_ACCEPTED_USER_TYPES_SUCCESS,
	FETCH_ACCEPTED_USER_TYPES_ERROR,
	AVAILABLE_USERS_REQUEST,
	AVAILABLE_USERS_SUCCESS,
	AVAILABLE_USERS_ERROR,
	SAVE_AVAILABLE_USERS_REQUEST,
	SAVE_AVAILABLE_USERS_SUCCESS,
	SAVE_AVAILABLE_USERS_ERROR,
	EMPTY_AVAILABLE_USERS,
} from "./adminType";
import { adminApi } from "api/admin/UsersManagments/index";
import Swal from "utils/Alert";
import { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const selectIdsAsync = () => async (dispatch) => {
	dispatch({
		type: SELECTED_IDS,
	});
};

export const deleteSourceAsync = (flag) => async (dispatch) => {
	dispatch({
		type: DELETE_SOURCE,
		payload: { flag },
	});
};

export const deleteDestinationAsync = (flag) => async (dispatch) => {
	dispatch({
		type: DELETE_DESTINATION,
		payload: { flag },
	});
};

export const selectSchoolClassSourceAsync = (courseSourceId) => async (dispatch) => {
	dispatch({
		type: SELECT_SCHOOL_CLASS_SOURCE,
		payload: { courseSourceId },
	});
};

export const selectCoursesSourceAsync = (sectionSourceId) => async (dispatch) => {
	dispatch({
		type: SELECT_COURSES_SOURCE,
		payload: { sectionSourceId },
	});
};

export const selectSectionSourceAsync = (sectionSource, classId, courseId, hideAlert) => async (dispatch) => {
	dispatch({
		type: SELECT_SECTIONS_SOURCE,
		payload: { sectionSource, classId, courseId },
	});
	hideAlert();
};

export const selectSchoolClassDestinationAsync = (courseDestinationId) => async (dispatch) => {
	dispatch({
		type: SELECT_SCHOOL_CLASS_DESTINATION,
		payload: { courseDestinationId },
	});
};

export const selectCoursesDestinationAsync = (sectionDestinationId) => async (dispatch) => {
	dispatch({
		type: SELECT_COURSES_DESTINATION,
		payload: { sectionDestinationId },
	});
};

export const selectSectionDestinationAsync = (sectionDestination, sectionSourceId) => async (dispatch) => {
	if (sectionSourceId == null) {
		toast.error(tr`please_enter_section_source_before_add_destination`);
	} else {
		dispatch({
			type: SELECT_SECTIONS_DESTINATION,
			payload: { sectionDestination, sectionSourceId },
		});
	}
};

export const deleteSectionDestinationAsync = (sectionSourceId, sectionDestinationId) => async (dispatch) => {
	dispatch({
		type: DELETE_SECTION_DESTINATION,
		payload: { sectionSourceId, sectionDestinationId },
	});
};

export const selectValuesSectionDestination = (sectionSourceId) => async (dispatch) => {
	dispatch({
		type: SELECT_VALUES_SECTION_DESTINATION,
		payload: { sectionSourceId },
	});
};

export const synchronizeSectionAsync = () => async (dispatch) => {
	dispatch({ type: GET_SYNCHRONIZE_ACTION_REQUEST });
	try {
		const response = await adminApi.synchronizeSection();
		if (response.data.status == 1) {
			const data = response.data.data;
			dispatch({
				type: GET_SYNCHRONIZE_ACTION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_SYNCHRONIZE_ACTION_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_SYNCHRONIZE_ACTION_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveSynchronizeRabAsync = (data) => async (dispatch) => {
	dispatch({ type: SAVE_SYNCHRONIZE_RAB_REQUEST });
	try {
		const response = await adminApi.saveSynchronizeRab(data);
		if (response.data.status == 1) {
			dispatch({
				type: SAVE_SYNCHRONIZE_RAB_SUCCESS,
			});
			dispatch(synchronizeSectionAsync());
		} else {
			dispatch({
				type: SAVE_SYNCHRONIZE_RAB_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_SYNCHRONIZE_RAB_ERROR,
		});
		toast.error(error?.message);
	}
};

export const attendanceReportsAsync = (date) => async (dispatch) => {
	dispatch({ type: GET_ATTENDANCE_REPORTS_REQUEST });
	try {
		const response = await adminApi.attendanceReport(date);
		if (response.data.status == 1) {
			const data = response.data.data.data;
			dispatch({
				type: GET_ATTENDANCE_REPORTS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_ATTENDANCE_REPORTS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ATTENDANCE_REPORTS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const emptyAttendanceReport = () => async (dispatch) => {
	dispatch({
		type: EMPTY_ATTENDANCE_REPORT,
		payload: { data: [] },
	});
};

export const checkUser = (iD) => async (dispatch) => {
	dispatch({
		type: CHECK_USER,
		iD,
	});
};

export const saveUsersFromUserAsync = (data, handleClose) => async (dispatch) => {
	dispatch({ type: SAVE_USERS_FROM_USER_REQUEST });
	try {
		const response = await adminApi.saveUsersFromUser(data);
		if (response.data.status == 1) {
			dispatch({
				type: SAVE_USERS_FROM_USER_SUCCESS,
			});
			handleClose();
			window.location.reload();
		} else {
			dispatch({
				type: SAVE_USERS_FROM_USER_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_USERS_FROM_USER_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchAcceptedUserTypesAsync = (table_name) => async (dispatch) => {
	dispatch({ type: FETCH_ACCEPTED_USER_TYPES_REQUEST });
	try {
		const response = await adminApi.acceptedUserTypes(table_name);
		if (response.data.status == 1) {
			const acceptedUserTypes = response.data.data.acceptedUserTypes;
			dispatch({
				type: FETCH_ACCEPTED_USER_TYPES_SUCCESS,
				payload: { acceptedUserTypes },
			});
		} else {
			dispatch({
				type: FETCH_ACCEPTED_USER_TYPES_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: FETCH_ACCEPTED_USER_TYPES_ERROR,
		});
		toast.error(error?.message);
	}
};

export const emptyAvailableUsersAsync = () => async (dispatch) => {
	dispatch({
		type: EMPTY_AVAILABLE_USERS,
		payload: { data: [] },
	});
};

export const getAvailableUsersAsync = (table_name, data, url) => async (dispatch) => {
	dispatch({ type: AVAILABLE_USERS_REQUEST });

	try {
		let response;
		url ? (response = await adminApi.getAllUsersFromUserUrl(url, data)) : (response = await adminApi.availableUsers(table_name, data));

		if (response.data.status == 1) {
			const availableUsers = response.data.data.users.data;
			let firstPageUrl = response.data.data.users.first_page_url;
			let lastPageUrl = response.data.data.users.last_page_url;
			let currentPage = response.data.data.users.current_page;
			let lastPage = response.data.data.users.last_page;
			let prevPageUrl = response.data.data.users.prev_page_url;
			let nextPageUrl = response.data.data.users.next_page_url;
			let total = response.data.data.users.total;
			dispatch({
				type: AVAILABLE_USERS_SUCCESS,
				payload: {
					availableUsers,
					firstPageUrl,
					lastPageUrl,
					currentPage,
					lastPage,
					prevPageUrl,
					nextPageUrl,
					total,
				},
			});
		} else {
			dispatch({
				type: AVAILABLE_USERS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: AVAILABLE_USERS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveAvailableUsersAsync = (table_name, data, handleClose) => async (dispatch) => {
	dispatch({ type: SAVE_AVAILABLE_USERS_REQUEST });
	try {
		const response = await adminApi.storeAvailableUsers(table_name, data);
		if (response.data.status == 1) {
			dispatch({
				type: SAVE_AVAILABLE_USERS_SUCCESS,
				payload: {},
			});
			handleClose();
		} else {
			dispatch({
				type: SAVE_AVAILABLE_USERS_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_AVAILABLE_USERS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchUserFromUserAsync = (table_name, url) => async (dispatch) => {
	dispatch({ type: GET_USERS_FROM_USER_REQUEST });
	try {
		let response;
		url ? (response = await adminApi.getAllUsersFromUserUrl(url)) : (response = await adminApi.getAllUsersFromUser(table_name));

		if (response.data.status == 1) {
			let AllUsers = response.data.data.users.data;
			let firstPageUrl = response.data.data.users.first_page_url;
			let lastPageUrl = response.data.data.users.last_page_url;
			let currentPage = response.data.data.users.current_page;
			let lastPage = response.data.data.users.last_page;
			let prevPageUrl = response.data.data.users.prev_page_url;
			let nextPageUrl = response.data.data.users.next_page_url;
			let total = response.data.data.users.total;

			dispatch({
				type: GET_USERS_FROM_USER_SUCCESS,
				payload: {
					AllUsers,
					firstPageUrl,
					lastPageUrl,
					currentPage,
					lastPage,
					prevPageUrl,
					nextPageUrl,
					total,
				},
			});
		} else {
			dispatch({
				type: GET_USERS_FROM_USER_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_USERS_FROM_USER_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchGradesLevels = () => async (dispatch) => {
	dispatch({ type: GET_GRADES_LEVELS_REQUEST });
	try {
		const response = await adminApi.gradesLevels();
		if (response.data.status == 1) {
			let AllGradesLevels = response.data.data.school_classes;
			let AllEducationStages = response.data.data.education_stages;
			dispatch({
				type: GET_GRADES_LEVELS,
				payload: { AllGradesLevels, AllEducationStages },
			});
		} else {
			dispatch({
				type: GET_GRADES_LEVELS_ERROR,
				payload: response.data.msg,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_GRADES_LEVELS_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
	}
};

export const fetchUserById = (tabel_name, id) => async (dispatch) => {
	dispatch({ type: GET_USER_BY_ID_REQUEST });
	try {
		const response = await adminApi.getUserById(tabel_name, id);

		if (response.data.status == 1) {
			let userById = response.data.data;
			dispatch({
				type: GET_USER_BY_ID,
				payload: { userById },
			});
		} else {
			dispatch({
				type: GET_USER_BY_ID_ERROR,
				payload: response.data.msg,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_USER_BY_ID_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
	}
};
