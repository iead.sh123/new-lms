import React from 'react'
import RCollapsedDivV2 from './RCollapsedDivV2'
import { useState } from 'react'
import styles from './SchoolLister.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import { types } from './constants'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import { deleteSweetAlert } from 'components/Global/RComs/RAlert2'
import tr from 'components/Global/RComs/RTranslator'
import { useMutateData } from 'hocs/useMutateData'
import { schoolManagementV2API } from 'api/SchoolManagementV2'
import { coursesManagerApi } from 'api/global/coursesManager'
import GCoursesSchoolV2 from './GCoursesSchoolV2'


const GCurriculaV2 = (
    {
        id,
        gradeLevelId,
        currentCurricula,
        allCurriculas,
        index,
        setFieldValue,
        openCurriculaId,
        onDelete,
        handleChangingLevelsProperties,
        reOrderCurriculaMutation,
        courses
    }
) => {
    const user = useSelector((state) => state?.auth);
    // const organization_id = user && user?.user && user?.user?.organization_id;
    const organization_id = 1

    const createUpdateSchoolCurriculaMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.createUpdateLevel(payload, organization_id),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ data, variables }) => {
            if (variables.payload?.fakeId) {
                const newCurriculas = JSON.parse(JSON.stringify(allCurriculas))
                const curriculaIndex = allCurriculas.findIndex((curricula) => curricula.fakeId == id)
                newCurriculas[curriculaIndex] = { ...data?.data }
                setFieldValue(types.Curriculas, newCurriculas)
                reOrderCurriculaMutation.mutate({ payload: { levels: [...newCurriculas] } })
            }
            setSaved(true)
        }
    })

    const createCurriculaAsCategory = useMutateData({
        queryFn: ({ payload, other }) => coursesManagerApi.addCategoryToTree(payload),
        onSuccessFn: ({ data, variables }) => {
            createUpdateSchoolCurriculaMutation.mutate({ payload: { ...variables.other, ...variables.payload, entity_id: data.data.id } })
        }
    })
    const currentCourses = courses.filter((course, index) => course.parent_level_id == id)
    console.log("Current Courses", currentCourses)
    const [saved, setSaved] = useState(id > 0 ? true : false)
    const [inputValue, setInputValue] = useState(currentCurricula?.name ?? "")
    const [alert1, setAlert] = useState(false);

    const [enterPressed, setEnterPressed] = useState(false)
    const [saveClicked, setSaveClicked] = useState(false)
    const [addClicked, setAddClicked] = useState(false)
    const [openCourses, setOpenCourses] = useState(false)
    const handleCloseCoursesModal = () => {
        setOpenCourses(false)
    }
    const childPrefix = 'content'

    const editCurricula = () => {
        console.log("Edit Triggered")
        setEnterPressed(false)
        setSaved(!saved)
    }

    const hideAlert = () => setAlert(null);
    const showAlerts = (child) => setAlert(child);

    const deleteCurricula = (id) => {
        const confirm = tr`Yes, delete it`;
        const message = (
            <div>
                <h6>{tr`Are you sure? `}</h6>
                <p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
            </div>
        );
        deleteSweetAlert(showAlerts, hideAlert, onDelete, { curriculaId: id }, message, confirm);
    }
    const handleInputChange = (event) => {
        setSaveClicked(false)
        setAddClicked(false)
        setEnterPressed(false)
        setInputValue(event.target.value)
    }

    const handleInputDown = async (event) => {
        if (event.key === 'Enter') {
            if (enterPressed)
                return
            setEnterPressed(true)
            setSaveClicked(true)
            if (inputValue != "") {
                const { type, attributes, ...other } = currentCurricula
                const payload = {
                    name: inputValue
                }
                if (id > 0) {
                    createUpdateSchoolCurriculaMutation.mutate({ payload: { ...payload, ...other } })
                }
                else {
                    createCurriculaAsCategory.mutate({ payload, other })
                }
            }
        }
    }
    const handleSaveButton = async () => {
        if (enterPressed) {
            return
        }
        setSaveClicked(true)
        setEnterPressed(true)
        if (inputValue != '') {
            const { type, attributes, ...other } = currentCurricula
            const payload = {
                name: inputValue
            }
            if (id > 0) {
                createUpdateSchoolCurriculaMutation.mutate({ payload: { ...payload, ...other } })
            }
            else {
                createCurriculaAsCategory.mutate({ payload, other })
            }
        }
    }
    const handleDivClicked = (index) => {
        const newProperties = {
            [types.OpenCurriculaId]: openCurriculaId == id ? null : id,
            [types.OpenCurriculaIndex]: openCurriculaId == id ? null : index
        }
        handleChangingLevelsProperties(newProperties)

    }
    const openCourseCataloug = () => {
        if (id <= 0) {
            setAddClicked(true)
            // setErrorMessage('Please Create GradeLevel First')
            return
        }
        setOpenCourses(true)
    }

    // const [curriculaBodyElements, setCurriculaBodyElements] = useState(<GCurriculaContent />);
    // const generateCurriculaBodyComponent = () => {
    //     // Create a new instance of your dynamic component and add it to the state
    //     const newComponent = <span className='m-0 p-0 text-primary'>Body</span>;
    //     setCurriculaBodyElements([...curriculaBodyElements, newComponent]);
    // };

    return (
        <>
            <RCollapsedDivV2
                handleInputChange={handleInputChange}
                handleInputDown={handleInputDown}
                handleDivClicked={handleDivClicked}
                handleEditFunctionality={editCurricula}
                handleDeleteFunctionality={deleteCurricula}
                handleSaveButton={handleSaveButton}
                setSaved={setSaved}
                id={id}
                collapseChilds={currentCourses}
                openLevelId={openCurriculaId}
                inputValue={inputValue}
                isOpen={openCurriculaId == id ? true : false}
                number={currentCourses?.length}
                saved={saved}
                addClicked={addClicked}
                saveClicked={saveClicked}
                index={index}
                inputPlaceHolder="Curricula here"
                buttonText="Add Curricula"
                currentType={types.Curriculas}
                childType="Courses"
                childPrefix={childPrefix}
                openCourseCataloug={openCourseCataloug}
                isLoading={createCurriculaAsCategory?.isLoading || createUpdateSchoolCurriculaMutation?.isLoading}
            />
            <AppModal size={"lg"}
                show={openCourses}
                header={"Courses"}
                parentHandleClose={handleCloseCoursesModal}
                headerSort={<GCoursesSchoolV2
                    isModal={true}
                    setFieldValue={setFieldValue}
                    handleCloseCoursesModal={handleCloseCoursesModal}
                    curriculaId={id}
                    allCurriculasCourses={courses}
                    currentCurriculaCourses={currentCourses}
                />} />
            {alert1}
        </>
    )
}

export default GCurriculaV2