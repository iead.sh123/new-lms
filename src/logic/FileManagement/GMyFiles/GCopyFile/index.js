import React from "react";
import RUploadFileSection from "view/FileManagement/RMyFiles/RUploadFileSection";
import RMyFilesCard from "view/FileManagement/RMyFiles/RMyFilesCard";
import RButton from "components/Global/RComs/RButton";
import styles from "../../FileManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { fileManagementApi } from "api/global/fileManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useFormik } from "formik";

const GCopyFile = ({ idsSelected, parentInvalidateKey, handleCloseCopyModal }) => {
	const initialValues = {
		data: {},
		groupingOfSearchFields: { name: "", sortByFiled: "", sortByOrderType: 1, mime_type: "", modified_by: "", folder_id: null },
	};
	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});
	// - - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const nameField = values?.groupingOfSearchFields?.name;
	const folderIdField = values?.groupingOfSearchFields?.folder_id;

	let queryParam = "";

	if (!nameField && !folderIdField) {
		queryParam = "";
	} else {
		queryParam = "?";

		if (folderIdField) {
			queryParam += `folder_id=${folderIdField}&`;
		}

		if (nameField) {
			queryParam += `name=${nameField}&`;
		}

		// Remove the trailing '&' if it exists
		queryParam = queryParam.slice(0, -1);
	}

	const rootFoldersData = useFetchDataRQ({
		queryKey: ["copyItems", values?.groupingOfSearchFields],
		queryFn: () => fileManagementApi.rootFolders(queryParam),
		onSuccessFn: ({ data }) => {
			const modifiedSubFolders = data.data.sub_folders.map((subFolder) => ({
				...subFolder,
				saved: true,
			}));

			const modifiedFiles = data.data.files.map((file) => ({
				...file,
				saved: true,
			}));

			const updatedData = {
				...data.data,
				sub_folders: modifiedSubFolders,
				files: modifiedFiles,
			};

			setFieldValue("data", updatedData);
		},
	});

	const createSubFolderMutate = useMutateData({
		queryFn: (data) => fileManagementApi.createSubFolders(data),
		onSuccessFn: ({ data }) => {
			values.data.sub_folders.splice(values.data.sub_folders.length - 1, 1);
			values.data.sub_folders.push({ ...data.data, saved: true });
		},
	});
	const copyItemsToAnotherFileMutate = useMutateData({
		queryFn: ({ folderId, data }) =>
			fileManagementApi.copyItemsToAnotherItem({
				folderId,
				data,
			}),
		onSuccessFn: () => {
			handleCloseCopyModal();
		},
		invalidateKeys: [["copyItems", values?.groupingOfSearchFields], parentInvalidateKey],
		multipleKeys: true,
		displaySuccess: true,
	});

	// - - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -
	const handleChangeText = (value, name) => {
		setFieldValue(name, value);
	};

	const handleInputSaved = (event, item) => {
		if (event.key == "Enter" || !event.key) {
			createSubFolderMutate.mutate({ folder_name: event.target.value, parent_folder: values.data.current_folder.id });
		}
	};

	//  - - - - - - - Handle Add New Folder - - - - - - -
	const handleAddNewFolder = () => {
		const folderNames = values.data.sub_folders.map((folder) => folder.folder_name);

		let defaultFolderName = "NewFolder";

		// Check if 'NewFolder' already exists
		if (folderNames.includes(defaultFolderName)) {
			let index = 1;
			// Keep incrementing the index until a unique folder name is found
			while (folderNames.includes(`${defaultFolderName}(${index})`)) {
				index++;
			}
			// Set the default folder name with the incremented index
			defaultFolderName = `${defaultFolderName}(${index})`;
		}
		const newFolder = { fakeId: -1, folder_name: defaultFolderName, size: 0, created_at: new Date(), saved: false };
		setFieldValue("data.sub_folders", [...values.data.sub_folders, newFolder]);
	};

	//  - - - - - - - Handle Show Specific Folder - - - - - - -
	const handleShowSpecificFolder = (id) => {
		setFieldValue("groupingOfSearchFields.folder_id", id);
	};

	return (
		<RFlex className={styles.container}>
			{/* - - - - - - - Create New Folder - - - - - - - */}
			<RUploadFileSection
				data={{ values }}
				handlers={{ handleAddNewFolder, setFieldValue }}
				loading={{ createSubFolderLoading: createSubFolderMutate.isLoading, allItemsLoading: rootFoldersData.isLoading }}
				removeUploadFile={true}
			/>

			<div className={styles.container__scroll}>
				{/* - - - - - - - All Files Card Mode - - - - - - - */}
				<RMyFilesCard
					data={{ values }}
					handlers={{ handleChangeText, handleInputSaved, handleShowSpecificFolder }}
					loading={{
						rootFoldersLoading: rootFoldersData.isLoading,
					}}
					removeActions={true}
				/>
			</div>

			<RFlex styleProps={{ width: "100%", justifyContent: "flex-end" }}>
				<RButton
					text={tr`copy_here`}
					onClick={() =>
						copyItemsToAnotherFileMutate.mutate({
							folderId: rootFoldersData?.data?.data?.data?.current_folder?.id,
							data: {
								folder_ids: idsSelected?.folderIds,
								file_ids: idsSelected?.fileIds,
							},
						})
					}
					color="primary"
					loading={copyItemsToAnotherFileMutate.isLoading}
					disabled={copyItemsToAnotherFileMutate.isLoading}
				/>
			</RFlex>
		</RFlex>
	);
};

export default GCopyFile;
