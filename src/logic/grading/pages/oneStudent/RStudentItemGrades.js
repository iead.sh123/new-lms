import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RGrading.css'
import {Collapse} from "reactstrap";
import RStudentTypeGrades from "./RStudentTypeGrades";

function RStudentItemGrades({item,finalMark,typesGrades}){
    const [collapse, setCollapse] = useState(false);
    const toggle = () => setCollapse(!collapse);
  
    return(
        <div className={collapse==false?"item_row":"collapsed_item"} style={{ }}>
        <Row >
           <Col xs lg="4" className="item_text">
         <span> {item}</span>  
           </Col>
           <Col xs lg="4" className="item_text">
           <span> final mark: {finalMark}</span>  
           </Col>
           <Col xs lg="3" className="item_text"></Col>
           <Col xs lg="1" className="item_angle">
               <button onClick={toggle}>  <i className= {collapse==false?"fa fa-angle-up":"fa fa-angle-down"} ></i> </button>   
      
           </Col>
          
        </Row>
        <Row >
        <Col  xs lg="11" >
        <Collapse isOpen={collapse}>
                    <RStudentTypeGrades typesGrades={typesGrades}/>
               </Collapse>
  
        </Col>
        <Col  xs lg="1" >   </Col>
      </Row>    
   </div>
    )

}

export default RStudentItemGrades;


