import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  saveEducationStageAsync,
  saveGradeLevelAsync,
  saveCurriculaAsync,
} from "store/actions/global/schoolInitiation.actions";
import { useParams } from "react-router-dom";
import GSchoolInitiationListers from "./GSchoolInitiationListers";
import GSchoolInitiationTabs from "./GSchoolInitiationTabs";
import GSchoolInitiationTree from "./GSchoolInitiationTree";

export const SchoolInitiationContext = React.createContext();
const GSchoolInitiation = () => {
  const dispatch = useDispatch();
  const { sIType, sITypeId, semesterId, tabTitle } = useParams();

  const [show, setShow] = useState(false);
  const [message, setMessage] = useState(null);
  const [idAdded, setIdAdded] = useState(null);
  const [showAll, setShowAll] = useState(false);

  const handleOpen = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    setAddEducationStage({ name: "", order: null });
    setAddGradeLevels({
      app_grade_level_id: null,
      grade_level_order: null,
      education_stage_id: null,
      code: null,
      title: null,
      grade_scale_id: null,
      assessment_plan_id: null,
      report_card_template_id: null,
      app_grade_level: null,
      grade_scale: null,
      education_stage: null,
    });
    setAddCurricula({
      main_course_id: null,
      name: "",
      grade_level_id: null,
      semester_id: null,
      main_course: {},
    });
    setMessage(null);
  };

  const [addEducationStage, setAddEducationStage] = useState({
    name: "",
    order: null,
  });
  const [addGradeLevels, setAddGradeLevels] = useState({
    code: null,
    title: null,
    grade_level_order: null,
    app_grade_level_id: null,
    education_stage_id: null,
    assessment_plan_id: null,
    grade_scale_id: null,
    report_card_template_id: null,
    app_grade_level: null,
    grade_scale: null,
    education_stage: null,
  });
  const [addCurricula, setAddCurricula] = useState({
    main_course_id: null,
    name: "",
    grade_level_id: null,
    semester_id: null,
    main_course: {},
  });

  const handleChangeEducationStage = (name, value) => {
    setAddEducationStage({ ...addEducationStage, [name]: value });
  };

  const handleChangeGradeLevels = (name, value) => {
    setAddGradeLevels({ ...addGradeLevels, [name]: value });
  };

  const handleChangeCurricula = (name, value) => {
    setAddCurricula({ ...addCurricula, [name]: value });
  };

  const handleSaveEducationStage = () => {
    dispatch(
      saveEducationStageAsync(addEducationStage, setMessage, setIdAdded)
    );
  };

  const handleSaveGradeLevels = () => {
    dispatch(
      saveGradeLevelAsync(
        {
          ...addGradeLevels,
          education_stage_id: addGradeLevels.education_stage?.value,
          app_grade_level_id: addGradeLevels.app_grade_level?.value,
          grade_scale_id: addGradeLevels.grade_scale?.value,
        },
        setMessage,
        setIdAdded
      )
    );
  };

  const handleSaveCurricula = () => {
    if (sIType == "grade_levels") {
      dispatch(
        saveCurriculaAsync(
          {
            ...addCurricula,
            main_course_id: addCurricula.main_course.value,
            grade_level_id: sITypeId,
            semester_id: semesterId,
          },
          setMessage,
          setIdAdded
        )
      );
    } else {
      dispatch(
        saveCurriculaAsync(
          {
            ...addCurricula,
            main_course_id: addCurricula.main_course.value,
          },
          setMessage,
          setIdAdded
        )
      );
    }
  };

  const handleShowAll = () => {
    setShowAll(true);
    setMessage(null);
    handleClose();
  };

  const {
    semesters,
    gradeScales,
    mainCourses,
    appGradeLevels,
    educationStage,
    tree,
    namePartOfSchool,
    semestersLoading,
    gradeScalesLoading,
    appGradeLevelsLoading,
    mainCoursesLoading,
    educationStageLoading,
    saveEducationStageLoading,
    saveGradeLevelLoading,
    saveCurriculaLoading,
    egcLoading,
    treeLoading,
  } = useSelector((state) => state.schoolInitiationRed);

  useEffect(() => {
    //i use this in grade level form to set education stage value inside select
    if (namePartOfSchool) {
      sIType == "education_stage" &&
        setAddGradeLevels({
          ...addGradeLevels,
          education_stage: namePartOfSchool,
        });
    }
  }, [namePartOfSchool]);

  return (
    <SchoolInitiationContext.Provider
      value={{
        show,
        message,
        idAdded,
        setAddEducationStage,
        setAddGradeLevels,
        setAddCurricula,
        setShowAll,
        setMessage,
        showAll,

        handleOpen,
        handleClose,
        handleChangeEducationStage,
        handleChangeGradeLevels,
        handleChangeCurricula,
        handleSaveEducationStage,
        handleSaveGradeLevels,
        handleSaveCurricula,
        handleShowAll,
        namePartOfSchool,

        addEducationStage,
        addGradeLevels,
        addCurricula,
        semesters,

        gradeScales,
        appGradeLevels,
        mainCourses,
        educationStage,
        tree,

        semestersLoading,
        gradeScalesLoading,
        appGradeLevelsLoading,
        mainCoursesLoading,
        educationStageLoading,
        saveEducationStageLoading,
        saveGradeLevelLoading,
        saveCurriculaLoading,
        egcLoading,
        treeLoading,
      }}
    >
      <GSchoolInitiationTree />
      {!tabTitle && <GSchoolInitiationListers />}
      {tabTitle && <GSchoolInitiationTabs />}
    </SchoolInitiationContext.Provider>
  );
};

export default GSchoolInitiation;
