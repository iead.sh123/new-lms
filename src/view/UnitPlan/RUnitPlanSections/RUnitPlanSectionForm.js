import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import RLessonPlanAddButton from "view/LessonPlan/RLessonPlanItem/RLessonPlanAddButton";
import RUnitPlanItems from "./RUnitPlanItems/RUnitPlanItems";
import RButton from "components/Global/RComs/RButton";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RUnitPlanSectionGroupButtons from "./RUnitPlanSectionGroupButtons";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import iconsFa6 from "variables/iconsFa6";

const RUnitPlanSectionForm = () => {
	const UnitPlanContextData = useContext(UnitPlanContext);
	const designByDirection = UnitPlanContextData.dir ? styles.text_ltr : styles.text_rtl;

	return (
		<>
			{UnitPlanContextData.unitPlan.uplsections.map((section, index) => (
				<div
					kay={section?.id ? section?.id : section?.fakeId}
					ref={(ref) => {
						UnitPlanContextData.targetRefs.current[section.id ? section.id : section.fakeId] = ref;
					}}
				>
					{/* RLessonPlanAddButton Up */}
					<RLessonPlanAddButton handleAddNewLessonPlanItems={UnitPlanContextData.handleAddNewSections} index={index} position="up" />
					<Form>
						{/* subject and actions */}
						<Row className="mb-3">
							<Col lg={9} xs={12} className="pl-0 ">
								<FormGroup>
									<label className={designByDirection}>{tr("subject")}</label>
									<Input
										id="name"
										required
										type="text"
										onChange={(event) =>
											UnitPlanContextData.handelSectionChange("name", event.target.value, section?.id ? section?.id : section?.fakeId)
										}
										value={section?.name}
										placeholder={tr("subject")}
									/>
									{UnitPlanContextData.unitPlanErrors &&
										UnitPlanContextData.unitPlanErrors.uplsections &&
										UnitPlanContextData.unitPlanErrors.uplsections[index] &&
										UnitPlanContextData.unitPlanErrors.uplsections[index].name && (
											<FormText color="danger">{UnitPlanContextData.unitPlanErrors.uplsections[index].name}</FormText>
										)}
								</FormGroup>
							</Col>

							<Col lg={3} xs={12} className={styles.section_icons}>
								<RUnitPlanSectionGroupButtons section={section} />
							</Col>
						</Row>

						{/* description */}
						<Row className="mb-3">
							<Col xs={12} className="pl-0">
								<FormGroup>
									<label className={designByDirection}>{tr("description")}</label>

									<Input
										id="description"
										required
										type="textarea"
										onChange={(event) =>
											UnitPlanContextData.handelSectionChange(
												"description",
												event.target.value,
												section?.id ? section?.id : section?.fakeId
											)
										}
										value={section?.description}
										placeholder={tr("description")}
									/>

									{UnitPlanContextData.unitPlanErrors &&
										UnitPlanContextData.unitPlanErrors.uplsections &&
										UnitPlanContextData.unitPlanErrors.uplsections[index] &&
										UnitPlanContextData.unitPlanErrors.uplsections[index].description && (
											<FormText color="danger">{UnitPlanContextData.unitPlanErrors.uplsections[index].description}</FormText>
										)}
								</FormGroup>
							</Col>
						</Row>
					</Form>

					{/* rubric */}
					{section?.rubric_id && (
						<RFlex>
							<i
								className="fa fa-times-circle-o fa-lg"
								aria-hidden="true"
								style={{ color: "red", cursor: "pointer" }}
								onClick={() => UnitPlanContextData.handleRemoveRubricFromSection(section?.id ? section?.id : section?.fakeId)}
							/>
							<i
								class="fa fa-refresh"
								aria-hidden="true"
								onClick={() => {
									UnitPlanContextData.setUnitPlanSectionId(section?.id ? section?.id : section?.fakeId);
									UnitPlanContextData.setTypeToPick("section");
									UnitPlanContextData.handleOpenPickARubric();
								}}
								style={{ cursor: "pointer" }}
							/>
							<h6>{section.rubric.title}</h6>
						</RFlex>
					)}
					{/* lesson_plan */}
					{section?.lesson_plan_id && (
						<RFlex>
							<i
								className="fa fa-times-circle-o fa-lg"
								aria-hidden="true"
								style={{ color: "red", cursor: "pointer" }}
								onClick={() => UnitPlanContextData.handleRemoveLessonPlanFromSection(section?.id ? section?.id : section?.fakeId)}
							/>
							<i
								class="fa fa-refresh"
								aria-hidden="true"
								style={{ cursor: "pointer" }}
								onClick={() => {
									UnitPlanContextData.setUnitPlanSectionId(section?.id ? section?.id : section?.fakeId);
									UnitPlanContextData.setTypeToPick("section");

									UnitPlanContextData.handleOpenPickALessonPlan();
								}}
							/>
							<h6>{section.lesson_plan.name}</h6>
						</RFlex>
					)}
					{/* attachments */}
					{section.attachments !== null && section.attachments && section?.attachments?.length > 0 && (
						<RFlex>
							<i
								className="fa fa-times-circle-o fa-lg"
								aria-hidden="true"
								style={{ color: "red", cursor: "pointer" }}
								onClick={() => {
									UnitPlanContextData.handleRemoveAttachmentFromSection(section?.id ? section?.id : section?.fakeId);
								}}
							/>
							<RFileSuite
								parentCallback={(values) => {
									UnitPlanContextData.handelSectionChange("attachments", values, section?.id ? section?.id : section?.fakeId);
								}}
								singleFile={true}
								binary={true}
								value={section?.attachments}
								fileType="image/*"
								theme="button_replace_theme"
							/>

							<h6>{section.attachments[0].file_name ?? section.attachments[0].name}</h6>
						</RFlex>
					)}
					{/* create_item */}
					<Row>
						<Col xs={4} className="d-flex justify-content-start pl-0">
							<RButton
								text={tr`create_new_item`}
								onClick={() =>
									UnitPlanContextData.handleAddItemsToSection(section?.id ? section?.id : section?.fakeId, null, null, "", "parent", true)
								}
								color="primary"
								faicon={iconsFa6.plus}
							/>
						</Col>
					</Row>

					{section.items !== null && section.items && section?.items?.length > 0 && (
						<RUnitPlanItems sectionId={section?.id ? section?.id : section?.fakeId} items={section.items} />
					)}

					{/* RLessonPlanAddButton Down */}
					<RLessonPlanAddButton handleAddNewLessonPlanItems={UnitPlanContextData.handleAddNewSections} index={index + 1} position="down" />
				</div>
			))}
		</>
	);
};

export default RUnitPlanSectionForm;
