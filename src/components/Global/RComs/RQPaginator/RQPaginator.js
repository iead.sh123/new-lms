import React, { useState } from "react";
import { PaginationItem, PaginationLink, Pagination } from "reactstrap";
import { lang } from "../RTranslator";
import RFlex from "../RFlex/RFlex";

const RQPaginator = ({ info, handleChangePage, page }) => {
	const [currentPage, setCurrentPage] = useState(1);

	const handlePageClick = (page) => {
		setCurrentPage(page);
		handleChangePage(page);
	};

	// This thing is funny
	const paginationDataOneFormate = { key: info?.data?.data?.data?.data?.last_page, value: info?.data?.data?.data?.data };
	const paginationDataTwoFormate = { key: info?.data?.data?.data?.last_page, value: info?.data?.data?.data };
	const paginationDataThreeFormate = { key: info?.data?.data?.data?.users?.last_page, value: info?.data?.data?.data?.users };
	const paginationDataFourFormate = { key: info?.data?.data?.records?.meta?.last_page, value: info?.data?.data?.records?.meta };
	const paginationDataPythonFormate = { key: info?.data?.data?.data?.paginate?.last_page, value: info?.data?.data?.data?.paginate };
	const paginationDataFifthFormate = { key: info?.data?.data.last_page, value: info?.data?.data }
	let paginationData = {};
	if (paginationDataOneFormate.key) {
		paginationData = paginationDataOneFormate.value;
	} else if (paginationDataTwoFormate.key) {
		paginationData = paginationDataTwoFormate.value;
	} else if (paginationDataThreeFormate.key) {
		paginationData = paginationDataThreeFormate.value;
	} else if (paginationDataFourFormate.key) {
		paginationData = paginationDataFourFormate.value;
	} else if (paginationDataPythonFormate.key) {
		paginationData = paginationDataPythonFormate.value;
	} else if (paginationDataFifthFormate.key) {
		paginationData = paginationDataFifthFormate.value;

	}

	const totalPages = paginationData.last_page || 1;

	const renderPaginationItems = () => {
		const items = [];
		const startPage = Math.floor((currentPage - 1) / 5) * 5 + 1;
		const endPage = Math.min(startPage + 4, totalPages);

		for (let i = 1; i <= totalPages; i++) {
			items.push(
				<PaginationItem key={i} active={i === page}>
					<PaginationLink onClick={() => handlePageClick(i)}>{i}</PaginationLink>
				</PaginationItem>
			);
		}
		return items;
	};

	return (
		<RFlex styleProps={{ justifyContent: lang == "english" ? "flex-end" : "flex-start", width: "100%" }}>
			<Pagination size="sm" className="hmdi" aria-label="Page navigation example">
				<PaginationItem>
					<PaginationLink
						previous
						onClick={() => {
							handleChangePage(1);
						}}
					/>
				</PaginationItem>

				{/* {new Array(paginationData?.last_page).fill(null).map((__, index) => (
					<PaginationItem key={index}>
						<PaginationLink
							onClick={() => {
								handleChangePage(index + 1);
							}}
						>
							{index + 1}
						</PaginationLink>
					</PaginationItem>
				))} */}

				{renderPaginationItems()}

				<PaginationItem disabled={paginationData?.current_page == paginationData?.last_page ? true : false}>
					<PaginationLink
						next
						onClick={() => {
							handleChangePage(paginationData?.last_page);
						}}
					/>
				</PaginationItem>
			</Pagination>
		</RFlex>
	);
};

export default RQPaginator;
