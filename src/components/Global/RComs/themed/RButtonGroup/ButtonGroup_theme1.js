import styles from "./styles/theme1.Module.scss";
import React from "react";
import { ButtonGroup, Button } from "reactstrap";

const ButtonGroup_theme1 = ({ buttons, activeButtonId }) => (
  <ButtonGroup>
    {buttons?.map((child) => (
      <Button
        className={styles["Button"]}
        key={child.id}
        active={child.id === activeButtonId}
        onClick={child.onClick}
        style={{ display: "flex" }}
      >
        <UserFirstLetter str={child.text} width={30} top={5} />
        <p
          style={{
            top: "6px",
            position: "relative",
            padding: "0px 5px 0px 5px",
          }}
        >
          {child.text}
        </p>
        <p
          style={{
            top: "6px",
            position: "relative",
            padding: "0px 3px 0px 3px",
            background: "white",
            color: "black",
          }}
        >
          {child.id === activeButtonId && countEvent}
        </p>
      </Button>
    ))}
  </ButtonGroup>
);

export default ButtonGroup_theme1;
