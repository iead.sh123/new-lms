import { get, post, put } from 'config/api';
import { Services } from 'engine/services';

export const calenderApi = {
  events: () => get(`${Services.calender.backend}user-events`),
  types: ()=>get(`${Services.calender.backend}event-types/blocks`),
  blockEventTypes: (blockerId, organizationId, blockedIds) => put(`${Services.calender.backend}event-types/${blockerId}/organizations/${organizationId}/blocks`, {
    events: blockedIds
  }),
  saveRelations: (payload)=>put(`${Services.calender.backend}event-types/blocks`,payload),
  comments: eventId => get(`${Services.calender.backend}comments/events/${eventId}`),
  notes: eventId => get(`${Services.calender.backend}notes/${eventId}`),
  addComment: (eventId,payload)  => post(`${Services.calender.backend}comments/events/${eventId}`,payload),
  addNote: (eventId,payload) =>post(`${Services.calender.backend}notes/${eventId}`,payload),
  print: payload=>post(`${Services.print.backend}api/JsonToPdf`,payload)
};
