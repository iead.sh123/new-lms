import React from "react";
import RRubricHeader from "view/Courses/CourseManagement/Rubric/RRubricHeader";
import RRubricViewer from "view/Courses/CourseManagement/Rubric/Viewer/RRubricViewer";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const GRubricViewer = () => {
	return (
		<RFlex styleProps={{ gap: 30, flexDirection: "column" }}>
			<RRubricHeader />
			<RRubricViewer />
		</RFlex>
	);
};

export default GRubricViewer;
