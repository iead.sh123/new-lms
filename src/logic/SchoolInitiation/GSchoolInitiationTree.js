import React, { useContext, useEffect } from "react";
import { schoolInitiationTreeAsync } from "store/actions/global/schoolInitiation.actions";
import { SchoolInitiationContext } from "./GSchoolInitiation";
import { useDispatch } from "react-redux";
import Drawer from "react-modern-drawer";
import styles from "./SchoolInitiation.module.scss";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Tree from "./Tree";
import tr from "components/Global/RComs/RTranslator";
import "react-modern-drawer/dist/index.css";

const GSchoolInitiationTree = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  const dispatch = useDispatch();

  const [isOpen, setIsOpen] = React.useState(false);
  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState);
  };

  useEffect(() => {
    dispatch(schoolInitiationTreeAsync());
  }, []);

  return (
    <React.Fragment>
      <div className={styles.tree_box} onClick={toggleDrawer}>
        <div className={styles.watermark}></div>
      </div>

      <Drawer
        open={isOpen}
        onClose={toggleDrawer}
        direction="right"
        className={styles.drawer_tree}
      >
        {SchoolInitiationData.treeLoading ? (
          <Loader />
        ) : (
          <>
            <Tree data={SchoolInitiationData.tree} />
            <RFlex
              onClick={toggleDrawer}
              className={styles.hide_drawer + " m-3"}
            >
              <p>{tr`hide`}</p>
              <i
                className="fa fa-arrow-right"
                style={{ position: "relative", top: "5px" }}
              />
            </RFlex>
          </>
        )}
      </Drawer>
    </React.Fragment>
  );
};

export default GSchoolInitiationTree;
