import React from 'react'

//fa-regular fa-pen-to-square
//fa-solid fa-check
const RIconDiv = (
    {
        initialIcon,
        index,
        iconStyle,
        divClassName,
        divStyle,
        onClick,
        detailsState,
        icon,
        text,
        iconSize = "fa-sm"
    }
) => {
    const defaultStyling = { width: '22px', height: '22px', margin: 'auto', backgroundColor: '#F3F3F3', cursor: 'pointer', display: 'flex', justifyContent: 'center', alignItems: 'center' }
    return (
        <div className={`${divClassName} text-primary`} style={{ ...defaultStyling, ...divStyle }} onClick={() => onClick&&onClick(index)}>
            {!text ? <i className={`${icon} ${iconSize}`} style={iconStyle}></i> : text}
        </div>
    )
}

export default RIconDiv