import Swal, { DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { learningObjects } from "api/global/learningObjects";
import {
	GET_LEARNING_OBJECT_BY_ID_REQUEST,
	GET_LEARNING_OBJECT_BY_ID_SUCCESS,
	GET_LEARNING_OBJECT_BY_ID_ERROR,
	GET_LEARNING_OBJECT_AVAILABLE_REQUEST,
	GET_LEARNING_OBJECT_AVAILABLE_SUCCESS,
	GET_LEARNING_OBJECT_AVAILABLE_ERROR,
	EDIT_LEARNING_OBJECT_REQUEST,
	EDIT_LEARNING_OBJECT_SUCCESS,
	EDIT_LEARNING_OBJECT_ERROR,
	ADD_VALUES_TO_LEARNING_OBJECT,
	GET_STATE_MACHINE_COMPONENTS_REQUEST,
	GET_STATE_MACHINE_COMPONENTS_SUCCESS,
	GET_STATE_MACHINE_COMPONENTS_ERROR,
	GET_INTERACTION_CONSTRAINT_TYPE,
	GET_STATE_MACHINE_BY_ID_REQUEST,
	GET_STATE_MACHINE_BY_ID_SUCCESS,
	GET_STATE_MACHINE_BY_ID_ERROR,
	ADD_VALUES_TO_STATE_MACHINE,
	ADD_NEW_NODE,
	REMOVE_NODE,
	ADD_STATUS_TO_NODE,
	SET_NEW_STATUS_TO_START_AND_END_NODE,
	ADD_CONSTRAINTS_TO_NODE,
	REMOVE_CONSTRAINTS_FROM_NODE,
	ADD_DATA_TO_CONSTRAINT,
	ADD_RESPONSE_CONSTRAINT_TYPE_TO_CONSTRAINT,
	ADD_RESPONSE_CONSTRAINT,
	REMOVE_AUTO_CORRECT_ANSWER,
	ADD_STATE_MACHINE_REQUEST,
	ADD_STATE_MACHINE_SUCCESS,
	ADD_STATE_MACHINE_ERROR,
	EDIT_STATE_MACHINE_REQUEST,
	EDIT_STATE_MACHINE_SUCCESS,
	EDIT_STATE_MACHINE_ERROR,
	EMPTY_STATE_MACHINE,
	GET_LEARNING_OBJECT_MY_PROGRESS_REQUEST,
	GET_LEARNING_OBJECT_MY_PROGRESS_SUCCESS,
	GET_LEARNING_OBJECT_MY_PROGRESS_ERROR,
	USER_INTERACTION_REQUEST,
	USER_INTERACTION_SUCCESS,
	USER_INTERACTION_ERROR,
} from "./globalTypes";
import { userLogsAsync } from "./track.action";
import { toast } from "react-toastify";

export const getLearningObjectById = (id, type) => async (dispatch) => {
	dispatch({ type: GET_LEARNING_OBJECT_BY_ID_REQUEST });
	try {
		const response = await learningObjects.getLearningObjectById(id, type);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_LEARNING_OBJECT_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_LEARNING_OBJECT_BY_ID_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LEARNING_OBJECT_BY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const getLearningObjectAvailable = (type) => async (dispatch) => {
	dispatch({ type: GET_LEARNING_OBJECT_AVAILABLE_REQUEST });
	try {
		const response = await learningObjects.getLearningObjectAvailable(type);
		if (response.data.status === 1) {
			const Type = type;
			const data = response.data.data[Type];

			dispatch({
				type: GET_LEARNING_OBJECT_AVAILABLE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_LEARNING_OBJECT_AVAILABLE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LEARNING_OBJECT_AVAILABLE_ERROR });
		toast.error(error?.message);
	}
};

export const editLearningObject = (id, data) => async (dispatch) => {
	if (data.credit_type && !data.credit_id) {
		toast.error(tr`credit id is required`);
	} else {
		dispatch({ type: EDIT_LEARNING_OBJECT_REQUEST });
		try {
			const response = await learningObjects.editLessonObject(id, data);
			if (response.data.status === 1) {
				dispatch({
					type: EDIT_LEARNING_OBJECT_SUCCESS,
				});

				dispatch(
					userLogsAsync([
						{
							interface_name: "learning object",
							operation: "update",
							context: data,
						},
					])
				);
			} else {
				dispatch({ type: EDIT_LEARNING_OBJECT_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: EDIT_LEARNING_OBJECT_ERROR });
			toast.error(error?.message);
		}
	}
};

export const addValuesToLearningObject = (name, value) => async (dispatch) => {
	dispatch({ type: ADD_VALUES_TO_LEARNING_OBJECT, payload: { name, value } });
};

export const addValuesToStateMachine = (name, value) => async (dispatch) => {
	dispatch({ type: ADD_VALUES_TO_STATE_MACHINE, payload: { name, value } });
};

export const getStateMachineById = (learningObjectId) => async (dispatch) => {
	dispatch({ type: GET_STATE_MACHINE_BY_ID_REQUEST });
	try {
		const response = await learningObjects.getStateMachineById(learningObjectId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_STATE_MACHINE_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_STATE_MACHINE_BY_ID_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_STATE_MACHINE_BY_ID_ERROR });
		toast.error(error?.message);
	}
};

export const getStateMachineComponents = (type) => async (dispatch) => {
	dispatch({ type: GET_STATE_MACHINE_COMPONENTS_REQUEST });
	try {
		const response = await learningObjects.getStateMachineComponents(type);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_STATE_MACHINE_COMPONENTS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_STATE_MACHINE_COMPONENTS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_STATE_MACHINE_COMPONENTS_ERROR });
		toast.error(error?.message);
	}
};

export const addStateMachine = (learningObjectId, data) => async (dispatch) => {
	if (!data.start_object_status_name) {
		toast.error(tr`start_status_is_required`);
	} else if (!data.end_object_statuses_names || (data.end_object_statuses_names && data.end_object_statuses_names?.length == 0)) {
		toast.error(tr`end_status_is_required`);
	} else {
		dispatch({ type: ADD_STATE_MACHINE_REQUEST });
		try {
			const response = await learningObjects.addStateMachine(learningObjectId, data);
			if (response.data.status === 1) {
				dispatch({
					type: ADD_STATE_MACHINE_SUCCESS,
				});
				dispatch(emptyStateMachine());
				dispatch(
					userLogsAsync([
						{
							interface_name: "learning object",
							operation: "Add",
							context: data,
						},
					])
				);
			} else {
				dispatch({ type: ADD_STATE_MACHINE_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: ADD_STATE_MACHINE_ERROR });
			toast.error(error?.message);
		}
	}
};

export const editStateMachine = (stateMachineId, data, learningObjectId) => async (dispatch) => {
	dispatch({ type: EDIT_STATE_MACHINE_REQUEST });
	try {
		const response = await learningObjects.editStateMachine(stateMachineId, data);
		if (response.data.status === 1) {
			dispatch({
				type: EDIT_STATE_MACHINE_SUCCESS,
			});
			dispatch(getStateMachineById(learningObjectId));
			dispatch(
				userLogsAsync([
					{
						interface_name: "state machine",
						operation: "update",
						context: data,
					},
				])
			);
		} else {
			dispatch({ type: EDIT_STATE_MACHINE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: EDIT_STATE_MACHINE_ERROR });
		toast.error(error?.message);
	}
};

export const getInteractionConstraintType = (name, interactionValue, nodeId, constraintId) => async (dispatch) => {
	dispatch({
		type: GET_INTERACTION_CONSTRAINT_TYPE,
		payload: { name, interactionValue, nodeId, constraintId },
	});
};

export const addNewNode = () => async (dispatch) => {
	dispatch({
		type: ADD_NEW_NODE,
	});
};

export const removeNode = (nodeId) => async (dispatch) => {
	dispatch({
		type: REMOVE_NODE,
		payload: { nodeId },
	});
};

export const addStatusToNode = (name, value, nodeId) => async (dispatch) => {
	dispatch({
		type: ADD_STATUS_TO_NODE,
		payload: { name, value, nodeId },
	});
};

export const setNewStatusToStartAndEndNode = (selectedStatus) => async (dispatch) => {
	dispatch({
		type: SET_NEW_STATUS_TO_START_AND_END_NODE,
		payload: { selectedStatus },
	});
};

export const addConstraintToNode = (nodeId) => async (dispatch) => {
	dispatch({
		type: ADD_CONSTRAINTS_TO_NODE,
		payload: { nodeId },
	});
};

export const removeConstraintFromNode = (nodeId, constraintId) => async (dispatch) => {
	dispatch({
		type: REMOVE_CONSTRAINTS_FROM_NODE,
		payload: { nodeId, constraintId },
	});
};

export const addDataToConstraint = (name, value, nodeId, constraintId) => async (dispatch) => {
	dispatch({
		type: ADD_DATA_TO_CONSTRAINT,
		payload: { name, value, nodeId, constraintId },
	});
};

export const addResponseConstraintTypeToConstraint = (name, value, nodeId, constraintId) => async (dispatch) => {
	dispatch({
		type: ADD_RESPONSE_CONSTRAINT_TYPE_TO_CONSTRAINT,
		payload: { name, value, nodeId, constraintId },
	});
};

export const addResponseConstraint = (name, value, nodeId, constraintId, responseScoreType) => async (dispatch) => {
	dispatch({
		type: ADD_RESPONSE_CONSTRAINT,
		payload: { name, value, nodeId, constraintId, responseScoreType },
	});
};

export const removeAutoCorrectAnswer = (ind, nodeId, constraintId) => async (dispatch) => {
	dispatch({
		type: REMOVE_AUTO_CORRECT_ANSWER,
		payload: { ind, nodeId, constraintId },
	});
};

export const emptyStateMachine = () => async (dispatch) => {
	dispatch({
		type: EMPTY_STATE_MACHINE,
	});
};

export const getLearningObjectMyProgressAsync = (learningObjectId) => async (dispatch) => {
	dispatch({ type: GET_LEARNING_OBJECT_MY_PROGRESS_REQUEST });
	try {
		const response = await learningObjects.getLearningObjectMyProgress(learningObjectId);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_LEARNING_OBJECT_MY_PROGRESS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_LEARNING_OBJECT_MY_PROGRESS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LEARNING_OBJECT_MY_PROGRESS_ERROR });
		toast.error(error?.message);
	}
};

export const userInteractionAsync = (objectId, data, learningObjectId) => async (dispatch) => {
	dispatch({ type: USER_INTERACTION_REQUEST });
	try {
		const response = await learningObjects.userInteraction(objectId, data);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: USER_INTERACTION_SUCCESS,
			});
			dispatch(getLearningObjectMyProgressAsync(learningObjectId));
		} else {
			dispatch({ type: USER_INTERACTION_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: USER_INTERACTION_ERROR });
		toast.error(error?.message);
	}
};
