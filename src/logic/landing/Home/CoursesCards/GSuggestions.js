import React, { useContext, useEffect, useState } from "react";
import { Pagination, Navigation } from "swiper";
import { LandingPageContext } from "../GHome";
import { SwiperSlide } from "swiper/react";
import { Services } from "engine/services";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import styles from "../Home.module.scss";
import RCard from "components/Global/RComs/RCards/RCard";
import REmptyData from "components/RComponents/REmptyData";

const GSuggestions = () => {
	const LandingPageData = useContext(LandingPageContext);
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		if (
			LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.suggestions &&
			LandingPageData.landingPage.suggestions.courses &&
			LandingPageData.landingPage.suggestions.courses.length > 0
		) {
			buildCourses();
		}
	}, [
		LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.suggestions &&
			LandingPageData.landingPage.suggestions.courses &&
			LandingPageData.landingPage.suggestions.courses.length > 0,
		LandingPageData.cartItems,
		LandingPageData.addToCartLoading,
	]);

	const buildCourses = () => {
		let handleCourses = [];

		LandingPageData.landingPage?.suggestions?.courses?.map((course, index) => {
			let data = {};
			data.image = course.image ? `${Services.courses_manager.file + course.image}` : null;
			data.icon = course.icon ? `${Services.courses_manager.file + course.icon}` : null;

			data.id = course.id;
			data.name = course?.name;
			data.color = course?.color;
			data.rate = course?.rating;
			data.categoryName = course?.category?.category_name;
			data.discount = course?.discounts;
			data.users = course?.teachers;
			data.isOnline = course?.isOnline;
			data.extra = course?.extra;
			data.overview = course?.overview;
			data.isEnroll = course?.isEnrolled;
			data.enrollCourse = {
				onClick: () => LandingPageData.handleEnrollInCourse(course.id),
				loading: LandingPageData.enrollInCourseLoading,
			};
			data.addToCart = {
				onClick: () => LandingPageData.handleAddToCart({ productID: course?.id, productType: course?.paymentInfo?.type }),
				loading: LandingPageData.addToCartLoading,
				disabled: LandingPageData.cartItems && LandingPageData.cartItems?.find((item) => +item.ProductID == course.id) ? true : false,
			};

			data.link = `/landing/course/${course.id}/course-overview`;
			handleCourses.push(data);
		});

		setCourses(handleCourses);
	};
	return (
		<section className={styles.padding}>
			<h6>{LandingPageData?.landingPage?.suggestions?.label}</h6>
			<section className={styles.all_courses}>
				{courses && courses?.length > 0 ? (
					<RSwiper slidesPerView={4} navigation={false} modules={[Pagination, Navigation]}>
						{courses.map((course) => (
							<SwiperSlide key={course.id}>
								<RCard course={course} hiddenFlag={true} />
							</SwiperSlide>
						))}
					</RSwiper>
				) : (
					<div style={{ justifyContent: "center" }}>
						<REmptyData />
					</div>
				)}
			</section>
		</section>
	);
};

export default GSuggestions;
