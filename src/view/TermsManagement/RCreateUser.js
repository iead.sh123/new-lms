import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";
import tr from "components/Global/RComs/RTranslator";
import { FormGroup, Input, FormText } from "reactstrap";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import Switch from "react-bootstrap-switch";
import RPasswordFields from "components/Global/RComs/RInputs/RPasswordFields";

const RCreateUser = ({ values, errors, touched, handleBlur, handleChange, getInputClass, setFieldValue }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ width: "100%" }}>
				<RFileSuite
					parentCallback={(value) => setFieldValue("profile", value)}
					roundWidth={{ width: "70px", height: "70px", type: "default" }}
					singleFile={true}
					showFileAdd={true}
					binary={true}
					value={values?.profile}
					fileType={["image/*"]}
					uploadName="upload"
					theme="round"
				></RFileSuite>
			</RFlex>
			<RFlex styleProps={{ width: "100%" }}>
				<FormGroup style={{ width: "100%" }}>
					<label>{tr("full_name")}</label>
					<Input
						name="name"
						type="text"
						value={values.name}
						placeholder={tr("full_name")}
						onBlur={handleBlur}
						onChange={handleChange}
						className={getInputClass(touched, errors, "name")}
					/>
					{touched.name && errors.name && <FormText color="danger">{errors.name}</FormText>}
				</FormGroup>
			</RFlex>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
				<FormGroup style={{ width: "48%" }}>
					<label>{tr("first_name")}</label>
					<Input
						name="first_name"
						type="text"
						value={values.first_name}
						placeholder={tr("first_name")}
						onBlur={handleBlur}
						onChange={handleChange}
						className={getInputClass(touched, errors, "first_name")}
					/>
					{touched.first_name && errors.first_name && <FormText color="danger">{errors.first_name}</FormText>}
				</FormGroup>

				<FormGroup style={{ width: "48%" }}>
					<label>{tr("last_name")}</label>
					<Input
						name="last_name"
						type="text"
						value={values.last_name}
						placeholder={tr("last_name")}
						onBlur={handleBlur}
						onChange={handleChange}
						className={getInputClass(touched, errors, "last_name")}
					/>
					{touched.last_name && errors.last_name && <FormText color="danger">{errors.last_name}</FormText>}
				</FormGroup>
			</RFlex>

			<RFlex styleProps={{ width: "100%" }}>
				<FormGroup style={{ width: "100%" }}>
					<label>{tr("email")}</label>
					<Input
						name="email"
						type="email"
						value={values.email}
						placeholder={tr("email")}
						onBlur={handleBlur}
						onChange={handleChange}
						className={getInputClass(touched, errors, "email")}
					/>
					{touched.email && errors.email && <FormText color="danger">{errors.email}</FormText>}
				</FormGroup>
			</RFlex>

			<RFlex styleProps={{ width: "100%" }}>
				<FormGroup style={{ width: "100%" }}>
					<label>{tr("password")}</label>
					<RPasswordFields
						onChange={handleChange}
						onBlur={handleBlur}
						touched={touched.password}
						errors={errors.password}
						value={values.password}
						name={`password`}
					/>
				</FormGroup>
			</RFlex>

			<RFlex styleProps={{ width: "100%" }}>
				<label>{tr("active")}</label>

				<Switch
					defaultValue={true}
					offText={tr`off`}
					onText={tr`on`}
					onColor="success"
					offColor="success"
					onChange={(event, status) => setFieldValue("active", status)}
					size="small"
				/>
			</RFlex>
		</RFlex>
	);
};

export default RCreateUser;
