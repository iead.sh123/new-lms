import firebase from "firebase";
import firebaseConfig from "views/auth/firebase-config";

firebase.initializeApp(firebaseConfig);
export default firebase;
// export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();